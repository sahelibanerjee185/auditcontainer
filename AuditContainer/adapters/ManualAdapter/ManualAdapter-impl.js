/*
 * Nov, 23 2015
 * ManualAdapter-impl.js
 * Preda Alin Cristinel
 */


function onRequestSuccess(data, context) {
	return {
		isSuccessful: true,
		status: 200,
		errorCode: 0,
		errorMsg: null,
		invocationContext: context,
		invocationResult: data
	}
}

function onRequestFailure(errorMsg, errorCode, data, context) {
	return {
		isSuccessful: false,
		status: 200,
		errorCode: errorCode,
		errorMsg: errorMsg,
		invocationContext: context,
		invocationResult: data
	}
}

function userCredentialsValid(username, token, context) {

	// Verify user and token in our database
	var invocationData = {
			adapter : "DBAdapterContainer",
			procedure : "verifyToken",
			parameters : [username, token, (context != undefined ? context : null)]
	};
	var data = WL.Server.invokeProcedure(invocationData);

	// Check returned data
	if (data != undefined && data.invocationResult != undefined) {
		
		// Return token validity
		return data.invocationResult.tokenValid === true;
	}
	
	// Incorrect data - possibly user does not exist in the database
	return false;
}

function getManual(username, token, url, context) {
	
	// Sanity check
	// 1. Username and token
	if (username == undefined || username.length == 0) {
		return onRequestFailure("User credentials not provided (username)", 105, null, context);
	}
	
	var changedURL = encodeURI(WL.Server.configuration["manualsPath"] + url.slice(url.lastIndexOf("/")));
	
	var javaObject = new com.ibm.audit.FileDownload();
	var pdf = javaObject.download(changedURL);
	
	switch(pdf) {
    case "0":
    	return onRequestFailure("Malformed URL",107,null,context);
        break;
    case "1":
    	return onRequestFailure("Failed to open connection from URL",107,null,context);
        break;
    case "2":
    	return onRequestFailure("Failed to set Authorization header",107,null,context);
        break;
    case "3":
    	return onRequestFailure("IOException when opening input strem",107,null,context);
        break;
    case "4":
    	return onRequestFailure("IOException after successful connection",107,null,context);
        break;
    default:
    	return onRequestSuccess(pdf,context);
}
	
}
