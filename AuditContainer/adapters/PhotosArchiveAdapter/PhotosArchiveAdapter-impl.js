/**
 * May, 30 2016
 * PhotosArchiveAdapter.js
 * Eugen Buzila
 */

//Standard success handler
function onDBRequestSuccess(data, context) {
	return {
		isSuccessful : true,
		status : 200,
		errorCode : 0,
		errorMsg : null,
		invocationContext : context,
		invocationResult : data
	}
}

//Standard fail handler
function onDBRequestFailure(errorMsg, errorCode, data, context) {
	return {
		isSuccessful : false,
		status : 200,
		errorCode : errorCode,
		errorMsg : errorMsg,
		invocationContext : context,
		data : data
	}
}

//Mocked functionality 
function userCredentialsValid(username, token, context) {
	return true;
	// Verify user and token in our database
	var invocationData = {
			adapter : "DBAdapterContainer",
			procedure : "verifyToken",
			parameters : [username, token, (context != undefined ? context : null)]
	};
	var data = WL.Server.invokeProcedure(invocationData);

	// Check returned data
	if (data != undefined && data.invocationResult != undefined) {
		
		// Return token validity
		return data.invocationResult.tokenValid === true;
	}
	
	// Incorrect data - possibly user does not exist in the database
	return false;
}

/**
* @name addArchive
* @desc Upload a base64 photos archive on server
* @param {String} username of the current logged in user 
* @param {Integer} visitId unique identifier for an existing visit
* @param {String} archive base64 encoded string representing an archive of photos
* @param {Object} context 
* @returns {Function} - Standard success/fail function 
* @memberOf Adapters.PhotosArchiveAdapter
*/
function addArchive(username,visitId,archive,context){
	var archivePath = uploadFile(WL.Server.configuration["photosBaseDir"] ,archive);
	
	if(archivePath != null) 
		return onDBRequestSuccess(archivePath, context);
	else 
		return onDBRequestFailure("Could not write archive file to disk " , 110, null, context);
}

/**
* @name getPictureArchive
* @desc Download the photos archive from server as base64
* @param {String} username of the current logged in user 
* @param {String} token 
* @param {String} archiveName to be retrieved from server
* @param {Object} context 
* @returns {Function} - Standard success/fail function 
* @memberOf Adapters.PhotosArchiveAdapter
*/
function getPictureArchive(username, token, archiveName, context) {
	//1. Sanity check Username and token (mocked)
	if (username == undefined || username.length == 0) {
		return onDBRequestFailure("User credentials not provided (username)", 105, null, context);
	}
	
	var archive = getFile(WL.Server.configuration["photosBaseDir"] + archiveName);
	if (archive != null && archive != undefined)
		return onDBRequestSuccess(archive, context);
	else
		return onDBRequestFailure("Could not read archive file from disk", 111, archive, context);
	
}

//Helper function to upload files to a specified path
function uploadFile(path,file){
	var javaObject = new com.ibm.audit.FileHandler();
	var success = javaObject.writeStringToFile(path,file);
	 
	return success;
}

//Helper function to download files from a specified path
function getFile(path){
	var javaObject = new com.ibm.audit.FileHandler();
	return javaObject.readStringFromFile(path);
}