/*
 *  Licensed Materials - Property of IBM
 *  5725-I43 (C) Copyright IBM Corp. 2011, 2013. All Rights Reserved.
 *  US Government Users Restricted Rights - Use, duplication or
 *  disclosure restricted by GSA ADP Schedule Contract with IBM Corp.
 */

/**
 * February, 16 2017
 * PdfArchiveAdapter.js
 * Andrew Radulescu
 */


//Standard success handler 
function onRequestSuccess(data, context) {
    return {
        isSuccessful: true,
        status: 200,
        errorCode: 0,
        errorMsg: null,
        invocationContext: context,
        invocationResult: data
    }
};

//Standard fail handler 
function onRequestFailure(errorMsg, errorCode, data, context) {
    return {
        isSuccessful: false,
        status: 200,
        errorCode: errorCode,
        errorMsg: errorMsg,
        invocationContext: context,
        data: data
    }
};


/**
* @name uploadPDF
* @desc Upload PDF file on server
* @param {Integer} visitId unique identifier for an existing visit
* @param {String} file base64 converted PDF file 
* @param {Object} context
* @returns {Object} - standard success or fail object 
* @memberOf Adapters.PdfArchiveAdapter
*/
function uploadPDF(visitId, file, context) {

    //Retrieve path of pdfs directory via worklight
    var path = WL.Server.configuration["pdfsBaseDir"];

    //Build up file name to be created on server
    var fileName = visitId + ".pdf";

    //Try to upload - return boolean
    var isCreated = uploadFile(WL.Server.configuration["pdfsBaseDir"], file, fileName);

    if (isCreated) {
        WL.Logger.info("PDF Uploaded successfully with name: " + fileName);
        return onRequestSuccess(path, context);
    } else {
        return onRequestFailure("Could not write PDF file to disk ", 110, null, context);
    }
};


/**
* @name downloadPDF
* @desc Download PDF from server
* @param {Integer} visitId unique identifier for an existing visit
* @param {Object} context
* @returns {Function} - Standard success/fail function 
* @memberOf Adapters.PdfArchiveAdapter
*/
function downloadPDF(visitId, context) {

    if (visitId == undefined) {
        return onRequestFailure("VisitID parameter was not provided", 110, null, context);
    }
    var fileName = visitId + ".pdf";

    //Retrieve path of pdfs directory via worklight
    var path = WL.Server.configuration["pdfsBaseDir"];

    var pdf = getFile(path + "/" + fileName);

    if (pdf != null && pdf != undefined) {
        WL.Logger.info("PDF Downloaded successfully with name: " + fileName);
        return onRequestSuccess(pdf, context);
    } else {
        return onRequestFailure("Could not read PDF file from disk ", 110, null, context);
    }
}


// Generic function to upload file 
function uploadFile(path, file, fileName) {

    //Use FileHandler to write to server dir
    var javaObject = new com.ibm.audit.FileHandler();

    //Return file name if file was written or NULL if error
    var fileName = javaObject.writePdfAsBinaryToFile(path, file, fileName);

    //Check if file was written
    if (fileName) {
        return true;
    }

    return false;
};

// Generic function to retrieve file 
function getFile(path) {
    var javaObject = new com.ibm.audit.FileHandler();
    return javaObject.readStringFromFile(path);
}




