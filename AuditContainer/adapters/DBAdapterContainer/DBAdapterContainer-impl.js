/*
 *  Licensed Materials - Property of IBM
 *  5725-I43 (C) Copyright IBM Corp. 2011, 2013. All Rights Reserved.
 *  US Government Users Restricted Rights - Use, duplication or
 *  disclosure restricted by GSA ADP Schedule Contract with IBM Corp.
 */

/**
 * Nov, 23 2015
 * DBAdapter-impl.js
 * Iulian Corcoja, Preda Alin Cristinel
 */

/* Error Codes:
 * 
 * 0   - No error
 * 99  - Unknown error
 * 100 - Database SQL statement execution error
 * 101 - Database SQL execution error thrown (couldn't connect to the database)
 * 102 - Invalid or expired token
 * 103 - Username not provided
 * 104 - Token not provided
 * 105 - User credentials not provided (username and/or token)
 * 106 - Password not provided
 * 107 - Bad URL parameter
 * 108 - Could not generate a new token (database request failed)
 * 109 - Login failure (incorrect username or password)
 * 
 */

/**
 * @category Constants
 */

var expiryTime = 604800.0; // One week in seconds

/**
 * @category Database statements
 */

var getUserDataStatement = WL.Server.createSQLStatement(
		"SELECT \"id\", \"username\", \"token\", \"expiry_date\", \"last_login\" " +
		"FROM USERS " +
		"WHERE \"username\" = ?"
		);

var updateUserTokenStatement = WL.Server.createSQLStatement(
		"UPDATE USERS " +
		"SET \"token\" = ?, \"expiry_date\" = ?, \"last_login\" = ? " +
		"WHERE \"username\" = ?");

var updateLastLoginStatement = WL.Server.createSQLStatement(
		"UPDATE USERS " +
		"SET \"last_login\" = ? " +
		"WHERE \"username\" = ?");

var addUserTokenStatement = WL.Server.createSQLStatement(
		"INSERT INTO USERS (\"username\", \"token\", \"expiry_date\", \"last_login\") " +
		"VALUES (?, ?, ?, ?)");

var getManualListStatement = WL.Server.createSQLStatement(
		"SELECT \"name\", \"locationPath\", \"language\", \"extension\", \"addedDate\" " +
//		"SELECT name, locationPath, language, extension, addedDate " +
		"FROM manuals "
//		"FROM manual "
		);


/*
 * Return dictionary objects
 *
 * isSuccessful			Whether the procedure call is successful. Note: The text following the table explains the
 * 						circumstances when a request is considered to be successful.
 * status				HTTP status code from the procedure call. This is not the HTTP code from the backend service,
 * 						only from the connection with the Worklight Server.
 * errorCode			A possible error code if the call is not successful.
 * errorMsg				A possible error message if the call is not successful.
 * invocationContext	An optional object that is sent in the procedure call and is returned as-is.
 * invocationResult		JSON object that is returned by your procedure call. This object may be augmented with
 * 						additional data such as session information.
 */

function onDBRequestSuccess(data, context) {
	return {
		isSuccessful : true,
		status : 200,
		errorCode : 0,
		errorMsg : null,
		invocationContext : context,
		invocationResult : data
	}
}

function onDBRequestFailure(errorMsg, errorCode, data, context) {
	return {
		isSuccessful : false,
		status : 200,
		errorCode : errorCode,
		errorMsg : errorMsg,
		invocationContext : context,
		data : data
	}
}

/*
 * Database requests
 */
function invokeSQL(statement, parameters, context) {

	try {
		// Invoke SQL statement
		var returnData = WL.Server.invokeSQLStatement({
			preparedStatement : statement,
			parameters : parameters
		});

		// Return SQL statement data
		return returnData;

	} catch (error) {
		return onDBRequestFailure("Database SQL execution error thrown", 101, error, context);
	}
}

/*
 * Adapter methods
 */
	function getManualList(username, token, context){
	
	// Sanity check
	// Username and token
	if (username == undefined || username.length == 0 ||
			token == undefined || token.length == 0) {
		return onDBRequestFailure("User credentials not provided (username and/or token)", 105, null, context);
	}
	
	var data = invokeSQL(getManualListStatement, null, context);
	
	if (data.isSuccessful && data.resultSet.length > 0) {
		var manuals = [];
		
		for (var manual in data.resultSet){
			manuals.push({
				title : data.resultSet[manual].name,
				language : data.resultSet[manual].language,
				extension : data.resultSet[manual].extension,
				serverPath : data.resultSet[manual].locationPath,
				addedDate : data.resultSet[manual].addedDate,
			});
		}
		return onDBRequestSuccess(manuals, context);
		
	}
}
	
	
function getUserData(username, context) {

	// Sanity check
	if (username == undefined || username.length == 0) {
		return onDBRequestFailure("Username not provided", 103, null, context);
	}
	
	// Set username to lower case
	username = username.toLowerCase();

	// Invoke Get User Data Statement
	var data = invokeSQL(getUserDataStatement, [username], context);

	if (data.isSuccessful && data.resultSet.length > 0) {
		return onDBRequestSuccess(data.resultSet[0], context);
	}
	return onDBRequestFailure("User not found - SQL error", 100, null, context);
}

function setNewToken(username, context) {

	// Sanity check
	if (username == undefined || username.length == 0) {
		return onDBRequestFailure("Username not provided", 103, null, context);
	}
	
	// Set username to lower case
	username = username.toLowerCase();

	// First generate a new UUID and get current date
	var uuid = guid().toString();
	var now = dateToSQLDateTime(new Date()).toString();

	// Second, try to update user token and expiry date
	var updateData = updateUserToken(username, uuid, now, context);
	if (updateData.isSuccessful) {

		// Update successful, return token and expiry date
		return onDBRequestSuccess({
			token : uuid,
			expiryDate : now
		}, context);
	} else {

		// Could not update user data, perhaps it hasn't been added to our database yet - add it
		var addUserData = addUserToken(username, uuid, now, context);
		if (addUserData.isSuccessful) {

			// User has been successfully added to the database, return expiry date and token
			return onDBRequestSuccess({
				token : uuid,
				expiryDate : now
			}, context);
		} else {
			
			// Could not add new user to database
			//return addUserData;
			return onDBRequestFailure("Unknown error - SQL error", 99, addUserData, context);
		}
	}
	return onDBRequestFailure("Unknown error - SQL error", 99, updateData, context);
}

function verifyToken(username, token, context) {

	// Sanity check
	if (username == undefined || username.length == 0) {
		return onDBRequestFailure("Username not provided", 103, null, context);
	}
	if (token == undefined || token.length == 0) {
		return onDBRequestFailure("Token not provided", 104, null, context);
	}
	
	// Set username to lower case
	username = username.toLowerCase();

	// Get user data from the database
	var userData = getUserData(username);

	if (userData.isSuccessful) {
		var now = new Date();

		// Check if token matches the token from database
		var tokensMatch = token == userData.invocationResult.token;

		if (tokensMatch != true) {
			
			// Delete token from user data
			delete userData.invocationResult.token;

			// Return database success with the token information
			var returnData = onDBRequestSuccess({
				userData : userData.invocationResult,
				serverDate : now,
				tokenDate : null,
				tokenLifeTime : null,
				tokenValid : tokensMatch
			}, context);

			WL.Logger.info(returnData);
			return returnData;
		}

		// Tokens are matching, compute elapsed time and check if the token is still valid (hasn't expired)
		var tokeExpiryDate = new Date(userData.invocationResult.expiry_date);
		var timeDiff = (now.getTime() - tokeExpiryDate.getTime()) / 1000.0;

		// Check if token is expired
		var tokenExpired = timeDiff > expiryTime;
		
		// Delete token from user data
		delete userData.invocationResult.token;

		// Update last_login 
		if (!tokenExpired){
			var updatedTime = dateToSQLDateTime(new Date()).toString();
			updateLogin(username, updatedTime, context);
			userData.invocationResult.last_login = updatedTime;
		}
		
		// Return database success with the token information
		var returnData = onDBRequestSuccess({
			userData : userData.invocationResult,
			serverDate : now,
			tokenDate : tokeExpiryDate,
			tokenLifeTime : timeDiff,
			tokenValid : !tokenExpired
		}, context);

		WL.Logger.info(returnData);
		
		
		return returnData;
	} else {

		// Failed to get user from the database (not found)
		return onDBRequestFailure(userData.errorMsg + " - SQL error", 100, username, context);
	}
}

/*
 * Helper methods
 */

function updateLogin(username, now, context){
	// Set username to lower case
	username = username.toLowerCase();
	
	// Invoke Update Last Login Statement
	var data = invokeSQL(updateLastLoginStatement, [now, username], context);
	
}

function addUserToken(username, token, expiryDate, context) {

	// Invoke SQL statement
	var now = dateToSQLDateTime(new Date()).toString();
	var data = invokeSQL(addUserTokenStatement, [ username, token, expiryDate, now ], context);

	WL.Logger.info(data);

	if (data.isSuccessful) {
		return onDBRequestSuccess(username, context);
	}
	return onDBRequestFailure("Could not add new user - SQL error", 100,
			{
				data: data,
				username: username,
				token: token,
				expiryDate: expiryDate,
				now: now
		}, context);
}

function updateUserToken(username, token, expiryDate, context) {

	// Invoke SQL statement
	var now = dateToSQLDateTime(new Date()).toString();
	var data = invokeSQL(updateUserTokenStatement, [ token, expiryDate, now, username ], context);

	WL.Logger.info(data);

	if (data.isSuccessful && data.updateStatementResult.updateCount > 0) {
		return onDBRequestSuccess(data, context);
	}
	return onDBRequestFailure("Could not update user token - SQL error", 100,
			username, context);
}

function guid() {

	// Generate a random sequence
	function s4() {
		return Math.floor((1 + Math.random()) * 0x10000).toString(16)
				.substring(1);
	}

	// Return the GUID
	return s4() + s4() + "-" + s4() + "-" + s4() + "-" + s4() + "-" + s4()
			+ s4() + s4();
}

function dateToSQLDateTime(date) {
	return date.getUTCFullYear() + '-'
			+ ('00' + (date.getUTCMonth() + 1)).slice(-2) + '-'
			+ ('00' + date.getUTCDate()).slice(-2) + ' '
			+ ('00' + date.getUTCHours()).slice(-2) + ':'
			+ ('00' + date.getUTCMinutes()).slice(-2) + ':'
			+ ('00' + date.getUTCSeconds()).slice(-2);
}
