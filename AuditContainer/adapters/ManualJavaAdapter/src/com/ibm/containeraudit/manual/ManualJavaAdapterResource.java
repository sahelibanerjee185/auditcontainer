/*
 *    Licensed Materials - Property of IBM
 *    5725-I43 (C) Copyright IBM Corp. 2015. All Rights Reserved.
 *    US Government Users Restricted Rights - Use, duplication or
 *    disclosure restricted by GSA ADP Schedule Contract with IBM Corp.
*/

package com.ibm.containeraudit.manual;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import javax.xml.bind.DatatypeConverter;

import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.ibm.audit.FileDownload;
import com.worklight.adapters.rest.api.WLServerAPI;
import com.worklight.adapters.rest.api.WLServerAPIProvider;
import com.worklight.core.auth.OAuthSecurity;
import com.worklight.server.bundle.api.WorklightConfiguration;

@Path("/")
public class ManualJavaAdapterResource {

	// Define logger (Standard java.util.Logger)
	static Logger logger = Logger.getLogger(ManualJavaAdapterResource.class.getName());

	// Define the server api to be able to perform server operations
	WLServerAPI api = WLServerAPIProvider.getWLServerAPI();

	// Define standard errors 
	public static final String ERROR_CASE_0 = "Malformed URL";
	public static final String ERROR_CASE_1 = "Failed to open connection from URL";
	public static final String ERROR_CASE_2 = "Failed to set Authorization header";
	public static final String ERROR_CASE_3 = "IOException when opening input stream";
	public static final String ERROR_CASE_4 = "IOException after successful connection";
	public static final String ERROR_CASE_5 = "User credentials not provided (username)";
	public static final String ERROR_CASE_6 = "Cannot decode URL";
	public static final int ERROR_CODE_107 = 107;
	public static final int ERROR_CODE_105 = 105;


	/**
	* @name manualDownload
	* @desc Get the PDF files (Manuals) from server 
	* @param {String} paramsjson - JSON object including all needed parameters
	* { username, token, url, context }
	* URL - used to locate the resource on server 
	* @returns {Object} - Response object 
	* @memberOf Adapters.ManualJavaAdapter
	*/
	@GET
	@OAuthSecurity(enabled = true)
	@Path("/getManual")
	@Produces("application/json")
	public Response manualDownload(@QueryParam("parameters") String paramsjson) {

		String serverPath = WorklightConfiguration.getInstance().getStringProperty("manualsPath");

		JSONArray params = null;
		JSONParser parser = new JSONParser();

		// Get the parameters string and parse them to JSONArray
		try {
			params = (JSONArray) parser.parse(paramsjson);
		} catch (ParseException e) {
			e.printStackTrace();
		}

		// The JS adapter receives an array called "parameters" containing
		// (username, token, url, context)
		String username = null, token = null, url = null, context = null;
		try {
			username = params.get(0).toString();
			token = params.get(1).toString();
			url = params.get(2).toString();
			context = params.get(3).toString();

			if (username.isEmpty() || token.isEmpty()) {
				ResponseObject resp = new ResponseObject("false", 200, ERROR_CODE_105, ERROR_CASE_5, context, null);
				return Response.ok(resp.toJsonString()).build();
			}

			String encodedURL = (URLEncoder.encode(url.substring(url.lastIndexOf("/") + 1), "UTF-8")).replace("+",
					"%20");
			url = serverPath + "/" + encodedURL;
			logger.log(Level.WARNING, "manual URL - " + url);

		} catch (IndexOutOfBoundsException ex) {
			ex.printStackTrace();

			if (username == null || username.isEmpty() == true || token == null || url == null) {
				ResponseObject resp = new ResponseObject("false", 200, ERROR_CODE_105, ERROR_CASE_5, context, null);
				return Response.ok(resp.toJsonString()).build();
			}
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			ResponseObject resp = new ResponseObject("false", 200, ERROR_CODE_105, ERROR_CASE_6, context, null);
			return Response.ok(resp.toJsonString()).build();
		}

		//Create FileDownload instance to start download from URL
		FileDownload fileDownload = new FileDownload();
		//Store the result to identify possible errors
		String result = fileDownload.download(url);

		ResponseObject resp = null;
		switch (result) {
		
		case "0":
			resp = new ResponseObject("false", 200, ERROR_CODE_107, ERROR_CASE_0, context, null);
			return Response.ok(resp.toJsonString()).build();
		case "1":
			resp = new ResponseObject("false", 200, ERROR_CODE_107, ERROR_CASE_1, context, null);
			return Response.ok(resp.toJsonString()).build();
		case "2":
			resp = new ResponseObject("false", 200, ERROR_CODE_107, ERROR_CASE_2, context, null);
			return Response.ok(resp.toJsonString()).build();
		case "3":
			resp = new ResponseObject("false", 200, ERROR_CODE_107, ERROR_CASE_3, context, null);
			return Response.ok(resp.toJsonString()).build();
		case "4":
			resp = new ResponseObject("false", 200, ERROR_CODE_107, ERROR_CASE_4, context, null);
			return Response.ok(resp.toJsonString()).build();
		default:
			resp = new ResponseObject("true", 200, 0, "", context, result);
			return Response.ok(resp.toJsonString()).build();
		}

	}
	
}
