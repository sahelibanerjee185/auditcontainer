/*
 * Apr, 5 2016
 * AuditAdapter-impl.js
 * Stavarache Vlad
 */

/* Error Codes:
 * 
 * 0   - No error
 * 99  - Unknown error
 * 100 - Database SQL statement execution error
 * 101 - Database SQL execution error thrown (couldn't connect to the database)
 * 102 - Invalid or expired token
 * 103 - Username not provided
 * 104 - Token not provided
 * 105 - User credentials not provided (username and/or token)
 * 106 - Password not provided
 * 107 - Query arguments invalid (not provided or incorrect format)
 * 108 - Could not generate a new token (database request failed)
 * 109 - Login failure (incorrect username or password)
 * 110 - No rows affected by update
 */

/**
 * @category Database statements
 */

var beginTransaction = WL.Server.createSQLStatement(
	"BEGIN TRANSACTION"
);

var commitTransaction = WL.Server.createSQLStatement(
	"COMMIT TRANSACTION"
);

var rollbackTransaction = WL.Server.createSQLStatement(
	"ROLLBACK TRANSACTION"
);

var updateContainerProcedure = WL.Server.createSQLStatement(
	"EXEC [dbo].[updateContainer] " +
	"@Shop_id = ?, " +
	"@workOrderNo = ?, " +
	"@UserId = ?, " +
	"@VisitDate = ?, " + 
	"@AuditStatus = ?, " +
	"@outMessage = '', " +
	"@outMessageCode = ''"
);

var markWorkOrderForAuditProcedure = WL.Server.createSQLStatement(
	"EXEC [dbo].[markWorkOrderForAudit] " +
	"@UserId = ?, " +
	"@workOrderNo = ?, " +
	"@VisitDate = ?, " +
	"@VisitId = ?, " +
	"@VisitDesc = ?, " +
	"@AuditStatus = ?, " +
	"@Shop_id = ?, " +
	"@outMessage = '', " +
	"@outMessageCode = ''"
);

var updateMissedWorkOrdersQuery = WL.Server.createSQLStatement(
	"UPDATE MESC1TS_AUDIT_DETAILS " +
	"SET AUDIT_STATUS = 3 " +
	"WHERE AUDITOR = ? AND " +
	"AUDIT_STATUS = 0 AND " +
	"AUDIT_DATE < ?"
);

var getWorkOrdersCountWithStatusQuery = WL.Server.createSQLStatement(
	"SELECT COUNT(*) as count FROM MESC1TS_AUDIT_HISTORY " +
	"WHERE AUDITOR = ? AND VISIT_ID = ? AND MERC_STATUS = ?"	
);

var getWorkOrdersCountQuery = WL.Server.createSQLStatement(
	"SELECT COUNT(*) as count FROM MESC1TS_AUDIT_HISTORY " +
	"WHERE AUDITOR = ? AND VISIT_ID = ?"
);

var getCalendarDataQuery = WL.Server.createSQLStatement(
	"BEGIN " +
	"DECLARE @auditor VARCHAR(50) SET @auditor = ? " +
	"END " +
	"SELECT CONVERT(VARCHAR(10), MIN(H.AUDIT_DATE), 21) AS date, MIN(H.VISIT_DESC) as description, MIN(H.SHOP_CD) as shopCode, MIN(S.SHOP_DESC) AS shopDescription, " +
	"H.VISIT_ID AS visitId, 'Finished' AS auditStatus, MIN(C.COUNTRY_DESC) as country, MIN(L.LOC_DESC) as locationDescription FROM MESC1TS_AUDIT_HISTORY H " +
	"LEFT JOIN MESC1TS_SHOP S " + 
	"ON H.SHOP_CD = S.SHOP_CD " +
	"LEFT JOIN MESC1TS_COUNTRY C " +
	"ON SUBSTRING(S.LOC_CD, 1, 2) = C.COUNTRY_CD " +
	"LEFT JOIN MESC1TS_LOCATION L " +
	"ON S.LOC_CD = L.LOC_CD " +
	"WHERE AUDITOR = @auditor " +
	"AND H.AUDIT_DATE > DATEADD(year,-1,GETDATE()) " +
	"GROUP BY VISIT_ID " +
	"UNION " +
	"SELECT CONVERT(VARCHAR(10), MIN(DH.AUDIT_DATE), 21) AS date, MIN(VISIT_DESC) AS description, MIN(DH.SHOP_CD) as shopCode, " +
	"MIN(DS.SHOP_DESC) AS shopDescription, VISIT_ID AS visitId, MIN(DSC.STATUS_DSC) AS auditStatus, " +
	"MIN(DC.COUNTRY_DESC) AS country, MIN(DL.LOC_DESC) as locationDescription " +
	"FROM MESC1TS_AUDIT_DETAILS DH " +
	"LEFT JOIN MESC1TS_SHOP DS " +
	"ON DH.SHOP_CD = DS.SHOP_CD " +
	"LEFT JOIN MESC1TS_COUNTRY DC " +
	"ON SUBSTRING(DS.LOC_CD, 1, 2) = DC.COUNTRY_CD " +
	"LEFT JOIN MESC1TS_LOCATION DL " +
	"ON DS.LOC_CD = DL.LOC_CD " +
	"LEFT JOIN MESC1TS_AUDITSTATUS_CODE DSC " +
	"ON DH.AUDIT_STATUS = DSC.AUDIT_STATUS " +
	"WHERE AUDITOR = @auditor AND (DH.AUDIT_STATUS = 0 OR DH.AUDIT_STATUS = 3) " +
	"AND DH.AUDIT_DATE > DATEADD(year,-1,GETDATE()) " +
	"GROUP BY VISIT_ID"
);

 var markWorkOrderForAuditQuery = WL.Server.createSQLStatement(
	 "BEGIN " +
	 "DECLARE @auditor VARCHAR(50) SET @auditor = ? " +
	 "DECLARE @auditDate DATETIME SET @auditDate = ? " +
	 "DECLARE @visitId VARCHAR(50) SET @visitId = ? " +
	 "DECLARE @workOrderNo INT SET @workOrderNo = ? " +
	 "DECLARE @shopCode VARCHAR(50) SET @shopCode = ? " +
	 "DECLARE @auditStatus INT SET @auditStatus = ? " +
	 "DECLARE @visitDescription VARCHAR(50) SET @visitDescription = ? " +
	 "IF EXISTS(SELECT 1 FROM MESC1TS_AUDIT_DETAILS WHERE WO_ID = @workOrderNo) " +
	 "BEGIN " +
	 "Update MESC1TS_AUDIT_DETAILS " +
	 "SET " +
	 "AUDIT_STATUS = @auditStatus, " +
	 "AUDIT_DATE = @auditDate, " +
	 "AUDITOR = @auditor, " +
	 "VISIT_DESC = @visitDescription, " +
	 "VISIT_ID = @visitId, " +
	 "SHOP_CD = @shopCode " +
	 "WHERE " +
	 "WO_ID = @workOrderNo AND " +
	 "AUDIT_STATUS <> 0 AND " +
	 "AUDIT_STATUS <> 1 " +
	 "END " +
	 "ELSE " +
	 "BEGIN " +
	 "INSERT INTO MESC1TS_AUDIT_DETAILS " +
	 "(WO_ID, " +
	 "SHOP_CD, " +
	 "AUDIT_DATE, " +
	 "AUDITOR, " +
	 "AUDIT_STATUS, " +
	 "VISIT_DESC, " +
	 "VISIT_ID) " +
	 "VALUES(@workOrderNo , " +
	 "@shopCode, " +
	 "@auditDate, " +
	 "@auditor, " +
	 "@auditStatus, " +
	 "@visitDescription, " +
	 "@visitId) " +
	 "END " +
	 "END"
);
 
var getVisitIdQuery = WL.Server.createSQLStatement(
	"SELECT GETDATE() AS CurrentDateTime"
);

var getWorkOrderCountForFinishedVisitQuery = WL.Server.createSQLStatement(
	"SELECT count(*) AS count FROM MESC1TS_AUDIT_HISTORY WHERE VISIT_ID = ? AND AUDITOR = ?"
);

// TODO  - procedure to be created for this
var getVisitsQuery = WL.Server.createSQLStatement(
	"SELECT A.WO_ID AS workOrderId, W.EQPNO AS containerNo, CONVERT(VARCHAR(10), W.REPAIR_DTE, 21) AS repairDate, " +
	"EQSTYPE AS containerType, W.WOTYPE AS woType, SC.STATUS_DSC AS woStatus, W.TOTAL_COST_LOCAL_USD as repairCost, W.STATUS_CODE as statusCode, " +
	"A.SHOP_CD AS shopCode, CONVERT(VARCHAR(10), A.AUDIT_DATE, 21) AS auditDate, AC.STATUS_DSC AS auditStatus, A.VISIT_DESC AS description, " +
	"A.VISIT_ID AS visitId, S.SHOP_DESC AS shopDescription, C.COUNTRY_DESC AS country, L.LOC_DESC AS location,W.MODE AS mode FROM MESC1TS_AUDIT_DETAILS A " +
	"LEFT JOIN MESC1TS_SHOP S " +
	"ON A.SHOP_CD = S.SHOP_CD " +
	"LEFT JOIN MESC1TS_COUNTRY C " +
	"ON SUBSTRING(S.LOC_CD, 1, 2) = C.COUNTRY_CD " +
	"LEFT JOIN MESC1TS_LOCATION L " +
	"ON S.LOC_CD = L.LOC_CD " +
	"LEFT JOIN MESC1TS_WO W " +
	"ON A.WO_ID = W.WO_ID " +
	"LEFT JOIN MESC1TS_STATUS_CODE SC " +
	"ON SC.STATUS_CODE = W.STATUS_CODE " +
	"LEFT JOIN MESC1TS_AUDITSTATUS_CODE AC " +
	"ON A.AUDIT_STATUS = AC.AUDIT_STATUS " +
	"WHERE AUDITOR = ? AND "+
	"(A.AUDIT_STATUS = 0 OR A.AUDIT_STATUS = 1 OR A.AUDIT_STATUS = 3)"
);

var getWorkOrderInfoQuery = WL.Server.createSQLStatement(
	"DECLARE @auditor VARCHAR(50) SET @auditor = ? " +
	"SELECT W.WO_ID AS workOrderId, W.EQPNO AS containerNo, CONVERT(VARCHAR(10), W.REPAIR_DTE, 21) AS repairDate, " +  
	"EQSTYPE AS containerType, W.WOTYPE AS woType, SC.STATUS_DSC AS woStatus, W.TOTAL_COST_LOCAL_USD as repairCost, W.STATUS_CODE as statusCode, " +
	"W.SHOP_CD AS shopCode, CONVERT(VARCHAR(10), A.AUDIT_DATE, 21) AS auditDate, AC.STATUS_DSC AS auditStatus, A.VISIT_DESC AS description, " +  
	"A.VISIT_ID AS visitId, S.SHOP_DESC AS shopDescription, C.COUNTRY_DESC AS country, L.LOC_DESC AS location,W.MODE AS mode FROM MESC1TS_WO W " +
	"LEFT JOIN MESC1TS_AUDIT_DETAILS A " +  
	"ON A.WO_ID = W.WO_ID " +
	"LEFT JOIN MESC1TS_SHOP S " +  
	"ON W.SHOP_CD = S.SHOP_CD " +  
	"LEFT JOIN MESC1TS_COUNTRY C " +  
	"ON SUBSTRING(S.LOC_CD, 1, 2) = C.COUNTRY_CD " +  
	"LEFT JOIN MESC1TS_LOCATION L " +  
	"ON S.LOC_CD = L.LOC_CD " +  
	"LEFT JOIN MESC1TS_STATUS_CODE SC " +  
	"ON SC.STATUS_CODE = W.STATUS_CODE " +  
	"LEFT JOIN MESC1TS_AUDITSTATUS_CODE AC " +  
	"ON A.AUDIT_STATUS = AC.AUDIT_STATUS " +  
	"WHERE ((@auditor IS NOT NULL AND A.AUDITOR = @auditor) OR (@auditor IS NULL)) AND W.WO_ID = ?"
);

var getMissedVisitWorkOrdersQuery = WL.Server.createSQLStatement(
	"SELECT A.WO_ID AS workorderid, W.EQPNO AS containerno, CONVERT(VARCHAR(10), W.REPAIR_DTE, 21) AS repairdate, " +
	"EQSTYPE AS containertype, W.WOTYPE AS wotype, SC.STATUS_DSC AS wostatus, W.TOTAL_COST_LOCAL_USD as repaircost, " +
	"A.SHOP_CD AS shopcode, CONVERT(VARCHAR(10), A.AUDIT_DATE, 21) AS auditdate, AC.STATUS_DSC AS auditstatus, A.VISIT_DESC AS description, " +
	"A.VISIT_ID AS visitid, S.SHOP_DESC AS shopdescription, C.COUNTRY_DESC AS country, L.LOC_DESC AS location, W.MODE AS mode, SC.STATUS_CODE AS statuscode FROM MESC1TS_AUDIT_DETAILS A " +
	"LEFT JOIN MESC1TS_SHOP S " +
	"ON A.SHOP_CD = S.SHOP_CD " +
	"LEFT JOIN MESC1TS_COUNTRY C " +
	"ON SUBSTRING(S.LOC_CD, 1, 2) = C.COUNTRY_CD " +
	"LEFT JOIN MESC1TS_LOCATION L " +
	"ON S.LOC_CD = L.LOC_CD " +
	"LEFT JOIN MESC1TS_WO W " +
	"ON A.WO_ID = W.WO_ID " +
	"LEFT JOIN MESC1TS_STATUS_CODE SC " +
	"ON SC.STATUS_CODE = W.STATUS_CODE " +
	"LEFT JOIN MESC1TS_AUDITSTATUS_CODE AC " +
	"ON A.AUDIT_STATUS = AC.AUDIT_STATUS " +
	"WHERE AUDITOR = ? AND " +
	"A.AUDIT_STATUS = 3 AND " +
	"A.VISIT_ID = ?"
);

var getFinishedVisitWorkOrdersQuery = WL.Server.createSQLStatement(
	"SELECT A.WO_ID AS workOrderId, W.EQPNO AS containerNo, CONVERT(VARCHAR(10), W.REPAIR_DTE, 21) AS repairDate, 'Finished' AS auditStatus, A.MERC_STATUS AS mercStatus, " +
	"MERC_COMMENTS AS mercComments, " +
	"RC.RESULT_DSC AS auditResult, EQSTYPE AS containerType, W.WOTYPE AS woType, SC.STATUS_DSC AS woStatus, W.TOTAL_COST_LOCAL_USD as repairCost, " +
	"A.SHOP_CD AS shopCode, CONVERT(VARCHAR(10), A.AUDIT_DATE, 21) AS auditDate, A.VISIT_DESC AS description, A.COMMENT AS finalComment, " +
	"A.VISIT_ID AS visitId, S.SHOP_DESC AS shopDescription, C.COUNTRY_DESC AS country, W.MODE AS mode, L.LOC_DESC AS location FROM MESC1TS_AUDIT_HISTORY A " +
	"LEFT JOIN MESC1TS_SHOP S " + 
	"ON A.SHOP_CD = S.SHOP_CD " +
	"LEFT JOIN MESC1TS_COUNTRY C " +
	"ON SUBSTRING(S.LOC_CD, 1, 2) = C.COUNTRY_CD " +
	"LEFT JOIN MESC1TS_LOCATION L " +
	"ON S.LOC_CD = L.LOC_CD " +
	"LEFT JOIN MESC1TS_WO W " +
	"ON A.WO_ID = W.WO_ID " +
	"LEFT JOIN MESC1TS_STATUS_CODE SC " +
	"ON SC.STATUS_CODE = W.STATUS_CODE " +
	"LEFT JOIN MESC1TS_AUDITRESULT_CODE RC " +
	"ON RC.AUDIT_RESULT = A.AUDIT_RESULT " +
	"WHERE AUDITOR = ? " +
	"AND VISIT_ID = ?"
);

var getFinishedVisitWorkOrdersPostRepairQuery = WL.Server.createSQLStatement(
	"SELECT A.WO_ID AS workOrderId, W.EQPNO AS containerNo, CONVERT(VARCHAR(10), W.REPAIR_DTE, 21) AS repairDate, 'Finished' AS auditStatus, A.MERC_STATUS AS mercStatus, " +
	"MERC_COMMENTS AS mercComments, A.STATUS_CODE AS statusCode, A.REPAIR_QUALITY AS repairQuality, " +
	"RC.RESULT_DSC AS auditResult, EQSTYPE AS containerType, W.WOTYPE AS woType, SC.STATUS_DSC AS woStatus, W.TOTAL_COST_LOCAL_USD as repairCost, " +
	"A.SHOP_CD AS shopCode, CONVERT(VARCHAR(10), A.AUDIT_DATE, 21) AS auditDate, A.VISIT_DESC AS description, A.COMMENT AS finalComment, " +
	"A.VISIT_ID AS visitId, S.SHOP_DESC AS shopDescription, C.COUNTRY_DESC AS country, W.MODE AS mode, L.LOC_DESC AS location FROM MESC1TS_AUDIT_HISTORY A " +
	"LEFT JOIN MESC1TS_SHOP S " + 
	"ON A.SHOP_CD = S.SHOP_CD " +
	"LEFT JOIN MESC1TS_COUNTRY C " +
	"ON SUBSTRING(S.LOC_CD, 1, 2) = C.COUNTRY_CD " +
	"LEFT JOIN MESC1TS_LOCATION L " +
	"ON S.LOC_CD = L.LOC_CD " +
	"LEFT JOIN MESC1TS_WO W " +
	"ON A.WO_ID = W.WO_ID " +
	"LEFT JOIN MESC1TS_STATUS_CODE SC " +
	"ON SC.STATUS_CODE = W.STATUS_CODE " +
	"LEFT JOIN MESC1TS_AUDITRESULT_CODE RC " +
	"ON RC.AUDIT_RESULT = A.AUDIT_RESULT " +
	"WHERE AUDITOR = ? " +
	"AND VISIT_ID = ?"
);

var setWorkOrderToAvailableQuery = WL.Server.createSQLStatement(
	"UPDATE MESC1TS_AUDIT_DETAILS " +
	"SET " +
	"AUDIT_STATUS = 2, " +
	"VISIT_ID = null, " +
	"AUDITOR = null " +
	"WHERE " +
	"WO_ID = ? AND " +
	"AUDITOR = ?"
);

var cancelAuditQuery = WL.Server.createSQLStatement(
	"UPDATE MESC1TS_AUDIT_DETAILS " +
	"SET " +
	"AUDIT_STATUS = 2, " +
	"VISIT_ID = null, " +
	"AUDITOR = null " +
	"WHERE " +
	"VISIT_ID = ? AND " +
	"AUDITOR = ? " +
	"AND AUDIT_STATUS <> 2"
);

var rescheduleVisitQuery = WL.Server.createSQLStatement(
	"UPDATE MESC1TS_AUDIT_DETAILS " +
	"SET " +
	"AUDIT_STATUS = 0, " +
	"AUDIT_DATE = ? " +
	"WHERE " +
	"VISIT_ID = ? AND " +
	"AUDITOR = ? AND " +
	"(AUDIT_STATUS = 0 OR AUDIT_STATUS = 3)"
);

var startVisitQuery = WL.Server.createSQLStatement(
	"BEGIN " +
	"DECLARE @auditor NVARCHAR(50) SET @auditor = ? " +
	"DECLARE @visitId VARCHAR(50) SET @visitId = ? " +
	"DECLARE @visitDate DATETIME SET @visitDate = ? " +
	"END " +
	"IF NOT EXISTS(SELECT 1 FROM MESC1TS_AUDIT_DETAILS WHERE AUDITOR = @auditor AND AUDIT_STATUS = 1) " +
	"UPDATE MESC1TS_AUDIT_DETAILS " +
	"SET " +
	"AUDIT_STATUS = 1, " +
	"AUDIT_DATE = @visitDate " +
	"WHERE " +
	"VISIT_ID = @visitId AND " +
	"AUDITOR = @auditor AND " +
	"AUDIT_STATUS = 0"
);

var getWorkOrderPartsQuery = WL.Server.createSQLStatement( // to remove
	"BEGIN " +
	"DECLARE @workOrderId VARCHAR(50) SET @workOrderId = ? " +
	"END " +
	"SELECT P.PART_CD AS partCode, M.PART_DESC AS partDescription, CONVERT(varchar, P.QTY_PARTS) AS quantity, " +
	"RTRIM(LTRIM(IsNull(R.DAMAGE_CD, ''))) AS damageCode, RTRIM(LTRIM(P.REPAIR_CD)) AS repairCode, RTRIM(LTRIM(IsNull(R.REPAIR_LOC_CD, ''))) AS repairLocCode, " +
	"R.ACTUAL_MANH AS manHrs, RTRIM(LTRIM(IsNull(R.TPI_CD, ''))) AS TPI, M.CORE_PART_SW AS corePartSW, P.COST_CPH AS partCost FROM " +
	"((SELECT * FROM MESC1TS_WOPART " +
	"WHERE WO_ID = @workOrderId) P " +
	"LEFT JOIN " +
	"(SELECT * FROM MESC1TS_WOREPAIR " +
	"WHERE WO_ID = @workOrderId) R " +
	"ON P.REPAIR_CD = R.REPAIR_CD " +
	"LEFT JOIN MESC1TS_MASTER_PART M " +
	"ON P.PART_CD = M.PART_CD)"
);

var getWorkOrderPartsQueryImproved = WL.Server.createSQLStatement(
	"BEGIN " +
	"DECLARE @workOrderId VARCHAR(50) SET @workOrderId = ? " +
	"END " +
	"SELECT R.WO_ID as workOrderId, RTRIM(LTRIM(R.REPAIR_CD)) as repairCode, " +
	"RTRIM(LTRIM(IsNull(R.DAMAGE_CD, ''))) as damageCode, RTRIM(LTRIM(IsNull(R.REPAIR_LOC_CD, ''))) as repairLocCode, " + 
	"RTRIM(LTRIM(IsNull(R.TPI_CD, ''))) as TPI, R.MODE as mode, C.REPAIR_DESC as repairDescription, '' as partCode, " +
	"'' as partDescription, R.QTY_REPAIRS as quantity, " +
	"null as partCost, R.SHOP_MATERIAL_AMT as materialCost, R.ACTUAL_MANH as manHrs,  R.QTY_REPAIRS * R.SHOP_MATERIAL_AMT as totalPerCode " +
	"FROM MESC1TS_WOREPAIR R LEFT JOIN " +
	"MESC1TS_REPAIR_CODE C ON R.REPAIR_CD = C.REPAIR_CD AND R.MODE = C.MODE AND R.MANUAL_CD = C.MANUAL_CD " +
	"WHERE R.WO_ID = @workOrderId UNION ALL " +
	"SELECT P.WO_ID as workOrderId, RTRIM(LTRIM(P.REPAIR_CD)) as repairCode, null as damageCode, null as repairLocCode, " +
	"null as TPI, null as mode, null as repairDescription, P.PART_CD as partCode, " +
	"M.PART_DESC as partDescription, QTY_PARTS as quantity, " +
	"COST_CPH as partCost, null as materialCost, null as manHrs, null as totalPerCode  " +
	"FROM MESC1TS_WOPART P LEFT JOIN " +
	"MESC1TS_MASTER_PART M ON P.PART_CD = M.PART_CD " +
	"WHERE P.WO_ID = @workOrderId"
);

var getFinishedWorkOrderPartsQuery = WL.Server.createSQLStatement(
	"BEGIN " +
	"DECLARE @auditor VARCHAR(50) SET @auditor = ? " +
	"DECLARE @workOrderId VARCHAR(50) SET @workOrderId = ? " +
	"DECLARE @visitId VARCHAR(50) SET @visitId = ? " +
	"END " +
	"SELECT P.AUDIT_RESULT AS auditResult, P.PRE_EQPNO AS prePartCode, P.POST_EQPNO AS postPartCode, isNull(M.PART_DESC, '') AS prePartDescription, " +
	"isNull(M2.PART_DESC, '') AS postPartDescription, P.PRE_QTY_PARTS AS preParts, " +
	"P.POST_QTY_PARTS AS postParts, P.PRE_REPAIR_CD AS preRepairCode, P.POST_REPAIR_CD AS postRepairCode, IsNull(P.PRE_DAMAGE_CD, '') AS preDamageCode, " +
	"IsNull(P.POST_DAMAGE_CD, '') AS postDamageCode, LTRIM(IsNull(P.PRE_REPAIR_LOC_CD, '')) AS preRepairLocCode, " +
	"LTRIM(IsNull(P.POST_REPAIR_LOC_CD, '')) AS postRepairLocCode, " +
	"RTRIM(LTRIM(isNull(R.ACTUAL_MANH, ''))) AS manHrs, LTRIM(IsNull(R.TPI_CD, '')) AS TPI, M.CORE_PART_SW AS corePartSW FROM " +
	"((SELECT * FROM MESC1TS_AUDIT_RESULTS " +
	"WHERE WO_ID = @workOrderId AND AUDITOR = @auditor AND VISIT_ID = @visitId) P " +
	"LEFT JOIN " +
	"(SELECT * FROM MESC1TS_WOREPAIR " +
	"WHERE WO_ID = @workOrderId) R " +
	"ON P.PRE_REPAIR_CD = R.REPAIR_CD AND P.PRE_REPAIR_LOC_CD = R.REPAIR_LOC_CD " +
	"LEFT JOIN MESC1TS_MASTER_PART M " +
	"ON P.PRE_EQPNO = M.PART_CD " +
	"LEFT JOIN MESC1TS_MASTER_PART M2 " +
	"ON P.POST_EQPNO = M2.PART_CD)"
);

var getFinishedVisitPartsQuery = WL.Server.createSQLStatement(
	"BEGIN " + 
	"DECLARE @auditor VARCHAR(50) SET @auditor = ? " + 
	"DECLARE @visitId VARCHAR(50) SET @visitId = ? " + 
	"END " + 
	"SELECT R.AUDIT_RESULT AS accepted, R.VISIT_ID AS visitId, R.WO_ID AS workOrderId, R.WO_REPAIR AS woRepair, " + 
	"R.REPAIR_INDEX AS repairIndex, R.PART_INDEX AS partIndex, R.PRE_EQPNO AS prePartCode, R.POST_EQPNO AS postPartCode, " + 
	"R.PRE_QTY_PARTS AS preParts, R.POST_QTY_PARTS AS postParts, " + 
	"R.PRE_REPAIR_CD AS preRepairCode, R.POST_REPAIR_CD AS postRepairCode, R.PRE_REPAIR_LOC_CD AS preRepairLocCode, " + 
	"R.POST_REPAIR_LOC_CD AS postRepairLocCode, R.PRE_DAMAGE_CD AS preDamageCode, R.POST_DAMAGE_CD AS postDamageCode, " + 
	"R.PRE_TPI AS preTPI, R.POST_TPI AS postTPI, R.REPAIR_DESC AS preRepairDescription, RC.REPAIR_DESC AS postRepairDescription, " + 
	"R.PART_DESC AS partDescription, '' AS postPartDescription, " +  
	"R.MAN_HRS AS manHrs, R.MATERIAL_COST AS materialCost, R.TOTAL_PER_CODE AS totalPerCode, R.PART_COST AS partCost " + 
	"FROM MESC1TS_AUDIT_RESULTS R " +  
	"LEFT JOIN MESC1TS_WO W ON R.WO_ID = W.WO_ID " + 
	"LEFT JOIN MESC1TS_REPAIR_CODE RC " + 
	"ON R.POST_REPAIR_CD = RC.REPAIR_CD AND W.MODE = RC.MODE AND W.MANUAL_CD = RC.MANUAL_CD " + 
	"WHERE R.VISIT_ID = @visitId AND R.AUDITOR = @auditor AND R.WO_REPAIR = 1 " + 
	"UNION ALL " + 
	"SELECT R.AUDIT_RESULT AS auditResult, R.VISIT_ID AS visitId, R.WO_ID AS workOrderId, R.WO_REPAIR AS woRepair, " + 
	"R.REPAIR_INDEX AS repairIndex, R.PART_INDEX AS partIndex, R.PRE_EQPNO AS prePartCode, R.POST_EQPNO AS postPartCode, " + 
	"R.PRE_QTY_PARTS AS preParts, R.POST_QTY_PARTS AS postParts, " + 
	"R.PRE_REPAIR_CD AS preRepairCode, R.POST_REPAIR_CD AS postRepairCode, R.PRE_REPAIR_LOC_CD AS preRepairLocCode, " + 
	"R.POST_REPAIR_LOC_CD AS postRepairLocCode, R.PRE_DAMAGE_CD AS preDamageCode, R.POST_DAMAGE_CD AS postDamageCode, " + 
	"R.PRE_TPI AS preTPI, R.POST_TPI AS postTPI, R.REPAIR_DESC AS preRepairDescription, '' AS postRepairDescription, " + 
	"R.PART_DESC AS partDescription, MP.PART_DESC AS postPartDescription, " +
	"R.MAN_HRS AS manHrs, R.MATERIAL_COST AS materialCost, R.TOTAL_PER_CODE AS totalPerCode, R.PART_COST AS partCost " +  
	"FROM MESC1TS_AUDIT_RESULTS R " +
	"LEFT JOIN MESC1TS_MASTER_PART MP " + 
	"ON MP.PART_CD = R.POST_EQPNO " +
	"WHERE R.VISIT_ID = @visitId AND R.AUDITOR = @auditor AND R.WO_REPAIR = 0"
);

var getFinishedVisitPartsPostRepairQuery = WL.Server.createSQLStatement(
	"BEGIN " + 
	"DECLARE @auditor VARCHAR(50) SET @auditor = ? " + 
	"DECLARE @visitId VARCHAR(50) SET @visitId = ? " + 
	"END " + 
	"SELECT R.AUDIT_RESULT AS accepted, R.VISIT_ID AS visitId, R.WO_ID AS workOrderId, R.WO_REPAIR AS woRepair, " + 
	"R.REPAIR_INDEX AS repairIndex, R.PART_INDEX AS partIndex, R.PRE_EQPNO AS prePartCode, R.POST_EQPNO AS postPartCode, " + 
	"R.PRE_QTY_PARTS AS preParts, R.POST_QTY_PARTS AS postParts, " + 
	"R.PRE_REPAIR_CD AS preRepairCode, R.POST_REPAIR_CD AS postRepairCode, R.PRE_REPAIR_LOC_CD AS preRepairLocCode, " + 
	"R.POST_REPAIR_LOC_CD AS postRepairLocCode, R.PRE_DAMAGE_CD AS preDamageCode, R.POST_DAMAGE_CD AS postDamageCode, " + 
	"R.PRE_TPI AS preTPI, R.POST_TPI AS postTPI, R.REPAIR_DESC AS preRepairDescription, RC.REPAIR_DESC AS postRepairDescription, " + 
	"R.PART_DESC AS partDescription, '' AS postPartDescription, R.REPAIR_QUALITY AS repairQuality, " +  
	"R.MAN_HRS AS manHrs, R.MATERIAL_COST AS materialCost, R.TOTAL_PER_CODE AS totalPerCode, R.PART_COST AS partCost " + 
	"FROM MESC1TS_AUDIT_RESULTS R " +  
	"LEFT JOIN MESC1TS_WO W ON R.WO_ID = W.WO_ID " + 
	"LEFT JOIN MESC1TS_REPAIR_CODE RC " + 
	"ON R.POST_REPAIR_CD = RC.REPAIR_CD AND W.MODE = RC.MODE AND W.MANUAL_CD = RC.MANUAL_CD " + 
	"WHERE R.VISIT_ID = @visitId AND R.AUDITOR = @auditor AND R.WO_REPAIR = 1 " + 
	"UNION ALL " + 
	"SELECT R.AUDIT_RESULT AS auditResult, R.VISIT_ID AS visitId, R.WO_ID AS workOrderId, R.WO_REPAIR AS woRepair, " + 
	"R.REPAIR_INDEX AS repairIndex, R.PART_INDEX AS partIndex, R.PRE_EQPNO AS prePartCode, R.POST_EQPNO AS postPartCode, " + 
	"R.PRE_QTY_PARTS AS preParts, R.POST_QTY_PARTS AS postParts, " + 
	"R.PRE_REPAIR_CD AS preRepairCode, R.POST_REPAIR_CD AS postRepairCode, R.PRE_REPAIR_LOC_CD AS preRepairLocCode, " + 
	"R.POST_REPAIR_LOC_CD AS postRepairLocCode, R.PRE_DAMAGE_CD AS preDamageCode, R.POST_DAMAGE_CD AS postDamageCode, " + 
	"R.PRE_TPI AS preTPI, R.POST_TPI AS postTPI, R.REPAIR_DESC AS preRepairDescription, '' AS postRepairDescription, " + 
	"R.PART_DESC AS partDescription, MP.PART_DESC AS postPartDescription, R.REPAIR_QUALITY AS repairQuality, " +
	"R.MAN_HRS AS manHrs, R.MATERIAL_COST AS materialCost, R.TOTAL_PER_CODE AS totalPerCode, R.PART_COST AS partCost " +  
	"FROM MESC1TS_AUDIT_RESULTS R " +
	"LEFT JOIN MESC1TS_MASTER_PART MP " + 
	"ON MP.PART_CD = R.POST_EQPNO " +
	"WHERE R.VISIT_ID = @visitId AND R.AUDITOR = @auditor AND R.WO_REPAIR = 0"
);

var saveFinishedWorkOrderQuery = WL.Server.createSQLStatement(
	"BEGIN " +
	"DECLARE @auditor VARCHAR(50) SET @auditor = ? " +
	"DECLARE @auditDate DATETIME SET @auditDate = ? " +
	"DECLARE @visitId VARCHAR(50) SET @visitId = ? " +
	"DECLARE @workOrderNo INT SET @workOrderNo = ? " + 
	"DECLARE @shopCode VARCHAR(50) SET @shopCode = ? " +
	"DECLARE @auditResult INT SET @auditResult = ? " +
	"DECLARE @visitDescription VARCHAR(50) SET @visitDescription = ? " +
	"END " +
	"INSERT INTO MESC1TS_AUDIT_HISTORY " +
	"(WO_ID, " +
	"SHOP_CD, " +
	"AUDIT_DATE, " +
	"AUDITOR, " +
	"AUDIT_RESULT, " +
	"VISIT_DESC, " +
	"VISIT_ID) " +
	"VALUES(@workOrderNo, " +
	"@shopCode, " +
	"@auditDate, " +
	"@auditor, " +
	"@auditResult, " +
	"@visitDescription, " +
	"@visitId)"
);

var saveFinishedWorkOrderPartQuery = WL.Server.createSQLStatement(
	"BEGIN " +
	"DECLARE @auditor VARCHAR(50) SET @auditor = ? " +
	"DECLARE @auditDate DATETIME SET @auditDate = ? " +
	"DECLARE @visitId VARCHAR(50) SET @visitId = ? " +
	"DECLARE @workOrderNo INT SET @workOrderNo = ? " +
	"DECLARE @shopCode VARCHAR(50) SET @shopCode = ? " +
	"DECLARE @auditResult INT SET @auditResult = ? " +
	"DECLARE @preEqpNo VARCHAR(50) SET @preEqpNo = ? " +
	"DECLARE @postEqpNo VARCHAR(50) SET @postEqpNo = ? " +
	"DECLARE @preQtyParts VARCHAR(50) SET @preQtyParts = ? " +
	"DECLARE @postQtyParts VARCHAR(50) SET @postQtyParts = ? " +
	"DECLARE @preRepairCode VARCHAR(50) SET @preRepairCode = ? " +
	"DECLARE @postRepairCode VARCHAR(50) SET @postRepairCode = ? " +
	"DECLARE @preRepairLocCode VARCHAR(50) SET @preRepairLocCode = ? " +
	"DECLARE @postRepairLocCode VARCHAR(50) SET @postRepairLocCode = ? " +
	"DECLARE @preDamageCode VARCHAR(50) SET @preDamageCode = ? " +
	"DECLARE @postDamageCode VARCHAR(50) SET @postDamageCode = ? " +
	"END " +
	"INSERT INTO MESC1TS_AUDIT_RESULTS " +
	"(WO_ID, " +
	"SHOP_CD, " +
	"AUDIT_DATE, " +
	"AUDITOR, " +
	"AUDIT_RESULT, " +
	"VISIT_ID, " +
	"PRE_EQPNO, " +
	"POST_EQPNO, " +
	"PRE_QTY_PARTS, " +
	"POST_QTY_PARTS, " +
	"PRE_REPAIR_CD, " +
	"POST_REPAIR_CD, " +
	"PRE_REPAIR_LOC_CD, " +
	"POST_REPAIR_LOC_CD, " +
	"PRE_DAMAGE_CD, " +
	"POST_DAMAGE_CD) " +
	"VALUES(@workOrderNo, " +
	"@shopCode, " +
	"@auditDate, " +
	"@auditor, " +
	"@auditResult, " +
	"@visitId, " +
	"@preEqpNo, " +
	"@postEqpNo, " +
	"@preQtyParts, " +
	"@postQtyParts, " +
	"@preRepairCode, " +
	"@postRepairCode, " +
	"@preRepairLocCode, " +
	"@postRepairLocCode, " +
	"@preDamageCode, " +
	"@postDamageCode)"	
);

var savePhotosQuery = WL.Server.createSQLStatement(
	"INSERT INTO MESC1TS_AUDIT_PHOTOS "+
	"(PATH,NAME,DATE,VISIT_ID,WO_ID,AUDITOR,EXTRA_INFO) "+
	"VALUES(?,?,?,?,?,?,?)"	
);

var getPlanVisitStatusQuery = WL.Server.createSQLStatement(
	"SELECT STATUS_DSC AS status FROM MESC1TS_STATUS_CODE WHERE STATUS_CODE >= 100 AND STATUS_CODE <= 390"
);

var getAllStatusCodesQuery = WL.Server.createSQLStatement(
	"SELECT STATUS_DSC AS status FROM MESC1TS_STATUS_CODE WHERE STATUS_CODE > 100 AND STATUS_CODE <= 900"
);

var getPlanVisitModeQuery = WL.Server.createSQLStatement(
		"SELECT MODE AS mode FROM MESC1TS_MODE"
	);

var saveFinishedWorkOrderQueryBeta = WL.Server.createSQLStatement(
		"BEGIN " +
		"DECLARE @auditor VARCHAR(50) SET @auditor = ? " +
		"DECLARE @auditDate DATETIME SET @auditDate = ? " +
		"DECLARE @visitId VARCHAR(50) SET @visitId = ? " +
		"DECLARE @archiveId VARCHAR(50) SET @archiveId = ? " +
		"DECLARE @workOrderNo INT SET @workOrderNo = ? " + 
		"DECLARE @shopCode VARCHAR(50) SET @shopCode = ? " +
		"DECLARE @auditResult INT SET @auditResult = ? " +
		"DECLARE @visitDescription VARCHAR(50) SET @visitDescription = ? " +
		"END " +
		"INSERT INTO MESC1TS_AUDIT_HISTORY " +
		"(WO_ID, " +
		"SHOP_CD, " +
		"AUDIT_DATE, " +
		"AUDITOR, " +
		"AUDIT_RESULT, " +
		"VISIT_DESC, " +
		"VISIT_ID, ARCHIVE_ID ) " +
		"VALUES(@workOrderNo, " +
		"@shopCode, " +
		"@auditDate, " +
		"@auditor, " +
		"@auditResult, " +
		"@visitDescription, " +
		"@visitId, @archiveId)"
	);

var getArchivePathQuery = WL.Server.createSQLStatement(
	"SELECT DISTINCT ARCHIVE_ID " +
	"FROM MESC1TS_AUDIT_HISTORY " +
	"WHERE VISIT_ID=? AND AUDITOR= ?"
);

var getPhotosQuery = WL.Server.createSQLStatement(
	"SELECT PATH AS path, NAME AS name, DATE AS date, VISIT_ID AS visitId, WO_ID AS wono "+
	"FROM MESC1TS_AUDIT_PHOTOS "+
	"WHERE AUDITOR=? AND VISIT_ID=?"
);


function onRequestSuccess(data, context) {
	return {
		isSuccessful : true,
		status : 200,
		errorCode : 0,
		errorMsg : null,
		invocationContext : context,
		invocationResult : data
	}
}

function onRequestFailure(errorMsg, errorCode, data, context) {
	return {
		isSuccessful : false,
		status : 200,
		errorCode : errorCode,
		errorMsg : errorMsg,
		invocationContext : context,
		data : data
	}
}

function invokeSQL(statement, parameters, context) {
	try {
		// Invoke SQL statement
		var returnData = WL.Server.invokeSQLStatement({
			preparedStatement : statement,
			parameters : parameters
		});
		
		// Return SQL statement data
		return returnData;

	} catch (error) {
		return onRequestFailure("Database SQL execution error thrown", 101, error, context);
	}
}

function updateContainer(username, token, shopCode, workOrderId, visitDate, auditStatus, context) {
	// Sanity check + Token validity
	if (checkUsernameAndToken(username, token, context).valid === false) {
		return checkUsernameAndToken(username, token, context).response;
	}
	
	// shopCode and workOrderId validity
	if (shopCode == undefined || shopCode.length == 0 || workOrderId == undefined || workOrderId.length == 0) {
		return onRequestFailure("shopCode, workOrderId query argument invalid (not provided or incorrect format)", 107, null, context);
	}
	
	if (auditStatus === undefined || auditStatus.length == 0) {
		return onRequestFailure("auditStatus query argument invalid (not provided or incorrect format)", 107, null, context);
	}
	
	var userId = username;
	
	if (visitDate === null) {
		// is visitDate is null, this means that any visits for the work order will be canceled
		userId = null;
		sqlVisitDate = null;
	} else {
		var sqlVisitDate = dateToSQLDateTime(new Date(visitDate));
		
		// fromDate, toDate validity	
		if (!sqlVisitDate) {
			return onRequestFailure("visitDate query argument invalid (not provided or incorrect format)", 107, data, context);
		}
		
		sqlVisitDate = sqlVisitDate.toString();
	}
	
	var data = invokeSQL(updateContainerProcedure, [shopCode, workOrderId, userId, sqlVisitDate, auditStatus], context);
	
	if (data.isSuccessful && data.updateStatementResult && data.updateStatementResult.updateCount == 1) {
		// update was successful
		return onRequestSuccess(data, context);
	}
	
	if (data.isSuccessful && data.updateStatementResult && data.updateStatementResult.updateCount != 1) {
		// update was unsuccessul, probably input data is wrong
		return onRequestFailure("No rows were updated", 110, data, context);
	}
	
	// return generic error
	return onRequestFailure("SQL error", 100, data, context);
}

function saveFinishedWorkOrderPart(username, token, workOrderId, visitDate, visitId, shopCode, auditResult, preEqpNo, postEqpNo, preQtyParts, postQtyParts, 
		preRepairCode, postRepairCode, preRepairLocCode, postRepairLocCode, preDamageCode, postDamageCode, context) {
	WL.Logger.info("[AuditAdapter][saveFinishedWorkOrderPart] username: " + username + ",workOrderId: " + workOrderId + "visitId: " + visitId);
	// Sanity check + Token validity
	if (checkUsernameAndToken(username, token, context).valid === false) {
		return checkUsernameAndToken(username, token, context).response;
	}
	
	// General validation
	if (!validArguments([workOrderId, visitId, shopCode, visitDate])) {
		return onRequestFailure("workOrderId, visitId or shopCode query argument invalid (not provided or incorrect format)", 107, null, context);
	}
	
	// validate auditStatus
	if (auditResult != 0 && auditResult != 1) {
		return onRequestFailure("auditStatus query argument invalid (not provided or incorrect format)", 107, null, context);
	}
	
	if (!stringArguments([preQtyParts, postQtyParts, preRepairCode, postRepairCode, preRepairLocCode, postRepairCode, preRepairLocCode, postRepairLocCode, preDamageCode, postDamageCode])) {
		return onRequestFailure("preQtyParts, postQtyParts, preRepairCode, postRepairCode, preRepairLocCode, postRepairCode, preRepairLocCode, postRepairLocCode, preDamageCode or postDamageCode query argument invalid (not provided or incorrect format)", 107, null, context);
	}
	
	var data = invokeSQL(saveFinishedWorkOrderPartQuery, [username, visitDate, visitId, workOrderId, shopCode, auditResult, preEqpNo,
	                                                      postEqpNo, preQtyParts, postQtyParts, preRepairCode, postRepairCode, preRepairLocCode, 
	                                                      postRepairLocCode, preDamageCode, postDamageCode, context]);
	
	if (data.isSuccessful && data.updateStatementResult && data.updateStatementResult.updateCount == 1) {
		// update was successful
		return onRequestSuccess(data, context);
	}
	
	// return generic error
	return onRequestFailure("SQL error", 100, data, context);
}

function addWorkOrdersToVisit(username, token, visitId, visitDate, currentShop, description, auditStatus, workOrderArray, context) {
	WL.Logger.info("[AuditAdapter][addWorkOrdersToVisit] username: " + username + ", visitId: " + visitId + ", currentShop: " + currentShop);
	// Sanity check + Token validity
	if (checkUsernameAndToken(username, token, context).valid === false) {
		return checkUsernameAndToken(username, token, context).response;
	}
	
	var lockedWorkOrders = [];
	var plannedWorkOrders = [];
	
	try {
		var response = invokeSQL(beginTransaction, []); 
		
		for (var i = 0; i < workOrderArray.length; i++) {
			var workOrder = workOrderArray[i];
			
			response = markWorkOrderForAudit(username, workOrder.workOrderId, visitDate, visitId, description, currentShop, auditStatus, context);
			
			if (response && response.isSuccessful) {
				if (response.invocationResult && response.invocationResult.updateStatementResult) {
					if (response.invocationResult.updateStatementResult.updateCount == 0) { // insert was unsuccessful
						 lockedWorkOrders.push(workOrder.workOrderId); // the work order was already booked
					} else if (response.invocationResult.updateStatementResult.updateCount > 0) {
						response = getWorkOrderInfo(username, workOrder.workOrderId);
						
						if (response && response.invocationResult && response.isSuccessful) {
							if (response.invocationResult.length > 0) {
								plannedWorkOrders.push(response.invocationResult[0]);
							}
						} else {
							invokeSQL(rollbackTransaction, []); 
							return onRequestFailure("SQL error", 100, response, context);
						}
					}
				}
			} else {
				invokeSQL(rollbackTransaction, []); 
				return onRequestFailure("SQL error", 100, response, context);
			}
		}
		
		response = invokeSQL(commitTransaction, []);
		
		return onRequestSuccess("OK", {lockedWorkOrders : lockedWorkOrders, plannedWorkOrders : plannedWorkOrders, visitId : visitId});
	} catch(e) {
		invokeSQL(rollbackTransaction, []); 
		return onRequestFailure("SQL error", 100, null, context);
	}
	
	
}

function planVisit(username, token, visitDate, currentShop, description, workOrderArray, context) {
	return planVisitImproved(username, token, visitDate, currentShop, description, workOrderArray, context);
}

function planVisitImproved(username, token, visitDate, currentShop, description, workOrderArray, context) {
	WL.Logger.info("[AuditAdapter][planVisitImproved] username: " + username + ", visitId: " + visitId + ", currentShop: " + currentShop);
	// Sanity check + Token validity
	if (checkUsernameAndToken(username, token, context).valid === false) {
		return checkUsernameAndToken(username, token, context).response;
	}
	
	var auditStatus = 0; // planned
	var visitId;
	
	var response = getVisitId(username, token);
	
	if (response && response.isSuccessful) {
		if (response.invocationResult) {
			visitId = response.invocationResult.visitId;
		}
	} else {
		return onRequestFailure("SQL error", 100, response, context);
	}
	
	var lockedWorkOrders = [];
	var plannedWorkOrders = [];  // for saving in json store without making a new request
	
	try {
		var response = invokeSQL(beginTransaction, []); 
		
		for (var i = 0; i < workOrderArray.length; i++) {
			var workOrder = workOrderArray[i];
			
			response = markWorkOrderForAudit(username, workOrder.workOrderId, visitDate, visitId, description, currentShop, auditStatus, context);
			
			if (response && response.isSuccessful) {
				if (response.invocationResult && response.invocationResult.updateStatementResult) {
					if (response.invocationResult.updateStatementResult.updateCount == 0) { // insert was unsuccessful
						 lockedWorkOrders.push(workOrder.workOrderId); // the work order was already booked
					} else if (response.invocationResult.updateStatementResult.updateCount > 0) {
						// insert was successful, return 
						// need user and work order id only
						response = getWorkOrderInfo(username, workOrder.workOrderId);
						
						if (response && response.invocationResult && response.isSuccessful) {
							if (response.invocationResult.length > 0) {
								plannedWorkOrders.push(response.invocationResult[0]);
							}
						} else {
							invokeSQL(rollbackTransaction, []); 
							return onRequestFailure("SQL error", 100, response, context);
						}
					}
				}
			} else {
				invokeSQL(rollbackTransaction, []); 
				return onRequestFailure("SQL error", 100, response, context);
			}
		}
		
		response = invokeSQL(commitTransaction, []);
		
		return onRequestSuccess("OK", {lockedWorkOrders : lockedWorkOrders, plannedWorkOrders : plannedWorkOrders, visitId : visitId});
	} catch(e) {
		invokeSQL(rollbackTransaction, []); 
		return onRequestFailure("SQL error", 100, null, context);
	}
}


function getWOStatusList (username, token, context){
	// Sanity check + Token validity
	if (checkUsernameAndToken(username, token, context).valid === false) {
		return checkUsernameAndToken(username, token, context).response;
	}
	var data = invokeSQL(getPlanVisitStatusQuery, context);
	
	if (data.isSuccessful) {
		return onRequestSuccess(data, context); 
	}
	
	return onRequestFailure("SQL error", 100, data, context);
}

function getAllWOStatusList(username, token, context) {
	// Sanity check + Token validity
	if (checkUsernameAndToken(username, token, context).valid === false) {
		return checkUsernameAndToken(username, token, context).response;
	}
	var data = invokeSQL(getAllStatusCodesQuery, context);
	
	if (data.isSuccessful) {
		return onRequestSuccess(data, context); 
	}
	
	return onRequestFailure("SQL error", 100, data, context);
}

function getWOModeList (username, token, context){
	// Sanity check + Token validity
	if (checkUsernameAndToken(username, token, context).valid === false) {
		return checkUsernameAndToken(username, token, context).response;
	}
	var data = invokeSQL(getPlanVisitModeQuery, context);
	
	if (data.isSuccessful) {
		return onRequestSuccess(data, context); 
	}
	
	return onRequestFailure("SQL error", 100, data, context);
}

function getWorkOrderInfo(username, workOrderId, context) {
	var data = invokeSQL(getWorkOrderInfoQuery, [username, workOrderId], context);
	
	if (data.isSuccessful && data.resultSet) {
		return onRequestSuccess(data.resultSet, context); 
	}
	
	return onRequestFailure("SQL error", 100, data, context);
}

function markWorkOrderForAudit(username, workOrderId, visitDate, visitId, visitDesc, shopCode, auditStatus, context) {
	// General validation
	if (!validArguments([workOrderId, visitId, shopCode, visitDate])) {
		return onRequestFailure("workOrderId, visitId or shopCode query argument invalid (not provided or incorrect format)", 107, null, context);
	}
	
	// validate auditStatus
	if (auditStatus != 0 && auditStatus != 1) {
		return onRequestFailure("auditStatus query argument invalid (not provided or incorrect format)", 107, null, context);
	}
	
	if (typeof visitDesc !== 'string') {
		return onRequestFailure("visitDesc query argument invalid (not provided or incorrect format)", 107, null, context);
	}
	
	var data = invokeSQL(markWorkOrderForAuditQuery, [username, visitDate, visitId, workOrderId, shopCode, auditStatus, visitDesc], context);
	
	if (data.isSuccessful) {
		// update was successful
		return onRequestSuccess(data, context);
	}
	
	// return generic error
	return onRequestFailure("SQL error", 100, data, context);
}

function getVisitId(username, token, context) {
	if (checkUsernameAndToken(username, token, context).valid === false) {
		return checkUsernameAndToken(username, token, context).response;
	}
	
	var data = invokeSQL(getVisitIdQuery, [], context);
	
	if (data.isSuccessful && data.resultSet.length > 0) {
		var unixTimestamp = (+new Date(data.resultSet[0].CurrentDateTime)).toString();
		return onRequestSuccess({visitId: unixTimestamp}, context);
	}
	
	return onRequestFailure("SQL error", 100, data, context);
}

function checkIfVisitFinished(username, token, visitId, context) {
	if (checkUsernameAndToken(username, token, context).valid === false) {
		return checkUsernameAndToken(username, token, context).response;
	}
	
	var data = invokeSQL(getWorkOrderCountForFinishedVisitQuery, [visitId, username], context);
	
	if (data.isSuccessful && data.resultSet.length > 0) {
		var workOrderCount = data.resultSet[0].count
		var isFinished = (workOrderCount > 0) ? true : false;
		return onRequestSuccess({isFinished: isFinished}, context);
	}
	
	return onRequestFailure("SQL error", 100, data, context);
}

function addAuditedContainersLinesToVisit() {
	// TODO
}

// savePhotosQuery   
function savePhotos(username, token, path, name, date, visitId, woId, auditor, extra, context) {
	if (checkUsernameAndToken(username, token, context).valid === false) {
		return checkUsernameAndToken(username, token, context).response;
	}
	
	if (visitId === undefined || visitId.length == 0) {
		return onRequestFailure("visitId query argument invalid (not provided or incorrect format)", 107, null, context);
	}
	
	var data = invokeSQL(savePhotosQuery, [path, name, date,, visitId, woId, auditor, extra], context);
	
	if (data.isSuccessful) {
		return onRequestSuccess(data, context);
	}
	
	// return generic error
	return onRequestFailure("SQL error", 100, data, context);
}

function getCalendarData(username, token, context) {
	if (checkUsernameAndToken(username, token, context).valid === false) {
		return checkUsernameAndToken(username, token, context).response;
	}
	
	var data = invokeSQL(getCalendarDataQuery, [username], context);
	
	if (data.isSuccessful && data.resultSet) {
		return onRequestSuccess(data.resultSet, context);
	}
	
	return onRequestFailure("SQL error", 100, data, context);
}

function startAudit(username, token, visitId, visitDate, context) {
	if (checkUsernameAndToken(username, token, context).valid === false) {
		return checkUsernameAndToken(username, token, context).response;
	}
	
	if (visitId === undefined || visitId.length == 0) {
		return onRequestFailure("visitId query argument invalid (not provided or incorrect format)", 107, null, context);
	}
	
	if (!visitDate) {
		return onRequestFailure("visitDate query argument invalid (not provided or incorrect format)", 107, data, context);
	}
	
	var data = invokeSQL(startVisitQuery, [username, visitId, visitDate], context);
	
	if (data.isSuccessful) {
		return onRequestSuccess(data, context);
	}
	
	// return generic error
	return onRequestFailure("SQL error", 100, data, context);
}

function cancelAudit(username, token, visitId, context) {
	if (checkUsernameAndToken(username, token, context).valid === false) {
		return checkUsernameAndToken(username, token, context).response;
	}
	
	if (visitId === undefined || visitId.length == 0) {
		return onRequestFailure("visitId query argument invalid (not provided or incorrect format)", 107, null, context);
	}
	
	var data = invokeSQL(cancelAuditQuery, [visitId, username], context);
	
	if (data.isSuccessful) {
		// update was successful
		return onRequestSuccess(data, context);
	}
	
	// return generic error
	return onRequestFailure("SQL error", 100, data, context);
}

function rescheduleAudit(username, token, visitId, visitDate, context) {
	if (checkUsernameAndToken(username, token, context).valid === false) {
		return checkUsernameAndToken(username, token, context).response;
	}
	
	if (visitId === undefined || visitId.length == 0) {
		return onRequestFailure("visitId query argument invalid (not provided or incorrect format)", 107, null, context);
	}
	
	if (!visitDate) {
		return onRequestFailure("visitDate query argument invalid (not provided or incorrect format)", 107, data, context);
	}
	
	var data = invokeSQL(rescheduleVisitQuery, [visitDate, visitId, username], context);
	
	if (data.isSuccessful) {
		return onRequestSuccess(data, context);
	}
	
	// return generic error
	return onRequestFailure("SQL error", 100, data, context);
}

function setWorkOrderToAvailable(username, token, workOrderId, context) {
	
	if (checkUsernameAndToken(username, token, context).valid === false) {
		return checkUsernameAndToken(username, token, context).response;
	}
	
	if (workOrderId === undefined || workOrderId.length == 0) {
		return onRequestFailure("workOrderId query argument invalid (not provided or incorrect format)", 107, null, context);
	}
	
	var data = invokeSQL(setWorkOrderToAvailableQuery, [workOrderId, username], context);
	
	if (data.isSuccessful) {
		// update was successful
		return onRequestSuccess(data, context);
	}
	
	// return generic error
	return onRequestFailure("SQL error", 100, data, context);
}

function updateMissedWorkOrders(username, token, date, context) {
	if (checkUsernameAndToken(username, token, context).valid === false) {
		return checkUsernameAndToken(username, token, context).response;
	}
	
	if (date === undefined || date.length == 0) {
		return onRequestFailure("date query argument invalid (not provided or incorrect format)", 107, null, context);
	}
	
	var data = invokeSQL(updateMissedWorkOrdersQuery, [username, date], context);
	
	if (data.isSuccessful) {
		// update was successful
		return onRequestSuccess(data, context);
	}
	
	// return generic error
	return onRequestFailure("SQL error", 100, data, context);
}

/*
 *  get photos for specific VISIT
 */
function getPhotos(username, token, visitId,context) {
	// Sanity check + Token validity
	if (checkUsernameAndToken(username, token, context).valid === false) {
		return checkUsernameAndToken(username, token, context).response;
	}
	
	var data = invokeSQL(getPhotosQuery, [username, visitId], context);
	
	if (data.isSuccessful && data.resultSet) {
		return onRequestSuccess(data.resultSet, context);
	}
	
	return onRequestFailure("SQL error - or unlikely, no results", 100, data, context);
}

function getVisits(username, token, context) {
	// Sanity check + Token validity
	if (checkUsernameAndToken(username, token, context).valid === false) {
		return checkUsernameAndToken(username, token, context).response;
	}
	
	var data = invokeSQL(getVisitsQuery, [username], context);
	
	if (data.isSuccessful && data.resultSet) {
		return onRequestSuccess(data.resultSet, context);
	}
	
	return onRequestFailure("SQL error - or unlikely, no results", 100, data, context);
}

function statusesForFinishedVisitsSync(username, token, multipleVisitIds, context) {
	var allVisitsTotal = 0;
	var allVisitsProcessed = 0;
	var allVisitsSuccessful = 0;
	var allVisitsFailed = 0;
	var visitCounter = 0
	
	if (!multipleVisitIds || multipleVisitIds.length == 0) {
		return onRequestFailure("multipleVisitIds query argument invalid - needs to be an array", 107, null, context);
	}
	
	for (index in multipleVisitIds) {
		visitCounter++;
		var eachVisitResult = getVisitProcessingStatus(username, token, multipleVisitIds[index], context);
		
		if (eachVisitResult.isSuccessful) {
			allVisitsTotal = allVisitsTotal + eachVisitResult.invocationResult.totalCount;
			allVisitsSuccessful = allVisitsSuccessful + eachVisitResult.invocationResult.successfulCount;
			allVisitsFailed = allVisitsFailed + eachVisitResult.invocationResult.failedCount;
			allVisitsProcessed = allVisitsProcessed + eachVisitResult.invocationResult.processedCount;
		} else {
			return onRequestFailure("SQL error", 100, eachVisitResult, context);
		}
	}
	
	return onRequestSuccess({totalCount:allVisitsTotal, processedCount:allVisitsProcessed, successfulCount:allVisitsSuccessful, failedCount:allVisitsFailed, visitCounter:visitCounter });
}



function getVisitProcessingStatus(username, token, visitId, context) {
	if (visitId === undefined || visitId.length == 0) {
		return onRequestFailure("visitId query argument invalid (not provided or incorrect format)", 107, null, context);
	}
	
	var _failed = "FAILED";
	var _successful = "SUCCESS"
	var totalCount;
	var successfulCount;
	var failedCount;
	var processedCount;
	
	
	
	var totalCountResponse = invokeSQL(getWorkOrdersCountQuery, [username, visitId], context);
	
	if (totalCountResponse.isSuccessful && totalCountResponse.resultSet) {
		totalCount = totalCountResponse.resultSet[0].count;
	} else {
		return onRequestFailure("SQL error", 100, totalCountResponse, context);
	}
	
	var successfulCountResponse = invokeSQL(getWorkOrdersCountWithStatusQuery, [username, visitId, _successful], context);
	
	if (successfulCountResponse.isSuccessful && successfulCountResponse.resultSet) {
		successfulCount = successfulCountResponse.resultSet[0].count;
	} else {
		return onRequestFailure("SQL error", 100, successfulCountResponse, context);
	}
	
	var failedCountResponse = invokeSQL(getWorkOrdersCountWithStatusQuery, [username, visitId, _failed], context);
	
	if (failedCountResponse.isSuccessful && failedCountResponse.resultSet) {
		failedCount = failedCountResponse.resultSet[0].count;
	} else {
		return onRequestFailure("SQL error", 100, failedCountResponse, context);
	}
	
	processedCount = successfulCount + failedCount;
	
	return onRequestSuccess({totalCount: totalCount, processedCount: processedCount, successfulCount: successfulCount, failedCount: failedCount}, context);
}


function getFinishedVisitWorkOrders(username, token, visitId, context) {
	// Sanity check + Token validity
	if (checkUsernameAndToken(username, token, context).valid === false) {
		return checkUsernameAndToken(username, token, context).response;
	}
	
	if (visitId === undefined || visitId.length == 0) {
		return onRequestFailure("visitId query argument invalid (not provided or incorrect format)", 107, null, context);
	}
	
	var data = invokeSQL(getFinishedVisitWorkOrdersQuery, [username, visitId], context);
	
	if (data.isSuccessful && data.resultSet) {
		return onRequestSuccess(data.resultSet, context);
	}
	
	return onRequestFailure("SQL error", 100, data, context);
}

function getFinishedVisitWorkOrdersPostRepair(username, token, visitId, context) {
	// Sanity check + Token validity
	if (checkUsernameAndToken(username, token, context).valid === false) {
		return checkUsernameAndToken(username, token, context).response;
	}
	
	if (visitId === undefined || visitId.length == 0) {
		return onRequestFailure("visitId query argument invalid (not provided or incorrect format)", 107, null, context);
	}
	
	var data = invokeSQL(getFinishedVisitWorkOrdersPostRepairQuery, [username, visitId], context);
	
	if (data.isSuccessful && data.resultSet) {
		return onRequestSuccess(data.resultSet, context);
	}
	
	return onRequestFailure("SQL error", 100, data, context);
}

function getMissedVisitWorkOrders(username, token, visitId, context) {
	// Sanity check + Token validity
	if (checkUsernameAndToken(username, token, context).valid === false) {
		return checkUsernameAndToken(username, token, context).response;
	}
	
	if (visitId === undefined || visitId.length == 0) {
		return onRequestFailure("visitId query argument invalid (not provided or incorrect format)", 107, null, context);
	}
	
	var data = invokeSQL(getMissedVisitWorkOrdersQuery, [username, visitId], context);
	
	if (data.isSuccessful && data.resultSet) {
		return onRequestSuccess(data.resultSet, context);
	}
	
	return onRequestFailure("SQL error", 100, data, context);
}

function getWorkOrderParts(username, token, workOrderId, context) {
	// Sanity check + Token validity
	if (checkUsernameAndToken(username, token, context).valid === false) {
		return checkUsernameAndToken(username, token, context).response;
	}
	
	if (workOrderId === undefined || workOrderId.length == 0) {
		return onRequestFailure("workOrderId query argument invalid (not provided or incorrect format)", 107, null, context);
	}
	
	var data = invokeSQL(getWorkOrderPartsQuery, [workOrderId], context);
	
	if (data.isSuccessful && data.resultSet) {
		return onRequestSuccess(data.resultSet, context);
	}
	
	return onRequestFailure("SQL error - or unlikely, no results", 100, data, context);
} 

function getWorkOrderPartsForWOArrray(username, token, workOrders, context) {
	// Sanity check + Token validity
	if (checkUsernameAndToken(username, token, context).valid === false) {
		return checkUsernameAndToken(username, token, context).response;
	}
	
	if (!workOrders || workOrders.length == 0) {
		return onRequestFailure("workOrders query argument invalid (not provided or incorrect format)", 107, null, context);
	}
	
	var woParts = [];
	
	for (var i = 0; i < workOrders.length; i++) {
		var workOrderId = workOrders[i].workOrderId;
		
		// Get repairs and parts for each work order
		var data = invokeSQL(getWorkOrderPartsQueryImproved, [workOrderId], context);
		
		if (data.isSuccessful && data.resultSet) {
			for (var j = 0; j < data.resultSet.length; j++) {
				var line = data.resultSet[j];
				
				if (line.repairDescription) {
					// This is a repair
					line.woRepair = true;
				} else {
					// This is a part
					line.woRepair = false;
				}
				
				woParts.push(line);
			}
		} else {
			return onRequestFailure("SQL error", 100, data, context);
		}
	}
	
	return onRequestSuccess(woParts, context);
}

function getWorkOrderPartsImproved(username, token, workOrderId, context) {
	// Sanity check + Token validity
	if (checkUsernameAndToken(username, token, context).valid === false) {
		return checkUsernameAndToken(username, token, context).response;
	}
	
	if (workOrderId === undefined || workOrderId.length == 0) {
		return onRequestFailure("workOrderId query argument invalid (not provided or incorrect format)", 107, null, context);
	}
	
	var data = invokeSQL(getWorkOrderPartsQueryImproved, [workOrderId], context);
	
	if (data.isSuccessful && data.resultSet) {
		// Loop and build lines for app use
		
		var parentList = []; // Repair lines and isolated parts
		
		for (var i = 0; i < data.resultSet.length; i++) {
			var line = data.resultSet[i];
			
			// Check if line is repair 
			
			if (line.repairDescription) { 
				line.$childParts = []; // init children array
				parentList.push(line);
			} else { 
				// This line is a part
				
				var parentRepairLine = getObjectForRepairCode(line.repairCode, parentList);
				
				// Check to see if it's a child or an orphan
				
				line.totalPerCode = parentRepairLine.materialCost + line.partCost;
				
				if (parentRepairLine) {
					parentRepairLine.$childParts.push(line);
				} else {
					// not possible with how the data is inserted into MERC+
				}
			}
		}
		
		return onRequestSuccess(parentList, context);
	}
	
	return onRequestFailure("SQL error - or unlikely, no results", 100, data, context);
	
	// Helper function
	
	function getObjectForRepairCode(repairCode, array) {
		for (var i = 0; i < array.length; i++) {
			var object = array[i];
			
			if (object.repairCode == repairCode && object.$childParts instanceof Array) return object;
		}
		
		return false;
	} 
} 

function getFinishedWorkOrderParts(username, token, workOrderId, visitId, context) {
	// Sanity check + Token validity
	if (checkUsernameAndToken(username, token, context).valid === false) {
		return checkUsernameAndToken(username, token, context).response;
	}
	
	if (workOrderId === undefined || workOrderId.length == 0) {
		return onRequestFailure("workOrderId query argument invalid (not provided or incorrect format)", 107, null, context);
	}
	
	var data = invokeSQL(getFinishedWorkOrderPartsQuery, [username, workOrderId, visitId], context);
	
	if (data.isSuccessful && data.resultSet) {
		return onRequestSuccess(data.resultSet, context);
	}
	
	return onRequestFailure("SQL error", 100, data, context);
}

function getFinishedVisitParts(username, token, visitId, context) {
	// Sanity check + Token validity
	if (checkUsernameAndToken(username, token, context).valid === false) {
		return checkUsernameAndToken(username, token, context).response;
	}
	
	if (visitId === undefined || visitId.length == 0) {
		return onRequestFailure("visitId query argument invalid (not provided or incorrect format)", 107, null, context);
	}
	
	var data = invokeSQL(getFinishedVisitPartsQuery, [username, visitId], context);
	
	if (data.isSuccessful && data.resultSet) {
		return onRequestSuccess(data.resultSet, context);
	}
	
	return onRequestFailure("SQL error", 100, data, context);
}

function getFinishedVisitPartsPostRepair(username, token, visitId, context) {
	// Sanity check + Token validity
	if (checkUsernameAndToken(username, token, context).valid === false) {
		return checkUsernameAndToken(username, token, context).response;
	}
	
	if (visitId === undefined || visitId.length == 0) {
		return onRequestFailure("visitId query argument invalid (not provided or incorrect format)", 107, null, context);
	}
	
	var data = invokeSQL(getFinishedVisitPartsPostRepairQuery, [username, visitId], context);
	
	if (data.isSuccessful && data.resultSet) {
		return onRequestSuccess(data.resultSet, context);
	}
	
	return onRequestFailure("SQL error", 100, data, context);
}

function saveFinishedWorkOrderBeta(username, token, workOrderId, visitDate, visitId, visitDesc, shopCode, auditResult, archiveId, context) {
	// Sanity check + Token validity
	if (checkUsernameAndToken(username, token, context).valid === false) {
		return checkUsernameAndToken(username, token, context).response;
	}
	
	// General validation
	if (!validArguments([workOrderId, visitId, shopCode, visitDate])) {
		return onRequestFailure("workOrderId, visitId, visitDate or shopCode query argument invalid (not provided or incorrect format)", 107, null, context);
	}
	
	// validate auditStatus
	if (auditResult != 0 && auditResult != 1) {
		return onRequestFailure("auditStatus query argument invalid (not provided or incorrect format)", 107, null, context);
	}
	
	if (typeof visitDesc !== 'string') {
		return onRequestFailure("visitDesc query argument invalid (not provided or incorrect format)", 107, null, context);
	}
	
	var data = invokeSQL(saveFinishedWorkOrderQueryBeta, [username, visitDate, visitId, archiveId, workOrderId, shopCode, auditResult, visitDesc], context);
	
	if (data.isSuccessful && data.updateStatementResult && data.updateStatementResult.updateCount == 1) {
		// update was successful
		return onRequestSuccess(data, context);
	}
	
	// return generic error
	return onRequestFailure("SQL error", 100, data, context);
}

/*
 * get archive name from server
 */
function getArchivePath(username, token, visitId, context) {
	// Sanity check + Token validity
	if (checkUsernameAndToken(username, token, context).valid === false) {
		return checkUsernameAndToken(username, token, context).response;
	}
	
	if (visitId === undefined || visitId.length == 0) {
		return onRequestFailure("visitId query argument invalid (not provided or incorrect format)", 107, null, context);
	}
	
	var data = invokeSQL(getArchivePathQuery, [visitId,username], context);
	if (data && data.isSuccessful) {
		return onRequestSuccess(data, context);
	}
	
	return onRequestFailure("Database SQL execution error thrown", 101, data, context);
}

function userCredentialsValid(username, token, context) {

	// Verify user and token in our database
	var invocationData = {
			adapter : "DBAdapterContainer",
			procedure : "verifyToken",
			parameters : [username, token, (context != undefined ? context : null)]
	};
	var data = WL.Server.invokeProcedure(invocationData);

	// Check returned data
	if (data != undefined && data.invocationResult != undefined) {
		
		// Return token validity
		return data.invocationResult.tokenValid === true;
	}
	
	// Incorrect data - possibly user does not exist in the database
	return false;
}

/*
 * Helper functions
 */

function checkUsernameAndToken(username, token, context) {
	return {valid: true};
	if (username == undefined || username.length == 0 || token == undefined || token.length == 0) {
		return {valid: false, response: onRequestFailure("User credentials not provided (username and/or token)", 105, null, context)}
	} 
	if (!userCredentialsValid(username, token, context)) {
		WL.Logger.info("user credentials invalid: " + username + ":" + token);
		
		// Either user does not exist or user token is invalid or expired - return an error
		return {valid: false, response: onRequestFailure("User does not exist or token expired or invalid (user credentials validity)",
				102, null, context)}
	}
	WL.Logger.info("user \"" + username + "\"token \"" + token + "\" are valid");
	return {valid: true};
}

function validArguments(inputArray) {
	for (var i = 0; i < inputArray.length; i++) {
		var input = inputArray[i];
		if (input == undefined || input.length == 0) {
			return false;
		}
	}
	return true;
}

function stringArguments(inputArray) {
	for (var i = 0; i < inputArray.length; i++) {
		var input = inputArray[i];
		if (typeof input !== 'string') {
			return false;
		}
	}
	return true;
}

function isArray(value) {
	// doesn't work check later TODO
    return value &&
    typeof value === 'object' &&
    typeof value.length === 'number' &&
    typeof value.splice === 'function'
};

function dateToSQLDateTime(date) {
	if (isNaN(date.getUTCFullYear()))
		return;
	
	return date.getFullYear() + '-'
			+ ('00' + (date.getMonth() + 1)).slice(-2) + '-'
			+ ('00' + date.getDate()).slice(-2) + ' '
			+ ('00' + date.getHours()).slice(-2) + ':'
			+ ('00' + date.getMinutes()).slice(-2) + ':'
			+ ('00' + date.getSeconds()).slice(-2);
}

function isNaN(variable) {
	return variable !== variable;
}

function guid() {
	function s4() {
		return Math.floor((1 + Math.random()) * 0x10000).toString(32).substring(1);
	}

	return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
	s4() + '-' + s4() + s4() + s4();
}; 
