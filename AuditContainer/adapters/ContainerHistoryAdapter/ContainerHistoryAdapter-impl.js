/*
 * Mar, 30 2016
 * ContainerHistoryAdapter-impl.js
 * Stavarache Vlad
 */

/* Error Codes:
 * 
 * 0   - No error
 * 99  - Unknown error
 * 100 - Database SQL statement execution error
 * 101 - Database SQL execution error thrown (couldn't connect to the database)
 * 102 - Invalid or expired token
 * 103 - Username not provided
 * 104 - Token not provided
 * 105 - User credentials not provided (username and/or token)
 * 106 - Password not provided
 * 107 - Query arguments invalid (not provided or incorrect format)
 * 108 - Could not generate a new token (database request failed)
 * 109 - Login failure (incorrect username or password)
 */

/**
 * @category Database statements
 */

var getWORemarksHistoryQuery = WL.Server.createSQLStatement(
	"SELECT TOP 20 W.WO_ID as workOrderId, W.WOREMARK_ID as workOrderRemarkId, W.REMARK as remark, W.CRTS as date, W.CHUSER as chuser, W.REMARK_TYPE as source " +
	"FROM MESC1TS_WOREMARK W " +
	"WHERE W.WO_ID = ? " +
	"ORDER BY W.CRTS DESC" 
);

var getContainerHistoryQuery = WL.Server.createSQLStatement(
	"SELECT W.WO_ID , W.MODE, W.EQPNO, W.SHOP_CD, S.SHOP_DESC, L.LOC_DESC, L.LOC_CD, W.STATUS_CODE, W.TOT_COST_REPAIR, W.CRTS, " + 
	"R.REPAIR_CD, R.QTY_REPAIRS, R.SHOP_MATERIAL_AMT, R.REPAIR_LOC_CD, R.DAMAGE_CD, R.MODE as REPAIR_MODE, P.PART_CD, " +
	"P.QTY_PARTS, M.PART_DESC,M.PART_PRICE,M.CORE_PART_SW, RC.REPAIR_DESC FROM MESC1TS_WOREPAIR R " +
	"LEFT JOIN MESC1TS_WOPART P on R.WO_ID=P.WO_ID AND R.REPAIR_CD=P.REPAIR_CD " +
	"LEFT JOIN MESC1TS_MASTER_PART M ON P.PART_CD=M.PART_CD " +
	"LEFT JOIN MEsC1TS_WO W ON R.WO_ID=W.WO_ID " +
	"LEFT JOIN MEsC1TS_SHOP S ON W.SHOP_CD = S.SHOP_CD " +
	"LEFT JOIN MEsC1TS_LOCATION L ON L.LOC_CD = S.LOC_CD " +
	"INNER JOIN MEsC1TS_REPAIR_CODE RC ON R.REPAIR_CD = RC.REPAIR_CD AND R.MODE = RC.MODE " +
	"WHERE W.WO_ID IN " +  
	"(SELECT TOP (10) WO_ID FROM MESC1TS_WO WHERE EQPNO = ? ORDER BY CRTS DESC) ORDER BY W.CRTS DESC"
);

var getContainerRepairsPartsByContainerIdQuery = WL.Server.createSQLStatement(
		"DECLARE @workOrderId VARCHAR(100) = (SELECT TOP (1) WO_ID FROM MESC1TS_WO "+ 
		"WHERE EQPNO = ? and SHOP_CD = ? ORDER BY CRTS DESC) "+  

		"SELECT R.WO_ID as workOrderId, RTRIM(LTRIM(R.REPAIR_CD)) as repairCode, "+
		"RTRIM(LTRIM(IsNull(R.DAMAGE_CD, ''))) as damageCode, RTRIM(LTRIM(IsNull(R.REPAIR_LOC_CD, ''))) as repairLocCode, "+  
		"RTRIM(LTRIM(IsNull(R.TPI_CD, ''))) as TPI, R.MODE as mode, C.REPAIR_DESC as repairDescription, '' as partCode, "+  
		"'' as partDescription, R.QTY_REPAIRS as quantity, "+  
		"null as partCost, R.SHOP_MATERIAL_AMT as materialCost, R.ACTUAL_MANH as manHrs, R.QTY_REPAIRS * R.SHOP_MATERIAL_AMT as totalPerCode, S.STATUS_DSC as statusCode "+  
		"FROM MESC1TS_WOREPAIR R LEFT JOIN "+ 
		"MESC1TS_REPAIR_CODE C ON R.REPAIR_CD = C.REPAIR_CD AND R.MODE = C.MODE AND R.MANUAL_CD = C.MANUAL_CD "+  
		"LEFT JOIN MESC1TS_WO O ON O.WO_ID = R.WO_ID LEFT JOIN MESC1TS_STATUS_CODE S ON S.STATUS_CODE = O.STATUS_CODE "+
		"WHERE R.WO_ID = @workOrderId UNION ALL "+
		"SELECT P.WO_ID as workOrderId, RTRIM(LTRIM(P.REPAIR_CD)) as repairCode, null as damageCode, null as  repairLocCode, "+ 
		"null as TPI, null as mode, null as repairDescription, P.PART_CD as partCode, "+
		"M.PART_DESC as partDescription, QTY_PARTS as quantity, "+ 
		"COST_CPH as partCost, null as materialCost, null as manHrs, null as totalPerCode, T.STATUS_DSC as statusCode "+ 
		"FROM MESC1TS_WOPART P LEFT JOIN "+ 
		"MESC1TS_MASTER_PART M ON P.PART_CD = M.PART_CD "+  
		"LEFT JOIN MESC1TS_WO O ON O.WO_ID = P.WO_ID LEFT JOIN MESC1TS_STATUS_CODE T ON T.STATUS_CODE = O.STATUS_CODE "+ 
		"WHERE P.WO_ID = @workOrderId"
);

//Standard success handler
function onRequestSuccess(data, context) {
	return {  
		isSuccessful : true,
		status : 200,
		errorCode : 0,
		errorMsg : null,
		invocationContext : context,
		invocationResult : data
	}
}

//Standard fail handler
function onRequestFailure(errorMsg, errorCode, data, context) {
	return {
		isSuccessful : false,
		status : 200,
		errorCode : errorCode,
		errorMsg : errorMsg,
		invocationContext : context,
		data : data
	}
}

//Used to call database with invokeSQLStatement
function invokeSQL(statement, parameters, context) {
	try {
		// Invoke SQL statement
		var returnData = WL.Server.invokeSQLStatement({
			preparedStatement : statement,
			parameters : parameters
		});
		
		// Return SQL statement data
		return returnData;

	} catch (error) {
		return onRequestFailure("Database SQL execution error thrown", 101, error, context);
	}
}

function userCredentialsValid(username, token, context) {

	// Verify user and token in our database
	var invocationData = {
			adapter : "DBAdapterContainer",
			procedure : "verifyToken",
			parameters : [username, token, (context != undefined ? context : null)]
	};
	var data = WL.Server.invokeProcedure(invocationData);

	// Check returned data
	if (data != undefined && data.invocationResult != undefined) {
		
		// Return token validity
		return data.invocationResult.tokenValid === true;
	}
	
	// Incorrect data - possibly user does not exist in the database
	return false;
}

/**
* @name getWORemarksHistory
* @desc Get the history of the remarksa Work Order  
* @param {String} username of the current logged in user 
* @param {String} token to be checked 
* @param {Integer} workOrderId unique ID of the work order
* @returns {Function} - Standard success/fail function 
* @memberOf Adapters.ContainerHistoryAdapter
*/
function getWORemarksHistory(username, token, workOrderId,context){
	// Sanity check + Token validity
	if (checkUsernameAndToken(username, token, context).valid === false) {
		return checkUsernameAndToken(username, token, context).response;
	}
	// Check workOrderId 
	if (workOrderId == undefined || workOrderId.length == 0)  {
		return onRequestFailure("Query arguments invalid (not provided or incorrect format)", 107, null, context);
	}

	var data = invokeSQL(getWORemarksHistoryQuery, [workOrderId], context);

	if (data && data.isSuccessful && data.resultSet) {
		var results = data.resultSet;

		return onRequestSuccess(results, context);
	}

	if (data && data.invocationResult && data.invocationResult.isSuccessful === false) {
		WL.Logger.info("Error code - " + data.errorCode + "; Error message - " + data.errorMsg);
		
		return onRequestFailure("Error running \"getWORemarksHistory\" procedure", 100, null, context);
	}
	
	return onRequestFailure("Database SQL execution error thrown", 101, data, context);
};

/**
* @name getContainerHistory
* @desc Get the container history (repair and parts) 
* Prepare array of objects to be returned to client
* Add both REPAIRS and PARTS in same array
* @param {String} username of the current logged in user 
* @param {String} token to be checked 
* @param {String} equipmentNo the containerId for which we get history
* @returns {Function} - Standard success/fail function 
* @memberOf Adapters.ContainerHistoryAdapter
*/
function getContainerHistory(username, token, equipmentNo, context) {
	// Sanity check + Token validity
	if (checkUsernameAndToken(username, token, context).valid === false) {
		return checkUsernameAndToken(username, token, context).response;
	}
	
	// 2. equipmentNo
	if (equipmentNo == undefined || equipmentNo.length == 0)  {
		return onRequestFailure("Query arguments invalid (not provided or incorrect format)", 107, null, context);
	}
	
	var data = invokeSQL(getContainerHistoryQuery, [equipmentNo], context);
	
	if (data && data.isSuccessful && data.resultSet) {
		var results = data.resultSet;
		var parsedContainerHistory = [];
		
		for (var index in results) {
			var repairWorkOrder = results[index];
			
			// if it has a spare part, introduce above the repair if it does not exist
			if(repairWorkOrder.PART_CD == null || repairWorkOrder.PART_CD == undefined && repairWorkOrder.PART_CD == ''){
				parsedContainerHistory.push({
					damageCode : repairWorkOrder.DAMAGE_CD,
					repairCode : repairWorkOrder.REPAIR_CD,
					repairLocCode : repairWorkOrder.REPAIR_LOC_CD,
					repairMode : repairWorkOrder.REPAIR_MODE,
					repairDescription : repairWorkOrder.REPAIR_DESC,
					totalRepairCost : repairWorkOrder.TOT_COST_REPAIR,
					workOrderId : repairWorkOrder.WO_ID,
					parts : repairWorkOrder.QTY_REPAIRS,
					shopCode : repairWorkOrder.SHOP_CD,
					shopName : repairWorkOrder.SHOP_DESC,
					locationName : repairWorkOrder.LOC_DESC, 
					locationCode : repairWorkOrder.LOC_CD,
					partDescription: repairWorkOrder.PART_DESC,
					partNumber : "",
					repairDate : (repairWorkOrder.CRTS) ? repairWorkOrder.CRTS.substring(0, 10) : null,
				})
			}
			else{
				
				var found = false;
				var indexFound = 0;
				
				for(index1 in parsedContainerHistory){
					if (parsedContainerHistory[index1].repairCode+"" == repairWorkOrder.REPAIR_CD+"" && parsedContainerHistory[index1].workOrderId+"" == repairWorkOrder.WO_ID+""){
						found = true;
						indexFound = index1;
						break;
					}
				}
				
				if(!found){
					// add repair
					parsedContainerHistory.push({
						damageCode : repairWorkOrder.DAMAGE_CD,
						repairCode : repairWorkOrder.REPAIR_CD,
						repairLocCode : repairWorkOrder.REPAIR_LOC_CD,
						repairMode : repairWorkOrder.REPAIR_MODE,
						repairDescription : repairWorkOrder.REPAIR_DESC,
						totalRepairCost : repairWorkOrder.TOT_COST_REPAIR,
						workOrderId : repairWorkOrder.WO_ID,
						parts : repairWorkOrder.QTY_REPAIRS,
						shopCode : repairWorkOrder.SHOP_CD,
						shopName : repairWorkOrder.SHOP_DESC,
						locationName : repairWorkOrder.LOC_DESC, 
						locationCode : repairWorkOrder.LOC_CD,
						partDescription: repairWorkOrder.PART_DESC,
						partNumber : "",
						repairDate : (repairWorkOrder.CRTS) ? repairWorkOrder.CRTS.substring(0, 10) : null,
					})
					
					// add part
					
					parsedContainerHistory.push({
						damageCode : repairWorkOrder.DAMAGE_CD,
						repairCode : repairWorkOrder.REPAIR_CD,
						repairLocCode : repairWorkOrder.REPAIR_LOC_CD,
						repairMode : repairWorkOrder.REPAIR_MODE,
						repairDescription : repairWorkOrder.REPAIR_DESC,
						totalRepairCost : repairWorkOrder.TOT_COST_REPAIR,
						workOrderId : repairWorkOrder.WO_ID,
						parts :repairWorkOrder.QTY_PARTS,
						shopCode : repairWorkOrder.SHOP_CD,
						shopName : repairWorkOrder.SHOP_DESC,
						locationName : repairWorkOrder.LOC_DESC, 
						locationCode : repairWorkOrder.LOC_CD,
						partDescription: repairWorkOrder.PART_DESC,
						partNumber : repairWorkOrder.PART_CD,
						repairDate : (repairWorkOrder.CRTS) ? repairWorkOrder.CRTS.substring(0, 10) : null,
					})
				}
				else{
					parsedContainerHistory.splice(indexFound+1,0,{
						damageCode : repairWorkOrder.DAMAGE_CD,
						repairCode : repairWorkOrder.REPAIR_CD,
						repairLocCode : repairWorkOrder.REPAIR_LOC_CD,
						repairMode : repairWorkOrder.REPAIR_MODE,
						repairDescription : repairWorkOrder.REPAIR_DESC,
						totalRepairCost : repairWorkOrder.TOT_COST_REPAIR,
						workOrderId : repairWorkOrder.WO_ID,
						parts : repairWorkOrder.QTY_PARTS,
						shopCode : repairWorkOrder.SHOP_CD,
						shopName : repairWorkOrder.SHOP_DESC,
						locationName : repairWorkOrder.LOC_DESC, 
						locationCode : repairWorkOrder.LOC_CD,
						partDescription: repairWorkOrder.PART_DESC,
						partNumber : repairWorkOrder.PART_CD,
						repairDate : (repairWorkOrder.CRTS) ? repairWorkOrder.CRTS.substring(0, 10) : null,
					})
				}
				
			}
			
			
		}
		
		return onRequestSuccess(parsedContainerHistory, context);
	}
	
	if (data && data.invocationResult && data.invocationResult.isSuccessful === false) {
		WL.Logger.info("Error code - " + data.errorCode + "; Error message - " + data.errorMsg);
		
		return onDBRequestFailure("Error running \"getContainerHistory\" procedure", 100, null, context);
	}
	
	return onDBRequestFailure("Database SQL execution error thrown", 101, data, context);
};


/**
* @name getContainerRepairsPartsByContainerId
* @desc Get the latest Work Order for a specified ContainerID
* Including Repirs and Parts 
* Used ONLY for Add Visit using OCR
* @param {String} username of the current logged in user 
* @param {String} token to be checked 
* @param {String} containerId the containerId for which we get the latest WO
* @param {String} shopId the shopId against we search the latest WO for the containerId
* @param {Object} context
* @returns {Function} - Standard success/fail function 
* @memberOf Adapters.ContainerHistoryAdapter
*/
function getContainerRepairsPartsByContainerId(username, token, containerId, shopId, context){
	//1.Sanity check + Token validity
	if (checkUsernameAndToken(username, token, context).valid === false) {
		return checkUsernameAndToken(username, token, context).response;
	}

	//2. ContainerId && shopId
	if (containerId == undefined || containerId.length == 0 && shopId == undefined || shopId.length == 0)  {
		return onRequestFailure("Query arguments invalid (not provided or incorrect format)", 107, null, context);
	}

	var data = invokeSQL(getContainerRepairsPartsByContainerIdQuery, [containerId, shopId], context);

	if (data && data.isSuccessful && data.resultSet) {
		var results = data.resultSet;

		var repairsList = []; // Repair lines and isolated parts

		for (var i = 0; i < results.length; i++) {
			var line = results[i];

			if(line.repairDescription){
				line.parts = [];
				repairsList.push(line);
			}else{
				var parentRepairLine = checkRepairExistForPartRepairCode(line.repairCode,repairsList);

				if(parentRepairLine){
					parentRepairLine.parts.push(line);
				}
			}
			
		}

		return onRequestSuccess(repairsList, context);
	}

	return onRequestFailure("SQL error - or unlikely, no results", 100, data, context);

	//Check for repair code in repairsList to attach part
	function checkRepairExistForPartRepairCode(repairCode, repairsList) {
		for (var i = 0; i < repairsList.length; i++) {
			var line = repairsList[i];

			if (line.repairCode == repairCode && line.parts instanceof Array) return line;
		}

		return false;
	}

}


//Mocked return (No real functionality)
function checkUsernameAndToken(username, token, context) {
	return {valid: true};
	if (username == undefined || username.length == 0 || token == undefined || token.length == 0) {
		return {valid: false, response: onRequestFailure("User credentials not provided (username and/or token)", 105, null, context)}
	} 
	if (!userCredentialsValid(username, token, context)) {
		WL.Logger.info("user credentials invalid: " + username + ":" + token);
		
		// Either user does not exist or user token is invalid or expired - return an error
		return {valid: false, response: onRequestFailure("User does not exist or token expired or invalid (user credentials validity)",
				102, null, context)}
	}
	WL.Logger.info("user \"" + username + "\"token \"" + token + "\" are valid");
	return {valid: true};
}


