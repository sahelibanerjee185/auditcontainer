package com.ibm.counter.mail;

import com.ibm.json.java.JSONObject;

public class EmailErrorMessages {

	private String msg;
	private String code;
	
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	
	@Override
	public String toString() {
		JSONObject obj = new JSONObject();
		obj.put(code, msg);
		return obj.toString();
	}
	
}
