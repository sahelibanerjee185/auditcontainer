/*
 *    Licensed Materials - Property of IBM
 *    5725-I43 (C) Copyright IBM Corp. 2015. All Rights Reserved.
 *    US Government Users Restricted Rights - Use, duplication or
 *    disclosure restricted by GSA ADP Schedule Contract with IBM Corp.
*/

package com.ibm.counter.mail;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import com.ibm.json.java.JSONArray;
import com.ibm.json.java.JSONObject;
import com.worklight.adapters.rest.api.WLServerAPI;
import com.worklight.adapters.rest.api.WLServerAPIProvider;
import com.worklight.core.auth.OAuthSecurity;

@Path("/sendMail")
public class EmailAdapterResource {
		
	// Define logger (Standard java.util.Logger)
	static Logger logger = Logger.getLogger(EmailAdapterResource.class.getName());

    // Define the server api to be able to perform server operations
    WLServerAPI api = WLServerAPIProvider.getWLServerAPI();

	/* Path for method: "<server address>/Counter/adapters/EmailAdapter/sendMail/{from}/{to}/{subject}/{message}" */
	@POST
	@OAuthSecurity(enabled=false)
	@Path("/{from}/{to}")
	@Produces({"application/json"})
	public String sendMail(
			@PathParam("from") String from, 
			@PathParam("to") String to,
			@FormParam("subject") String subject,
			@FormParam("message") String message,
			@FormParam("attachments") String attachments,
			@FormParam("bcc") String bcc
			) {
		
		if (attachments != null && !attachments.isEmpty()) {
			try {
				JSONArray array = JSONArray.parse(attachments);
				List<AttachmentData> attachmentsList = new ArrayList<AttachmentData>();
				
				// Get attachment data
				for (Object object : array) {
					JSONObject attachmentJson = JSONObject.parse(object.toString());
					
					if (attachmentJson.get("fileName") != null && attachmentJson.get("data") != null) {
						attachmentsList.add(new AttachmentData(
								attachmentJson.get("fileName").toString(),
								attachmentJson.get("data").toString()));
					} 
				}

				// Sent email without any attachments
				return (new EmailAdapterApplication()).sendEmailWithAttachments(from, to, subject, message, attachmentsList, bcc);
			} catch (IOException e) {
				e.printStackTrace();
				
				// Could not parse JSON object, return an error
				EmailErrorMessages error = new EmailErrorMessages();
				error.setCode("415");
				error.setMsg("Could not parse JSON object");
				return error.toString();
			}
		} else {
			
			// Sent email without any attachments
			return (new EmailAdapterApplication()).sendEmail(from, to, subject, message, bcc);
		}
	}
}

