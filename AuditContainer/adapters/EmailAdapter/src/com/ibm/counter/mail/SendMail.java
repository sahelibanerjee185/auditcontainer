package com.ibm.counter.mail;


import java.util.List;
import java.util.Properties;
import java.util.logging.Logger;

import javax.activation.DataHandler;
import javax.mail.Address;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.xml.bind.DatatypeConverter;

/**
 * SendEmail adapter
 * @author larisestavarache
 *
 */
public class SendMail {
	
	private static String PROPS_MAIL_USER_KEY = "mail.user";
	private static String PROPS_MAIL_PASSWORD_KEY = "mail.password";
	private static String PROPS_MAIL_SMTP_AUTH_KEY = "mail.smtp.auth";
	private static String PROPS_MAIL_TLS_ENABLE_KEY = "mail.smtp.starttls.enable";
	private static String PROPS_MAIL_SMTP_HOST_KEY = "mail.smtp.host";
	private static String PROPS_MAIL_SMTP_PORT_KEY = "mail.smtp.port";
	
	private static String smtpHost = "mail78.apmoller.net"; 
	private static String smtpPort = "25";
	private static boolean smtpAuth = false;
	private static String smtpUser = null;
	private static String smtpPass = null;
	private static boolean smtpStartTls = false;
	
	static Logger logger = Logger.getLogger(EmailAdapterApplication.class.getName());
	
	public static void setSMTPConfiguration(String smtpHost, String smtpPort,
			boolean smtpAuth, String smtpUser, String smtpPass, boolean smtpStartTls) {
		SendMail.smtpHost = smtpHost;
		SendMail.smtpPort = smtpPort;
		SendMail.smtpAuth = smtpAuth;
		SendMail.smtpUser = smtpUser;
		SendMail.smtpPass = smtpPass;
		SendMail.smtpStartTls = smtpStartTls;
	}
	
	/**
	 * Public exposed method called from the worklight adapter.
	 * 
	 * Tries to init the mail properties based on the worklight.properties file
	 * and to send the email.
	 * 
	 * @return
	 */
	public static String sendMail(String from, String to, String subject, String message, Boolean attach, List<AttachmentData> attachments, String bcc) {
		
		String ret = null;
		System.out.println("SendMail :: sendMail :: from " + from + ", to " + to
				+ ", subject " + subject + ", message " + message);
		
		Session session = null;
		Properties mailProps = getEmailProperties();

		logger.info("SendMail :: callMailSend :: smtpAuth = "+smtpAuth + ", smtpHost = "+smtpHost);
		try {
			if (smtpAuth) {
				session = Session.getInstance(mailProps,
						new javax.mail.Authenticator() {
							protected PasswordAuthentication getPasswordAuthentication() {
								return new PasswordAuthentication(smtpUser,
										smtpPass);
							}
						});
			} else {
				session = Session.getInstance(mailProps);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		logger.info(session + "");
		try {
			logger.info("TO =========== " + to);
			logger.info("SendMail :: callMailSend :: Sending mail...");
			Message mimeMessage = new MimeMessage(session);
			mimeMessage.setFrom(new InternetAddress(from));
			
			String delims = ",";
		    String[] emails = to.split(delims);
		    
		    Address[] adresses = new Address[emails.length];
		    
			for (int i = 0; i < emails.length; i++) {
				logger.info("TO " + i + " ==== " + emails[i]);
				adresses[i] = new InternetAddress(emails[i]);
		    }
		
			mimeMessage.addRecipients(Message.RecipientType.TO, adresses);
			
			
			
			mimeMessage.setSubject(subject);
			
			if (bcc != null && !bcc.isEmpty()) {
				mimeMessage.addRecipient(Message.RecipientType.BCC, new InternetAddress(bcc));
			}
			
			// Create a multi-part message
			Multipart multipart = new MimeMultipart();
			
			// Add text message body part
			BodyPart messageBodyPart = new MimeBodyPart();
			messageBodyPart.setText(message + "\n");
			multipart.addBodyPart(messageBodyPart);

			if (attach) {
				for (AttachmentData attachmentData : attachments) {
					
			         // Decode base64 attachment
			         byte[] bytes = DatatypeConverter.parseBase64Binary(attachmentData.getData());
	
			         // Part two is attachment
			         BodyPart attchmentBodyPart = new MimeBodyPart();
			         attchmentBodyPart.setDataHandler(new DataHandler(bytes, "application/octet-stream"));
			         attchmentBodyPart.setFileName(attachmentData.getFileName());
			         multipart.addBodyPart(attchmentBodyPart);					
				}
			}
			
			// Add message body parts
			mimeMessage.setContent(multipart);
			
			// Send email
			Transport.send(mimeMessage);
			logger.info("SendMail :: callMailSend :: mail sent!");
		} catch (MessagingException e) {
			logger.info("SendMail :: callMailSend :: Error sending mail: "
							+ e.getMessage());
			e.printStackTrace();
			ret = e.getMessage();
		}

		return ret;
	}
	
	private static Properties getEmailProperties() {
		Properties p = new Properties();	
		p.setProperty(PROPS_MAIL_SMTP_HOST_KEY, SendMail.smtpHost);
		p.setProperty(PROPS_MAIL_SMTP_PORT_KEY, SendMail.smtpPort);
		p.setProperty(PROPS_MAIL_SMTP_AUTH_KEY, "" + SendMail.smtpAuth);
		p.setProperty(PROPS_MAIL_TLS_ENABLE_KEY, "" + SendMail.smtpStartTls);
		SendMail.smtpUser = (SendMail.smtpUser == null ? "" : SendMail.smtpUser);
		p.setProperty(PROPS_MAIL_USER_KEY, SendMail.smtpUser);
		SendMail.smtpPass = (SendMail.smtpPass == null ? "" : SendMail.smtpPass);
		p.setProperty(PROPS_MAIL_PASSWORD_KEY, SendMail.smtpPass);
		
		return p;
	}
}
