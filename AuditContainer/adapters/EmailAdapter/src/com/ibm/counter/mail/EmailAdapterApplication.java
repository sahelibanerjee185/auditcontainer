/*
 *    Licensed Materials - Property of IBM
 *    5725-I43 (C) Copyright IBM Corp. 2015. All Rights Reserved.
 *    US Government Users Restricted Rights - Use, duplication or
 *    disclosure restricted by GSA ADP Schedule Contract with IBM Corp.
*/

package com.ibm.counter.mail;

import java.util.List;
import java.util.logging.Logger;

import com.worklight.wink.extensions.MFPJAXRSApplication;

public class EmailAdapterApplication extends MFPJAXRSApplication{

	static Logger logger = Logger.getLogger(EmailAdapterApplication.class.getName());
	
	@Override
	protected void init() throws Exception {
		logger.info("Email Adapter initialized!");
	}
	
	@Override
	protected void destroy() throws Exception {
		logger.info("Email Adapter destroyed!");
	}
	
	@Override
	protected String getPackageToScan() {
		return getClass().getPackage().getName();
	}
	
	/**
	 * 
	 * @param from
	 * @param to
	 * @param subject
	 * @param message
	 * @action send email to GSIM
	 */
	public String sendEmail(String from, String to, String subject, String message, String bcc){
		EmailErrorMessages error = new EmailErrorMessages();
		try {
			String aux = SendMail.sendMail(from, to, subject, message, false , null, bcc);
			if ( aux == null){
				error.setCode("200");
				error.setMsg("Email sent succesfully");
				return error.toString();
			}
			else{
				error.setCode("300");
				error.setMsg(aux);
				return error.toString();
			}
		} catch (Exception e) {
			
			error.setCode("401");
			error.setMsg("Exception unrelated to email thrown");
			return error.toString();
			
		}
		
	}
	
	/**
	 * 
	 * @param from
	 * @param to
	 * @param subject
	 * @param message
	 * @return 
	 * @action send email to GSIM
	 */
	public String sendEmailWithAttachments(String from, String to, String subject, String message, List<AttachmentData> attachments, String bcc) {

		EmailErrorMessages error = new EmailErrorMessages();
		try {
			String aux = SendMail.sendMail(from, to, subject, message, true, attachments, bcc);
			if ( aux == null){
				error.setCode("200");
				error.setMsg("Email sent succesfully");
				return error.toString();
			}
			else{
				error.setCode("300");
				error.setMsg(aux);
				return error.toString();
			}
		} catch (Exception e) {
			
			error.setCode("401");
			error.setMsg("Exception unrelated to email thrown");
			return error.toString();
			
		}
	}
}
