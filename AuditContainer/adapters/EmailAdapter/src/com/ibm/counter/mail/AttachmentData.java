package com.ibm.counter.mail;

public class AttachmentData {
	
	private String fileName;
	private String data;
	
	public AttachmentData(String fileName, String data) {
		super();
		this.fileName = fileName;
		this.data = data;
	}

	public String getFileName() {
		return fileName;
	}
	
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	
	public String getData() {
		return data;
	}
	
	public void setData(String data) {
		this.data = data;
	}
	
	@Override
	public String toString() {
		return "AttachmentData [fileName=" + fileName + ", data=" + data + "]";
	}
}

