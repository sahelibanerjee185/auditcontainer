/*
 * Mar, 30 2016
 * ContainerHistoryAdapter-impl.js
 * Stavarache Vlad
 */

/* Error Codes:
 * 
 * 0   - No error
 * 99  - Unknown error
 * 100 - Database SQL statement execution error
 * 101 - Database SQL execution error thrown (couldn't connect to the database)
 * 102 - Invalid or expired token
 * 103 - Username not provided
 * 104 - Token not provided
 * 105 - User credentials not provided (username and/or token)
 * 106 - Password not provided
 * 107 - Query arguments invalid (not provided or incorrect format)
 * 108 - Could not generate a new token (database request failed)
 * 109 - Login failure (incorrect username or password)
 */

/**
 * @category Database statements
 */

var getShopsByCountryProcedure = WL.Server.createSQLStatement(
	"EXEC [dbo].[getShopsByCountry] " +
	"@Country_code = ?, " +
	"@UserId = '', " +
	"@token = '', " +
	"@outMessage = '', " +
	"@outMessageCode = ''"
);

var searchAllRepairShopsQuery = WL.Server.createSQLStatement(
	"SELECT TOP(15) S.SHOP_CD, S.SHOP_DESC, L.LOC_DESC, L.COUNTRY_CD FROM MESC1TS_SHOP S " +
	"LEFT JOIN MESC1TS_LOCATION L " +
	"ON S.LOC_CD = L.LOC_CD " +
	"WHERE S.SHOP_DESC LIKE ? OR S.SHOP_CD LIKE ?"
);

// might have to change procedure to match query TODO

var geShopsByCountryQuery = WL.Server.createSQLStatement(
	"SELECT L.COUNTRY_CD, L.LOC_DESC,  S.* FROM MESC1TS_SHOP S , MESC1TS_LOCATION L " +
	"WHERE S.LOC_CD = L.LOC_CD AND COUNTRY_CD = ?"
);

var getShopsForCertificateQuery = WL.Server.createSQLStatement(
	"DECLARE @userId VARCHAR(100) = (SELECT TOP 1 U.USER_ID FROM SEC_USER U WHERE U.LOGIN = ?) " +
	"DECLARE @authGroupId VARCHAR(100) = (SELECT TOP 1 A.AUTHGROUP_ID FROM SEC_AUTHGROUP_USER A WHERE A.USER_ID = @userId) " +
	"IF @authGroupId = '3' " +
	// AREA
	"SELECT S.SHOP_CD AS code, " +
	"L.LOC_DESC AS location, " +
	"S.SHOP_DESC AS name, " +
	"LO.COUNTRY_CD AS country " +
	"FROM MESC1TS_SHOP S " +
	"LEFT JOIN MESC1VS_SHOP_LOCATION LO " +
	"ON LO.SHOP_CD = S.SHOP_CD " +
	"LEFT JOIN MESC1TS_LOCATION L " +
	"ON L.LOC_CD = LO.LOC_CD " +
	"WHERE S.SHOP_ACTIVE_SW = 'Y' AND LO.AREA_CD IN ( " +
	"SELECT U.COLUMN_VALUE FROM SEC_AUTHGROUP_USER U WHERE U.USER_ID = @userId " +
	") " +
	"ELSE IF @authGroupId = '4' " +
	// COUNTRY 
	"SELECT S.SHOP_CD AS code, " +
	"L.LOC_DESC AS location, " +
	"S.SHOP_DESC AS name, " +
	"LO.COUNTRY_CD AS country " +
	"FROM MESC1TS_SHOP S " +
	"LEFT JOIN MESC1VS_SHOP_LOCATION LO " +
	"ON LO.SHOP_CD = S.SHOP_CD " +
	"LEFT JOIN MESC1TS_LOCATION L " +
	"ON L.LOC_CD = LO.LOC_CD " +
	"WHERE S.SHOP_ACTIVE_SW = 'Y' AND LO.COUNTRY_CD IN ( " +
	"SELECT U.COLUMN_VALUE FROM SEC_AUTHGROUP_USER U WHERE U.USER_ID = @userId " +
	") " +
	"ELSE " +
	// OTHER
	"SELECT TOP 0 NULL"
);

var getShopEmailQuery = WL.Server.createSQLStatement(
	"SELECT S.EMAIL_ADR as email FROM MESC1TS_SHOP S " +
	"WHERE S.SHOP_CD = ?"
);

var getAvailableCountriesQuery = WL.Server.createSQLStatement(
	"SELECT AVAILABLE_COUNTRIES.COUNTRY_CD, C.COUNTRY_DESC FROM " +
	"(SELECT DISTINCT SUBSTRING(LOC_CD, 1, 2) AS COUNTRY_CD FROM dbo.MESC1TS_SHOP) " +
	"AS AVAILABLE_COUNTRIES " +
	"LEFT JOIN dbo.MESC1TS_COUNTRY C " +
	"ON C.COUNTRY_CD = AVAILABLE_COUNTRIES.COUNTRY_CD"
);

var getAvailableCountriesForCertificateQuery = WL.Server.createSQLStatement(
	"DECLARE @userId VARCHAR(100) = (SELECT TOP 1 U.USER_ID FROM SEC_USER U WHERE U.LOGIN = ?) " +
	"DECLARE @authGroupId VARCHAR(100) = (SELECT TOP 1 A.AUTHGROUP_ID FROM SEC_AUTHGROUP_USER A WHERE A.USER_ID = @userId) " +
	"IF @authGroupId = '3' " +
	// AREA 
	"SELECT C.COUNTRY_DESC AS name, C.COUNTRY_CD AS code  FROM SEC_AUTHGROUP_USER A LEFT JOIN MESC1TS_COUNTRY C " +
	"ON A.COLUMN_VALUE = C.AREA_CD " +
	"WHERE A.USER_ID = @userId " +
	"ELSE IF @authGroupId = '4' " +
	// COUNTRY 
	"SELECT C.COUNTRY_DESC AS name, C.COUNTRY_CD AS code  FROM SEC_AUTHGROUP_USER A LEFT JOIN MESC1TS_COUNTRY C " +
	"ON A.COLUMN_VALUE = C.COUNTRY_CD " +
	"WHERE A.USER_ID = @userId " +
	"ELSE " +
	// OTHER
	"SELECT TOP 0 NULL"
)
	
var getWorkOrdersByShopIdQuery = WL.Server.createSQLStatement(
	"SELECT * FROM (SELECT W.WO_ID AS workOrderId, W.MODE AS repairMode, W.TOTAL_COST_LOCAL_USD as repairCost, CONVERT(VARCHAR(10), D.AUDIT_DATE, 21) AS auditDate, " +
	"W.EQPNO AS containerNo, W.SHOP_CD AS shopCode, S.STATUS_DSC AS status, CONVERT(VARCHAR(10), W.REPAIR_DTE, 21) AS repairDate, " +
	"(CASE WHEN D.AUDIT_STATUS = 0 OR D.AUDIT_STATUS = 1 THEN 1 ELSE 0 END) AS locked from MESC1TS_WO W " +
	"LEFT JOIN MESC1TS_AUDIT_DETAILS D ON W.WO_ID=D.WO_ID " +
	"LEFT JOIN MEsC1TS_STATUS_CODE S ON S.STATUS_CODE = W.STATUS_CODE WHERE " +
	"W.REPAIR_DTE >= ? and W.REPAIR_DTE <= ? and W.SHOP_CD= ? AND W.STATUS_CODE <> 9999) T"
);

var getWorkOrdersByShopIdQueryImproved = WL.Server.createSQLStatement(
	"DECLARE @limitDate DATETIME SET @limitDate = DateAdd(DD, -28, GETDATE()) " +
	"SELECT TOP 40000 W.WO_ID AS workOrderId, CONVERT(VARCHAR(10), W.GATEINDTE, 21) AS gateInDate, W.MODE AS repairMode, W.TOTAL_COST_LOCAL_USD as repairCost, " +
	"W.EQPNO AS containerNo, W.SHOP_CD AS shopCode, (CASE WHEN (W.STATUS_CODE > 390) THEN 'Post - Repair' ELSE S.STATUS_DSC END) AS status, CONVERT(VARCHAR(10), W.REPAIR_DTE, 21) AS repairDate, W.STATUS_CODE AS statusCode, " +
	"(CASE WHEN (D.AUDIT_STATUS = 0 OR D.AUDIT_STATUS = 1) THEN 1 WHEN W.STATUS_CODE = '150' THEN 2 ELSE 0 END) AS locked " +
	"from MESC1TS_WO W " +
	"LEFT JOIN MESC1TS_AUDIT_DETAILS D ON W.WO_ID=D.WO_ID " +
	"LEFT JOIN MEsC1TS_STATUS_CODE S ON S.STATUS_CODE = W.STATUS_CODE WHERE " +
	"W.REPAIR_DTE >= ? and W.SHOP_CD= ? AND W.STATUS_CODE >= 100 AND W.STATUS_CODE <= 900 AND " +
	"((W.STATUS_CODE > 390 AND W.REPAIR_DTE >= @limitDate) OR (W.STATUS_CODE <= 390))" + 
	"ORDER BY W.REPAIR_DTE DESC"
);

var getWorkOrdersByShopIdQueryForDevData = WL.Server.createSQLStatement(
	"DECLARE @limitDate DATETIME SET @limitDate = DateAdd(DD, -1000, GETDATE()) " +
	"SELECT TOP 40000 W.WO_ID AS workOrderId, CONVERT(VARCHAR(10), W.GATEINDTE, 21) AS gateInDate, W.MODE AS repairMode, W.TOTAL_COST_LOCAL_USD as repairCost, " +
	"W.EQPNO AS containerNo, W.SHOP_CD AS shopCode, (CASE WHEN (W.STATUS_CODE > 390) THEN 'Post - Repair' ELSE S.STATUS_DSC END) AS status, CONVERT(VARCHAR(10), W.REPAIR_DTE, 21) AS repairDate, W.STATUS_CODE AS statusCode, " +
	"(CASE WHEN (D.AUDIT_STATUS = 0 OR D.AUDIT_STATUS = 1) THEN 1 WHEN W.STATUS_CODE = '150' THEN 2 ELSE 0 END) AS locked " +
	"from MESC1TS_WO W " +
	"LEFT JOIN MESC1TS_AUDIT_DETAILS D ON W.WO_ID=D.WO_ID " +
	"LEFT JOIN MEsC1TS_STATUS_CODE S ON S.STATUS_CODE = W.STATUS_CODE WHERE " +
	"W.REPAIR_DTE >= ? and W.SHOP_CD= ? AND W.STATUS_CODE >= 100 AND W.STATUS_CODE <= 900 AND " +
	"((W.STATUS_CODE > 390 AND W.REPAIR_DTE >= @limitDate) OR (W.STATUS_CODE <= 390))" + 
	"ORDER BY W.REPAIR_DTE DESC"
);

var getWorkOrderByShopIdCount = WL.Server.createSQLStatement(
	"DECLARE @limitDate DATETIME SET @limitDate = DateAdd(DD, -28, GETDATE()) " +
	"SELECT COUNT(*) AS count from MESC1TS_WO W " +
	"WHERE " + 
	"W.REPAIR_DTE >= ? and W.SHOP_CD = ? AND W.STATUS_CODE >= 100 AND W.STATUS_CODE <= 900 " +
	"AND ((W.STATUS_CODE > 390 AND W.REPAIR_DTE >= @limitDate) OR (W.STATUS_CODE <= 390)) " +  
	"GROUP BY W.SHOP_CD"
);

var getWorkOrderByShopIdCountForDevData = WL.Server.createSQLStatement(
	"DECLARE @limitDate DATETIME SET @limitDate = DateAdd(DD, -1000, GETDATE()) " +
	"SELECT COUNT(*) AS count from MESC1TS_WO W " +
	"WHERE " + 
	"W.REPAIR_DTE >= ? and W.SHOP_CD = ? AND W.STATUS_CODE >= 100 AND W.STATUS_CODE <= 900 " +
	"AND ((W.STATUS_CODE > 390 AND W.REPAIR_DTE >= @limitDate) OR (W.STATUS_CODE <= 390)) " +  
	"GROUP BY W.SHOP_CD"
);

var getWorkOrdersByIdListQuery = "SELECT W.WO_ID AS workOrderId, W.MODE AS repairMode, W.TOTAL_COST_LOCAL_USD as repairCost, " +
	"W.EQPNO AS containerNo, W.SHOP_CD AS shopCode, S.STATUS_DSC AS status, CONVERT(VARCHAR(10), W.REPAIR_DTE, 21) AS repairDate, " +
	"(CASE WHEN D.AUDIT_STATUS = 0 OR D.AUDIT_STATUS = 1 THEN 1 ELSE 0 END) AS locked from MESC1TS_WO W " +
	"LEFT JOIN MESC1TS_AUDIT_DETAILS D ON W.WO_ID=D.WO_ID " +
	"LEFT JOIN MEsC1TS_STATUS_CODE S ON S.STATUS_CODE = W.STATUS_CODE WHERE " +
	"W.WO_ID IN ("; 

for (var i = 0; i < 2000; i++) {
	getWorkOrdersByIdListQuery += "?,";
}

getWorkOrdersByIdListQuery = getWorkOrdersByIdListQuery.substring(0, getWorkOrdersByIdListQuery.length - 1);

getWorkOrdersByIdListQuery += ")";

var getWorkOrdersByIdList = WL.Server.createSQLStatement(
	getWorkOrdersByIdListQuery
)

var getWorkOrderIdsByShopId = WL.Server.createSQLStatement(
	"SELECT W.WO_ID AS i from MESC1TS_WO W " +
	"WHERE " +
	"W.REPAIR_DTE >= ? and W.SHOP_CD = ? AND W.STATUS_CODE <> 9999 " +
	"ORDER BY W.REPAIR_DTE DESC"
);

var getWorkOrderByShopIdProcedure = WL.Server.createSQLStatement(
	"EXEC [dbo].[getWorkOrderByShopId] " +
	"@shop_code = ?, " +
	"@from_date = ?, " +
	"@to_date = ?, " +
	"@UserId = '', " +
	"@token = '', " +
	"@outMessage = '', " +
	"@outMessageCode = ''"
);

var getAllRepairShopsQuery = WL.Server.createSQLStatement(
	"SELECT L.COUNTRY_CD, L.LOC_DESC,  S.* FROM MESC1TS_SHOP S , MESC1TS_LOCATION L " +
	"WHERE S.LOC_CD = L.LOC_CD"
);

var getAvailableRepairShopsQuery = WL.Server.createSQLStatement(
	"SELECT L.COUNTRY_CD, L.LOC_DESC,  S.* FROM MESC1TS_SHOP S , MESC1TS_LOCATION L " +
	"WHERE S.LOC_CD = L.LOC_CD"
);

function onRequestSuccess(data, context) {
	return {
		isSuccessful : true,
		status : 200,
		errorCode : 0,
		errorMsg : null,
		invocationContext : context,
		invocationResult : data
	}
}

function onRequestFailure(errorMsg, errorCode, data, context) {
	return {
		isSuccessful : false,
		status : 200,
		errorCode : errorCode,
		errorMsg : errorMsg,
		invocationContext : context,
		data : data
	}
}

function invokeSQL(statement, parameters, context) {
	try {
		// Invoke SQL statement
		var returnData = WL.Server.invokeSQLStatement({
			preparedStatement : statement,
			parameters : parameters
		});
		
		// Return SQL statement data
		return returnData;

	} catch (error) {
		return onRequestFailure("Database SQL execution error thrown", 101, error, context);
	}
}

function userCredentialsValid(username, token, context) {

	// Verify user and token in our database
	var invocationData = {
			adapter : "DBAdapterContainer",
			procedure : "verifyToken",
			parameters : [username, token, (context != undefined ? context : null)]
	};
	var data = WL.Server.invokeProcedure(invocationData);

	// Check returned data
	if (data != undefined && data.invocationResult != undefined) {
		
		// Return token validity
		return data.invocationResult.tokenValid === true;
	}
	
	// Incorrect data - possibly user does not exist in the database
	return false;
}

function getShopsByCountry(username, token, countryCode, context) {
	// Sanity check + Token validity
	if (checkUsernameAndToken(username, token, context).valid === false) {
		return checkUsernameAndToken(username, token, context).response;
	}
	
	// countryCode validity
	if (countryCode == undefined || countryCode.length == 0 || countryCode.length > 2)  {
		return onRequestFailure("countryCode query argument invalid (not provided or incorrect format)", 107, null, context);
	}
	
	var data = invokeSQL(geShopsByCountryQuery, [countryCode], context);
	
	if (data.isSuccessful && data.resultSet.length > 0) {
		var parsedShops = [];
		var results = data.resultSet;
		
		for (var index in results) {
			var shop = results[index];
			
			parsedShops.push({
				location : shop.LOC_DESC,
				shopName : shop.SHOP_DESC,
				shopNo : shop.SHOP_CD,
				countryCode : shop.COUNTRY_CD
			})
		}
		
		return onRequestSuccess(parsedShops, context);
	}
	
	return onRequestFailure("SQL error - or unlikely, no results", 100, data, context);
}

/**
 * @name getAvailableCountries
 * @desc Retrieves all countries from MESC1TS_COUNTRY table
 * @param {String} username
 * @param {String} token - no token auth used
 * @param {Any} context
 * @returns {Object}
 * @memberof Adapters.RepairShopAdapter
 */
function getAvailableCountries(username, token, context) {
	// Sanity check + Token validity
	if (checkUsernameAndToken(username, token, context).valid === false) {
		return checkUsernameAndToken(username, token, context).response;
	}
	
	var data = invokeSQL(getAvailableCountriesQuery, [], context);
	
	if (data.isSuccessful && data.resultSet.length > 0) {
		var parsedCountries = [];
		var results = data.resultSet;
		
		for (var index in results) {
			var country = results[index];
			
			parsedCountries.push({
				name : country.COUNTRY_DESC,
				code : country.COUNTRY_CD
			})
		}
		
		return onRequestSuccess(parsedCountries, context);
	}
	
	return onRequestFailure("Countries not found - SQL error", 100, data, context);
}

/**
 * @name getAvailableCountriesForCertificate
 * @desc Retrieves countries asscoiated with certificate from MESC1TS_COUNTRY table
 * for authentification groups AREA and COUNTRY
 * @param {String} username
 * @param {String} token - no token auth used
 * @param {String} certificateId 
 * @param {Any} context
 * @returns {Object}
 * @memberof Adapters.RepairShopAdapter
 */
function getAvailableCountriesForCertificate(username, token, certificateId, context) {
	if (typeof username  != 'string' || username.length == 0)  {
		return onRequestFailure("username query argument invalid (not provided or incorrect format)", 107, null, context);
	}
	
	if (typeof certificateId  == 'undefined' || certificateId.length == 0)  {
		return onRequestFailure("certificateId query argument invalid (not provided or incorrect format)", 107, null, context);
	}
	
	var data = invokeSQL(getAvailableCountriesForCertificateQuery, [certificateId], context);
	
	if (data.isSuccessful && data.resultSet.length > 0) {
		var results = data.resultSet;
		return onRequestSuccess(results, context);
	}
	
	return onRequestFailure("Countries not found - SQL error", 100, data, context);
}

/**
 * @name getWorkOrdersByShopId
 * @desc Deprecated
 * @memberof Adapters.RepairShopAdapter
 */
function getWorkOrdersByShopId(username, token, shopCode, fromDate, toDate, context) {
	// Sanity check + Token validity
	if (checkUsernameAndToken(username, token, context).valid === false) {
		return checkUsernameAndToken(username, token, context).response;
	}
	
	// shopCode validity
	if (shopCode == undefined || shopCode.length == 0)  {
		return onRequestFailure("shopCode query argument invalid (not provided or incorrect format)", 107, null, context);
	}
	
	var sqlFromDate = dateToSQLDateTime(new Date(fromDate));
	var sqlToDate = dateToSQLDateTime(new Date(toDate));
	
    // fromDate, toDate validity	
	if (!sqlFromDate || !sqlToDate) {
		return onRequestFailure("fromDate or toDate query argument invalid (not provided or incorrect format)", 107, data, context);
	}
	
	var data = invokeSQL(getWorkOrdersByShopIdQuery, [sqlFromDate.toString(), sqlToDate.toString(), shopCode], context);
	
	if (data.isSuccessful && data.resultSet) {
		// result needs to be parsed to return only necessary fields
		var results = data.resultSet;
		
		return onRequestSuccess(results, context);
	}
	
	return onRequestFailure("Work orders not found - SQL error", 100, data, context);
}

/**
 * @name getShopEmail
 * @desc Retrieves email column associated with shopCode
 * from MESC1TS_SHOP table
 * @param {String} username
 * @param {String} token - no token auth used
 * @param {String} shopCode
 * @param {Any} - context
 * @returns {Object}
 * @memberof Adapters.RepairShopAdapter
 */
function getShopEmail(username, token, shopCode, context) {
	// Sanity check + Token validity
	if (checkUsernameAndToken(username, token, context).valid === false) {
		return checkUsernameAndToken(username, token, context).response;
	}
	
	// shopCode validity
	if (shopCode == undefined || shopCode.length == 0)  {
		return onRequestFailure("shopCode query argument invalid (not provided or incorrect format)", 107, null, context);
	}
	
	var data = invokeSQL(getShopEmailQuery, [shopCode], context);
	
	if (data.isSuccessful && data.resultSet) {
		// result needs to be parsed to return only necessary fields
		var results = data.resultSet;
		
		if (results.length > 0) {
			return onRequestSuccess(results[0], context);
		}
	}
	
	return onRequestFailure("Work orders not found - SQL error", 100, data, context);
}

/**
 * @name getWorkOrdersByShopIdImproved
 * @desc Retrieves work orders from shop,
 * maximum of maxResultsCount count,
 * pre-repairs (with a status of <= 390) with a minimum date of fromDate
 * post-repairs (with a status > 390) and change timestamp larger than 15 days ago
 * status code 150 is set to locked value 2,
 * and already booked work orders have a lock value of 1
 * @param {String} - username
 * @param {String} - token - no token auth used
 * @param {String} shopCode
 * @param {Date} fromDate - minimum repair date of retrieved work orders
 * @param {Any} - context
 * @returns {Object} - {results: Array, countExceeded: Bool, startingDate: Date}
 * @memberof Adapters.RepairShopAdapter
 */
function getWorkOrdersByShopIdImproved(username, token, shopCode, fromDate, context) {
	// Sanity check + Token validity
	if (checkUsernameAndToken(username, token, context).valid === false) {
		return checkUsernameAndToken(username, token, context).response;
	}
	
	// shopCode validity
	if (shopCode == undefined || shopCode.length == 0)  {
		return onRequestFailure("shopCode query argument invalid (not provided or incorrect format)", 107, null, context);
	}
	
	var sqlFromDate = dateToSQLDateTime(new Date(fromDate));
	
    // fromDate, toDate validity	
	if (!sqlFromDate) {
		return onRequestFailure("fromDate or toDate query argument invalid (not provided or incorrect format)", 107, data, context);
	}
	
	var maxResultsCount = 40000; 
	var countExceeded = false;
	var startingDate;
	var responseObject = {};
	
	// get result count first!
	
	var resultsCount = invokeSQL(getWorkOrderByShopIdCount, [sqlFromDate.toString(), shopCode], context);
	
	if (resultsCount.isSuccessful && resultsCount.resultSet) {
		if (resultsCount.resultSet.length == 0) {
			responseObject.results = [];
			return onRequestSuccess(responseObject, context);
		} else {
			if (resultsCount.resultSet[0].count > maxResultsCount) {
				countExceeded = true;
			} 
		}
	} else {
		return onRequestFailure("Work orders not found - SQL error", 100, data, context);
	}
	
	var data = invokeSQL(getWorkOrdersByShopIdQueryImproved, [sqlFromDate.toString(), shopCode], context);
	
	if (data.isSuccessful && data.resultSet) {
		// result needs to be parsed to return only necessary fields
		responseObject.results = data.resultSet
		responseObject.countExceeded = countExceeded;
		
		if (countExceeded) {
			startingDate = responseObject.results[responseObject.results.length - 1]['repairDate'];
			responseObject.startingDate = startingDate;
		}
		
		return onRequestSuccess(responseObject, context);
	}
	
	return onRequestFailure("Work orders not found - SQL error", 100, data, context);
}

/**
 * @name getWorkOrdersByShopIdForDev
 * @desc Used only for testing on dev/preprod
 * same as getWorkOrdersByShopIdImproved
 * except that @limitDate is 1000 days ago
 * @param {String} - username
 * @param {String} - token - no token auth used
 * @param {String} shopCode
 * @param {Date} fromDate - minimum repair date of retrieved work orders
 * @param {Any} - context
 * @returns {Object} - {results: Array, countExceeded: Bool, startingDate: Date}
 * @memberof Adapters.RepairShopAdapter
 */
function getWorkOrdersByShopIdForDev(username, token, shopCode, fromDate, context) {
	// Sanity check + Token validity
	if (checkUsernameAndToken(username, token, context).valid === false) {
		return checkUsernameAndToken(username, token, context).response;
	}
	
	// shopCode validity
	if (shopCode == undefined || shopCode.length == 0)  {
		return onRequestFailure("shopCode query argument invalid (not provided or incorrect format)", 107, null, context);
	}
	
	var sqlFromDate = dateToSQLDateTime(new Date(fromDate));
	
    // fromDate, toDate validity	
	if (!sqlFromDate) {
		return onRequestFailure("fromDate or toDate query argument invalid (not provided or incorrect format)", 107, data, context);
	}
	
	var maxResultsCount = 40000; 
	var countExceeded = false;
	var startingDate;
	var responseObject = {};
	
	// get result count first!
	
	var resultsCount = invokeSQL(getWorkOrderByShopIdCountForDevData, [sqlFromDate.toString(), shopCode], context);
	
	if (resultsCount.isSuccessful && resultsCount.resultSet) {
		if (resultsCount.resultSet.length == 0) {
			responseObject.results = [];
			return onRequestSuccess(responseObject, context);
		} else {
			if (resultsCount.resultSet[0].count > maxResultsCount) {
				countExceeded = true;
			} 
		}
	} else {
		return onRequestFailure("Work orders not found - SQL error", 100, data, context);
	}
	
	var data = invokeSQL(getWorkOrdersByShopIdQueryForDevData, [sqlFromDate.toString(), shopCode], context);
	
	if (data.isSuccessful && data.resultSet) {
		// result needs to be parsed to return only necessary fields
		responseObject.results = data.resultSet
		responseObject.countExceeded = countExceeded;
		
		if (countExceeded) {
			startingDate = responseObject.results[responseObject.results.length - 1]['repairDate'];
			responseObject.startingDate = startingDate;
		}
		
		return onRequestSuccess(responseObject, context);
	}
	
	return onRequestFailure("Work orders not found - SQL error", 100, data, context);
}

/**
 * @name getPartialWorkOrdersByShopId
 * @desc Not used
 * @memberof Adapters.RepairShopAdapter
 */
function getPartialWorkOrdersByShopId(username, shopCode, fromDate, idArray, limit, offset, context) {
	if (username == undefined || username.length == 0) {
		return onRequestFailure("User not provided", 105, null, context)
	}
	
	var sqlFromDate = dateToSQLDateTime(new Date(fromDate));
	
	var data = invokeSQL(getWorkOrderIdsByShopId, [sqlFromDate.toString(), shopCode], 'testContext');
	
	if (data.isSuccessful && data.resultSet) {
		var results = data.resultSet;
		var listIdArray = [];
		var stringIdList = "";
		
		for (var i = 0; i < results.length; i++) {
			listIdArray.push(results[i].i);
		}
		
		var partialIds = listIdArray.slice(0, 2000);
		var partialResult = invokeSQL(getWorkOrdersByIdList, partialIds, context);
		
		return {queryResults : partialResult.resultSet};
	}
}

/**
 * @name searchAllRepairShops
 * @desc Not used
 * @memberof Adapters.RepairShopAdapter
 */
function searchAllRepairShops(username, token, searchString, context) {
	// Sanity check + Token validity
	if (checkUsernameAndToken(username, token, context).valid === false) {
		return checkUsernameAndToken(username, token, context).response;
	}
	
	// searchString validity
	if (searchString == undefined || searchString.length < 2)  {
		return onRequestFailure("searchString query argument invalid (not provided or incorrect format)", 107, null, context);
	}
	
	var shopDescriptionSearch = "%" + searchString + "%";
	var shopCodeSearch = searchString + "%";
	
	var data = invokeSQL(searchAllRepairShopsQuery, [shopDescriptionSearch.toString(), shopCodeSearch.toString()], context);
	
	if (data.isSuccessful && data.resultSet) {
		// result needs to be parsed to return only necessary fields
		
		var results = data.resultSet;
		var parsedResults= [];
		
		for (var index in results) {
			var shop = results[index];
			
			parsedResults.push({
				location : shop.LOC_DESC,
				shopName : shop.SHOP_DESC,
				shopNo : shop.SHOP_CD,
				countryCode : shop.COUNTRY_CD
			})
		}
		
		return onRequestSuccess(parsedResults, context);
	}
	
	return onRequestFailure("SQL error", 100, data, context);
}

/**
 * @name getAllRepairShops
 * @desc Retrieves all shops from MESC1TS_SHOP table
 * @param {String} username
 * @param {String} token - no token auth used
 * @param {Any} - context
 * @returns {Object}
 * @memberof Adapters.RepairShopAdapter
 */
function getAllRepairShops(username, token, context) {
	// Sanity check + Token validity
	if (checkUsernameAndToken(username, token, context).valid === false) {
		return checkUsernameAndToken(username, token, context).response;
	}
	
	var data = invokeSQL(getAllRepairShopsQuery, [], context);
	
	if (data.isSuccessful && data.resultSet) {
		// result needs to be parsed to return only necessary fields
		
		var results = data.resultSet;
		var parsedResults= [];
		
		for (var index in results) {
			var shop = results[index];
			
			parsedResults.push({
				location : shop.LOC_DESC,
				name : shop.SHOP_DESC,
				code : shop.SHOP_CD,
				country : shop.COUNTRY_CD
			})
		}
		
		return onRequestSuccess(parsedResults, context);
	}
	
	return onRequestFailure("SQL error", 100, data, context);
}

/**
 * @name getAvailableRepairShops
 * @desc Same as getAllRepairShops
 * @param {String} username
 * @param {String} token - no token auth used
 * @param {Any} - context
 * @returns {Object}
 * @memberof Adapters.RepairShopAdapter
 */
function getAvailableRepairShops(username, token, context) {
	// Sanity check + Token validity
	if (checkUsernameAndToken(username, token, context).valid === false) {
		return checkUsernameAndToken(username, token, context).response;
	}
	
	var data = invokeSQL(getAvailableRepairShopsQuery, [], context);
	
	if (data.isSuccessful && data.resultSet) {
		// result needs to be parsed to return only necessary fields
		
		var results = data.resultSet;
		var parsedResults= [];
		
		for (var index in results) {
			var shop = results[index];
			
			parsedResults.push({
				location : shop.LOC_DESC,
				name : shop.SHOP_DESC,
				code : shop.SHOP_CD,
				country : shop.COUNTRY_CD
			})
		}
		
		return onRequestSuccess(parsedResults, context);
	}
	
	return onRequestFailure("SQL error", 100, data, context);
}

/**
 * @name getAvailableRepairShops
 * @desc Retrieves shops asscoiated with certificate from MESC1TS_SHOP table
 * for authentification groups AREA and COUNTRY
 * @param {String} username
 * @param {String} token - no token auth used
 * @param {String} certificateId
 * @param {Any} - context
 * @returns {Object}
 * @memberof Adapters.RepairShopAdapter
 */
function getShopsForCertificate(username, token, certificateId, context) {
	// Sanity check + Token validity
	
	if (typeof username  != 'string' || username.length == 0)  {
		return onRequestFailure("username query argument invalid (not provided or incorrect format)", 107, null, context);
	}
	
	if (typeof certificateId  == 'undefined' || certificateId.length == 0)  {
		return onRequestFailure("certificateId query argument invalid (not provided or incorrect format)", 107, null, context);
	}
	
	var data = invokeSQL(getShopsForCertificateQuery, [certificateId], context);
	
	if (data.isSuccessful && data.resultSet) {
		// result needs to be parsed to return only necessary fields
		
		var results = data.resultSet;
		
		return onRequestSuccess(results, context);
	}
	
	return onRequestFailure("SQL error", 100, data, context);
}

/*
 * Helper functions
 */

function checkUsernameAndToken(username, token, context) {
	return {valid: true};
	if (username == undefined || username.length == 0 || token == undefined || token.length == 0) {
		return {valid: false, response: onRequestFailure("User credentials not provided (username and/or token)", 105, null, context)}
	} 
	if (!userCredentialsValid(username, token, context)) {
		WL.Logger.info("user credentials invalid: " + username + ":" + token);
		
		// Either user does not exist or user token is invalid or expired - return an error
		return {valid: false, response: onRequestFailure("User does not exist or token expired or invalid (user credentials validity)",
				102, null, context)}
	}
	WL.Logger.info("user \"" + username + "\"token \"" + token + "\" are valid");
	return {valid: true};
}

function dateToSQLDateTime(date) {
	if (isNaN(date.getUTCFullYear()))
		return;
	
	return date.getUTCFullYear() + '-'
			+ ('00' + (date.getUTCMonth() + 1)).slice(-2) + '-'
			+ ('00' + date.getUTCDate()).slice(-2) + ' '
			+ ('00' + date.getUTCHours()).slice(-2) + ':'
			+ ('00' + date.getUTCMinutes()).slice(-2) + ':'
			+ ('00' + date.getUTCSeconds()).slice(-2);
}

function isNaN(variable) {
	return variable !== variable;
}
