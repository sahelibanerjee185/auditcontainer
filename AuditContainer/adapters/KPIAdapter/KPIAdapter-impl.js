/*
 * May, 19 2016
 * KPIAdapter-impl.js
 * Buzila Eugen
 */

/**
 * @category Database statements
 */

var getPassedInspectionsyQuery = WL.Server.createSQLStatement(
	"SELECT (Count(*)) AS passedCount FROM MESC1TS_AUDIT_HISTORY WHERE AUDITOR= ? AND AUDIT_RESULT = 1 AND "+
	"AUDIT_DATE between ? and ?"
);

var getRejectedInspectionsyQuery = WL.Server.createSQLStatement(
	"SELECT (Count(*)) AS rejectedCount FROM MESC1TS_AUDIT_HISTORY WHERE AUDITOR= ? AND AUDIT_RESULT = 0 AND "+
	"AUDIT_DATE between ? and ?"
);

var getRejectedInspectionsForMonthQuery = WL.Server.createSQLStatement(
	"SELECT (Count(*)) AS rejectedCountPerMonth FROM MESC1TS_AUDIT_HISTORY WHERE AUDITOR= ? AND AUDIT_RESULT = 0 AND  "+
	"MONTH(AUDIT_DATE) = ? AND YEAR(AUDIT_DATE) = ?"
);

var getPassedInspectionsForMonthQuery = WL.Server.createSQLStatement(
	"SELECT (Count(*)) AS passedCountPerMonth FROM MESC1TS_AUDIT_HISTORY WHERE AUDITOR= ? AND AUDIT_RESULT = 1 AND  "+
	"MONTH(AUDIT_DATE) = ? AND YEAR(AUDIT_DATE) = ?"
);
	
var getAllInspectionsForMonthQuery = WL.Server.createSQLStatement(
	"SELECT (Count(*)) AS allCountPerMonth FROM MESC1TS_AUDIT_HISTORY WHERE AUDITOR= ? AND  "+
	"MONTH(AUDIT_DATE) = ? AND YEAR(AUDIT_DATE) = ?"
);

var getAllShopsQuery = WL.Server.createSQLStatement(
	"SELECT DISTINCT  H.SHOP_CD, S.SHOP_DESC, L.LOC_DESC FROM MESC1TS_AUDIT_HISTORY H "+
	"LEFT JOIN MESC1TS_SHOP S on H.SHOP_CD = S.SHOP_CD "+
	"LEFT JOIN MESC1TS_LOCATION L ON S.LOC_CD=L.LOC_CD "+
	"WHERE H.AUDITOR = ? AND H.AUDIT_DATE between ? and ?"	
);

var getInspectionResultsForPastYearQuery = WL.Server.createSQLStatement(
	"BEGIN " +
	"DECLARE @auditor VARCHAR(50) SET @auditor = ? " +
	"DECLARE @date VARCHAR(20) SET @date = ? " +
	"END " +
	"SELECT DISTINCT H.AUDIT_RESULT as result, YEAR(H.AUDIT_DATE) as year, MONTH(H.AUDIT_DATE) as month, count(*) as inspectionCount FROM MESC1TS_AUDIT_HISTORY H " +
	"WHERE H.AUDITOR = @auditor AND H.AUDIT_DATE between DATEADD(YEAR, -1, @date) and @date " +
	"GROUP BY H.AUDIT_RESULT, YEAR(H.AUDIT_DATE), MONTH(H.AUDIT_DATE)"
);
	
/*
 * Failed Data
 */

var getFailedSparePartNumberQuery = WL.Server.createSQLStatement(
	"SELECT (Count(*)) AS sparePartNumber FROM MESC1TS_AUDIT_RESULTS R "+
	"LEFT JOIN MESC1TS_AUDIT_HISTORY H " +
	"ON R.WO_ID = H.WO_ID AND R.VISIT_ID = H.VISIT_ID AND R.AUDITOR = H.AUDITOR " + 
	"WHERE R.AUDITOR= ? AND " +
	"MONTH(H.AUDIT_DATE) = ? AND YEAR(H.AUDIT_DATE) = ? AND " +
	"R.PRE_EQPNO<>R.POST_EQPNO  AND R.SHOP_CD = ?"
);

var getFailedRepairCodeQuery = WL.Server.createSQLStatement(
	"SELECT (Count(*)) AS repairCodeNumber FROM MESC1TS_AUDIT_RESULTS R "+
	"LEFT JOIN MESC1TS_AUDIT_HISTORY H " +
	"ON R.WO_ID = H.WO_ID AND R.VISIT_ID = H.VISIT_ID AND R.AUDITOR = H.AUDITOR " + 
	"WHERE R.AUDITOR= ? AND " +
	"MONTH(H.AUDIT_DATE) = ? AND YEAR(H.AUDIT_DATE) = ? AND " +
	"R.PRE_REPAIR_CD<>R.POST_REPAIR_CD  AND R.SHOP_CD = ?"
);

var getFailedRepairLocCdQuery = WL.Server.createSQLStatement(
	"SELECT (Count(*)) AS repairLocCodeNumber FROM MESC1TS_AUDIT_RESULTS R "+
	"LEFT JOIN MESC1TS_AUDIT_HISTORY H " +
	"ON R.WO_ID = H.WO_ID AND R.VISIT_ID = H.VISIT_ID AND R.AUDITOR = H.AUDITOR " + 
	"WHERE R.AUDITOR= ? AND " +
	"MONTH(H.AUDIT_DATE) = ? AND YEAR(H.AUDIT_DATE) = ? AND " +
	"R.PRE_REPAIR_LOC_CD<>R.POST_REPAIR_LOC_CD AND R.SHOP_CD = ?"
);

var getFailedDamageCodeQuery = WL.Server.createSQLStatement(
	"SELECT (Count(*)) AS damageCodeNumber FROM MESC1TS_AUDIT_RESULTS R "+
	"LEFT JOIN MESC1TS_AUDIT_HISTORY H " +
	"ON R.WO_ID = H.WO_ID AND R.VISIT_ID = H.VISIT_ID AND R.AUDITOR = H.AUDITOR " + 
	"WHERE R.AUDITOR= ? AND " +
	"MONTH(H.AUDIT_DATE) = ? AND YEAR(H.AUDIT_DATE) = ? AND " +
	"R.PRE_DAMAGE_CD<>R.POST_DAMAGE_CD AND R.SHOP_CD = ?"
);

var getFailedPcsQuery = WL.Server.createSQLStatement(
	"SELECT (Count(*)) AS pcsNumber FROM MESC1TS_AUDIT_RESULTS R "+
	"LEFT JOIN MESC1TS_AUDIT_HISTORY H " +
	"ON R.WO_ID = H.WO_ID AND R.VISIT_ID = H.VISIT_ID AND R.AUDITOR = H.AUDITOR " + 
	"WHERE R.AUDITOR= ? AND " +
	"MONTH(H.AUDIT_DATE) = ? AND YEAR(H.AUDIT_DATE) = ? AND " +
	"R.PRE_QTY_PARTS<>R.POST_QTY_PARTS AND R.SHOP_CD = ?"	
);

var getFailedTpiQuery = WL.Server.createSQLStatement(
	"SELECT (Count(*)) AS tpiNumber FROM MESC1TS_AUDIT_RESULTS R "+
	"LEFT JOIN MESC1TS_AUDIT_HISTORY H " +
	"ON R.WO_ID = H.WO_ID AND R.VISIT_ID = H.VISIT_ID AND R.AUDITOR = H.AUDITOR " + 
	"WHERE R.AUDITOR= ? AND " +
	"MONTH(H.AUDIT_DATE) = ? AND YEAR(H.AUDIT_DATE) = ? AND " +
	"R.PRE_TPI<>R.POST_TPI AND R.SHOP_CD = ?"	
);

var getRejectedInspectionsForFailQuery = WL.Server.createSQLStatement(
	"SELECT (Count(*)) AS rejectedCount FROM MESC1TS_AUDIT_HISTORY WHERE AUDITOR= ? AND AUDIT_RESULT = 0 AND "+
	"AUDIT_DATE between ? and ? AND SHOP_CD= ?"
);

var getAllInspectionsForShopQuery = WL.Server.createSQLStatement(
	"SELECT (Count(*)) AS allCount FROM MESC1TS_AUDIT_HISTORY WHERE AUDITOR= ? AND "+
	"AUDIT_DATE between ? and ? AND SHOP_CD= ?"
);

var getAllFailledWOForShopQuery = WL.Server.createSQLStatement(
	"SELECT WO_ID, AUDIT_DATE, VISIT_DESC FROM MESC1TS_AUDIT_HISTORY "+
	"WHERE SHOP_CD=? AND AUDITOR=? AND AUDIT_RESULT='0' AND AUDIT_DATE between ? and ?"
);

var getAllPartsForWOQuery = WL.Server.createSQLStatement(
	"SELECT R.WO_ID AS woNo, R.PRE_EQPNO AS preSparePartNumber, "+
	"R.POST_EQPNO AS postsparePartNumber, "+
	"R.PRE_QTY_PARTS AS prePcsNumber, "+
	"R.POST_QTY_PARTS AS postPcsNumber, "+
	"R.PRE_REPAIR_CD AS preRepairCodeNumber, "+
	"R.POST_REPAIR_CD AS postRepairCodeNumber, "+
	"R.PRE_REPAIR_LOC_CD AS  preRepairLocCodeNumber, "+
	"R.POST_REPAIR_LOC_CD AS  postRepairLocCodeNumber, "+
	"R.PRE_DAMAGE_CD AS preDamageCodeNumber, "+
	"R.POST_DAMAGE_CD AS postDamageCodeNumber, "+
	"R.PRE_TPI AS preTpi, "+
	"R.POST_TPI AS postTpi "+
	"FROM MESC1TS_AUDIT_RESULTS R LEFT JOIN MESC1TS_AUDIT_HISTORY H " +
	"ON R.WO_ID = H.WO_ID AND R.VISIT_ID = H.VISIT_ID AND R.AUDITOR = H.AUDITOR " + 
	"WHERE R.WO_ID=? AND R.AUDITOR=? AND CONVERT(VARCHAR(25), H.AUDIT_DATE, 126) LIKE ?"
);

function onRequestSuccess(data, context) {
	return {
		isSuccessful : true,
		status : 200,
		errorCode : 0,
		errorMsg : null,
		invocationContext : context,
		invocationResult : data
	}
}

function onRequestFailure(errorMsg, errorCode, data, context) {
	return {
		isSuccessful : false,
		status : 200,
		errorCode : errorCode,
		errorMsg : errorMsg,
		invocationContext : context,
		data : data
	}
}

function invokeSQL(statement, parameters, context) {
	try {
		// Invoke SQL statement
		var returnData = WL.Server.invokeSQLStatement({
			preparedStatement : statement,
			parameters : parameters
		});
		
		// Return SQL statement data
		return returnData;

	} catch (error) {
		return onRequestFailure("Database SQL execution error thrown", 101, error, context);
	}
}

function userCredentialsValid(username, token, context) {

	// Verify user and token in our database
	var invocationData = {
			adapter : "DBAdapterContainer",
			procedure : "verifyToken",
			parameters : [username, token, (context != undefined ? context : null)]
	};
	var data = WL.Server.invokeProcedure(invocationData);

	// Check returned data
	if (data != undefined && data.invocationResult != undefined) {
		
		// Return token validity
		return data.invocationResult.tokenValid === true;
	}
	
	// Incorrect data - possibly user does not exist in the database
	return false;
}

/**
 * @name getInspectionResultsForPastYear
 * @desc Retrieves information for the past year regarding inspections,
 * for each month: count of inspections either failed or successful
 * and audit status
 * @param {String} username
 * @param {String} token - no token auth used
 * @param {Date} today - max date of inspection results to be retrieved
 * @param {Any} context - param that will be returned in the response
 * @returns {Object} - An array of inspection info for each month
 * @memberof Adapters.KPIAdapter
 */
function getInspectionResultsForPastYear(username, token, today, context) {
	if (!username || username.length == 0) {
		return onRequestFailure("username query argument invalid (not provided or incorrect format)", 107, null, context);
	}
	
	var data = invokeSQL(getInspectionResultsForPastYearQuery, [username, today], context);
	
	if (data && data.isSuccessful && data.resultSet) {
		return onRequestSuccess(data.resultSet, context);
	}
	
	return onRequestFailure("Database SQL execution error thrown", 101, data, context);
}

/**
 * @name getPassedInspections
 * @desc Retrieves total count of passed inspections
 * , total count of rejected inspections
 * and total count of all inspections
 * @param {String} username
 * @param {String} token - no token auth used
 * @param {Date} today - max date of inspections to be retrieved
 * @param {Date} oneYearAgo - min date of inspections to be retrieved
 * @param {Any} context - param that will be returned in the response
 * @returns {Object} - {passedCount : {Number},
 * rejectedCount : {Number}, totalCount : {Number}}
 * @memberof Adapters.KPIAdapter
 */
function getPassedInspections(username, token, today, oneYearAgo, context) {
	// Sanity check + Token validity	
	if (checkUsernameAndToken(username, token, context).valid === false) {
		return checkUsernameAndToken(username, token, context).response;
	}
	
	var data = {
			passedCount : "",
			rejectedCount : "",
			totalCount : ""
	};
	
	var dataPassed = invokeSQL(getPassedInspectionsyQuery, [username,oneYearAgo,today], context);
	if (dataPassed && dataPassed.isSuccessful) {
		data.passedCount = dataPassed.resultSet[0].passedCount;
		
		var dataRejected = invokeSQL(getRejectedInspectionsyQuery, [username,oneYearAgo,today], context);
		if (dataRejected && dataRejected.isSuccessful) {
			data.rejectedCount = dataRejected.resultSet[0].rejectedCount;
			data.totalCount = parseInt(data.passedCount) + parseInt(data.rejectedCount);
			
			return onRequestSuccess(data, context);
		}
	}
	
	return onRequestFailure("Database SQL execution error thrown", 101, data, context);
}

/**
 * @name getAllShops
 * @desc Retrieves all shops for which username
 * has made at least one successful visit
 * @param {String} username
 * @param {String} token - no token auth used
 * @param {Date} today - max date of inspections to be retrieved
 * @param {Date} oneYearAgo - min date of inspections to be retrieved
 * @param {Any} context - param that will be returned in the response
 * @returns {Object} - an array of shops
 * @memberof Adapters.KPIAdapter
 */
function getAllShops(username, token, today, oneYearAgo, context) {
	// Sanity check + Token validity	
	if (checkUsernameAndToken(username, token, context).valid === false) {
		return checkUsernameAndToken(username, token, context).response;
	}
	 
	var data = invokeSQL(getAllShopsQuery, [username,oneYearAgo,today], context);
	if (data && data.isSuccessful) {
		return onRequestSuccess(data, context);
	}
	
	return onRequestFailure("Database SQL execution error thrown", 101, data, context);
}

/**
 * @name getRejectedInspectionsForMonth
 * @desc Not used
 * @memberof Adapters.KPIAdapter
 */
function getRejectedInspectionsForMonth(username, token, month, actualYear, context) {
	// Sanity check + Token validity	
	if (checkUsernameAndToken(username, token, context).valid === false) {
		return checkUsernameAndToken(username, token, context).response;
	}
	
	var data = [];
	for(index = month, a=0; a < 12; index--,a++){
		if(index == 0) {
			index = 12;
			actualYear--;
		}
		
		var result = invokeSQL(getRejectedInspectionsForMonthQuery, [username,index,actualYear], context);
		if (result && result.isSuccessful) {
			data.unshift(result.resultSet[0].rejectedCountPerMonth)
			
			if(a == 11) return onRequestSuccess(data, context);
		}
	}
	return onRequestFailure("Database SQL execution error thrown", 101, data, context);
}

/**
 * @name getPassedInspectionsForMonth
 * @desc Not used
 * @memberof Adapters.KPIAdapter
 */
function getPassedInspectionsForMonth(username, token, month, actualYear, context) {
	// Sanity check + Token validity	
	if (checkUsernameAndToken(username, token, context).valid === false) {
		return checkUsernameAndToken(username, token, context).response;
	}
	
	var data = [];
	for(index = month, a=0; a < 12; index--,a++){
		if(index == 0) {
			index = 12;
			actualYear--;
		}
		
		var result = invokeSQL(getPassedInspectionsForMonthQuery, [username,index,actualYear], context);
		if (result && result.isSuccessful) {
			data.unshift(result.resultSet[0].passedCountPerMonth)
			
			if(a == 11) return onRequestSuccess(data, context);
		}
	}
	return onRequestFailure("Database SQL execution error thrown", 101, data, context);
}

/**
 * @name getAllInspectionsForMonth
 * @desc Not used
 * @memberof Adapters.KPIAdapter
 */
function getAllInspectionsForMonth(username, token, month, actualYear, context) {
	// Sanity check + Token validity	
	if (checkUsernameAndToken(username, token, context).valid === false) {
		return checkUsernameAndToken(username, token, context).response;
	}
	
	var data = [];
	for(index = month, a=0; a < 12; index--,a++){
		if(index == 0) {
			index = 12;
			actualYear--;
		}
		
		var result = invokeSQL(getAllInspectionsForMonthQuery, [username,index,actualYear], context);
		if (result && result.isSuccessful) {
			data.unshift(result.resultSet[0].allCountPerMonth)
			
			if(a == 11) return onRequestSuccess(data, context);
		}
	}
	return onRequestFailure("Database SQL execution error thrown", 101, data, context);
}

/**
 * @name getFailedData
 * @desc Populates and returns an object which 
 * has information on failed inspections count and 
 * total inspections count for a shop.
 * count for each month for each possible editable metric of a work order
 * for which work orders have been rejected,
 * repair code, part code, damage code, tpi, pieces, repair loc code.
 * @param {String} username
 * @param {String} token - no token auth used
 * @param {Number} month - max date of inspections to be retrieved
 * @param {Number} actualYear - min date of inspections to be retrieved
 * @param {Date} today
 * @param {Date} tomorow
 * @param {Date} oneYearAgo - date from one year ago to be passed
 * @param {String} shopCode - shop to get associated data from
 * @param {Any} context - param that will be returned in the response
 * @returns {Object}
 * @memberof Adapters.KPIAdapter
 */
function getFailedData(username, token, month, actualYear, today,tomorow,oneYearAgo, shopCode ,context) {
	// Sanity check + Token validity	
	if (checkUsernameAndToken(username, token, context).valid === false) {
		return checkUsernameAndToken(username, token, context).response;
	}
	
	var obj = {
			sparePartNumber 	: [],
			repairCodeNumber 	: [],
			repairLocCodeNumber	: [],
			damageCodeNumber 	: [], 
			pcsNumber 			: [],
			tpiNumber			: [],
			failedCount 		: null,
			totalCount			: null
	}
	
	var query = [ getFailedRepairCodeQuery,getFailedSparePartNumberQuery,getFailedRepairLocCdQuery, getFailedDamageCodeQuery, getFailedPcsQuery,getFailedTpiQuery];
	var type = ['repairCodeNumber','sparePartNumber','repairLocCodeNumber','damageCodeNumber','pcsNumber','tpiNumber'];
	
	for(index1 in type){
		var data = [];
		var year = actualYear;
		for(index = month, a=0; a < 12; index--,a++){     
			if(index == 0) {
				index = 12;
				year--;
			}
			
			var result = invokeSQL(query[index1], [username,index,year,shopCode], context);
			if (result && result.isSuccessful) {
				data.unshift(result.resultSet[0][type[index1]])
				if(a == 11) {
					obj[type[index1]] = data; 
				}
			}
		}
	}
	
	var dataRejected = invokeSQL(getRejectedInspectionsForFailQuery, [username,oneYearAgo,tomorow,shopCode], context);
	if (dataRejected && dataRejected.isSuccessful) 
		obj.failedCount = dataRejected.resultSet[0].rejectedCount;
	
	var dataAll = invokeSQL(getAllInspectionsForShopQuery, [username,oneYearAgo,tomorow,shopCode], context);
	if (dataAll && dataAll.isSuccessful) 
		obj.totalCount = dataAll.resultSet[0].allCount;

	obj.dateTomorow = tomorow;
	return onRequestSuccess(obj, context);
	
	return onRequestFailure("Database SQL execution error thrown", 101, data, context);
}

/**
 * @name getAllFailledWOForShop
 * @desc Get all failed work orders for a specific shop for the past year
 * @param {String} username
 * @param {String} token - no token auth used
 * @param {Date} today
 * @param {Date} oneYearAgo - date from one year ago to be passed
 * @param {String} shop - shop to get associated data from
 * @param {Any} context - param that will be returned in the response
 * @returns {Object} - an array of shops
 * @memberof Adapters.KPIAdapter
 */
function getAllFailledWOForShop(username, token, today, oneYearAgo,shop, context) {
	// Sanity check + Token validity	
	if (checkUsernameAndToken(username, token, context).valid === false) {
		return checkUsernameAndToken(username, token, context).response;
	}
	
	var data = invokeSQL(getAllFailledWOForShopQuery, [shop,username,oneYearAgo,today], context);
	if (data && data.isSuccessful) {
		return onRequestSuccess(data.resultSet, context);
	}
	
	return onRequestFailure("Database SQL execution error thrown", 101, data, context);
}

/**
 * @name getAllPartsForWO
 * @desc Get all repairs and parts associated with a work order number
 * @param {String} username
 * @param {String} token - no token auth used
 * @param {String} woNo - work order id
 * @param {Date} auditDate - audit date
 * @param {Any} context - param that will be returned in the response
 * @returns {Object} - an array of repairs and parts
 * @memberof Adapters.KPIAdapter
 */
function getAllPartsForWO(username, token, woNo, auditDate, context) {
	// Sanity check + Token validity	
	if (checkUsernameAndToken(username, token, context).valid === false) {
		return checkUsernameAndToken(username, token, context).response;
	}
	
	var data = invokeSQL(getAllPartsForWOQuery, [woNo, username, auditDate], context);
	if (data && data.isSuccessful) {
		return onRequestSuccess(data.resultSet, context);
	}
	
	return onRequestFailure("Database SQL execution error thrown", 101, data, context);
}

function checkUsernameAndToken(username, token, context) {
	return {valid: true};
	if (username == undefined || username.length == 0 || token == undefined || token.length == 0) {
		return {valid: false, response: onRequestFailure("User credentials not provided (username and/or token)", 105, null, context)}
	} 
	if (!userCredentialsValid(username, token, context)) {
		WL.Logger.info("user credentials invalid: " + username + ":" + token);
		
		// Either user does not exist or user token is invalid or expired - return an error
		return {valid: false, response: onRequestFailure("User does not exist or token expired or invalid (user credentials validity)",
				102, null, context)}
	}
	WL.Logger.info("user \"" + username + "\"token \"" + token + "\" are valid");
	return {valid: true};
}

