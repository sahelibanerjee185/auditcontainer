/*
 * Dec, 9 2015
 * LoginAdapter-impl.js
 * Preda Alin Cristinel
 */

/* Error Codes:
 * 
 * 0   - No error
 * 99  - Unknown error
 * 100 - Database SQL statement execution error
 * 101 - Database SQL execution error thrown (couldn't connect to the database)
 * 102 - Invalid or expired token
 * 103 - Username not provided
 * 104 - Token not provided
 * 105 - User credentials not provided (username and/or token)
 * 106 - Password not provided
 * 107 - Query arguments invalid (not provided or incorrect format)
 * 108 - Could not generate a new token (database request failed)
 * 109 - Login failure (incorrect username or password)
 * 
 */

/* Login Result Codes
 * 
 * 200 - success
 * 201 - failure
 * 
 */

/**
 * @category Database statements
 */

var runLoginProcedure = WL.Server.createSQLStatement(
	"DECLARE @outMessage varchar(100) \n" +
	"DECLARE @outMessageCode varchar(3) \n" +
	"EXEC [dbo].[checkUserCredentials]" +
	"@UserId = ?," +
	"@pass_word = ?," +
	"@outMessageCode = @outMessageCode OUTPUT," +
	"@outMessage = @outMessage OUTPUT \n" + 
	"SELECT @outMessageCode AS \"resultCode\""
);

var runLoginCertificateProcedure = WL.Server.createSQLStatement(
	"SELECT DISTINCT (Count(*)) AS certificateCount " +
	"FROM SEC_AUTHGROUP_USER A " +
	"LEFT JOIN SEC_USER U " +
	"ON U.USER_ID = A.USER_ID " +
	"WHERE U.LOGIN = ? AND A.AUTHGROUP_ID IN ('3','4') AND U.ACTIVE_STATUS = 'Y'"
);

var getUserIdQuery = WL.Server.createSQLStatement(
	"SELECT USER_ID AS userId " +
	"FROM SEC_USER WHERE LOGIN = ? AND ACTIVE_STATUS = 'Y'"
);

//var runLoginProcedure = WL.Server.createSQLStatement(
//	"BEGIN " +
//	"IF EXISTS(SELECT * FROM USER_LOGIN WHERE User_id = ? AND Password = ?) " +
//	"BEGIN " +
//	"SELECT 200 AS 'resultCode' " +
//	"END " + 
//	"ELSE " +
//	"BEGIN " +
//	"SELECT 201 AS 'resultCode' " +
//	"END " +
//	"END"	
//);
	
function onRequestSuccess(data, context) {
	return {
		isSuccessful : true,
		status : 200,
		errorCode : 0,
		errorMsg : null,
		invocationContext : context,
		invocationResult : data
	}
}

function onRequestFailure(errorMsg, errorCode, data, context) {
	return {
		isSuccessful : false,
		status : 200,
		errorCode : errorCode,
		errorMsg : errorMsg,
		invocationContext : context,
		data : data
	}
}

function invokeSQL(statement, parameters, context) {
	try {
		// Invoke SQL statement
		var returnData = WL.Server.invokeSQLStatement({
			preparedStatement : statement,
			parameters : parameters
		});
		
		// Return SQL statement data
		return returnData;

	} catch (error) {
		return onRequestFailure("Database SQL execution error thrown", 101, error, context);
	}
}

function backendIndentityCheck(username, password, context) {
	return invokeSQL(runLoginProcedure, [username, password], context);
}

function backendCertificateCheck(certificateId,context){
	return invokeSQL(runLoginCertificateProcedure, [certificateId], context);
}

function setNewToken(username, context) {

	// Verify user token in our database
	var invocationData = {
			adapter : "DBAdapterContainer",
			procedure : "setNewToken",
			parameters : [username, (context != undefined ? context : null)]
	};
	var data = WL.Server.invokeProcedure(invocationData);

	// Check returned data
	if (data != undefined && data.invocationResult != undefined && data.invocationResult.token != undefined &&
			data.invocationResult.token.length > 0) {
		return data.invocationResult.token;
	}
	return null;
}

function userCredentialsValid(username, token, context) {

	// Verify user and token in our database
	var invocationData = {
			adapter : "DBAdapterContainer",
			procedure : "verifyToken",
			parameters : [username, token, (context != undefined ? context : null)]
	};
	var data = WL.Server.invokeProcedure(invocationData);

	// Check returned data
	if (data != undefined && data.invocationResult != undefined) {
		
		// Return token validity
		return data.invocationResult.tokenValid === true;
	}
	
	// Incorrect data - possibly user does not exist in the database
	return false;
}

/*
 * Adapter methods
 */

function login(username, password, context){
	// Sanity check
	
	// 1. Username
	if (username == undefined || username.length == 0) {
		return onRequestFailure("Username not provided", 103, null, context);
	}
	
	// 2. Password
	if (password == undefined || password.length == 0) {
		return onRequestFailure("Password not provided", 106, null, context);
	}
	
	// Verify user identity with backend
	
	var loginResult = backendIndentityCheck(username, password, context);
	
	if (requestSuccessful(loginResult) && loginResult.resultSet[0] && loginResult.resultSet[0].resultCode == 201) {
		// Invalid Credentials
		return onRequestFailure("Login failure (incorrect username or password)", 109, null, context);
	}
	
	if (!requestSuccessful(loginResult) && loginResult.errors && loginResult.errors[0]) {
		// Running SQL statement failed
		WL.Logger.info("Error running checkUserCredentials procedure - " + loginResult.errors[0]);
		
		return onRequestFailure(loginResult.errors[0], 101, loginResult, context);
	}
		
	if (requestSuccessful(loginResult) && loginResult.resultSet[0] && loginResult.resultSet[0].resultCode == 200) {
		// Login was successful
		
		// Set new token
		var token = setNewToken(username, context);
		
		// Check if token is valid and not NULL
		if (token == null || token == undefined || token.length <= 0) {
			return onRequestFailure("Could not generate a new token (database request failed)", 108, null, context);
		}
		
		return onRequestSuccess({
			token : token,
			message : "Login successful",
		}, context);
	}
	
	// Undocumented Error
	
	return onRequestFailure("Database SQL execution error thrown", 101, loginResult, context);
}

function verifyIdentity(username, token, context){
	// Sanity check
	// Username and token
	if (username == undefined || username.length == 0 ||
			token == undefined || token.length == 0) {
		return onRequestFailure("User credentials not provided (username and/or token)", 105, {
			indentityVerified : false
		}, context);
	}
	
	// Check user credentials validity
	if (!userCredentialsValid(username, token, context)) {
		WL.Logger.info("user credentials invalid: " + username + ":" + token);
		
		// Either user does not exist or user token is invalid or expired - return an error
		return onRequestFailure("User does not exist or token expired or invalid (user credentials validity)",
				102, {
					indentityVerified : false
				}, context);
	}
	
	return onRequestSuccess({
		indentityVerified : true
	})
}

function loginWithCertificate(certificateId, context){
	
	if(typeof certificateId !== 'string' || certificateId.length == 0 ){
		return onRequestFailure("User certificate not provided", 105, {
			indentityVerified : false
		}, context);
	}
	
	var loginResult = backendCertificateCheck(certificateId,context);
	
	
	if (loginResult.isSuccessful && loginResult.resultSet) {
		// result needs to be parsed to return only necessary fields
		var results = loginResult.resultSet;
		
		if (results.length > 0) {
			return onRequestSuccess(results[0], context);
		}
	}
	
	// Undocumented Error
	
	return onRequestFailure("Database SQL execution error thrown", 101, loginResult, context);
}

function getUsername(certificateId, context) {
	if (typeof certificateId !== 'string' || certificateId.length == 0 ){
		return onRequestFailure("User certificate not provided", 105, {
			indentityVerified : false
		}, context);
	}
	
	var getUserResult = invokeSQL(getUserIdQuery, [certificateId], context);
	
	if (getUserResult.isSuccessful && getUserResult.resultSet) {
		var results = getUserResult.resultSet;
		
		if (results.length > 0) {
			return onRequestSuccess(results[0].userId, context);
		} else {
			return onRequestSuccess(null, context);
		}
	}
	
	// Undocumented Error
	
	return onRequestFailure("Database SQL execution error thrown", 101, getUserResult, context);
}

/*
 * Helper methods
 */

function requestSuccessful(data) {
	return !!(data && data.isSuccessful && data.resultSet);
}


