var beginTranProc = WL.Server.createSQLStatement(
	"BEGIN TRAN"
);


var comitTranProc = WL.Server.createSQLStatement(
	"COMMIT TRAN"
);


var rollBackProc = WL.Server.createSQLStatement(
	"ROLLBACK TRAN"
);

var savePhotosQuery = WL.Server.createSQLStatement(
	"INSERT INTO MESC1TS_AUDIT_PHOTOS "+
	"(PATH,NAME,DATE,VISIT_ID,WO_ID,AUDITOR,EXTRA_INFO) "+
	"VALUES(?,?,?,?,?,?,?)"	
);


var saveFinishedWorkOrderQueryBeta = WL.Server.createSQLStatement(
	"BEGIN " +
	"DECLARE @auditor VARCHAR(50) SET @auditor = ? " +
	"DECLARE @auditDate DATETIME SET @auditDate = ? " +
	"DECLARE @visitId VARCHAR(50) SET @visitId = ? " +
	"DECLARE @archiveId VARCHAR(50) SET @archiveId = ? " +
	"DECLARE @workOrderNo INT SET @workOrderNo = ? " + 
	"DECLARE @shopCode VARCHAR(50) SET @shopCode = ? " +
	"DECLARE @auditResult INT SET @auditResult = ? " +
	"DECLARE @visitDescription VARCHAR(50) SET @visitDescription = ? " +
	"DECLARE @visitComment VARCHAR(255) SET @visitComment = ? " +
	"DECLARE @certificateId VARCHAR(30) SET @certificateId = ? " +
	"DECLARE @mercStatus VARCHAR(20) SET @mercStatus = ? " +
	"END " +
	"INSERT INTO MESC1TS_AUDIT_HISTORY " +
	"(WO_ID, " +
	"SHOP_CD, " +
	"AUDIT_DATE, " +
	"AUDITOR, " +
	"AUDIT_RESULT, " +
	"VISIT_DESC, " +
	"VISIT_ID, ARCHIVE_ID , COMMENT, CERTIFICATE_ID, MERC_STATUS) " +
	"VALUES(@workOrderNo, " +
	"@shopCode, " +
	"@auditDate, " +
	"@auditor, " +
	"@auditResult, " +
	"@visitDescription, " +
	"@visitId, @archiveId, @visitComment, @certificateId, @mercStatus)"
);

var saveFinishedWorkOrderQueryPostRepair = WL.Server.createSQLStatement(
	"BEGIN " +
	"DECLARE @auditor VARCHAR(50) SET @auditor = ? " +
	"DECLARE @auditDate DATETIME SET @auditDate = ? " +
	"DECLARE @visitId VARCHAR(50) SET @visitId = ? " +
	"DECLARE @archiveId VARCHAR(50) SET @archiveId = ? " +
	"DECLARE @workOrderNo INT SET @workOrderNo = ? " + 
	"DECLARE @shopCode VARCHAR(50) SET @shopCode = ? " +
	"DECLARE @auditResult INT SET @auditResult = ? " +
	"DECLARE @visitDescription VARCHAR(50) SET @visitDescription = ? " +
	"DECLARE @visitComment VARCHAR(255) SET @visitComment = ? " +
	"DECLARE @certificateId VARCHAR(30) SET @certificateId = ? " +
	"DECLARE @mercStatus VARCHAR(20) SET @mercStatus = ? " +
	"DECLARE @statusCode INT SET @statusCode = ? " +
	"DECLARE @repairQuality FLOAT SET @repairQuality = ? " +
	"END " +
	"INSERT INTO MESC1TS_AUDIT_HISTORY " +
	"(WO_ID, " +
	"SHOP_CD, " +
	"AUDIT_DATE, " +
	"AUDITOR, " +
	"AUDIT_RESULT, " +
	"VISIT_DESC, " +
	"VISIT_ID, ARCHIVE_ID , COMMENT, CERTIFICATE_ID, MERC_STATUS, STATUS_CODE, REPAIR_QUALITY) " +
	"VALUES(@workOrderNo, " +
	"@shopCode, " +
	"@auditDate, " +
	"@auditor, " +
	"@auditResult, " +
	"@visitDescription, " +
	"@visitId, @archiveId, @visitComment, @certificateId, @mercStatus, @statusCode, @repairQuality)"
);

var saveFinishedWorkOrderPartQuery = WL.Server.createSQLStatement(
	"BEGIN " +
	"DECLARE @auditor VARCHAR(50) SET @auditor = ? " +
	"DECLARE @auditDate DATETIME SET @auditDate = ? " +
	"DECLARE @visitId VARCHAR(50) SET @visitId = ? " +
	"DECLARE @workOrderNo INT SET @workOrderNo = ? " +
	"DECLARE @shopCode VARCHAR(50) SET @shopCode = ? " +
	"DECLARE @auditResult INT SET @auditResult = ? " +
	"DECLARE @preEqpNo VARCHAR(50) SET @preEqpNo = ? " +
	"DECLARE @postEqpNo VARCHAR(50) SET @postEqpNo = ? " +
	"DECLARE @preQtyParts VARCHAR(50) SET @preQtyParts = ? " +
	"DECLARE @postQtyParts VARCHAR(50) SET @postQtyParts = ? " +
	"DECLARE @preRepairCode VARCHAR(50) SET @preRepairCode = ? " +
	"DECLARE @postRepairCode VARCHAR(50) SET @postRepairCode = ? " +
	"DECLARE @preRepairLocCode VARCHAR(50) SET @preRepairLocCode = ? " +
	"DECLARE @postRepairLocCode VARCHAR(50) SET @postRepairLocCode = ? " +
	"DECLARE @preDamageCode VARCHAR(50) SET @preDamageCode = ? " +
	"DECLARE @postDamageCode VARCHAR(50) SET @postDamageCode = ? " +
	"DECLARE @preTPI VARCHAR(50) SET @preTPI = ? " +
	"DECLARE @postTPI VARCHAR(50) SET @postTPI = ? " +
	"DECLARE @woRepair INT SET @woRepair = ? " +
	"DECLARE @manHrs VARCHAR(50) SET @manHrs = ? " +
	"DECLARE @materialCost VARCHAR(50) SET @materialCost = ? " +
	"DECLARE @partCost VARCHAR(50) SET @partCost = ? " +
	"DECLARE @totalPerCode VARCHAR(50) SET @totalPerCode = ? " +
	"DECLARE @partIndex INT SET @partIndex = ? " +
	"DECLARE @repairIndex INT SET @repairIndex = ? " +
	"DECLARE @repairDesc VARCHAR(50) SET @repairDesc = ? " +
	"DECLARE @partDesc VARCHAR(50) SET @partDesc = ? " +
	"END " +
	"INSERT INTO MESC1TS_AUDIT_RESULTS " +
	"(WO_ID, " +
	"SHOP_CD, " +
	"AUDIT_DATE, " +
	"AUDITOR, " +
	"AUDIT_RESULT, " +
	"VISIT_ID, " +
	"PRE_EQPNO, " +
	"POST_EQPNO, " +
	"PRE_QTY_PARTS, " +
	"POST_QTY_PARTS, " +
	"PRE_REPAIR_CD, " +
	"POST_REPAIR_CD, " +
	"PRE_REPAIR_LOC_CD, " +
	"POST_REPAIR_LOC_CD, " +
	"PRE_DAMAGE_CD, " +
	"POST_DAMAGE_CD, " +
	"PRE_TPI, " +
	"POST_TPI, " +
	"WO_REPAIR, " +
	"MAN_HRS, " +
	"MATERIAL_COST, " +
	"PART_COST, " +
	"TOTAL_PER_CODE, " +
	"REPAIR_INDEX, " +
	"PART_INDEX, " +
	"REPAIR_DESC, " +
	"PART_DESC, " +
	"IDENTIFIER) " +
	"VALUES(@workOrderNo, " +
	"@shopCode, " +
	"@auditDate, " +
	"@auditor, " +
	"@auditResult, " +
	"@visitId, " +
	"@preEqpNo, " +
	"@postEqpNo, " +
	"@preQtyParts, " +
	"@postQtyParts, " +
	"@preRepairCode, " +
	"@postRepairCode, " +
	"@preRepairLocCode, " +
	"@postRepairLocCode, " +
	"@preDamageCode, " +
	"@postDamageCode, " +
	"@preTPI, " +
	"@postTPI, " +
	"@woRepair, " +
	"@manHrs, " +
	"@materialCost, " +
	"@partCost, " +
	"@totalPerCode, " +
	"@repairIndex, " +
	"@partIndex, " +
	"@repairDesc, " +
	"@partDesc, " +
	"'')"
);

var saveFinishedWorkOrderPartQueryPostRepair = WL.Server.createSQLStatement(
	"BEGIN " +
	"DECLARE @auditor VARCHAR(50) SET @auditor = ? " +
	"DECLARE @auditDate DATETIME SET @auditDate = ? " +
	"DECLARE @visitId VARCHAR(50) SET @visitId = ? " +
	"DECLARE @workOrderNo INT SET @workOrderNo = ? " +
	"DECLARE @shopCode VARCHAR(50) SET @shopCode = ? " +
	"DECLARE @auditResult INT SET @auditResult = ? " +
	"DECLARE @preEqpNo VARCHAR(50) SET @preEqpNo = ? " +
	"DECLARE @postEqpNo VARCHAR(50) SET @postEqpNo = ? " +
	"DECLARE @preQtyParts VARCHAR(50) SET @preQtyParts = ? " +
	"DECLARE @postQtyParts VARCHAR(50) SET @postQtyParts = ? " +
	"DECLARE @preRepairCode VARCHAR(50) SET @preRepairCode = ? " +
	"DECLARE @postRepairCode VARCHAR(50) SET @postRepairCode = ? " +
	"DECLARE @preRepairLocCode VARCHAR(50) SET @preRepairLocCode = ? " +
	"DECLARE @postRepairLocCode VARCHAR(50) SET @postRepairLocCode = ? " +
	"DECLARE @preDamageCode VARCHAR(50) SET @preDamageCode = ? " +
	"DECLARE @postDamageCode VARCHAR(50) SET @postDamageCode = ? " +
	"DECLARE @preTPI VARCHAR(50) SET @preTPI = ? " +
	"DECLARE @postTPI VARCHAR(50) SET @postTPI = ? " +
	"DECLARE @woRepair INT SET @woRepair = ? " +
	"DECLARE @manHrs VARCHAR(50) SET @manHrs = ? " +
	"DECLARE @materialCost VARCHAR(50) SET @materialCost = ? " +
	"DECLARE @partCost VARCHAR(50) SET @partCost = ? " +
	"DECLARE @totalPerCode VARCHAR(50) SET @totalPerCode = ? " +
	"DECLARE @partIndex INT SET @partIndex = ? " +
	"DECLARE @repairIndex INT SET @repairIndex = ? " +
	"DECLARE @repairDesc VARCHAR(50) SET @repairDesc = ? " +
	"DECLARE @partDesc VARCHAR(50) SET @partDesc = ? " +
	"DECLARE @repairQuality INT SET @repairQuality = ? " +
	"END " +
	"INSERT INTO MESC1TS_AUDIT_RESULTS " +
	"(WO_ID, " +
	"SHOP_CD, " +
	"AUDIT_DATE, " +
	"AUDITOR, " +
	"AUDIT_RESULT, " +
	"VISIT_ID, " +
	"PRE_EQPNO, " +
	"POST_EQPNO, " +
	"PRE_QTY_PARTS, " +
	"POST_QTY_PARTS, " +
	"PRE_REPAIR_CD, " +
	"POST_REPAIR_CD, " +
	"PRE_REPAIR_LOC_CD, " +
	"POST_REPAIR_LOC_CD, " +
	"PRE_DAMAGE_CD, " +
	"POST_DAMAGE_CD, " +
	"PRE_TPI, " +
	"POST_TPI, " +
	"WO_REPAIR, " +
	"MAN_HRS, " +
	"MATERIAL_COST, " +
	"PART_COST, " +
	"TOTAL_PER_CODE, " +
	"REPAIR_INDEX, " +
	"PART_INDEX, " +
	"REPAIR_DESC, " +
	"PART_DESC, " +
	"REPAIR_QUALITY, " +
	"IDENTIFIER) " +
	"VALUES(@workOrderNo, " +
	"@shopCode, " +
	"@auditDate, " +
	"@auditor, " +
	"@auditResult, " +
	"@visitId, " +
	"@preEqpNo, " +
	"@postEqpNo, " +
	"@preQtyParts, " +
	"@postQtyParts, " +
	"@preRepairCode, " +
	"@postRepairCode, " +
	"@preRepairLocCode, " +
	"@postRepairLocCode, " +
	"@preDamageCode, " +
	"@postDamageCode, " +
	"@preTPI, " +
	"@postTPI, " +
	"@woRepair, " +
	"@manHrs, " +
	"@materialCost, " +
	"@partCost, " +
	"@totalPerCode, " +
	"@repairIndex, " +
	"@partIndex, " +
	"@repairDesc, " +
	"@partDesc, " +
	"@repairQuality, " +
	"'')"
);


var cancelAuditQuery = WL.Server.createSQLStatement(
	"UPDATE MESC1TS_AUDIT_DETAILS " +
	"SET " +
	"AUDIT_STATUS = 2, " +
	"VISIT_ID = null, " +
	"AUDITOR = null " +
	"WHERE " +
	"VISIT_ID = ? AND " +
	"AUDITOR = ? " +
	"AND AUDIT_STATUS <> 2"
);

function onRequestSuccess(data, context) {
	return {
		isSuccessful : true,
		status : 200,
		errorCode : 0,
		errorMsg : null,
		invocationContext : context,
		invocationResult : data
	}
}

function onRequestFailure(errorMsg, errorCode, data, context) {
	return {
		isSuccessful : false,
		status : 200,
		errorCode : errorCode,
		errorMsg : errorMsg,
		invocationContext : context,
		data : data
	}
}

function invokeSQL(statement, parameters) {
	try {
		// Invoke SQL statement
		var returnData = WL.Server.invokeSQLStatement({
			preparedStatement : statement,
			parameters : parameters
		});
		
		// Return SQL statement data
		return returnData;

	} catch (error) {
		return onRequestFailure("Database SQL e xecution error thrown", 101, error);
	}
}

/**
 * @name finishVisit
 * @desc Deprecated
 * @memberof Adapters.FinishAdapter
 */
function finishVisit(username, token, workOrders, date, archivePath, auditParts, shopCode, photos, visitId, certificateId, context) {
	WL.Logger.info("[FinishAdapter][finishVisit] username: " + username + ",workOrders: " + JSON.stringify(workOrders) + ", visitId: " + visitId + ", shopCode: " + shopCode);
	// Sanity check + Token validity
	if (checkUsernameAndToken(username, token, context).valid === false) {
		return checkUsernameAndToken(username, token, context).response;
	}
	
	/*
		*  START Transaction 
		*/
	
	try {
		var data = invokeSQL(beginTranProc, []); 
		
		// Save WorkOrders in MESC1TS_AUDIT_HISTORY table
		var response = null;
		for(index in workOrders){
			response = saveFinishedWorkOrderBeta(username, workOrders[index].workorderid, date, workOrders[index].visitid, workOrders[index].visitdesc ,workOrders[index].shopcode, workOrders[index].auditresult, archivePath, workOrders[index].finalcomment, certificateId, context);
			if (!response.isSuccessful){
				invokeSQL(rollBackProc, []); // If error ROLLBACK
				WL.Logger.error("finishVisit" + JSON.stringify(response));
				return onRequestFailure("SQL error0", 100, response, context);
			}
		}
		
		// Save AuditParts in MESC1TS_AUDIT_RESULTS table
		for(index in auditParts){
			response = saveFinishedWorkOrderPart(username,auditParts[index].workorderid, date, auditParts[index].visitid, shopCode, auditParts[index].accepted, 
					auditParts[index].prepartcode, auditParts[index].postpartcode, auditParts[index].prequantity, auditParts[index].postquantity, 
					auditParts[index].prerepaircode, auditParts[index].postrepaircode, auditParts[index].prerepairloccode, auditParts[index].postrepairloccode,
					auditParts[index].predamagecode, auditParts[index].postdamagecode, auditParts[index].pretpi, auditParts[index].posttpi,
					auditParts[index].worepair, auditParts[index].manhrs, auditParts[index].materialcost, auditParts[index].partcost,
					auditParts[index].totalpercode, auditParts[index].partindex, auditParts[index].repairindex, auditParts[index].repairdescription, 
					auditParts[index].partdescription, context);
			if (!response.isSuccessful){
				invokeSQL(rollBackProc, []); // If error ROLLBACK
				WL.Logger.error("finishVisit" + JSON.stringify(response));
				return onRequestFailure("SQL error1", 100, response, context);
			}
		}
		
		
		// Save PHOTOS in MESC1TS_AUDIT_PHOTOS table
		for(index in photos){
			response = savePhotos(photos[index].json.path, photos[index].json.name, photos[index].json.date, 
					photos[index].json.visitId, photos[index].json.wono, username,"",context);
			if (!response.isSuccessful){
				invokeSQL(rollBackProc, []); // If error ROLLBACK
				WL.Logger.error("finishVisit" + JSON.stringify(response));
				return onRequestFailure("SQL error2", 100, response, context);
			}
		}
		
		// CHANGE AUDIT STATUS TO FINISH
		response =  cancelAudit(username, visitId, context)
		
		if (!response.isSuccessful){
			invokeSQL(rollBackProc, []); // If error ROLLBACK
			WL.Logger.error("finishVisit" + JSON.stringify(response));
			return onRequestFailure(response, 100, response, context);
		}
		
		invokeSQL(comitTranProc, []); // If everything was OK COMMIT
		
		return onRequestSuccess("success", context);
	} catch(e) {
		invokeSQL(rollBackProc, []); // If error ROLLBACK
		WL.Logger.error("finishVisit" + JSON.stringify(e));
		return onRequestFailure("SQL error", 100, null, context);
	}
}
	
/**
 * @name finishVisitPostRepair
 * @desc Runs a transaction that
 * saves work orders in MESC1TS_AUDIT_HISTORY table,
 * saves work order repairs and parts in MESC1TS_AUDIT_RESULTS table,
 * saves photo info MESC1TS_AUDIT_PHOTOS table,
 * sets the status of work orders to available
 * in MESC1TS_AUDIT_DETAILS table
 * @param {String} username
 * @param {String} token - no token auth used
 * @param {Array} workOrders - work orders inspected by auditor
 * @param {Date} date - date of audit
 * @param {String} archivePath - photo archive path on the server
 * @param {Array} auditParts - work order repairs and parts
 * associated with inspected work orders
 * @param {String} shopCode
 * @param {Array} photos - array of photo information from 'galleryPhoto' collection
 * @param {String} visitId - visit id of visit to be finished
 * @param {String} certificateId - certificate user logged in with
 * @param {Any} context - param that will be returned in the response
 * @returns {Object} - An array of inspection info for each month
 * @memberof Adapters.FinishAdapter
 */	
function finishVisitPostRepair(username, token, workOrders, date, archivePath, auditParts, shopCode, photos, visitId, certificateId, context) {
	WL.Logger.info("[FinishAdapter][finishVisitPostRepair] username: " + username + ",workOrders: " + JSON.stringify(workOrders) + ", visitId: " + visitId + ", shopCode: " + shopCode);
	// Sanity check + Token validity
	if (checkUsernameAndToken(username, token, context).valid === false) {
		return checkUsernameAndToken(username, token, context).response;
	}
	
	/*
	 *  START Transaction 
	 */
	
	try {
		var data = invokeSQL(beginTranProc, []); 
		
		// Save WorkOrders in MESC1TS_AUDIT_HISTORY table
		var response = null;
		for(index in workOrders){
			if(workOrders[index].repairquality === "N/A"){
				workOrders[index].repairquality = -1;
			}
			response = saveFinishedWorkOrderPostRepair(username, workOrders[index].workorderid, date, workOrders[index].visitid,
					workOrders[index].visitdesc,workOrders[index].shopcode, workOrders[index].auditresult, archivePath, workOrders[index].finalcomment, 
					certificateId, workOrders[index].statuscode, workOrders[index].repairquality, context);
			if (!response.isSuccessful){
				invokeSQL(rollBackProc, []); // If error ROLLBACK
				return onRequestFailure("SQL error0", 100, response, context);
				
			}
		}
		
		// Save AuditParts in MESC1TS_AUDIT_RESULTS table
		for(index in auditParts){
			if(auditParts[index].repairquality === "N/A"){
				auditParts[index].repairquality = -1;
			}
			
			// testing
			
			if (!isInteger(auditParts[index].prequantity)) {
				auditParts[index].prequantity = (Math.ceil(auditParts[index].prequantity) + 1).toString();
			}
			
			response = saveFinishedWorkOrderPartPostRepair(username,auditParts[index].workorderid, date, auditParts[index].visitid, shopCode, auditParts[index].accepted, 
					auditParts[index].prepartcode, auditParts[index].postpartcode, auditParts[index].prequantity, auditParts[index].postquantity, 
					auditParts[index].prerepaircode, auditParts[index].postrepaircode, auditParts[index].prerepairloccode, auditParts[index].postrepairloccode,
					auditParts[index].predamagecode, auditParts[index].postdamagecode, auditParts[index].pretpi, auditParts[index].posttpi,
					auditParts[index].worepair, auditParts[index].manhrs, auditParts[index].materialcost, auditParts[index].partcost,
					auditParts[index].totalpercode, auditParts[index].partindex, auditParts[index].repairindex, auditParts[index].repairdescription, 
					auditParts[index].partdescription, auditParts[index].repairquality, context);
			if (!response.isSuccessful){
				invokeSQL(rollBackProc, []); // If error ROLLBACK
				return onRequestFailure("SQL error1", 100, response, context);
			}
		}
		
		
		// Save PHOTOS in MESC1TS_AUDIT_PHOTOS table
		for(index in photos){
			response = savePhotos(photos[index].json.path, photos[index].json.name, photos[index].json.date, 
					photos[index].json.visitId, photos[index].json.wono, username,"",context);
			if (!response.isSuccessful){
				invokeSQL(rollBackProc, []); // If error ROLLBACK
				return onRequestFailure("SQL error2", 100, response, context);
			}
		}
		
		// CHANGE AUDIT STATUS TO FINISH
		response =  cancelAudit(username, visitId, context)
		
		if (!response.isSuccessful){
			invokeSQL(rollBackProc, []); // If error ROLLBACK
			return onRequestFailure(response, 100, response, context);
		}
		
		invokeSQL(comitTranProc, []); // If everything was OK COMMIT
		
		return onRequestSuccess("success", context);
	} catch(e) {
		invokeSQL(rollBackProc, []); // If error ROLLBACK
		return onRequestFailure("SQL error", 100, null, context);
	}
}
	
/**
 * @name saveFinishedWorkOrderBeta
 * @desc Deprecated
 * @memberof Adapters.FinishAdapter
 */	
function saveFinishedWorkOrderBeta(username,workOrderId, visitDate, visitId, visitDesc, shopCode, auditResult, archiveId, comment, certificateId, context) {
	
	// General validation
	if (!validArguments([workOrderId, visitId, shopCode, visitDate])) {
		return onRequestFailure("workOrderId, visitId, visitDate or shopCode query argument invalid (not provided or incorrect format)", 107, null, context);
	}
	
	// validate auditStatus
	if (auditResult != 0 && auditResult != 1) {
		return onRequestFailure("auditStatus query argument invalid (not provided or incorrect format)", 107, null, context);
	}
	
	if (typeof visitDesc !== 'string') {
		return onRequestFailure("visitDesc query argument invalid (not provided or incorrect format)", 107, null, context);
	}
	
	if (certificateId && certificateId.length == 0) {
		return onRequestFailure("certificateId query argument invalid (not provided or incorrect format)", 107, null, certificateId);
	}
	
	if(comment == null || comment == undefined) comment = "";
	
	var data = invokeSQL(saveFinishedWorkOrderQueryBeta, [username, visitDate, visitId, archiveId, workOrderId, shopCode, auditResult, visitDesc, comment, certificateId, "NEW"], context);
	
	if (data.isSuccessful && data.updateStatementResult && data.updateStatementResult.updateCount == 1) {
		// update was successful
		return onRequestSuccess(data, context);
	}
	
	// return generic error
	return onRequestFailure("SQL error", 100, data, context);
}
	
// Release 2 - Sprint 2 - March 2017

function saveFinishedWorkOrderPostRepair(username,workOrderId, visitDate, visitId, visitDesc, shopCode, auditResult, archiveId, comment, certificateId, statusCode, repairQuality, context) {
	
	// General validation
	if (!validArguments([workOrderId, visitId, shopCode, visitDate])) {
		return onRequestFailure("workOrderId, visitId, visitDate or shopCode query argument invalid (not provided or incorrect format)", 107, null, context);
	}
	
	// validate auditStatus
	if (auditResult != 0 && auditResult != 1) {
		return onRequestFailure("auditStatus query argument invalid (not provided or incorrect format)", 107, null, context);
	}
	
	if (typeof visitDesc !== 'string') {
		return onRequestFailure("visitDesc query argument invalid (not provided or incorrect format)", 107, null, context);
	}
	
	if (certificateId && certificateId.length == 0) {
		return onRequestFailure("certificateId query argument invalid (not provided or incorrect format)", 107, null, certificateId);
	}
	
	if(comment == null || comment == undefined) comment = "";
	
	if (typeof statusCode == 'undefined' || (typeof statusCode == 'string' && statusCode.length == 0)) {
		statusCode = null;
	}
	
	if (typeof repairQuality == 'undefined'  || (typeof repairQuality == 'string' && repairQuality.length == 0)) {
		repairQuality = null;
	}
	
	var data = invokeSQL(saveFinishedWorkOrderQueryPostRepair, [username, visitDate, visitId, archiveId, workOrderId, shopCode, auditResult, visitDesc, comment, certificateId, "NEW", statusCode, repairQuality], context);
	
	if (data.isSuccessful && data.updateStatementResult && data.updateStatementResult.updateCount == 1) {
		// update was successful
		return onRequestSuccess(data, context);
	}
	
	// return generic error
	return onRequestFailure("SQL error", 100, data, context);
}
	
/**
 * @name saveFinishedWorkOrderPart
 * @desc Deprecated
 * @memberof Adapters.FinishAdapter
 */	
function saveFinishedWorkOrderPart(username,workOrderId, visitDate, visitId, shopCode, auditResult, preEqpNo, postEqpNo, preQtyParts, postQtyParts, 
		preRepairCode, postRepairCode, preRepairLocCode, postRepairLocCode, preDamageCode, postDamageCode, preTPI, postTPI, woRepair,
		manHrs, materialCost, partCost, totalPerCode, partIndex, repairIndex, repairDesc, partDesc, context) {
	
	// General validation
	if (!validArguments([workOrderId, visitId, shopCode, visitDate])) {
		return onRequestFailure("workOrderId, visitId or shopCode query argument invalid (not provided or incorrect format)", 107, null, context);
	}
	
	// validate auditStatus
	if (auditResult != 0 && auditResult != 1) {
		return onRequestFailure("auditStatus query argument invalid (not provided or incorrect format)", 107, null, context);
	}
	
	if (woRepair != 0 && woRepair != 1) {
		return onRequestFailure("woRepair query argument invalid (not provided or incorrect format)", 107, null, context);
	}
	
	if (!stringArguments([preQtyParts, postQtyParts, preRepairCode, postRepairCode, preRepairLocCode, postRepairCode, preRepairLocCode, postRepairLocCode, preDamageCode, postDamageCode])) {
		return onRequestFailure("preQtyParts, postQtyParts, preRepairCode, postRepairCode, preRepairLocCode, postRepairCode, preRepairLocCode, postRepairLocCode, preDamageCode or postDamageCode query argument invalid (not provided or incorrect format)", 107, null, context);
	}
	
	var data = invokeSQL(saveFinishedWorkOrderPartQuery, [username, visitDate, visitId, workOrderId, shopCode, auditResult, preEqpNo,
															postEqpNo, preQtyParts, postQtyParts, preRepairCode, postRepairCode, preRepairLocCode, 
															postRepairLocCode, preDamageCode, postDamageCode,
															preTPI, postTPI, woRepair, manHrs, materialCost,
															partCost, totalPerCode, partIndex, repairIndex, 
															repairDesc, partDesc], context);
	
	if (data.isSuccessful && data.updateStatementResult && data.updateStatementResult.updateCount == 1) {
		// update was successful
		return onRequestSuccess(data, context);
	}
	
	// return generic error
	return onRequestFailure("SQL error", 100, data, context);
}
	
// Release 2 - Sprint 2 - March 2017

function saveFinishedWorkOrderPartPostRepair(username,workOrderId, visitDate, visitId, shopCode, auditResult, preEqpNo, postEqpNo, preQtyParts, postQtyParts, 
		preRepairCode, postRepairCode, preRepairLocCode, postRepairLocCode, preDamageCode, postDamageCode, preTPI, postTPI, woRepair,
		manHrs, materialCost, partCost, totalPerCode, partIndex, repairIndex, repairDesc, partDesc, repairQuality, context) {
	
	// General validation
	if (!validArguments([workOrderId, visitId, shopCode, visitDate])) {
		return onRequestFailure("workOrderId, visitId or shopCode query argument invalid (not provided or incorrect format)", 107, null, context);
	}
	
	// validate auditStatus
	if (auditResult != 0 && auditResult != 1) {
		return onRequestFailure("auditStatus query argument invalid (not provided or incorrect format)", 107, null, context);
	}
	
	if (woRepair != 0 && woRepair != 1) {
		return onRequestFailure("woRepair query argument invalid (not provided or incorrect format)", 107, null, context);
	}
	
	if (!stringArguments([preQtyParts, postQtyParts, preRepairCode, postRepairCode, preRepairLocCode, postRepairCode, preRepairLocCode, postRepairLocCode, preDamageCode, postDamageCode])) {
		return onRequestFailure("preQtyParts, postQtyParts, preRepairCode, postRepairCode, preRepairLocCode, postRepairCode, preRepairLocCode, postRepairLocCode, preDamageCode or postDamageCode query argument invalid (not provided or incorrect format)", 107, null, context);
	}
	
	if (typeof repairQuality == 'undefined' || (typeof repairQuality == 'string' && repairQuality.length == 0)) {
		repairQuality = null;
	}
	
	var data = invokeSQL(saveFinishedWorkOrderPartQueryPostRepair, [username, visitDate, visitId, workOrderId, shopCode, auditResult, preEqpNo,
															postEqpNo, preQtyParts, postQtyParts, preRepairCode, postRepairCode, preRepairLocCode, 
															postRepairLocCode, preDamageCode, postDamageCode,
															preTPI, postTPI, woRepair, manHrs, materialCost,
															partCost, totalPerCode, partIndex, repairIndex, 
															repairDesc, partDesc, repairQuality], context);
	
	if (data.isSuccessful && data.updateStatementResult && data.updateStatementResult.updateCount == 1) {
		// update was successful
		return onRequestSuccess(data, context);
	}
	
	// return generic error
	return onRequestFailure("SQL error", 100, data, context);
}
	
	
//savePhotosQuery   
function savePhotos(path, name, date, visitId, woId, auditor, extra, context) {
	
	if (visitId === undefined || visitId.length == 0) {
		return onRequestFailure("visitId query argument invalid (not provided or incorrect format)", 107, null, context);
	}
	
	var data = invokeSQL(savePhotosQuery, [path, name, date,visitId, woId, auditor, extra], context);
	
	if (data.isSuccessful) {
		return onRequestSuccess(data, context);
	}
	
	// return generic error
	return onRequestFailure("SQL error", 100, data, context);
}

function userCredentialsValid(username, token, context) {

		// Verify user and token in our database
		var invocationData = {
				adapter : "DBAdapterContainer",
				procedure : "verifyToken",
				parameters : [username, token, (context != undefined ? context : null)]
		};
		var data = WL.Server.invokeProcedure(invocationData);

		// Check returned data
		if (data != undefined && data.invocationResult != undefined) {
			
			// Return token validity
			return data.invocationResult.tokenValid === true;
		}
		
		// Incorrect data - possibly user does not exist in the database
		return false;
	}
	
function cancelAudit(username, visitId, context) {
	
	if (visitId === undefined || visitId.length == 0) {
		return onRequestFailure("visitId query argument invalid (not provided or incorrect format)", 107, null, context);
	}
	
	var data = invokeSQL(cancelAuditQuery, [visitId, username], context);
	
	if (data.isSuccessful) {
		// update was successful
		return onRequestSuccess(data, context);
	}
	
	// return generic error
	return onRequestFailure("SQL error", 100, data, context);
}

function checkUsernameAndToken(username, token, context) {
	return {valid: true};
	if (username == undefined || username.length == 0 || token == undefined || token.length == 0) {
		return {valid: false, response: onRequestFailure("User credentials not provided (username and/or token)", 105, null, context)}
	} 
	if (!userCredentialsValid(username, token, context)) {
		WL.Logger.info("user credentials invalid: " + username + ":" + token);
		
		// Either user does not exist or user token is invalid or expired - return an error
		return {valid: false, response: onRequestFailure("User does not exist or token expired or invalid (user credentials validity)",
				102, null, context)}
	}
	WL.Logger.info("user \"" + username + "\"token \"" + token + "\" are valid");
	return {valid: true};
}

function validArguments(inputArray) {
	for (var i = 0; i < inputArray.length; i++) {
		var input = inputArray[i];
		if (input == undefined || input.length == 0) {
			return false;
		}
	}
	return true;
}

function isInteger(value) {
	return (value == parseInt(value, 10));
}

function stringArguments(inputArray) {
	for (var i = 0; i < inputArray.length; i++) {
		var input = inputArray[i];
		if (typeof input !== 'string') {
			return false;
		}
	}
	return true;
}

function successHandler(response, defer) {
	if (!response.invocationResult.isSuccessful) {
		defer.reject(response.invocationResult);
	} else {
		defer.resolve(response);
	}
}

function errorHandler(error, defer) {
	console.log('error', error);
	defer.reject(error);
} 

function errorHandlerWithFeedback(error, defer) {
	console.log('error', error);
	$ionicLoading.hide();
	defer.reject(error);
	responseHandler.feedback(error);
} 



