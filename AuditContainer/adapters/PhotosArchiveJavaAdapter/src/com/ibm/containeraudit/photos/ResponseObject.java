package com.ibm.containeraudit.photos;

public class ResponseObject {

	public String isSuccessful;
	public int status;
	public int errorCode;
	public String errorMsg;
	public String invocationContext;
	public String invocationResult;
	
	public ResponseObject(String _isSuccessful, int _status, int _errorCode, String _errorMsg, String _invocationContext, String _invocationResult) {
		isSuccessful = _isSuccessful;
		status = _status;
		errorCode = _errorCode;
		errorMsg = _errorMsg;
		invocationContext = _invocationContext;
		invocationResult = _invocationResult;
	}
	
	public String getIsSuccessful() {
		return isSuccessful;
	}
	public void setIsSuccessful(String isSuccessful) {
		this.isSuccessful = isSuccessful;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public int getErrorCode() {
		return errorCode;
	}
	public void setErrorCode(int errorCode) {
		this.errorCode = errorCode;
	}
	public String getErrorMsg() {
		return errorMsg;
	}
	public void setErrorMsg(String errorMsg) {
		this.errorMsg = errorMsg;
	}
	public String getInvocationContext() {
		return invocationContext;
	}
	public void setInvocationContext(String invocationContext) {
		this.invocationContext = invocationContext;
	}
	public String getInvocationResult() {
		return invocationResult;
	}
	public void setInvocationResult(String invocationResult) {
		this.invocationResult = invocationResult;
	}
	
	public String toJsonString(){
		String string =  "{\n";
		string = string + "    \"isSuccessful\" : " + getIsSuccessful() + ",\n";
		string = string + "    \"status\" : " + String.valueOf(getStatus()) + ",\n";
		string = string + "    \"errorCode\" : " + String.valueOf(getErrorCode()) + ",\n";
		string = string + "    \"errorMsg\" : " + "\"" + getErrorMsg() + "\",\n";
		string = string + "    \"invocationContext\" : " + "\"" + getInvocationContext() + "\",\n";
		string = string + "    \"invocationResult\" : " + "\"" + getInvocationResult()+ "\"\n";
		string = string + "}";
		return string;
	}
	
	
	  /**
	   * Convert the given object to string with each line indented by 4 spaces
	   * (except the first line).
	   */
	  private String toIndentedString(Object o) {
	    if (o == null) {
	      return "null";
	    }
	    return o.toString().replace("\n", "\n    ");
	  }

}
