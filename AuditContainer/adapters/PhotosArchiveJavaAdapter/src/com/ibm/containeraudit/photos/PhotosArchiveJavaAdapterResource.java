/*
 *    Licensed Materials - Property of IBM
 *    5725-I43 (C) Copyright IBM Corp. 2015. All Rights Reserved.
 *    US Government Users Restricted Rights - Use, duplication or
 *    disclosure restricted by GSA ADP Schedule Contract with IBM Corp.
*/

package com.ibm.containeraudit.photos;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.logging.Logger;

import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.CacheControl;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.StreamingOutput;

import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.ibm.audit.FileHandler;
import com.worklight.adapters.rest.api.WLServerAPI;
import com.worklight.adapters.rest.api.WLServerAPIProvider;
import com.worklight.core.auth.OAuthSecurity;
import com.worklight.server.bundle.api.WorklightConfiguration;

@Path("/")
public class PhotosArchiveJavaAdapterResource {

	//Define logger (Standard java.util.Logger)
	static Logger logger = Logger.getLogger(PhotosArchiveJavaAdapterResource.class.getName());

    //Define the server api to be able to perform server operations
    WLServerAPI api = WLServerAPIProvider.getWLServerAPI();

    public static final String ERROR_CASE_0 = "Malformed URL";
	public static final String ERROR_CASE_1 = "Failed to open connection from URL";
	public static final String ERROR_CASE_2 = "Failed to set Authorization header";
	public static final String ERROR_CASE_3 = "IOException when opening input stream";
	public static final String ERROR_CASE_4 = "IOException after successful connection";
	public static final String ERROR_CASE_5 = "User credentials not provided (username)";
	public static final String ERROR_CASE_6 = "Cannot decode URL";
	public static final String ERROR_CASE_7 = "Could not read archive file from disk";
	public static final String ERROR_CASE_8 = "Could not write archive file to disk";
	public static final int ERROR_CODE_107 = 107;
	public static final int ERROR_CODE_105 = 105;
	public static final int ERROR_CODE_110 = 110;
	public static final int ERROR_CODE_111 = 111;

	@POST
	@Path("/numbers")
	@OAuthSecurity(enabled = false)
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
    public Response streamExample(){
		
		CacheControl cc = new CacheControl();
		cc.setNoCache(true);
		cc.setMaxAge(-1);
		cc.setMustRevalidate(true); 
		
        StreamingOutput stream = new StreamingOutput() {
            @Override
            public void write(OutputStream out) throws IOException, WebApplicationException {
                Writer writer = new BufferedWriter(new OutputStreamWriter(out));
                for (int i = 0; i < 10000000 ; i++){
                    writer.write(i + " ");
                    if(i%500==0){
                    	writer.flush();

                    }
                   
                }
                
                out.close();
            }
        };

        return Response.ok(stream).status(200).cacheControl(cc).build(); 
    }

	
	 @GET
	 @Path("/download")
	 @OAuthSecurity(enabled = false)
	 @Produces(MediaType.APPLICATION_OCTET_STREAM)
	 public Response downloadFilebyPath(@QueryParam("fileName") String fileName) {
		 Response response = null;
		 String serverPath = WorklightConfiguration.getInstance().getStringProperty("photosBaseDir");
	
		 File file = new File(serverPath + fileName);
		 
		 if (file.exists()) {
			 ResponseBuilder builder = Response.ok(file);
			 builder.header("Content-Disposition", "attachment; filename=" + file.getName());
			 response = builder.build();
//			 long file_size = file.length();

		 } else {
//			 logger.error(String.format("Inside downloadFile==> FILE NOT FOUND: fileName: %s",fileName));
			 response = Response.status(404).entity("FILE NOT FOUND: " + serverPath + fileName).type("text/plain").build();
		 }

		 return response;
	 }

    /*
     * @param paramsjson
	 *            The parameters argument in JSON array format
	 * @return Response in JSON format
     */
	@GET
	@OAuthSecurity(enabled = false)
	@Path("/getArchive")
	@Produces("application/json")
	public Response getPhotoArchive(@QueryParam("parameters") String paramsjson) {
		
		String serverPath = WorklightConfiguration.getInstance().getStringProperty("photosBaseDir");

		JSONArray params = null;
		JSONParser parser = new JSONParser();

		// Get the parameters string and parse them to JSONArray
		try {
			params = (JSONArray) parser.parse(paramsjson);
		} catch (ParseException e) {
			e.printStackTrace();
		}

		// The JS adapter receives an array called "parameters" containing
		// (username, token, url, context)
		String username = null, token = null, archiveName = null, context = null;
		try {
			username = params.get(0).toString();
			token = params.get(1).toString();
			archiveName = params.get(2).toString();
			context = params.get(3).toString();

			if (username.isEmpty() || token.isEmpty() || archiveName.isEmpty()) {
				ResponseObject resp = new ResponseObject("false", 200, ERROR_CODE_105, ERROR_CASE_5, context, null);
				return Response.ok(resp.toJsonString()).build();
			}

		} catch (IndexOutOfBoundsException ex) {
			ex.printStackTrace();

			if (username == null || username.isEmpty() || token == null || token.isEmpty() || archiveName == null || archiveName.isEmpty()) {
				ResponseObject resp = new ResponseObject("false", 200, ERROR_CODE_105, ERROR_CASE_5, context, null);
				return Response.ok(resp.toJsonString()).build();
			}
		}
		
		String archivePath = serverPath + archiveName;
		FileHandler fileHandler = new FileHandler();
		
		String dataFile = fileHandler.readStringFromFile(archivePath);

		if(dataFile==null || dataFile.isEmpty()){
			ResponseObject resp = new ResponseObject("false", 200, ERROR_CODE_111, ERROR_CASE_7, context, null);
			return Response.ok(resp.toJsonString()).build();
		}
	
		ResponseObject resp = new ResponseObject("true", 200, 0, "", context, dataFile);
		return Response.ok(resp.toJsonString()).build();
	}
    
	@POST
	@OAuthSecurity(enabled = false)
	@Path("/addArchive")
	@Produces("application/json")
	public Response addPhotoArchive(
			@FormParam("username") String username,
			@FormParam("visitId") String visitId,
			@FormParam("archiveBase64") String archiveBase64,
			@FormParam("context") String context) {

		String serverPath = WorklightConfiguration.getInstance().getStringProperty("photosBaseDir");

		// The JS adapter receives an array called "parameters" containing
		// (username, visitId, archiveBase64, context)

		if (username.isEmpty() || visitId.isEmpty() || archiveBase64.isEmpty()) {
			ResponseObject resp = new ResponseObject("false", 200, ERROR_CODE_105, ERROR_CASE_5, context, null);
			return Response.ok(resp.toJsonString()).build();
		}

		FileHandler fileHandler = new FileHandler();
		String fileName = null;
		try{
			fileName = fileHandler.writeStringToFile(serverPath, archiveBase64);	
		}catch(Exception e){
			ResponseObject resp = new ResponseObject("false", 200, ERROR_CODE_110, ERROR_CASE_8, context, null);
			return Response.ok(resp.toJsonString()).build();
		}
		
		ResponseObject resp = new ResponseObject("true", 200, 0, "", null, fileName);
		return Response.ok(resp.toJsonString()).build();
	}

	
	
}
