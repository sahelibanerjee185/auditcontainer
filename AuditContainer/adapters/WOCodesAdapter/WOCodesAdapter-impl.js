/*
 * June, 1 2016
 * WOCodesAdapter-impl.js
 * Stavarache Vlad
 */

/* Error Codes:
 * 
 * 0   - No error
 * 99  - Unknown error
 * 100 - Database SQL statement execution error
 * 101 - Database SQL execution error thrown (couldn't connect to the database)
 * 102 - Invalid or expired token
 * 103 - Username not provided
 * 104 - Token not provided
 * 105 - User credentials not provided (username and/or token)
 * 106 - Password not provided
 * 107 - Query arguments invalid (not provided or incorrect format)
 * 108 - Could not generate a new token (database request failed)
 * 109 - Login failure (incorrect username or password)
 */

/**
 * @category Database statements
 */

var getRepairCodesQuery = WL.Server.createSQLStatement(
	"SELECT RTRIM(REPAIR_CD) AS code, REPAIR_DESC AS description, MODE as mode " +
	"FROM MESC1TS_REPAIR_CODE"
);

var getRepairLocCodesQuery = WL.Server.createSQLStatement(
	"SELECT cedex_code AS code, description " +
	"FROM MESC1TS_REPAIR_LOC"
);

var getDamageCodesQuery = WL.Server.createSQLStatement(
	"SELECT D.cedex_code AS code, D.description, D.name, D.numerical_code, D.CHUSER AS chuser, CONVERT(VARCHAR(10), D.CHTS, 21) AS chts " +
	"FROM MESC1TS_DAMAGE D"
);

var getPartCodesQuery = WL.Server.createSQLStatement(
	"SELECT RTRIM(PART_CD) AS code, PART_DESC AS description " +
	"FROM MESC1TS_MASTER_PART"
);

var getPartCodesForModesQuery = WL.Server.createSQLStatement(
	"SELECT RTRIM(M.PART_CD) AS code, RTRIM(LTRIM(M.PART_DESC)) AS description, M.PARTS_GROUP_CD AS partGroup, " +
	"IsNull(M.PART_DESIGNATION_1, '') AS designation, M.QUANTITY as qty, M.MANUFCTR AS manufacturer, " +
	"M.PART_PRICE AS price, M.PART_ACTIVE_SW AS active, M.CORE_PART_SW AS core, " +
	"M.CORE_VALUE AS coreValue, M.DEDUCT_CORE AS deductCore " +
	"FROM MESC1TS_MASTER_PART M WHERE M.PART_ACTIVE_SW = 'Y' " +
	"ORDER BY M.PART_CD " +
	"OFFSET ? ROWS " +
	"FETCH NEXT ? ROWS ONLY"
);

var getTotalCountForCodesForModesQuery = WL.Server.createSQLStatement(
	"SELECT COUNT(*) AS count " +
	"FROM MESC1TS_MASTER_PART M WHERE M.PART_ACTIVE_SW = 'Y'"
);

var getAssociatePartsWithCodesQuery = WL.Server.createSQLStatement(
	"SELECT R.MANUAL_CD AS manual, R.MODE AS mode, R.REPAIR_CD AS repairCode, " +
	"R.PART_CD AS partCode, M.PART_DESC AS partDesc, R.MAX_PART_QTY AS maxPartQty " +
	"FROM MESC1TS_RPRCODE_PART R  " +
	"LEFT JOIN MESC1TS_MASTER_PART M " +
	"ON M.PART_CD = R.PART_CD"
);

var getTotalCountForAssociatePartsWithCodesQuery = WL.Server.createSQLStatement(
	"SELECT COUNT(*) AS count " +
	"FROM MESC1TS_RPRCODE_PART R  " +
	"LEFT JOIN MESC1TS_MASTER_PART M " +
	"ON M.PART_CD = R.PART_CD"
);

var getPartialAssociatePartsWithCodesQuery = WL.Server.createSQLStatement(
	"SELECT R.MANUAL_CD AS manualCode, R.MODE AS mode, R.REPAIR_CD AS repairCode, IsNull(C.REPAIR_DESC, '') AS repairDesc, " +
	"R.PART_CD AS partCode, IsNull(M.PART_DESC, '') AS partDesc, IsNull(R.MAX_PART_QTY, '') AS maxPartQty  " +
	"FROM MESC1TS_RPRCODE_PART R  " +
	"LEFT JOIN MESC1TS_MASTER_PART M " +
	"ON M.PART_CD = R.PART_CD " +
	"LEFT JOIN MESC1TS_REPAIR_CODE C " +
	"ON C.REPAIR_CD = R.REPAIR_CD AND C.MODE = R.MODE AND C.MANUAL_CD = R.MANUAL_CD " +
	"ORDER BY M.PART_CD " +
	"OFFSET ? ROWS " +
	"FETCH NEXT ? ROWS ONLY"
);

var getAllShopsInfoQuery = WL.Server.createSQLStatement(
	"SELECT S.SHOP_CD AS code, S.SHOP_DESC AS name, " +
	"S.LOC_CD AS geoLoc, S.SHOP_ACTIVE_SW AS active, " +
	"S.VENDOR_CD AS vendorCode, S.RKRPLOC AS rkrpLoc, " +
	"S.SHOP_TYPE_CD AS shopType, S.CUCDN AS currCode, " +
	"S.EMAIL_ADR AS email, S.PHONE AS phone, " +
	"S.RRIS70_SUFFIX_CD AS rris70Code, " +
	"S.CHUSER AS chUser, CONVERT(VARCHAR(10), S.CHTS, 21) AS chts, " +
	"S.BYPASS_LEASE_RULES AS bypass, " +
	"S.SALES_TAX_PART_CONT AS conSalesTaxParts, " +
	"S.SALES_TAX_LABOR_CON AS conSalesTaxLabor, " +
	"S.SALES_TAX_PART_GEN AS genSalesTaxParts, " +
	"S.SALES_TAX_LABOR_GEN AS genSalesTaxLabor, " +
	"S.IMPORT_TAX AS importTax, " +
	"S.ACEP_SW AS acep, " +
	"S.OVERTIME_SUSP_SW AS otSuspended, " +
	"S.PREPTIME_SW AS prepTime, " +
	"S.DECENTRALIZED AS decentralized, " +
	"S.AUTO_COMPLETE_SW AS autocomplete, " +
	"S.SHOP_ACTIVE_SW AS shopActive " +
	"FROM MESC1TS_SHOP S"
);

var getAllShopLimitsQuery = WL.Server.createSQLStatement(
	"SELECT L.SHOP_CD AS shopCode, S.SHOP_DESC AS shopName, " +
	"L.MODE AS mode, M.MODE_DESC AS modeDesc, L.REPAIR_AMT_LIMIT AS repairLimit, " +
	"L.AUTO_APPROVE_LIMIT AS approveLimit, L.SHOP_MATERIAL_LIMIT AS materialLimit " +
	"FROM MESC1TS_SHOP_LIMITS L " +
	"LEFT JOIN MESC1TS_SHOP S " +
	"ON L.SHOP_CD = S.SHOP_CD " +
	"LEFT JOIN MESC1TS_MODE M " +
	"ON L.MODE = M.MODE"
);

var getPartialShopLimitsQuery = WL.Server.createSQLStatement(
	"SELECT L.SHOP_CD AS shopCode, S.SHOP_DESC AS shopName, " +
	"L.MODE AS mode, M.MODE_DESC AS modeDesc, L.REPAIR_AMT_LIMIT AS repairLimit, " +
	"L.AUTO_APPROVE_LIMIT AS approveLimit, L.SHOP_MATERIAL_LIMIT AS materialLimit " +
	"FROM MESC1TS_SHOP_LIMITS L " +
	"LEFT JOIN MESC1TS_SHOP S " +
	"ON L.SHOP_CD = S.SHOP_CD " +
	"LEFT JOIN MESC1TS_MODE M " +
	"ON L.MODE = M.MODE " +
	"ORDER BY S.SHOP_CD " +
	"OFFSET ? ROWS " +
	"FETCH NEXT ? ROWS ONLY"
);

var getCountForShopLimitsQuery = WL.Server.createSQLStatement(
	"SELECT COUNT(*) AS count " +
	"FROM MESC1TS_SHOP_LIMITS L " +
	"LEFT JOIN MESC1TS_SHOP S " +
	"ON L.SHOP_CD = S.SHOP_CD " +
	"LEFT JOIN MESC1TS_MODE M " +
	"ON L.MODE = M.MODE"
);

var getLaborRatesQuery = WL.Server.createSQLStatement(
	"SELECT L.SHOP_CD AS shopCode, L.CUSTOMER_CD AS customer, L.EQTYPE AS eqType, " +
	"CONVERT(VARCHAR(10), L.EFF_DTE, 21) AS effDate, CONVERT(VARCHAR(10), L.EXP_DTE, 21) AS expDate, L.REGULAR_RT AS regRate, " +
	"L.OVERTIME_RT AS ot1Rate, L.DOUBLETIME_RT AS ot2Rate, L.MISC_RT AS ot3Rate " +
	"FROM MESC1TS_LABOR_RATE L"
);

var getCountForLaborRatesQuery = WL.Server.createSQLStatement(
	"SELECT COUNT(*) AS count " +
	"FROM MESC1TS_LABOR_RATE L"
);

var getPartialLaborRatesQuery = WL.Server.createSQLStatement(
	"SELECT L.SHOP_CD AS shopCode, L.CUSTOMER_CD AS customer, L.EQTYPE AS eqType, " +
	"CONVERT(VARCHAR(10), L.EFF_DTE, 21) AS effDate, CONVERT(VARCHAR(10), L.EXP_DTE, 21) AS expDate, L.REGULAR_RT AS regRate, " +
	"L.OVERTIME_RT AS ot1Rate, L.DOUBLETIME_RT AS ot2Rate, L.MISC_RT AS ot3Rate " +
	"FROM MESC1TS_LABOR_RATE L " +
	"ORDER BY L.SHOP_CD " +
	"OFFSET ? ROWS " +
	"FETCH NEXT ? ROWS ONLY"
);

// Add MAnual code description! TODO

var getRepairSTSCodesQuery = WL.Server.createSQLStatement(
	"SELECT R.MANUAL_CD AS manualCode, " +
	"R.MODE AS mode, MODE_DESC AS modeDesc, R.INDEX_ID AS indexId,  " +
	"I.INDEX_DESC AS indexDesc, R.REPAIR_CD AS repairCode, " +
	"R.REPAIR_DESC AS repairDesc " +
	"R.MAN_HOUR as manHour, " +
	"R.MAX_QUANTITY as maxQuantity, " + 
	"R.SHOP_MATERIAL_CEILING as shopMaterialCeiling " + 
	"FROM MESC1TS_REPAIR_CODE R " +
	"LEFT JOIN MESC1TS_MODE M " +
	"ON M.MODE = R.MODE " +
	"LEFT JOIN MESC1TS_INDEX I " +
	"ON R.INDEX_ID = I.INDEX_ID AND I.MODE = R.MODE AND R.MANUAL_CD = I.MANUAL_CD " + 
	"WHERE R.REPAIR_ACTIVE_SW = 'Y'"
);

var getPartialSTSCodesQuery = WL.Server.createSQLStatement(
	"SELECT R.MANUAL_CD AS manualCode, " +
	"R.MODE AS mode, MODE_DESC AS modeDesc, R.INDEX_ID AS indexId,  " +
	"IsNull(I.INDEX_DESC, '') AS indexDesc, R.REPAIR_CD AS repairCode, " +
	"R.REPAIR_DESC AS repairDesc, R.MAN_HOUR as manHour, " +
	"R.MAX_QUANTITY as maxQuantity, " +
	"R.SHOP_MATERIAL_CEILING as shopMaterialCeiling, MA.MANUAL_DESC AS manualDesc " +
	"FROM MESC1TS_REPAIR_CODE R " +
	"LEFT JOIN MESC1TS_MODE M " +
	"ON M.MODE = R.MODE " +
	"LEFT JOIN MESC1TS_MANUAL MA " +
	"ON MA.MANUAL_CD = R.MANUAL_CD " +
	"LEFT JOIN MESC1TS_INDEX I " +
	"ON R.INDEX_ID = I.INDEX_ID AND I.MODE = R.MODE AND R.MANUAL_CD = I.MANUAL_CD " +
	"WHERE R.REPAIR_ACTIVE_SW = 'Y' "+
	"ORDER BY R.REPAIR_CD " +
	"OFFSET ? ROWS " +
	"FETCH NEXT ? ROWS ONLY"
);

var getCountForRepairSTSCodesQuery = WL.Server.createSQLStatement(
	"SELECT COUNT(*) AS count " +
	"FROM MESC1TS_REPAIR_CODE R " +  
	"LEFT JOIN MESC1TS_MODE M " +  
	"ON M.MODE = R.MODE " +  
	"LEFT JOIN MESC1TS_INDEX I " +  
	"ON R.INDEX_ID = I.INDEX_ID AND I.MODE = R.MODE AND R.MANUAL_CD = I.MANUAL_CD " +
	"WHERE R.REPAIR_ACTIVE_SW = 'Y'"
);

var getContractsSearchQuery = WL.Server.createSQLStatement(
	"DECLARE @shopCode VARCHAR(100) = ? " +
	"DECLARE @repairCode VARCHAR(100) = ? + '%' " +
	"SELECT C.REPAIR_CD AS repairCode, C.MODE AS mode, C.CONTRACT_AMOUNT AS amount, " +
	"C.MANUAL_CD AS manual, CONVERT(VARCHAR(10), C.EFF_DTE, 21) AS effDate, " +
	"CONVERT(VARCHAR(10), C.EXP_DTE, 21) AS expDate " +
	"FROM MESC1TS_SHOP_CONT C WHERE " +
	"C.SHOP_CD = @shopCode AND C.REPAIR_CD LIKE @repairCode"
);

// Dropdown info

var getManualCodesQuery = WL.Server.createSQLStatement(
	"SELECT M.MANUAL_CD AS code FROM MESC1TS_MANUAL M"
)

var getPartsGroupCodesQuery = WL.Server.createSQLStatement(
	"SELECT G.PARTS_GROUP_CD AS code FROM MESC1TS_PARTS_GROUP G"
)

var getManufacturerCodesQuery = WL.Server.createSQLStatement(
	"SELECT M.MANUFCTR AS code FROM MESC1TS_MANUFACTUR M"
)

function onRequestSuccess(data, context) {
	return {
		isSuccessful : true,
		status : 200,
		errorCode : 0,
		errorMsg : null,
		invocationContext : context,
		invocationResult : data
	}
}

function onRequestFailure(errorMsg, errorCode, data, context) {
	return {
		isSuccessful : false,
		status : 200,
		errorCode : errorCode,
		errorMsg : errorMsg,
		invocationContext : context,
		data : data
	}
}

function invokeSQL(statement, parameters, context) {
	try {
		// Invoke SQL statement
		var returnData = WL.Server.invokeSQLStatement({
			preparedStatement : statement,
			parameters : parameters
		});
		
		// Return SQL statement data
		return returnData;

	} catch (error) {
		return onRequestFailure("Database SQL execution error thrown", 101, error, context);
	}
}

/**
 * @name userCredentialsValid
 * @desc Not used
 * @memberof Adapters.WOCodesAdapter
 */
function userCredentialsValid(username, token, context) {

	// Verify user and token in our database
	var invocationData = {
			adapter : "DBAdapterContainer",
			procedure : "verifyToken",
			parameters : [username, token, (context != undefined ? context : null)]
	};
	var data = WL.Server.invokeProcedure(invocationData);

	// Check returned data
	if (data != undefined && data.invocationResult != undefined) {
		
		// Return token validity
		return data.invocationResult.tokenValid === true;
	}
	
	// Incorrect data - possibly user does not exist in the database
	return false;
}

/**
 * @name getManualCodes
 * @desc Retrieves all manual codes from MESC1TS_MANUAL table
 * @param {String} username
 * @param {String} token - no token auth used
 * @param {Any} context - param that will be returned in the response
 * @returns {Object} - An array of manual codes
 * @memberof Adapters.WOCodesAdapter
 */
function getManualCodes(username, token, context) {
	// Sanity check + Token validity
	if (checkUsernameAndToken(username, token, context).valid === false) {
		return checkUsernameAndToken(username, token, context).response;
	}
	
	var data = invokeSQL(getManualCodesQuery, [], context);
	
	if (data.isSuccessful && data.resultSet) {
		// result needs to be parsed to return only necessary fields
		
		var results = data.resultSet;
		
		return onRequestSuccess(results, context);
	}
	
	return onRequestFailure("SQL error", 100, data, context);
}

/**
 * @name getPartsGroupCodes
 * @desc Retrieves all part group codes
 * from MESC1TS_PARTS_GROUP table
 * @param {String} username
 * @param {String} token - no token auth used
 * @param {Any} context - param that will be returned in the response
 * @returns {Object} - An array of part group codes
 * @memberof Adapters.WOCodesAdapter
 */
function getPartsGroupCodes(username, token, context) {
	// Sanity check + Token validity
	if (checkUsernameAndToken(username, token, context).valid === false) {
		return checkUsernameAndToken(username, token, context).response;
	}
	
	var data = invokeSQL(getPartsGroupCodesQuery, [], context);
	
	if (data.isSuccessful && data.resultSet) {
		// result needs to be parsed to return only necessary fields
		
		var results = data.resultSet;
		
		return onRequestSuccess(results, context);
	}
	
	return onRequestFailure("SQL error", 100, data, context);
}

/**
 * @name getManufacturerCodes
 * @desc Retrieves all manufacturer codes
 * from MESC1TS_MANUFACTUR table
 * @param {String} username
 * @param {String} token - no token auth used
 * @param {Any} context - param that will be returned in the response
 * @returns {Object} - An array of manufacturer codes
 * @memberof Adapters.WOCodesAdapter
 */
function getManufacturerCodes(username, token, context) {
	// Sanity check + Token validity
	if (checkUsernameAndToken(username, token, context).valid === false) {
		return checkUsernameAndToken(username, token, context).response;
	}
	
	var data = invokeSQL(getManufacturerCodesQuery, [], context);
	
	if (data.isSuccessful && data.resultSet) {
		// result needs to be parsed to return only necessary fields
		
		var results = data.resultSet;
		
		return onRequestSuccess(results, context);
	}
	
	return onRequestFailure("SQL error", 100, data, context);
}

/**
 * @name getDamageCodes
 * @desc Retrieves all damage codes
 * from MESC1TS_DAMAGE table
 * @param {String} username
 * @param {String} token - no token auth used
 * @param {Any} context - param that will be returned in the response
 * @returns {Object} - An array of damage codes and associated info
 * @memberof Adapters.WOCodesAdapter
 */
function getDamageCodes(username, token, context) {
	// Sanity check + Token validity
	if (checkUsernameAndToken(username, token, context).valid === false) {
		return checkUsernameAndToken(username, token, context).response;
	}
	
	var data = invokeSQL(getDamageCodesQuery, [], context);
	
	if (data.isSuccessful && data.resultSet) {
		// result needs to be parsed to return only necessary fields
		
		var results = data.resultSet;
		
		return onRequestSuccess(results, context);
	}
	
	return onRequestFailure("SQL error", 100, data, context);
}

/**
 * @name getRepairCodes
 * @desc Retrieves all repair codes
 * from MESC1TS_REPAIR_CODE table
 * @param {String} username
 * @param {String} token - no token auth used
 * @param {Any} context - param that will be returned in the response
 * @returns {Object} - An array of repair codes,
 * their description and mode
 * @memberof Adapters.WOCodesAdapter
 */
function getRepairCodes(username, token, context) {
	// Sanity check + Token validity
	if (checkUsernameAndToken(username, token, context).valid === false) {
		return checkUsernameAndToken(username, token, context).response;
	}
	
	var data = invokeSQL(getRepairCodesQuery, [], context);
	
	if (data.isSuccessful && data.resultSet) {
		// result needs to be parsed to return only necessary fields
		
		var results = data.resultSet;
		
		return onRequestSuccess(results, context);
	}
	
	return onRequestFailure("SQL error", 100, data, context);
}

/**
 * @name getPartCodes
 * @desc Retrieves all part codes
 * from MESC1TS_MASTER_PART table
 * @param {String} username
 * @param {String} token - no token auth used
 * @param {Any} context - param that will be returned in the response
 * @returns {Object} - An array of part codes and
 * their description
 * @memberof Adapters.WOCodesAdapter
 */
function getPartCodes(username, token, context) {
	// Sanity check + Token validity
	if (checkUsernameAndToken(username, token, context).valid === false) {
		return checkUsernameAndToken(username, token, context).response;
	}
	
	var data = invokeSQL(getPartCodesQuery, [], context);
	
	if (data.isSuccessful && data.resultSet) {
		// result needs to be parsed to return only necessary fields
		
		var results = data.resultSet;
		
		return onRequestSuccess(results, context);
	}
	
	return onRequestFailure("SQL error", 100, data, context);
}

/**
 * @name getPartCodesForModes
 * @desc Helper method used to retrieves a partial number of part codes entries
 * from MESC1TS_MASTER_PART table
 * @param {String} username
 * @param {String} token - no token auth used
 * @param {Number} offset - offset for sql query
 * @param {Number} limit - limit for sql query
 * @param {Any} context - param that will be returned in the response
 * @returns {Object} - An array of part codes and
 * associated info
 * @memberof Adapters.WOCodesAdapter
 */
function getPartCodesForModes(username, token, offset, limit, context) {
	// Sanity check + Token validity
	if (checkUsernameAndToken(username, token, context).valid === false) {
		return checkUsernameAndToken(username, token, context).response;
	}
	
	if (typeof offset != 'number' || typeof limit != 'number') {
		return onRequestFailure("offset or limit query arguments not provided or incorrect format", 107, null, context);
	}
	
	var data = invokeSQL(getPartCodesForModesQuery, [offset, limit], context);
	
	if (data.isSuccessful && data.resultSet) {
		// result needs to be parsed to return only necessary fields
		
		var results = data.resultSet;
		
		return onRequestSuccess(results, context);
	}
	
	return onRequestFailure("SQL error", 100, data, context);
}

/**
 * @name getTotalCountForCodesForModes
 * @desc Helper method used to retrieves the total count of part codes entries
 * from MESC1TS_MASTER_PART table
 * @param {String} username
 * @param {String} token - no token auth used
 * @param {Any} context - param that will be returned in the response
 * @returns {Object} - total count of entries from query result
 * @memberof Adapters.WOCodesAdapter
 */
function getTotalCountForCodesForModes(username, token, context) {
	// Sanity check + Token validity
	if (checkUsernameAndToken(username, token, context).valid === false) {
		return checkUsernameAndToken(username, token, context).response;
	}
	
	var data = invokeSQL(getTotalCountForCodesForModesQuery, [], context);
	
	if (data.isSuccessful && data.resultSet) {
		// result needs to be parsed to return only necessary fields
		if (data.resultSet.length > 0 && data.resultSet[0].count) {
			var count = data.resultSet[0].count;
			return onRequestSuccess(count, context);
		} else {
			return onRequestSuccess(0, context);
		}
	}
	
	return onRequestFailure("SQL error", 100, data, context);
}

/**
 * @name getAssociatePartsWithCodes
 * @desc Not used, getPartialAssociatePartsWithCodes and 
 * getTotalCountForAssociatePartsWithCodes are used instead
 * @memberof Adapters.WOCodesAdapter
 */
function getAssociatePartsWithCodes(username, token, context) {
	// Sanity check + Token validity
	if (checkUsernameAndToken(username, token, context).valid === false) {
		return checkUsernameAndToken(username, token, context).response;
	}
	
	// Make 2 requests - too much data
	
	var data = invokeSQL(getAssociatePartsWithCodesQuery, [], context);
	
	if (data.isSuccessful && data.resultSet) {
		// result needs to be parsed to return only necessary fields
		
		var results = data.resultSet;
		
		return onRequestSuccess(results, context);
	}
	
	return onRequestFailure("SQL error", 100, data, context);
}

/**
 * @name getTotalCountForAssociatePartsWithCodes
 * @desc Helper method used to retrieves the total count of code entries
 * from MESC1TS_RPRCODE_PART table
 * @param {String} username
 * @param {String} token - no token auth used
 * @param {Any} context - param that will be returned in the response
 * @returns {Object} - total count of entries from query result
 * @memberof Adapters.WOCodesAdapter
 */
function getTotalCountForAssociatePartsWithCodes(username, token, context) {
	// Sanity check + Token validity
	if (checkUsernameAndToken(username, token, context).valid === false) {
		return checkUsernameAndToken(username, token, context).response;
	}
	
	var data = invokeSQL(getTotalCountForAssociatePartsWithCodesQuery, [], context);
	
	if (data.isSuccessful && data.resultSet) {
		// result needs to be parsed to return only necessary fields
		if (data.resultSet.length > 0 && data.resultSet[0].count) {
			var count = data.resultSet[0].count;
			return onRequestSuccess(count, context);
		} else {
			return onRequestSuccess(0, context);
		}
	}
	
	return onRequestFailure("SQL error", 100, data, context);
}

/**
 * @name getPartialAssociatePartsWithCodes
 * @desc Helper method used to retrieves a partial number of code entries
 * from MESC1TS_RPRCODE_PART table
 * @param {String} username
 * @param {String} token - no token auth used
 * @param {Number} offset - offset for sql query
 * @param {Number} limit - limit for sql query
 * @param {Any} context - param that will be returned in the response
 * @returns {Object} - An array of codes and
 * associated info
 * @memberof Adapters.WOCodesAdapter
 */
function getPartialAssociatePartsWithCodes(username, token, offset, limit, context) {
	// Sanity check + Token validity
	if (checkUsernameAndToken(username, token, context).valid === false) {
		return checkUsernameAndToken(username, token, context).response;
	}
	
	if (typeof offset != 'number' || typeof limit != 'number') {
		return onRequestFailure("offset or limit query arguments not provided or incorrect format", 107, null, context);
	}
	
	var data = invokeSQL(getPartialAssociatePartsWithCodesQuery, [offset, limit], context);
	
	if (data.isSuccessful && data.resultSet) {
		// result needs to be parsed to return only necessary fields
		
		var results = data.resultSet;
		
		return onRequestSuccess(results, context);
	}
	
	return onRequestFailure("SQL error", 100, data, context);
}

/**
 * @name getRepairLocCodes
 * @desc Retrieves all repair loc codes
 * from MESC1TS_REPAIR_LOC table
 * @param {String} username
 * @param {String} token - no token auth used
 * @param {Any} context - param that will be returned in the response
 * @returns {Object} - An array of repair loc codes and associated info
 * @memberof Adapters.WOCodesAdapter
 */
function getRepairLocCodes(username, token, context) {
	// Sanity check + Token validity
	if (checkUsernameAndToken(username, token, context).valid === false) {
		return checkUsernameAndToken(username, token, context).response;
	}
	
	var data = invokeSQL(getRepairLocCodesQuery, [], context);
	
	if (data.isSuccessful && data.resultSet) {
		// result needs to be parsed to return only necessary fields
		
		var results = data.resultSet;
		
		return onRequestSuccess(results, context);
	}
	
	return onRequestFailure("SQL error", 100, data, context);
}

/**
 * @name getAllShopsInfo
 * @desc Retrieves all shops and 
 * their associated info
 * from MESC1TS_SHOP table
 * @param {String} username
 * @param {String} token - no token auth used
 * @param {Any} context - param that will be returned in the response
 * @returns {Object} - An array of shop codes and associated info
 * @memberof Adapters.WOCodesAdapter
 */
function getAllShopsInfo(username, token, context) {
	// Sanity check + Token validity
	if (checkUsernameAndToken(username, token, context).valid === false) {
		return checkUsernameAndToken(username, token, context).response;
	}
	
	var data = invokeSQL(getAllShopsInfoQuery, [], context);
	
	if (data.isSuccessful && data.resultSet) {
		// result needs to be parsed to return only necessary fields
		
		var results = data.resultSet;
		
		return onRequestSuccess(results, context);
	}
	
	return onRequestFailure("SQL error", 100, data, context);
}

/**
 * @name getAllShopLimits
 * @desc Not used, getPartialShopLimits and 
 * getTotalCountForAllShopLimits are used instead
 * @memberof Adapters.WOCodesAdapter
 */
function getAllShopLimits(username, token, context) {
	// Sanity check + Token validity
	if (checkUsernameAndToken(username, token, context).valid === false) {
		return checkUsernameAndToken(username, token, context).response;
	}
	
	var data = invokeSQL(getAllShopLimitsQuery, [], context);
	
	if (data.isSuccessful && data.resultSet) {
		// result needs to be parsed to return only necessary fields
		
		var results = data.resultSet;
		
		return onRequestSuccess(results, context);
	}
	
	return onRequestFailure("SQL error", 100, data, context);
}

/**
 * @name getTotalCountForAllShopLimits
 * @desc Helper method used to retrieves the total count of code entries
 * from MESC1TS_SHOP_LIMITS table
 * @param {String} username
 * @param {String} token - no token auth used
 * @param {Any} context - param that will be returned in the response
 * @returns {Object} - total count of entries from query result
 * @memberof Adapters.WOCodesAdapter
 */
function getTotalCountForAllShopLimits(username, token, context) {
	// Sanity check + Token validity
	if (checkUsernameAndToken(username, token, context).valid === false) {
		return checkUsernameAndToken(username, token, context).response;
	}
	
	var data = invokeSQL(getCountForShopLimitsQuery, [], context);
	
	if (data.isSuccessful && data.resultSet) {
		// result needs to be parsed to return only necessary fields
		if (data.resultSet.length > 0 && data.resultSet[0].count) {
			var count = data.resultSet[0].count;
			return onRequestSuccess(count, context);
		} else {
			return onRequestSuccess(0, context);
		}
	}
	
	return onRequestFailure("SQL error", 100, data, context);
}

/**
 * @name getPartialShopLimits
 * @desc Helper method used to retrieves a partial number of entries,
 * representing various pricing limits associated with shops
 * from MESC1TS_SHOP_LIMITS table
 * @param {String} username
 * @param {String} token - no token auth used
 * @param {Number} offset - offset for sql query
 * @param {Number} limit - limit for sql query
 * @param {Any} context - param that will be returned in the response
 * @returns {Object} - An array of shop limits and
 * associated info
 * @memberof Adapters.WOCodesAdapter
 */
function getPartialShopLimits(username, token, offset, limit, context) {
	// Sanity check + Token validity
	if (checkUsernameAndToken(username, token, context).valid === false) {
		return checkUsernameAndToken(username, token, context).response;
	}
	
	if (typeof offset != 'number' || typeof limit != 'number') {
		return onRequestFailure("offset or limit query arguments not provided or incorrect format", 107, null, context);
	}
	
	var data = invokeSQL(getPartialShopLimitsQuery, [offset, limit], context);
	
	if (data.isSuccessful && data.resultSet) {
		// result needs to be parsed to return only necessary fields
		
		var results = data.resultSet;
		
		return onRequestSuccess(results, context);
	}
	
	return onRequestFailure("SQL error", 100, data, context);
}

/**
 * @name getLaborRates
 * @desc Not used, getTotalCountForLaborRates and 
 * getPartialLaborRates are used instead
 * @memberof Adapters.WOCodesAdapter
 */
function getLaborRates(username, token, context) {
	// Sanity check + Token validity
	if (checkUsernameAndToken(username, token, context).valid === false) {
		return checkUsernameAndToken(username, token, context).response;
	}
	
	var data = invokeSQL(getLaborRatesQuery, [], context);
	
	if (data.isSuccessful && data.resultSet) {
		// result needs to be parsed to return only necessary fields
		
		var results = data.resultSet;
		
		return onRequestSuccess(results, context);
	}
	
	return onRequestFailure("SQL error", 100, data, context);
}

/**
 * @name getTotalCountForLaborRates
 * @desc Helper method used to retrieves the total count of entries
 * from MESC1TS_LABOR_RATE table
 * @param {String} username
 * @param {String} token - no token auth used
 * @param {Any} context - param that will be returned in the response
 * @returns {Object} - total count of entries from query result
 * @memberof Adapters.WOCodesAdapter
 */
function getTotalCountForLaborRates(username, token, context) {
	// Sanity check + Token validity
	if (checkUsernameAndToken(username, token, context).valid === false) {
		return checkUsernameAndToken(username, token, context).response;
	}
	
	var data = invokeSQL(getCountForLaborRatesQuery, [], context);
	
	if (data.isSuccessful && data.resultSet) {
		// result needs to be parsed to return only necessary fields
		if (data.resultSet.length > 0 && data.resultSet[0].count) {
			var count = data.resultSet[0].count;
			return onRequestSuccess(count, context);
		} else {
			return onRequestSuccess(0, context);
		}
	}
	
	return onRequestFailure("SQL error", 100, data, context);
}

/**
 * @name getPartialLaborRates
 * @desc Helper method used to retrieves a partial number of entries,
 * representing various labor limits associated with shops
 * from MESC1TS_LABOR_RATE table
 * @param {String} username
 * @param {String} token - no token auth used
 * @param {Number} offset - offset for sql query
 * @param {Number} limit - limit for sql query
 * @param {Any} context - param that will be returned in the response
 * @returns {Object} - An array of shop labor rates and
 * associated info
 * @memberof Adapters.WOCodesAdapter
 */
function getPartialLaborRates(username, token, offset, limit, context) {
	// Sanity check + Token validity
	if (checkUsernameAndToken(username, token, context).valid === false) {
		return checkUsernameAndToken(username, token, context).response;
	}
	
	if (typeof offset != 'number' || typeof limit != 'number') {
		return onRequestFailure("offset or limit query arguments not provided or incorrect format", 107, null, context);
	}
	
	var data = invokeSQL(getPartialLaborRatesQuery, [offset, limit], context);
	
	if (data.isSuccessful && data.resultSet) {
		var results = data.resultSet;
		return onRequestSuccess(results, context);
	}
	
	return onRequestFailure("SQL error", 100, data, context);
}

/**
 * @name getRepairSTSCodes
 * @desc Not used, getTotalCountForRepairSTSCodes and 
 * getPartialRepairSTSCodes are used instead
 * @memberof Adapters.WOCodesAdapter
 */
function getRepairSTSCodes(username, token, context) {
	// Sanity check + Token validity
	if (checkUsernameAndToken(username, token, context).valid === false) {
		return checkUsernameAndToken(username, token, context).response;
	}
	
	var data = invokeSQL(getRepairSTSCodesQuery, [], context);
	
	if (data.isSuccessful && data.resultSet) {
		// result needs to be parsed to return only necessary fields
		
		var results = data.resultSet;
		
		return onRequestSuccess(results, context);
	}
	
	return onRequestFailure("SQL error", 100, data, context);
}

/**
 * @name getTotalCountForRepairSTSCodes
 * @desc Helper method used to retrieves the total count of entries
 * from MESC1TS_REPAIR_CODE table
 * @param {String} username
 * @param {String} token - no token auth used
 * @param {Any} context - param that will be returned in the response
 * @returns {Object} - total count of entries from query result
 * @memberof Adapters.WOCodesAdapter
 */
function getTotalCountForRepairSTSCodes(username, token, context) {
	// Sanity check + Token validity
	if (checkUsernameAndToken(username, token, context).valid === false) {
		return checkUsernameAndToken(username, token, context).response;
	}
	
	var data = invokeSQL(getCountForRepairSTSCodesQuery, [], context);
	
	if (data.isSuccessful && data.resultSet) {
		// result needs to be parsed to return only necessary fields
		if (data.resultSet.length > 0 && data.resultSet[0].count) {
			var count = data.resultSet[0].count;
			return onRequestSuccess(count, context);
		} else {
			return onRequestSuccess(0, context);
		}
	}
	
	return onRequestFailure("SQL error", 100, data, context);
}

/**
 * @name getPartialRepairSTSCodes
 * @desc Helper method used to retrieves a partial number of entries,
 * representing repair information associated with repair codes
 * from MESC1TS_REPAIR_CODE table
 * @param {String} username
 * @param {String} token - no token auth used
 * @param {Number} offset - offset for sql query
 * @param {Number} limit - limit for sql query
 * @param {Any} context - param that will be returned in the response
 * @returns {Object} - An array of shop labor rates and
 * associated info
 * @memberof Adapters.WOCodesAdapter
 */
function getPartialRepairSTSCodes(username, token, offset, limit, context) {
	// Sanity check + Token validity
	if (checkUsernameAndToken(username, token, context).valid === false) {
		return checkUsernameAndToken(username, token, context).response;
	}
	
	if (typeof offset != 'number' || typeof limit != 'number') {
		return onRequestFailure("offset or limit query arguments not provided or incorrect format", 107, null, context);
	}
	
	var data = invokeSQL(getPartialSTSCodesQuery, [offset, limit], context);
	
	if (data.isSuccessful && data.resultSet) {
		// result needs to be parsed to return only necessary fields
		
		var results = data.resultSet;
		
		return onRequestSuccess(results, context);
	}
	
	return onRequestFailure("SQL error", 100, data, context);
}

/**
 * @name getContractsSearch
 * @desc Retrieves shop contracts associated with
 * shopCode and repairCode params
 * from MESC1TS_SHOP_CONT table
 * @param {String} username
 * @param {String} token - no token auth used
 * @param {String} shopCode
 * @param {String} repairCode
 * @param {Any} context - param that will be returned in the response
 * @returns {Object} - An array of shop contracts
 * @memberof Adapters.WOCodesAdapter
 */
function getContractsSearch(username, token, shopCode, repairCode, context) {
	// Sanity check + Token validity
	if (checkUsernameAndToken(username, token, context).valid === false) {
		return checkUsernameAndToken(username, token, context).response;
	}
	
	var data = invokeSQL(getContractsSearchQuery, [shopCode, repairCode], context);
	
	if (data.isSuccessful && data.resultSet) {
		// result needs to be parsed to return only necessary fields
		
		var results = data.resultSet;
		
		return onRequestSuccess(results, context);
	}
	
	return onRequestFailure("SQL error", 100, data, context);
}

/*
 * Helper functions
 */

function checkUsernameAndToken(username, token, context) {
	return {valid: true};
	if (username == undefined || username.length == 0 || token == undefined || token.length == 0) {
		return {valid: false, response: onRequestFailure("User credentials not provided (username and/or token)", 105, null, context)}
	} 
	if (!userCredentialsValid(username, token, context)) {
		WL.Logger.info("user credentials invalid: " + username + ":" + token);
		
		// Either user does not exist or user token is invalid or expired - return an error
		return {valid: false, response: onRequestFailure("User does not exist or token expired or invalid (user credentials validity)",
				102, null, context)}
	}
	WL.Logger.info("user \"" + username + "\"token \"" + token + "\" are valid");
	return {valid: true};
}

function dateToSQLDateTime(date) {
	if (isNaN(date.getUTCFullYear()))
		return;
	
	return date.getUTCFullYear() + '-'
			+ ('00' + (date.getUTCMonth() + 1)).slice(-2) + '-'
			+ ('00' + date.getUTCDate()).slice(-2) + ' '
			+ ('00' + date.getUTCHours()).slice(-2) + ':'
			+ ('00' + date.getUTCMinutes()).slice(-2) + ':'
			+ ('00' + date.getUTCSeconds()).slice(-2);
}

function isNaN(variable) {
	return variable !== variable;
}
