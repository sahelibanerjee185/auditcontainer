CONNECT TO AUDIT;

DROP TABLE DEV.manuals;


CREATE TABLE DEV.manuals (
	"manualID"  INTEGER          NOT NULL GENERATED ALWAYS AS IDENTITY (START WITH 1 INCREMENT BY 1, NOMAXVALUE),
	"name" VARCHAR(255)			 NOT NULL,
	"locationPath" VARCHAR(255)  NOT NULL,
	"language" VARCHAR(255)      NOT NULL,
	"extension" VARCHAR(255)     NOT NULL,
	"addedDate" TIMESTAMP		 DEFAULT CURRENT TIMESTAMP,
	PRIMARY KEY("manualID")
);

INSERT INTO dev.manuals ("name", "locationPath", "language", "extension")
VALUES 
	('Mode02 - Steel Reefer Box', 'http://group.apmoller.net/bu/mli/func/operations/Networksourcing/Equipmentandinland/Equipment/Emr/EMR%20library/Box%20related/Manuals/STS%20codes/Mode02%20-%20Steel%20Reefer%20Box.pdf', 'EN', 'pdf'),
	('Mode04 - Flatrack Equipment', 'http://group.apmoller.net/bu/mli/func/operations/Networksourcing/Equipmentandinland/Equipment/Emr/EMR%20library/Box%20related/Manuals/STS%20codes/Mode04%20-%20Flatrack%20Equipment.pdf', 'EN', 'pdf'),
	('Mode05 - Opentop Equipment', 'http://group.apmoller.net/bu/mli/func/operations/Networksourcing/Equipmentandinland/Equipment/Emr/EMR%20library/Box%20related/Manuals/STS%20codes/Mode05%20-%20Opentop%20Equipment.pdf', 'EN', 'pdf'),
	('Mode31-33 - Genset Equipment', 'http://group.apmoller.net/bu/mli/func/operations/Networksourcing/Equipmentandinland/Equipment/Emr/EMR%20library/Box%20related/Manuals/STS%20codes/Mode31-33%20-%20Genset%20Equipment.pdf', 'EN', 'pdf'),
	('Mode41-43 - Reefer Unit', 'http://group.apmoller.net/bu/mli/func/operations/Networksourcing/Equipmentandinland/Equipment/Emr/EMR%20library/Box%20related/Manuals/STS%20codes/Mode41-43%20-%20Reefer%20Unit.pdf', 'EN', 'pdf'),
	('Mode45 - CA Reefer Unit', 'http://group.apmoller.net/bu/mli/func/operations/Networksourcing/Equipmentandinland/Equipment/Emr/EMR%20library/Box%20related/Manuals/STS%20codes/Mode45%20-%20CA%20Reefer%20Unit.pdf', 'EN', 'pdf'),
	('Mode71- Powerpack', 'http://group.apmoller.net/bu/mli/func/operations/Networksourcing/Equipmentandinland/Equipment/Emr/EMR%20library/Box%20related/Manuals/STS%20codes/Mode71-%20Powerpack.pdf', 'EN', 'pdf'),
	('Reefer Repair Manual', '/Reefer%20Repair%20Manual.pdf', 'EN', 'pdf'),
	('Repair Manual - Steel Dry Container v2.1', '/Repair%20Manual%20-%20Steel%20Dry%20Container%20v2.1.pdf', 'EN', 'pdf'),
	('STS Manual - Steel Dry Container (Mode 03) v2', '/STS_Manual_Steel_Dry_Container_(Mode_03)_v2.pdf', 'EN', 'pdf'),
	('MCIC_V7_2016', '/MCIC_V7_2016.pdf', 'EN', 'pdf'),
	('MRIC_V4_2016', '/MRIC_V4_2016.pdf', 'EN', 'pdf');
