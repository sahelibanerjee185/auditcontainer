package com.ibm.audit;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.Date;

import org.apache.commons.io.IOUtils;

public class FileHandler {

	public FileHandler() {
	}
	
//Generate a new file in the folder described by path and write the fileData into it
//Returns new file name
	public String writeStringToFile(String path, String fileData){
		try {			
			File createdFile = File.createTempFile(new Long(new Date().getTime()).toString(), ".archive" , new File(path));
			PrintWriter out = new PrintWriter(createdFile);
			out.print(fileData);
			out.close();
			return createdFile.getName();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public String writePdfAsBinaryToFile(String path, String fileData, String fileName){
		
		//TODO fileData base64 to binary before writing into FILE
		try{	
			File createdFile = new File(path + "/" + fileName);
			PrintWriter out = new PrintWriter(createdFile);
			out.print(fileData);
			out.close();
			return createdFile.getName();
		}catch(IOException e){
			e.printStackTrace();
		}
		
		return null;
	}
	
	public String readStringFromFile(String path){
		try {
			InputStream in = new FileInputStream(path);
			return IOUtils.toString(in);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

}
