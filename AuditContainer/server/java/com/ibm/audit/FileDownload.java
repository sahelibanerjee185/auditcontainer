package com.ibm.audit;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;

//import java.util.logging.Logger;

//import java.util.Base64;
import javax.xml.bind.DatatypeConverter;



public class FileDownload {

//	private static final Logger log = Logger.getLogger( FileDownload.class.getName() );
	
	public String download(String url){
		URL link = null;
		ByteArrayOutputStream out = null;
		InputStream in;
		byte[] response = null;
		HttpURLConnection URLConnection = null;
		String userCredentials = "mro209:Sanfran49ers3";
		String basicAuth = "Basic " + new String(DatatypeConverter.printBase64Binary(userCredentials.getBytes()));
//		String basicAuth = "Basic bXJvMjA5OlNhbmZyYW40OWVyczM=";
		
			 try {
				link = new URL(url);
			} catch (MalformedURLException e1) {
				e1.printStackTrace();
				return "0";
			}
			 try {
				URLConnection = (HttpURLConnection)link.openConnection();
			} catch (IOException e1) {
				e1.printStackTrace();
				return "1";
			}
			 URLConnection.setRequestProperty ("Authorization", basicAuth);
			 try {
				URLConnection.setRequestMethod("GET");
			} catch (ProtocolException e1) {
				e1.printStackTrace();
				return "2";
			}
			 URLConnection.setDoOutput(true);
		
			 try {
//				 	log.info("URL Connection");
//				 	log.info(URLConnection.getURL().toString());
//				 	log.info(URLConnection.getContent().toString());
				 System.out.println("URL Connection");
				 System.out.println(URLConnection.getURL().toString());
					in = new BufferedInputStream(URLConnection.getInputStream());
			} catch (IOException e1) {
				e1.printStackTrace();
				return "3";
			}	
			 
//		File file = new File(url);
//		try {
//			in = new FileInputStream(file);
//		} catch (FileNotFoundException e1) {
//			e1.printStackTrace();
//			return "0";
//		}
		
		try {
			 out = new ByteArrayOutputStream();
			
			 byte[] buf = new byte[1024];
			 int n = 0;
			 while (-1!=(n=in.read(buf)))
			 {
			    out.write(buf, 0, n);
			 }
			 response = out.toByteArray();
			 
			if (out != null)
				out.close();
			if (in != null)
				in.close();
		} catch (IOException e) {
			e.printStackTrace();
			return "4";
		}
//		return Base64.getEncoder().encodeToString(response);
		return new String(DatatypeConverter.printBase64Binary(response));
	}

}

