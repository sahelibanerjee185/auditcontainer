//
//  PhotoViewController.h
//  PhotoViewController
//
//  Created by DanTascau on 5/31/16.
//  Copyright © 2016 IBM. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for PhotoViewController.
FOUNDATION_EXPORT double PhotoViewControllerVersionNumber;

//! Project version string for PhotoViewController.
FOUNDATION_EXPORT const unsigned char PhotoViewControllerVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <PhotoViewController/PublicHeader.h>

