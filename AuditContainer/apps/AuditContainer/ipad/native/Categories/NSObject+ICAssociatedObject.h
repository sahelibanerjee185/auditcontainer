//
//  NSObject+ICAssociatedObject.h
//  Object's Associated Object
//
//  Created by Iulian Corcoja on 5/1/14.
//  Copyright (c) 2015 Iulian Corcoja. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSObject (AssociatedObject)

@property (nonatomic, strong) id associatedObject;

@end
