//
//  HTMLtoPDF.m
//  VesselAuditVesselAuditIpad
//
//  Created by Iulian Corcoja on 1/14/16.
//
//

#import <objc/runtime.h>
#import "HTMLtoPDF.h"

#define kDefaultPPI 72.0f

#pragma mark - HTML to PDF page renderer interface

@interface HTMLtoPDFPageRenderer : UIPrintPageRenderer

@property (nonatomic, assign) CGFloat topAndBottomMarginSize;

@property (nonatomic, assign) CGFloat leftAndRightMarginSize;

@end

#pragma mark - HTML to PDF page renderer implementation

@implementation HTMLtoPDFPageRenderer

- (CGRect)paperRect
{
	return UIGraphicsGetPDFContextBounds();
}

- (CGRect)printableRect
{
	return CGRectInset([self paperRect], self.leftAndRightMarginSize, self.topAndBottomMarginSize);
}

@end

#pragma mark - HTML to PDF interface

@interface HTMLtoPDF () <UIWebViewDelegate>

@property (nonatomic, strong) UIWebView *webView;

@property (nonatomic, copy) void (^completionHandler)(BOOL success, NSData *pdfData);

@end

#pragma mark - HTML to PDF implementation

@implementation HTMLtoPDF

#pragma mark - Singleton getter

+ (HTMLtoPDF *)sharedInstance
{
	static HTMLtoPDF *sharedInstance;
	static dispatch_once_t predicate;
	
	dispatch_once(&predicate, ^{
		
		if (!sharedInstance) {
			
			// Create a new instance of HTML to PDF converter
			sharedInstance = [[HTMLtoPDF alloc] init];
		}
	});
	return sharedInstance;
}

#pragma mark - Instance methods

- (void)pdfFromHTMLString:(NSString *)html completion:(void (^)(BOOL success, NSData *pdfData))completion
{
    // Sanity check
    if (!html || html.length == 0) {
        completion(NO, nil);
        return;
    }
    
	// Stop previous conversions
	if (self.webView.isLoading) {
		[self.webView stopLoading];
	}
	
	// Set completion handler
	self.completionHandler = completion;
	
	// Load HTML string
	[self.webView loadHTMLString:html baseURL:[NSURL fileURLWithPath:[[NSBundle mainBundle] bundlePath]]];
}

#pragma mark - Setters and getters

- (UIWebView *)webView
{
	if (!_webView) {
		
		// Createa a new instance of web view
		_webView = [[UIWebView alloc] init];
		_webView.delegate = self;
	}
	return _webView;
}

#pragma mark - Web view delegate methods

- (void)webViewDidStartLoad:(UIWebView *)webView
{
	NSLog(@"web view started loading HTML page");
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
	// Get print formatter and page renderer
	UIPrintFormatter *printFormatter = webView.viewPrintFormatter;
	HTMLtoPDFPageRenderer *pageRenderer = [[HTMLtoPDFPageRenderer alloc] init];
	
	// Set default 1/4" margins
	pageRenderer.topAndBottomMarginSize = 0.25f * 72.0f;
	pageRenderer.leftAndRightMarginSize = 0.25f * 72.0f;
	
	// Start rendering the HTML with the first page
	[pageRenderer addPrintFormatter:printFormatter startingAtPageAtIndex:0];
	
	self.pdfData = [NSMutableData data];
	
	// Get page size depending on current locale (A4 for metric system, US letter for empirical system)
	CGSize pageSize;
	NSLocale *locale = [NSLocale currentLocale];
	BOOL useMetric = [[locale objectForKey:NSLocaleUsesMetricSystem] boolValue];

	if (useMetric) {
		
		// A4 page size
		pageSize = CGSizeMake(8.26666667f * kDefaultPPI, 11.6916667f * kDefaultPPI);
	} else {

		// US letter page size
		pageSize = CGSizeMake(8.5f * kDefaultPPI, 11.0f * kDefaultPPI);
	}
	CGRect pageRect = CGRectMake(0, 0, pageSize.width, pageSize.height);

	// Start rendering PDF
	UIGraphicsBeginPDFContextToData(self.pdfData, pageRect, nil);
	[pageRenderer prepareForDrawingPages:NSMakeRange(0, 1)];

	NSInteger pages = [pageRenderer numberOfPages];
	
	for (NSInteger i = 0; i < pages; i++) {
		
		// Draw the page
		UIGraphicsBeginPDFPage();
		[pageRenderer drawPageAtIndex:i inRect:pageRenderer.paperRect];
	}

	// Eng rendering the PDF
	UIGraphicsEndPDFContext();
	
	// Call completion block with success set to YES and PDF data
	if (self.completionHandler) {
        dispatch_async(dispatch_get_main_queue(), ^{
            self.completionHandler(YES, self.pdfData);
            
            // Delete completion handler to prevent calling it multiple times
            self.completionHandler = nil;
        });
	}
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
	// Call completion block with success set to NO
	if (self.completionHandler) {
        dispatch_async(dispatch_get_main_queue(), ^{
            self.completionHandler(NO, nil);
            
            // Delete completion handler to prevent calling it multiple times
            self.completionHandler = nil;
        });
	}
}

@end
