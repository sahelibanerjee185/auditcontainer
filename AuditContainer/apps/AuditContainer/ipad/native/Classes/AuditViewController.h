//
//  AuditViewController.h
//  AuditContainerAuditContainerIpad
//
//  Created by Iulian Corcoja on 10/29/15.
//
//

#import <UIKit/UIKit.h>
#import <IBMMobileFirstPlatformFoundationHybrid/IBMMobileFirstPlatformFoundationHybrid.h>
#import <Cordova/CDVViewController.h>
#import "AuditContainerWebView.h"
#import "OpticalCharacterRecognitionController.h"
#import "PDFPreviewItem.h"
#import "Header.h"
#import "Footer.h"

@interface AuditViewController : CDVViewController <UIScrollViewDelegate, WLActionReceiver, WLDelegate,
													AuditContainerWebViewDelegate, HeaderDelegate, FooterDelegate,
                                                    OpticalCharacterRecognitionControllerDelegate,
                                                    QLPreviewControllerDataSource, QLPreviewControllerDelegate>

@property (nonatomic, strong) IBOutlet AuditContainerWebView* webView;

@end
