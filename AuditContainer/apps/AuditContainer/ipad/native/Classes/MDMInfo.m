//
//  MDMInfo.m
//  DepotAuditDepotAuditIpad
//
//  Created by Iulian Corcoja on 7/27/16.
//
//

#import <AppConnect/AppConnect.h>
#import "MDMInfo.h"

#pragma mark - Class implementation

@implementation MDMInfo

#pragma mark - Class methods

+ (void)getMMDInfo:(MDMInfoCompletionBlock)completion
{
    
    completion(nil);
    return;
    
    
#if TARGET_IPHONE_SIMULATOR
    
    completion(nil);
    return;
    
#endif
    
    // Sanity check
    if (!completion) {
        return;
    }
    
    // Check if app connect is already ready
    if ([[AppConnect sharedInstance] isReady]) {
        
        NSLog(@"MDMInfo - AppConnect was already ready");
        
        // App connnect is ready, get the config file and call completion block
        completion([[AppConnect sharedInstance] config]);
        return;
    }
    
    NSLog(@"MDMInfo - AppConnect not ready. Wait for it");
    
    // App connect is not ready yet, wait until a notification will be received
    [[NSNotificationCenter defaultCenter] addObserverForName:kMDMIsReadyNotificationKey object:nil queue:nil usingBlock:
     ^(NSNotification * _Nonnull note) {
         
         NSLog(@"MDMInfo - AppConnect is ready");
         
         // Check if app connect is already ready
         if ([[AppConnect sharedInstance] isReady]) {
             
             // App connnect is ready, get the config file and call completion block
             completion([[AppConnect sharedInstance] config]);
             return;
         }
     }];
}

@end

