//
//  ColorStateButton.h
//  AuditContainerAuditContainerIpad
//
//  Created by Iulian Corcoja on 11/3/15.
//
//

#import <UIKit/UIKit.h>
#import "CenterImageButton.h"

@interface ColorStateButton : CenterImageButton

@property (nonatomic, strong) IBInspectable UIColor *defaultTintColor;

@property (nonatomic, strong) IBInspectable UIColor *selectedTintColor;

@property (nonatomic, strong) IBInspectable UIColor *disabledTintColor;

@end
