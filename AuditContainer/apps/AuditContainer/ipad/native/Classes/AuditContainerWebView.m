//
//  AuditContainerWebView.m
//  AuditContainerAuditContainerIpad
//
//  Created by Iulian Corcoja on 12/18/15.
//
//

#import "AuditContainerWebView.h"

@implementation AuditContainerWebView

#pragma mark - Touch events delegate methods

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        //
		NSLog(@"init with coder");
		
		UIPanGestureRecognizer *panGestureRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:
														@selector(panGestureActionEvent:)];
		[self addGestureRecognizer:panGestureRecognizer];
		panGestureRecognizer.delegate = self;
    }
    return self;
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer
shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
	return YES;
}

- (void)panGestureActionEvent:(UIPanGestureRecognizer *)panGesture
{
	if (panGesture.state == UIGestureRecognizerStateEnded || panGesture.state == UIGestureRecognizerStateCancelled ||
		panGesture.state == UIGestureRecognizerStateFailed) {
		
		if (!CGRectContainsPoint(UIEdgeInsetsInsetRect(self.frame, self.scrollView.contentInset),
								[panGesture locationInView:[[[UIApplication sharedApplication] delegate] window]])) {
			
			CGPoint touchLocation = [panGesture locationInView:[[[UIApplication sharedApplication] delegate] window]];
			
			// Call delegate method
			if (self.auditContainerWebViewDelegate && [self.auditContainerWebViewDelegate respondsToSelector:
													   @selector(auditContainerWebView:panGestureOutsideBounds:)]) {
				[self.auditContainerWebViewDelegate auditContainerWebView:self panGestureOutsideBounds:touchLocation];
			}
			
			NSLog(@"outside");
		}
	}
}

@end
