//
//  FooterDelegate.h
//  AuditContainerAuditContainerIpad
//
//  Created by Iulian Corcoja on 11/2/15.
//
//

#import <Foundation/Foundation.h>

@class Footer;

@protocol FooterDelegate <NSObject>

@optional

- (void)footerView:(Footer *)footer toolButtonTouchEvent:(UIButton *)sender withTag:(NSUInteger)tag;

@end

