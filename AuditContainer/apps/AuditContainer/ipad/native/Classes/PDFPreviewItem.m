//
//  PDFPreviewItem.m
//  VesselAuditVesselAuditIpad
//
//  Created by Iulian Corcoja on 1/18/16.
//
//

#import "PDFPreviewItem.h"

@implementation PDFPreviewItem

- (instancetype)init
{
    self = [super init];
    if (self) {
		// Initialization code
    }
    return self;
}

- (instancetype)initWithURL:(NSURL *)url andTitle:(NSString *)title
{
	self = [super init];
	if (self) {
		// Initialization code
		self.previewItemURL = url;
		self.previewItemTitle = title;
	}
	return self;
}

@end
