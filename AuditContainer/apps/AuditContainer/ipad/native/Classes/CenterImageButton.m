//
//  CenterImageButton.m
//  AuditContainerAuditContainerIpad
//
//  Created by Iulian Corcoja on 12/15/15.
//
//

#import "CenterImageButton.h"

@implementation CenterImageButton

- (CGRect)imageRectForContentRect:(CGRect)contentRect
{
	CGRect frame = [super imageRectForContentRect:contentRect];
	return [self horizontalCenteredRect:frame inRect:self.bounds];
}

- (CGRect)titleRectForContentRect:(CGRect)contentRect
{
	CGRect frame = [super titleRectForContentRect:contentRect];
	return [self horizontalCenteredRect:frame inRect:self.bounds];
}

- (CGRect)horizontalCenteredRect:(CGRect)centeredRect inRect:(CGRect)rect
{
	centeredRect.origin.x = lroundf(MIN(rect.size.width / 2.0f - centeredRect.size.width / 2.0f, rect.size.width));
	return centeredRect;
}

@end

