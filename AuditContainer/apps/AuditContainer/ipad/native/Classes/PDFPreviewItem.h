//
//  PDFPreviewItem.h
//  VesselAuditVesselAuditIpad
//
//  Created by Iulian Corcoja on 1/18/16.
//
//

#import <Foundation/Foundation.h>
#import <QuickLook/QuickLook.h>

@interface PDFPreviewItem : NSObject <QLPreviewItem>

@property (nonatomic, strong) NSURL *previewItemURL;

@property (nonatomic, strong) NSString *previewItemTitle;

- (instancetype)initWithURL:(NSURL *)url andTitle:(NSString *)title;

@end
