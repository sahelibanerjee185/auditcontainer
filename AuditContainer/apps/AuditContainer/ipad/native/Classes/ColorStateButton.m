//
//  ColorStateButton.m
//  AuditContainerAuditContainerIpad
//
//  Created by Iulian Corcoja on 11/3/15.
//
//

#import "ColorStateButton.h"

IB_DESIGNABLE
@implementation ColorStateButton

#pragma mark - View's lifecycle methods

- (instancetype)initWithFrame:(CGRect)frame
{
	self = [super initWithFrame:frame];
	if (self) {
		// Initialization code
		[self commonInit];
	}
	return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
	self = [super initWithCoder:aDecoder];
	if (self) {
		// Initialization code
		[self commonInit];
	}
	return self;
}

- (void)commonInit
{
	// Set image with rendering mode
	[self setImage:[[self imageForState:UIControlStateNormal] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate]
		  forState:UIControlStateNormal];
	
	// Set default button tint color (applied on button images too)
	self.tintColor = self.defaultTintColor;
}

#pragma mark - Setters and getters

- (void)setHighlighted:(BOOL)highlighted
{
	[super setHighlighted:highlighted];
	
	// Set button tint color (applied on button images too) based on selected and highlighted value
	self.tintColor = self.selected ?  self.selectedTintColor : (highlighted ? self.selectedTintColor :
																self.defaultTintColor);
}

- (void)setSelected:(BOOL)selected
{
	[super setSelected:selected];
	
	// Set button tint color (applied on button images too) based on selected value
	self.tintColor = selected ? self.selectedTintColor : self.defaultTintColor;
}

- (void)setEnabled:(BOOL)enabled
{
	[super setEnabled:enabled];
	
	// Set disabled button tint color (applied on button images too)
	self.tintColor = self.disabledTintColor;
}

@end
