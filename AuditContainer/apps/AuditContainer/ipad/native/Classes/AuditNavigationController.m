//
//  AuditNavigationController.m
//  AuditContainerAuditContainerIpad
//
//  Created by Iulian Corcoja on 11/2/15.
//
//

#import "AuditNavigationController.h"

@interface AuditNavigationController ()

@end

@implementation AuditNavigationController

#pragma mark - Navigation controller's lifecycle methods

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
	self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
	if (self) {
		// Custom initialization
	}
	return self;
}

- (void)viewDidLoad
{
	[super viewDidLoad];
	// Do any additional setup after loading the view.
}

#pragma mark - View controller delegate methods

- (BOOL)prefersStatusBarHidden
{
    if (self.topViewController) {
        return self.topViewController.prefersStatusBarHidden;
    }
    return false;
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    if (self.topViewController) {
        return self.topViewController.preferredStatusBarStyle;
    }
	return UIStatusBarStyleDefault;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    if (self.topViewController) {
        if (self.topViewController.presentedViewController) {
            return self.topViewController.presentedViewController.supportedInterfaceOrientations;
        } else {
            return self.topViewController.supportedInterfaceOrientations;
        }
    }
    return UIInterfaceOrientationMaskLandscape;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    if (self.topViewController) {
        return self.topViewController.preferredInterfaceOrientationForPresentation;
    }
    return UIInterfaceOrientationLandscapeLeft;
}

#pragma mark - View controller's memory management

- (void)didReceiveMemoryWarning
{
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
	
	NSLog(@"memory warning received");
}

@end
