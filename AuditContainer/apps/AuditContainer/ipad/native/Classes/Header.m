//
//  Header.m
//  AuditContainerAuditContainerIpad
//
//  Created by Iulian Corcoja on 11/2/15.
//
//

#import "Header.h"
#import "Constants.h"

#pragma mark - View interface

@interface Header ()

@property (nonatomic, strong) NSLayoutConstraint *heightConstraint;

@end

@implementation Header

#pragma mark - View lifecycle methods

+ (id)initFromNib
{
	Header *header = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass(self) owner:nil options:nil] firstObject];
	
	// Make sure header view was initialized properly
	if (header && [header isKindOfClass:[Header class]]) {
		
		// Remove auto-created layout constraints
		header.translatesAutoresizingMaskIntoConstraints = NO;
		
		// Return header
		return header;
	}
	return nil;
}

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
		// Custom initialization...
    }
    return self;
}

- (void)setUserInteractionEnabled:(BOOL)userInteractionEnabled
{
    [super setUserInteractionEnabled:userInteractionEnabled];
    
    self.syncButton.enabled = userInteractionEnabled;
}

#pragma mark - Auto layout methods

- (void)layoutSubviews
{
	self.heightConstraint.constant = kHeaderHeight + [[UIApplication sharedApplication] statusBarFrame].size.height;
	
	NSLog(@"%@", NSStringFromCGRect([[UIApplication sharedApplication] statusBarFrame]));
	
	// Call default implementation of layout subviews
	[super layoutSubviews];
}

#pragma mark - Setters and getters

- (void)setSyncing:(BOOL)syncing
{
    _syncing = syncing;
    
    // Animation key
    static NSString * const animationKey = @"rotation";
    static NSTimeInterval const animationDuration = 0.85;
    
    if (syncing) {
        
        // Remove previous animation
        [self.syncButton.layer removeAnimationForKey:animationKey];
        
        // Create rotation animation
        CABasicAnimation *rotationAnimation = [CABasicAnimation animationWithKeyPath:@"transform.rotation"];
        rotationAnimation.fromValue = [NSNumber numberWithFloat:0];
        rotationAnimation.toValue = [NSNumber numberWithFloat:-M_PI * 2.0f];
        rotationAnimation.duration = animationDuration;
        rotationAnimation.repeatCount = HUGE_VALF;
        rotationAnimation.removedOnCompletion = YES;
        
        // Add rotation animation to sync image view' layer
        [self.syncButton.layer addAnimation:rotationAnimation forKey:animationKey];
    } else {
        
        // Get current rotation angle
        CGFloat angle = [[self.syncButton.layer.presentationLayer valueForKeyPath:@"transform.rotation"] floatValue];
        
        // Add completion rotation animation
        CABasicAnimation *completionAnimation = [CABasicAnimation animationWithKeyPath:@"transform.rotation"];
        completionAnimation.fromValue = [NSNumber numberWithFloat:angle];
        completionAnimation.toValue = [NSNumber numberWithFloat:-M_PI * 2.0f * signbit(angle)];
        completionAnimation.duration = ABS(-M_PI * 2.0f * signbit(angle) - angle) / (M_PI * 2.0f) * animationDuration;
        completionAnimation.repeatCount = 1;
        completionAnimation.removedOnCompletion = YES;
        
        // Remove animation from image view's layer and add the completion rotation animatiom
        [self.syncButton.layer removeAnimationForKey:animationKey];
        [self.syncButton.layer addAnimation:completionAnimation forKey:animationKey];
    }
}

- (void)setSyncButtonVisible:(BOOL)syncButtonVisible
{
	_syncButtonVisible = syncButtonVisible;
	
	// Set sync button and date label visibility
	self.syncButton.hidden = !syncButtonVisible;
	self.dateLabel.hidden = !syncButtonVisible;
}

#pragma mark - Action and gesture methods

- (IBAction)backTouchEvent:(UIButton *)sender
{
	// Call delegate method
	if (self.delegate && [self.delegate respondsToSelector:@selector(headerView:backTouchEvent:)]) {
		[self.delegate headerView:self backTouchEvent:sender];
	}
}

- (IBAction)syncTouchEvent:(UIButton *)sender
{
	// Call delegate method
	if (self.delegate && [self.delegate respondsToSelector:@selector(headerView:syncTouchEvent:)]) {
		[self.delegate headerView:self syncTouchEvent:sender];
	}
}

- (IBAction)headerTapGesture:(UITapGestureRecognizer *)tapGesture
{
	// Call delegate method
	if (self.delegate && [self.delegate respondsToSelector:@selector(headerViewTapEvent:)]) {
		[self.delegate headerViewTapEvent:self];
	}
}

#pragma mark - View delegate methods

- (void)didMoveToSuperview
{
	[super didMoveToSuperview];
	
	// Add new constraints
	self.heightConstraint = [NSLayoutConstraint constraintWithItem:self attribute:NSLayoutAttributeHeight relatedBy:
							 NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0
														  constant:kHeaderHeight];
	[self.superview addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[view]|" options:0 metrics:
									nil views:@{@"view" : self}]];
	[self.superview addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[view]" options:0 metrics:
									nil views:@{@"view" : self}]];
	[self.superview addConstraint:self.heightConstraint];
	
	NSLog(@"header moved to a new superview - add new constraints");
}

@end
