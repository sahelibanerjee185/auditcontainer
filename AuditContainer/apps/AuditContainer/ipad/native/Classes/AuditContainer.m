//
//  MyAppDelegate.m
//  AuditContainer
//
//

#import "AuditContainer.h"
#import "AuditViewController.h"
#import "AuditNavigationController.h"
#import "MDMInfo.h"

@implementation MyAppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    BOOL result = [super application:application didFinishLaunchingWithOptions:launchOptions];
    
    // Create a Audit View Controller
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    AuditNavigationController *navigationController = [[AuditNavigationController alloc] init];
    navigationController.navigationBar.hidden = YES;
    
    // Set window's root view controller and show it
    [self.window setRootViewController:navigationController];
    [self.window makeKeyAndVisible];
    
//#if !(TARGET_IPHONE_SIMULATOR)
//    
//    // Show application's splash screen while MDM is checkiung app authorization
//    [[WL sharedInstance] showSplashScreen];
//    
//    // Initialize the AppConnect library
//    [AppConnect initWithDelegate:self];
//    [[AppConnect sharedInstance] startWithLaunchOptions:launchOptions];
//    
//#else
    
    // Initialize web framework
    [[WL sharedInstance] initializeWebFrameworkWithDelegate:self];
    
//#endif
    
    return result;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of
    // temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application
    // and it begins the transition to the background state. Use this method to pause ongoing tasks, disable timers, and
    // throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application
    // state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate:
    // when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes
    // made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application
    // was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also
    // applicationDidEnterBackground:.
}

#pragma mark - Worklight delegate methods

// This method is called after the WL web framework initialization is complete and web resources are ready to be used.
- (void)wlInitWebFrameworkDidCompleteWithResult:(WLWebFrameworkInitResult *)result
{
    if ([result statusCode] == WLWebFrameworkInitResultSuccess) {
        [self wlInitDidCompleteSuccessfully];
    } else {
        [self wlInitDidFailWithResult:result];
    }
}

- (void)wlInitDidCompleteSuccessfully
{
    AuditNavigationController *navigationController = (AuditNavigationController *) self.window.rootViewController;
    AuditViewController *auditViewController = [[AuditViewController alloc] initWithNibName:@"AuditViewController"
                                                                                     bundle:nil];
    auditViewController.startPage = [[WL sharedInstance] mainHtmlFilePath];
    [navigationController setViewControllers:@[auditViewController] animated:NO];
    
    // Hide splash screen
    [[WL sharedInstance] hideSplashScreen];
}

- (void)wlInitDidFailWithResult:(WLWebFrameworkInitResult *)result
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"ERROR" message:[result message]
                                                            preferredStyle:UIAlertControllerStyleAlert];
    [alert addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:nil]];
    [self.window.rootViewController presentViewController:alert animated:YES completion:nil];
}


#pragma mark - AppConnect delegate methods

- (void)appConnectIsReady:(AppConnect *)appConnect
{
    NSLog(@"AppConnect is ready");
    
    // Post a notification that app connect is ready
    [[NSNotificationCenter defaultCenter] postNotificationName:kMDMIsReadyNotificationKey object:self];
    
    // Initialize web framework
    if (![[WLClient sharedInstance] isInitialized]) {
        [[WL sharedInstance] initializeWebFrameworkWithDelegate:self];
        [[WL sharedInstance] hideSplashScreen];
    }
}

- (void)appConnect:(AppConnect *)appConnect authStateChangedTo:(ACAuthState)newAuthState withMessage:(NSString *)newMessage
{
    NSString *authStateString;
    switch (newAuthState) {
        case ACAUTHSTATE_AUTHORIZED:
            authStateString = @"authorized";
            break;
        case ACAUTHSTATE_RETIRED:
            authStateString = @"retired";
            break;
        case ACAUTHSTATE_UNAUTHORIZED:
            authStateString = @"unauthorized";
            break;
    }
    [AppConnect logAtLevel:ACLOGLEVEL_STATUS format:@"authStateChangedTo: %@", authStateString];
    [appConnect authStateApplied:ACPOLICY_APPLIED message:nil];
}

- (void)appConnect:(AppConnect *)appConnect pasteboardPolicyChangedTo:(ACPasteboardPolicy)newPasteboardPolicy
{
    [appConnect pasteboardPolicyApplied:ACPOLICY_APPLIED message:nil];
}

- (void)appConnect:(AppConnect *)appConnect openInPolicyChangedTo:(ACOpenInPolicy)newOpenInPolicy whitelist:(NSSet *)newWhitelist
{
    [appConnect openInPolicyApplied:ACPOLICY_APPLIED message:nil];
}

- (void)appConnect:(AppConnect *)appConnect printPolicyChangedTo:(ACPrintPolicy)newPrintPolicy
{
    [appConnect printPolicyApplied:ACPOLICY_APPLIED message:nil];
}

- (void)appConnect:(AppConnect *)appConnect secureFileIOPolicyChangedTo:(ACSecureFileIOPolicy)newSecureFileIOPolicy
{
    [appConnect secureFileIOPolicyApplied:ACPOLICY_APPLIED message:nil];
}

- (void)appConnect:(AppConnect *)appConnect secureServicesAvailabilityChangedTo:(ACSecureServicesAvailability)secureServicesAvailability
{
    if ([[AppConnect sharedInstance] isReady]) {
        NSLog(@"app connect config: %@", [[AppConnect sharedInstance] config]);
    }
}

- (void)appConnect:(AppConnect *)appConnect configChangedTo:(NSDictionary *)newConfig
{
    [appConnect configApplied:ACPOLICY_APPLIED message:nil];
}

@end
