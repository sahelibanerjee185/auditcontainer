/*
* Licensed Materials - Property of IBM
* 5725-I43 (C) Copyright IBM Corp. 2006, 2013. All Rights Reserved.
* US Government Users Restricted Rights - Use, duplication or
* disclosure restricted by GSA ADP Schedule Contract with IBM Corp.
*/

#import <UIKit/UIKit.h>
#import <AppConnect/AppConnect.h>

int main(int argc, char *argv[]) {
	@autoreleasepool {
		//return UIApplicationMain(argc, argv, nil, @"MyAppDelegate");
		return UIApplicationMain(argc, argv, kACUIApplicationClassName, @"MyAppDelegate");
	}
}
