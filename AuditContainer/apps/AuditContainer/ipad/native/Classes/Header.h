//
//  Header.h
//  AuditContainerAuditContainerIpad
//
//  Created by Iulian Corcoja on 11/2/15.
//
//

#import <UIKit/UIKit.h>
#import "HeaderDelegate.h"

@interface Header : UIView

@property (nonatomic, weak) IBOutlet UIButton *backButton;

@property (nonatomic, weak) IBOutlet UIButton *syncButton;

@property (nonatomic, weak) IBOutlet UILabel *titleLabel;

@property (nonatomic, weak) IBOutlet UILabel *syncLabelCount;

@property (nonatomic, weak) IBOutlet UILabel *dateLabel;

@property (nonatomic, weak) id<HeaderDelegate> delegate;

@property (nonatomic, assign, getter = isSyncButtonVisible) BOOL syncButtonVisible;

@property (nonatomic, assign, getter = isSyncing) BOOL syncing;

#pragma mark - Class methods

+ (id)initFromNib;

@end
