//
//  UIImageCompress.swift
//  AuditContainerAuditContainerIpad
//
//  Created by Andrew Radulescu on 5/16/17.
//
//

import UIKit

extension UIImage {

    public func compressedImageData(toMaxImageRawSize maxImageSize: Int,
                                    maxCompressionIterations iterations: Int,
                                    interationDownscaleRatio downscaleRatio: CGFloat) -> Data? {
        
        // Compress the image to not exceed the limit size
        var maxIterations = iterations
        var ratio: CGFloat = 1.0
        var compressedImage = UIImageJPEGRepresentation(self, ratio)
        var compressedImageSize = compressedImage?.count ?? maxImageSize
        
        // If the image is larger than the max image size, try to bring it down to the max image size
        while compressedImageSize > maxImageSize && maxIterations > 0 {
            maxIterations = maxIterations - 1
            ratio = ratio * downscaleRatio
            
            compressedImage = UIImageJPEGRepresentation(self, ratio)
            compressedImageSize = compressedImage?.count ?? maxImageSize
        }
        return compressedImage
    }
}

