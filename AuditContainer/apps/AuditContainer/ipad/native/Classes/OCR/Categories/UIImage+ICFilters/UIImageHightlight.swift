//
//  UIImageHightlight.swift
//  ImageHighlight
//
//  Created by Iulian Corcoja on 12/8/16.
//  Copyright © 2016 Iulian Corcoja. All rights reserved.
//

import UIKit

extension UIImage {

	public func highlightCircle(_ position: CGPoint, radius: CGFloat, color: UIColor) -> UIImage? {

		// Create image context
		UIGraphicsBeginImageContextWithOptions(size, false, 1.0)

		// Get current context
		guard let context = UIGraphicsGetCurrentContext() else {

			// Failed to get current context, return nil
			UIGraphicsEndImageContext()
			return nil
		}

		// Draw the image
		draw(in: CGRect(origin: CGPoint(), size: size))

		// Save graphic state for creating highlighted area (new layer)
		context.saveGState()

		// Draw circle path with the provided position and radius
		context.beginPath()
		context.addArc(center: position, radius: radius, startAngle: 0.0, endAngle: 2.0 * CGFloat.pi, clockwise: false)
		context.addRect(CGRect.infinite)
		context.closePath()

		// Create a mask by clipping clipping area of the context using even-odd rule (even path is clipped from the odd
		// path)
		context.clip()

		// Fill the mask with the provided color
		context.setFillColor(color.cgColor)
		context.fill(CGRect.infinite)

		// Restore saved graphic state and apply it to the context (merge layers)
		context.restoreGState()

		// Get image from the context and return it
		let image = UIGraphicsGetImageFromCurrentImageContext()
		UIGraphicsEndImageContext()
		return image
	}
}
