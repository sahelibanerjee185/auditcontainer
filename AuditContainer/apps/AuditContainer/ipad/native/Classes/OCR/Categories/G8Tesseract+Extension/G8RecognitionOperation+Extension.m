//
//  G8RecognitionOperation+Extension.m
//  LiveCharacterRecognitionDemo
//
//  Created by Iulian Corcoja on 8/9/16.
//  Copyright © 2016 Iulian Corcoja. All rights reserved.
//

#import "G8RecognitionOperation+Extension.h"

@interface G8RecognitionOperation () <G8TesseractDelegate>

@property (nonatomic, strong) G8Tesseract *tesseract;

@end

@implementation G8RecognitionOperation (Extension)

- (id)initWithLanguage:(NSString *)language
	  absoluteDataPath:(NSString *)absoluteDataPath
			engineMode:(G8OCREngineMode)engineMode
{
	self = [super init];
	if (self != nil) {
		self.tesseract = [[G8Tesseract alloc] initWithLanguage:language
											  absoluteDataPath:absoluteDataPath
													engineMode:engineMode];
		self.tesseract.delegate = self;

		__weak __typeof(self) weakSelf = self;
		self.completionBlock = ^{
			__strong __typeof(weakSelf) strongSelf = weakSelf;

			G8RecognitionOperationCallback callback = [strongSelf.recognitionCompleteBlock copy];
			G8Tesseract *tesseract = strongSelf.tesseract;
			if (callback != nil) {
				[[NSOperationQueue mainQueue] addOperationWithBlock:^{
					callback(tesseract);
				}];
			}
		};
	}
	return self;
}

@end
