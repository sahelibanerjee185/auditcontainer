//
//  G8RecognitionOperation+Extension.h
//  LiveCharacterRecognitionDemo
//
//  Created by Iulian Corcoja on 8/9/16.
//  Copyright © 2016 Iulian Corcoja. All rights reserved.
//

#import <TesseractOCR/TesseractOCR.h>
#import "G8Tesseract+Extension.h"

@interface G8RecognitionOperation (Extension)

- (id)initWithLanguage:(NSString *)language
	  absoluteDataPath:(NSString *)absoluteDataPath
			engineMode:(G8OCREngineMode)engineMode;

@end
