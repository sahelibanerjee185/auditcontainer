//
//  UIImage+OpenCV.h
//  LiveCharacterRecognitionDemo
//
//  Created by Iulian Corcoja on 3/8/16.
//  Copyright © 2016 Iulian Corcoja. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <opencv2/opencv.hpp>

@interface UIImage (OpenCV)

@property (nonatomic, assign, readonly) cv::Mat cvMat;

@property (nonatomic, assign, readonly) cv::Mat cvGrayscaleMat;

+ (UIImage *)imageFromCVMatrix:(cv::Mat)cvMat;


@end
