//
//  G8Tesseract+Extension.m
//  LiveCharacterRecognitionDemo
//
//  Created by Iulian Corcoja on 8/9/16.
//  Copyright © 2016 Iulian Corcoja. All rights reserved.
//

#import "G8Tesseract+Extension.h"

@interface G8Tesseract ()

@property (nonatomic, copy) NSString *absoluteDataPath;

@end

@implementation G8Tesseract (Extension)

- (instancetype)initWithLanguage:(NSString *)language
				absoluteDataPath:(NSString *)absoluteDataPath
					  engineMode:(G8OCREngineMode)engineMode
{
	return [self initWithLanguage:language
				 configDictionary:nil
				  configFileNames:nil
				 absoluteDataPath:absoluteDataPath
					 moveTessdata:NO
					   engineMode:engineMode];
}

@end
