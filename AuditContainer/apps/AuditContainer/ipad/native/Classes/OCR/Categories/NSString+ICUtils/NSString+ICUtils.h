//
//  NSString+ICUtils.h
//  LiveCharacterRecognitionDemo
//
//  Created by Iulian Corcoja on 7/26/16.
//  Copyright © 2016 Iulian Corcoja. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (ICUtils)

- (NSUInteger)numberOfLines;

- (NSString *)splitStringByWordsWithSet:(NSCharacterSet *)characterSet
							splitString:(NSString *)splitString
				   maxSplitStringLength:(NSUInteger)lineLength;

@end
