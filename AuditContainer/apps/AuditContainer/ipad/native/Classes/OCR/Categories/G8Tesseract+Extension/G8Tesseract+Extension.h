//
//  G8Tesseract+Extension.h
//  LiveCharacterRecognitionDemo
//
//  Created by Iulian Corcoja on 8/9/16.
//  Copyright © 2016 Iulian Corcoja. All rights reserved.
//

#import <TesseractOCR/TesseractOCR.h>

@interface G8Tesseract (Extension)

- (instancetype)initWithLanguage:(NSString *)language
				absoluteDataPath:(NSString *)absoluteDataPath
					  engineMode:(G8OCREngineMode)engineMode;

@end
