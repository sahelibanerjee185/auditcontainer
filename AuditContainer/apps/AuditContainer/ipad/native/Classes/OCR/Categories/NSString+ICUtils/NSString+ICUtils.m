//
//  NSString+ICUtils.m
//  LiveCharacterRecognitionDemo
//
//  Created by Iulian Corcoja on 7/26/16.
//  Copyright © 2016 Iulian Corcoja. All rights reserved.
//

#import "NSString+ICUtils.h"

@implementation NSString (ICUtils)

- (NSUInteger)numberOfLines
{
	NSUInteger numberOfLines = 0;
	NSUInteger length = self.length;

	for (NSInteger idx = 0; idx < length; numberOfLines++) {
		idx = NSMaxRange([self lineRangeForRange:NSMakeRange(idx, 0)]);
	}

	return numberOfLines;
}

- (NSString *)splitStringByWordsWithSet:(NSCharacterSet *)characterSet
							splitString:(NSString *)splitString
				   maxSplitStringLength:(NSUInteger)lineLength
{
	// Sanity check
	if (lineLength == 0) {
		return self;
	}

	// Duplicate original string and create the final string
	NSString *string = self.copy;
	NSMutableString *finalString = [[NSMutableString alloc] initWithCapacity:string.length];

	// Loop through string lines
	while (string.length > lineLength) {

		// Get the range of the last occurence of whitespace or newline character
		NSRange range = [[string substringToIndex:lineLength] rangeOfCharacterFromSet:characterSet options:
						 NSBackwardsSearch];

		if (range.location != NSNotFound && range.location > 0) {

			// Add extracted string, appended by split string, to final string
			[finalString appendFormat:@"%@%@", [string substringToIndex:range.location], splitString];
		} else {

			// No whitespace or newline characters were found on this line - hard split this line
			range.location = lineLength;
			range.length = 0;
			[finalString appendFormat:@"%@%@", [string substringToIndex:lineLength], splitString];
		}

		// Subtract the extracted string from the parsed string
		string = [[string substringFromIndex:range.location + range.length] stringByTrimmingCharactersInSet:
				  characterSet];
	}

	// Add last line to the final string
	[finalString appendString:string];

	return finalString;
}

@end
