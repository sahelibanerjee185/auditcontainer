//
//  UIImage+ICFilters.m
//  LiveCharacterRecognitionDemo
//
//  Created by Iulian Corcoja on 1/27/16.
//  Copyright © 2016 Iulian Corcoja. All rights reserved.
//

#import "UIImage+ICFilters.h"

#define kRedColorRatio 0.3f
#define kGreenColorRatio 0.5f
#define kBlueColorRatio 0.2f

#define kBlackColorThreshold 80

typedef NS_ENUM(NSUInteger, PIXELS) {
	ALPHA = 0,
	BLUE = 1,
	GREEN = 2,
	RED = 3
};

@implementation UIImage (ICFilters)

- (UIImage *)binarizedImage
{
	CGSize size = [self size];
	int width = size.width;
	int height = size.height;
	
	// The pixels will be painted to this array
	uint32_t *pixels = (uint32_t *) malloc(width * height * sizeof(uint32_t));
	
	// Clear the pixels so any transparency is preserved
	memset(pixels, 0, width * height * sizeof(uint32_t));
	
	CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
	
	// Create a context with RGBA pixels
	CGContextRef context = CGBitmapContextCreate(pixels, width, height, 8, width * sizeof(uint32_t), colorSpace,
												 kCGBitmapByteOrder32Little | kCGImageAlphaPremultipliedLast);
	
	// Paint the bitmap to our context which will fill in the pixels array
	CGContextDrawImage(context, CGRectMake(0, 0, width, height), [self CGImage]);
	
	for(int y = 0; y < height; y++) {
		for(int x = 0; x < width; x++) {
			uint8_t *rgbaPixel = (uint8_t *) &pixels[y * width + x];
			
			// Convert to grayscale using recommended method:
			// http://en.wikipedia.org/wiki/Grayscale#Converting_color_to_grayscale
			uint32_t gray = (kRedColorRatio * rgbaPixel[RED] +
							 kGreenColorRatio * rgbaPixel[GREEN] +
							 kBlueColorRatio * rgbaPixel[BLUE]);
			
			// Binarize gray color
			gray = (gray < kBlackColorThreshold ? 0 : UINT32_MAX);
			
			// Set the pixels to gray
			rgbaPixel[RED] = gray;
			rgbaPixel[GREEN] = gray;
			rgbaPixel[BLUE] = gray;
		}
	}
	
	// Create a new CGImageRef from our context with the modified pixels
	CGImageRef image = CGBitmapContextCreateImage(context);
	
	// We're done with the context, color space, and pixels
	CGContextRelease(context);
	CGColorSpaceRelease(colorSpace);
	free(pixels);
	
	// Make a new UIImage to return
	UIImage *resultUIImage = [UIImage imageWithCGImage:image scale:0 orientation:self.imageOrientation];
	
	// We're done with image now too
	CGImageRelease(image);
	
	return resultUIImage;
}

- (UIImage *)rasterizedImageOrientation
{
	// No-op if the orientation is already correct
	if (self.imageOrientation == UIImageOrientationUp) {
		return self;
	}
	
	// We need to calculate the proper transformation to make the image upright. We do it in 2 steps: Rotate if
	// Left/Right/Down, and then flip if Mirrored.
	CGAffineTransform transform = CGAffineTransformIdentity;
	
	switch (self.imageOrientation) {
		case UIImageOrientationDown:
		case UIImageOrientationDownMirrored:
			transform = CGAffineTransformTranslate(transform, self.size.width, self.size.height);
			transform = CGAffineTransformRotate(transform, M_PI);
			break;
			
		case UIImageOrientationLeft:
		case UIImageOrientationLeftMirrored:
			transform = CGAffineTransformTranslate(transform, self.size.width, 0);
			transform = CGAffineTransformRotate(transform, M_PI_2);
			break;
			
		case UIImageOrientationRight:
		case UIImageOrientationRightMirrored:
			transform = CGAffineTransformTranslate(transform, 0, self.size.height);
			transform = CGAffineTransformRotate(transform, -M_PI_2);
			break;
		case UIImageOrientationUp:
		case UIImageOrientationUpMirrored:
			break;
	}
	
	switch (self.imageOrientation) {
		case UIImageOrientationUpMirrored:
		case UIImageOrientationDownMirrored:
			transform = CGAffineTransformTranslate(transform, self.size.width, 0);
			transform = CGAffineTransformScale(transform, -1, 1);
			break;
			
		case UIImageOrientationLeftMirrored:
		case UIImageOrientationRightMirrored:
			transform = CGAffineTransformTranslate(transform, self.size.height, 0);
			transform = CGAffineTransformScale(transform, -1, 1);
			break;
		case UIImageOrientationUp:
		case UIImageOrientationDown:
		case UIImageOrientationLeft:
		case UIImageOrientationRight:
			break;
	}
	
	// Now we draw the underlying CGImage into a new context, applying the transform calculated above.
	CGContextRef ctx = CGBitmapContextCreate(NULL, self.size.width, self.size.height,
											 CGImageGetBitsPerComponent(self.CGImage), 0,
											 CGImageGetColorSpace(self.CGImage),
											 CGImageGetBitmapInfo(self.CGImage));
	CGContextConcatCTM(ctx, transform);
	switch (self.imageOrientation) {
		case UIImageOrientationLeft:
		case UIImageOrientationLeftMirrored:
		case UIImageOrientationRight:
		case UIImageOrientationRightMirrored: {
			CGContextDrawImage(ctx, CGRectMake(0,0,self.size.height,self.size.width), self.CGImage);
			break;
		}
		default: {
			CGContextDrawImage(ctx, CGRectMake(0,0,self.size.width,self.size.height), self.CGImage);
			break;
		}
	}
	
	// And now we just create a new UIImage from the drawing context
	CGImageRef imageRef = CGBitmapContextCreateImage(ctx);
	UIImage *image = [UIImage imageWithCGImage:imageRef];
	CGContextRelease(ctx);
	CGImageRelease(imageRef);
	return image;
}

- (UIImage *)cropImageRect:(CGRect)rect
{
	CGImageRef imageRef = CGImageCreateWithImageInRect(self.CGImage, rect);
	UIImage *croppedImage = [UIImage imageWithCGImage:imageRef];
	CGImageRelease(imageRef);
	return croppedImage;
}

- (CGImageRef)CGImageWithCorrectOrientation CF_RETURNS_RETAINED
{
	if (self.imageOrientation == UIImageOrientationDown) {
		// Retaining because caller expects to own the reference
		CGImageRef cgImage = [self CGImage];
		CGImageRetain(cgImage);
		return cgImage;
	}
	UIGraphicsBeginImageContextWithOptions(self.size, NO, 0.0);
	
	CGContextRef context = UIGraphicsGetCurrentContext();
	
	if (self.imageOrientation == UIImageOrientationRight) {
		CGContextRotateCTM (context, 90 * M_PI/180);
	} else if (self.imageOrientation == UIImageOrientationLeft) {
		CGContextRotateCTM (context, -90 * M_PI/180);
	} else if (self.imageOrientation == UIImageOrientationUp) {
		CGContextRotateCTM (context, 180 * M_PI/180);
	}
	
	[self drawAtPoint:CGPointMake(0, 0)];
	
	CGImageRef cgImage = CGBitmapContextCreateImage(context);
	UIGraphicsEndImageContext();
	
	return cgImage;
}

- (UIImage *)resizedImageByWidth:(NSUInteger)width
{
	CGImageRef imgRef = [self CGImageWithCorrectOrientation];
	CGFloat original_width  = CGImageGetWidth(imgRef);
	CGFloat original_height = CGImageGetHeight(imgRef);
	CGFloat ratio = width/original_width;
	CGImageRelease(imgRef);
	return [self drawImageInBounds: CGRectMake(0, 0, width, round(original_height * ratio))];
}

- (UIImage *)resizedImageByHeight:(NSUInteger)height
{
	CGImageRef imgRef = [self CGImageWithCorrectOrientation];
	CGFloat original_width  = CGImageGetWidth(imgRef);
	CGFloat original_height = CGImageGetHeight(imgRef);
	CGFloat ratio = height/original_height;
	CGImageRelease(imgRef);
	return [self drawImageInBounds: CGRectMake(0, 0, round(original_width * ratio), height)];
}

- (UIImage *)resizedImageWithMinimumSize:(CGSize)size
{
	CGImageRef imgRef = [self CGImageWithCorrectOrientation];
	CGFloat original_width  = CGImageGetWidth(imgRef);
	CGFloat original_height = CGImageGetHeight(imgRef);
	CGFloat width_ratio = size.width / original_width;
	CGFloat height_ratio = size.height / original_height;
	CGFloat scale_ratio = width_ratio > height_ratio ? width_ratio : height_ratio;
	CGImageRelease(imgRef);
	return [self drawImageInBounds: CGRectMake(0, 0, round(original_width * scale_ratio), round(original_height * scale_ratio))];
}

- (UIImage *)resizedImageWithMaximumSize:(CGSize)size
{
	CGImageRef imgRef = [self CGImageWithCorrectOrientation];
	CGFloat original_width  = CGImageGetWidth(imgRef);
	CGFloat original_height = CGImageGetHeight(imgRef);
	CGFloat width_ratio = size.width / original_width;
	CGFloat height_ratio = size.height / original_height;
	CGFloat scale_ratio = width_ratio < height_ratio ? width_ratio : height_ratio;
	CGImageRelease(imgRef);
	return [self drawImageInBounds: CGRectMake(0, 0, round(original_width * scale_ratio), round(original_height * scale_ratio))];
}

- (UIImage *)drawImageInBounds:(CGRect)bounds
{
	UIGraphicsBeginImageContextWithOptions(bounds.size, NO, 0.0);
	[self drawInRect: bounds];
	UIImage *resizedImage = UIGraphicsGetImageFromCurrentImageContext();
	UIGraphicsEndImageContext();
	return resizedImage;
}

@end
