//
//  UIImage+ICFilters.h
//  LiveCharacterRecognitionDemo
//
//  Created by Iulian Corcoja on 1/27/16.
//  Copyright © 2016 Iulian Corcoja. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (ICFilters)

- (UIImage *)binarizedImage;

- (UIImage *)rasterizedImageOrientation;

- (UIImage *)cropImageRect:(CGRect)rect;

- (UIImage *)resizedImageByWidth:(NSUInteger)width;

- (UIImage *)resizedImageByHeight:(NSUInteger)height;

- (UIImage *)resizedImageWithMaximumSize:(CGSize)size;

- (UIImage *)resizedImageWithMinimumSize:(CGSize)size;

@end
