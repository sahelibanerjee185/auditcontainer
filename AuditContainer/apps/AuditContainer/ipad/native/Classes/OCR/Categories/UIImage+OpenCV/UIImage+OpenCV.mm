//
//  UIImage+OpenCV.mm
//  LiveCharacterRecognitionDemo
//
//  Created by Iulian Corcoja on 3/8/16.
//  Copyright © 2016 Iulian Corcoja. All rights reserved.
//

#import "UIImage+OpenCV.h"

@implementation UIImage (OpenCV)

- (cv::Mat)cvMat
{
	// Get image size
	CGFloat columns = self.size.width;
	CGFloat rows = self.size.height;
	
	// Check if image is in grayscale
	BOOL isGrayscale = (CGColorSpaceGetModel(CGImageGetColorSpace(self.CGImage)) == kCGColorSpaceModelMonochrome);
	
	// Create OpenCV image matrix
	cv::Mat cvMat(rows, columns, isGrayscale ? CV_8UC1 : CV_8UC4);
	
	// Create bitmap flags
	uint32_t bitmapFlags = kCGBitmapByteOrderDefault | (!isGrayscale ? kCGImageAlphaPremultipliedLast : 0);
	// Create Core Graphics context
	CGContextRef contextRef = CGBitmapContextCreate(cvMat.data,							// Pointer to  data
													columns,							// Width of bitmap
													rows,								// Height of bitmap
													8,									// Bits per component
													cvMat.step[0],						// Bytes per row
													CGImageGetColorSpace(self.CGImage),	// Colorspace
													bitmapFlags);						// Bitmap info flags
	// Draw the image into context
	CGContextDrawImage(contextRef, CGRectMake(0.0f, 0.0f, columns, rows), self.CGImage);
	CGContextRelease(contextRef);
	
	// Return image matrix
	return cvMat;
}

- (cv::Mat)cvGrayscaleMat
{
	// Get image size
	CGFloat columns = self.size.width;
	CGFloat rows = self.size.height;
	
	// Create OpenCV image matrix
	cv::Mat cvMat(rows, columns, CV_8UC1);
	
	// Create Core Graphics context
	CGContextRef contextRef = CGBitmapContextCreate(cvMat.data,							// Pointer to  data
													columns,							// Width of bitmap
													rows,								// Height of bitmap
													8,									// Bits per component
													cvMat.step[0],						// Bytes per row
													CGColorSpaceCreateDeviceGray(),		// Gray colorspace
													kCGImageAlphaNone |
													kCGBitmapByteOrderDefault);			// Bitmap info flags

	// Draw the image into context
	CGContextDrawImage(contextRef, CGRectMake(0.0f, 0.0f, columns, rows), self.CGImage);
	CGContextRelease(contextRef);
	
	// Return image matrix
	return cvMat;
}

+ (UIImage *)imageFromCVMatrix:(cv::Mat)cvMat
{
	// Sanity checks
	if (cvMat.empty()) {
		return nil;
	}

	// Create image raw data
	NSData *data = [NSData dataWithBytes:cvMat.data length:cvMat.elemSize() * cvMat.total()];
	CGColorSpaceRef colorSpace = (cvMat.elemSize() == 1 ?
								  CGColorSpaceCreateDeviceGray() :
								  CGColorSpaceCreateDeviceRGB());
	
	CGDataProviderRef provider = CGDataProviderCreateWithCFData((__bridge CFDataRef) data);
	
	// Creating Core Graphics image from OpenCV image matrix
	CGImageRef imageRef = CGImageCreate(cvMat.cols,										// Width
										cvMat.rows,										// Height
										8,												// Bits per component
										8 * cvMat.elemSize(),							// Bits per pixel
										cvMat.step[0],									// BytesPerRow
										colorSpace,										// Colorspace
										kCGImageAlphaNone | kCGBitmapByteOrderDefault,	// Bitmap info
										provider,										// CGDataProviderRef
										NULL,											// Decode
										false,											// Should interpolate
										kCGRenderingIntentDefault);						// Intent
	
	// Getting image from Core Graphics image
	UIImage *image = [UIImage imageWithCGImage:imageRef];
	CGImageRelease(imageRef);
	CGDataProviderRelease(provider);
	CGColorSpaceRelease(colorSpace);
	
	return image;
}

@end
