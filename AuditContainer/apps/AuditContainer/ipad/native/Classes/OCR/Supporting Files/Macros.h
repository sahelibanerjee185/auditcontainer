//
//  Macros.h
//  Macro functions
//
//  Created by Iulian Corcoja on 5/9/16.
//  Copyright © 2016 Iulian Corcoja. All rights reserved.
//

#define tick_0_args()		CFTimeInterval __tickTime##0 = CACurrentMediaTime()
#define tick_1_args(idx)	CFTimeInterval __tickTime##idx = CACurrentMediaTime()

#define tick_macro_chooser(x, A, FUNC, ...) FUNC

#define tick(...) tick_macro_chooser(,##__VA_ARGS__,\
		tick_1_args(__VA_ARGS__),\
		tick_0_args(__VA_ARGS__))

#define tock_0_args()										NSLog(@"%f", CACurrentMediaTime() - __tickTime##0)
#define tock_1_args(idx)									NSLog(@"%f", CACurrentMediaTime() - __tickTime##idx)
#define tock_2_args(idx, comment)							NSLog(@"%@%f", comment, CACurrentMediaTime() -\
																  __tickTime##idx)
#define tock_3_args(idx, comment_prefix, comment_suffix)	NSLog(@"%@%f%@", comment_prefix,\
																  CACurrentMediaTime() - __tickTime##idx,\
																  comment_suffix)
#define tock_macro_chooser(x, A, B, C, FUNC, ...) FUNC

#define tock(...) tock_macro_chooser(,##__VA_ARGS__,\
				  tock_3_args(__VA_ARGS__),\
				  tock_2_args(__VA_ARGS__),\
				  tock_1_args(__VA_ARGS__),\
				  tock_0_args(__VA_ARGS__))

