//
//  SelectAreaViewDelegate.h
//  Select Area Delegate
//
//  Created by Iulian Corcoja on 1/25/16.
//  Copyright © 2016 Iulian Corcoja. All rights reserved.
//

#import <Foundation/Foundation.h>

@class SelectAreaView;

@protocol SelectAreaViewDelegate <NSObject>

@optional

- (void)selectAreaView:(SelectAreaView *)view isChangingAreaRect:(CGRect)rect;

- (void)selectAreaView:(SelectAreaView *)view didChangeAreaRect:(CGRect)rect;

@end
