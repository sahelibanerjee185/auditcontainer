//
//  SelectAreaView.h
//  Select Area
//
//  Created by Iulian Corcoja on 1/21/16.
//  Copyright © 2016 Iulian Corcoja. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SelectAreaViewDelegate.h"

IB_DESIGNABLE
@interface SelectAreaView : UIView <UIGestureRecognizerDelegate>

@property (nonatomic, strong) IBInspectable UIColor *backgroundColor;

@property (nonatomic, strong) IBInspectable UIColor *areaColor;

@property (nonatomic, strong) IBInspectable UIColor *gridColor;

@property (nonatomic, assign) IBInspectable CGFloat areaCornerRadius;

@property (nonatomic, assign) IBInspectable CGFloat gridLineWidth;

@property (nonatomic, assign) IBInspectable CGSize areaScale;

@property (nonatomic, strong) IBInspectable NSString *topText;

@property (nonatomic, strong) IBInspectable NSString *bottomText;

@property (nonatomic, strong) IBInspectable NSString *fontName;

@property (nonatomic, assign) IBInspectable CGFloat fontSize;

@property (nonatomic, strong) IBInspectable UIColor *fontColor;

@property (nonatomic, assign) IBInspectable CGFloat edgeInsetTop;

@property (nonatomic, assign) IBInspectable CGFloat edgeInsetRight;

@property (nonatomic, assign) IBInspectable CGFloat edgeInsetBottom;

@property (nonatomic, assign) IBInspectable CGFloat edgeInsetLeft;

@property (nonatomic, assign, readonly) CGRect areaRect;

@property (nonatomic, weak) IBOutlet id<SelectAreaViewDelegate> delegate;

@end
