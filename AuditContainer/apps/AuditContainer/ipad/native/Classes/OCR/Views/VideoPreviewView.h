//
//  VideoPreviewView.h
//  Optical Character Recognition
//
//  Created by Iulian Corcoja on 1/21/16.
//  Copyright © 2016 Iulian Corcoja. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

@interface VideoPreviewView : UIView

@property (nonatomic) AVCaptureSession *session;

@end
