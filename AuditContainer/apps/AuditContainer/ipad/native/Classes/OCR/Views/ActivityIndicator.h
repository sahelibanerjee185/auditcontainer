//
//  ActivityIndicator.h
//  Activity Indicator
//
//  Created by Iulian Corcoja on 11/17/15.
//
//

#import <UIKit/UIKit.h>

@interface ActivityIndicator : UIView

@property (nonatomic, weak) IBOutlet UILabel *titleLabel;

@property (nonatomic, weak) IBOutlet UIActivityIndicatorView *activityIndicatorView;

@property (nonatomic, assign, getter = isActivityIndicatorViewHidden) BOOL activityIndicatorViewHidden;

- (instancetype)initWithTitle:(NSString *)title;

- (void)showActivityInView:(UIView *)view animated:(BOOL)animated;

- (void)showActivityInView:(UIView *)view forInterval:(NSTimeInterval)interval animated:(BOOL)animated;

- (void)hideActivityAnimated:(BOOL)animated;

@end
