//
//  ActivityIndicator.m
//  Activity Indicator
//
//  Created by Iulian Corcoja on 11/17/15.
//
//

#import "ActivityIndicator.h"

@interface ActivityIndicator ()

@property (nonatomic, weak) IBOutlet NSLayoutConstraint *titleLabelCenterYConstraint;

@end

@implementation ActivityIndicator

#pragma mark - View lifecycle methods

- (instancetype)initWithTitle:(NSString *)title
{
	self = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class]) owner:nil options:nil] firstObject];

	// Make sure activity indicator view was initialized properly
	if (self && [self isKindOfClass:[ActivityIndicator class]]) {
		
		// Set activity indicator properties
		self.titleLabel.text = title;
		
		[self commonInit];
	}
	return self;
}

- (instancetype)initWithCoder:(NSCoder *)coder
{
	self = [super initWithCoder:coder];
	if (self) {
		// Custom initialization...
		[self commonInit];
	}
	return self;
}

- (void)commonInit
{
	// Remove auto-created layout constraints
	self.translatesAutoresizingMaskIntoConstraints = NO;
	
	// Set default value of activity view hidden
	self.activityIndicatorViewHidden = NO;
}

#pragma mark - Setters and getters

- (void)setActivityIndicatorViewHidden:(BOOL)activityIndicatorViewHidden
{
	_activityIndicatorViewHidden = activityIndicatorViewHidden;

	// Set visibility of activity view
	self.activityIndicatorView.hidden = activityIndicatorViewHidden;
	self.titleLabelCenterYConstraint.priority = (activityIndicatorViewHidden ? UILayoutPriorityDefaultHigh :
												 UILayoutPriorityDefaultLow);
}

#pragma mark - Instance methods

- (void)showActivityInView:(UIView *)view animated:(BOOL)animated
{
	// Sanity checks
	if (!view) {
		return;
	}
	
	// Add activity indicator to the view and set constraints to fill the screen
	[view addSubview:self];
	[view bringSubviewToFront:self];
	[view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[view]|" options:0 metrics:nil views:
						  @{@"view" : self}]];
	[view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[view]|" options:0 metrics:nil views:
						  @{@"view" : self}]];
	
	// Show activity indicator
	self.alpha = 0.0f;
	[UIView animateWithDuration:animated ? 0.3f : 0.0f animations:^{
		self.alpha = 1.0f;
	}];
}

- (void)showActivityInView:(UIView *)view forInterval:(NSTimeInterval)interval animated:(BOOL)animated
{
	// First - show activity view
	[self showActivityInView:view animated:animated];
	
	// Second - wait for the interval time to pass
	dispatch_after(dispatch_time(DISPATCH_TIME_NOW, interval * NSEC_PER_SEC), dispatch_get_main_queue(), ^{

		// And last - hide activity view
		[self hideActivityAnimated:animated];
	});
}

- (void)hideActivityAnimated:(BOOL)animated
{
	// Hide activity indicator
	[UIView animateWithDuration:animated ? 0.3f : 0.0f animations:^{
		self.alpha = 0.0f;
	} completion:^(BOOL finished) {
		
		// Remove activity indicator from superview
		[self removeFromSuperview];
	}];
}

@end
