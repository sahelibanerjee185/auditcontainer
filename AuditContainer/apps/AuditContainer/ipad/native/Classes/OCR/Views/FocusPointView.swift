//
//  FocusPointView.swift
//  LiveCharacterRecognitionDemo
//
//  Created by Iulian Corcoja on 8/17/16.
//  Copyright © 2016 Iulian Corcoja. All rights reserved.
//

import UIKit

// MARK: - Constants

private let kFocusPointFlashStartScale = 1.95
private let kFocusPointFlashAlpha = 0.4
private let kFocusPointFlashCount = 2.0
private let kFocusPointFlashAnimationDuration = 0.3
private let kFocusPointAppearanceAnimationDuration = 0.18

// MARK: -

@IBDesignable
public class FocusPointView: UIView {

	// MARK: - Public stored properties

	@IBInspectable
	public var color: UIColor = UIColor.black {
		didSet {
			// Set layer's fill color
			(layer as? FocusPointViewLayer)?.color = color
		}
	}

	@IBInspectable
	public var strokeWidth: CGFloat = 0.0 {
		didSet {

			// Limit stroke width to 0.0
			strokeWidth = max(strokeWidth, 0.0)

			// Set layer's stroke width
			(layer as? FocusPointViewLayer)?.strokeWidth = strokeWidth
		}
	}

	@IBInspectable
	public var middleLinesLenghtScale: CGFloat = 0.0 {
		didSet {

			// Clamp middle lines length scale between 0.0 and 1.0
			middleLinesLenghtScale = min(max(middleLinesLenghtScale, 0.0), 1.0)

			// Set layer's middle lines length scale
			(layer as? FocusPointViewLayer)?.middleLinesLenghtScale = middleLinesLenghtScale
		}
	}

	// MARK: - View's lifecycle methods

    override public init(frame: CGRect) {
		super.init(frame: frame)
		commonInit()
	}

	required public init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
		commonInit()
	}

	func commonInit() {
		layer.opacity = 0.0
	}

	// MARK: - View delegate methods

	override public class var layerClass : AnyClass {
		return FocusPointViewLayer.self
	}

	// MARK: - Views instance methods

	public func startFlashAnimation() {

		// Create animation group containing all the animations
		let animationGroup = CAAnimationGroup()
		animationGroup.duration = kFocusPointAppearanceAnimationDuration + kFocusPointFlashAnimationDuration *
			kFocusPointFlashCount * 2.0 + kFocusPointFlashAnimationDuration

		// First animation is the scale (focus point view shrinks)
		let scaleAnimation = CABasicAnimation(keyPath: "transform")
		scaleAnimation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseIn)
		scaleAnimation.fromValue = NSValue(caTransform3D: CATransform3DMakeScale(CGFloat(kFocusPointFlashStartScale),
			CGFloat(kFocusPointFlashStartScale), 1.0))
		scaleAnimation.toValue = NSValue(caTransform3D: CATransform3DIdentity)
		scaleAnimation.duration = kFocusPointAppearanceAnimationDuration

		// Second animation is the alpha animation (appearing animation)
		let appearanceAnimation = CABasicAnimation(keyPath: "opacity")
		appearanceAnimation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseIn)
		appearanceAnimation.fromValue = 0.0
		appearanceAnimation.toValue = 1.0
		appearanceAnimation.duration = kFocusPointAppearanceAnimationDuration

		// Third animation is the flashing alpha animation
		let flashingAnimation = CABasicAnimation(keyPath: "opacity")
		flashingAnimation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
		flashingAnimation.fromValue = 1.0
		flashingAnimation.toValue = kFocusPointFlashAlpha
		flashingAnimation.repeatCount = Float(kFocusPointFlashCount)
		flashingAnimation.autoreverses = true
		flashingAnimation.beginTime = kFocusPointAppearanceAnimationDuration
		flashingAnimation.duration = kFocusPointFlashAnimationDuration

		// Last animation is the hide animation (alpha to zero)
		let hideAnimation = CABasicAnimation(keyPath: "opacity")
		hideAnimation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseOut)
		hideAnimation.fromValue = 1.0
		hideAnimation.toValue = 0.0
		hideAnimation.beginTime = kFocusPointAppearanceAnimationDuration + kFocusPointFlashAnimationDuration *
			kFocusPointFlashCount * 2.0
		hideAnimation.duration = kFocusPointFlashAnimationDuration

		// Add all animations to the group, then add the animation group to the layer
		animationGroup.animations = [scaleAnimation, appearanceAnimation, flashingAnimation, hideAnimation]
		layer.add(animationGroup, forKey: "flashAnimation")
	}
}

fileprivate class FocusPointViewLayer: CAShapeLayer {

	// MARK: - Public stored properties

	var color: UIColor = UIColor.black {
		didSet {
			// Set fill color
			fillColor = color.cgColor
		}
	}

	var strokeWidth: CGFloat = 1.0 {
		didSet {
			// Update focus point rect path
			path = focusPointRectPath(forBounds: bounds).cgPath
		}
	}

	var middleLinesLenghtScale: CGFloat = 0.0 {
		didSet {
			// Update focus point rect path
			path = focusPointRectPath(forBounds: bounds).cgPath
		}
	}

	// MARK: - Layer's lifecycle methods

	override init() {
		super.init()
		commonInit()
	}

	override init(layer: Any) {
		super.init(layer: layer)
		commonInit()

		// Copy layer properties
		color = (layer as? FocusPointViewLayer)?.color ?? UIColor.black
		strokeWidth = (layer as? FocusPointViewLayer)?.strokeWidth ?? 1.0
	}

	required init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
		commonInit()
	}

	fileprivate func commonInit() {

		// Setup layer properties
		backgroundColor = UIColor.clear.cgColor
		fillRule = kCAFillRuleEvenOdd
	}

	// MARK: - Layer delegate methods

	override func layoutSublayers() {
		super.layoutSublayers()

		// Update focus point rect path
		path = focusPointRectPath(forBounds: bounds).cgPath
	}

	// MARK: - Helper methods

	func focusPointRectPath(forBounds bounds: CGRect) -> UIBezierPath {

		// Compute inner, outer rect sizes and line lenght
		let outerRectSize = min(bounds.width, bounds.height)
		let innerRectSize = outerRectSize - strokeWidth * 2.0
		let lineLegth = (innerRectSize / 2.0) * middleLinesLenghtScale

		// Create outer rect layer path
		let outerRectPath = UIBezierPath(rect: CGRect(
			x: (bounds.width - outerRectSize) / 2.0,
			y: (bounds.height - outerRectSize) / 2.0,
			width: outerRectSize, height: outerRectSize))

		// Create inner rect layer path
		let innerRectPath = UIBezierPath(rect: CGRect(
			x: (bounds.width - innerRectSize) / 2.0,
			y: (bounds.height - innerRectSize) / 2.0,
			width: innerRectSize, height: innerRectSize))

		// Create top, bottom, left and right middle lines
		let topMiddleLinePath = UIBezierPath(rect: CGRect(
			x: (bounds.width - strokeWidth) / 2.0,
			y: (bounds.height - innerRectSize) / 2.0,
			width: strokeWidth, height: lineLegth))

		let bottomMiddleLinePath = UIBezierPath(rect: CGRect(
			x: (bounds.width - strokeWidth) / 2.0,
			y: (bounds.height + innerRectSize) / 2.0 - lineLegth,
			width: strokeWidth, height: lineLegth))

		let leftMiddleLinePath = UIBezierPath(rect: CGRect(
			x: (bounds.width - innerRectSize) / 2.0,
			y: (bounds.height - strokeWidth) / 2.0,
			width: lineLegth, height: strokeWidth))

		let rightMiddleLinePath = UIBezierPath(rect: CGRect(
			x: (bounds.width + innerRectSize) / 2.0 - lineLegth,
			y: (bounds.height - strokeWidth) / 2.0,
			width: lineLegth, height: strokeWidth))

		// Add inner rect path to the outer rect path and set even-odd fill rule to mask the inner rect
		outerRectPath.usesEvenOddFillRule = true
		outerRectPath.append(innerRectPath)

		// Add middle lines paths to the outer rect path
		outerRectPath.append(topMiddleLinePath)
		outerRectPath.append(bottomMiddleLinePath)
		outerRectPath.append(leftMiddleLinePath)
		outerRectPath.append(rightMiddleLinePath)

		return outerRectPath
	}
}
