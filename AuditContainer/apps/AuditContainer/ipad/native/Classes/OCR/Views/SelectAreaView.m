//
//  SelectAreaView.m
//  Select Area
//
//  Created by Iulian Corcoja on 1/21/16.
//  Copyright © 2016 Iulian Corcoja. All rights reserved.
//

#import "SelectAreaView.h"
#import "NSString+ICUtils.h"

#pragma mark - Type defines

#define kSelectAreaTouchSize 112.0f

#define kSelectAreaTouchHalfSize (kSelectAreaTouchSize / 2.0f)

typedef NS_ENUM(NSInteger, TouchAreaLocation) {
	TouchAreaLocationUnknown		= 0,
	TouchAreaLocationTopLeft		= 1,
	TouchAreaLocationTop			= 2,
	TouchAreaLocationTopRight		= 3,
	TouchAreaLocationRight			= 4,
	TouchAreaLocationBottomRight	= 5,
	TouchAreaLocationBottom			= 6,
	TouchAreaLocationBottomLeft		= 7,
	TouchAreaLocationLeft			= 8,
	TouchAreaLocationInsideRect		= 9,
	TouchAreaLocationOutsideRect	= 10
};

#pragma mark - View interface definition

@interface SelectAreaView ()

@property (nonatomic, assign) CGRect areaRect;

@property (nonatomic, strong) UIPanGestureRecognizer *panGestureRecognizer;
@property (nonatomic, strong) UIPinchGestureRecognizer *pinchGestureRecognizer;

@property (nonatomic, strong) NSDictionary *textAttributes;
@property (nonatomic, assign) NSUInteger topTextLineCount;
@property (nonatomic, assign) NSUInteger bottomTextLineCount;

@end

@implementation SelectAreaView

#pragma mark - View's lifecycle methods

- (instancetype)initWithFrame:(CGRect)frame
{
	self = [super initWithFrame:frame];
	if (self) {
		// Initialization code
		[self commonInit];
	}
	return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
	self = [super initWithCoder:aDecoder];
	if (self) {
		// Initialization code
		[self commonInit];
	}
	return self;
}

- (void)commonInit
{
	// Create pan and pinch gesture recognizers
	self.panGestureRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(gestureAction:)];
	self.pinchGestureRecognizer = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:
								   @selector(gestureAction:)];
	self.panGestureRecognizer.delegate = self;
	self.pinchGestureRecognizer.delegate = self;
	self.panGestureRecognizer.minimumNumberOfTouches = 1;
	[self addGestureRecognizer:self.panGestureRecognizer];
	[self addGestureRecognizer:self.pinchGestureRecognizer];
}

#pragma mark - View's auto layout methods

- (void)layoutSubviews
{
	[super layoutSubviews];
	
	// Update area rect
	[self updateAreaScaleAndRect:self.areaScale];
	
	// Redraw view grid
	[self setNeedsDisplay];
}

#pragma mark - Setters and getters

- (void)setBackgroundColor:(UIColor *)backgroundColor
{
	_backgroundColor = backgroundColor;
	
	// Redraw view grid
	[self setNeedsDisplay];
}

- (void)setAreaColor:(UIColor *)areaColor
{
	_areaColor = areaColor;
	
	// Redraw view grid
	[self setNeedsDisplay];
}

- (void)setGridColor:(UIColor *)gridColor
{
	_gridColor = gridColor;
	
	// Redraw view grid
	[self setNeedsDisplay];
}

- (void)setAreaCornerRadius:(CGFloat)areaCornerRadius
{
	_areaCornerRadius = areaCornerRadius;
	
	// Redraw view grid
	[self setNeedsDisplay];
}

- (void)setGridLineWidth:(CGFloat)gridLineWidth
{
	_gridLineWidth = gridLineWidth;
	
	// Redraw view grid
	[self setNeedsDisplay];
}

- (void)setAreaScale:(CGSize)areaScale
{
	[self updateAreaScaleAndRect:areaScale];

	// Call did change area rect delegate method
	if (self.delegate && [self.delegate respondsToSelector:@selector(selectAreaView:didChangeAreaRect:)]) {
		[self.delegate selectAreaView:self didChangeAreaRect:self.areaRect];
	}
}

// This helper method was created with the intention to prevent calling delegate methods while setting area scale from
// internal code with setAreaScale:

- (void)updateAreaScaleAndRect:(CGSize)areaScale
{
	// Get view bounds with edge inset and minimum area scale
	CGRect bounds = [self edgeInsetBounds];
	CGSize minAreaScale = CGSizeMake(kSelectAreaTouchSize / CGRectGetWidth(bounds),
									 kSelectAreaTouchSize / CGRectGetHeight(bounds));

	// Clamp and set area scale (clamp between mininum area scale and 1.0)
	_areaScale = CGSizeMake(MAX(minAreaScale.width, MIN(1.0f, areaScale.width)),
							MAX(minAreaScale.height, MIN(1.0f, areaScale.height)));

	// Set area rect
	self.areaRect = [self areaRectFromScale:_areaScale withinBounds:bounds];

	// Redraw view grid
	[self setNeedsDisplay];
}

- (void)setTopText:(NSString *)topText
{
	_topText = topText;

	// Set number of lines for top text
	self.topTextLineCount = topText.numberOfLines;

	// Redraw view grid
	[self setNeedsDisplay];
}

- (void)setBottomText:(NSString *)bottomText
{
	_bottomText = bottomText;

	// Set number of lines for bottom text
	self.bottomTextLineCount = bottomText.numberOfLines;

	// Redraw view grid
	[self setNeedsDisplay];
}

- (void)setTopTextLineCount:(NSUInteger)topTextLineCount
{
	_topTextLineCount = MAX(topTextLineCount, 1);
}

- (void)setBottomTextLineCount:(NSUInteger)bottomTextLineCount
{
	_bottomTextLineCount = MAX(bottomTextLineCount, 1);
}

- (void)setFontName:(NSString *)fontName
{
	_fontName = fontName;
	
	// Update text attributes and redraw view grid
	[self updateTextAttributes];
	[self setNeedsDisplay];
}

- (void)setFontSize:(CGFloat)fontSize
{
	_fontSize = MAX(1.0f, fontSize);
	
	// Update text attributes and redraw view grid
	[self updateTextAttributes];
	[self setNeedsDisplay];
}

- (void)setFontColor:(UIColor *)fontColor
{
	_fontColor = fontColor;
	
	// Update text attributes and redraw view grid
	[self updateTextAttributes];
	[self setNeedsDisplay];
}

- (void)setEdgeInsetTop:(CGFloat)edgeInsetTop
{
	_edgeInsetTop = edgeInsetTop;
	
	// Update layout
	[self setNeedsLayout];
}

- (void)setEdgeInsetRight:(CGFloat)edgeInsetRight
{
	_edgeInsetRight = edgeInsetRight;
	
	// Update layout
	[self setNeedsLayout];
}

- (void)setEdgeInsetBottom:(CGFloat)edgeInsetBottom
{
	_edgeInsetBottom = edgeInsetBottom;
	
	// Update layout
	[self setNeedsLayout];
}

- (void)setEdgeInsetLeft:(CGFloat)edgeInsetLeft
{
	_edgeInsetLeft = edgeInsetLeft;
	
	// Update layout
	[self setNeedsLayout];
}

#pragma mark - Gesture recognizers and actions

- (void)gestureAction:(UIGestureRecognizer *)gestureRecognizer
{
	static TouchAreaLocation touchAreaLocation = TouchAreaLocationUnknown;
	static CGSize areaScale;
	static CGPoint startTouchLocation;
	
	// Get gesture state
	switch (gestureRecognizer.state) {
		case UIGestureRecognizerStateBegan: {
			
			// Get finger start touch location and set touch location area depending on touch location
			if (gestureRecognizer.numberOfTouches > 0) {
				startTouchLocation = [gestureRecognizer locationOfTouch:0 inView:gestureRecognizer.view];
			} else {
				startTouchLocation = [gestureRecognizer locationInView:gestureRecognizer.view];
			}
			touchAreaLocation = [self touchAreaLocationFromTouchLocation:startTouchLocation];
			
			// Set initial area scale
			areaScale = self.areaScale;
			break;
		}
		case UIGestureRecognizerStateChanged: {
			
			// If this is pinch gesture recognizer and only one finger left, do not compute changed state anymore
			if ([gestureRecognizer isKindOfClass:[UIPinchGestureRecognizer class]]) {
				if (gestureRecognizer.numberOfTouches < 2) {
					break;
				}
			}
			
			// Get finger translation distance based on gesture recognizer type
			CGPoint translation;
			if ([gestureRecognizer isKindOfClass:[UIPanGestureRecognizer class]]) {
				
				// Get pan gesture recognizer translation
				translation = [(UIPanGestureRecognizer *) gestureRecognizer translationInView:gestureRecognizer.view];
			} else {
				
				// Handle translation manually because this gesture recognizer is a generic one
				CGPoint touchLocation;

				if (gestureRecognizer.numberOfTouches > 0) {
					touchLocation = [gestureRecognizer locationOfTouch:0 inView:gestureRecognizer.view];
				} else {
					touchLocation = [gestureRecognizer locationInView:gestureRecognizer.view];
				}
				
				translation = CGPointMake(touchLocation.x - startTouchLocation.x,
										  touchLocation.y - startTouchLocation.y);
			}
			
			// New area scale to set and proportional area scale
			CGSize newAreaScale;
			CGSize proportionalTranslation = CGSizeMake(translation.x / self.bounds.size.width * 2.0f,
														translation.y / self.bounds.size.height * 2.0f);
			
			// Depending on touch area location, move the area rectangle
			switch (touchAreaLocation) {
				case TouchAreaLocationTop:
					newAreaScale = CGSizeMake(self.areaScale.width, areaScale.height - proportionalTranslation.height);
					break;
				case TouchAreaLocationBottom:
					newAreaScale = CGSizeMake(self.areaScale.width, areaScale.height + proportionalTranslation.height);
					break;
				case TouchAreaLocationLeft:
					newAreaScale = CGSizeMake(areaScale.width - proportionalTranslation.width, self.areaScale.height);
					break;
				case TouchAreaLocationRight:
					newAreaScale = CGSizeMake(areaScale.width + proportionalTranslation.width, self.areaScale.height);
					break;
				case TouchAreaLocationTopLeft:
					newAreaScale = CGSizeMake(areaScale.width - proportionalTranslation.width,
											  areaScale.height - proportionalTranslation.height);
					break;
				case TouchAreaLocationTopRight:
					newAreaScale = CGSizeMake(areaScale.width + proportionalTranslation.width,
											  areaScale.height - proportionalTranslation.height);
					break;
				case TouchAreaLocationBottomLeft:
					newAreaScale = CGSizeMake(areaScale.width - proportionalTranslation.width,
											  areaScale.height + proportionalTranslation.height);
					break;
				case TouchAreaLocationBottomRight:
					newAreaScale = CGSizeMake(areaScale.width + proportionalTranslation.width,
											  areaScale.height + proportionalTranslation.height);
					break;
				default:
					newAreaScale = areaScale;
					break;
			}

			// Set new area scale
			[self updateAreaScaleAndRect:newAreaScale];

			// Call is changing area rect delegate method
			if (self.delegate && [self.delegate respondsToSelector:@selector(selectAreaView:isChangingAreaRect:)]) {
				[self.delegate selectAreaView:self isChangingAreaRect:self.areaRect];
			}
			break;
		}
		case UIGestureRecognizerStateEnded: {
			
			// Reset touch area location
			touchAreaLocation = TouchAreaLocationUnknown;
			
			// Call did change area rect delegate method
			if (self.delegate && [self.delegate respondsToSelector:@selector(selectAreaView:didChangeAreaRect:)]) {
				[self.delegate selectAreaView:self didChangeAreaRect:self.areaRect];
			}
			break;
		}
		default: {
			
			// Gesture cancelled or failed - set touch area location to unknown
			touchAreaLocation = TouchAreaLocationUnknown;
			break;
		}
	}
}

#pragma mark - Gesture recognizer delegate methods

- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer
{
	if (gestureRecognizer == self.panGestureRecognizer || gestureRecognizer == self.pinchGestureRecognizer) {
		CGPoint touchLocation;

		if (gestureRecognizer.numberOfTouches > 0) {
			touchLocation = [gestureRecognizer locationOfTouch:0 inView:gestureRecognizer.view];
		} else {
			touchLocation = [gestureRecognizer locationInView:gestureRecognizer.view];
		}

		TouchAreaLocation touchAreaLocation = [self touchAreaLocationFromTouchLocation:touchLocation];
		return (touchAreaLocation != TouchAreaLocationInsideRect &&
				touchAreaLocation != TouchAreaLocationOutsideRect);
	}
	return YES;
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer
shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
	if (gestureRecognizer == self.panGestureRecognizer || otherGestureRecognizer == self.panGestureRecognizer ||
		gestureRecognizer == self.pinchGestureRecognizer || otherGestureRecognizer == self.pinchGestureRecognizer) {
		return NO;
	}
	return YES;
}

#pragma mark - View delegate methods

- (BOOL)pointInside:(CGPoint)point withEvent:(UIEvent *)event
{
	TouchAreaLocation touchAreaLocation = [self touchAreaLocationFromTouchLocation:point];
	
	// Check if point is inside or outside the select area
	if (touchAreaLocation == TouchAreaLocationInsideRect || touchAreaLocation == TouchAreaLocationOutsideRect) {
		return NO;
	}
	return YES;
}

#pragma mark - Helper methods

- (TouchAreaLocation)touchAreaLocationFromTouchLocation:(CGPoint)touchLocation
{
	// Detect finger touch location
	if (CGRectContainsPoint(CGRectMake(CGRectGetMinX(self.areaRect) + kSelectAreaTouchHalfSize,
									   CGRectGetMinY(self.areaRect) - kSelectAreaTouchHalfSize,
									   CGRectGetWidth(self.areaRect) - kSelectAreaTouchSize,
									   kSelectAreaTouchSize), touchLocation)) {
		// Top line
		return TouchAreaLocationTop;
		
	} else if (CGRectContainsPoint(CGRectMake(CGRectGetMinX(self.areaRect) + kSelectAreaTouchHalfSize,
											  CGRectGetMaxY(self.areaRect) - kSelectAreaTouchHalfSize,
											  CGRectGetWidth(self.areaRect) - kSelectAreaTouchSize,
											  kSelectAreaTouchSize), touchLocation)) {
		// Bottom line
		return TouchAreaLocationBottom;
		
	} else if (CGRectContainsPoint(CGRectMake(CGRectGetMinX(self.areaRect) - kSelectAreaTouchHalfSize,
											  CGRectGetMinY(self.areaRect) + kSelectAreaTouchHalfSize,
											  kSelectAreaTouchSize, CGRectGetHeight(self.areaRect) -
											  kSelectAreaTouchSize), touchLocation)) {
		// Left line
		return TouchAreaLocationLeft;
		
	} else if (CGRectContainsPoint(CGRectMake(CGRectGetMaxX(self.areaRect) - kSelectAreaTouchHalfSize,
											  CGRectGetMinY(self.areaRect) + kSelectAreaTouchHalfSize,
											  kSelectAreaTouchSize, CGRectGetHeight(self.areaRect) -
											  kSelectAreaTouchSize), touchLocation)) {
		// Right line
		return TouchAreaLocationRight;
		
	} else if (CGRectContainsPoint(CGRectMake(CGRectGetMinX(self.areaRect) - kSelectAreaTouchHalfSize,
											  CGRectGetMinY(self.areaRect) - kSelectAreaTouchHalfSize,
											  kSelectAreaTouchSize, kSelectAreaTouchSize), touchLocation)) {
		// Top left corner
		return TouchAreaLocationTopLeft;
		
	} else if (CGRectContainsPoint(CGRectMake(CGRectGetMaxX(self.areaRect) - kSelectAreaTouchHalfSize,
											  CGRectGetMinY(self.areaRect) - kSelectAreaTouchHalfSize,
											  kSelectAreaTouchSize, kSelectAreaTouchSize), touchLocation)) {
		// Top right corner
		return TouchAreaLocationTopRight;
		
	} else if (CGRectContainsPoint(CGRectMake(CGRectGetMinX(self.areaRect) - kSelectAreaTouchHalfSize,
											  CGRectGetMaxY(self.areaRect) - kSelectAreaTouchHalfSize,
											  kSelectAreaTouchSize, kSelectAreaTouchSize), touchLocation)) {
		// Bottom left corner
		return TouchAreaLocationBottomLeft;
		
	} else if (CGRectContainsPoint(CGRectMake(CGRectGetMaxX(self.areaRect) - kSelectAreaTouchHalfSize,
											  CGRectGetMaxY(self.areaRect) - kSelectAreaTouchHalfSize,
											  kSelectAreaTouchSize, kSelectAreaTouchSize), touchLocation)) {
		// Bottom right corner
		return TouchAreaLocationBottomRight;
		
	} else if (CGRectContainsPoint(self.areaRect, touchLocation)) {
		
		// Inside select rectangle
		return TouchAreaLocationInsideRect;
	}
	
	// Outside select rectangle
	return TouchAreaLocationOutsideRect;
}

- (CGRect)areaRectFromScale:(CGSize)areaScale withinBounds:(CGRect)bounds
{
	return CGRectMake(CGRectGetMinX(bounds) + (CGRectGetWidth(bounds) / 2.0f) * (1.0f - areaScale.width),
					  CGRectGetMinY(bounds) + (CGRectGetHeight(bounds) / 2.0f) * (1.0f - areaScale.height),
					  CGRectGetWidth(bounds) * areaScale.width,
					  CGRectGetHeight(bounds) * areaScale.height);
}

- (CGRect)edgeInsetBounds
{
	// Return view bounds with edge inset applied
	return UIEdgeInsetsInsetRect(self.bounds, UIEdgeInsetsMake(self.edgeInsetTop, self.edgeInsetLeft,
															   self.edgeInsetBottom, self.edgeInsetRight));
}

- (void)updateTextAttributes
{
	static NSMutableParagraphStyle *paragrahpStyle = nil;
	
	// Sanity checks
	if (self.fontName && self.fontColor) {
		
		// Lazy instantiate text paragraph style
		if (!paragrahpStyle) {
			
			paragrahpStyle = [[NSParagraphStyle defaultParagraphStyle] mutableCopy];
			paragrahpStyle.alignment = NSTextAlignmentCenter;
		}
		
		// Create font for the text
		UIFont *font = nil;
		
		if ([self.fontName.lowercaseString isEqualToString:@"system"]) {
			font = [UIFont systemFontOfSize:self.fontSize];
		} else {
			font = [UIFont fontWithName:self.fontName size:self.fontSize];
		}
		
		// Check font for nil before creating the dictionary
		if (!font) {
			
			// Get system font
			font = [UIFont systemFontOfSize:self.fontSize];
		}
		
		// Set font specific variables and text attributes
		self.textAttributes = @{NSFontAttributeName: font,
								NSStrokeWidthAttributeName: @(0),
								NSForegroundColorAttributeName: self.fontColor,
								NSParagraphStyleAttributeName: paragrahpStyle};
	}
}

#pragma mark - Core graphics delegate methods

// Only override drawRect: if you perform custom drawing. An empty implementation adversely affects performance during
// animation
- (void)drawRect:(CGRect)dirtyRect
{
	// Get the current graphics context and clear it
	CGContextRef context = UIGraphicsGetCurrentContext();
	CGContextClearRect(context, dirtyRect);
	
	// Get view bounds with edge inset
	CGRect bounds = [self edgeInsetBounds];
	
	// Fill whole are with the background color
	[self.backgroundColor setFill];
	CGContextFillRect(context, bounds);
	
	// Draw grid lines
	[self.gridColor setStroke];
	CGContextSetLineWidth(context, self.gridLineWidth);
	
	// Draw top grid line
	CGContextMoveToPoint(context, CGRectGetMinX(bounds), CGRectGetMinY(self.areaRect) + self.areaCornerRadius);
	CGContextAddLineToPoint(context, CGRectGetMaxX(bounds), CGRectGetMinY(self.areaRect) + self.areaCornerRadius);
	CGContextStrokePath(context);
	
	// Draw bottom grid line
	CGContextMoveToPoint(context, CGRectGetMinX(bounds), CGRectGetMaxY(self.areaRect) - self.areaCornerRadius);
	CGContextAddLineToPoint(context, CGRectGetMaxX(bounds), CGRectGetMaxY(self.areaRect) - self.areaCornerRadius);
	CGContextStrokePath(context);
	
	// Draw left grid line
	CGContextMoveToPoint(context, CGRectGetMinX(self.areaRect) + self.areaCornerRadius, CGRectGetMinY(bounds));
	CGContextAddLineToPoint(context, CGRectGetMinX(self.areaRect) + self.areaCornerRadius, CGRectGetMaxY(bounds));
	CGContextStrokePath(context);
	
	// Draw left grid line
	CGContextMoveToPoint(context, CGRectGetMaxX(self.areaRect) - self.areaCornerRadius, CGRectGetMinY(bounds));
	CGContextAddLineToPoint(context, CGRectGetMaxX(self.areaRect) - self.areaCornerRadius, CGRectGetMaxY(bounds));
	CGContextStrokePath(context);
	
	// Clip selected area be setting blend mode to clear
	CGContextSetBlendMode(context, kCGBlendModeClear);
	
	// Create bezier path with rounded corners insire area rectangle and fill it
	UIBezierPath *selectedAreaPath = [UIBezierPath bezierPathWithRoundedRect:self.areaRect
																cornerRadius:self.areaCornerRadius];
	[[UIColor whiteColor] setFill];
	[selectedAreaPath fill];
	
	// Back to normal blend mode
	CGContextSetBlendMode(context, kCGBlendModeNormal);
	
	// Fill selected area path with its color
	[self.areaColor setFill];
	[selectedAreaPath fill];
	
	selectedAreaPath.lineWidth = self.gridLineWidth;
	[selectedAreaPath stroke];
	
	// Draw the text above top grid line
	if (self.topText && self.fontName && self.fontColor) {
		
		// Define draw rect for text
		CGRect textRect = CGRectMake(0.0f, CGRectGetMinY(self.areaRect) + self.areaCornerRadius - self.fontSize * 2.0f *
									 self.topTextLineCount, CGRectGetWidth(bounds), self.fontSize * 2.0f *
									 self.topTextLineCount);
		// Draw the text
		[self.topText drawInRect:textRect withAttributes:self.textAttributes];
	}

	// Draw the text below bottom grid line
	if (self.bottomText && self.fontName && self.fontColor) {

		// Define draw rect for text
		CGRect textRect = CGRectMake(0.0f, CGRectGetMaxY(self.areaRect) + self.areaCornerRadius, CGRectGetWidth(bounds),
									 self.fontSize * 2.0f * self.bottomTextLineCount);
		// Draw the text
		[self.bottomText drawInRect:textRect withAttributes:self.textAttributes];
	}
}

#pragma mark - View's memory management methods

- (void)dealloc
{
	[[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
