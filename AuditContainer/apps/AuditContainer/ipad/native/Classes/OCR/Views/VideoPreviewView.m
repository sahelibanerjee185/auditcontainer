//
//  VideoPreviewView.m
//  Optical Character Recognition
//
//  Created by Iulian Corcoja on 1/21/16.
//  Copyright © 2016 Iulian Corcoja. All rights reserved.
//

#import "VideoPreviewView.h"

@implementation VideoPreviewView

+ (Class)layerClass
{
	return [AVCaptureVideoPreviewLayer class];
}

- (AVCaptureSession *)session
{
	AVCaptureVideoPreviewLayer *previewLayer = (AVCaptureVideoPreviewLayer *) self.layer;
	return previewLayer.session;
}

- (void)setSession:(AVCaptureSession *)session
{
	AVCaptureVideoPreviewLayer *previewLayer = (AVCaptureVideoPreviewLayer *) self.layer;
	previewLayer.session = session;
}

@end
