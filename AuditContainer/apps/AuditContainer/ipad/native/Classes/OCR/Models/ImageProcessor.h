//
//  ImageProcessor.h
//  Optical Character Recognition - Image Processor
//
//  Created by Iulian Corcoja on 3/8/16.
//  Copyright © 2016 Iulian Corcoja. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface ImageProcessor : NSObject

+ (UIImage *)prepareImageForOCR:(UIImage *)image;

+ (UIImage *)prepareImageForOCR:(UIImage *)image
	  withPerspectiveCorrection:(float)perspectiveAngle
				 tiltCorrection:(float)tiltAngle;

+ (NSArray<UIImage *> *)prepareImageForOCR:(UIImage *)image
				 withPerspectiveCorrection:(float)perspectiveAngle
							tiltCorrection:(float)tiltAngle
				   includeProcessingStages:(BOOL)includeProcessingStages;

@end

