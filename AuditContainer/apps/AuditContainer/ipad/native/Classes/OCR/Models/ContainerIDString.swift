//
//  ContainerIDString.swift
//  AuditContainerAuditContainerIpad
//
//  Created by Andrew Radulescu on 5/16/17.
//
//

import UIKit

let kContainerIdLastResortString = "???????????"

public class ContainerIDString: NSObject {
    
    public static func getContainerID(fromString string: String) -> String {
        
        // Trim whitespaces and newlines from the text
        let textChunks = string.uppercased().components(
            separatedBy: CharacterSet.whitespacesAndNewlines)
        let text = textChunks.joined(separator: "")
        
        // Check if the string is empty (this means no text was recognized)
        if text.isEmpty {
            
            // Return last resort string
            return kContainerIdLastResortString
        }
        
        // Separate the string in two parts: prefix - the part containing only characters, and prefix - the part
        // containing only numbers
        var prefix = ""
        var suffix = ""
        
        // If we have 2 chunk sizes, than most certainly its the suffix and prefix
        if textChunks.count == 2 {
            prefix = textChunks[0]
            suffix = textChunks[1]
            
        } else {
            
            // Prefix is equal to the string before first digit is found, suffix is the rest of the string
            if let range = text.rangeOfCharacter(from: CharacterSet.decimalDigits) {
                prefix = text.substring(to: range.lowerBound)
                suffix = text.substring(from: range.lowerBound)
            } else {
                
                // No digits were found: prefix is the first four characters, suffix is the rest
                if let index = text.characters.index(text.startIndex, offsetBy: 4, limitedBy: text.endIndex) {
                    prefix = text.substring(to: index)
                    suffix = text.substring(from: index)
                } else {
                    
                    // Recognized text length is less then 4 characters - prefix is the full recognized text,
                    // suffix is an empty string
                    prefix = text
                    suffix = ""
                }
            }
        }
        
        // Replace digits to characters by visual resemblance (for suffix)
        for (idx, char) in prefix.characters.enumerated() {
            let range = Range(prefix.characters.index(prefix.startIndex, offsetBy: idx)..<prefix.characters.index(prefix.startIndex, offsetBy: idx + 1))
            switch char {
            case "1": prefix = prefix.replacingCharacters(in: range, with: "M")
            case "2": prefix = prefix.replacingCharacters(in: range, with: "Z")
            case "3": prefix = prefix.replacingCharacters(in: range, with: "B")
            case "4": prefix = prefix.replacingCharacters(in: range, with: "A")
            case "5": prefix = prefix.replacingCharacters(in: range, with: "S")
            case "6": prefix = prefix.replacingCharacters(in: range, with: "G")
            case "7": prefix = prefix.replacingCharacters(in: range, with: "T")
            case "8": prefix = prefix.replacingCharacters(in: range, with: "B")
            case "9": prefix = prefix.replacingCharacters(in: range, with: "?")
            case "0": prefix = prefix.replacingCharacters(in: range, with: "O")
            default : break
            }
        }
        
        // Replace digits to characters by visual resemblance (for suffix)
        for (idx, char) in suffix.characters.enumerated() {
            let range = Range(suffix.characters.index(suffix.startIndex, offsetBy: idx)..<suffix.characters.index(suffix.startIndex, offsetBy: idx + 1))
            switch char {
            case "0"..."9": break
            case "I", "L": suffix = suffix.replacingCharacters(in: range, with: "1")
            case "Z": suffix = suffix.replacingCharacters(in: range, with: "2")
            case "E", "F": suffix = suffix.replacingCharacters(in: range, with: "3")
            case "A", "K": suffix = suffix.replacingCharacters(in: range, with: "4")
            case "S": suffix = suffix.replacingCharacters(in: range, with: "5")
            case "G": suffix = suffix.replacingCharacters(in: range, with: "6")
            case "J", "T", "V", "Y": suffix = suffix.replacingCharacters(in: range, with: "7")
            case "B", "R", "X": suffix = suffix.replacingCharacters(in: range, with: "8")
            case "P": suffix = suffix.replacingCharacters(in: range, with: "9")
            case "C", "D", "O", "Q", "U": suffix = suffix.replacingCharacters(in: range, with: "0")
            default: suffix = suffix.replacingCharacters(in: range, with: "?")
            }
        }
        
        // Prefix: append question mark character if there are not enough characters, or truncate the string if there
        // are too much characters
        if prefix.characters.count < 4 {
            prefix += String(repeating: "?", count: 4 - prefix.characters.count)
        } else if prefix.characters.count > 4 {
            prefix = prefix.substring(to: prefix.characters.index(prefix.startIndex, offsetBy: 4))
        }
        
        // Suffix: append question mark character if there are not enough characters, or truncate the string if there
        // are too much characters
        if suffix.characters.count < 7 {
            suffix += String(repeating: "?", count: 7 - suffix.characters.count)
        } else if suffix.characters.count > 7 {
            suffix = suffix.substring(to: suffix.characters.index(suffix.startIndex, offsetBy: 7))
        }
        
        // Return prefix concatenated with suffix
        return prefix + suffix
    }
}
