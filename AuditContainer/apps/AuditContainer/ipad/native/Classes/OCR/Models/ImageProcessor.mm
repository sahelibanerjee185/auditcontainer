//
//  ImageProcessor.m
//  Optical Character Recognition - Image Processor
//
//  Created by Iulian Corcoja on 3/8/16.
//  Copyright © 2016 Iulian Corcoja. All rights reserved.
//

#import <opencv2/opencv.hpp>
#import <opencv2/text.hpp>
#import <algorithm>
#import <vector>
#import <numeric>
#import "ImageProcessor.h"
#import "UIImage+OpenCV.h"

#pragma mark - Type definitions

typedef NS_ENUM(NSInteger, RectangleFilterLevel) {
	kRectangleFilterLevelNone					= 0,
	kRectangleFilterLevelRatioOnly				= 1,
	kRectangleFilterLevelRatioWithAverageHeight	= 2
};

typedef NS_ENUM(NSInteger, ContourDetectionApprox) {
	kContourDetectionApproxNone				= cv::CHAIN_APPROX_NONE,
	kContourDetectionApproxSimple			= cv::CHAIN_APPROX_SIMPLE,
	kContourDetectionApproxTehChin89L1		= cv::CHAIN_APPROX_TC89_L1,
	kContourDetectionApproxTehChin89Kcos	= cv::CHAIN_APPROX_TC89_KCOS,
};

typedef NS_ENUM(NSInteger, LastDigitRectDetection) {
	kLastDigitRectDetectionNone				= 0,
	kLastDigitRectDetectionHoughLines		= 1,
	kLastDigitRectDetectionDouglasPeucker	= 2
};

typedef cv::Vec4i Segment;
typedef Segment Line;
typedef cv::Vec8i Quad;
typedef std::vector<Segment> SegmentVector;
typedef std::vector<Quad> QuadVector;
typedef std::vector<cv::Rect> RectVector;
typedef std::pair<cv::Rect, RectVector> RelatedRects;
typedef std::vector<RelatedRects> RelatedRectsVector;
typedef std::vector<cv::Point> PointVector;
typedef std::vector<PointVector> ContourVector;

#pragma mark - Constant definitions

#define kImageAlpha 1.10
#define kImageBeta -16.0
#define kImageProcessingBlurValue MAX(1.0, imageLargestSide * 0.003)
#define kImageMinProcessingSize 256.0
#define kImageMaxProcessingSize 2048.0
#define kImageMinLuminance 160.0

#define kImageCleanSmallKernelSide (7.0 / 400.0)
#define kImageCleanMediumKernelSide (9.0 / 400.0)
#define kImageCleanLargeKernelSide (11.0 / 400.0)

// #define kComputeBoundingRectSizeInMinMaxRange
// #define kComputeQuadByAveragingFilteredQuads

/*
 Value extracted based on the following measurements (average):
 1)  0.6000000000
 2)  0.5952380952
 3)  0.5000000000
 4)  0.5714285714
 5)  0.6250000000
 6)  0.6666666667
 7)  0.4909090909
 8)  0.5833333333
 9)  0.5932203390
 10) 0.6279069767
 */
#define kLastDigitBoundingRectAverageRectWidthRatio 0.5853703073
#define kLastDigitBoundingRectHeightCharacterRatio 1.1

@implementation ImageProcessor

#pragma mark - Image processor class methods

+ (UIImage *)prepareImageForOCR:(UIImage *)image
{
	return [self prepareImageForOCR:image withPerspectiveCorrection:0.0 tiltCorrection:0.0];
}

+ (UIImage *)prepareImageForOCR:(UIImage *)image
	  withPerspectiveCorrection:(float)perspectiveAngle
				 tiltCorrection:(float)tiltAngle
{
	return [[self prepareImageForOCR:image withPerspectiveCorrection:perspectiveAngle tiltCorrection:tiltAngle
			 includeProcessingStages:NO] firstObject];
}

+ (NSArray<UIImage *> *)prepareImageForOCR:(UIImage *)image
				 withPerspectiveCorrection:(float)perspectiveAngle
							tiltCorrection:(float)tiltAngle
				   includeProcessingStages:(BOOL)includeProcessingStages
{
	__block NSMutableArray<UIImage *> *outputImages = [[NSMutableArray alloc] init];
	__block cv::Mat imageMatrix;
	__block cv::Mat outputImageMatrix;
	__block cv::Mat maskImageMatrix;
	int imageLargestSide;

	// Create a dispatch group for multi-threaded processing
	dispatch_group_t dispatch_group = dispatch_group_create();

	// Get image matrix raw data in OpenCV format
	imageMatrix = image.cvMat;

	NSLog(@"prepare image with size: %dx%d", imageMatrix.size().width, imageMatrix.size().height);
	NSLog(@"correct perspective with: %.02f°", perspectiveAngle * (180.0 / M_PI));
	NSLog(@"correct tilt with: %.02f°", tiltAngle * (180.0 / M_PI));

#pragma mark STEP 1 - Perspective and tilt correction and image resizing

	// Apply perspective and tilt correction
	tick(perspective);
	imageMatrix = [self fixImage:imageMatrix withPerspectiveAngle:perspectiveAngle andTiltAngle:tiltAngle];
	tock(perspective, @"background thread (perspective correction): work done in ", @"s");

	imageLargestSide = (imageMatrix.size().width > imageMatrix.size().height ?
						imageMatrix.size().width :
						imageMatrix.size().height);

	// Resize the image if it's fit size is larger than image max processing size
	if (imageLargestSide > kImageMaxProcessingSize) {

		// Image is larger - get new resized width and height
		cv::Size size;
		imageLargestSide = kImageMaxProcessingSize;

		if (imageMatrix.size().width > imageMatrix.size().height) {
			size.width = kImageMaxProcessingSize;
			size.height = (((float) imageMatrix.size().height / (float) imageMatrix.size().width) *
						   kImageMaxProcessingSize);
		} else {
			size.height = kImageMaxProcessingSize;
			size.width = (((float) imageMatrix.size().width / (float) imageMatrix.size().height) *
						  kImageMaxProcessingSize);
		}

		NSLog(@"image resize: %dx%d to %dx%d", imageMatrix.size().width, imageMatrix.size().height,
			  size.width, size.height);

		// Resize the image to fit max processing size
		cv::resize(imageMatrix, imageMatrix, size);
	}

#pragma mark STEP 2 - Image grayscaling

	if (imageMatrix.type() != CV_8UC1 && imageMatrix.type() != CV_8SC1) {

		// Convert the image to HSV color space and extract the lightness component - this will be used for further
		// processing. Light component allows to detect the text ignoring any sun glares or uneven light
		std::vector<cv::Mat> hsvChannels;
		cv::cvtColor(imageMatrix, imageMatrix, CV_RGB2HSV_FULL);
		cv::split(imageMatrix, hsvChannels);
		hsvChannels[2].copyTo(imageMatrix);
	}

#pragma mark STEP 3 - Increase image contrast using alpha and beta values

	// Increase image contrast using alpha and beta values
	imageMatrix.convertTo(imageMatrix, -1, kImageAlpha, kImageBeta);

	// Increase image luminance is case
	double luminance = cv::mean(imageMatrix)[0];

	NSLog(@"image luminance: %g", luminance);

	if (luminance < kImageMinLuminance) {
		imageMatrix *= (kImageMinLuminance / luminance);
	}

	NSLog(@"image luminance: %g", cv::mean(imageMatrix)[0]);

#pragma mark STEP 4 - Redraft the image in order to contain light background with dark text on

	// Redraft the image in order to contain light background with dark text on
	tick(image_redrafting);

	BOOL changesForWhiteText;
	imageMatrix = [self redraftImage:imageMatrix toWhiteBackgrounds:YES changes:&changesForWhiteText];

	tock(image_redrafting, ([NSString stringWithFormat:@"image redrafting (%@ changes): ",
							 changesForWhiteText ? @"with" : @"no"]), @"s");

	if (includeProcessingStages) {
		[outputImages addObject:[UIImage imageFromCVMatrix:imageMatrix]];
	}

#pragma mark STEP 5 - Image binarization using Otsu and adaptive threshold methods

	dispatch_group_async(dispatch_group, dispatch_get_global_queue(QOS_CLASS_BACKGROUND, 0), ^{
		tick(filter_mask);

		// Create a mask for filtering out noise from the original image. The mask is a blurred binarized, and scaled
		// down image. Blur value is 1.00% from the processing image size
		int scaledDownLargestSide = imageLargestSide / 2;
		int blurValue = MAX(3, (int) [self nearestOddNumber:(double) scaledDownLargestSide * 0.01]);

		cv::pyrDown(imageMatrix, maskImageMatrix);
		cv::GaussianBlur(maskImageMatrix, maskImageMatrix, cv::Size(blurValue, blurValue), 0.0);

		cv::threshold(maskImageMatrix, maskImageMatrix, 128.0, 255.0, cv::THRESH_BINARY | cv::THRESH_OTSU);
		cv::morphologyEx(maskImageMatrix, maskImageMatrix, cv::MORPH_ERODE,
						 cv::getStructuringElement(cv::MORPH_RECT, cv::Size(MAX(3.0, scaledDownLargestSide * 0.005),
																			MAX(3.0, scaledDownLargestSide * 0.005))));
		cv::resize(maskImageMatrix, maskImageMatrix, imageMatrix.size(), cv::INTER_LINEAR);

		if (includeProcessingStages) {
			[outputImages addObject:[UIImage imageFromCVMatrix:maskImageMatrix]];
		}

		tock(filter_mask, @"thread 0 (de-noise mask creation): work done in ", ([NSString stringWithFormat:@"s (%d)",
																				 blurValue]));
	});

	dispatch_group_async(dispatch_group, dispatch_get_global_queue(QOS_CLASS_BACKGROUND, 0), ^{
		tick(binary_image);

		// Blur a little original image
		cv::blur(imageMatrix, outputImageMatrix, cv::Size(kImageProcessingBlurValue, kImageProcessingBlurValue));

		// Binarize image using adaptive threshold. Threshold value is 6.50% from the processing image size
		int thresholdValue = MAX(3, (int) [self nearestOddNumber:(double) imageLargestSide * 0.065]);
		cv::adaptiveThreshold(outputImageMatrix, outputImageMatrix, 255.0, cv::ADAPTIVE_THRESH_MEAN_C,
							  cv::THRESH_BINARY, thresholdValue, thresholdValue * 0.05);

		tock(binary_image, @"thread 1 (original image binarization): work done in ", ([NSString stringWithFormat:
																					   @"s (%d)", thresholdValue]));
		if (includeProcessingStages) {
			[outputImages addObject:[UIImage imageFromCVMatrix:outputImageMatrix]];
		}
	});

	// Wait for other threads to complete data processing before proceeding further
	dispatch_group_wait(dispatch_group, DISPATCH_TIME_FOREVER);

	tick(image_filtering);

	// Apply mask on original binarized image
	cv::bitwise_or(outputImageMatrix, maskImageMatrix, outputImageMatrix);

	if (includeProcessingStages) {
		[outputImages addObject:[UIImage imageFromCVMatrix:outputImageMatrix]];
	}

#pragma mark STEP 6 - Image clean-up based on related rectangles #1

	// Detect related contours based on the most related bounding rects filtering algorithm
	ContourVector contours;
	RectVector rects = [self extractContourBoundingRectsInMatrix:outputImageMatrix fromHeightRatio:1.0 toHeightRatio:
						0.15 method:kContourDetectionApproxSimple filledContoursOnly:NO contours:&contours];
	RectVector relatedRects = [self detectRelatedRects:rects
										  withMaxRatio:cv::Size2d(0.0, 0.25)
										   andMinRatio:cv::Size2d(0.0, 0.25)];

	// Compute average character height
	double characterHeight = 0.0;
	for (cv::Rect &rect: relatedRects) {
		characterHeight += (double) rect.height;
	}
	characterHeight /= (double) relatedRects.size();

	NSLog(@"character height: %.04f", characterHeight);

	// Leave only the contours and rects that are considered to be characters
	outputImageMatrix = [self maskedOutMatrix:outputImageMatrix onlyWithContours:contours];
	outputImageMatrix = [self maskedOutMatrix:outputImageMatrix onlyWithRects:relatedRects];

	tock(image_filtering, @"image filtering by related rects and character height: work done in ", @"s");

#pragma mark STEP 7 - Last digit wrapping rectangle removal

	tick(rectangle_removal);

	Quad lastDigitRect = [self detectLastDigitBoungindRect:outputImageMatrix
										   characterHeight:characterHeight
												 threshold:0.15
										withinBoundingRect:relatedRects
											 withAlgorithm:kLastDigitRectDetectionHoughLines
									  debugProcessingImage:includeProcessingStages ? outputImages : nil];

	// Mask out last digit rect using white color. The thickness is defined by the character height (7.5% from the
	// height)
	[self drawQuads:{lastDigitRect} onMatrix:outputImageMatrix thickness:
	 characterHeight * 0.075 color:cv::Scalar(255.0, 255.0, 255.0)];

	tock(rectangle_removal, @"rectangle detection done in ", @"s");
	tick(rectangle_removal_cleanup);

	// Extract last 30% of image width and apply closing morphologies on it in order to remove the remaining artifacts
	// from rectangle removal
	int subMatrixWidthX1 = outputImageMatrix.size().width * 0.30;
	int subMatrixWidthX2 = outputImageMatrix.size().width * 0.25;
	cv::Mat subImageMatrix;
	cv::Rect subMatrixRect = cv::Rect(outputImageMatrix.size().width - subMatrixWidthX1, 0.0, subMatrixWidthX1,
									  outputImageMatrix.size().height);
	cv::Mat(outputImageMatrix, subMatrixRect).copyTo(subImageMatrix);

	// Create and apply closing morphology operation with the kernel size 5.0% from the computed character height
	double cleanupKernelSize = MAX(1.0, characterHeight * 0.05);
	cv::Mat cleanupKernel = cv::getStructuringElement(cv::MORPH_RECT, cv::Size(cleanupKernelSize, cleanupKernelSize));
	cv::morphologyEx(subImageMatrix, subImageMatrix, cv::MORPH_CLOSE, cleanupKernel);

	// Shrink image one more time to remove artifacts from left side of the image (due to sliced characters or other
	// objects). Resize is made only for width from 30% of the original image to 25%
	cv::Mat cleanSubImageMatrix;
	cv::Mat(subImageMatrix, cv::Rect(subMatrixWidthX1 - subMatrixWidthX2, 0.0,
									 subMatrixWidthX2, subImageMatrix.size().height)).copyTo(cleanSubImageMatrix);

	// Create region of interest and copy the clean sub-image matrix
	cv::Rect roi = cv::Rect(cv::Point(outputImageMatrix.size().width - cleanSubImageMatrix.size().width, 0.0),
							cleanSubImageMatrix.size());
	cleanSubImageMatrix.copyTo(outputImageMatrix(roi));

	tock(rectangle_removal_cleanup, @"rectangle removal cleanup done in ", ([NSString stringWithFormat:@"s (%f)",
																			 cleanupKernelSize]));

#pragma mark STEP 8 - Image clean-up based on morphologies

	tick(image_morphology);

	cv::Mat scratchlessMaskMatrix;
	outputImageMatrix.copyTo(scratchlessMaskMatrix);

	// Create kernels for artifacts and defects removal from the image
	cv::Mat wideKernel = cv::getStructuringElement(cv::MORPH_ELLIPSE,
												   cv::Size(MAX(1.0, characterHeight * kImageCleanLargeKernelSide),
															MAX(1.0, characterHeight * kImageCleanSmallKernelSide)));
	cv::Mat tallKernel = cv::getStructuringElement(cv::MORPH_ELLIPSE,
												   cv::Size(MAX(1.0, characterHeight * kImageCleanSmallKernelSide),
															MAX(1.0, characterHeight * kImageCleanLargeKernelSide)));
	cv::Mat smallSquareKernel = cv::getStructuringElement(cv::MORPH_ELLIPSE,
														  cv::Size(MAX(1.0, characterHeight *
																	   kImageCleanSmallKernelSide),
																   MAX(1.0, characterHeight *
																	   kImageCleanSmallKernelSide)));
	cv::Mat mediumSquareKernel = cv::getStructuringElement(cv::MORPH_ELLIPSE,
														   cv::Size(MAX(1.0, characterHeight *
																		kImageCleanMediumKernelSide),
																	MAX(1.0, characterHeight *
																		kImageCleanMediumKernelSide)));
	// Apply morphology operations
	cv::morphologyEx(scratchlessMaskMatrix, scratchlessMaskMatrix, cv::MORPH_CLOSE, tallKernel);
	cv::morphologyEx(scratchlessMaskMatrix, scratchlessMaskMatrix, cv::MORPH_CLOSE, wideKernel);
	cv::morphologyEx(scratchlessMaskMatrix, scratchlessMaskMatrix, cv::MORPH_OPEN, mediumSquareKernel);

	// Combine scratchless mask with the output image
	cv::bitwise_or(scratchlessMaskMatrix, outputImageMatrix, outputImageMatrix);

	// One last morphology for image clean un
	cv::morphologyEx(outputImageMatrix, outputImageMatrix, cv::MORPH_OPEN, smallSquareKernel);

	tock(image_morphology, @"image morphology processing: work done in ", @"s");

#pragma mark STEP 9 - Image clean-up based on related rectangles #2

	tick(cleanup_after_rect_removal);

	cv::threshold(outputImageMatrix, outputImageMatrix, 128.0, 255.0, cv::THRESH_BINARY);

	rects = [self extractContourBoundingRectsInMatrix:outputImageMatrix fromHeightRatio:1.0 toHeightRatio:
			 0.15 method:kContourDetectionApproxSimple filledContoursOnly:NO contours:NULL];
	relatedRects = [self detectRelatedRects:rects
							   withMaxRatio:cv::Size2d(0.75, 0.25)
								andMinRatio:cv::Size2d(0.95, 0.25)];

	// Leave only the rects that are considered to be characters
	outputImageMatrix = [self maskedOutMatrix:outputImageMatrix onlyWithRects:relatedRects];

	tock(cleanup_after_rect_removal, @"cleanup after rect removal done in ", @"s");

#pragma mark STEP 10 - Add border to the image (this helps tesseract to better detect text area)
	
	// Add border to the image (this helps tesseract to better detect text area)
	int borderWidth = MAX(3.0, imageLargestSide * 0.05);
	cv::copyMakeBorder(outputImageMatrix, outputImageMatrix, borderWidth, borderWidth, borderWidth, borderWidth,
					   cv::BORDER_CONSTANT, cv::Scalar(255.0, 255.0, 255.0));

	// Add output image (this image will go to tesseract)
	[outputImages insertObject:[UIImage imageFromCVMatrix:outputImageMatrix] atIndex:0];

	// Return processed images
	return outputImages;
}

#pragma mark - Drawing methods

+ (cv::Mat)maskedOutMatrix:(cv::Mat)matrix onlyWithRects:(RectVector)rects
{
	return [self maskedOutMatrix:matrix onlyWithRects:rects fromWidthRatio:0.0 toWidthRatio:1.0];
}

+ (cv::Mat)maskedOutMatrix:(cv::Mat)matrix
			 onlyWithRects:(RectVector)rects
			fromWidthRatio:(double)from
			  toWidthRatio:(double)to
{
	return [self maskedOutMatrix:matrix onlyWithRects:rects fromWidthRatio:from toWidthRatio:to rectsScaleRatio:1.0];
}

+ (cv::Mat)maskedOutMatrix:(cv::Mat)matrix
			 onlyWithRects:(RectVector)rects
			fromWidthRatio:(double)from
			  toWidthRatio:(double)to
		   rectsScaleRatio:(double)scale
{
	// Swap values, if case
	if (from > to) {

		// Swap "from" and "to" variables
		double x = from;
		from = to;
		to = x;
	}

	cv::Mat newMatrix;
	cv::Mat maskMatrix = cv::Mat::ones(matrix.size(), CV_8UC1) * 255.0;

	// Fill rectangles with white color on the mask image
	for (cv::Rect &rect: rects) {
		if (ABS(from) > DBL_EPSILON && ABS(to) > DBL_EPSILON) {

			// Add rectangles to the mask based on X position in from-to range
			if (rect.x >= matrix.size().width * from && rect.x <= matrix.size().width * to) {
				cv::rectangle(maskMatrix, [self enlargedRect:rect withScaleRatio:scale], cv::Scalar(0.0), CV_FILLED);
			}
		} else {

			// Add all rectangles to the mask
			cv::rectangle(maskMatrix, [self enlargedRect:rect withScaleRatio:scale], cv::Scalar(0.0), CV_FILLED);
		}
	}

	// Apply bitwise or with the original image and mask
	cv::bitwise_or(matrix, maskMatrix, newMatrix);

	return newMatrix;
}

+ (cv::Mat)maskedOutMatrix:(cv::Mat)matrix onlyWithContours:(ContourVector)contours
{
	cv::Mat newMatrix;
	cv::Mat maskMatrix = cv::Mat::ones(matrix.size(), CV_8UC1) * 255.0;

	// Fill contours with white color on the mask image
	cv::drawContours(maskMatrix, contours, -1, cv::Scalar(0.0), CV_FILLED);

	// Apply bitwise or with the original image and mask
	cv::bitwise_or(matrix, maskMatrix, newMatrix);

	return newMatrix;
}

+ (void)drawBoundingRects:(RectVector)rects onMatrix:(cv::Mat)matrix
{
	for (cv::Rect &rect: rects) {

		// Draw a bounding rect on the image
		cv::rectangle(matrix, rect, cv::Scalar(255.0, 0.0, 0.0), 2);
	}
}

+ (void)drawSegments:(SegmentVector)segments onMatrix:(cv::Mat)matrix
{
	for (Segment &l: segments) {
		cv::line(matrix, cv::Point(l[0], l[1]), cv::Point(l[2], l[3]), cv::Scalar(0.0, 255.0, 0.0), 2, cv::LINE_AA);
	}
}

+ (void)drawQuads:(QuadVector)quads onMatrix:(cv::Mat)matrix
{
	[self drawQuads:quads onMatrix:matrix thickness:2 color:cv::Scalar(255.0, 0.0, 0.0)];
}

+ (void)drawQuads:(QuadVector)quads onMatrix:(cv::Mat)matrix thickness:(int)thickness color:(cv::Scalar)color
{
	for (Quad &q: quads) {
		cv::line(matrix, cv::Point(q[0], q[1]), cv::Point(q[2], q[3]), color, thickness, cv::LINE_AA);
		cv::line(matrix, cv::Point(q[2], q[3]), cv::Point(q[4], q[5]), color, thickness, cv::LINE_AA);
		cv::line(matrix, cv::Point(q[4], q[5]), cv::Point(q[6], q[7]), color, thickness, cv::LINE_AA);
		cv::line(matrix, cv::Point(q[6], q[7]), cv::Point(q[0], q[1]), color, thickness, cv::LINE_AA);
	}
}

#pragma mark - Shape detection and recognition

+ (RectVector)extractContourBoundingRectsInMatrix:(cv::Mat)inputMatrix
								  fromHeightRatio:(double)fromHeightRatio
									toHeightRatio:(double)toHeightRatio
										 contours:(ContourVector *)outputContours
{
	return [self extractContourBoundingRectsInMatrix:inputMatrix fromHeightRatio:fromHeightRatio toHeightRatio:
			toHeightRatio method:kContourDetectionApproxNone contours:outputContours];
}

+ (RectVector)extractContourBoundingRectsInMatrix:(cv::Mat)inputMatrix
								  fromHeightRatio:(double)fromHeightRatio
									toHeightRatio:(double)toHeightRatio
										   method:(ContourDetectionApprox)method
										 contours:(ContourVector *)outputContours
{
	return [self extractContourBoundingRectsInMatrix:inputMatrix fromHeightRatio:fromHeightRatio toHeightRatio:
			toHeightRatio method:method filledContoursOnly:NO contours:outputContours];
}

+ (RectVector)extractContourBoundingRectsInMatrix:(cv::Mat)inputMatrix
								  fromHeightRatio:(double)fromHeightRatio
									toHeightRatio:(double)toHeightRatio
										   method:(ContourDetectionApprox)method
							   filledContoursOnly:(BOOL)filledContoursOnly
										 contours:(ContourVector *)outputContours
{
	RectVector rects;
	std::vector<std::vector<cv::Point>> contours;
	int nonFilledContours = 0;

	// Duplicate the matrix in order to preserve original image (find contours destroys the input image)
	cv::Mat matrix;
	inputMatrix.copyTo(matrix);
	cv::bitwise_not(matrix, matrix);

	// Find contours
	cv::findContours(matrix, contours, cv::RETR_LIST, method);

	// Filter only rectangles that are between min and max image boundaries
	for (int idx = 0; idx < contours.size(); idx++) {

		// Get bounding rect, upper and lower limits of the rect
		cv::Rect rect = cv::boundingRect(contours[idx]);
		double upperLimit = fromHeightRatio * matrix.size().height;
		double lowerLimit = toHeightRatio * matrix.size().height;

		if (rect.height <= upperLimit && rect.height >= lowerLimit) {

			if (filledContoursOnly) {

				// Crop the bounding rect of the contour from original image
				cv::Mat cropppedImage;
				inputMatrix(rect).copyTo(cropppedImage);

				// Create the mask by filling the contour with white
				cv::Mat mask = cv::Mat::zeros(cropppedImage.rows, cropppedImage.cols, CV_8UC1);
				cv::drawContours(mask, contours, idx, cv::Scalar(255.0), CV_FILLED, cv::LINE_8, cv::noArray(), INT_MAX,
								 cv::Point(-rect.x, - rect.y));
				cv::erode(mask, mask, cv::getStructuringElement(cv::MORPH_ELLIPSE, cv::Size(3.0, 3.0)));

				// Copy the image using the mask
				cv::Mat contourImg = cv::Mat::ones(cropppedImage.rows, cropppedImage.cols, CV_8UC1) * 255.0;
				cropppedImage.copyTo(contourImg, mask);

				// Compute contour area and fill area of the contour
				double contourArea = cv::contourArea(contours[idx]);
				double fillArea = (double) cropppedImage.total() - cv::countNonZero(contourImg);

				if (fillArea > contourArea * 0.25) {

					// Add rect to the array
					rects.push_back(rect);

					// Add contour to the output array
					if (outputContours != nil) {
						(*outputContours).push_back(contours[idx]);
					}
				} else {
					nonFilledContours += 1;
				}
			} else {

				// Add rect to the array
				rects.push_back(rect);

				// Add contour to the output array
				if (outputContours != nil) {
					(*outputContours).push_back(contours[idx]);
				}
			}
		}
	}
	if (filledContoursOnly) {
		NSLog(@"non filed contours count: %d", nonFilledContours);
	}
	return rects;
}

+ (RectVector)detectRelatedRects:(RectVector)rects
			  withMaxRatio:(cv::Size2d)maxRatio
			   andMinRatio:(cv::Size2d)minRatio
{
	RelatedRects mostRelatedRects = RelatedRects(cv::Rect(), RectVector());

	// Create a vector with original rects and a list of rects related to a specific rect
	RelatedRectsVector relatedRects = RelatedRectsVector();
	for (cv::Rect &rect: rects) {
		relatedRects.push_back(RelatedRects(rect, RectVector()));
	}

	// Detect which sides should be compared
	BOOL compareWidth = ABS(maxRatio.width) > DBL_EPSILON || ABS(minRatio.width) > DBL_EPSILON;
	BOOL compareHeight = ABS(maxRatio.height) > DBL_EPSILON || ABS(minRatio.height) > DBL_EPSILON;

	// Group rects related to each other
	for (RelatedRects &rr: relatedRects) {

		// Off-topic: Declaring variables inside loops, if-conditions or switches in C/C++
		// http://stackoverflow.com/questions/982963

		cv::Size2d currentRectSize = rr.first.size();
		cv::Size2d upperLimit = cv::Size2d(currentRectSize.width + (maxRatio.width * currentRectSize.width),
										   currentRectSize.height + (maxRatio.height * currentRectSize.height));
		cv::Size2d lowerLimit = cv::Size2d(currentRectSize.width - (minRatio.width * currentRectSize.width),
										   currentRectSize.height - (minRatio.height * currentRectSize.height));

		for (cv::Rect &rect: rects) {

			// If this rect is related to current rect (has a height and width between lower and upper limit), add it to
			// the list
			if ((compareWidth ? (rect.width < upperLimit.width && rect.width > lowerLimit.width) : YES) &&
				(compareHeight ? (rect.height < upperLimit.height && rect.height > lowerLimit.height) : YES)) {
				rr.second.push_back(rect);
			}
		}

		// Get the most related rect (the one that has most rects associated to it)
		if (rr.second.size() > mostRelatedRects.second.size()) {
			mostRelatedRects = rr;
		}
	}
	
	// Return most related rects
	return mostRelatedRects.second;
}

+ (SegmentVector)extractSegmentsInImage:(cv::Mat)image minSegmentLength:(double)minLength segmentScale:(double)scale
				   debugProcessingImage:(NSMutableArray<UIImage *> *)debugArray
{
	SegmentVector segments;

	// Sanity checks
	if (image.empty()) {
		return segments;
	}

	// If image is RGB, convert it to grayscale color space
	if (image.type() != CV_8UC1) {

		NSLog(@"convert image from RGB to grayscale for segment extraction");
		cv::cvtColor(image, image, CV_RGB2GRAY);
	}

	// Apply canny edge algorithm for image and dilate the margins to 3 pixels thick
	cv::Mat kernel = cv::getStructuringElement(cv::MORPH_ELLIPSE, cv::Size(3.0, 3.0));
	cv::Canny(image, image, 50, 150, 3);
	cv::dilate(image, image, kernel);
	cv::GaussianBlur(image, image, cv::Size(3.0, 3.0), 0.0);

	if (debugArray) {
		[debugArray addObject:[UIImage imageFromCVMatrix:image]];
	}

	// Detect segments with hough lines
	cv::HoughLinesP(image, segments, minLength * 0.025, M_PI / 180.0, 160, minLength, minLength * 0.2);

	// Scale the segments if case
	if (ABS(scale - 1.0) > DBL_EPSILON) {
		for (Segment &segment: segments) {
			segment = [self scaleSegment:segment factor:scale];
		}
	}

	return segments;
}

+ (QuadVector)generateAllQuadsFromSegments:(SegmentVector)segments intersectionThreshold:(double)threshold
{
	// Sanity checks
	if (segments.size() <= 0) {
		return QuadVector();
	}

	QuadVector quads = QuadVector();
	std::vector<std::vector<std::pair<int, cv::Point2d>>> intersectedSegments;

	// Create arrays that will contain pairs of intersected segments
	for (int i = 0; i < segments.size() - 1; i++) {
		intersectedSegments.push_back(std::vector<std::pair<int, cv::Point2d>>());
	}

	// Add all the segments that intersect in the hash table
	for (int i = 0; i < segments.size() - 1; i++) {
		for (int j = i + 1; j < segments.size(); j++) {

			// Get intersection point between these two segments (if one exists)
			cv::Point2d intersectionPoint = [self intersectionOfSegment:segments[i] andSegment:segments[j]
														   maxThreshold:threshold];

			// If the segments intersect, add them to the hash table
			if (intersectionPoint.x != -1 && intersectionPoint.y != -1) {
				intersectedSegments[i].push_back(std::pair<int, cv::Point2d>(j, intersectionPoint));
			}
		}
	}

	// If no intersected segments were found, return from the function
	if (intersectedSegments.size() == 0) {
		return quads;
	}

	// Generate quad vector from 2 pairs of segments that will intersect with each other, forming a quad
	for (int i = 0; i < intersectedSegments.size() - 1; i++) {
		for (int j = i + 1; j < intersectedSegments.size(); j++) {

			// Loop through all intersected segments in the compared vectors
			for (int x = 0; x < intersectedSegments[i].size(); x++) {
				for (int y = 0; y < intersectedSegments[j].size(); y++) {

					// Segments indices (A intersects B and C intersectes D):
					// a) i
					// b) intersectedSegments[i][x].first
					// c) j
					// d) intersectedSegments[j][y].first
					int a = i;
					int b = intersectedSegments[i][x].first;
					int c = j;
					int d = intersectedSegments[j][y].first;

					// Intersection points:
					// p1) intersection between A and B
					// p3) intersection between C and D
					cv::Point2d p1 = intersectedSegments[i][x].second;
					cv::Point2d p3 = intersectedSegments[j][y].second;

					// Check that segments are unique
					if (a != d && a != c && b != c && b != d) {

						// Check the intersection between two segments from different couples, A & C
						cv::Point2d p2 = [self intersectionOfSegment:segments[a] andSegment:segments[c]
														maxThreshold:threshold];

						if (p2.x != -1 && p2.y != -1) {

							// Segments A and C intersect, check if segments B & D intersect
							cv::Point2d p4 = [self intersectionOfSegment:segments[b] andSegment:segments[d]
															maxThreshold:threshold];

							if (p4.x != -1 && p4.y != -1) {

								// Segments B and D intersect, a quad was found - add it to the list
								quads.push_back({(int) p1.x, (int) p1.y, (int) p2.x, (int) p2.y,
									(int) p3.x, (int) p3.y, (int) p4.x, (int) p4.y});
								continue;
							}
						}

						// Segments A & C don't intersect, check intersections between other two segments, A & D
						p2 = [self intersectionOfSegment:segments[a] andSegment:segments[d] maxThreshold:threshold];

						if (p2.x != -1 && p2.y != -1) {

							// Segments A and D intersect, check if segments B & C intersect
							cv::Point2d p4 = [self intersectionOfSegment:segments[b] andSegment:segments[c]
															maxThreshold:threshold];

							if (p4.x != -1 && p4.y != -1) {

								// Segments B and C intersect, a quad was found - add it to the list
								quads.push_back({(int) p1.x, (int) p1.y, (int) p2.x, (int) p2.y,
									(int) p3.x, (int) p3.y, (int) p4.x, (int) p4.y});
								continue;
							}
						}
					}
				}
			}
		}
	}
	return quads;
}

+ (QuadVector)filterQauds:(QuadVector)quads
		 inWidthRangeFrom:(double)widthFrom
					   to:(double)widthTo
		inHeightRangeFrom:(double)heightFrom
					   to:(double)heightTo
	ignoreTrapezoidShapes:(BOOL)ignoreTrapezoids
	  ignoreConcaveShapes:(BOOL)ignoreConcaves
{
	// Sanity checks
	if (ABS(widthFrom - widthTo) < DBL_EPSILON || ABS(heightFrom - heightTo) < DBL_EPSILON) {
		return QuadVector();
	}

	// Swap width values, if case
	if (widthFrom > widthTo) {

		// Swap width "from" and "to" variables
		double x = widthFrom;
		widthFrom = widthTo;
		widthTo = x;
	}

	// Swap height values, if case
	if (heightFrom > heightTo) {

		// Swap width "from" and "to" variables
		double x = heightFrom;
		heightFrom = heightTo;
		heightTo = x;
	}

	// Statistics variables
	int concaveShapesIgnored = 0;
	int trapezoidShapesIgnored = 0;

	// Create the array to store filtered quads
	QuadVector filteredQuads = QuadVector();

	for (Quad &q: quads) {
		double lengthSide1 = 0.0,
		lengthSide2 = 0.0,
		lengthSide3 = 0.0,
		lengthSide4 = 0.0;

		// Compute quad's width and height
		cv::Size2d size = [self sizeOfQuad:q];

		// Filter the quad by comparing width and height values
		if (size.width < widthFrom || size.width > widthTo || size.height < heightFrom || size.height > heightTo) {
			continue;
		}

		// Initialize quad side length variables
		if (ignoreTrapezoids || ignoreConcaves) {
			lengthSide1 = [self segmentLength:{q[0], q[1], q[2], q[3]}];
			lengthSide2 = [self segmentLength:{q[2], q[3], q[4], q[5]}];
			lengthSide3 = [self segmentLength:{q[4], q[5], q[6], q[7]}];
			lengthSide4 = [self segmentLength:{q[6], q[7], q[0], q[1]}];
		}

		// Check for trapezoid shapes
		if (ignoreTrapezoids) {

			const double minThreshold = 0.85;

			// Check if side ratios are not smaller than minimum threshold
			if (MIN(lengthSide1, lengthSide3) / MAX(lengthSide1, lengthSide3) < minThreshold ||
				MIN(lengthSide2, lengthSide4) / MAX(lengthSide2, lengthSide4) < minThreshold) {
				trapezoidShapesIgnored += 1;
				continue;
			}
		}

		// Check for concave shapes
		if (ignoreConcaves) {

			double lengthDiagonal = [self segmentLength:{q[0], q[1], q[4], q[5]}];

			// If diagonal length is smaller than on of the sides, quad is concave
			if (lengthDiagonal < lengthSide1 || lengthDiagonal < lengthSide4) {
				concaveShapesIgnored += 1;
				continue;
			}
		}

		// If the quad has passed all the validations, add it into the filtered list
		filteredQuads.push_back(q);
	}

	NSLog(@"filter quads - trapezoid shapes ignored: %d", trapezoidShapesIgnored);
	NSLog(@"filter quads - concave shapes ignored: %d", concaveShapesIgnored);

	return filteredQuads;
}

+ (Quad)computeLastDigitBoungindRectFromQuads:(QuadVector)quads
							  characterHeight:(double)characterHeight
									threshold:(double)threshold
{
	// Sanity check
	if (quads.size() == 0) {
		return Quad();
	}

	// Get min and max width and height of the quads from the filtered quad array
	cv::Size minQuad(INT_MAX, INT_MAX);
	cv::Size maxQuad(INT_MIN, INT_MIN);

	for (Quad &q: quads) {
		cv::Size2d size = [self sizeOfQuad:q];

		// Compute quad's min and max width and height
		minQuad.width = MIN(minQuad.width, size.width);
		minQuad.height = MIN(minQuad.height, size.height);
		maxQuad.width = MAX(maxQuad.width, size.width);
		maxQuad.height = MAX(maxQuad.height, size.height);
	}

#ifdef kComputeBoundingRectSizeInMinMaxRange

	// Compute average bounding rect
	cv::Size boundingRectSize = cv::Size();
	boundingRectSize.height = MIN(MAX(characterHeight * kLastDigitBoundingRectHeightCharacterRatio,
									  minQuad.height), maxQuad.height);
	boundingRectSize.width = boundingRectSize.height * kLastDigitBoundingRectAverageRectWidthRatio;

#else

	cv::Size boundingRectSize = cv::Size(characterHeight * kLastDigitBoundingRectHeightCharacterRatio *
										 kLastDigitBoundingRectAverageRectWidthRatio,
										 characterHeight * kLastDigitBoundingRectHeightCharacterRatio);
#endif

	NSLog(@"average bounding rect size: {%d, %d}", boundingRectSize.width, boundingRectSize.height);

	// Filter only the quads that are larger than 80% of bounding's rect size width using a λ lambda function
	QuadVector filteredQuads;
	std::copy_if(quads.begin(), quads.end(), std::back_inserter(filteredQuads), [&] (const Quad &q) -> bool {
		cv::Size2d size = [ImageProcessor sizeOfQuad:q];
		return size.width >= boundingRectSize.width * 0.8;
	});

	// Sanity check
	if (filteredQuads.size() == 0) {
		return Quad();
	}

	NSLog(@"original vs. filtered count: %lu vs. %lu", quads.size(), filteredQuads.size());
	NSLog(@"min vs. max width: %d vs. %d", minQuad.width, maxQuad.width);
	NSLog(@"min vs. max height: %d vs. %d", minQuad.height, maxQuad.height);

	// Compute min and max threshold on Y axis (height)
	double minThresholdY = boundingRectSize.height - boundingRectSize.height * (threshold / 2.0);
	double maxThresholdY = boundingRectSize.height + boundingRectSize.height * (threshold / 2.0);

	// Sort the vector by applying weight to quads based on:
	// 1. The widest quad will have a weight equal to 1 on X axis, the narrowest will have a weight equal to 0
	// 2. The quad with the height most close to bounding rect height (with a certain threshold) will have a weight
	//    equal to 1 on Y axis, the quad with the furthest height from bounding rect height will have a weight equal
	//    to 0
	// 3. Weight both on X and Y are multiplied resulting in the final weight [0,0...1,0]

	// λ lambda function to compute the weight
	auto weightFunc = [&] (const Quad &quad) -> double {
		cv::Size2d size = [ImageProcessor sizeOfQuad:quad];

		// Compute weight on X
		double weightX = (size.width - minQuad.width) / (maxQuad.width - minQuad.width);

		// Compute weight on Y
		double weightY = ((size.height >= minThresholdY && size.height <= maxThresholdY) ? 1.0 :
						  (size.height < minThresholdY ?
						   (size.height - minQuad.height) / (minThresholdY - minQuad.height) :
						   1.0 - (size.height - maxThresholdY) / (maxQuad.height - maxThresholdY)));

		// Return multiplied X and Y weights
		return weightX * weightY;
	};

	// Compute weight for every quad
	std::vector<std::pair<Quad, double>> weightedQuads;
	for (Quad &q: filteredQuads) {
		weightedQuads.push_back(std::pair<Quad, double>(q, weightFunc(q)));
	}

	// Sort the array based on weight
	std::sort(weightedQuads.begin(), weightedQuads.end(), [&] (const std::pair<Quad, double> &wq1,
															   const std::pair<Quad, double> &wq2) -> bool {
		return wq1.second > wq2.second;
	});

#ifdef kComputeQuadByAveragingFilteredQuads

	// Get only the quads that have a weight more than 0.5
	filteredQuads.clear();
	for (const std::pair<Quad, double> &wq: weightedQuads) {
		if (wq.second > 0.5) {
			filteredQuads.push_back([self fixQuadVertexOrder:wq.first]);
		}
	}

	// Compute average quad from filtered quads
	Quad averageQuad = {0, 0, 0, 0, 0, 0, 0, 0};
	for (int i = 0; i < filteredQuads.size(); i++) {
		for (int j = 0; j < 8; j++) {
			averageQuad[j] = averageQuad[j] + (double) (filteredQuads[i][j] - averageQuad[j]) / (i + 1.0);
		}
	}
	return averageQuad;

#else

	// Return only if the quad with the maximum weight is more than 0.5 units
	if (weightedQuads.size() > 0 && weightedQuads.front().second > 0.5) {
		return weightedQuads.front().first;
	}
	return Quad();
#endif
}

+ (Quad)detectLastDigitBoungindRect:(cv::Mat)imageMatrix
					characterHeight:(double)characterHeight
						  threshold:(double)threshold
				 withinBoundingRect:(RectVector)rects
					  withAlgorithm:(LastDigitRectDetection)algorithm
			   debugProcessingImage:(NSMutableArray<UIImage *> *)debugArray
{
	// Sanity checks
	if (imageMatrix.empty()) {
		return Quad();
	}

	// Start rectangle detection process (for removal of the last character wrapping square)
	cv::Mat quadExtractionImageMatrix;

	if (!rects.empty()) {

		// Mask out first 70% of the contours from the image. The quad is always in the last 30% of the image.
		quadExtractionImageMatrix = [self maskedOutMatrix:imageMatrix onlyWithRects:rects
										   fromWidthRatio:0.7 toWidthRatio:1.0 rectsScaleRatio:1.3];
	}

	if (quadExtractionImageMatrix.empty()) {
		return Quad();
	}

	// If image is RGB, convert it to grayscale color space
	if (quadExtractionImageMatrix.type() != CV_8UC1) {

		NSLog(@"convert image from RGB to grayscale for segment extraction");
		cv::cvtColor(quadExtractionImageMatrix, quadExtractionImageMatrix, CV_RGB2GRAY);
	}

	if (debugArray) {
		[debugArray addObject:[UIImage imageFromCVMatrix:quadExtractionImageMatrix]];
	}

	// Based on selected algorithm, detect last digit bounding quads
	QuadVector quads;
	switch (algorithm) {
		case kLastDigitRectDetectionHoughLines: {

			// Extract all the segments in the image (Hough lines algorithm)
			SegmentVector segments = [self extractSegmentsInImage:quadExtractionImageMatrix
												 minSegmentLength:characterHeight * 0.5
													 segmentScale:1.0
											 debugProcessingImage:debugArray];

			NSLog(@"segments extracted: %lu", segments.size());

			// Generate all possible combination of segments that form a quad
			quads = [self generateAllQuadsFromSegments:segments intersectionThreshold:characterHeight * 0.2];

			// Filter the quads based on character height (width is between 50% and 120% of character height and width
			// is between 80% and 140% of character height)
			quads = [self filterQauds:quads
					 inWidthRangeFrom:characterHeight * 0.5
								   to:characterHeight * 1.2
					inHeightRangeFrom:characterHeight * 0.8
								   to:characterHeight * 1.4
				ignoreTrapezoidShapes:YES
				  ignoreConcaveShapes:YES];

			NSLog(@"quads: %lu", quads.size());
			break;
		}
		case kLastDigitRectDetectionDouglasPeucker: {

			// Apply canny edge algorithm for image and apply closing morphology to close small gaps
			cv::Canny(quadExtractionImageMatrix, quadExtractionImageMatrix, 50, 150, 3);
			cv::Mat closingKernel = cv::getStructuringElement(cv::MORPH_ELLIPSE,
															  cv::Size(MAX(1.0, characterHeight * 0.02),
																	   MAX(1.0, characterHeight * 0.02)));
			cv::morphologyEx(quadExtractionImageMatrix, quadExtractionImageMatrix, cv::MORPH_CLOSE, closingKernel);

			// Find all the contours in the image
			std::vector<std::vector<cv::Point>> contours;
			cv::findContours(quadExtractionImageMatrix, contours, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE);

			// Based on contours, find the ones that match quad objects
			for (int i = 0; i < contours.size(); i++) {
				std::vector<cv::Point> polygon;

				// Use the Douglas-Peucker algorithm to approximate a polygonal curve with the specified precision
				cv::approxPolyDP(contours[i], polygon, cv::arcLength(contours[i], true) * 0.1, true);

				// If the polygon has 4 vertices, it's a quad
				if (polygon.size() == 4) {

					quads.push_back(Quad(polygon[0].x, polygon[0].y,
										 polygon[1].x, polygon[1].y,
										 polygon[2].x, polygon[2].y,
										 polygon[3].x, polygon[3].y));
				}
			}

			NSLog(@"quads: %lu", quads.size());
			break;
		}
		default: {
			break;
		}
	}

	// Compute last digit bounding rect from the extracted quad
	Quad quad = [self computeLastDigitBoungindRectFromQuads:quads characterHeight:characterHeight threshold:threshold];
	return quad;
}

#pragma mark - Image transformations

+ (cv::Mat)fixImage:(cv::Mat)image withPerspectiveAngle:(float)perspectiveAngle andTiltAngle:(float)tiltAngle
{
	if (ABS(perspectiveAngle) <= DBL_EPSILON && ABS(tiltAngle) <= DBL_EPSILON) {

		NSLog(@"no changes should be done for perspective and tilt correction");
		return image;
	}

	// Rotation matrix on X-axis for creating a perspective rotation effect
	cv::Matx44f xRotationMatrix = cv::Matx44f(1.0f,		0.0f,					0.0f,						0.0f,
											  0.0f,		cosf(perspectiveAngle),	-sinf(perspectiveAngle),	0.0f,
											  0.0f,		sinf(perspectiveAngle),	cosf(perspectiveAngle),		0.0f,
											  0.0f,		0.0f,					0.0f,						1.0f);

	// Rotation matrix on Z-axis for creating a tilt rotation effect
	cv::Matx44f zRotationMatrix = cv::Matx44f(cosf(tiltAngle),	-sinf(tiltAngle),	0.0f,	0.0f,
											  sinf(tiltAngle),	cosf(tiltAngle),	0.0f,	0.0f,
											  0.0f,				0.0f,				1.0f,	0.0f,
											  0.0f,				0.0f,				0.0f,	1.0f);

	// Compute transform matrix by multiplying X and Z rotation matrices
	cv::Matx44f transformMatrix = ((ABS(perspectiveAngle) > DBL_EPSILON ? xRotationMatrix : cv::Matx44f::eye()) *
								   (ABS(tiltAngle) > DBL_EPSILON ? zRotationMatrix : cv::Matx44f::eye()));

	return [self perspectiveTransformImage:image withTransformMatrix:transformMatrix];
}

+ (cv::Mat)perspectiveTransformImage:(cv::Mat)image withTransformMatrix:(cv::Matx44f)matrix
{
	cv::Mat outputImage;

	// Get width and height and the maximum value from them, for ease of use in matrices
	float w = (float) image.cols;
	float h = (float) image.rows;
	float x = MAX(w, h);

	// Declare original, and perspective corrected, images bounding boxes
	std::vector<cv::Point2f> imageBoundingBox = {{0.0, 0.0}, {0.0, h}, {w, 0.0}, {w, h}};
	std::vector<cv::Point2f> newImageBoundingBox;

	// Projection conversion matrix - transforms image coordinates from 2D to 3D
	cv::Matx43f conversionMatrix = cv::Matx43f(1.0f, 0.0f, -w / 2.0f,
											   0.0f, 1.0f, -h / 2.0f,
											   0.0f, 0.0f, 0.0f,
											   0.0f, 0.0f, 1.0f);

	// Translation matrix to set distance of the view (camera) to the image - Z-axis
	cv::Matx44f translationMatrix = cv::Matx44f(1.0f, 0.0f, 0.0f, 0.0f,
												0.0f, 1.0f, 0.0f, 0.0f,
												0.0f, 0.0f, 1.0f, x,
												0.0f, 0.0f, 0.0f, 1.0f);

	// Projection conversion matrix - transforms image coordinates from 3D to 2D
	cv::Matx34f projectionMatrix = cv::Matx34f(x,		0.0f,	w / 2.0f,	0.0f,
											   0.0f,	x,		h / 2.0f,	0.0f,
											   0.0f,	0.0f,	1.0f,		0.0f);

	// Merge all operations in one transform matrix and compute perspective transformation matrix
	cv::Matx33f transformMatrix = projectionMatrix * (translationMatrix * (matrix * conversionMatrix));
	cv::perspectiveTransform(imageBoundingBox, newImageBoundingBox, transformMatrix.inv());

	// Get minimum and maximum X and Y values in order to compute new image bounding box width and height
	float minX = newImageBoundingBox[0].x;
	float maxX = newImageBoundingBox[0].x;
	float minY = newImageBoundingBox[0].y;
	float maxY = newImageBoundingBox[0].y;

	for (int i = 0; i < newImageBoundingBox.size(); i++) {
		minX = MIN(minX, newImageBoundingBox[i].x);
		maxX = MAX(maxX, newImageBoundingBox[i].x);
		minY = MIN(minY, newImageBoundingBox[i].y);
		maxY = MAX(maxY, newImageBoundingBox[i].y);
	}

	// Compute new image bounding box width and height
	float newWidth = maxX - minX;
	float newHeight = maxY - minY;
	cv::Size newSize = cv::Size(newWidth, newHeight);

	// Set new perspective values of the rescaled image
	cv::Point2f offset = {minX , minY};
	newImageBoundingBox = {newImageBoundingBox[0] - offset,
		newImageBoundingBox[1] - offset,
		newImageBoundingBox[2] - offset,
		newImageBoundingBox[3] - offset};

	cv::Mat perspectiveTransform = cv::getPerspectiveTransform(imageBoundingBox, newImageBoundingBox);
	cv::warpPerspective(image, outputImage, perspectiveTransform, newSize, cv::INTER_LINEAR, cv::BORDER_REPLICATE);
	return outputImage;
}

#pragma mark - Helper methods

+ (double)nearestOddNumber:(double)number
{
	if ((int) ceilf(number) % 2 == 1) {
		return ceilf(number);
	}
	if ((int) floorf(number) % 2 == 1) {
		return floorf(number);
	}
	if (ABS(floorf(number) - number) < ABS(ceilf(number) - number)) {
		return floorf(number) - 1.0;
	} else {
		return ceilf(number) + 1.0;
	}
}

+ (cv::Mat)redraftImage:(cv::Mat)image toWhiteBackgrounds:(BOOL)white
{
	return [self redraftImage:image toWhiteBackgrounds:white changes:NULL];
}

+ (cv::Mat)redraftImage:(cv::Mat)image toWhiteBackgrounds:(BOOL)white changes:(BOOL *)changes
{
	cv::Size size = image.size();
	cv::Mat negativeImage;

	// Negate image bits for comparing which one of them has more white pixels
	cv::bitwise_not(image, negativeImage);

	// Binarize the images for comparing black and white bits
	cv::Mat i1, i2;
	cv::threshold(image, i1, 128.0, 255.0, cv::THRESH_BINARY | cv::THRESH_OTSU);
	cv::threshold(negativeImage, i2, 128.0, 255.0, cv::THRESH_BINARY | cv::THRESH_OTSU);

	// Ratio of non-zero pixels in the binarized images
	double whiteRatio1 = (double) countNonZero(i1) / (size.width * size.height);
	double whiteRatio2 = (double) countNonZero(i2) / (size.width * size.height);

	NSLog(@"white ratio in the image: %0.3f vs. %0.3f", whiteRatio1, whiteRatio2);

	// Compare if white ratios are valid (at least one of the should be over 0.65)
	if (whiteRatio1 < 0.90 && whiteRatio2 < 0.90 && (whiteRatio1 >= 0.65 || whiteRatio2 >= 0.65)) {

		// Valid white ratio, return the results
		*changes = (whiteRatio1 > whiteRatio2 ? !white : white);
		return (whiteRatio1 > whiteRatio2 ? (white ? image : negativeImage) : (white ? negativeImage : image));
	}

	// White ratios were not high enough - apply adaptive thresholding on images and choose the one with the highest
	// white ratio
	int imageLargestSide = (size.width > size.height ? size.width : size.height);
	int blurValue = MAX(3, (int) [self nearestOddNumber:(double) imageLargestSide * 0.01]);
	int thresholdValue = MAX(3, (int) [self nearestOddNumber:(double) imageLargestSide * 0.03]);

	// Blur a little the images reduce noise
	cv::GaussianBlur(image, i1, cv::Size(blurValue, blurValue), 0.0);
	cv::GaussianBlur(negativeImage, i2, cv::Size(blurValue, blurValue), 0.0);

	cv::adaptiveThreshold(i1, i1, 255.0, cv::ADAPTIVE_THRESH_MEAN_C, cv::THRESH_BINARY, thresholdValue,
						  thresholdValue * 0.05);
	cv::adaptiveThreshold(i2, i2, 255.0, cv::ADAPTIVE_THRESH_MEAN_C, cv::THRESH_BINARY, thresholdValue,
						  thresholdValue * 0.05);

	// Ratio of non-zero pixels in the binarized images
	whiteRatio1 = (double) countNonZero(i1) / (size.width * size.height);
	whiteRatio2 = (double) countNonZero(i2) / (size.width * size.height);

	NSLog(@"white ratio in the image (after adaptive thresholding): %0.3f vs. %0.3f", whiteRatio1, whiteRatio2);

	// Return the results
	*changes = (whiteRatio1 > whiteRatio2 ? !white : white);
	return (whiteRatio1 > whiteRatio2 ? (white ? image : negativeImage) : (white ? negativeImage : image));
}

+ (cv::Mat)quantizedMatrix:(cv::Mat)matrix clusterCount:(int)k
{
	int size = matrix.rows * matrix.cols;

	// Split image matrix RGB channels
	std::vector<cv::Mat> rgbChannels;
	cv::split(matrix, rgbChannels);

	// Flatten the matrix - single row matrix
	cv::Mat flatMatrix(size, 3, CV_8U);

	for (int i = 0; i != 3; i++) {
		rgbChannels[i].reshape(1, size).copyTo(flatMatrix.col(i));
	}

	// Convert flat image from single channel to 4 channels -  RGB channels and alpha
	flatMatrix.convertTo(flatMatrix, CV_32F);

	// Apply K-means algorithm
	cv::Mat clusters;
	cv::kmeans(flatMatrix, k, clusters, cv::TermCriteria(CV_TERMCRIT_ITER | CV_TERMCRIT_EPS, 10000, 0.0001), 5,
			   cv::KMEANS_PP_CENTERS);

	// Reshape matrix from flat to 2D matrix
	clusters = clusters.reshape(0, matrix.rows);

	// Convert color scale of the clusters (from [0..k] to [0..255])
	cv::convertScaleAbs(clusters, clusters, 255.0 / k);

	return clusters;
}

+ (cv::Rect)enlargedRect:(cv::Rect)rect withScaleRatio:(double)scale
{
	// Sanity checks
	if (ABS(scale - 1.0) < DBL_EPSILON) {
		return rect;
	}

	cv::Rect newRect = cv::Rect(rect.x, rect.y, rect.width * scale, rect.height * scale);
	newRect.x -= (newRect.width - rect.width) / 2.0;
	newRect.y -= (newRect.height - rect.height) / 2.0;
	return newRect;
}

+ (cv::Size)sizeOfQuad:(Quad)q
{
	// Compute min and max X and Y
	cv::Point2d min = cv::Point2d(MIN(q[0], MIN(q[2], MIN(q[4], q[6]))),
								  MIN(q[1], MIN(q[3], MIN(q[5], q[7]))));
	cv::Point2d max = cv::Point2d(MAX(q[0], MAX(q[2], MAX(q[4], q[6]))),
								  MAX(q[1], MAX(q[3], MAX(q[5], q[7]))));

	// Compute and return width and height (by applying quad bounding rect)
	return cv::Size2d(max.x - min.x, max.y - min.y);
}

+ (cv::Point2d)intersectionOfSegment:(Segment)s1 andSegment:(Segment)s2
{
	return [self intersectionOfSegment:s1 andSegment:s2 maxThreshold:0.0];
}

+ (cv::Point2d)intersectionOfSegment:(Segment)s1 andSegment:(Segment)s2 maxThreshold:(double)threshold
{
	cv::Point2d intersectionPoint;
	int x1 = s1[0], y1 = s1[1], x2 = s1[2], y2 = s1[3];
	int x3 = s2[0], y3 = s2[1], x4 = s2[2], y4 = s2[3];
	double dist = ((x1 - x2) * (y3 - y4)) - ((y1 - y2) * (x3 - x4));

	// Detect if segments collide with each other by computing the distance
	if (ABS(dist) > DBL_EPSILON) {

		// Compute intersection point of the segments (considering an infinite length, i. e. lines)
		intersectionPoint.x = (double) ((x1 * y2 - y1 * x2) * (x3 - x4) - (x1 - x2) * (x3 * y4 - y3 * x4)) / dist;
		intersectionPoint.y = (double) ((x1 * y2 - y1 * x2) * (y3 - y4) - (y1 - y2) * (x3 * y4 - y3 * x4)) / dist;

		// Point of intersection can be off by at most with a certain amount of pixels (threshold value)
		if (intersectionPoint.x < MIN(x1, x2) - threshold || intersectionPoint.x > MAX(x1, x2) + threshold ||
			intersectionPoint.y < MIN(y1, y2) - threshold || intersectionPoint.y > MAX(y1, y2) + threshold) {

			// Point of intersection is neither on first nor on second segment considering the threshold
			return cv::Point2d(-1.0, -1.0);
		}
		if (intersectionPoint.x < MIN(x3, x4) - threshold || intersectionPoint.x > MAX(x3, x4) + threshold ||
			intersectionPoint.y < MIN(y3, y4) - threshold || intersectionPoint.y > MAX(y3, y4) + threshold) {

			// Point of intersection is neither on first nor on second segment considering the threshold
			return cv::Point2d(-1.0, -1.0);
		}
		return intersectionPoint;
	}

	// Segments don't collide
	return cv::Point2d(-1.0, -1.0);
}

+ (cv::Point2d)intersectionOfLine:(Line)l1 andLine:(Line)l2
{
	// Line representation: Ax + By = X
	// Get A, B, C of the first line - l1
	double a1 = l1[3] - l1[1];
	double b1 = l1[0] - l1[2];
	double c1 = a1 * l1[0] + b1 * l1[1];

	// Get A, B, C of the second line - l2
	double a2 = l2[3] - l2[1];
	double b2 = l2[0] - l2[2];
	double c2 = a2 * l2[0] + b2 * l2[1];

	// Get delta and check if the lines are parallel
	double delta = a1 * b2 - a2 * b1;

	if (ABS(delta) < DBL_EPSILON) {
		return cv::Point2d();
	}

	// Return the intersection point of lines 1 and 2
	return cv::Point2d((b2 * c1 - b1 * c2) / delta,
					   (a1 * c2 - a2 * c1) / delta);
}

+ (double)segmentLength:(Segment)segment
{
	return sqrt(pow(segment[0] - segment[2], 2.0) + pow(segment[1] - segment[3], 2.0));
}

+ (Segment)scaleSegment:(Segment)segment factor:(double)factor
{
	Segment newSegment;

	// Compute length to be added on both ends of the segment
	double lengthDiffX = segment[2] - segment[0];
	double lengthDiffY = segment[3] - segment[1];
	double scaleFactor = (factor - 1.0) / 2.0;

	// Compute new segment coordinates
	newSegment[0] = segment[0] - lengthDiffX * scaleFactor;
	newSegment[1] = segment[1] - lengthDiffY * scaleFactor;
	newSegment[2] = segment[2] + lengthDiffX * scaleFactor;
	newSegment[3] = segment[3] + lengthDiffY * scaleFactor;

	return newSegment;
}

+ (Quad)fixQuadVertexOrder:(Quad)quad
{
	std::vector<int8_t> indices = {-1, -1, -1, -1};

	// Get point locations relative to P1
	cv::Point2d p1 = cv::Point2d(quad[2] - quad[0], quad[3] - quad[1]);
	cv::Point2d p3 = cv::Point2d(quad[6] - quad[0], quad[7] - quad[1]);

	// Get diagonal line relative to P1, as well as Ox and Oy lines
	Line diag = Line(p1.x, p1.y, p3.x, p3.y);
	Line oxLine = Line(0.0, 0.0, 1.0, 0.0);
	Line oyLine = Line(0.0, 0.0, 0.0, 1.0);

	// Compute the intersection of OX line and diagonal line
	cv::Point2d oxIntersection = [self intersectionOfLine:diag andLine:oxLine];
	cv::Point2d oyIntersection = [self intersectionOfLine:diag andLine:oyLine];

	// We now know the index of first point from Ox and Oy intersection of the opposite diagonal
	indices[0] = (oxIntersection.x >= 0.0 && oyIntersection.y < 0.0 ? 0 :
				  (oxIntersection.x < 0.0 && oyIntersection.y < 0.0 ? 1 :
				   (oxIntersection.x < 0.0 && oyIntersection.y >= 0.0 ? 2 : 3)));

	// Based on the index of first point, find out which one of second and fourth points is next by computing the
	// distance between point two and four and Ox and Oy axes
	cv::Point2d intersection = (indices[0] == 0 || indices[0] == 2 ? oxIntersection : oyIntersection);
	double distP1 = [self segmentLength:{(int) p1.x, (int) p1.y, (int) intersection.x, (int) intersection.y}];
	double distP3 = [self segmentLength:{(int) p3.x, (int) p3.y, (int) intersection.x, (int) intersection.y}];

	if (distP1 < distP3) {
		indices[1] = (indices[0] + 1) % 4;
		indices[3] = ((indices[0] - 1) + 4) % 4;
	} else {
		indices[1] = ((indices[0] - 1) + 4) % 4;
		indices[3] = (indices[0] + 1) % 4;
	}

	// The index of last point is the remainder index (the opposite index of first point)
	indices[2] = (indices[0] + 2) % 4;

	// Reorder the points
	//
	//    X----▶︎X
	//    ▲     |
	//    |     |
	//    |     ▼
	//    X◀︎----X
	//
	Quad newQuad;

	for (int i = 0; i < 4; i++) {
		int idx = indices[i];

		newQuad[idx * 2 + 0] = quad[i * 2 + 0];
		newQuad[idx * 2 + 1] = quad[i * 2 + 1];
	}

	return newQuad;
}

+ (NSString *)imageType:(cv::Mat)image
{
	NSString *type;

	unsigned char depth = image.type() & CV_MAT_DEPTH_MASK;
	unsigned char channels = 1 + (image.type() >> CV_CN_SHIFT);

	switch (depth) {
		case CV_8U:		type = @"8U";	break;
		case CV_8S:		type = @"8S";	break;
		case CV_16U:	type = @"16U";	break;
		case CV_16S:	type = @"16S";	break;
		case CV_32S:	type = @"32S";	break;
		case CV_32F:	type = @"32F";	break;
		case CV_64F:	type = @"64F";	break;
		default:		type = @"User";	break;
	}

	return [NSString stringWithFormat:@"%@C%c", type, channels + '0'];
}

@end

