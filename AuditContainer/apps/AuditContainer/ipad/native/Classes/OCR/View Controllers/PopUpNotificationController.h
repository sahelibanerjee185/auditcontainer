//
//  PopupNotificationController.h
//  LiveCharacterRecognitionDemo
//
//  Created by Iulian Corcoja on 7/29/16.
//  Copyright © 2016 Iulian Corcoja. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PopupNotificationControllerDelegate.h"

#pragma mark - Types definitions

typedef NS_OPTIONS(NSUInteger, PopupNotificationControllerArrowDirection) {
	PopupNotificationControllerArrowDirectionUp = 1UL << 0,
	PopupNotificationControllerArrowDirectionDown = 1UL << 1,
	PopupNotificationControllerArrowDirectionLeft = 1UL << 2,
	PopupNotificationControllerArrowDirectionRight = 1UL << 3,
	PopupNotificationControllerArrowDirectionAny = PopupNotificationControllerArrowDirectionUp | PopupNotificationControllerArrowDirectionDown | PopupNotificationControllerArrowDirectionLeft | PopupNotificationControllerArrowDirectionRight,
	PopupNotificationControllerArrowDirectionUnknown = NSUIntegerMax
};

typedef NS_ENUM(NSInteger, PopupNotificationControllerStyle) {
	PopupNotificationControllerStyleDefault			= 0, // Dark content, for use on light backgrounds
	PopupNotificationControllerStyleLightContent	= 1, // Light content, for use on dark backgrounds
};

@interface PopupNotificationController : UIViewController

#pragma mark - Properties and outlets

@property (nonatomic, strong) NSString *text;
@property (nonatomic, strong) NSString *switchLabelText;

@property (nonatomic, assign) PopupNotificationControllerStyle style;
@property (nonatomic, assign) PopupNotificationControllerArrowDirection permittedArrowDirections;

@property (nonatomic, weak) id<PopupNotificationControllerDelegate> delegate;

@property (nonatomic, weak) IBOutlet UILabel *textLabel;
@property (nonatomic, weak) IBOutlet UILabel *switchLabel;
@property (nonatomic, weak) IBOutlet UISwitch *switchButton;

#pragma mark - Initialization methods

- (instancetype)initWithSourceView:(UIView *)sourceView popupSize:(CGSize)popupSize;

@end
