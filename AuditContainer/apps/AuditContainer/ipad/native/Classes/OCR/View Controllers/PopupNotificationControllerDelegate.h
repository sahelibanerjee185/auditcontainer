//
//  PopupNotificationControllerDelegate.h
//  LiveCharacterRecognitionDemo
//
//  Created by Iulian Corcoja on 8/4/16.
//  Copyright © 2016 Iulian Corcoja. All rights reserved.
//

#import <Foundation/Foundation.h>

@class PopupNotificationController;

@protocol PopupNotificationControllerDelegate <NSObject>

@optional

- (void)popupNotificationControllerDidLoad:(PopupNotificationController *)popupNotificationController;

- (void)popupNotificationController:(PopupNotificationController *)popupNotificationController switchValueChanged:(BOOL)switchValue;

@end

