//
//  PopupNotificationController.m
//  LiveCharacterRecognitionDemo
//
//  Created by Iulian Corcoja on 7/29/16.
//  Copyright © 2016 Iulian Corcoja. All rights reserved.
//

#import "PopUpNotificationController.h"

@interface PopupNotificationController () <UIPopoverPresentationControllerDelegate>

@end

@implementation PopupNotificationController

#pragma mark - View controller's lifecycle methods

- (instancetype)init
{
	@throw [NSException exceptionWithName:NSInternalInconsistencyException reason:
			[NSString stringWithFormat:@"-init is not a valid initializer for the class %@",
			 NSStringFromClass([self class])] userInfo:nil];
	return nil;
}

- (instancetype)initWithSourceView:(UIView *)sourceView popupSize:(CGSize)popupSize
{
    self = [super init];
    if (self) {

		// Set presentation style of the pop up notification controller
		self.modalPresentationStyle = UIModalPresentationPopover;

		// Set popover presentation controller properties
		self.preferredContentSize = popupSize;
		self.popoverPresentationController.delegate = self;
		self.popoverPresentationController.sourceView = sourceView;
		self.popoverPresentationController.sourceRect = sourceView.bounds;

		// Set defaul values
		self.permittedArrowDirections = UIPopoverArrowDirectionAny;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.

	// Setup view controls
	[self.textLabel setText:self.text];
	[self.switchLabel setText:self.switchLabelText];
	[self setStyle:self.style];

	// Call delegate method
	if (self.delegate && [self.delegate respondsToSelector:@selector(popupNotificationControllerDidLoad:)]) {
		[self.delegate popupNotificationControllerDidLoad:self];
	}
}

#pragma mark - Setters and getters

- (void)setText:(NSString *)text
{
	_text = text;

	// Set text label text
	self.textLabel.text = text;
}

- (void)setSwitchLabelText:(NSString *)switchLabelText
{
	_switchLabelText = switchLabelText;

	// Set switch label text
	self.switchLabel.text = switchLabelText;
}

- (void)setStyle:(PopupNotificationControllerStyle)style
{
	_style = style;

	switch (style) {
		case PopupNotificationControllerStyleDefault: {

			// Set popover presentation controller backround color to clear color
			self.popoverPresentationController.backgroundColor = nil;

			// Set labels text color to black
			self.textLabel.textColor = [UIColor darkGrayColor];
			self.switchLabel.textColor = [UIColor darkGrayColor];

			// Set switch color
			self.switchButton.tintColor = [UIColor lightGrayColor];
			self.switchButton.onTintColor = [UIColor lightGrayColor];
			self.switchButton.thumbTintColor = nil;

			break;
		}
		case PopupNotificationControllerStyleLightContent: {

			// Set popover presentation controller backround color to clear color
			self.popoverPresentationController.backgroundColor = [UIColor colorWithWhite:0.0f alpha:0.35f];

			// Set labels text color to black
			self.textLabel.textColor = [UIColor whiteColor];
			self.switchLabel.textColor = [UIColor whiteColor];

			// Set switch color
			self.switchButton.tintColor = [UIColor whiteColor];
			self.switchButton.onTintColor = [UIColor colorWithWhite:0.9f alpha:1.0f];
			self.switchButton.thumbTintColor = nil;

			break;
		}
	}
}

- (void)setPermittedArrowDirections:(PopupNotificationControllerArrowDirection)permittedArrowDirections
{
	self.popoverPresentationController.permittedArrowDirections = ((UIPopoverArrowDirection) permittedArrowDirections);
}

- (PopupNotificationControllerArrowDirection)permittedArrowDirections
{
	return (PopupNotificationControllerArrowDirection) self.popoverPresentationController.permittedArrowDirections;
}

#pragma mark - Gesture recognizers and actions

- (IBAction)switchValueChanged:(UISwitch *)sender
{
	// Call delegate method
	if (self.delegate && [self.delegate respondsToSelector:@selector(popupNotificationController:switchValueChanged:)]) {
		[self.delegate popupNotificationController:self switchValueChanged:sender.isOn];
	}
}

#pragma mark - Popover presentation controller delegate methods

- (UIModalPresentationStyle)adaptivePresentationStyleForPresentationController:(UIPresentationController *)controller
															   traitCollection:(UITraitCollection *)traitCollection
{
	return UIModalPresentationNone;
}

#pragma mark - View controller's memory management methods

- (void)didReceiveMemoryWarning
{
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.

	NSLog(@"memory warning received");
}

@end

