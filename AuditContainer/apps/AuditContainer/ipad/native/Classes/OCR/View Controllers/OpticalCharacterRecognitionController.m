//
//  OpticalCharacterRecognitionController.m
//  Optical Character Recognition Controller
//
//  Created by Iulian Corcoja on 3/30/16.
//
//

#import "OpticalCharacterRecognitionController.h"
#import "OpticalCharacterRecognitionCameraController.h"

NSString *const kOpticalCharacterRecognitionControllerImage = @"optionsImageKey";
NSString *const kOpticalCharacterRecognitionControllerRecognizedText = @"optionsRecognizedTextKey";
NSString *const kOpticalCharacterRecognitionControllerAnglePitch = @"optionsAnglePitchKey";
NSString *const kOpticalCharacterRecognitionControllerAngleTilt = @"optionsAngleTiltKey";
NSString *const kOpticalCharacterRecognitionControllerFlash = @"optionsFlashKey";
NSString *const kOpticalCharacterRecognitionControllerProcessingTime = @"optionsProcessingTimeKey";

@interface OpticalCharacterRecognitionController ()

@end

@implementation OpticalCharacterRecognitionController

#pragma mark - View controller's lifecycle methods

- (instancetype)init
{
	// First of all, create an instance of optical character recognition camera controller
	OpticalCharacterRecognitionCameraController *controller = [OpticalCharacterRecognitionCameraController initFromNib];
	
	// Check if controller was initialized successfully
	if (controller) {
		
		// Initialize navigation controller with root camera controller
		self = [super initWithRootViewController:controller];
		
		if (self) {
			// Custom initialization...
			self.navigationBarHidden = YES;
		}
	}
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

#pragma mark - View controller delegate methods

- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
	return UIInterfaceOrientationMaskAll;
}

- (UITraitCollection *)overrideTraitCollectionForChildViewController:(UIViewController *)childViewController
{
	if (CGRectGetWidth(self.view.bounds) < CGRectGetHeight(self.view.bounds)) {
		return [UITraitCollection traitCollectionWithHorizontalSizeClass:UIUserInterfaceSizeClassCompact];
	} else {
		
		return [UITraitCollection traitCollectionWithTraitsFromCollections:
				@[[UITraitCollection traitCollectionWithHorizontalSizeClass:UIUserInterfaceSizeClassRegular],
				  [UITraitCollection traitCollectionWithVerticalSizeClass:UIUserInterfaceSizeClassCompact]]];
	}
}

#pragma mark - View controller's memory management methods

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
