//
//  OpticalCharacterRecognitionController.h
//  Optical Character Recognition Controller
//
//  Created by Iulian Corcoja on 3/30/16.
//
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import <CoreMotion/CoreMotion.h>
#import <ImageIO/ImageIO.h>
#import <TesseractOCR/TesseractOCR.h>
#import "OpticalCharacterRecognitionControllerDelegate.h"
#import "G8RecognitionOperation+Extension.h"
#import "ImageProcessor.h"

#pragma mark - Constants

FOUNDATION_EXPORT NSString *const kOpticalCharacterRecognitionControllerImage;
FOUNDATION_EXPORT NSString *const kOpticalCharacterRecognitionControllerRecognizedText;
FOUNDATION_EXPORT NSString *const kOpticalCharacterRecognitionControllerAnglePitch;
FOUNDATION_EXPORT NSString *const kOpticalCharacterRecognitionControllerAngleTilt;
FOUNDATION_EXPORT NSString *const kOpticalCharacterRecognitionControllerFlash;
FOUNDATION_EXPORT NSString *const kOpticalCharacterRecognitionControllerProcessingTime;

#pragma mark - Optical character recognition controller interface

@interface OpticalCharacterRecognitionController : UINavigationController

@property (nonatomic, weak) id<OpticalCharacterRecognitionControllerDelegate> recognitionDelegate;

@property (nonatomic, strong) NSArray<UIImage *> *preprocessedImages;

@end
