//
//  OpticalCharacterRecognitionCameraController.m
//  Optical Character Recognition
//
//  Created by Iulian Corcoja on 1/22/16.
//  Copyright © 2016 Iulian Corcoja. All rights reserved.
//

#import "OpticalCharacterRecognitionCameraController.h"
#import "OpticalCharacterRecognitionController.h"
#import "PopUpNotificationController.h"
#import "VideoPreviewView.h"
#import "SelectAreaView.h"
#import "ActivityIndicator.h"
#import "UIImage+ICFilters.h"
#import "NSString+ICUtils.h"
#import "ContainerAudit-Swift.h"

#pragma mark Type defines

typedef NS_ENUM(NSInteger, AVCamSetupResult) {
	AVCamSetupResultSuccess,
	AVCamSetupResultCameraNotAuthorized,
	AVCamSetupResultSessionConfigurationFailed
};

typedef NS_ENUM(NSInteger, FMFlashMode) {
	FMFlashModeUnknown	= -1,
	FMFlashModeOff		=  AVCaptureFlashModeOff,
	FMFlashModeOn		=  AVCaptureFlashModeOn,
	FMFlashModeAuto		=  AVCaptureFlashModeAuto
};

#pragma mark - Constants

static const UILayoutPriority UILayoutPriorityDefaultVeryHigh = 900;

#define kUserDefaultsShowAnglePopupNotificationKey @"ShowAnglePopupNotification"

#define kResultImagePopAnimationDuration 0.5

#define kInfoLabelAnimationDuration 0.8
#define kInfoLabelAnimationDelay 1.0

#define kCameraMaxZoomFactor 6.0f
#define kCameraAutoFocusTriggerCountdown 8.0
#define kCameraMinZoomFactorWarning 3.0f
#define kCameraMinResolutionWarning 1024.0f
#define kCameraMaxAngleWarning 0.52359877f

#define kTintColorNormal [UIColor whiteColor]
#define kTintColorHighlighted [UIColor colorWithRed:0.41f green:0.72f blue:0.84f alpha:1.0f]

#define kInfoTextFlashOn @"Flash On"
#define kInfoTextFlashOff @"Flash Off"
#define kInfoTextFlashAuto @"Flash Auto"
#define kInfoTextAngleCorrectionOn @"Angle Correction On"
#define kInfoTextAngleCorrectionOff @"Angle Correction Off"
#define kInfoAngleCorrectionDescription @"Control angle correction by tapping on this button. Disabled "\
										"angle correction allows you to capture photos of containers that are sloping "\
										"or not positioned parallel to the horizon."

#define kWarningMessageMaxLineLength 40
#define kWarningMessageFormat @"Warning! %@ may affect the results"
#define kWarningSmallReticleArea @"small reticle area"
#define kWarningHighZoomFactor @"high zoom factor"
#define kWarningSteepDeviceAngle @"steep device angle"

#define kPopupAngleCorrectionSwitchText @"I am aware of this"
#define kPopupAngleCorrectionSize CGSizeMake(236.0f, 200.0f)

#define kSelecAreaViewDefaultColor [UIColor colorWithRed:0.41f green:0.72f blue:0.84f alpha:1.0f]
#define kSelecAreaViewWarningColor [UIColor colorWithRed:1.0f green:0.76f blue:0.17f alpha:1.0f]

#define kFocusPointViewColor [UIColor colorWithRed:0.41f green:0.72f blue:0.84f alpha:1.0f]
#define kFocusPointViewStrokeWidth 1.0f
#define kFocusPointViewSize 74.0f
#define kFocusPointViewMiddleLinesLengthScale 0.15f

#pragma mark - Optical character recognition camera controller interface

@interface OpticalCharacterRecognitionCameraController () <G8TesseractDelegate, SelectAreaViewDelegate,
														   PopupNotificationControllerDelegate>

@property (nonatomic, weak) IBOutlet VideoPreviewView *previewView;
@property (nonatomic, weak) IBOutlet SelectAreaView *selectAreaView;
@property (nonatomic, weak) IBOutlet UIImageView *resultImageView;
@property (nonatomic, weak) IBOutlet UIView *resultCoverView;
@property (nonatomic, weak) IBOutlet UIView *controlsView;
@property (nonatomic, weak) IBOutlet UIButton *shutterButton;
@property (nonatomic, weak) IBOutlet UIButton *flashButton;
@property (nonatomic, weak) IBOutlet UIButton *angleCorrectionButton;
@property (nonatomic, weak) IBOutlet UILabel *infoLabel;

@property (nonatomic, weak) IBOutlet NSLayoutConstraint *resultImageViewWidthConstraint;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *resultImageViewHeightConstraint;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *flashButtonWidthConstraint;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *flashButtonHeightConstraint;

@property (nonatomic, strong) AVCaptureDevice *videoDevice;
@property (nonatomic, strong) AVCaptureSession *captureSesion;
@property (nonatomic, strong) AVCaptureStillImageOutput *stillCameraOutput;
@property (nonatomic, strong) dispatch_queue_t cameraSessionQueue;
@property (nonatomic, strong) NSOperationQueue *tesseractOperationQueue;

@property (nonatomic, assign) CGSize cameraResolution;
@property (nonatomic, assign) AVCamSetupResult setupResult;

@property (nonatomic, strong) CMMotionManager *motionManager;

@property (nonatomic, assign) FMFlashMode flashMode;
@property (nonatomic, strong) FocusPointView *focusPointView;
@property (nonatomic, strong) NSTimer *autoFocusTimer;

@property (nonatomic, assign) BOOL angleCorrection;
@property (nonatomic, assign) float pitch;
@property (nonatomic, assign) float tilt;

@property (nonatomic, assign) BOOL showAnglePopupNotification;

@property (nonatomic, assign) BOOL smallReticleCameraWarning;
@property (nonatomic, assign) BOOL zoomCameraWarning;
@property (nonatomic, assign) BOOL steepAngleCameraWarning;

@property (nonatomic, weak, nullable, readonly) OpticalCharacterRecognitionController *parentViewController;

@end

#pragma mark - Optical character recognition camera controller implementation

@implementation OpticalCharacterRecognitionCameraController

@dynamic parentViewController;

#pragma mark - View controller's lifecycle methods

+ (instancetype)initFromNib
{
	OpticalCharacterRecognitionCameraController *viewController = [[OpticalCharacterRecognitionCameraController alloc]
																   initWithNibName:NSStringFromClass([self class])
																   bundle:nil];
	return viewController;
}

- (void)viewDidLoad
{
	[super viewDidLoad];
	// Do any additional setup after loading the view.
	
	// Set defualt values
	self.setupResult = AVCamSetupResultSuccess;
	self.angleCorrection = YES;

	// Set select area view properties
	self.selectAreaView.delegate = self;
	
	// Setup the preview view
	self.previewView.session = self.captureSesion;
	
	// Communicate with the session and other session objects on this queue
	self.cameraSessionQueue = dispatch_queue_create("VideoSessionQueue", DISPATCH_QUEUE_SERIAL);
	
	// Check video authorization status - video access is required
	switch ([AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo]) {
		case AVAuthorizationStatusAuthorized: {
			
			// The user has previously granted access to the camera
			break;
		}
		case AVAuthorizationStatusNotDetermined: {
			
			// The user has not yet been presented with the option to grant video access. We suspend the session queue
			// to delay session setup until the access request has completed to avoid image capturing without an image
			// output
			dispatch_suspend(self.cameraSessionQueue);
			[AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted) {
				if (!granted) {
					self.setupResult = AVCamSetupResultCameraNotAuthorized;
				}
				dispatch_resume(self.cameraSessionQueue);
			}];
			break;
		}
		default: {
			// The user has previously denied access
			self.setupResult = AVCamSetupResultCameraNotAuthorized;
			break;
		}
	}
	
	// Setup the capture session. In general it is not safe to mutate an AVCaptureSession or any of its inputs, outputs,
	// or connections from multiple threads at the same time. Why not do all of this on the main queue? Because
	// -[AVCaptureSession startRunning] is a blocking call which can take a long time. We dispatch session setup to the
	// sessionQueue so that the main queue isn't blocked, which keeps the UI responsive.
	dispatch_async(self.cameraSessionQueue, ^{
		NSError *error = nil;
		
		if (self.setupResult != AVCamSetupResultSuccess) {
			return;
		}
		
		AVCaptureDevice *videoDevice = [OpticalCharacterRecognitionCameraController deviceWithMediaType:
										AVMediaTypeVideo preferringPosition:AVCaptureDevicePositionBack];
		AVCaptureDeviceInput *videoDeviceInput = [AVCaptureDeviceInput deviceInputWithDevice:videoDevice error:&error];
		
		if (!videoDeviceInput) {
			NSLog(@"could not create video device input: %@", error);
		}
		
		[self.captureSesion beginConfiguration];
		
		// Add video input device (back device camera)
		if ([self.captureSesion canAddInput:videoDeviceInput]) {
			[self.captureSesion addInput:videoDeviceInput];
			self.videoDevice = videoDevice;
			
			// Enable video device continous auto-focus and auto-exposure
			if ([self.videoDevice isFocusModeSupported:AVCaptureFocusModeContinuousAutoFocus] &&
				[self.videoDevice isExposureModeSupported:AVCaptureExposureModeContinuousAutoExposure]) {
				if ([self.videoDevice lockForConfiguration:&error]) {
					
					// Set video device focus mode to auto-focus and auto-exposure
					[self.videoDevice setFocusMode:AVCaptureFocusModeContinuousAutoFocus];
					[self.videoDevice setExposureMode:AVCaptureExposureModeContinuousAutoExposure];
					[self.videoDevice unlockForConfiguration];
				}
			}
			
			// Why are we dispatching this to the main queue? Because capture video preview layer is the backing layer
			// for video preview view and UIView can only be manipulated on the main thread
			dispatch_async(dispatch_get_main_queue(), ^{
				
				// Set initial video orientation and aspect fill
				[self setPreviewLayerOutputOrientation];
				[(AVCaptureVideoPreviewLayer *) self.previewView.layer setVideoGravity:
				 AVLayerVideoGravityResizeAspectFill];

				if (!self.videoDevice.hasFlash) {

					// Hide flash button because this camera device has no flash
					self.flashButton.hidden = YES;
					self.flashButtonWidthConstraint.priority = UILayoutPriorityDefaultVeryHigh;
					self.flashButtonHeightConstraint.priority = UILayoutPriorityDefaultVeryHigh;
					
				} else {
					
					// Change flash enabled state without invoking the setter
					_flashMode = (FMFlashMode) self.videoDevice.flashMode;

					// Update flash button icon
					[self updateFlashButtonIcon];
				}
			});
		} else {
			
			NSLog(@"could not add video device input to the session");
			self.setupResult = AVCamSetupResultSessionConfigurationFailed;
		}
		
		// Create and add still camera image output
		self.stillCameraOutput = [[AVCaptureStillImageOutput alloc] init];
		if ([self.captureSesion canAddOutput:self.stillCameraOutput]) {
			[self.captureSesion addOutput:self.stillCameraOutput];
		}
		
		// Commit current configurations
		[self.captureSesion commitConfiguration];
	});
}

- (void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
	
	// Update status bar appearance
	[self setNeedsStatusBarAppearanceUpdate];
	
	// Start camera capture session
	dispatch_async(self.cameraSessionQueue, ^{
		switch (self.setupResult) {
			case AVCamSetupResultSuccess: {
				
				// Only setup observers and start the session running if setup succeeded.
				[self.captureSesion startRunning];
				break;
			}
			case AVCamSetupResultCameraNotAuthorized: {
				NSLog(@"application doesn't have permission to use the camera, please change privacy settings");
				break;
			}
			case AVCamSetupResultSessionConfigurationFailed: {
				NSLog(@"something went wrong during capture session configuration");
				break;
			}
		}
	});
	
	// Start retrieving device gyroscope data
	if (self.motionManager.deviceMotionAvailable) {
		[self.motionManager startDeviceMotionUpdatesUsingReferenceFrame:
		 CMAttitudeReferenceFrameXArbitraryCorrectedZVertical toQueue:[NSOperationQueue mainQueue] withHandler:
		 ^(CMDeviceMotion * _Nullable motion, NSError * _Nullable error) {

			 // Set device perspective (pitch) and tilt according to device orientation
			 switch ([[UIDevice currentDevice] orientation]) {
				 case UIDeviceOrientationPortrait: {
					 self.tilt = atan2(motion.gravity.x, motion.gravity.y) - M_PI;
					 self.pitch = atan2(motion.gravity.y, motion.gravity.z) + M_PI_2;
					 break;
				 }
				 case UIDeviceOrientationPortraitUpsideDown: {
					 self.tilt = atan2(motion.gravity.x, motion.gravity.y);
					 self.pitch = -atan2(motion.gravity.y, motion.gravity.z) + M_PI_2;
					 break;
				 }
				 case UIDeviceOrientationLandscapeLeft: {
					 self.tilt = atan2(motion.gravity.x, motion.gravity.y) + M_PI_2;
					 self.pitch = atan2(motion.gravity.x, motion.gravity.z) + M_PI_2;
					 break;
				 }
				 case UIDeviceOrientationLandscapeRight: {
					 self.tilt = atan2(motion.gravity.x, motion.gravity.y) - M_PI_2;
					 self.pitch = atan2(motion.gravity.z, motion.gravity.x);
					 break;
				 }
				 default: {
					 self.tilt = 0.0;
					 self.pitch = 0.0;
					 break;
				 }
			 }
			 
			 // Set tilt and pitch values in [-180..180] degrees range
			 self.tilt = atan2(sin(self.tilt), cos(self.tilt));
			 self.pitch = atan2(sin(self.pitch), cos(self.pitch));

			 // Update steep angle camera warning
			 self.steepAngleCameraWarning = (self.angleCorrection && (self.tilt > kCameraMaxAngleWarning ||
																	  self.tilt < -kCameraMaxAngleWarning ||
																	  self.pitch > kCameraMaxAngleWarning ||
																	  self.pitch < -kCameraMaxAngleWarning));
		 }];
	}
}

- (void)viewDidAppear:(BOOL)animated
{
	[super viewDidAppear:animated];

	// Show popup notification about angle correction
	if (self.showAnglePopupNotification) {
		[self performSelector:@selector(showAngleCorrectionPopupHelper) withObject:nil afterDelay:0.1];
	}
}

- (void)viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
	
	// Update status bar appearance
	[self setNeedsStatusBarAppearanceUpdate];
}

- (void)viewDidDisappear:(BOOL)animated
{
	// Stop running camera capture session
	dispatch_async(self.cameraSessionQueue, ^{
		if (self.setupResult == AVCamSetupResultSuccess) {
			[self.captureSesion stopRunning];
		}
	});
	
	// Stop retrieving device gyroscope data
	[self.motionManager stopDeviceMotionUpdates];
	
	[super viewDidDisappear:animated];
}

#pragma mark - Auto-layout delegate methods

- (void)viewDidLayoutSubviews
{
	[super viewDidLayoutSubviews];
}

#pragma mark - Setters and getter

- (void)setVideoDevice:(AVCaptureDevice *)videoDevice
{
	_videoDevice = videoDevice;

	// Get camera resolution by comparing all available formats
	if (videoDevice.formats.count > 0) {
		AVCaptureDeviceFormat *highestFormat = videoDevice.formats.firstObject;
		int maxWidth = highestFormat.highResolutionStillImageDimensions.width;

		for (AVCaptureDeviceFormat *format in videoDevice.formats) {
			if (format.highResolutionStillImageDimensions.width > maxWidth) {

				highestFormat = format;
			}
		}

		// Set camera resolution with the highest format
		self.cameraResolution = CGSizeMake(highestFormat.highResolutionStillImageDimensions.width,
										   highestFormat.highResolutionStillImageDimensions.height);

		NSLog(@"camera resolution: %@", NSStringFromCGSize(self.cameraResolution));
	}
}

- (AVCaptureSession *)captureSesion
{
	if (!_captureSesion) {
		
		// Lazy initialize video capture session
		_captureSesion = [[AVCaptureSession alloc] init];
		_captureSesion.sessionPreset = AVCaptureSessionPresetPhoto;
	}
	return _captureSesion;
}

- (NSOperationQueue *)tesseractOperationQueue
{
	if (!_tesseractOperationQueue) {
		
		// Lazy initialize tesseract operation queue
		_tesseractOperationQueue = [[NSOperationQueue alloc] init];
	}
	return _tesseractOperationQueue;
}

- (CMMotionManager *)motionManager
{
	if (!_motionManager) {
		
		// Lazy initialize motion manager
		_motionManager = [[CMMotionManager alloc] init];
		_motionManager.deviceMotionUpdateInterval = 0.1;
	}
	return _motionManager;
}

- (void)setFlashMode:(FMFlashMode)flashMode
{
	if (self.videoDevice && self.videoDevice.hasFlash) {
		NSError *error = nil;

		// Lock camera video device for turning flash on or off
		if ([self.videoDevice lockForConfiguration:&error]) {

			// Set flash mode on or off and change variable backing state
			[self.videoDevice setFlashMode:(AVCaptureFlashMode) flashMode];
			_flashMode = flashMode;
			[self.videoDevice unlockForConfiguration];

		} else {

			// An error occurred while trying to turn flash on or off
			NSLog(@"an error occurred while trying to turn flash on or off: %@", error);
		}
	} else {

		NSLog(@"this camera device has no flash");
	}

	// Update flash button icon
	[self updateFlashButtonIcon];
}

- (void)setAngleCorrection:(BOOL)angleCorrection
{
	_angleCorrection = angleCorrection;

	// Update angle correction button icon
	[self updateAngleCorrectionButtonIcon];
}

- (FocusPointView *)focusPointView
{
	if (!_focusPointView) {
		
		// Lazy initialize focus flash view
		_focusPointView =  [[FocusPointView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, kFocusPointViewSize,
																			kFocusPointViewSize)];
		_focusPointView.strokeWidth = kFocusPointViewStrokeWidth;
		_focusPointView.color = kFocusPointViewColor;
		_focusPointView.middleLinesLenghtScale = kFocusPointViewMiddleLinesLengthScale;
		_focusPointView.userInteractionEnabled = NO;
		
		// Add focus point view to the main view below controls view
		[self.view insertSubview:_focusPointView belowSubview:self.controlsView];
	}
	return _focusPointView;
}

- (BOOL)showAnglePopupNotification
{
	id value = [[NSUserDefaults standardUserDefaults] objectForKey:kUserDefaultsShowAnglePopupNotificationKey];
	return (value && [value isKindOfClass:[NSNumber class]] ? [value boolValue] : YES);
}

- (void)setShowAnglePopupNotification:(BOOL)showAnglePopupNotification
{
	[[NSUserDefaults standardUserDefaults] setObject:@(showAnglePopupNotification)
											  forKey:kUserDefaultsShowAnglePopupNotificationKey];
	[[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)setSmallReticleCameraWarning:(BOOL)smallReticleCameraWarning
{
	// Check if new state is different then the old one – if so, update camera warning text
	if (_smallReticleCameraWarning != smallReticleCameraWarning) {
		_smallReticleCameraWarning = smallReticleCameraWarning;
		[self updateCameraWarningText];
	}
	_smallReticleCameraWarning = smallReticleCameraWarning;
}

- (void)setZoomCameraWarning:(BOOL)zoomCameraWarning
{
	// Check if new state is different then the old one – if so, update camera warning text
	if (_zoomCameraWarning != zoomCameraWarning) {
		_zoomCameraWarning = zoomCameraWarning;
		[self updateCameraWarningText];
	}
	_zoomCameraWarning = zoomCameraWarning;
}

- (void)setSteepAngleCameraWarning:(BOOL)steepAngleCameraWarning
{
	// Check if new state is different then the old one – if so, update camera warning text
	if (_steepAngleCameraWarning != steepAngleCameraWarning) {
		_steepAngleCameraWarning = steepAngleCameraWarning;
		[self updateCameraWarningText];
	}
	_steepAngleCameraWarning = steepAngleCameraWarning;
}

#pragma mark - Gesture recognizers and actions

- (IBAction)takePhotoTouchAction:(UIButton *)sender
{
    // Disable controls view user interaction
    self.controlsView.userInteractionEnabled = NO;
    
	// Capture still image from camera session
	[self captureStillImage];

	// Stop motion manager capturing motion updated
	[self.motionManager stopDeviceMotionUpdates];
}

- (IBAction)flashTouchAction:(UIButton *)sender
{
	// Loop through flash modes
	self.flashMode = (self.flashMode == FMFlashModeOff ? FMFlashModeOn :
					  (self.flashMode == FMFlashModeOn ? FMFlashModeAuto : FMFlashModeOff));
	
	// Animate info label to show flash status
	[self flashInfoLabelWithText:(self.flashMode == FMFlashModeOff ? kInfoTextFlashOff :
								  (self.flashMode == FMFlashModeOn ? kInfoTextFlashOn : kInfoTextFlashAuto))];
}

- (IBAction)angleCorrectionTouchAction:(UIButton *)sender
{
	// Switch angle correction
	self.angleCorrection = !self.angleCorrection;

	// Animate info label to show angle correction status
	[self flashInfoLabelWithText:self.angleCorrection ? kInfoTextAngleCorrectionOn : kInfoTextAngleCorrectionOff];
}

- (IBAction)cancelTouchAction:(UIButton *)sender
{
	// Call did cancel delegate method
	if (self.parentViewController.recognitionDelegate &&
		[self.parentViewController.recognitionDelegate
		 respondsToSelector:@selector(opticalCharacterRecognitionControllerDidCancel:)]) {

			[self.parentViewController.recognitionDelegate opticalCharacterRecognitionControllerDidCancel:
			 self.parentViewController];
	}
}

- (IBAction)tapGesture:(UITapGestureRecognizer *)tapRecognizer
{
	CGPoint touchLocation = [tapRecognizer locationInView:tapRecognizer.view];
	
	// Show focus point view
	self.focusPointView.center = touchLocation;
	[self.focusPointView startFlashAnimation];
	
	// Get focus point
	CGPoint focusPoint = [(AVCaptureVideoPreviewLayer *) self.previewView.layer captureDevicePointOfInterestForPoint:
						  touchLocation];
	
	NSLog(@"focus at point: %@", NSStringFromCGPoint(focusPoint));
	
	// Set video device focus point to the touch location
	if ([self.videoDevice isFocusPointOfInterestSupported] &&
		[self.videoDevice isFocusModeSupported:AVCaptureFocusModeAutoFocus] &&
		[self.videoDevice isExposureModeSupported:AVCaptureExposureModeAutoExpose]) {
		NSError *error = nil;
		
		// Lock camera video device for capturing focus point of interest and auto-exposure
		if ([self.videoDevice lockForConfiguration:&error]) {
			
			// Invalidate auto-focus timer (we'll start a new countdown for continous auto-focus)
			[self.autoFocusTimer invalidate];
			self.autoFocusTimer = nil;
			
			// Set focus point of interest and auto-exposure
			[self.videoDevice setFocusPointOfInterest:focusPoint];
			[self.videoDevice setFocusMode:AVCaptureFocusModeAutoFocus];
			[self.videoDevice setExposureMode:AVCaptureExposureModeAutoExpose];
			[self.videoDevice unlockForConfiguration];
			
			// When point of interest has been set, start the auto-focus timer to return back to continous auto-focus
			// after a specfic time
			self.autoFocusTimer = [NSTimer scheduledTimerWithTimeInterval:kCameraAutoFocusTriggerCountdown target:
								   self selector:@selector(autoFocusTimerTriggered:) userInfo:nil repeats:NO];
		} else {
			
			// An error occurred while trying to capture focus point of interest and auto-exposure
			NSLog(@"an error occurred while trying to capture focus point of interest and auto-exposure: %@", error);
		}
	}
}

- (IBAction)pinchGesture:(UIPinchGestureRecognizer *)pinchRecognizer
{
	static CGFloat startVideoZoomFactor;
	
	switch (pinchRecognizer.state) {
		case UIGestureRecognizerStateBegan: {
			startVideoZoomFactor = self.videoDevice.videoZoomFactor;
		}
		case UIGestureRecognizerStateChanged: {
			NSError *error = nil;
			
			// Lock camera video device for camera zoom
			if ([self.videoDevice lockForConfiguration:&error]) {
				
				// Set new zoom factor of the video device
				self.videoDevice.videoZoomFactor = MAX(1.0f,
													   MIN(startVideoZoomFactor * pinchRecognizer.scale,
														   MIN(kCameraMaxZoomFactor,
															   self.videoDevice.activeFormat.videoMaxZoomFactor)));
				[self.videoDevice unlockForConfiguration];

				// Update zoom camera warning status
				self.zoomCameraWarning = (self.videoDevice.videoZoomFactor > kCameraMinZoomFactorWarning);

			} else {
		
				// An error occurred while trying to zoom
				NSLog(@"an error occurred while trying to zoom video device: %@", error);
			}
			break;
		}
		default: {
			break;
		}
	}
}

#pragma mark - Text detection and recognition methods

- (void)startOpticalCharacterRecognitionProcessWithImage:(UIImage *)image
{
	// Stop capture session from running
	[self.captureSesion stopRunning];

	// Set result image view image and size
	self.resultCoverView.hidden = NO;
	self.resultImageView.hidden = NO;
	self.resultImageView.image = image;
	self.resultImageViewWidthConstraint.constant = self.selectAreaView.areaRect.size.width;
	self.resultImageViewHeightConstraint.constant = self.selectAreaView.areaRect.size.height;

	// Bring result image and cover view to front
	[self.view bringSubviewToFront:self.resultCoverView];
	[self.view bringSubviewToFront:self.resultImageView];

	// Update layout in order to update result image view size
	[self.view layoutIfNeeded];

	// Set new result image view height and width (which are equal to select area view size)
	self.resultImageViewWidthConstraint.constant = self.selectAreaView.bounds.size.width;
	self.resultImageViewHeightConstraint.constant = self.selectAreaView.bounds.size.height;

	// Start result image view pop animation
	[UIView animateWithDuration:kResultImagePopAnimationDuration animations:^{

		// Hide select area view and result cover view
		self.selectAreaView.alpha = 0.0f;
		self.resultCoverView.alpha = 1.0f;

		// Update layout in order to update result image view size
		[self.view layoutIfNeeded];
	}];

	// Remember current time for measuring execution time for OCR
	CFTimeInterval executionStartTime = CACurrentMediaTime();

	// Start activity indicator for optical character recogniton
	ActivityIndicator *activityIndicator = [[ActivityIndicator alloc] initWithTitle:@"Recognizing ID..."];
	[activityIndicator showActivityInView:self.navigationController.view animated:YES];

	// Start text detection and recognition in a background thread (recognize container ID)
	dispatch_async(dispatch_get_global_queue(QOS_CLASS_BACKGROUND, 0), ^{

		// Pre-process the image before running tesseract on it
		NSArray<UIImage *> *processedImages = [self preprocessedImagesForTesseractFromImage:image];

		// Assign pre-processed images to parent view controller
		self.parentViewController.preprocessedImages = processedImages;

		// Get main queue and run tesseract for text recognition (this process uses an operation queue so there is
		// no need in creating a separate thread)
		dispatch_async(dispatch_get_main_queue(), ^{
			if (processedImages.firstObject) {

				// Run tesseract on the processed image
				[self recognizeTextFromImage:processedImages.firstObject completionBlock:
				 ^(NSString *recognizedText) {

					 NSLog(@"tesseract recognized text: %@", recognizedText);

					 // Hide activity indicator
					 [activityIndicator hideActivityAnimated:YES];

					 // Hide result image and cover view and send them to back
					 self.resultCoverView.alpha = 0.0f;
                     self.selectAreaView.alpha = 1.0f;
					 self.resultCoverView.hidden = NO;
					 self.resultImageView.hidden = NO;
					 [self.view sendSubviewToBack:self.resultCoverView];
					 [self.view sendSubviewToBack:self.resultImageView];

					 // Check if delegate has did finish recognition method
					 if (self.parentViewController.recognitionDelegate &&
						 [self.parentViewController.recognitionDelegate
						  respondsToSelector:@selector(opticalCharacterRecognitionController:
													   didFinishRecognitionForImage:recognizedText:options:)]) {

							  // Create options dictionary
							  NSDictionary *options =
							  @{kOpticalCharacterRecognitionControllerImage: image,
								kOpticalCharacterRecognitionControllerRecognizedText: recognizedText ?: @"",
								kOpticalCharacterRecognitionControllerAnglePitch: @(self.pitch),
								kOpticalCharacterRecognitionControllerAngleTilt: @(self.tilt),
								kOpticalCharacterRecognitionControllerProcessingTime: @(CACurrentMediaTime() -
									executionStartTime), kOpticalCharacterRecognitionControllerFlash:
									(self.flashMode == FMFlashModeAuto ? @"auto" :
									 (self.flashMode == FMFlashModeOn ? @"on" :
									  (self.flashMode == FMFlashModeOff ? @"off" : @"unknown")))};

							  // Call did finish recognition delegate method
							  [self.parentViewController.recognitionDelegate opticalCharacterRecognitionController:
							   self.parentViewController didFinishRecognitionForImage:image recognizedText:
							   recognizedText options:options];
						 }
                     
                     // Enable controls view user interaction, start motion manager capturing and capture session
                     self.controlsView.userInteractionEnabled = YES;
                     [self.motionManager startDeviceMotionUpdates];
                     [self.captureSesion startRunning];
				 }];
			} else {

				// No images were returned from the pre-processing stage, call did cancel recognition delegate
				// method
				if (self.parentViewController.recognitionDelegate &&
					[self.parentViewController.recognitionDelegate respondsToSelector:
					 @selector(opticalCharacterRecognitionControllerDidCancel:)]) {
						
						[self.parentViewController.recognitionDelegate
						 opticalCharacterRecognitionControllerDidCancel:self.parentViewController];
					}
                
                // Enable controls view user interaction, start motion manager capturing and capture session
                self.controlsView.userInteractionEnabled = YES;
                [self.motionManager startDeviceMotionUpdates];
                [self.captureSesion startRunning];
			}
		});
	});
}

- (NSArray<UIImage *> *)preprocessedImagesForTesseractFromImage:(UIImage *)image
{
	CGSize cropFactor = self.selectAreaView.areaScale;
	float zoomFactor = self.videoDevice.videoZoomFactor;
	
	// Set pitch which is applied after a threshold of 5 degrees is passed. Pitch relies on width crop factor: the
	// smaller crop factor is - the more pitch value is taken into account
	float pitchThreshold = (M_PI / 180.0f * 5.0f);
	float pitch = ((ABS(self.pitch) < pitchThreshold ? 0.0f : -self.pitch) * (cropFactor.width) / zoomFactor);
	
	// Set tilt which is lowered for higher pitch values and works in fractions of 5 degrees. The reason for lowering
	// tilt values is because at a very sharp pitch angle, tilt angle losses its precision. After a pitch of 45 or more
	// degrees, tilt is ignored completely
	float tiltStep = (M_PI / 180.0f * 5.0f);
	float pitchLimit = (M_PI / 180.0f * 45.0f);
	float tilt = (((self.tilt > 0.0 ? floorf(self.tilt / tiltStep) : ceilf(self.tilt / tiltStep)) * tiltStep) *
				  MAX(1.0f - sqrtf(ABS(pitch) / pitchLimit), 0.0f));

#ifdef DEBUG_IMAGE_PROCCESSING

	// Return pre-processed image
	return [ImageProcessor prepareImageForOCR:image
					withPerspectiveCorrection:self.angleCorrection ? pitch : 0.0f
							   tiltCorrection:self.angleCorrection ? tilt : 0.0f
					  includeProcessingStages:YES];
#else

	// Return pre-processed image
	UIImage *preprocessedImage = [ImageProcessor prepareImageForOCR:image
										  withPerspectiveCorrection:self.angleCorrection ? pitch : 0.0f
													 tiltCorrection:self.angleCorrection ? tilt : 0.0f];
	// If image was pre-processed successfully, return it
	if (preprocessedImage) {
		return @[preprocessedImage];
	}

	// Null array otherwise
	return nil;

#endif
}

- (void)recognizeTextFromImage:(UIImage *)image
			   completionBlock:(void (^_Nullable)(NSString *recognizedText))completionBlock
{
	// Get trained data absolute path
	NSString *tessdataPath = [[[NSBundle mainBundle] bundlePath] stringByAppendingString:@"/Trained Data/maersk"];

	NSLog(@"tessdata absolute path: %@", tessdataPath);

	// Create and setup recognition process
	G8RecognitionOperation *operation = [[G8RecognitionOperation alloc] initWithLanguage:@"maersk" absoluteDataPath:
										 tessdataPath engineMode:G8OCREngineModeTesseractOnly];
	operation.tesseract.pageSegmentationMode = G8PageSegmentationModeSingleBlock;
	operation.delegate = self;
	operation.tesseract.image = image;
    operation.tesseract.maximumRecognitionTime = 10.0;
	
	// Limit recognized characters
	operation.tesseract.charWhitelist = @"1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	
	tick();
	
	// Set recognition completion block
	operation.recognitionCompleteBlock = ^(G8Tesseract *tesseract) {
		
		// Fetch the recognized text
		NSString *recognizedText = tesseract.recognizedText;
		
		// Call completion block (if available) on the main thread
		if (completionBlock) {
			dispatch_async(dispatch_get_main_queue(), ^{
				completionBlock(recognizedText);
			});
		}
		
		tock(0, @"tesseract text recognition done in: ", @"s");
	};
	
	// Finally, add the recognition operation to the queue
	[self.tesseractOperationQueue addOperation:operation];
}

#pragma mark - Tesseract delegate methods

- (BOOL)shouldCancelImageRecognitionForTesseract:(G8Tesseract *)tesseract
{
	// Return YES, if you need to cancel recognition prematurely
	return NO;
}

#pragma mark - View controller delegate methods

- (BOOL)prefersStatusBarHidden
{
	return YES;
}

- (void)viewWillTransitionToSize:(CGSize)size
	   withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator
{
	[coordinator animateAlongsideTransition:^(id<UIViewControllerTransitionCoordinatorContext>  _Nonnull context) {
		
		// Set new video orientation
		[self.captureSesion beginConfiguration];
		[self setPreviewLayerOutputOrientation];
		[self.captureSesion commitConfiguration];

	} completion:nil];
	
	[super viewWillTransitionToSize:size withTransitionCoordinator:coordinator];
}

#pragma mark - Select area view delegate methods

- (void)selectAreaView:(SelectAreaView *)view isChangingAreaRect:(CGRect)rect
{
	[self updateSmallReticleCameraWarning];
}

- (void)selectAreaView:(SelectAreaView *)view didChangeAreaRect:(CGRect)rect
{
	[self updateSmallReticleCameraWarning];
}

#pragma mark - Popup notification controller delegate methods

- (void)popupNotificationControllerDidLoad:(PopupNotificationController *)popupNotificationController
{
	NSLog(@"popup did load");

	popupNotificationController.switchButton.onTintColor = kTintColorHighlighted;
}

- (void)popupNotificationController:(PopupNotificationController *)popupNotificationController switchValueChanged:(BOOL)switchValue
{
	NSLog(@"switch %@", switchValue ? @"on" : @"off");

	self.showAnglePopupNotification = !switchValue;
}

#pragma mark - User interface methods

- (void)showAngleCorrectionPopupHelper
{
	// Create popup notification controller
	PopupNotificationController *popup = [[PopupNotificationController alloc] initWithSourceView:
										  self.angleCorrectionButton popupSize:kPopupAngleCorrectionSize];

	// Setup popup notification controller
	popup.text = kInfoAngleCorrectionDescription;
	popup.switchLabelText = kPopupAngleCorrectionSwitchText;
	popup.style = PopupNotificationControllerStyleLightContent;
	popup.permittedArrowDirections = (PopupNotificationControllerArrowDirectionDown |
									  PopupNotificationControllerArrowDirectionRight);
	popup.delegate = self;

	// Show popup notification controller
	[self presentViewController:popup animated:YES completion:nil];
}

- (void)flashInfoLabelWithText:(NSString *)text
{
	// Animate info label with flashing effect
	self.infoLabel.text = text;
	[UIView animateWithDuration:kInfoLabelAnimationDuration delay:0.0 options:
	 UIViewAnimationOptionCurveEaseInOut | UIViewAnimationOptionBeginFromCurrentState animations:^{
		 
		 self.infoLabel.alpha = 1.0f;
		 
	 } completion:^(BOOL finished) {
		 [UIView animateWithDuration:kInfoLabelAnimationDuration delay:kInfoLabelAnimationDelay options:
		  UIViewAnimationOptionCurveEaseInOut animations:^{
			  
			  self.infoLabel.alpha = 0.0f;
			  
		  } completion:nil];
	 }];
}

- (void)updateFlashButtonIcon
{
	// Change button tint color according to flash state (on or off)
	self.flashButton.tintColor = (self.flashMode == FMFlashModeOff ? kTintColorNormal : kTintColorHighlighted);

	// Change button image according to auto or manual mode
	[self.flashButton setImage:(self.flashMode == FMFlashModeAuto ?
								[UIImage imageNamed:@"FlashIconAuto"] :
								[UIImage imageNamed:@"FlashIcon"])
					  forState:UIControlStateNormal];
}

- (void)updateAngleCorrectionButtonIcon
{
	// Change button tint color according to angle correction state (on or off)
	self.angleCorrectionButton.tintColor = (self.angleCorrection ? kTintColorHighlighted : kTintColorNormal);
}

- (void)updateCameraWarningText
{
	// Generate the array of warnings messages
	NSMutableArray *warningArray = [[NSMutableArray alloc] init];

	// Small reticle warning
	if (self.smallReticleCameraWarning) {
		[warningArray addObject:kWarningSmallReticleArea];
	}

	// Zoom factor camera warning
	if (self.zoomCameraWarning) {
		[warningArray addObject:kWarningHighZoomFactor];
	}

	// Steep device angle warning
	if (self.steepAngleCameraWarning) {
		[warningArray addObject:kWarningSteepDeviceAngle];
	}

	NSLog(@"update camera warning text: %@", warningArray);

	// Set select area view grid and font color depending on number of warnings
	self.selectAreaView.gridColor = (warningArray.count > 0 ? kSelecAreaViewWarningColor : kSelecAreaViewDefaultColor);
	self.selectAreaView.fontColor = self.selectAreaView.gridColor;

	// Set warning text
	if (warningArray.count > 0) {
		NSMutableString *allWarnings = [[NSMutableString alloc] init];

		for (NSString *warning in warningArray) {

			// Check if it is the string word in the array
			if (warning == warningArray.firstObject) {

				// Capitalize first letter
				[allWarnings appendFormat:@"%@%@", [[warning substringToIndex:1] uppercaseString],
				 [warning substringFromIndex:1]];
			} else {

				// Add connection string correspondingly – "or" for last string, "," for other strings
				[allWarnings appendFormat:@"%@%@", (warning == warningArray.lastObject ? @" or " : @", "), warning];
			}
		}

		// Set select area text with all the warnings
		self.selectAreaView.bottomText = [[NSString stringWithFormat:kWarningMessageFormat, allWarnings]
										  splitStringByWordsWithSet:[NSCharacterSet whitespaceCharacterSet]
										  splitString:@"\n" maxSplitStringLength:kWarningMessageMaxLineLength];
	} else {

		// No warnings – remove any text
		self.selectAreaView.bottomText = nil;
	}
}

#pragma mark - Timer methods

- (void)autoFocusTimerTriggered:(NSTimer *)timer
{
	NSError *error = nil;

	// Enable video device continous auto-focus and auto-exposure
	if ([self.videoDevice isFocusModeSupported:AVCaptureFocusModeContinuousAutoFocus] &&
		[self.videoDevice isExposureModeSupported:AVCaptureExposureModeContinuousAutoExposure]) {
		if ([self.videoDevice lockForConfiguration:&error]) {
			
			NSLog(@"back to continous auto-focus and auto-exposure");
			
			// Set video device focus mode to auto-focus and auto-exposure
			[self.videoDevice setFocusMode:AVCaptureFocusModeContinuousAutoFocus];
			[self.videoDevice setExposureMode:AVCaptureExposureModeContinuousAutoExposure];
			[self.videoDevice unlockForConfiguration];
			
		} else {
			
			NSLog(@"an error occurred while trying to lock the video device for auto-focus: %@", error);
		}
	} else {
		
		NSLog(@"video device does not supper auto-focus or auto-exposure");
	}
	
	// Dealloc the timer
	self.autoFocusTimer = nil;
}

#pragma mark - Helper methods

+ (AVCaptureDevice *)deviceWithMediaType:(NSString *)mediaType preferringPosition:(AVCaptureDevicePosition)position
{
	NSArray *devices = [AVCaptureDevice devicesWithMediaType:mediaType];
	AVCaptureDevice *captureDevice = devices.firstObject;
	
	for (AVCaptureDevice *device in devices) {
		if (device.position == position) {
			captureDevice = device;
			break;
		}
	}
	return captureDevice;
}

- (void)setPreviewLayerOutputOrientation
{
	// Use the status bar orientation as video orientation
	UIInterfaceOrientation statusBarOrientation = [[UIApplication sharedApplication] statusBarOrientation];
	AVCaptureVideoOrientation videoOrientation = AVCaptureVideoOrientationPortrait;
	if (statusBarOrientation != UIInterfaceOrientationUnknown) {
		videoOrientation = (AVCaptureVideoOrientation) statusBarOrientation;
	}
	
	// Set new video orientation
	[[(AVCaptureVideoPreviewLayer *) self.previewView.layer connection] setVideoOrientation:videoOrientation];
}

- (void)updateSmallReticleCameraWarning
{
	// Get select area view scale proportionally to the camera resolution
	CGFloat imageToPreviewViewScale = self.cameraResolution.width / CGRectGetWidth(self.previewView.bounds);
	if (CGRectGetHeight(self.previewView.bounds) * imageToPreviewViewScale <= self.cameraResolution.height) {
		imageToPreviewViewScale = 1.0f / imageToPreviewViewScale;
	} else {
		imageToPreviewViewScale = 1.0f / (self.cameraResolution.height / CGRectGetHeight(self.previewView.bounds));
	}

	const float currentWidth = self.selectAreaView.areaRect.size.width / imageToPreviewViewScale;
	const float currentHeight = self.selectAreaView.areaRect.size.height / imageToPreviewViewScale;

	self.smallReticleCameraWarning = (currentHeight < kCameraMinResolutionWarning &&
									  currentWidth < kCameraMinResolutionWarning);
}

- (void)captureStillImage
{
	dispatch_async(self.cameraSessionQueue, ^{
		
		AVCaptureConnection *connection = [self.stillCameraOutput connectionWithMediaType:AVMediaTypeVideo];
		
		// Check is a connecion was successfully established
		if (!connection) {
			dispatch_async(dispatch_get_main_queue(), ^{
				UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"An error has "
													  "occurred" message:@"Could not establish camera output "
													  "connection. It appears that this device has no rear "
													  "camera." preferredStyle:UIAlertControllerStyleAlert];
				[alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
																  handler:nil]];
				[self presentViewController:alertController animated:YES completion:nil];
                
                // Enable controls view user interaction and start motion manager capturing
                self.controlsView.userInteractionEnabled = YES;
                [self.motionManager startDeviceMotionUpdates];
			});
			return;
		}
		
		// Update the video orientation to the device on
		connection.videoOrientation = (AVCaptureVideoOrientation) [[UIApplication sharedApplication]
																   statusBarOrientation];
		
		[self.stillCameraOutput captureStillImageAsynchronouslyFromConnection:connection completionHandler:
		 ^(CMSampleBufferRef imageDataSampleBuffer, NSError *error) {
			 
			 if (!error) {
				 
				 // Get image data and metadata
				 NSData *imageData = [AVCaptureStillImageOutput jpegStillImageNSDataRepresentation:
									  imageDataSampleBuffer];
				 UIImage *image = [self croppedImageFromSelectedArea:[UIImage imageWithData:imageData]];

				 dispatch_async(dispatch_get_main_queue(), ^{
				 
					 // Start image processing
					 [self startOpticalCharacterRecognitionProcessWithImage:image];
				 });
			 } else {
				 
				 // An error has occurred while capturing the image
				 NSLog(@"an error occurred while capturing still image: %@", error);

				 dispatch_async(dispatch_get_main_queue(), ^{
					 UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"An error has "
														   "occurred" message:@"Could not establish camera output "
														   "connection. It appears that this device has no rear "
														   "camera." preferredStyle:UIAlertControllerStyleAlert];
					 [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
																	   handler:nil]];
					 [self presentViewController:alertController animated:YES completion:nil];
                     
                     // Enable controls view user interaction and start motion manager capturing
                     self.controlsView.userInteractionEnabled = YES;
                     [self.motionManager startDeviceMotionUpdates];
				 });
			 }
		 }];
	});
}

- (UIImage *)croppedImageFromSelectedArea:(UIImage *)image
{
	// Sanity check
	if (!image) {
		return nil;
	}
	
	// Get image pixel size
	CGSize imageSize = CGSizeMake(image.size.width * image.scale, image.size.height * image.scale);

	NSLog(@"image size: %@", NSStringFromCGSize(imageSize));
	
	// Get select area view scale proportionally to the image
	CGFloat imageToPreviewViewScale = imageSize.width / CGRectGetWidth(self.previewView.bounds);
	if (CGRectGetHeight(self.previewView.bounds) * imageToPreviewViewScale <= imageSize.height) {
		imageToPreviewViewScale = 1.0f / imageToPreviewViewScale;
	} else {
		imageToPreviewViewScale = 1.0f / (imageSize.height / CGRectGetHeight(self.previewView.bounds));
	}
	
	NSLog(@"preview view size: %@", NSStringFromCGSize(self.previewView.bounds.size));
	NSLog(@"image scaled size: [%.03f] %@", imageToPreviewViewScale,
		  NSStringFromCGSize(CGSizeMake(imageSize.width * imageToPreviewViewScale,
										imageSize.height * imageToPreviewViewScale)));

	// Get image crop offset and rectange
	CGPoint cropOffset = CGPointMake((imageSize.width - self.previewView.bounds.size.width /
									  imageToPreviewViewScale) / 2.0f,
									 (imageSize.height - self.previewView.bounds.size.height /
									  imageToPreviewViewScale) / 2.0f);
	CGRect cropRect = CGRectMake(cropOffset.x + (self.selectAreaView.areaRect.origin.x +
												 self.selectAreaView.frame.origin.x) / imageToPreviewViewScale,
								 cropOffset.y + (self.selectAreaView.areaRect.origin.y +
												 self.selectAreaView.frame.origin.y) / imageToPreviewViewScale,
								 self.selectAreaView.areaRect.size.width / imageToPreviewViewScale,
								 self.selectAreaView.areaRect.size.height / imageToPreviewViewScale);
	
	// Return final cropped image
	return [image.rasterizedImageOrientation cropImageRect:cropRect];
}

#pragma mark - View controller's memory management methods

- (void)didReceiveMemoryWarning
{
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
	
	NSLog(@"memory warning received");
}

@end
