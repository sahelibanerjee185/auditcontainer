//
//  OpticalCharacterRecognitionCameraController.h
//  Optical Character Recognition
//
//  Created by Iulian Corcoja on 1/22/16.
//  Copyright © 2016 Iulian Corcoja. All rights reserved.
//

#import "OpticalCharacterRecognitionController.h"

@interface OpticalCharacterRecognitionCameraController : UIViewController

#pragma mark - View controller's lifecycle methods

+ (instancetype)initFromNib;

@end
