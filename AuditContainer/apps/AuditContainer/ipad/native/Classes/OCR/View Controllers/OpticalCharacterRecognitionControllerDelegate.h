//
//  OpticalCharacterRecognitionControllerDelegate.h
//  Optical Character Recognition Controller Delegate
//
//  Created by Iulian Corcoja on 3/30/16.
//
//

#import <Foundation/Foundation.h>

@class OpticalCharacterRecognitionController;

@protocol OpticalCharacterRecognitionControllerDelegate <NSObject>

@optional

- (void)opticalCharacterRecognitionController:(OpticalCharacterRecognitionController *)controller
				 didFinishRecognitionForImage:(UIImage *)image
							   recognizedText:(NSString *)text
									  options:(NSDictionary *)options;

- (void)opticalCharacterRecognitionControllerDidCancel:(OpticalCharacterRecognitionController *)controller;

@end
