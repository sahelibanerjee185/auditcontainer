//
//  Footer.h
//  AuditContainerAuditContainerIpad
//
//  Created by Iulian Corcoja on 11/2/15.
//
//

#import <UIKit/UIKit.h>
#import "FooterDelegate.h"

#pragma mark - Type defines

#define kDefaultSelectedButtonTag 1

typedef NS_ENUM(NSUInteger, ToolbarButtonTag) {
	kToolbarButtonInspectionsTag		= 1,
	kToolbarButtonPlanVisitTag			= 2,
	kToolbarButtonManualsButtonTag		= 3,
	kToolbarButtonKpiButtonTag			= 4,
	kToolbarButtonLogOutButtonTag		= 5,
	kToolbarButtonsCount
};

#pragma mark - View controller interface

@interface Footer : UIView

@property (nonatomic, weak) IBOutlet UIButton *inspectionsButton;
@property (nonatomic, weak) IBOutlet UIButton *planVisitButton;
@property (nonatomic, weak) IBOutlet UIButton *manualsButton;
@property (nonatomic, weak) IBOutlet UIButton *kpiButton;
@property (nonatomic, weak) IBOutlet UIButton *logOutButton;

@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *buttons;

@property (nonatomic, weak) id<FooterDelegate> delegate;

#pragma mark - Class methods

+ (id)initFromNib;

#pragma mark - Footer instace methods

- (void)switchToButtonWithIndex:(NSInteger)idx;

@end
