//
//  HTMLtoPDF.h
//  VesselAuditVesselAuditIpad
//
//  Created by Iulian Corcoja on 1/14/16.
//
//

#import <Foundation/Foundation.h>

@interface HTMLtoPDF : NSObject

@property (nonatomic, strong) NSMutableData *pdfData;

#pragma mark - Singleton getter

+ (HTMLtoPDF *)sharedInstance;

#pragma mark - Instance methods

- (void)pdfFromHTMLString:(NSString *)html completion:(void (^)(BOOL success, NSData *pdfData))completion;

@end
