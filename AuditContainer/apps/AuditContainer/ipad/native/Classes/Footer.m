//
//  Footer.m
//  AuditContainerAuditContainerIpad
//
//  Created by Iulian Corcoja on 11/2/15.
//
//

#import "Footer.h"
#import "Constants.h"

#pragma mark - View interface

@interface Footer ()

@property (nonatomic, strong) NSLayoutConstraint *heightConstraint;

@end

@implementation Footer

#pragma mark - View lifecycle methods

+ (id)initFromNib
{
	Footer *footer = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass(self) owner:nil options:nil] firstObject];
	
	// Make sure footer view was initialized properly
	if (footer && [footer isKindOfClass:[Footer class]]) {
		
		// Remove auto-created layout constraints
		footer.translatesAutoresizingMaskIntoConstraints = NO;
		
		// Set default selected start button
		for (UIButton *button in footer.buttons) {
			button.selected = (button.tag == kDefaultSelectedButtonTag);
		}
		
		// Return footer
		return footer;
	}
	return nil;
}

- (instancetype)initWithCoder:(NSCoder *)coder
{
	self = [super initWithCoder:coder];
	if (self) {
		// Custom initialization...
	}
	return self;
}

#pragma mark - Auto layout methods

- (void)layoutSubviews
{
	self.heightConstraint.constant = kFooterHeight;
	
	// Call default implementation of layout subviews
	[super layoutSubviews];
}

#pragma mark - Footer instace methods

- (void)switchToButtonWithIndex:(NSInteger)idx
{
	if (idx < 1 || idx >= kToolbarButtonsCount) {
		
		// Index not a valid one
		return;
	}
	
	for (UIButton *button in self.buttons) {
		button.selected = (button.tag == idx);
	}
}

#pragma mark - Actions and gesture recognizers

- (IBAction)buttonTouchEvent:(UIButton *)sender
{
	BOOL callDelegate = (sender.selected == NO);
	
	if (sender.tag != kToolbarButtonLogOutButtonTag) {
		for (UIButton *button in self.buttons) {
			
			// Select current button and deselect other buttons
			button.selected = (button == sender);
		}
	}
	
	// Call delegate method depending on tapped button
	if (callDelegate && self.delegate && [self.delegate respondsToSelector:@selector(footerView:toolButtonTouchEvent:
																					 withTag:)]) {
		
		[self.delegate footerView:self toolButtonTouchEvent:sender withTag:sender.tag];
	}
}

#pragma mark - View delegate methods

- (void)didMoveToSuperview
{
	[super didMoveToSuperview];

	// Add new constraints
	self.heightConstraint = [NSLayoutConstraint constraintWithItem:self attribute:NSLayoutAttributeHeight relatedBy:
							 NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0
														  constant:kFooterHeight];
	[self.superview addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[view]|" options:0 metrics:
									nil views:@{@"view" : self}]];
	[self.superview addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[view]|" options:0 metrics:
									nil views:@{@"view" : self}]];
	[self.superview addConstraint:self.heightConstraint];
	
	NSLog(@"footer moved to a new superview - add new constraints");
}

@end

