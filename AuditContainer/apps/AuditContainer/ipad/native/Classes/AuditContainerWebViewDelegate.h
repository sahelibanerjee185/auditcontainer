//
//  AuditContainerWebViewDelegate.h
//  AuditContainerAuditContainerIpad
//
//  Created by Iulian Corcoja on 12/21/15.
//
//

#import <Foundation/Foundation.h>

@class AuditContainerWebView;

@protocol AuditContainerWebViewDelegate <NSObject>

@optional

- (void)auditContainerWebView:(AuditContainerWebView *)webView panGestureOutsideBounds:(CGPoint)touchLocation;

@end
