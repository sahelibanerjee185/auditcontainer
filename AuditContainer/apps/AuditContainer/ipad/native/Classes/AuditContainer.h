//
//  MyAppDelegate.h
//
//

#import <IBMMobileFirstPlatformFoundationHybrid/IBMMobileFirstPlatformFoundationHybrid.h>
#import <Cordova/CDVViewController.h>
#import <AppConnect/AppConnect.h>

@interface MyAppDelegate : WLAppDelegate <WLInitWebFrameworkDelegate, AppConnectDelegate>

@end
