//
//  HeaderDelegate.h
//  AuditContainerAuditContainerIpad
//
//  Created by Iulian Corcoja on 11/2/15.
//
//

#import <Foundation/Foundation.h>

@class Header;

@protocol HeaderDelegate <NSObject>

@optional

- (void)headerViewTapEvent:(Header *)header;

- (void)headerView:(Header *)header backTouchEvent:(UIButton *)sender;

- (void)headerView:(Header *)header syncTouchEvent:(UIButton *)sender;

@end

