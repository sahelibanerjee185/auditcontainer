//
//  AuditContainerWebView.h
//  AuditContainerAuditContainerIpad
//
//  Created by Iulian Corcoja on 12/18/15.
//
//

#import <UIKit/UIKit.h>
#import "AuditContainerWebViewDelegate.h"

@interface AuditContainerWebView : UIWebView <UIGestureRecognizerDelegate>

@property (nonatomic, weak) id<AuditContainerWebViewDelegate> auditContainerWebViewDelegate;

@end
