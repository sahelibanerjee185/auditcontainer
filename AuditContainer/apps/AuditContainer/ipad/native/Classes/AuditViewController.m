//
//  AuditViewController.m
//  AuditContainerAuditContainerIpad

#import "AuditViewController.h"
#import <PhotoViewController/PhotoViewController-Swift.h>
#import "HTMLtoPDF.h"
#import "Constants.h"
#import "MDMInfo.h"
#import "ContainerAudit-Swift.h"
#import "IntuneMAMEnrollmentManager.h"
#import "IntuneMAMPolicyManager.h"
#import "IntuneMAM.h"

#define kMaxImageSize 524288 // Maximum compressed image size in bytes

#define kImageBoundingFrameSize 2048.0

#define kImageDownscaleRatio 0.5

#define kImageDownscaleMaxIterations 24

@interface AuditViewController ()

@property (nonatomic, strong) Header *headerBar;
@property (nonatomic, strong) Footer *footerBar;
@property (nonatomic, strong) NSString *pdfFilePath;
@property (nonatomic, strong) NSString *pdfFileName;
@property (nonatomic, strong) NSString *pdfDirPath;
@property (nonatomic, strong) NSString *documentsPath;
@property (nonatomic, strong) NSString *ocrImageStorePath;
@property (nonatomic, strong) NSTimer *dateTimer;

@property (nonatomic, assign) UIInterfaceOrientation lastOrientation;

@end

@implementation AuditViewController

@dynamic webView;

#pragma mark - View controller lifecycle methods

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
	self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
	
	if (self) {
		if ([self respondsToSelector:@selector(setAutomaticallyAdjustsScrollViewInsets:)]) {
			self.automaticallyAdjustsScrollViewInsets = NO;
		}
	}
	return self;
}

- (void)viewDidLoad
{
	[super viewDidLoad];
	
	// Set worklight action receiver
	[[WL sharedInstance] addActionReceiver:self];
	
	// Initially, navigation bar and toolbar are hidden, so:
	// 1. Hide header and footer bars
	// 2. Set their delegates
	self.headerBar.hidden = YES;
	self.footerBar.hidden = YES;
	self.headerBar.delegate = self;
	self.footerBar.delegate = self;
    
    // Set default last view controller orientation
    self.lastOrientation = [[UIApplication sharedApplication] statusBarOrientation];
	
	// Set header current date
	// NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
	// dateFormatter.dateFormat = @"MMMM d, yyyy";
	// self.headerBar.dateLabel.text = [dateFormatter stringFromDate:[NSDate date]];
	
	// Create a timer that fires every second repeatedly for updating app time
    self.dateTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(updateLabel) userInfo:
					  nil repeats:YES];

	// Set web view delegate
	self.webView.scrollView.delegate = self;

	// Set web view properties
	self.webView.backgroundColor = [UIColor whiteColor];
	self.webView.scrollView.backgroundColor = [UIColor whiteColor];
	self.webView.scrollView.scrollEnabled = NO;
	self.webView.scrollView.bounces = NO;
	self.webView.auditContainerWebViewDelegate = self;
	[self updateWebViewContentInset];
	
	// Connect to MFP servers at startup
	[[WLClient sharedInstance] wlConnectWithDelegate:self];
}

- (void)updateLabel {
    
    // Set header current date
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MMMM d, yyyy"];
    
    self.headerBar.dateLabel.text = [dateFormatter stringFromDate:[NSDate date]];
}

- (void)viewDidAppear:(BOOL)animated
{
	[super viewDidAppear:animated];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(orientationChanged:)
                                                 name:UIDeviceOrientationDidChangeNotification
                                               object:nil];
	
	NSLog(@"%@", self.webView);
}

#pragma mark - Setters and getters

- (Header *)headerBar
{
	// Lazy instantiation
	if (!_headerBar) {
		
		// Create header
		_headerBar = [Header initFromNib];
		
		// Add header to the main view
		[self.view addSubview:_headerBar];
		[self.view bringSubviewToFront:_headerBar];
	}
	return _headerBar;
}

- (Footer *)footerBar
{
	// Lazy instantiation
	if (!_footerBar) {
		
		// Create footer
		_footerBar = [Footer initFromNib];
		
		// Add footer to the main view
		[self.view addSubview:_footerBar];
		[self.view bringSubviewToFront:_footerBar];
	}
	return _footerBar;
}

- (void)setLastOrientation:(UIInterfaceOrientation)lastOrientation
{
    if (lastOrientation == UIInterfaceOrientationLandscapeLeft ||
        lastOrientation == UIInterfaceOrientationLandscapeRight) {
        _lastOrientation = lastOrientation;
    }
}

#pragma mark - Mobile first platform action and delegate methods

- (void)onActionReceived:(NSString *)action withData:(NSDictionary *)data
{
	// Execute method call (that potentially may have UI changes) on main thread
	dispatch_async(dispatch_get_main_queue(), ^{
		
		NSLog(@"action received: %@\n%@", action, data);
        
        if ([action isEqualToString:@"getMAMInfo"]) {
            
            //Reference for Intune: https://docs.microsoft.com/en-us/intune/app-sdk-ios
            //NOTE!! In the app's Info.plist I have set
            //MAMPolicyRequired= NO (Otherwise it will require the app to be registered in the Intune portal)
            //AutoEnrollOnLaunch= YES
            
            
            
            NSString* enrolledAccount = [[IntuneMAMEnrollmentManager instance] enrolledAccount];
            NSLog(@"(Native log) MAM enrolled account: %@", enrolledAccount);
            
            NSString* primaryUser = [[IntuneMAMPolicyManager instance] primaryUser];
            NSLog(@"(Native log) MAM primaryUser : %@", primaryUser);
            
            if( [enrolledAccount isKindOfClass:[NSNull class]] || enrolledAccount == nil){
                enrolledAccount = @"";
            }
            if([primaryUser isKindOfClass:[NSNull class]] || primaryUser == nil){
                primaryUser = @"";
            }
            
            [[WL sharedInstance] sendActionToJS:@"MAMUser" withData:@{
                                                                      @"enrolledAccount" : enrolledAccount,
                                                                      @"primaryUser": primaryUser
                                                                      }];
        }
        
        else if ([action isEqualToString:@"getMAMVersion"]) {
            
            //            InTuneInfo *info = [[InTuneInfo alloc] init];
            //            NSInteger * version = [info getMAMVersion];
            NSString *version = [IntuneMAMVersionInfo sdkVersion];
            NSLog(@"(Native log) MAM SDK Version: %@", version);
            [[WL sharedInstance] sendActionToJS:@"MAMVersion" withData:@{@"version" : version}];
        } else if ([action isEqualToString:@"updateNativeUI"]) {
			
			// Update components from navigation bar and toolbar (back button visibility, title, etc.)
            [self updateNativeUI:data];
            
        } else if ([action isEqualToString:@"updateSyncCount"]) {
            
            NSInteger count = [data[@"count"] integerValue];
            
            // Update header sync count label text
            self.headerBar.syncLabelCount.text = (count > 0 ? [NSString stringWithFormat:@"%td", count] : @"");
            
        }else if ([action isEqualToString:@"switchToolbarButton"]) {
			
			// Update current toolbar button
			[self switchToolbarButton:data];

		} else if ([action isEqualToString:@"getFooterInfo"]) {

			// Call hybrid method with footer height as parameter
			[[WL sharedInstance] sendActionToJS:@"setFooterInfo" withData:
			 @{@"height" : @(self.footerBar.hidden ? 0.0f : self.footerBar.frame.size.height)}];
            
        } else if ([action isEqualToString:@"setNativeState"]) {
            
            // Set application UI state
            [self setUIState:data];
            
        } else if ([action isEqualToString:@"getDeviceUDID"]) {
            
            [[WL sharedInstance] sendActionToJS:@"deviceUDID" withData:
             @{@"udid" : [[UIDevice currentDevice].identifierForVendor UUIDString]}];
            
        } else if ([action isEqualToString:@"displayPhotoViewer"]) {
            
            // Show photo view controller
            [self showPhotoViewController:data];
        } else if ([action isEqualToString:@"openOCR"]) {
            
            // Get image path
            self.ocrImageStorePath = data[@"path"];
            
            // Show optical character recognition controller
            [self presentOpticalCharacterRecognitionController];
            
        } else if ([action isEqualToString:@"html2pdf"]) {
            
            NSString *documentsPath = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
            NSString *pdfFolderPath = [documentsPath stringByAppendingPathComponent:data[@"pdfPath"]];
            NSString *fullPdfPath = [pdfFolderPath stringByAppendingPathComponent:data[@"pdfName"]];
            
            NSError *error = nil;
            NSFileManager *fileManager = [NSFileManager defaultManager];
            
            BOOL isDir;
            if (! [fileManager fileExistsAtPath:pdfFolderPath isDirectory:&isDir]) {
                BOOL success = [fileManager createDirectoryAtPath:pdfFolderPath withIntermediateDirectories:YES attributes:nil error:&error];
                if (!success || error) {
                    NSLog(@"Error: %@", [error localizedDescription]);
                }
                NSAssert(success, @"Failed to create folder at path:%@", pdfFolderPath);
            }
            
            NSLog(@"pdf data, path, name: %@", data);
            
            // Generate PDF in Documents/PdfReports folder
            [[HTMLtoPDF sharedInstance] pdfFromHTMLString:data[@"html"] completion:^(BOOL success, NSData *pdfData) {
                
                NSLog(@"%@ - %p", success ? @"success" : @"failure", pdfData);
                
                // Write PDF to temporary file
                [pdfData writeToFile:fullPdfPath atomically:YES];
                
                NSLog(@"pdf file path: %@", fullPdfPath);
                
                [[WL sharedInstance] sendActionToJS:@"pdfRenderingDone" withData:
                 @{@"success" : @(YES),
                   @"path" : fullPdfPath}];
            }];

        } else if([action isEqualToString:@"previewPdf"]){
            NSString *documentsPath = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
            
            // Should trigger pdf preview
            NSLog(@"Should trigger pdf preview");
            
            // Get pdf file path
            self.pdfFilePath = [documentsPath stringByAppendingPathComponent:data[@"pdfPath"]];
            self.pdfFileName = @"Audit Container Report";
            
            if (data[@"pdfName"] != nil) {
                self.pdfFileName = data[@"pdfName"];
            }
        
            PDFPreviewItem *pdfItem = [[PDFPreviewItem alloc] initWithURL:[NSURL fileURLWithPath:self.pdfFilePath]
                                                                 andTitle:self.pdfFileName];

            // Test if the current PDF document can be opened
            if ([QLPreviewController canPreviewItem:pdfItem]) {
                
                // Document can be opened - create preview controller
                QLPreviewController *previewController = [[QLPreviewController alloc] init];
                previewController.delegate = self;
                previewController.dataSource = self;
                
                // Show preview view controller
                [self.navigationController presentViewController:previewController animated:YES completion: ^{
                    [[WL sharedInstance] sendActionToJS:@"pdfRenderingDone" withData:
                     @{@"success" : @(YES)}];
                }];
                
            } else {
                NSLog(@"document cannot be opened");
            }
        } else if([action isEqualToString:@"deletePdf"]) {
            
            // Should delete the pdf from tmp folder
            NSLog(@"Should delete the pdf from tmp folder");
            
            // Get pdf file path
            NSString *path = data[@"pdfPath"];
            
            // Sanity check
            if (path && path.length > 0) {
                
                // Delete PDF file
                NSError *error = nil;
                [[NSFileManager defaultManager] removeItemAtURL:[NSURL fileURLWithPath:path] error:&error];

                // Check for errors
                if (error) {
                    NSLog(@"an error occurred while deleting PDF file: %@", error.localizedDescription);
                }
            }
		} else if ([action isEqualToString:@"getMDMInfo"]) {

			// Get MDM config information, specifically user ID and email, and send them back to hybrid
			[MDMInfo getMMDInfo:^(NSDictionary *config) {

                [[WL sharedInstance] sendActionToJS:@"MDMInfo" withData:
                 @{@"userId" : (config && config[@"User"] ? config[@"User"] : [NSNull null]),
                   @"email" : (config && config[@"Email"] ? config[@"Email"] : [NSNull null])}];
			}];
        } else if ([action isEqualToString:@"syncCompleted"]){
            self.headerBar.syncing = NO;
        } else if ([action isEqualToString:@"syncStart"]){
            self.headerBar.syncing = YES;
        }
	});
}

#pragma mark - Audit container web view delegate methods

- (void)auditContainerWebView:(AuditContainerWebView *)webView panGestureOutsideBounds:(CGPoint)touchLocation
{
	NSLog(@"touch location: %@", NSStringFromCGPoint(touchLocation));
	
	// Call hybrid
	[[WL sharedInstance] sendActionToJS:@"panGestureOutsideBoundries" withData:
	 @{@"x" : @(touchLocation.x),
	   @"y" : @(touchLocation.y),
	   @"scaleFactor" : @([[UIScreen mainScreen] scale])}];
}

#pragma mark - Header delegate methods

- (void)headerView:(Header *)header backTouchEvent:(UIButton *)sender
{
	NSLog(@"backTouchEvent:");
	
	// Call hybrid back button touch event
	[[WL sharedInstance] sendActionToJS:@"backButtonClicked" withData:nil];
}

- (void)headerView:(Header *)header syncTouchEvent:(UIButton *)sender
{
	NSLog(@"syncTouchEvent:");

	// Get selected toolbar button index
	int idx = 0;
	for (int i = 0; i < self.footerBar.buttons.count; i++) {
		if ([((UIButton *) self.footerBar.buttons[i]) isSelected]) {
			idx = i;
			break;
		}
	}

	// Create data dictionary
	NSDictionary *data = @{@"toolbarButtonIndex" : @(idx)};
    
    self.headerBar.syncing = YES;
    
	// Call hybrid sync button touch event
	[[WL sharedInstance] sendActionToJS:@"syncButtonClicked" withData:data];

	NSLog(@"data: %@", data);
}

- (void)headerViewTapEvent:(Header *)header
{
	NSLog(@"headerViewTapEvent:");
	
	// Hide keyboard
	[self.view endEditing:YES];
}

#pragma mark - Footer delegate methods

- (void)footerView:(Footer *)footer toolButtonTouchEvent:(UIButton *)sender withTag:(NSUInteger)tag
{
	NSLog(@"toolbar button with id: %tu touch event", tag);

	// Call hybrid button touch event depending on tapped button
	switch (tag) {
		case kToolbarButtonInspectionsTag: {
			[[WL sharedInstance] sendActionToJS:@"surveyTab" withData:nil];
			break;
		}
		case kToolbarButtonPlanVisitTag: {
			[[WL sharedInstance] sendActionToJS:@"planVisitTab" withData:nil];
			break;
		}
		case kToolbarButtonManualsButtonTag: {
			[[WL sharedInstance] sendActionToJS:@"manualsTab" withData:nil];
			break;
		}
		case kToolbarButtonKpiButtonTag: {
			[[WL sharedInstance] sendActionToJS:@"kpiTab" withData:nil];
			break;
		}
		case kToolbarButtonLogOutButtonTag: {
			NSLog(@"log out from the app");
			
			// Call hybrid log out button touch event
			[[WL sharedInstance] sendActionToJS:@"logOutTab" withData:nil];
			
			break;
		}
	}
}

#pragma mark - User interface methods

- (void)updateNativeUI:(NSDictionary *)data
{
	NSLog(@"update native UI - header and footer bar");
	
	BOOL backButtonVisible = [data[@"showBackButton"] boolValue];
	BOOL refreshButtonVisible = [data[@"showRefreshButton"] boolValue];
	BOOL headerBarVisible = [data[@"showHeader"] boolValue];
	BOOL footerBarVisible = [data[@"showFooter"] boolValue];
	NSString *title = data[@"title"];
	
	NSLog(@"native title %@", title);

	// Update navigation bar and toolbar visibility
	self.headerBar.backButton.hidden = !backButtonVisible;
	self.headerBar.syncButtonVisible = refreshButtonVisible;
	self.headerBar.hidden = !headerBarVisible;
	self.footerBar.hidden = !footerBarVisible;
	
	// Update header bar title (only if this one is available and not nil)
	if (title.length > 0) {
		self.headerBar.titleLabel.text = title;
	}

	// Update web view content insets
	[self updateWebViewContentInset];
}

- (void)switchToolbarButton:(NSDictionary *)data
{	
	// Switch to new button (received index) in the toolbar
	NSInteger buttonIdx = [data[@"toolbarButtonIndex"] integerValue];
	[self.footerBar switchToButtonWithIndex:buttonIdx];
}

- (void)setUIState:(NSDictionary *)data
{
    BOOL disabledUI = [data[@"disabled"] boolValue];
    
    // Set UI disable state
    self.headerBar.userInteractionEnabled = !disabledUI;
    self.footerBar.userInteractionEnabled = !disabledUI;
}

- (void)showPhotoViewController:(NSDictionary *)data
{
    // Sanity checks
    if (!data || !data[@"photos"] || ![data[@"photos"] isKindOfClass:[NSArray class]]) {
        return;
    }
    
    // Extract current photo index
    NSUInteger index = 0;
    
    if (data[@"index"] && [data[@"index"] isKindOfClass:[NSNumber class]]) {
        index = [data[@"index"] unsignedIntegerValue];
    }
    
    // Extract photos from the dictionary
    NSMutableArray *photos = [[NSMutableArray alloc] initWithCapacity:[data[@"photos"] count]];
    
    for (NSDictionary *photo in data[@"photos"]) {
        
        // Remove file path prefix: "file://"
        NSString *path = photo[@"path"];
        NSRange prefixRange = [path rangeOfString:@"file://"];

        if (prefixRange.location != NSNotFound) {
            path = [path stringByReplacingCharactersInRange:prefixRange withString:@""];
        }
        
        // Extract the data
        UIImage *photoImage = [UIImage imageWithContentsOfFile:path];
        NSString *imageTitle = path.lastPathComponent.stringByDeletingPathExtension;
        NSString *imageSubtitle = photo[@"date"];
        
        // Create new photo type and fill it with the corresponding data
        PhotoType *photoType = [[PhotoType alloc] initWithImage:photoImage
                                                          title:imageTitle
                                                       subtitle:imageSubtitle
                                               photoDescription:@""];
        [photos addObject:photoType];
    }
    
    // Create and configure photo view controller
    PhotoViewController *photoViewController = [[PhotoViewController alloc] initWithPhotos:photos
                                                                             startingIndex:index
                                                                              subtitleIcon:nil];
    photoViewController.modalPresentationStyle = UIModalPresentationFormSheet;
    
    // Set photo view controller modal view size
    if ([photoViewController respondsToSelector:@selector(setPreferredContentSize:)]) {
        [photoViewController setPreferredContentSize:CGSizeMake(self.view.bounds.size.width * 0.80f,
                                                                self.view.bounds.size.height * 0.85f)];
    }
    
    // Show photo view controller
    [self.navigationController presentViewController:photoViewController animated:YES completion:nil];
}

- (void)presentOpticalCharacterRecognitionController
{
    OpticalCharacterRecognitionController *ocrController = [[OpticalCharacterRecognitionController alloc] init];
    ocrController.recognitionDelegate = self;
    
    [self.navigationController presentViewController:ocrController animated:YES completion:nil];
}

- (void)updateWebViewContentInset
{
	// Set web view edge insets
	self.webView.scrollView.contentInset =
	UIEdgeInsetsMake(self.headerBar.hidden ? 0.0f : self.headerBar.frame.size.height, 0.0f,
					 self.footerBar.hidden ? 0.0f : self.footerBar.frame.size.height, 0.0f);

	NSLog(@"%@", NSStringFromUIEdgeInsets(self.webView.scrollView.contentInset));
}

#pragma mark - Worklight client delegate methods

- (void)onSuccess:(WLResponse *)response
{
	NSLog(@"worklight connected to the server with success: %@", response);
}

- (void)onFailure:(WLFailResponse *)response
{
	NSLog(@"worklight could not connect to the server: %@", response);
}

#pragma mark - View controller delegate methods

- (BOOL)prefersStatusBarHidden
{
    return false;
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskLandscape;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    return self.lastOrientation;
}

#pragma mark - Web view delegate mathods

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView
{
	// No view - no scrolling, mhmmm...
	return nil;
}

#pragma mark - Preview controller data source methods

- (NSInteger)numberOfPreviewItemsInPreviewController:(QLPreviewController *)controller
{
	return 1;
}

- (id<QLPreviewItem>)previewController:(QLPreviewController *)controller previewItemAtIndex:(NSInteger)index
{
    return [[PDFPreviewItem alloc] initWithURL:[NSURL fileURLWithPath:self.pdfFilePath] andTitle:
            self.pdfFileName];
}

#pragma mark - Optical recognition controller delegate methods

- (void)opticalCharacterRecognitionControllerDidCancel:(OpticalCharacterRecognitionController *)controller
{
    // Dismiss recognition controller
    [controller dismissViewControllerAnimated:YES completion:nil];
    
    // Call hybrid cancel optical character recognition process
    [[WL sharedInstance] sendActionToJS:@"cancelOCR"];
}

- (void)opticalCharacterRecognitionController:(OpticalCharacterRecognitionController *)controller didFinishRecognitionForImage:(UIImage *)image recognizedText:(NSString *)text options:(NSDictionary *)options
{
    NSString *containerId = [ContainerIDString getContainerIDFromString:text];
    
    NSLog(@"optical character recognition options: %@", options);
    NSLog(@"optical character recognition result: %@", containerId);
    
    // Save image to received path
    if (self.ocrImageStorePath) {
        
        // Downscale the image first
        UIImage *scaledImage = image;
        if (image.size.width > kImageBoundingFrameSize && image.size.height > kImageBoundingFrameSize) {
            if (image.size.width > image.size.height) {
                scaledImage = [image resizeByWith:kImageBoundingFrameSize];
            } else {
                scaledImage = [image resizeByWith:image.size.width * (kImageBoundingFrameSize / image.size.height)];
            }
        }
        
        // Compress the image to not exceed the limit size
        NSData *compressedImage = [scaledImage compressedImageDataToMaxImageRawSize:kMaxImageSize
                                                            maxCompressionIterations:kImageDownscaleMaxIterations
                                                            interationDownscaleRatio:kImageDownscaleRatio];
        
        BOOL result = [compressedImage writeToURL:[NSURL URLWithString:self.ocrImageStorePath] atomically:YES];
        
        if (result == true) {
            NSLog(@"image saved successfully: %@", self.ocrImageStorePath);
            
            // Create data information to be sent to hybrid
            NSDictionary *data = @{@"fileName" : self.ocrImageStorePath.lastPathComponent,
                                   @"text" : containerId,
                                   @"pitch" : options[kOpticalCharacterRecognitionControllerAnglePitch] ?: [NSNull new],
                                   @"roll" : options[kOpticalCharacterRecognitionControllerAngleTilt] ?: [NSNull new],
                                   @"flash" : options[kOpticalCharacterRecognitionControllerFlash] ?: [NSNull new],
                                   @"processingTime" : options[kOpticalCharacterRecognitionControllerProcessingTime] ?: [NSNull new]};
            
            // Call hybrid cancel optical character recognition process
            [[WL sharedInstance] sendActionToJS:@"successOCR" withData:data];
            
        } else {
            NSLog(@"could not save ocr image for the provided image path: %@", self.ocrImageStorePath);
            
            // Call hybrid optical character recognition process failed
            [[WL sharedInstance] sendActionToJS:@"failOCR"];
        }
    } else {
        NSLog(@"image path for ocr image is nil");
        
        // No image path was found - called hybrid optical character recognition process failed
        [[WL sharedInstance] sendActionToJS:@"failOCR"];
    }
    
    // Dismiss recognition controller
    [controller dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Notification center methods

- (void)orientationChanged:(NSNotification *)notification
{
    self.lastOrientation = [[UIApplication sharedApplication] statusBarOrientation];
    
    NSLog(@"new orientation: %ld", self.lastOrientation);
}

#pragma mark - Helper methods

- (NSString *)randomFilePathWithExt:(NSString *)extension
{
        
    // Get temporary folder to set PDF file path
    NSString *temporaryFolder = NSTemporaryDirectory();
    if (!temporaryFolder) {
        
        // If temporary directory does not exist, get documents directory
        temporaryFolder = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
    }
    
    // Append generated file name and return the path
    return [temporaryFolder stringByAppendingFormat:@"%@%@%@", [[NSUUID UUID] UUIDString], extension ? @".": @"", extension ?: @""];
}

#pragma mark - View controller's memory management

- (void)didReceiveMemoryWarning
{
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
	
	NSLog(@"memory warning received");
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIDeviceOrientationDidChangeNotification object:nil];

    [self.dateTimer invalidate];
}

@end
