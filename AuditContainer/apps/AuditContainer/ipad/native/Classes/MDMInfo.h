//
//  MDMInfo.h
//  DepotAuditDepotAuditIpad
//
//  Created by Iulian Corcoja on 7/27/16.
//
//

#import <Foundation/Foundation.h>

#pragma mark - Constant defines

#define kMDMIsReadyNotificationKey @"MDMIsReadyNotification"

#pragma mark - Type defines

typedef void (^MDMInfoCompletionBlock)(NSDictionary *config);

#pragma mark - Class interface

@interface MDMInfo : NSObject

#pragma mark - MDM Info class methods

+ (void)getMMDInfo:(MDMInfoCompletionBlock)completion;

@end

