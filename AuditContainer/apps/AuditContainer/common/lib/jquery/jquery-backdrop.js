(function ( $ ) {
	
	$.fn.backdrop = $.backdrop = function(options) {
		var classes = "visible active";
        var defaultBackDropClass = "backdrop2";
		var backDrop = $("."+defaultBackDropClass);
		var loadingContainer = $(".loading-container2");
		if (!!options.show){
			if (backDrop.length < 1){
				backDrop = $("<div></div>").addClass(defaultBackDropClass).prependTo("body");
			}
			if (loadingContainer.length < 1){
				loadingContainer = $("<div></div>").addClass("loading-container2").prependTo("body");
				$("<div></div>").addClass("spinner").appendTo(loadingContainer);
			}
            if (options.backDropClass){
                backDrop.addClass( options.backDropClass);
            }
			backDrop.addClass(classes);
            if (!!!options.showSpinner){
                loadingContainer.removeClass(classes);
            } else {
                loadingContainer.addClass(classes);
            }
		} else {
            backDrop.removeClass(function(index, className){
                                 return className.replace(defaultBackDropClass,'');
            });
			loadingContainer.removeClass(classes);
		}
	};
}( jQuery ));