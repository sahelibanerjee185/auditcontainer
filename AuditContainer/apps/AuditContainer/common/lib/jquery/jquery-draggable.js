(function ( $ ) {
	$.fn.draggable = function(options) {
		var preItem = $("<div></div>").addClass("xclearfix").addClass("draggable-fix").css({
			"border-radius": "5px",
			"border": "2px dashed #f1f1f1",
			"padding": "10px",
			"margin-bottom": "5px",
			"margin-left": "0px",
			"margin-right": "0px",
			"margin-top": 0,
			//"height": 329px;
			"background-color": "#c9c9c9",
			"text-align": "center",
			//"width": 502px;
			"display": "inline-block",
			"margin-right": "2px",
			"overflow": "hidden"
		});
		var offset = undefined;
		//var cssPosition = undefined;
		var objState = {
			"state": undefined, /* 0-start,1-move,2-end */
			"firstTime" : true, /* true/false */
			"id" : undefined,
			"skipElement" : false
		};
		var checkState = function (state, objId){
			if( objState.id !== objId ){
				objState.state = state;
				objState.firstTime = true;
			} else {
				if (objState.state !== state){
					objState.state = state;
					objState.firstTime = true;
				}else{
					objState.firstTime = false;
				}
			}
		};
		var start = function(e) {
			e.preventDefault();
			objState.skipElement = $(this).find(".widget-btn-content-solo:visible").length > 0;
			if (objState.skipElement) return;
			var orig = e.originalEvent;
			var elementFromPoint = document.elementFromPoint(orig.changedTouches[0].pageX , orig.changedTouches[0].pageY );
			if (elementFromPoint !== null){
				if (!!$(elementFromPoint).attr("draggable-allow") !== true){
					objState.skipElement = true;
					return;
				}
			}else {
				objState.skipElement = true;
				return;
			}
			checkState(0, $(this).attr("id"));
			if (objState.firstTime){
				/*
				cssPosition = $(this).css("position");
				if (cssPosition === undefined){
					cssPosition = "inherit";
				}
				*/
			}
			
			var pos = $(this).position();
			offset = {
				position: "absolute",
				x: orig.changedTouches[0].pageX - pos.left,
				y: orig.changedTouches[0].pageY - pos.top
			};
			if (options.startDrag){
				options.startDrag.call(null);
			}
		};
		var moveMe = function(e) {
			e.preventDefault();
			if (objState.skipElement) return;
			checkState(1, $(this).attr("id"));
			var orig = e.originalEvent;
			if (objState.firstTime){
				preItem.css({
					"height": $(this).outerHeight(false),
					"width": $(this).outerWidth(false)
				});
				preItem.insertBefore($(this));
				$(this).css({
					position: "absolute",
					"z-index":5001
				});
			}
			$(this).css({
				top: orig.changedTouches[0].pageY - offset.y,
				left: orig.changedTouches[0].pageX - offset.x
			});
		};
		var end = function(e) {
			e.preventDefault();
			//return;
			if (objState.skipElement) return;
			checkState(2, $(this).attr("id"));
			preItem.remove();
			var orig = e.originalEvent;
			var element = $(this);
			while (!element.hasClass(options.targetClass)){
				element = element.parent();
			}
			element.removeAttr("style");
			/*
			element.css({
				position: cssPosition,
				top : "0",
				left : "0"
			});
			*/
			var target = null;
			var leftOrRight = undefined;
			var elementFromPoint = document.elementFromPoint(orig.changedTouches[0].pageX , orig.changedTouches[0].pageY );
			if (elementFromPoint !== null){
				//console.log(elementFromPoint);
				target = $(elementFromPoint);
				var failSave = 100;
				while (!target.hasClass(options.targetClass) && (!!failSave)){
					target = target.parent();
					failSave--;
				}
				if (!!!failSave){
					target = element.parent();
					if (target.hasClass(options.containerClass)){
						failSave = 100;
						target = target.children().first();
					}
				}
				if (!!failSave){
					var pos = target.position();
					leftOrRight = (orig.changedTouches[0].pageX - pos.left) < (target.width() / 2);
					if ((element.parent()[0] == target.parent()[0]) && (element[0] != target[0])){
						if (leftOrRight) {
							element.detach().insertBefore(target);
						}else{
							element.detach().insertAfter(target);
						}
					}
				}
			}
			if (options.endDrag){
				options.endDrag.call(null, element, target, leftOrRight);
			}
		};
		$(this).on("touchstart", start);
		$(this).on("touchmove", moveMe);
		$(this).on("touchend", end);
	};
}( jQuery ));
