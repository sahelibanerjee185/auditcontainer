var compareStrings = function ( firstString, secondString ){
	return  (firstString.localeCompare(secondString) == 0) ? true : false;
}

var validateNumeralOnly = function(amount) {
	var regex = /^(?:[0-9] ?){1,15}[0-9]$/;
	return regex.test(amount);
}

var validateEmail = function(email) {
    var regex = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]\.)*\w[\w-]{0,66})\.([a-z]{2,4}(?:\.[a-z]{2,4})?)$/;
    return regex.test(email);
}

var keyboardInfo = {
	keyboardHeight : null,
	nativeFooterHeight : 0,
	heightAdjust : 0,
	getHeight : function(){
		return this.keyboardHeight + this.nativeFooterHeight + this.heightAdjust;
	},
	adjustHeight : function(height){
		this.heightAdjust = height;
	}
};

var smallLoaderConfig = {
	 template : '<div class="spinnerTemplate"  ng-click="$event.stopPropagation();"> <div class="spinner"> <div class="ion-loading-c"></div> <div class="spinnerContent">Loading</div> </div> </div>',
	 animation : 'fade-out',
	 showBackdrop: false,
	 showDelay: 0,
	 hideOnStateChange : true
}

var serverLoaderConfig = {
	 template : '<div class="spinnerTemplate" ng-click="$event.stopPropagation();" > <div class="spinner"> <div class="ion-loading-c"></div> <div class="spinnerContent">Retrieving data from server</div> </div> </div>',
	 animation : 'fade-out',
	 showBackdrop: false,
	 showDelay: 0,
	 hideOnStateChange : true	
}

var initAppLoaderConfig = {
	 template : '<div class="spinnerTemplate"  ng-click="$event.stopPropagation();"> <div class="spinner"> <div class="ion-loading-c"></div> <div class="spinnerContent">Retrieving application data from server</div> </div> </div>',
	 animation : 'fade-out',
	 showBackdrop: false,
	 showDelay: 0,
	 hideOnStateChange : true	
}

var cancelVisitConfig = {
	 template : '<div class="spinnerTemplate"  ng-click="$event.stopPropagation();"> <div class="spinner"> <div class="ion-loading-c"></div> <div class="spinnerContent">Canceling visit</div> </div> </div>',
	 animation : 'fade-out',
	 showBackdrop: false,
	 showDelay: 0,
	 hideOnStateChange : true	
}

var getVisitsLoaderConfig = {
	 template : '<div class="spinnerTemplate"  ng-click="$event.stopPropagation();"> <div class="spinner"> <div class="ion-loading-c"></div> <div class="spinnerContent">Retrieving visit data from server</div> </div> </div>',
	 animation : 'fade-out',
	 showBackdrop: false,
	 showDelay: 0,
	 hideOnStateChange : true	
}



// regex pattern to use with angulars ng-pattern directive for validation
var validationRegex = {
		max3Digit : new RegExp('^[0-9]{0,3}$'),
		mediumAlphaNumeric : new RegExp('^[a-zA-Z0-9]{4,10}$'),
		alphaNumericMax8 : new RegExp('^[a-zA-Z0-9]{0,8}$'),
		alphaNumericMax2 : new RegExp('^[a-zA-Z0-9]{0,2}$'),
		alphaNumericMax20 : new RegExp('^[a-zA-Z0-9]{0,20}$'),
		alphaNumericMax36 : new RegExp('^[a-zA-Z0-9]{0,36}$'),
		mediumNumeric : new RegExp('^[0-9]{3,10}$'),
		largeMixedCharacters : new RegExp('^([A-Za-z0-9 _.,!"\'\/$]){5,30}$'),
		onlyAlpha2Chars: new RegExp('^[a-zA-z]{2}$'),
		onlyNumeric4Digits: new RegExp('^[0-9]{4}$'),
		onlyAlpha4Chars: new RegExp('^[a-zA-z]{4}$'),
		sparePartRegex: new RegExp('^[0-9]{6}[a-zA-Z\-]{1,6}$')
};

// used for modal extend options

var deepMerge = function(obj1, obj2){ // deep merge function
    var result = {}; // return result
    for (var i in obj1){      // for every property in obj1 
        if((i in obj2) && (typeof obj1[i] === "object") && (obj1[i] !== null) && (i !== null)){
            result[i] = deepMerge(obj1[i],obj2[i]); // if it's an object, merge   
        } else {
        	if ((obj1[i] === null) || (obj1[i] === undefined)) {
        		result[i] = obj2[i];
        	} else {
        		result[i] = obj1[i]; // add it to result
        	}
        		
        }
    }
    for (i in obj2){ // add the remaining properties from object 2
        if(i in result){ //conflict
            continue;
        }
        result[i] = obj2[i];
    }
    return result;
}

var dateInArray = function(arr, obj) {
	for (var i = 0; i < arr.length; i++) {
		if (arr[i].date == obj.date)
			return true;
	}
	return false;
}

var flags = {
		Denmark 	: 'images/flags/denmark.PNG',
		France 		: 'images/flags/france.PNG',
		Greece 		: 'images/flags/greece.PNG',
		Italy		: 'images/flags/italy.PNG',
		Lichtenstein: 'images/flags/lichtenstein.PNG',
		Norway		: 'images/flags/norway.PNG',
		Russia		: 'images/flags/russia.PNG',
		Spain		: 'images/flags/spain.PNG',
		Turkey		: 'images/flags/turkey.PNG'
};

var setSmallDatePickerConfig = function(disabledDates, weekDaysList, monthList, datePickerCallback, fromDate, toDate){
	return {
	      titleLabel: '',  //Optional
	      todayLabel: 'Today',  //Optional
	      closeLabel: 'Close',  //Optional
	      setLabel: 'Select',  //Optional
	      setButtonType : 'button-assertive',  //Optional
	      todayButtonType : 'button-assertive',  //Optional
	      closeButtonType : 'button-assertive',  //Optional
	      inputDate: new Date(),  //Optional
	      mondayFirst: true,  //Optional
	      disabledDates: disabledDates, //Optional
	      weekDaysList: weekDaysList, //Optional
	      monthList: monthList, //Optional
	      templateType: 'modal', //Optional
	      showTodayButton: 'true', //Optional
	      modalHeaderColor: 'bar-positive', //Optional
	      modalFooterColor: 'bar-positive', //Optional
	      from: fromDate || new Date(), //Optional
	      to: toDate || new Date(2100, 8, 25),  //Optional
	      callback: function (val, isCloseButton) {  //Mandatory
	        datePickerCallback(val, isCloseButton);
	      },
	      dateFormat: 'dd MMM yyyy', //Optional
	      closeOnSelect: false, //Optional
	 }
};

var loaderConfig = {
	 template : '<div class="spinnerTemplate"  ng-click="$event.stopPropagation();"> <div class="spinner"> <div class="ion-loading-c"></div> <div class="spinnerContent">Loading</div> </div> </div>',
	 animation : 'fade-out',
     showBackdrop: false,
     showDelay: 0,
	 hideOnStateChange : true
};

var planVisitLoader = {
		 template : '<div class="spinnerCustomPlanVisit"  ng-click="$event.stopPropagation();"><div class="spinner"><div class="ion-loading-c"></div> <div class="spinnerContent">Loading</div></div></div>',
		 animation : 'fade-out',
	     showBackdrop: false,
	     showDelay: 0,
		 hideOnStateChange : true
}

var loaderSyncConfig = {
	 template : '<div class="spinnerTemplate"  ng-click="$event.stopPropagation();"> <div class="spinner"> <div class="ion-loading-c"></div> <div class="spinnerContent">Syncing visits...</div> </div> </div>',
	 animation : 'fade-out',
     showBackdrop: false,
     showDelay: 0,
	 hideOnStateChange : true
};

var generatePdfReportLoaderConfig = {
    template: '<div class="spinnerTemplate"  ng-click="$event.stopPropagation();"> <div class="spinner"> <div class="ion-loading-c"></div> <div class="spinnerContent">Generating pdf...</div> </div> </div>',
    animation: 'fade-out',
    showBackdrop: false,
    showDelay: 0,
    hideOnStateChange: true
}

var monthNames = [ 'January', 'February', 'March', 'April', 'May', 'June',
                   'July', 'August', 'September', 'October', 'November', 'December' ];


/**
 * This function will handle the conversion from a file to base64 format
 *
 * @path string
 * @callback function receives as first parameter the content of the image
 */
function getFileContentAsBase64(path,callback){
	path = "file://"+path;
    window.resolveLocalFileSystemURL(path, gotFile, fail);
            
    function fail(e) {
		console.log(e);
    	console.log('Cannot find requested file');
		if (e.code == FileError.NOT_FOUND_ERR) {
			callback();
		} else {
			callback("ERROR");
		}
    }

    function gotFile(fileEntry) {
       fileEntry.file(function(file) {
          var reader = new FileReader();
          reader.onloadend = function(e) {
               var content = this.result;
               callback(content);
          };
          // The most important point, use the readAsDatURL Method from the file plugin
          reader.readAsDataURL(file);
       });
    }
}

var getUTCDate = function(){
    var now = new Date().toISOString();
    var utc = now.replace("T"," ").split(".")[0];
    
    return utc;
};

function dateToSQLDateTime(date) {
	if (isNaN(date.getFullYear()))
		return;
	
	return date.getFullYear() + '-'
			+ ('00' + (date.getMonth() + 1)).slice(-2) + '-'
			+ ('00' + date.getDate()).slice(-2) + ' '
			+ ('00' + date.getHours()).slice(-2) + ':'
			+ ('00' + date.getMinutes()).slice(-2) + ':'
			+ ('00' + date.getSeconds()).slice(-2);
}