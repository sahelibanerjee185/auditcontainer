/* JavaScript content from js/controllers/LoginController.js in folder common */
audit
.controller("LoginCtrl", ['$scope', '$location',  '$timeout' , '$ionicLoading', '$backend','$localDB','$manualsModule','multiModal','$surveyModule','$planVisit','repairShopService','postLoginInit', 'auditService', 'syncService','$rootScope','$unitRefer','AuditPollingService', 'PendingAuditsService','startedAudit', 'buildVersionService', function($scope, $location, $timeout, $ionicLoading, $backend, $localDB,$manualsModule, multiModal, $surveyModule, $planVisit, repairShopService, postLoginInit, auditService, syncService, $rootScope, $unitRefer, AuditPollingService, PendingAuditsService, startedAudit, buildVersionService){
	var fastClickLogin = document.getElementById('loginScreen');
	FastClick.attach(fastClickLogin);

	function keyboardShowHandlerScreen(event) {
	    var keyboardHeight = event.keyboardHeight;
	    var currentHeight = document.getElementById('loginSroll').clientHeight;
	    var containerHeight = currentHeight - keyboardHeight;
	    document.getElementById("loginSroll").style["height"] = containerHeight + 'px';
	}

	function keyboardHideHandlerScreen() {
	    var keyboardHeight = event.keyboardHeight;
	    var currentHeight = document.getElementById('loginSroll').clientHeight;
	    document.getElementById("loginSroll").style["height"] = '100%';
	}
	
	syncService.clearRefreshInterval();
	
	buildVersionService.getPrettyAppBuild()
	.then(function(buildVersion) {
		$scope.buildNo = buildVersion;
	});

	$scope.mdmInfoReceived = false;
	$scope.initFailed = false;
	$scope.failMessage = '';
	$scope.loginFailed = false;
	$scope.authentificationFailed = true;
	$scope.user = {
		userName : '',
		certificateId : ''
	}
	
	window.addEventListener('native.keyboardshow', keyboardShowHandlerScreen);
    window.addEventListener('native.keyboardhide', keyboardHideHandlerScreen);

	$ionicLoading.show(loaderConfig);
	
	$rootScope.$on("MAMInfoReceived", function(event, args){
		/** PROD -- START **/
		var userInfo = args.response;
		var username;
		/** PROD -- END **/
		/** DEV -- START **/
//		 userInfo = {};
//		 userInfo.userId = "VST222";
//		 userInfo.email = "bianca.radi@ro.ibm.com";
		/** DEV -- END **/
		if (userInfo.enrolledAccount || userInfo.primaryUser) { // authentication is ok
			if (userInfo.enrolledAccount && userInfo.enrolledAccount.length > 0) {
				username = userInfo.enrolledAccount;
			} else if (userInfo.primaryUser && userInfo.primaryUser.length > 0) {
				username = userInfo.primaryUser;
			}
				
			WL.Logger.info("[LoginController][MDMInfoReceived] username: " + userContext.getUsername() + " MDM authentication OK");
			// set username etc...
			$scope.failMessage = 'Certificate ID is too short!';
			$scope.mdmInfoReceived = true;
			userContext.setUsername(username).then(function(){
				$scope.user.userName = username;
				$scope.user.certificateId = userContext.getCertificateId();
				console.log($scope.userName + ' ' + $scope.certificateId);
				userContext.setEmail(username);
				$unitRefer.deleteStorageHelpers()
				$ionicLoading.hide();
			});
		} else {
			WL.Logger.error("[LoginController][MDMInfoReceived] username: " + userContext.getUsername() + " Failed to retrieve MDM info");
			// authentication failed
			$ionicLoading.hide();
			$scope.failMessage = 'MDM authentication failed.';
			$scope.$evalAsync(function(){
				$scope.mdmInfoReceived = true;
				$scope.initFailed = true;
				$scope.loginFailed = true;
				$scope.authentificationFailed = true;
			});
		}
	});
	
	$scope.tryAgain = function(){ 
		auditService.testSignal().then(function(){
			console.log("Internet access");
            $ionicLoading.hide();
			postLoginInit.init().then(function(){
                $ionicLoading.show(loaderConfig);
				WL.Analytics.log({AC_action : "Login Session",AC_userName : $scope.user.userName, AC_timestamp : getUTCDate() },'User Login');
				WL.Analytics.send();
				syncService.updateStaticDataRefreshTimer();
				syncService.setVisitRefreshInterval();
				//SPOT 8 - what to do with poling when check on status
				// AuditPollingService.startPollIfNeeded();
				directToInitialPage();
			}).fail(function(error){
				console.log(JSON.stringify(error));
				$ionicLoading.hide();
				// Sync or loading app data failed, probably not related to internet connection
				var $requestFailed = multiModal.setBasicOptions('notification', 'requestFailed', "Could not process your request. Please try again later.", 'popup');
				multiModal.openModal($requestFailed, $scope);
			})
		}).fail(function(){
			console.log("No Internet access");
			postLoginInit.initOffline().then(function(){
				WL.Analytics.log({AC_action : "Login Session",AC_userName : $scope.user.userName, AC_timestamp : getUTCDate() },'User Login');
				WL.Analytics.send();
				
				//SPOT 8 - what to do with poling when check on status
//				AuditPollingService.startPollIfNeeded(); //we cannot do polling i guess
				
				directToInitialPage();
			});
		});
	};
	
	function directToInitialPage() {
		$surveyModule.getStartedVisitCount().then(function(startedVisits){
			if (startedVisits > 0) {
				startedAudit.checkForStartedVisit().then(function(){
					$scope.$evalAsync(function(){
						$location.switchTab("surveyTab/started");
						$ionicLoading.hide();
					});
				});
			} else {
				if (userContext.firstTimeUsage) {
					$scope.$evalAsync(function () {
						$location.switchTab('planVisitTab');
					});
				} else {
					startedAudit.checkForStartedVisit().then(function(){
						$scope.$evalAsync(function(){
							$location.switchTab("surveyTab/upcoming");
							$ionicLoading.hide();
						});
					});
				}
			}

		});
	}
	
	$scope.login = function(){
		//set certifcateID
		setTimeout(function(){
			$ionicLoading.show(loaderConfig);
		}, 5);
		if(!!$scope.user.certificateId){
			$scope.loginFailed = false;
			document.activeElement.blur();
			var comment = "The certificate you entered is not valid.";
			var $tryAgain = "Could not process your request. Please try again.";
			var timestampComment = "Your offline token has expired, please connect to the internet and log in again.";
			var firstTimeComment = "First time usage of the application requires internet connection.";
			var commentOffline = "Please enter the Certificate ID you used at the last successful log in session.";

			var $firstTimeUsageModalOffline = multiModal.setBasicOptions('notification', 'firstTimeUsageOffline', firstTimeComment, 'popup');
			var $invalidCertificateModalOffline = multiModal.setBasicOptions('notification', 'invalidCertificateOffline', commentOffline, 'popup');
			var $invalidCertificate = multiModal.setBasicOptions('notification', 'invalidCertificate', comment, 'popup');
			var $timeExpired = multiModal.setBasicOptions('notification', 'timeExpired', timestampComment, 'popup');

			var now = new Date();
			now.setHours(0, 0, 0);
			
			//Is first time usage of the app?
			userContext.firstTimeUsage = userContext.getLoginTime() ? false : true;
			
			auditService.testSignal().then(function(){
				$backend.loginWithCertificate($scope.user.certificateId).then(function(result){
					if(result.certificateCount > 0){
						$scope.tryAgain();
						if ($scope.user.certificateId != userContext.getCertificateId()) {
							userContext.setCertificateHasChanged(true);
						}
						// If certificate id changed save in userContext!
						userContext.setCertificateId($scope.user.certificateId).then(function(){
							userContext.setLoginTime(Date.parse(now)).then(function(){
								console.log("Saved timestamp", Date.parse(now));
							});
							console.log("Certificate successfully saved!");
						});
					}else{
						//modal with not valid
						setTimeout(function(){
							$ionicLoading.hide();
						}, 5);
						multiModal.openModal($invalidCertificate, $scope);
					}
				}).fail(function(error){
					setTimeout(function(){
						$ionicLoading.hide();
					}, 5);
					multiModal.openModal($tryAgain, $scope);
				});
			}).fail(function(){
				setTimeout(function(){
					$ionicLoading.hide();
				}, 5);
				loginOffline(now, $timeExpired ,$invalidCertificateModalOffline,$firstTimeUsageModalOffline);
			});
			
			
		}else{
			setTimeout(function(){
				$ionicLoading.hide();
			}, 5);
			$scope.failMessage = 'Certificate ID is too short!';
			$scope.loginFailed = true;
		}
	}
	
	function loginOffline(now, failModal,invalidCertificateModalOffline, firstUsageModal){
		PendingAuditsService.getPendingVisitsCount().then(function(num){
			hybrid.setNoUnsyncedCommands(num);
		});
		
		//First time usage
		if(!userContext.getLoginTime()){
			multiModal.openModal(firstUsageModal, $scope);  
			return;
		}
		
		//Further usage with Token Expired
		if(userContext.getLoginTime() && userContext.getLoginInterval().end < Date.parse(now)){
			multiModal.openModal(failModal, $scope);
			// userContext.setLoginTime(null);
			return;
		}
		
		if(userContext.getUsername() == $scope.user.userName 
				&& userContext.getCertificateId() == $scope.user.certificateId){
			$scope.tryAgain();
			userContext.setCertificateId($scope.user.certificateId).then(function(){
				console.log("Certificate successfully saved!");
			});
		}else{
			multiModal.openModal(invalidCertificateModalOffline, $scope);
		}
	}
	
	window.contextPromise.promise().then(function(){
		hybrid.getMAMInfo();
		// $scope.tryAgain();
	});
	$scope.$on('$destroy', function () {
        window.removeEventListener('native.keyboardshow', keyboardShowHandlerScreen);
        window.removeEventListener('native.keyboardhide', keyboardHideHandlerScreen);
    });

	
}]); 	
