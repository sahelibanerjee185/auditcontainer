/**
 * Camera Controller
 * @namespace Controllers
 */
audit.controller("CameraCtrl", CameraCtrl);

CameraCtrl.$inject=['$scope', '$localDB', '$rootScope', '$backend', '$ionicLoading',
	'$location', '$routeParams', 'multiModal', '$q', '$timeout','$document', 
	'$base64EncoderDecoder', 'PhotoStorageDirectory', '$galleryModule', 'ToastService'];

/**
 * @namespace CameraCtrl
 * @desc Controller used to take pictures 
 * @memberOf Controllers
 */
function CameraCtrl($scope, $localDB, $rootScope, $backend, $ionicLoading, 
	$location, $routeParams, multiModal, $q, $timeout, $document,$base64EncoderDecoder,
 	PhotoStorageDirectory, $galleryModule, ToastService) {

	  $scope.attachments = [];
	  $scope.item = {};
	  $scope.tags = [];
		$scope.obj = {};
		$scope.imagesList = [];
	  $scope.imagesListNo = $scope.imagesList.length;
		
		var attachmentsFlags = [];
	  var emailMessage = 'Photos sent from Audit Application :';
	  var att = [];
	  var photoCount;
	  var feedbackCountLimit = 50;
	  var maxCountLimit = 70;
	  var maxCountForWO = 20;

	  
		/**
		* @name getBase64ImagesParallel
		* @desc Convert images to base64 
		* @returns {Promise} returns array of base64 converted images
    * @memberOf Controllers.CameraController
    */
	  var getBase64ImagesParallel = function() {

	    var defer = $q.defer();
	    var images = [],readers = [];
	    var counter = $scope.attachments.length;

	    for (attachment in $scope.attachments) {
	      if (attachmentsFlags[attachment]) {
	        readers[attachment] = new FileReader();
	        readers[attachment].onloadend = function(image) {
	          images.push(image.target.result);
	          if (--counter === 0) {
	            defer.resolve(images);
	          }
	        }
	        readers[attachment].readAsDataURL($scope.attachments[attachment]);
	      } else {
	        images.push($scope.attachments[attachment].image);
	        if (--counter === 0) {
	          defer.resolve(images);
	        }
	      }
	    }
	    return defer.promise;
	  };


	  $scope.getPictures = function() {
	    var $vImages = {};
	    $vImages = JSON.parse(localStorage.getItem('imagesList'));
	    return $vImages;
	  };

		/**
    * @name onCameraSuccess
    * @desc Triggered when native camera capture image with success
    * @param {String} imageURI path to the taken image
    * @returns {Void}
    * @memberOf Controllers.CameraController
    */
	  var onCameraSuccess = function(imageURI) {

	    //Search for image in FileSystem using imageURI received from Cordova Camera plugin
	    window.resolveLocalFileSystemURL(imageURI, function(entry) {
	        entry.file(function(file) {
	            $scope.$evalAsync(function() {

	              //Accept size lower than half of MB
	              if (file.size < 524288) {
	                file.name = imageURI.split("/").pop();
	                var count = 1;

	                //Update Utils collection with number of photos taken
	                $localDB.find("utils", {}, {}).then(function(data) {
	                  if (data.length == 0) {
	                    var obj = {
	                      photoCount: count
	                    }
	                    $localDB.add("utils", obj, {});
	                  } else {
	                    count = data[0].json.photoCount + 1;
	                    data[0].json.photoCount = data[0].json.photoCount + 1;
	                    $localDB.replace("utils", data[0], {});
	                  }

	                  $scope.imageName = "Photo " + count + ".jpg";
	                  $scope.attachments.push(file);
	                  attachmentsFlags.push(true);

	                  convertFileToBase64(imageURI).then(function(imageBase64) {

											$scope.item.photo = imageBase64;
	                    $base64EncoderDecoder.writePhotoToLocalStorage($scope.item.photo, getCurrentData(), $scope.imageName).then(function(url) {
	                      // refresh photos
	                      $scope.dataArray = $galleryModule.getData();

	                      if (photoCount + 1 == feedbackCountLimit) {
	                        ToastService.showNotificationMsg('You currently have 50 photos in the Started Visit Gallery. Please take into account that there is a limit of 70 photos per visit');
	                      }

	                      $localDB.find("galleryPhoto", {path: url}, {}).then(function(data) {

	                        $scope.dataArray.array.push({
	                          'taggedImages': data[0],
	                          'base64': imageBase64,
	                          'checked': false,
	                        })

	                        if (data.length !== 0) {

														$scope.attachments = [];
														var counter = $scope.dataArray.array.length;
														if (--counter == 0) {
																$scope.imageCount = $scope.dataArray.array.length;
																$timeout(function() {
																	$scope.messages.noPhoto = Messages.noPhotoAdded;
																})
														}
	                        }
	                      })
	                    })
	                  })
	                })
	              } else {
	                var $vOptions = multiModal.setBasicOptions('notification', 'smallerFiles', "Use smaller files");
	                multiModal.openModal($vOptions, $scope);
	              }
	            })
	          },
	          function(error) {
	            alert("Failed to retrieve file from entry because : ", error);
	          });
	      },
	      function(error) {
	        alert("Failed to resolve URI because : ", error);
	      });
	  };

		/**
    * @name onGallerySuccess
    * @desc Retrieve images from device gallery
    * @param {String} imageURI path to the image to retrieve
    * @returns {Void}
    * @memberOf Controllers.CameraController
    */
	  var onGallerySuccess = function(imageURI) {
	    window.resolveLocalFileSystemURL(imageURI, function(entry) {
          entry.file(function(file) {
              $scope.$evalAsync(function() {
                var reader = new FileReader();
                reader.onloadend = function(image) {
                  $scope.$evalAsync(function() {
                    var wrapper = {};
                    $scope.imageName = imageURI.split("/").pop();
                    if (WL.Client.getEnvironment() == WL.Environment.ANDROID) {
                      namePiece1 = $scope.imageName.slice(13);
                      namePiece2 = $scope.imageName.slice(8, 12);
                      wrapper.name = namePiece1.concat(namePiece2);
                    } else {
                      wrapper.name = $scope.imageName;
                    }
                    wrapper.image = image.target.result;
                    $scope.attachments.push(wrapper);
                    attachmentsFlags.push(false);

                    getBase64ImagesParallel().then(function(data) {
                      $scope.item.photo = data[data.length - 1];

                      $base64EncoderDecoder.writePhotoToLocalStorage($scope.item.photo,getCurrentData(),$scope.imageName)
                        .then(function(url) {
                          // refresh photos
                          $scope.dataArray = $galleryModule.getData();
                          // show notification when user reaches feedback limit
                          if (photoCount + 1 == feedbackCountLimit) {
                            ToastService.showNotificationMsg(
                              "You currently have 50 photos in the Started Visit Gallery. Please take into account that there is a limit of 70 photos per visit"
                            );
                          }

                          $localDB.find("galleryPhoto",{path: url},{}).then(function(data) {
                              $scope.dataArray.array.push({
                                taggedImages: data[0],
                                base64: "",
                                checked: false
                              });

                              if (data.length !== 0) {
                                // get the base64 string for every photo and store it
                                var counter =$scope.dataArray.array.length;
                                for (index in $scope.dataArray.array) {
                                  var componentsUrl = $scope.dataArray.array[index].taggedImages.json.path.split("/");
                                  $galleryModule.getBase64Image($scope.dataArray.array,PhotoStorageDirectory.getFullPath() + "" +
																		componentsUrl[componentsUrl.length - 1]).then(function(data) {
                                        $scope.dataArray.array = data;

                                        if (--counter == 0) {
                                          $scope.imageCount = $scope.dataArray.array.length;
                                          $timeout(function() {
                                            $scope.messages.noPhoto = Messages.noPhotoAdded;
                                          });
                                        }

                                      },
                                      function(error) {
                                        console.log("error", error);
                                    }
                                  );
                                }
                              }
                            });
                        });
                    });
                  });
                };
                reader.readAsDataURL(file);
              });
            }, function(error) {
              console.log("Failed to retrieve file from entry because : ", error);
            });
        }, function(error) {
          console.log("Failed to resolve URI because : ", error);
        });

	  };

		/**
    * @name onFail
    * @desc Triggered when native camera capture fail
    * @param {Object} error describes error ocurred when camera fails
    * @returns {Void}
    * @memberOf Controllers.CameraController
    */
	  var onFail = function(error) {
	    console.log("fail");
	  };

		/**
    * @name cameraButton
    * @desc Open camera to take picture
		* @param {Integer} visitId unique identifier of a Visit
		* @param {Integer} workOrderNo unique identifier of a Work Order
    * @returns {Void}
    * @memberOf Controllers.CameraController
    */
	  $scope.cameraButton = function(visitId, workOrderNo) {
	    // get photo count from json store if count is 20

	    getPhotoCountForVisit(visitId, true).then(function(count) {
	      if (count >= maxCountLimit) {
	        // toast message plus return!
	        ToastService.showNotificationMsg('You have reached the maximum of 70 photos per visit. If needed, delete from the existing ones associated to the visit.');
	        return;
	      }

	      getPhotoCountForWO(workOrderNo).then(function(woCount) {
	        if (woCount >= maxCountForWO) {
	          ToastService.showNotificationMsg('You have reached the maximum of 20 photos per work order.');
	          return;
	        }

	        $scope.item = {};
	        navigator.camera.getPicture(onCameraSuccess, onFail, {
	          quality: 20,
	          sourceType: Camera.PictureSourceType.CAMERA,
	          allowEdit: true,
	          destinationType: Camera.DestinationType.FILE_URI,
	          targetWidth: 1200,
	          targetHeight: 3000,
	          saveToPhotoAlbum: false
	        });
	      });
	    });
	  };

		/**
    * @name galleryButton
    * @desc Open gallery to select a picture based on VisitId and WorkOrderNo
		* @param {Integer} visitId unique identifier of a Visit
		* @param {Integer} workOrderNo unique identifier of a Work Order
    * @returns {Void}
    * @memberOf Controllers.CameraController
    */
	  $scope.galleryButton = function(visitId, workOrderNo) {

	    getPhotoCountForVisit(visitId, true).then(function(count) {
	      if (count >= maxCountLimit) {
	        // toast message plus return!
	        ToastService.showNotificationMsg('You have reached the maximum of 70 photos per visit. If needed, delete from the existing ones associated to the visit.');
	        return;
	      }

	      getPhotoCountForWO(workOrderNo).then(function(woCount) {
	        if (woCount >= maxCountForWO) {
	          ToastService.showNotificationMsg('You have reached the maximum of 20 photos per work order.');
	          return;
	        }
	        navigator.camera.getPicture(onGallerySuccess, onFail, {
	          quality: 20,
	          sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
	          destinationType: Camera.DestinationType.FILE_URI,
	          targetWidth: 1200,
	          targetHeight: 3000,
	          saveToPhotoAlbum: false
	        });
	      });
	    });
	  };

		/**
    * @name submitPhoto
    * @desc Adds a photo in gallery
    * @returns {Void}
    * @memberOf Controllers.CameraController 
    */
		//TODO possible unused (to check after Maersk systems are UP)
	  $scope.submitPhoto = function() {
	    if ($scope.tags.length == 0) {
	      var $selectTag = multiModal.setBasicOptions('notification', 'submitPhoto', 'Please adds at least one tag for the photo.');
	      multiModal.openModal($selectTag, $scope);
	    } else {
	      var $savePhoto = multiModal.setBasicOptions('notification', 'submitPhoto', 'Photo saved to gallery');
	      $savePhoto = multiModal.setActions($savePhoto, function() {
	        $ionicLoading.show(smallLoaderConfig);
	        $base64EncoderDecoder.writePhotoToLocalStorage($scope.item.photo, $scope.imageName, $scope.tags).then(function() {
	          $ionicLoading.hide();
	          $location.path('galleryTab');
	          console.log('successfully saved photo');
	        })
	      });
	      multiModal.openModal($savePhoto, $scope);
	    }
	  };
	
		/**
    * @name getPhotoCountForVisit
    * @desc Count the number of photos in a visit 
		* @param {Integer} visitId unique identifier for a visit
		* @param {Boolean} isCamera 
    * @returns {Promise}
    * @memberOf Controllers.CameraController 
    */
	  function getPhotoCountForVisit(visitId, isCamera) {
	    var dfd = $.Deferred();

	    $localDB.find("galleryPhoto", {visitId: visitId}, {exact: true}).then(function(data) {
	        if (data && data.length) {
	          photoCount = data.length;
	          dfd.resolve(data.length);
	        } else {
	          if (isCamera) {
	            dfd.resolve(0);
	          } else {
	            dfd.reject();
	          }
	        }
	      }).fail(function(error) {
	        dfd.reject(error);
	      })

	    return dfd.promise();
	  }

		/**
    * @name getPhotoCountForWO
    * @desc Count the number of photos in a Work Order
		* @param {Integer} visitId unique identifier for a visit
    * @returns {Promise}
    * @memberOf Controllers.CameraController 
    */
	  function getPhotoCountForWO(workOrderNo) {
	    var dfd = $.Deferred();

	    $localDB.find("galleryPhoto", {wono: workOrderNo}, {exact: true}).then(function(data) {
	        if (data && data.length) {
	          photoCount = data.length;
	          dfd.resolve(data.length);
	        } else {
	          dfd.resolve(0);
	        }
	      }).fail(function(error) {
	        dfd.reject(error);
	      })

	    return dfd.promise();
	  }

	  function getCurrentData() {
	    var today = new Date();
	    var dd = today.getDate();
	    var mm = today.getMonth() + 1;
	    var yyyy = today.getFullYear();

	    if (dd < 10) {
	      dd = '0' + dd
	    }

	    if (mm < 10) {
	      mm = '0' + mm
	    }

	    today = mm + '/' + dd + '/' + yyyy;
	    return today;
	  }

	  /**
    * @name convertFileToBase64
    * @desc Convert an image to base64
		* @param {String} path path to the image
    * @returns {Promise}
    * @memberOf Controllers.CameraController 
    */
	  function convertFileToBase64(path) {
	    var defer = $q.defer();

	    window.resolveLocalFileSystemURL(path, function(entry) {
	      entry.file(function(file) {
	        var FR = new FileReader();
	        FR.onloadend = function(image) {
	          defer.resolve(image.target.result);
	        }
	        FR.readAsDataURL(file);
	      });
	    }, function(err) {
	      defer.reject(err);
	    });

	    return defer.promise;

	  };


	}