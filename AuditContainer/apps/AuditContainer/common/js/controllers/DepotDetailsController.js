/**
 * DepotDetailsCtrl Controller
 * @namespace Controllers
 */
(function () {
    'use strict';

    audit.controller('DepotDetailsCtrl', DepotDetailsCtrl);

    DepotDetailsCtrl.$inject = ['$location', '$scope', 'multiModal', '$commonOperations',
        '$localDB', '$unitRefer', '$ionicLoading', '$depot', '$filter',
        '$planVisit', '$route', 'containerHistoryService', 'auditService', '$surveyModule',
        'repairShopService', 'PhotoStorageDirectory', '$timeout', 'startedAudit', '$kpiModule',
        '$rootScope', '$ionicScrollDelegate', 'FailMsgService', 'reportGenerator', 'GalleryExportService',
        'PendingAuditsService', 'ocrService', 'ToastService', 'EmailNotificationService',
        'rejectCorrectEstimate'];

    /**
     * @namespace DepotDetailsCtrl
     * @desc Controller used to view visit details and to work with a visit
     * @memberOf Controllers
     */
    function DepotDetailsCtrl($location, $scope, multiModal, $commonOperations, $localDB,
        $unitRefer, $ionicLoading, $depot, $filter, $planVisit, $route,
        containerHistoryService, auditService, $surveyModule, repairShopService,
        PhotoStorageDirectory, $timeout, startedAudit, $kpiModule, $rootScope,
        $ionicScrollDelegate, FailMsgService, reportGenerator, GalleryExportService,
        PendingAuditsService, ocrService, ToastService, EmailNotificationService,
        rejectCorrectEstimate) {

        // Modal for state change when there are no containers
        var $failedChangeState = multiModal.setBasicOptions('confirm', 'failedChangeState', 'Are you sure you want to leave this page? Your visit will be dismissed because there are no containers to inspect. ');

        $scope.addContainerToVisit = addContainerToVisit;
        $scope.addContainersToVisit = addContainersToVisit;
        $scope.obj = []
        $scope.past = false;
        $scope.mercstatus = "Accepted";
        $scope.obj = $depot.getobject();
        $scope.openPreviewPdf = openPreviewPdf;
        $scope.kpiTotalCount = $kpiModule.getObj().totalCount;
        $scope.kpiPassedCount = $kpiModule.getObj().passedCount;
        $scope.kpiRejectedCount = $kpiModule.getObj().rejectedCount;
        $scope.parseFloat = function (val) {
            return parseFloat(val).toFixed(2);
        }
        $scope.previewPdf = previewPdf;
        $scope.removeRequestEntry = removeRequestEntry;
        $scope.completeVisit = completeVisit;
        $scope.showHistory = showHistory;
        $scope.startOrStopVisit = startOrStopVisit;
        $scope.toastAcceptNotComplete = toastAcceptNotComplete;


        PhotoStorageDirectory.setVisitId($scope.obj.visitId);

        /* offline mode notification modal */
        var $confirmOfflineModeModal = multiModal.setBasicOptions('notification', 'offlineMode', 'This action can\'t be perfomed in offline mode', 'popup');
        $confirmOfflineModeModal = multiModal.setActions($confirmOfflineModeModal);
        /* */

        var options = {
            filter: ['containerNo', 'repairCost', 'repairDate', 'shopCode', 'visitId', 'woStatus', 'workOrderId', 'mode', 'auditResult', 'statusCode']
        };

        // OCR Functionality

        $scope.ocrService = ocrService;
        $scope.openOCR = openOCR;
        $scope.capitalizeLetters = capitalizeLetters;
        $scope.validateOCR = validateOCR;
        $scope.workOrderPreview = workOrderPreview;
        $scope.partialOCRModalView = "templates/OCR/containerIdEditPartial.html";

        function capitalizeLetters(item, index) {
            $scope.ocrData.letters[index] = item[item.length - 1].toUpperCase();
        };

        function validateOCR(ocrData, item, index) {
            var containerID = ocrData.letters.join("") + ocrData.numbers.join("");
            $scope.errors = ocrService.validateOcrText(containerID);
        };

        /**
         * @name openOCR
         * @desc Opens camera to take photo with OCR,
         * Once we get the text and fileName,
         * Opens the OCR modal with the appropiate data 
         * populated in the UI
         * @param N/A
         * @returns N/A
         * @memberOf Controllers.DepotDetailsCtrl
         */
        function openOCR() {
            // We lose hybrid control, show loader and hide after we gain it again
            $ionicLoading.show(loaderConfig);

            var path = ocrService.buildPhotoPath();

            /* This will trigger native OCR using hybrid.openOCR function */
            hybrid.openOCR(path).then(function (data) {

                $ionicLoading.hide();	//Hide Loader

                var containerID = data.text;
                var path = data.fileName;

                //Parse OCR data
                ocrService.parseOCRData(containerID, path).then(function (ocrData) {
                    $scope.ocrData = ocrData;

                    $scope.partialOCRModalView = "templates/OCR/containerIdEditPartial.html";
                    $scope.validateOCR($scope.ocrData); //Validate containerID received from Native OCR

                    //Simulate RouteParams in next screen (AddVisitController) to be able to use
                    ocrService.setVisitDetails($scope.obj.visitDate, $scope.obj.visitId);


                    //Open ContainerID Editing Modal
                    ocrService.openOCRMainModal($scope);
                });

            }, function (error) {
                //Display Toast that OCR failed "Please try again"
                $ionicLoading.hide();
                if (error.status == 102) {
                    ToastService.showNotificationMsg('OCR Failed to recognize container ID. Please try again!');
                }
            });
        };

        /**
         * @name workOrderPreview
         * @desc Finds the latest work order associated with the container
         * populate $scope.ocrData with work order info, id and associated repairs
         * show the UI with associated repairs/work order info
         * @param N/A
         * @returns N/A
         * @memberOf Controllers.DepotDetailsCtrl
         */
        function workOrderPreview() {
            ocrService.removeFileByName($scope.ocrData.imageName); // remove image from temporary folder
            // Build ContainerID
            $scope.ocrData.containerID = $scope.ocrData.letters.join("") + $scope.ocrData.numbers.join("");

            // GET repairs and parts for latest WO available for the provided ContainerID
            findContainerByID($scope.ocrData.containerID, $scope.obj.repairShopDetails.shopCode).then(function () {
                $scope.addContainerToVisit($scope.ocrData.workOrderID).then(function () {
                    $ionicLoading.hide();
                }).fail(function (err) {
                    $ionicLoading.hide();
                });
            }).fail(function (err) {
                $ionicLoading.hide();
            });
        };

        /**
         * @name findContainerByID
         * @desc Finds the latest work order associated with the container
         * checks if the work order is locked or not and
         * populates $scope.ocrData with work order info, id and associated repairs
         * @param {String} containerID
         * @param {String} shopID
         * @returns N/A
         * @memberOf Controllers.DepotDetailsCtrl
         */
        function findContainerByID(containerID, shopID) {
            var defer = $.Deferred();
            $ionicLoading.show(loaderConfig);

            // check if work order is locked first!

            ocrService.getLatestWORepairsByID(containerID, shopID).then(function (results) {
                $scope.ocrData.workOrderID = results[0].workOrderId;
                $scope.ocrData.repairs = results;

                // we have work order id, check if it's locked or not!

                auditService.getWorkOrderInfo(null, $scope.ocrData.workOrderID).then(function (workOrderInfo) {
                    if (workOrderInfo.auditStatus == 'Planned' || workOrderInfo.auditStatus == 'Started') {
                        ToastService.showNotificationMsg('Associated work order is locked and cannot be selected.');
                    } else {
                        //Check if WO already exists in Visit
                        if (!isWOAlreadyInVisit(containerID, $scope.ocrData.workOrderID)) {
                            //We can proceed to show WO preview and add container in visit
                            $scope.ocrData.modalTitle = "Work Order Preview";
                            $scope.partialOCRModalView = "templates/OCR/availableWOStartedVisit.html";
                        } else {
                            //We can't add the container again
                            ToastService.showWarningMessage('Container no. ' + containerID + ' is already booked in current visit. Please review the container number or existing Work Orders in visit');
                        }
                    }
                    defer.resolve($scope.ocrData.workOrderID);
                    // check if it's locked or not
                }).fail(function (error) {
                    ToastService.showNotificationMsg('No Work Order is currently available for Container no: ' + containerID);
                    defer.reject();
                });
            }, function (err) {
                ToastService.showNotificationMsg('No Work Order is currently available for Container no: ' + containerID);
                defer.reject();
            });
            return defer.promise();
        };

        function isWOAlreadyInVisit(containerID, workOrderId) {
            var woArray = $scope.obj.workOrdersForAudit;
            for (var i = 0; i < woArray.length; i++) {
                if (woArray[i].containerno == containerID && woArray[i].workorderid == workOrderId) {
                    return true;
                }
            }
            return false;
        };

        /* End of OCR Functionality */

        /**
         * @name completeVisit
         * @desc Populate $surveyModule service with started visit data,
         * direct to PDF Preview screen
         * @param N/A
         * @returns N/A
         * @memberOf Controllers.DepotDetailsCtrl
         */
        function completeVisit() {
            $surveyModule.getStartedVisitInfo()
                .then(function () {
                    $scope.$evalAsync(function () {
                        // Load pdf data to populate screen
                        $location.path("/previewPdf/");
                    });
                });
        }

        /**
         * @name previewPdf
         * @desc generates and opens a preview of the visit pdf
         * based on current visit info
         * @param {String} visitId
         * @param {String} inspectionStatus
         * @returns N/A
         * @memberOf Controllers.DepotDetailsCtrl
         */
        function previewPdf(visitId, inspectionStatus) {
            if (inspectionStatus == "Started") {
                var pdfName = 'preview-';
                $scope.generatePdf('previewDummySignature', visitId, true).then(function () {
                    $scope.openPreviewPdf(visitId, pdfName);
                });
            } else {
                var pdfName = '';
                $scope.openPreviewPdf(visitId, pdfName);
            }
        };

        function openPreviewPdf(visitId, pdfName) {
            var pdfPath = "PdfReports/" + visitId + "/" + pdfName + "audit-report-" + visitId + ".pdf";
            checkIfFileExists(pdfPath).then(function (exists) {
                if (exists) {
                    nativePdfPromise = $.Deferred();
                    WL.App.sendActionToNative("previewPdf", {
                        pdfPath: pdfPath
                    });
                    return nativePdfPromise.promise();
                } else {
                    var $noPDFLocally = multiModal.setBasicOptions('notification', 'noPDF', 'No cached information available to generate PDF', 'popup');
                    $noPDFLocally = multiModal.setActions($noPDFLocally);
                    multiModal.openModal($noPDFLocally, $scope);
                }
            }).fail(function () {
                var $noPDFLocally = multiModal.setBasicOptions('notification', 'noPDF', 'No cached information available to generate PDF', 'popup');
                $noPDFLocally = multiModal.setActions($noPDFLocally);
                multiModal.openModal($noPDFLocally, $scope);
            });
        };

        $scope.objVisit = $surveyModule.getObj();

        /**
         * @name generatePdfModel
         * @desc populates $surveyModule with necessary visit info on work orders,
         * used in the preview pdf screen for building the UI,
         * data also used to build the actual generated pdf
         * @param N/A
         * @returns N/A
         * @memberOf Controllers.DepotDetailsCtrl
         */
        function generatePdfModel() {
            var aWoTypes = ["plannedWorkOrders", "startedWorkOrders", "passedWorkOrders", "failedWorkOrders"];

            // Iterate through work order types
            angular.forEach(aWoTypes, function (woType, key) {
                // Check if woType exists in started info object
                if ($scope.objVisit.startedVisitInfo[woType] && $scope.objVisit.startedVisitInfo[woType].length > 0) {
                    // Iterate through visit info by work order type
                    angular.forEach($scope.objVisit.startedVisitInfo[woType], function (wo, woKey) {
                        $scope.objVisit.startedVisitInfo[woType][woKey]['parts'] = new Array();
                        if ($scope.objVisit.partsObject[wo.workorderid]) {
                            $scope.objVisit.startedVisitInfo[woType][woKey]['parts'] = $scope.objVisit.partsObject[wo.workorderid];
                        }
                    });
                }
            });
        }

        /**
         * @name generatePdf
         * @desc generates the PDF based on signatures and visit data,
         * saves the file in the 'PdfReports' folder on the device
         * @param {String} signatures - base64 signatures from the html
         * @param {String} visitId
         * @param {Bool} pdfPreview - adds a preview watermark if true
         * @returns N/A
         * @memberOf Controllers.DepotDetailsCtrl
         */
        $scope.generatePdf = function (signatures, visitId, pdfPreview) {
            var dfd = $.Deferred();
            // Mapping for Work Order types
            generatePdfModel();

            // Add base64 signatures for pdf
            $scope.objVisit.startedVisitInfo.signatures = signatures;

            console.log("started visit info!");

            if (pdfPreview === true) {
                $scope.objVisit.pdfPreview = 'pdfPreview';
            } else {
                $scope.objVisit.pdfPreview = '';
            }

            // Generate pdf report
            reportGenerator.generate($scope.objVisit, visitId, pdfPreview).then(function () {
                dfd.resolve();
            }).fail(function () {
                dfd.reject();
                // could not generate pdf
            });

            return dfd.promise();
        };


        var $historyModal = multiModal.setBasicOptions('confirm', 'historyModal', null, 'full');
        $historyModal = multiModal.setTemplateFile($historyModal, 'templates/common/visitDetailsModal.html');

        /**
         * @name goToInspectionScreen
         * @desc Directs user to inspections screen,
         * depending on work order status code,
         * either prerepair or postrepair inspection view is loaded,
         * passes an extra parameter that specifies what screen the back button should direct to
         * @param {Object} statusCode - the status code of the work order,
         * so we can tell if it's postrepair or prerepair
         * @param {String} ratingQuality - sets the reating quality of the work order
         * @param {String} containerNo - container number
         * @returns N/A
         * @memberOf Controllers.DepotDetailsCtrl
         */
        function goToInspectionScreen(statusCode, ratingQuality, containerNo, fromStartedVisit) {
            $ionicLoading.hide();
            $scope.$evalAsync(function () {
                if (auditService.isPostRepair(statusCode)) {
                    $unitRefer.setWORatingQuality(ratingQuality);
                    if (fromStartedVisit) {
                        $location.path('unitReferPostRepair/surveyTab').search({ containerNo: containerNo });
                    } else {
                        $location.path('unitReferPostRepair').search({ containerNo: containerNo });
                    }
                } else {
                    if (fromStartedVisit) {
                        $location.path('unitRefer/surveyTab').search({ containerNo: containerNo });
                    } else {
                        $location.path('unitRefer').search({ containerNo: containerNo });
                    }
                }
            })
        }

        /**
         * @name goToUnitRefer
         * @desc Populates services with required data and directs to inspections screen
         * @param {Object} visit - work order information
         * @param {String} containerNo - container number
         * @param {String} inspectionStatus - inspection status
         * @param {String} workOrderNo - work order number
         * @param {Bool} fromStartedVisit - tells if the screen has been accessed from the started visit tab
         * @returns N/A
         * @memberOf Controllers.DepotDetailsCtrl
         */
        $scope.goToUnitRefer = function (visit, containerNo, inspectionStatus, workOrderNo, fromStartedVisit) {
            $ionicLoading.show(loaderConfig);

            $unitRefer.setFinalComment(visit.finalcomment == null || visit.finalcomment == "" || visit.finalcomment == undefined ? "" || visit.finalcomment == "(NULL)" : visit.finalcomment);
            $unitRefer.setVisitId($scope.obj.visitId);
            $unitRefer.setContainerNo(containerNo);
            $unitRefer.setWorkOrderNo(workOrderNo);
            $unitRefer.setWorkOrderStatus(visit.wostatus);
            $unitRefer.setStatusCode(visit.statuscode);
            $unitRefer.setAuditResult(visit.auditresult);

            PhotoStorageDirectory.setWoNo(workOrderNo);

            // Check work order status

            switch ($scope.obj.inspectionStatus) {
                case 'Started':
                    startedAudit.getWorkOrderStatusAndComment(obj.workOrderNo, obj.visitId).then(function (data) {
                        $unitRefer.setFinalComment(data.finalComment);
                        switch (data.workOrderStatus) {
                            case 'Started':
                            case 'Planned':
                                auditService.testSignal()
                                    .then(function () {
                                        return populatePartsAndRepairsOnline(workOrderNo, data.workOrderStatus, containerNo, visit.wostatus, visit)
                                    }).then(function () {
                                        goToInspectionScreen(visit.statuscode, visit.ratingRQ, containerNo, fromStartedVisit);
                                    }).fail(function () {
                                        populatePartsAndRepairsOffline(workOrderNo, data.workOrderStatus, containerNo, visit.wostatus, visit)
                                            .then(function () {
                                                goToInspectionScreen(visit.statuscode, visit.ratingRQ, containerNo, fromStartedVisit);
                                            });
                                        $ionicLoading.hide();
                                    });
                                break;
                            case 'Finished':
                                $unitRefer.setStoredWorkOrderParts(workOrderNo, $scope.obj.visitId).then(function (parts) {
                                    $unitRefer.setInspectionStatus($scope.obj.inspectionStatus, data.workOrderStatus).then(function () {
                                        goToInspectionScreen(visit.statuscode, visit.ratingRQ, containerNo, fromStartedVisit);
                                    });
                                });
                                break;
                        }
                    });
                    break;
                case 'Planned':
                    auditService.testSignal()
                        .then(function () {
                            return populatePartsAndRepairsOnline(workOrderNo, 'Planned', containerNo, visit.wostatus, visit);
                        }).then(function () {
                            goToInspectionScreen(visit.wostatus, visit.ratingRQ, containerNo, fromStartedVisit);
                        }).fail(function () {
                            populatePartsAndRepairsOffline(workOrderNo, 'Planned', containerNo, visit.wostatus, visit)
                                .then(function () {
                                    goToInspectionScreen(visit.wostatus, visit.ratingRQ, containerNo, fromStartedVisit);
                                });
                        });
                    break;
                case 'Finished':
                    auditService.getFinishedWorkOrderParts(workOrderNo, $scope.obj.visitId).then(function (result) {
                        console.log('finished work order info', result);
                        $unitRefer.setStatusCode(visit.statuscode);
                        $unitRefer.setFinishedParts(result);
                        $unitRefer.setInspectionStatus($scope.obj.inspectionStatus, 'Finished').then(function () {
                            goToInspectionScreen(visit.statuscode, visit.repairquality, containerNo, fromStartedVisit);
                        });
                    });
                    break;
                case 'Missed':
                    auditService.testSignal()
                        .then(function () {
                            return populatePartsAndRepairsOnline(workOrderNo, 'Missed', containerNo, visit.wostatus, visit);
                        }).then(function () {
                            goToInspectionScreen(visit.statuscode, visit.repairquality, containerNo, fromStartedVisit);
                        }).fail(function () {
                            populatePartsAndRepairsOffline(workOrderNo, 'Missed', containerNo, visit.wostatus, visit)
                                .then(function () {
                                    goToInspectionScreen(visit.statuscode, visit.repairquality, containerNo, fromStartedVisit);
                                });
                        });
                    break;
                case 'Pending':
                    // Get data from Json Store from pendingAuditWorkOrderParts for parts
                    auditService.getPendingWorkOrderParts(workOrderNo).then(function (result) {
                        $unitRefer.setStatusCode(visit.statuscode);
                        $unitRefer.setPendingParts(result);
                        $unitRefer.setInspectionStatus($scope.obj.inspectionStatus, inspectionStatus).then(function () {
                            goToInspectionScreen(visit.statuscode, visit.repairQuality, containerNo, fromStartedVisit);
                        });
                    }).fail(function () {
                        var $noDataOfflineModeModal = multiModal.setBasicOptions('notification', 'noDataJSON', 'No data stored locally for this Work Order', 'popup');
                        $noDataOfflineModeModal = multiModal.setActions($noDataOfflineModeModal);
                        multiModal.openModal($noDataOfflineModeModal, $scope);
                        $ionicLoading.hide();
                    });
            }

        };

        /**
         * @name addContainerToVisit
         * @desc Used to add a single work order to visit 
         * by using functionality from addVisitController
         * @param {String} workOrderId
         * @returns N/A
         * @memberOf Controllers.DepotDetailsCtrl
         */
        function addContainerToVisit(workOrderId) {
            var defer = $.Deferred();

            if ($scope.addingDisabled) {
                return;
            }

            $scope.addingDisabled = true;
            $ionicLoading.show(loaderConfig);

            setTimeout(function () {
                $scope.addingDisabled = false;
            }, 300);

            auditService.testSignal().then(function () {
                var inspectionStatus = $scope.obj.inspectionStatus;
                var currentRepairShopNo = $scope.obj.repairShopDetails.shopCode;
                var visitDate = $scope.obj.visitDate;

                auditService.getWorkOrderInfo(null, workOrderId).then(function (woForAddInVisit) {
                    woForAddInVisit.repairMode = woForAddInVisit.mode;
                    woForAddInVisit.status = woForAddInVisit.woStatus;
                    $planVisit.setWorkOrdersForRepairShop([woForAddInVisit]).then(function (workOrder) {
                        $ionicLoading.hide();
                        defer.resolve();
                    });
                }).fail(function (error) {
                    defer.reject(error)
                });
            }).fail(function () {
                WL.Logger.error("[DepotDetailsController][addContainersToVisit][Test Signal] username: " + userContext.getUsername() + " Internet connection not available.");
                $ionicLoading.hide();
                multiModal.openModal($confirmOfflineModeModal, $scope);
                defer.reject(error);
            });

            return defer.promise();
        }

        function toastAcceptNotComplete() {
            ToastService.showNotificationMsg('This WO was accepted, but it was decided the work should not be completed');
        }

        /**
         * @name addContainersToVisit
         * @desc Directs to the addVisit screen,
         * populates screen with the work order shop data,
         * passes visit id as route parameter so 
         * we know that functionality is for adding work orders
         * to an already existing visit.
         * @param {Bool} fromStartedVisit - we need to know if action was performed 
         * from either started tab or inspections view so we know where to redirect
         * after adding work orders is successful
         * @returns N/A
         * @memberOf Controllers.DepotDetailsCtrl
         */
        function addContainersToVisit(fromStartedVisit) {
            if ($scope.addingDisabled) {
                return;
            }

            $scope.addingDisabled = true;
            $ionicLoading.show(loaderConfig);

            setTimeout(function () {
                $scope.addingDisabled = false;
            }, 300);

            auditService.testSignal().then(function () {
                var inspectionStatus = $scope.obj.inspectionStatus; // Check the inspection status in order to perform an action
                switch (inspectionStatus) {
                    case Messages.finished:
                    case Messages.pending:
                        $ionicLoading.hide();
                        multiModal.openModal($addContainerFinishedVisit, $scope);
                        break;
                    case Messages.missed:
                        $ionicLoading.hide();
                        multiModal.openModal($addContainerMissedVisit, $scope);
                        break;
                    default:
                        // Case for Planned or In progress visits (here you can add containers)
                        var currentRepairShopNo = $scope.obj.repairShopDetails.shopCode;
                        var visitDate = $scope.obj.visitDate;
                        PendingAuditsService.runVisitQueue().then(function(){
                        	$surveyModule.refreshVisitData().then(function() {
                        		repairShopService.getWorkOrdersByShopId(currentRepairShopNo).then(function (response) {
                                    $planVisit.setWorkOrdersForRepairShop(response.results).then(function (workOrders) {
                                        $ionicLoading.hide();

                                        if (workOrders.length == 0) {
                                            var $noWorkOrders = multiModal.setBasicOptions('notification', 'noWorkOrders', 'There are no work orders available for this repair shop.');
                                            multiModal.openModal($noWorkOrders, $scope);
                                            return;
                                        }

                                        if (response.countExceeded) {
                                            var $largeDataNotice = multiModal.setBasicOptions('notification', 'largeDataNotice', 'The selected repair shop has a high data volume. Work orders starting from ' + response.startingDate + ' will be listed.');
                                            $largeDataNotice = multiModal.setActions($largeDataNotice, function () {
                                                goToAddVisit(currentRepairShopNo, visitDate, fromStartedVisit);
                                            });

                                            multiModal.openModal($largeDataNotice, $scope);
                                            return;
                                        }

                                        goToAddVisit(currentRepairShopNo, visitDate, fromStartedVisit);
                                    });
                                }).fail(function (error) {
                                    WL.Logger.error("[DepotDetailsController][addContainersToVisit] username: " + userContext.getUsername() + " " + JSON.stringify(error));
                                });
                        	}).fail(function(err){
                            	var failMessage = multiModal.setBasicOptions('notification', 'failMessage', 'Could not connect to the server. Please try again later.');
                            	multiModal.openModal(failMessage, $scope);
                                $ionicLoading.hide();
                            });
                        }).fail(function(err){
                        	var failMessage = multiModal.setBasicOptions('notification', 'failMessage', 'Could not connect to the server. Please try again later.');
                        	multiModal.openModal(failMessage, $scope);
                            $ionicLoading.hide();
                        });
                }
            }).fail(function () {
                WL.Logger.error("[DepotDetailsController][addContainersToVisit][Test Signal] username: " + userContext.getUsername() + " Internet connection not available.");
                $ionicLoading.hide();
                multiModal.openModal($confirmOfflineModeModal, $scope);
                return;
            });
        };

        function goToAddVisit(repairShopNo, visitDate, fromStartedVisit) {
            $planVisit.setShopCode(repairShopNo);
            var screen;

            if (fromStartedVisit) {
                screen = "/addVisitTab/" + visitDate + '/' + $scope.obj.visitId + '/' + true;
            } else {
                screen = "/addVisitTab/" + visitDate + '/' + $scope.obj.visitId;
            }

            $scope.$evalAsync(function () {
                $location.path(screen);
            });
        }

        $scope.requestTabOpen = true;

        /**
         * @name removeRequestEntry
         * @desc Removes a work order and associated info/photos from a visit
         * @param {String} workOrderId
         * @param {String} auditResult 
         * @returns N/A
         * @memberOf Controllers.DepotDetailsCtrl
         */
        function removeRequestEntry(workOrderId, auditResult) {
            var inspectionStatus = $scope.obj.inspectionStatus;
            var $confirmRemove = multiModal.setBasicOptions('confirm', 'confirmRemove', 'Are you sure you want to remove this container?');
            $confirmRemove = multiModal.setActions($confirmRemove, function () {
                $ionicLoading.show(loaderConfig);
                removeWO(workOrderId, inspectionStatus)
                    .then(function () {
                        $ionicLoading.hide();
                    })
                    .fail(function (error) {
                        // something went wrong error - TODO
                        var $vFailed = multiModal.setBasicOptions('notification', 'vFailed', 'Something went wrong. Please try again later.');
                        multiModal.openModal($vFailed, $scope);
                        $ionicLoading.hide();
                    });
            });

            // Modal for removing the last container from visit (result is "Dismissed visit")
            var $confirmRemoveAndDismiss = multiModal.setBasicOptions('confirm', 'confirmRemoveAndDismiss', 'Removing the last work order will dismiss the visit. Are you sure you want to complete this action?');
            $confirmRemoveAndDismiss = multiModal.setActions($confirmRemoveAndDismiss, function () {
                $ionicLoading.show(loaderConfig);
                removeLastWO(workOrderId, inspectionStatus);
            });

            switch (inspectionStatus) {
                case Messages.planned:
                case Messages.started:
                    if (auditResult == "Accepted" || auditResult == "Rejected") {
                        // Case for finished inspection
                        var $finishedInspection = multiModal.setBasicOptions('notification', 'finishedInspection', "You can not remove a container from a finished inspection.");
                        multiModal.openModal($finishedInspection, $scope);
                    } else if ($scope.obj.workOrdersForAudit.length == 1) {
                        multiModal.openModal($confirmRemoveAndDismiss, $scope);
                    } else {
                        multiModal.openModal($confirmRemove, $scope);
                    }
                    break;
                default:
                    // Case for Finished and Missed
                    var $vOptionsPast = multiModal.setBasicOptions('notification', 'noContainersFound', "You can not remove a container from " + inspectionStatus + " visits.");
                    multiModal.openModal($vOptionsPast, $scope);
            }
        }

        /**
         * @name removeWO
         * @desc Removes a work order and associated info/photos from a visit,
         * if online removes work order from visit and sets it to available in the db
         * else marks work order
         * to be removed and made available on sync with server
         * @param {String} workOrderId
         * @param {String} inspectionStatus 
         * @returns N/A
         * @memberOf Controllers.DepotDetailsCtrl
         */
        function removeWO(workOrderId, inspectionStatus) {
            var defer = $.Deferred();

            var handler = []
            auditService.testSignal().then(function () {
                handler.push(auditService.setWorkOrderToAvailable(workOrderId));
                return handleRemoveWOArray(handler, workOrderId, inspectionStatus).then(function () {
                    $surveyModule.getStartedVisitInfo().then(function (notification) {
                        $scope.$evalAsync(function () {
                            $scope.disableUI = false;
                        });
                        defer.resolve();
                    }).fail(function (error) {
                        defer.reject(error);
                    });

                });
            }).fail(function () {
                handler.push(auditService.removeWOPartsByWorkOrderId(workOrderId));
                handleRemoveWOArray(handler, workOrderId, inspectionStatus)
                    .then(function () {
                        defer.resolve();
                    })
                    .fail(function (error) {
                        defer.reject(error);
                    })
            });

            return defer.promise();

        }

        /**
         * @name handleRemoveWOArray
         * @desc Makes the necessary updates to keep collections in sync 
         * online or offline.
         * Changes the work order "auditStatus" in "visit" collection to "Deleted"
         * Removes photos associated with work order
         * Deletes work order from "startedAuditWorkOrders" collection if 
         * in a started visit
         * @param {Array} handler - Array of functions that return promises,
         * in our case we have just one function
         * @param {String} workOrderId 
         * @param {String} inspectionStatus 
         * @returns N/A
         * @memberOf Controllers.DepotDetailsCtrl
         */
        function handleRemoveWOArray(handler, workOrderId, inspectionStatus) {
            var defer = $.Deferred();

            $.when.apply($, handler).then(function () {
                console.log("handleRemoveWOArray first apply");
                $localDB.find('visit', { workOrderId: workOrderId }).then(function (workOrder) {
                    var handleStarted = $.Deferred();

                    if (inspectionStatus == 'Started') {
                        startedAudit.removeStartedWorkOrder(workOrderId, $scope.obj.visitId).then(function () {
                            // Remove photos for this workorder
                            GalleryExportService.removePhotosForWorkOrder($scope.obj.visitId, workOrderId);
                            handleStarted.resolve();
                        });
                    } else {
                        handleStarted.resolve();
                    }

                    handleStarted.then(function () {
                        console.log("handle started ok");
                        workOrder[0].json.auditStatus = 'Deleted';
                        $localDB.replace('visit', workOrder[0], {}).then(function () {
                            if ($scope.obj.action == 'Start visit') {
                                var options2 = {
                                    filter: ['auditDate', 'auditStatus', 'containerNo', 'containerType', 'description',
                                        'repairCost', 'repairDate', 'shopCode', 'visitId', 'woStatus', 'woType', 'workOrderId', 'mode',
                                        'shopDescription', 'shopCode', 'auditresult', 'statusCode']
                                };

                                WL.JSONStore.get('visit').find({ visitId: $scope.obj.visitId }, options2).then(function (data) {
                                    // if data.length == 0 - it's no good TODO
                                    var parsedResults = [];

                                    for (var i = 0; i < data.length; i++) {
                                        if (data[i].auditstatus !== 'Deleted')
                                            parsedResults.push(data[i]);
                                    }

                                    $scope.$evalAsync(function () {
                                        $scope.obj.workOrdersForAudit = parsedResults;
                                        defer.resolve();
                                    });
                                });
                            }
                            else {
                                $localDB.find('startedAuditWorkOrders', { visitId: $scope.obj.visitId }, options).then(function (workOrders) {

                                    $scope.obj.workOrdersForAudit = [];
                                    workOrders.forEach(function (item) {
                                        if (item.auditresult == "1" || item.auditresult === true) {
                                            item.auditresult = "Accepted";
                                        } else if (item.auditresult == "0" || item.auditresult === false) {
                                            item.auditresult = "Rejected";
                                        } else {
                                            item.auditresult = "Pending";
                                        }

                                        $scope.$evalAsync(function () {
                                            $scope.obj.workOrdersForAudit.push(item);
                                        })
                                    });
                                    defer.resolve();
                                });
                            }

                        });
                    }).fail(function (error) {
                        defer.reject(error);
                    });
                });
            }).fail(function (error) {
                WL.Logger.error("[DepotDetailsController][removeWO] username: " + userContext.getUsername() + JSON.stringify(error));
                var $pFailed = multiModal.setBasicOptions('notification', 'pFailed', 'Could not connect to the server. Please try again later.');
                multiModal.openModal($pFailed, $scope);
                defer.reject();
            });

            return defer.promise();
        }

        /**
         * @name removeLastWO
         * @desc Removes last work order in a visit,
         * acts as canceling the visit
         * shows the email notification modal if online
         * @param {String} workOrderId
         * @param {String} inspectionStatus 
         * @returns N/A
         * @memberOf Controllers.DepotDetailsCtrl
         */
        function removeLastWO(workOrderId, inspectionStatus) {

            auditService.testSignal().then(function () {
                auditService.cancelAudit($scope.obj.visitId).then(function () {
                    $surveyModule.refreshVisitData().then(function () {
                        $ionicLoading.hide();
                        EmailNotificationService.setCallback(function () {
                            $ionicLoading.show(loaderConfig);
                            startedAudit.checkForStartedVisit().then(function () {
                                $ionicLoading.hide();
                                if (inspectionStatus === 'Started') {
                                	$scope.$parent.$parent.$parent.isVisitStarted = startedAudit.isVisitStarted();
                                    $scope.$parent.$parent.$parent.setView('upcoming');
                                } else {
                                	$scope.$evalAsync(function () {
                                        $location.switchTab('/surveyTab');
                                    });
                                }
                                ToastService.showNotificationMsg('Your visit has been successfully canceled.');
                            });
                        });
                        EmailNotificationService.showModal('cancelVisit', $scope, $scope.obj.workOrdersForAudit[0].shopcode);
                    }).fail(function () {
                        $ionicLoading.hide();
                        // something went wrong - couldn't refresh TODO
                    });
                });
            }).fail(function () {
                removeWO(workOrderId, inspectionStatus).then(function () {
                    console.log("removeWO resolved");
                    auditService.cancelAuditOffline($scope.obj.visitId).then(function () {
                        $surveyModule.refreshVisitDataOffline().then(function () {
                            startedAudit.checkForStartedVisit().then(function () {
                                $ionicLoading.hide();
                                if (inspectionStatus === 'Started') {
                                	$scope.$parent.$parent.$parent.isVisitStarted = startedAudit.isVisitStarted();
                                    $scope.$parent.$parent.$parent.setView('upcoming');
                                } else {
                                	$scope.$evalAsync(function () {
                                        $location.switchTab('/surveyTab');
                                    });
                                }
                            });
                        }).fail(function (error) {
                            console.log("error", JSON.stringify(error));
                            $ionicLoading.hide();
                            // something went wrong - couldn't refresh TODO
                        });
                    });
                }).fail(function (error) {
                    console.log("error", JSON.stringify(error));
                });
            });
        }

        $scope.res = function () {
            $ionicScrollDelegate.resize();
        }

        /**
         * @name startOrStopVisit
         * @desc Depending on the visit status
         * either provides functionality for starting a visit
         * or is disabled
         * @param {String} inspectionStatus
         * @returns N/A
         * @memberOf Controllers.DepotDetailsCtrl
         */
        function startOrStopVisit(inspectionStatus) {
            switch (inspectionStatus) {
                case Messages.planned:
                    $ionicLoading.show(loaderConfig);
                    startedAudit.checkForStartedVisit().then(function (visitStarted) {

                        if (visitStarted) {
                            $ionicLoading.hide();
                            multiModal.openModal($failedStartVisit, $scope);
                        } else {
                            auditService.testSignal().then(function () {
                                startVisitOnline();
                            }).fail(function () {
                                startVisitOffline();
                            });
                        }
                    });
                    break;
                case Messages.started:
                    var $goToStartedTab = multiModal.setBasicOptions('notification', 'goToStartedTab', 'You will be directed to the started audit tab in order to complete your visit.');

                    $goToStartedTab = multiModal.setActions($goToStartedTab, function () {
                        // send to start audit tab
                        $ionicLoading.show(loaderConfig);
                        $surveyModule.getStartedVisitInfo().then(function () {
                            startedAudit.checkForStartedVisit().then(function () {
                                $ionicLoading.hide();
                                $scope.$evalAsync(function () {
                                    $location.switchTab('/surveyTab/started');
                                });
                            });
                        });
                    });

                    multiModal.openModal($goToStartedTab, $scope);
                    break;
                case Messages.finished:
                    if (inspectionStatus.localeCompare(Messages.finished) == 0)
                        return;
                    break;
                case Messages.missed:
                    if (inspectionStatus.localeCompare(Messages.missed) == 0)
                        return;
                    break;
            }
            ;
        }

        // Notification modal for starting a visit if another one is already started

        var $failedStartVisit = multiModal.setBasicOptions('notification', 'failedStartVisit', 'You can\'t start this visit. Another visit is already in progress.');
        var $failedStartVisitNoContainer = multiModal.setBasicOptions('notification', 'failedStartVisitNoContainer', 'You can\'t proceed to start a visit if there are no containers to inspect. Please add containers in order to be able to perform this operation.');
        var $failedFinishVisitNoContainer = multiModal.setBasicOptions('notification', 'failedFinishVisitNoContainer', 'You can\'t proceed to finish a visit if there are no inspected containers. Please add containers in order to be able to perform this operation.');

        // Notification modals for add containers to visit (+ button from visit details)
        var $addContainerMissedVisit = multiModal.setBasicOptions('notification', 'addContainerMissedVisit', 'You are not able to add containers to a Missed Visit. Please reschedule this visit in order to be able to perform this operation.');
        var $addContainerFinishedVisit = multiModal.setBasicOptions('notification', 'addContainerFinishedVisit', 'You are not able to add containers to a Finished Visit.');

        //	Notification and confirm modals for reschedule

        var $rescheduleConfirm = multiModal.setBasicOptions('confirm', 'rescheduleConfirm', 'Are you sure you want to reschedule this visit?');
        var $rescheduleNotification = multiModal.setBasicOptions('notification', 'rescheduleNotification', 'The visit has been rescheduled.');
        $rescheduleNotification = multiModal.setActions($rescheduleNotification, function () {
            $ionicLoading.show(loaderConfig);
            startedAudit.checkForStartedVisit().then(function () {
                $ionicLoading.hide();
                $scope.$evalAsync(function () {
                    $location.switchTab('/surveyTab');
                });
            });
        });

        var $rescheduleNotificationNo = multiModal.setBasicOptions('notification', 'rescheduleNotificationNo', 'The visit can\'t be rescheduled because all containers are already planned.');
        var $rescheduleConfirmLessContainers = multiModal.setBasicOptions('confirm', 'rescheduleConfirmLessContainers', 'Are you sure you want to reschedule this visit?\n ');
        //	 reschedule datepicker setup

        var disabledDates = [];
        var weekDaysList = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];
        var monthList = ["Jan", "Feb", "March", "April", "May", "June", "July", "Aug", "Sept", "Oct", "Nov", "Dec"];

        /**
         * @name datePickerCallback
         * @desc function called on choosing an action in datepicker modal
         * for rescheduling
         * handles rescheduling the date of the visit
         * @param {String} rescheduledDate
         * @param {Bool} isCloseButton
         * @returns N/A
         * @memberOf Controllers.DepotDetailsCtrl
         */
        var datePickerCallback = function (rescheduledDate, isCloseButton) {
            if (isCloseButton) return;
            if (typeof (rescheduledDate) !== 'undefined') {

                // OBS - if we choose a date that's in the past we get the current date

                var today = new Date();
                today.setHours(0, 0, 0, 0);

                if (rescheduledDate.getTime() < today.getTime()) {
                    // you can't schedule a visit in the past
                    var $dateInFuture = multiModal.setBasicOptions('notification', 'dateInFuture', 'A visit cannot be scheduled for a past date.');
                    multiModal.openModal($dateInFuture, $scope);
                    return;
                }

                $scope.datepickerObject.inputDate = rescheduledDate;
                var visitId = $scope.obj.visitId;
                var stringDate = $filter('date')(rescheduledDate, 'yyyy-MM-dd');

                rescheduledDate = new Date(stringDate);


                $rescheduleConfirm = multiModal.setActions($rescheduleConfirm, function () {
                    $ionicLoading.show(loaderConfig);

                    auditService.rescheduleAudit(visitId, rescheduledDate).then(function (updateStatementResult) {
                        var updateCount = updateStatementResult.updateCount;
                        multiModal.resetPause();
                        var $rescheduleNotice;

                        var shopCode = $scope.obj.repairShopDetails.shopCode;

                        $ionicLoading.hide();

                        if (updateCount == $scope.obj.workOrdersForAudit.length) {
                            // all work orders rescheduled
                            $rescheduleNotice = multiModal.setBasicOptions('notification', 'rescheduleNotice', 'The audit has been rescheduled to ' + stringDate + '.');
                            $rescheduleNotice = multiModal.setActions($rescheduleNotice, function () {
                                refreshVisitsAndUI();
                            });

                            multiModal.openModal($rescheduleNotice, $rootScope, true);
                        } else if (updateCount > 0 && updateCount < $scope.obj.workOrdersForAudit.length) {
                            $rescheduleNotice = multiModal.setBasicOptions('notification', 'rescheduleNotice', 'The audit has been rescheduled with the available work orders to ' + stringDate + '.');
                            $rescheduleNotice = multiModal.setActions($rescheduleNotice, function () {
                                refreshVisitsAndUI();
                            });

                            multiModal.openModal($rescheduleNotice, $rootScope, true);
                        } else if (updateCount == 0) {
                            $rescheduleNotice = multiModal.setBasicOptions('notification', 'rescheduleNotice', 'The current audit information is no longer available, you will be redirected to the inspections tab.');
                            $rescheduleNotice = multiModal.setActions($rescheduleNotice, function () {
                                $surveyModule.refreshVisitData().then(function () {
                                    startedAudit.checkForStartedVisit().then(function () {
                                        $scope.$evalAsync(function () {
                                            $ionicLoading.hide();
                                            $location.switchTab('/surveyTab');
                                        });
                                    });
                                }).fail(function () {
                                    startedAudit.checkForStartedVisit().then(function () {
                                        $scope.$evalAsync(function () {
                                            $ionicLoading.hide();
                                            $location.switchTab('/surveyTab');
                                        });
                                    });
                                });
                            });

                            multiModal.openModal($rescheduleNotice, $rootScope, true);
                        } else {
                            // something went wrong!
                            $errorFeedback = multiModal.setBasicOptions('notification', 'connectionError', 'Something went wrong, try again.');
                            multiModal.openModal($errorFeedback, $rootScope, true);
                        }
                    });
                });

                multiModal.openModal($rescheduleConfirm, $scope);
            } else {
                var $dateInFuture = multiModal.setBasicOptions('notification', 'dateInFuture', 'A visit cannot be scheduled for a past date.');
                multiModal.openModal($dateInFuture, $scope);
            }
        };

        /**
         * @name refreshVisitsAndUI
         * @desc syncs user visit info with the server
         * and updates UI to reflect change of date
         * @returns N/A
         * @memberOf Controllers.DepotDetailsCtrl
         */
        function refreshVisitsAndUI() {
            $ionicLoading.show(loaderConfig);
            $surveyModule.refreshVisitData().then(function () {
                // we need to know auditStatus of visit at this point, might be missed or planned
                $depot.getVisitInfo($scope.obj.visitId).then(function (data) {
                    // need to send email at this point
                    if (data.length > 0) {
                        var shopCode = data[0].shopcode;
                        var auditDate = data[0].auditdate
                        var parsedData = data.map(function (obj) {
                            var rObj = {};
                            rObj.containerNo = obj.containerno;
                            rObj.workOrderId = obj.workorderid;
                            return rObj;
                        });
                        repairShopService.getShopEmail(shopCode).then(function (shopEmail) {
                            repairShopService.sendEmailToShop(shopEmail, userContext.getEmail(), auditDate, shopCode, parsedData, { rescheduled: true });
                        });
                    }

                    $timeout(function () {
                        $scope.obj.workOrdersForAudit = data;
                    });
                    $ionicLoading.hide();
                }).fail(function (err) {
                    if (typeof err === 'object') {
                        if (err.notFound) {
                            $depot.getVisitInfo($scope.obj.visitId, 'Missed').then(function (data) {
                                $ionicLoading.hide();
                                $timeout(function () {
                                    $scope.obj.workOrdersForAudit = data;
                                });
                            }).fail(function () {
                                $ionicLoading.hide();
                            });
                        }
                    }
                });
            }).fail(function () {
                // something went wrong while syncing
                $ionicLoading.hide();
            });
        }

        $scope.datepickerObject = setSmallDatePickerConfig(disabledDates, weekDaysList, monthList, datePickerCallback);

        $scope.datepickerObject.from.setDate($scope.datepickerObject.from.getDate() - 1);

        $scope.onStatusIconClick = function (workOrder) {
            if (workOrder.mercstatus == "FAILED") {
                var comment = FailMsgService.getFaildComment(workOrder.merccomments);
                var $commentNotif = multiModal.setBasicOptions('notification', 'rejectedStatusModal', comment, 'popup');
                multiModal.openModal($commentNotif, $scope);
            }
        }

        /**
         * @name populatePartsAndRepairsOnline
         * @desc Populates the '$unitRefer' factory with repairs and parts data
         * which are used for the inspection screen,
         * getting data from the server
         * @param N/A
         * @returns {Promise}
         * @memberOf Controllers.DepotDetailsCtrl
         */
        function populatePartsAndRepairsOnline(workNo, status, containerNo, woStatus, visit) {
            var dfd = $.Deferred();

            auditService.getWorkOrderParts(workNo).then(function (result) {
                // This is where the update is made
                auditService.updateWorkOrderParts(result, $scope.obj.visitId).then(function () {
                    $unitRefer.setParts(result);
                    $unitRefer.setInspectionStatus($scope.obj.inspectionStatus, status).then(function () {
                        dfd.resolve();
                    });
                });
            }).fail(function (error) {
                dfd.reject(error);
            });

            return dfd.promise();
        }

        /**
         * @name populatePartsAndRepairsOffline
         * @desc Populates the '$unitRefer' factory with repairs and parts data
         * which are used for the inspection screen,
         * getting data from json store
         * @param N/A
         * @returns {Promise}
         * @memberOf Controllers.DepotDetailsCtrl
         */
        function populatePartsAndRepairsOffline(workNo, inspectionStatus, containerNo, woStatus, visit) {
            var dfd = $.Deferred();

            auditService.getLocalWorkOrderParts(workNo).then(function (result) {
                $unitRefer.setParts(result);
                $unitRefer.setInspectionStatus($scope.obj.inspectionStatus, inspectionStatus).then(function () {
                    dfd.resolve();
                });
            }).fail(function () {
                var $noDataOfflineModeModal = multiModal.setBasicOptions('notification', 'noDataJSON', 'No data stored locally for this Work Order', 'popup');
                $noDataOfflineModeModal = multiModal.setActions($noDataOfflineModeModal, function () {
                    console.log("You shall not pass!!");
                });
                multiModal.openModal($noDataOfflineModeModal, $scope);
                $ionicLoading.hide();
                dfd.reject();
            });

            return dfd.promise();
        }

        /**
         * @name startVisitOffline
         * @desc updates the local collections to reflect visit has started
         * @param N/A
         * @returns N/A
         * @memberOf Controllers.DepotDetailsCtrl
         */
        function startVisitOffline() {
            //chek if workWorder have access to all the parts
            auditService.checkForLocalDataOnStartVisit($scope.obj.visitId).then(function () {
                auditService.startAuditOffline($scope.obj.visitId).then(function (updateCount) {
                    if (updateCount > 0) {
                        $surveyModule.refreshVisitDataOffline().then(function () {
                            //	It may be necessary to init parts also - TODO
                            startedAudit.initStartedWorkOrders($scope.obj.visitId).then(function () {
                                WL.Analytics.log({
                                    AC_action: "Start Visit",
                                    AC_userName: userContext.getUsername(),
                                    AC_shop: $scope.obj.repairShopDetails.shopCode,
                                    AC_timestamp: getUTCDate()
                                }, 'Start Visit');
                                $depot.startInspection(function () {
                                    // Redirect to started tab
                                    startedAudit.checkForStartedVisit().then(function () {
                                        $scope.$evalAsync(function () {
                                            $location.path('/surveyTab/started');
                                            $ionicLoading.hide();
                                        });
                                    });
                                });
                            })
                        });
                    } else {
                        var $noDataOfflineModeModal = multiModal.setBasicOptions('notification', 'noDataJSON', 'No data stored locally for this Work Order', 'popup');
                        $noDataOfflineModeModal = multiModal.setActions($noDataOfflineModeModa);
                        multiModal.openModal($noDataOfflineModeModal, $scope);
                        $ionicLoading.hide();
                    }
                });
            }).fail(function () {
                var $woIncompleteDataModal = multiModal.setBasicOptions('notification', 'woIncompleteData', 'Some work orders asociated to this vist do not have all the data stored locally', 'popup');
                $woIncompleteDataModal = multiModal.setActions($woIncompleteDataModal);
                multiModal.openModal($woIncompleteDataModal, $scope);
                $ionicLoading.hide();
            });
        }

        /**
         * @name startVisitOnline
         * @desc Updates visit data on db and locally
         * @param N/A
         * @returns N/A
         * @memberOf Controllers.DepotDetailsCtrl
         */
        function startVisitOnline() {
            $ionicLoading.hide();
            PendingAuditsService.runVisitQueue().then(function () {
                $ionicLoading.show(loaderConfig);
                auditService.startAudit($scope.obj.visitId).then(function (updateCount) {
                    console.log('updateCount', updateCount);
                    if (updateCount > 0) {
                        $unitRefer.resetLocalStorage();
                        $surveyModule.refreshVisitData().then(function () {
                            //	It may be necessary to init parts also - TODO
                            // If refresh fails should we refresh offline - TODO
                            startedAudit.initStartedWorkOrders($scope.obj.visitId).then(function () {
                                WL.Analytics.log({
                                    AC_action: "Start Visit",
                                    AC_userName: userContext.getUsername(),
                                    AC_shop: $scope.obj.repairShopDetails.shopCode,
                                    AC_timestamp: getUTCDate()
                                }, 'Start Visit');
                                $depot.startInspection(function () {
                                    // Redirect to started tab
                                    startedAudit.checkForStartedVisit().then(function () {
                                        $scope.$evalAsync(function () {
                                            $location.path('/surveyTab/started');
                                            $ionicLoading.hide();
                                        });
                                    });
                                });
                            })
                        });
                    } else {
                        $ionicLoading.hide();
                        // display modal? - TODO
                    }
                });
            }).fail(function () {
                var actionFailed = multiModal.setBasicOptions('notification', 'actionFailed', 'Could not process your request.');
                multiModal.openModal(actionFailed, $scope);
                $ionicLoading.hide();
            });

        }

        /**
         * @name checkIfFileExists
         * @desc Looks in the app persistent folder 
         * and returns true if finds the file at given path
         * @param {String} path - file path
         * @returns {Promise} - true or false 
         * @memberOf Controllers.DepotDetailsCtrl
         */
        function checkIfFileExists(path) {
            var dfd = $.Deferred();
            var fileExists = false;

            window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function (fileSystem) {
                fileSystem.root.getFile(path, { create: false }, function () {
                    fileExists = true;
                    dfd.resolve(fileExists);
                }, function (error) {
                    dfd.resolve(fileExists);
                });
            }, function (error) {
                dfd.reject(error);
            });

            return dfd.promise();
        }

        //----------------------------------History and Remarks SECTION -----------------------------------

        var $noVisits = multiModal.setBasicOptions('notification', 'containerHistory', 'There are no prior visits to this container.', 'popup');
        var $noHistoryRemarks = multiModal.setBasicOptions('notification', 'containerHistoryRemarks', 'There are no remarks for this container.', 'popup');

        // Pagination config object
        $scope.config = {
            pagesLimit: 3,
            entriesPerPage: 7,
            arrayLength: 0,
            pageNo: 1
        };

        //History object to keep track of data, containerId, woId
        $scope.historyObject = {
            containerId: null,
            woId: null,
            workOrderArray: [],
            repairWorkOrders: [],
            partialHistoryRemarksArray: [],
            historyRemarksArray: [],
            currentView: null,
            currentTemplate: null
        };

        /**
         * @desc Used to show/hide remarks for a row, and recalculate scroll
         * @param item - a remark item
         */
        $scope.showHideRemarks = function (item) {
            if (item.displayRemarks) {
                item.displayRemarks = false;
                $ionicScrollDelegate.$getByHandle('mainScroll').resize();
            } else {
                 //Close all other remarks 
                $scope.historyObject.partialHistoryRemarksArray.forEach(function (remark) {
                    remark.displayRemarks = false;
                });
                // Expand current remark item
                item.displayRemarks = true;
                //If item is one of last 3 in partialHistoryRemarksArray and we expand it, scroll to bottom and update scroll
                var indexOfCurrentItemInArray = $scope.historyObject.partialHistoryRemarksArray.indexOf(item);
                //If we pass the half of the screen, just scroll to bottom to render all text
                if(indexOfCurrentItemInArray >= 3){
                    $ionicScrollDelegate.$getByHandle('mainScroll').scrollBottom();
                }
            }
        };

        /**
         * @name showHistory
         * @desc Open a modal to show the history of a container
         * @param {String} containerNo the id of a container
         * @returns {Void} 
         * @memberOf Controllers.AddVisitController
         */
        function showHistory(containerNo) {
            //Build the promise
            var defer = $.Deferred();

            // User can navigate though History and Remarks until the modal is closed
            if (!!$scope.historyObject.workOrderArray && !!$scope.historyObject.workOrderArray.length) {
                //Update the modal variables
                $scope.historyObject.currentView = 'history';
                $scope.historyObject.currentTemplate = 'templates/common/modalHistory.html';
                defer.resolve();
            } else {
                containerHistoryService.getContainerHistory(containerNo).then(function (history) {
                    $scope.historyObject.workOrderArray = containerHistoryService.getContainerHistoryArray();

                    if ($scope.historyObject.workOrderArray.length < 1) {
                        defer.reject({ 'errorCode': 101, 'error': 'No containers' });
                        return;
                    }

                    $scope.historyObject.repairWorkOrders = containerHistoryService.getContainerHistoryRepair();
                    //Update the modal variables
                    $scope.historyObject.currentView = 'history';
                    $scope.historyObject.currentTemplate = 'templates/common/modalHistory.html';
                    // We have history, proceed to display the modal
                    defer.resolve();

                }, function (err) {
                    defer.reject({ 'errorCode': 102, 'error': err });
                    console.log("AddVisitController[showHistory] error " + err);
                    return;
                });
            }

            return defer.promise();
        };

        /**
         * Retrieve history remarks 
         */
        $scope.showHistoryRemarks = function (woId) {
            var defer = $.Deferred();

            if (!!$scope.historyObject.historyRemarksArray && !!$scope.historyObject.historyRemarksArray.length) {
                //Update the modal variables
                $scope.historyObject.currentView = 'remarks';
                $scope.historyObject.currentTemplate = 'templates/common/modalRemarks.html';
                //Create a sub-array for pagination starting with page 1 using config and historyObject
                $scope.historyObject.partialHistoryRemarksArray = $scope.historyObject.historyRemarksArray.slice(0, $scope.config.entriesPerPage);
                defer.resolve();
            } else {
                containerHistoryService.getWorkOrderRemarksHistoryArray(woId)
                    .then(function (results) {
                        console.log(results);
                        //If there are no results, we can disiplay nothing
                        if (results.length < 1) {
                            defer.reject({ 'errorCode': 101, 'error': 'No remarks' })
                            return;
                        }

                        //Limit the number of comments to 21
                        results = results.slice(0, 21);
                        //Attach history remarks data on scope
                        $scope.historyObject.historyRemarksArray = results;
                        //Configure pagination directive
                        $scope.config.arrayLength = results.length;
                        //Create a sub-array for pagination starting with page 1 using config and historyObject
                        $scope.historyObject.partialHistoryRemarksArray = results.slice(0, $scope.config.entriesPerPage);
                        //Update the modal variables
                        $scope.historyObject.currentView = 'remarks';
                        $scope.historyObject.currentTemplate = 'templates/common/modalRemarks.html';
                        //We have all data, resolve the promise
                        defer.resolve();
                    }, function (err) {
                        defer.reject({ 'errorCode': 102, 'error': err });
                        console.log(err);
                    });
            }

            return defer.promise();
        };

        /**
         * Display history modal with History or Remarks
         */
        $scope.showVisitDetails = function (containerNo, woId, view) {
            //Set the local variables to handle switch between views
            $scope.historyObject.containerId = containerNo;
            $scope.historyObject.woId = woId;

            if (view === 'history') {
                $ionicLoading.show(serverLoaderConfig);
                //Load data for container's history
                $scope.showHistory(containerNo).then(function () {
                    $ionicLoading.hide();
                    multiModal.openModal($historyModal, $scope);
                }, function (err) {
                    $ionicLoading.hide();
                    if (err.errorCode == 101)
                        multiModal.openModal($noVisits, $scope);
                });

            } else if (view === 'remarks') {
                $ionicLoading.show(serverLoaderConfig);
                //Call function to retrieve remarks history for the provided WO id
                $scope.showHistoryRemarks(woId).then(function () {
                    $ionicLoading.hide();
                    multiModal.openModal($historyModal, $scope);
                }, function (err) {
                    $ionicLoading.hide();
                    if (err.errorCode == 101)
                        multiModal.openModal($noHistoryRemarks, $scope);
                })

            }
        };

        /**
         * @name switchVisitDetailsHistoryRemarks
         * @desc Used to switch between visitDetailsHistory tabs (history or remarks)
         * @param {String} view - used to call specific function for each type of view
         * @returns {Void} 
         * @memberOf Controllers.AddVisitController
         */
        $scope.switchVisitDetailsHistoryRemarks = function (view) {
            $ionicLoading.show(serverLoaderConfig);
            if (view == 'history') {
                $scope.showHistory($scope.historyObject.containerId).then(function () {
                    $ionicLoading.hide();
                }, function (err) {
                    console.log(err);
                    $ionicLoading.hide();
                    if (err.errorCode == 101)
                        multiModal.openModal($noVisits, $scope);
                })
            } else if (view == 'remarks') {
                $scope.showHistoryRemarks($scope.historyObject.woId).then(function () {
                    $ionicLoading.hide();
                }, function (err) {
                    $ionicLoading.hide();
                    if (err.errorCode == 101)
                        multiModal.openModal($noHistoryRemarks, $scope);
                })
            }
        };

        /**
         * Reset history object when the history modal is closed 
         */
        $scope.resetHistoryData = function () {
            //Reset the history object to avoid displaying same data for different WOs/Containers
            $scope.historyObject = {
                containerId: null,
                woId: null,
                workOrderArray: [],
                repairWorkOrders: [],
                historyRemarksArray: [],
                currentView: null,
                currentTemplate: null
            }
        };

        /**
         * Called by pagination directive to update data to be displayed in the current page
         */
        $scope.changePageResults = function () {
            var defer = $.Deferred();
            console.log($scope.config);

            var offset = ($scope.config.pageNo - 1) * $scope.config.entriesPerPage;

            $scope.historyObject.partialHistoryRemarksArray = $scope.historyObject.historyRemarksArray.slice(offset, offset + $scope.config.entriesPerPage);

            defer.resolve();

            return defer.promise();
        }



        //----------------------------------END OF History and Remarks SECTION -----------------------------------


    }
})();