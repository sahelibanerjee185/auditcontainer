/**
 * June, 16 2017
 * PreviewPdfController.js
 * Stavarache Vlad
*/

/**
 * PreviewPdfCtrl Controller
 * @namespace Controllers
 */
(function(){
	'use strict';
	
	audit.controller('PreviewPdfCtrl', PreviewPdfCtrl);
	
	PreviewPdfCtrl.$inject = ['$surveyModule', '$scope', 'pdfGenerationHandler', 'finishVisitProgressService', '$ionicLoading',
	'multiModal', 'startedAudit', 'auditService', 'PendingAuditsService', 'AuditPollingService', 'EmailNotificationService', 
	'$depot', '$location'];
	
	/**
	 * @namespace PreviewPdfCtrl
	 * @desc Controller used to display visit preview PDF and use finish visit functionality
	 * @memberOf Controllers
	 */
	function PreviewPdfCtrl($surveyModule, $scope, pdfGenerationHandler, finishVisitProgressService,
	$ionicLoading, multiModal, startedAudit, auditService, PendingAuditsService, AuditPollingService, EmailNotificationService,
	$depot, $location) {
		
	    $scope.cancel = cancel;
	    $scope.cleanupVisitObj = cleanupVisitObj;
	    $scope.completeVisit = completeVisit;
	    $scope.currentStep = 1;
	    $scope.disableFinishVisit = false;
	    $scope.finish = finish;
	    $scope.finishVisit = finishVisit;
	    $scope.obj = $surveyModule.getObj();
	    $scope.obj = pdfGenerationHandler.generatePdfModel($scope.obj);
	    $scope.preFinishVisitConfirmation = preFinishVisitConfirmation;
	    $scope.previewPdf = previewPdf;
	    $scope.steps = {};
	    $scope.retry = retry;
	    
	    // Finish visit modal options
	    var $finishVisitModal = multiModal.setBasicOptions('notification', 'finishVisit', null, 'popup');
	    $finishVisitModal = multiModal.setTemplateFile($finishVisitModal, 'templates/components/finishVisitModal.html');
	
	
	    function completeVisit() {
	        multiModal.removeAllModals();
	        // $scope.cleanupVisitObj();
	        $ionicLoading.show(loaderConfig);
	        startedAudit.checkForStartedVisit().then(function(){
	        	$ionicLoading.hide();
	        	$scope.$evalAsync(function(){
	                $location.path('surveyTab/upcoming');
	            });
	        });
	    }
	
	    function cleanupVisitObj() {
	        $scope.$evalAsync(function () {
	            $scope.obj.startedAudit = [];
	            $scope.obj.startedVisitInfo = {};
	            $scope.obj.partsObject = {};
	        })
	    }
	
	    /**
	     * @name preFinishVisitConfirmation
	     * @desc Shows modal window to confirm finishing the visit
	     * @param N/A
	     * @returns N/A
	     * @memberOf Controllers.PreviewPdfCtrl
	     */
	    function preFinishVisitConfirmation() {
	        var visitId = $scope.obj.startedVisitInfo.visitId;
	
	        setTimeout(function () {
	            $scope.disableFinishVisit = false;
	        }, 400);
	        
	        if ($scope.disableFinishVisit) {
	            return;
	        }
	        $scope.disableFinishVisit = true;
	
	        var startedTabScope = angular.element(document.getElementById('pdfControllerDiv')).scope();
	
	        if (signaturesAndAgreementsAreOk(startedTabScope)) {
	            startedAudit.getStartedWorkOrders(visitId).then(function (workOrderInfo) {
	                var finishedWorkOrders = workOrderInfo.finishedWorkOrders;
	                var allWorkOrders = workOrderInfo.allWorkOrders;
	
	                var $finishAudit = getModalOptions(allWorkOrders, finishedWorkOrders);
	
	                $finishAudit = multiModal.setActions($finishAudit, function () {
	                    $scope.finishVisit(finishedWorkOrders);
	                });
	
	                multiModal.openModal($finishAudit, $scope);
	            });
	        } else {
	            $ionicLoading.hide();
	            signatureAndAgreementsFeedback(startedTabScope);
	        }
	    }
	
	    /**
	     * @name finishVisit
	     * @desc function that runs finish visit functionality,
	     * generating and uploading pdf, uploading galllery 
	     * and writing to the database
	     * @param {Array} finishedWorkOrders - Work orders that have been inspected
	     * @returns N/A
	     * @memberOf Controllers.PreviewPdfCtrl
	     */
	    function finishVisit(finishedWorkOrders) {
	        $ionicLoading.show(loaderConfig);
	        var visitId = $scope.obj.startedVisitInfo.visitId;
	        var startedTabScope = angular.element(document.getElementById('pdfControllerDiv')).scope();
	
	        auditService.testSignal().then(function () {
	            $scope.steps = finishVisitProgressService.getFinishProcessObject();
	            finishVisitProgressService.setCallbackFunction(finishCallback);
	
	            PendingAuditsService.runVisitQueue().then(function() {
	                $ionicLoading.hide();
	                if (finishedWorkOrders.length > 0) {
	                    multiModal.openModal($finishVisitModal, $scope).then(function () {
	                        finishVisitProgressService.startFinishVisit(startedTabScope.signatures, finishedWorkOrders, $scope.obj);
	                    });
	                } else {
	                    finishVisitProgressService.startFinishVisit(startedTabScope.signatures, finishedWorkOrders, $scope.obj);
	                }
	            }).fail(function () {
	                WL.Logger.error("[CalendarController][finishVisit][PendingAuditsService.runVisitQueue] username: " + userContext.getUsername() + " " + JSON.stringify(error));
	                var actionFailed = multiModal.setBasicOptions('notification', 'actionFailed', 'Could not process your request.');
	                multiModal.openModal(actionFailed, $scope);
	                $ionicLoading.hide();
	            });
	        }).fail(function () {
	            // Finish visit offline
	            $ionicLoading.show(loaderConfig);
	            pdfGenerationHandler.setPdfContext($scope.obj);
	            pdfGenerationHandler.generatePdf(startedTabScope.signatures, false).then(function () {
	                PendingAuditsService.savePendingVisit().then(function () {
	                    // Change visit status to pending from started in "visit" collection
	                    AuditPollingService.add(visitId);
	
	                    $surveyModule.refreshVisitDataOffline().then(function () {
	                        // $scope.cleanupVisitObj();
	                        $scope.$evalAsync(function () {
	                            $scope.disableUI = false;
	                        });
	
	                        $ionicLoading.hide();
	                        var successfulFinished = multiModal.setBasicOptions('notification', 'successfulFinished', 'The status of the visit has changed to Pending. Please manually sync the visit when internet access is available.');
	                        successfulFinished = multiModal.setActions(successfulFinished, function(){
	                            completeVisit();
	                        });
	                        multiModal.openModal(successfulFinished, $scope);
	                    });
	                }).fail(function (error) {
	                    // Could not perform action
	                    var actionFailed = multiModal.setBasicOptions('notification', 'actionFailed', 'Could not process your request.');
	                    multiModal.openModal(actionFailed, $scope);
	                    $ionicLoading.hide();
	                });
	            }).fail(function (error) {
	                // Could not perform action
	                WL.Logger.error("[CalendarController][finishVisit][pdfGenerationHandler.generatePdf] username: " + userContext.getUsername() + JSON.stringify(error));
	
	                var actionFailed = multiModal.setBasicOptions('notification', 'actionFailed', 'Could not process your request.');
	                multiModal.openModal(actionFailed, $scope);
	                $ionicLoading.hide();
	            });
	        });
	    }
	
	    function cancel() {
	        hybrid.stopSync();
	        multiModal.closeCurrentModal();
	    }
	
	    function finish() {
	        multiModal.closeCurrentModal();
	        EmailNotificationService.setCallback(completeVisit);
	        EmailNotificationService.showModal('finishVisit', $scope, $scope.obj.finishedWorkOrders[0].shopcode);
	    }
	
	    function retry() {
	        finishVisitProgressService.retry();
	    }
	
	    /**
	     * @name signatureAndAgreementsFeedback
	     * @desc Shows modal window to inform user signatures are not in order
	     * @param {Onject} startedTabScope - scope
	     * @returns N/A
	     * @memberOf Controllers.PreviewPdfCtrl
	     */
	    function signatureAndAgreementsFeedback(startedTabScope) {
	        if (!startedTabScope.agreement.auditor || !startedTabScope.agreement.shopManager) {
	            var $agreementMandatory = multiModal.setBasicOptions('notification', 'agreementMandatory',
	            'Both the auditor and the shop manager need to agree to the audit before it can be finished.');
	            multiModal.openModal($agreementMandatory, $scope);
	            return;
	        }
	        if (!startedTabScope.signature.auditor || !startedTabScope.signature.shopManager) {
	            var $signatureMandatory = multiModal.setBasicOptions('notification', 'signatureMandatory',
	            'Both the auditor and the shop manager need to sign before the audit can be finished.');
	            multiModal.openModal($signatureMandatory, $scope);
	        }
	    }
	
	    function signaturesAndAgreementsAreOk(startedTabScope) {
	        if (startedTabScope.agreement.auditor && startedTabScope.agreement.shopManager && startedTabScope.signature.auditor && startedTabScope.signature.shopManager) {
	            return true;
	        } else {
	            return false;
	        }
	    }
	
	    function getModalOptions(allWorkOrders, finishedWorkOrders) {
	        if (finishedWorkOrders.length == 0) {
	            return multiModal.setBasicOptions('confirm', 'cancelVisit', 'If you finish the visit without inspecting any of the work orders, then your visit will be canceled. Are you sure you want to cancel the visit?');
	        }
	
	        if (finishedWorkOrders.length > 0 && finishedWorkOrders.length < allWorkOrders.length) {
	            return multiModal.setBasicOptions('confirm', 'notAllInspectedContainers', 'The uninspected containers in this visit will be dismissed. Are you sure you want to finish this visit?');
	        }
	
	        if (finishedWorkOrders.length == allWorkOrders.length) {
	            return multiModal.setBasicOptions('confirm', 'finishVisit', 'Are you sure you want to finish this visit?');
	        }
	    }
	
	    function finishCallback(wasCanceled, visitId, startedTabScope) {
	        $ionicLoading.show(loaderConfig);
	        $scope.$evalAsync(function() {
	            $scope.disableUI = true;
	        });
	
	        console.log("wasCanceled", wasCanceled);
	
	        auditService.testSignal().then(function () {
	            $surveyModule.refreshVisitData().then(function () {
	                if (wasCanceled) {
	                    $ionicLoading.hide();
	                    EmailNotificationService.setCallback(completeVisit);
	                    EmailNotificationService.showModal('cancelVisit', $scope, $scope.obj.startedVisitInfo.repairShopNo, getContainerList($scope.obj.startedVisitInfo.plannedWorkOrders));
	                    $scope.$evalAsync(function () {
	                        $scope.disableUI = false;
	                    });
	                } else {
	                    // Start client side polling
	                    AuditPollingService.add(visitId).then(function () {
	                        AuditPollingService.startPollIfNeeded();
	                    });
	
	                    $depot.getVisitInfo(visitId, 'Finished', true).then(function() {
	                        $ionicLoading.hide();
	                        $scope.$evalAsync(function () {
	                            $scope.disableUI = false;
	                        });
	                    }).fail(function(e) {
	                        $ionicLoading.hide();
	                        WL.Logger.error("[CalendarController][finishCallback][getVisitInfo] username: " + userContext.getUsername() + " Failed to get visit info");
	                        $scope.$evalAsync(function () {
	                            $scope.disableUI = false;
	                        });
	                    });
	                }
	            }).fail(function (error) {
	                WL.Logger.error("[CalendarController][finishCallback][refreshVisitData] username: " + userContext.getUsername() + " Failed to refresh visit data with error: " + JSON.stringify(error));
	                // could not refresh visit data online
	                $surveyModule.refreshVisitDataOffline().then(function () {
	                    $ionicLoading.hide();
	                    $scope.$evalAsync(function () {
	                        $scope.disableUI = false;
	                    });
	                });
	            });
	        }).fail(function () {
	            WL.Logger.error("[CalendarController][finishCallback][testSignal] username: " + userContext.getUsername() + " Internet failed while finishing the visit.");
	            $surveyModule.refreshVisitDataOffline().then(function() {
	                $ionicLoading.hide();
	                $scope.$evalAsync(function () {
	                    $scope.disableUI = false;
	                });
	            });
	        });
	    }
	
	    function previewPdf(visitId, pdfPreview) {
	        return pdfGenerationHandler.previewPdf(visitId, pdfPreview);
	    };
	
	    function getContainerList(workOrderInfoArray) {
			var containerList = "";
			var arrayHelper = [];
			for (var i = 0; i < workOrderInfoArray.length; i++) {
				var workOrderInfo = workOrderInfoArray[i];
				arrayHelper.push(workOrderInfo.containerno);
			}
			
			containerList = arrayHelper.join(", ");
			return containerList;
		}
	
	    $scope.$on('valueChanged', function () {
	        $scope.$evalAsync(function () {
	            $scope.steps = finishVisitProgressService.getFinishProcessObject();
	            $scope.currentStep = finishVisitProgressService.getCurrentStep();
	        });
	    });
	}
})();

