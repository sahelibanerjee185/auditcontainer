audit.controller('KpiController',['$ionicLoading','$scope','$kpiModule','$backend','$location','$timeout','multiModal','$commonOperations','$filter','KPIService', function($ionicLoading,$scope,$kpiModule,$backend,$location,$timeout,multiModal,$commonOperations,$filter,KPIService) {
	
	/*KPI CONTROLLER START*/
	$scope.graph={};
	$scope.data = {};
	$scope.graph.labels = [];
	$scope.graph.series = ['Added Containers', 'Succesful Pre-Inspections', 'Failed Pre-Inspections'];
	$scope.graph.data = [];
	 
	$scope.graph.colors = ['#004e6b','#69b8d6','#b50030'];
	
	$kpiModule.populateMainGraph();
	
	$scope.obj = $kpiModule.getObj();
	 
	$scope.graph.data.push($scope.obj.allInspections);
	$scope.graph.data.push($scope.obj.passedInspections);
	$scope.graph.data.push($scope.obj.rejectedInspections);
	 
	 $scope.someOptions = {
		 datasetFill : true,	
     };
	 $scope.graph.someOptions = {
			 datasetFill : false,	
	     };
	 //takes the last 12 months from the current date
	 
		var today = new Date();
		var aMonth = today.getMonth();
		//var months= [], i;
		
		for(i = aMonth , a = 0;  a<12 ; a++, i--){
			if(i == -1) i = 11;
			console.log(monthNames[i])
			$scope.graph.labels.unshift(monthNames[i])
		}
			
		
		 $scope.selectDepot=function(repairShopNo){
			 console.log(repairShopNo);
			 $ionicLoading.show(loaderConfig);
			 $scope.obj.shopCode = repairShopNo;
			 $kpiModule.setFailledObj().then(function(){
				 $ionicLoading.hide();
				 $location.path('/failedInspections');
			 }).fail(function(){
				 $ionicLoading.hide();
				 var $kFailed = multiModal.setBasicOptions('notification', 'kFailed', 'Something went wrong. Please try again later.');
			     multiModal.openModal($kFailed, $scope);
			 });
			 
		 }
		
	 // function for the header sorting
		$scope.defaultOrderTag = 'depotName';
		$scope.switchOrder = true;   // ascending or descending
		$scope.ShopsOrderFunction = function(defaultOrderTag) {
		    $scope.switchOrder = ($scope.defaultOrderTag === defaultOrderTag) ? !$scope.switchOrder : false;
		    $scope.defaultOrderTag = defaultOrderTag;
		};
	 
}]); 