/**
 * @namespace UnitCtrl
 * @desc Controller which handles unitRefer screen
 * @memberOf Controllers
 */
'use strict';
audit.controller('UnitCtrl', UnitCtrl);

UnitCtrl.$inject = ['$scope', '$location', 'multiModal', '$timeout', '$routeParams', '$unitRefer',
    '$depot', '$commonOperations', '$ionicLoading', '$backend', '$rootScope', '$localDB', '$galleryModule',
    'PhotoStorageDirectory', '$base64EncoderDecoder', 'GalleryExportService', 'Base64Service',
    'GalleryImportService', 'startedAudit', 'auditService', '$surveyModule', 'ArchiveService',
    'archiveModule', '$ionicScrollDelegate', '$localStorage', 'WOCodesService', 'ToastService'
];


function UnitCtrl($scope, $location, multiModal, $timeout, $routeParams, $unitRefer, $depot,
    $commonOperations, $ionicLoading, $backend, $rootScope, $localDB, $galleryModule, PhotoStorageDirectory,
    $base64EncoderDecoder, GalleryExportService, Base64Service, GalleryImportService, startedAudit, auditService,
    $surveyModule, ArchiveService, archiveModule, $ionicScrollDelegate, $localStorage, WOCodesService, ToastService) {


    /* =================================================================
     LOCAL VARIABLES
     =================================================================
     */

    //MODALS
    var $commentModal = multiModal.setBasicOptions('confirm', 'commentModal', null, 'full'),
        $commentModalNotEditable = multiModal.setBasicOptions('confirm', 'commentModal', null, 'full'),
        $openInfoStartedModal = multiModal.setBasicOptions('confirm', 'commentModal', null, 'full'),
        $resubmitModal = multiModal.setBasicOptions('confirm', 'resubmitModal', "Are you sure you want to deselect the 'Resubmit' button? All changes will be discarded and the entries will restore to their default values"),
        $failedStartVisit = multiModal.setBasicOptions('notification', 'failedStartVisit', 'You can\'t start this inspection. Another visit is already in progress.'),
        $confirmRemoveComponent = multiModal.setBasicOptions('confirm', 'confirmRemoveComponent', 'Are you sure you want to remove this component?'),
        $validationErrors = multiModal.setBasicOptions('notification', 'validationNotification', 'Please fix the red highlighted fields in order to proceed.'),
        $uniqueRepairsError = multiModal.setBasicOptions('notification', 'uniqueRepairsError', 'Note that repair lines need to have unique combinations of repair code and repair loc code.'),
        $uniquePartsError = multiModal.setBasicOptions('notification', 'uniqueRepairsError', 'Note that child parts need to have unique spare part numbers.'),
        $allLinesNotAudited = multiModal.setBasicOptions('notification', 'allLinesNotAudited', 'Note that all repair and part lines need to have an action applied.'),
        $noAddedPartsForAccepted = multiModal.setBasicOptions('notification', 'noAddedPartsForAccepted', "You can't add parts to an 'Accepted' or 'Not Required' Repair."),
        $deviationModal = multiModal.setBasicOptions('confirm', 'deviationModal', "Are you sure you want to deselect the 'Deviation' button? All changes will be discarded and the entries will restore to their default values");

    $openInfoStartedModal = multiModal.setTemplateFile($openInfoStartedModal, 'templates/common/infoModalStarted.html');
    $commentModalNotEditable = multiModal.setTemplateFile($commentModalNotEditable, 'templates/common/modalFinalComment.html');
    $commentModal = multiModal.setTemplateFile($commentModal, 'templates/common/modalTextAreaComment.html');
    $resubmitModal = multiModal.setTemplateFile($resubmitModal, 'templates/common/resubmitDeviationModal.html');
    $deviationModal = multiModal.setTemplateFile($deviationModal, 'templates/common/resubmitDeviationModal.html');

    //CONSTANTS
    //constant classes for rating quality
    var RATING_CLASSES_CONST = {
        good: "good",
        semiGood: "semiGood",
        bad: "bad",
        notAvailable: "notAvailable"
    };

    /* =================================================================
     SCOPE VARIABLES
     =================================================================
     */

    $scope.obj = $unitRefer.getObj(); //Retrieve from factory all data for this screen (unitReferModule)
    $scope.obj.visitStatus = $depot.getobject().inspectionStatus; //Get Visit Status
    $scope.dataArray = $galleryModule.getData();

    $scope.regex = /^(\d){0,3}$/;
    $scope.addLineConf = false;
    $scope.showDrop = false;
    $scope.viewModal = "started";
    $scope.messages = [];
    $scope.formObject = {};
    $scope.componentsVisibility = [];
    $scope.taggedImages = [];
    $scope.ratingArray = ["5", "4", "3", "2", "1", "0", "N/A"];
    $scope.data = {
        finalComment: $scope.obj.finalComment,
        noComm: "There is no comment inserted."
    };


    //set validation patterns on scope
    $scope.max3Digit = validationRegex.max3Digit;
    $scope.mediumAlphaNumeric = validationRegex.mediumAlphaNumeric;
    $scope.mediumNumeric = validationRegex.mediumNumeric;
    $scope.largeMixedCharacters = validationRegex.largeMixedCharacters;
    $scope.onlyAlpha2Chars = validationRegex.onlyAlpha2Chars;
    $scope.onlyNumeric4Digits = validationRegex.onlyNumeric4Digits;
    $scope.onlyAlpha4Chars = validationRegex.onlyAlpha4Chars;
    $scope.sparePartRegex = validationRegex.sparePartRegex;
    $scope.alphaNumericMax8 = validationRegex.alphaNumericMax8;
    $scope.alphaNumericMax2 = validationRegex.alphaNumericMax2;
    $scope.alphaNumericMax20 = validationRegex.alphaNumericMax20;
    $scope.maxNumberOfCharacters = 255;


    /* =================================================================
     START OF FUNCTIONS DECLARATIONS (alphabetically ordered)
     =================================================================
     */
    $scope.showDropdown = showDropdown;
    $scope.acceptLine = acceptLine;
    $scope.acceptPostLine = acceptPostLine;
    $scope.actionStartStop = actionStartStop;
    $scope.addNewLine = addNewLine;
    $scope.addNewRepair = addNewRepair;
    $scope.addPart = addPart;
    $scope.changeComponentVisibility = changeComponentVisibility;
    $scope.closeDropDowns = closeAllRqDropdowns;
    $scope.confirmAddLine = confirmAddLine;
    $scope.deleteLine = deleteLine;
    $scope.deletePostLine = deletePostLine;
    $scope.deletePicture = deletePicture;
    $scope.editComponent = editComponent;
    $scope.estimateCorrectRejectWO = estimateCorrectRejectWO;
    $scope.fastClickTextareaModal = fastClickTextareaModal;
    $scope.fastClickTextareaNoEditModal = fastClickTextareaNoEditModal;
    $scope.getClassRQ = getClassRQ;
    $scope.handleDeviationAction = handleDeviationAction;
    $scope.handleResubmitAction = handleResubmitAction;
    $scope.hideModal = hideModal;
    $scope.keyboardEnter = keyboardEnter;
    $scope.onSwipeMove = onSwipeMove;
    $scope.onSwipeStart = onSwipeStart;
    $scope.openDropdownRq = openDropdownRq;
    $scope.openGalleryModal = openGalleryModal;
    $scope.openInfoStartedModal = openInfoStartedModal;
    $scope.partCodeChanged = partCodeChanged;
    $scope.partFieldChanged = partFieldChanged;
    $scope.removePart = removePart;
    $scope.removeRepair = removeRepair;
    $scope.repairCodeChanged = repairCodeChanged;
    $scope.repairFieldChanged = repairFieldChanged;
    $scope.saveToLocalStorage = saveToLocalStorage;
    $scope.scrollReset = scrollReset;
    $scope.setLinesStatus = setLinesStatus;
    $scope.setPartToNotRequired = setPartToNotRequired;
    $scope.setRatingRQ = setRatingRQ;
    $scope.showComment = showComment;
    $scope.unacceptLine = unacceptLine;
    $scope.updateChildRepairCodes = updateChildRepairCodes;
    $scope.valueRatingRQ = valueRatingRQ;


    /* =================================================================
     INIT CONTROLLER CALLED FUNCTIONS
     =================================================================
     */

    $unitRefer.getFinalComment();
    addMessages($scope.messages, ['reeferUnit', 'pcs', 'damageCode', 'repairCode', 'repairLoc', 'description', ' sparePartNr', 'manHrs', 'tpiIndicator', 'coreTag', 'materialCost', 'partNumber', 'partCost', 'totalPerCode', 'max3DigitPattern']);
    getGalleryPhotos();
    initComponentsVisibility(); //init components visibility array with NULL variables based on components length
    $scope.hasECRWStatus = isECRW();

    /* =================================================================
     FUNCTIONS IMPLEMENTATION
     =================================================================
     */

    //Show or hide dropdown when called 
    function showDropdown() {
        $scope.showDrop = !$scope.showDrop;
    }

    //Hide keyboard 
    function keyboardEnter($event) {
        $event.target.blur();
    };

    //Resize scroll
    function scrollReset() {
        $ionicScrollDelegate.resize();
    };

    /**
     * @name getGalleryPhotos
     * @desc Get gallery photos for a WO and Visit
     * @returns {Void} 
     * @memberOf Controllers.UnitController
     */
    function getGalleryPhotos() {
        $localDB.find("galleryPhoto", {
            wono: PhotoStorageDirectory.getWoNo(),
            visitId: PhotoStorageDirectory.getVisitId()
        }, {}).then(function (data) {

            for (index in data) {
                $scope.dataArray.array.push({
                    'taggedImages': data[index],
                    'base64': '',
                    'checked': false,
                })
            }

            if (data.length !== 0) {
                // get the base64 string for every photo and store it
                var counter = $scope.dataArray.array.length;
                for (index in $scope.dataArray.array) {
                    var componentsUrl = $scope.dataArray.array[index].taggedImages.json.path.split("/");
                    $galleryModule.getBase64Image($scope.dataArray.array, PhotoStorageDirectory.getFullPath() + "" + componentsUrl[componentsUrl.length - 1]).then(function (data) {
                        $scope.dataArray.array = data;

                        if (--counter == 0) {
                            $scope.imageCount = $scope.dataArray.array.length;
                            $timeout(function () {
                                $scope.messages.noPhoto = Messages.noPhotoAdded;
                            })
                        }
                    }, function (error) {
                        console.log("error : " + error);
                    });
                }
            }
            // if gallery is empty
            else {
                $timeout(function () {
                    $scope.messages.noPhoto = Messages.noPhotoAdded;
                });
            }
        });
    };

    /**
     * @name initComponentsVisibility
     * @desc Initialize empty array for components visibility with NULL values based on Components object
     * @returns {Void} 
     * @memberOf Controllers.UnitController
     */
    function initComponentsVisibility() {
        angular.forEach($scope.obj.components, function () {
            $scope.componentsVisibility.push(null);
        });
    }


    /**
     * @name openDropdownRq
     * @desc Show/hide rating dropdown
     * @param {Object} part repair part 
     * @returns {Void} 
     * @memberOf Controllers.UnitController
     */
    function openDropdownRq(part) {
        if (part.$notRequired || part.quantity == '0') {
            return;
        }
        if ($scope.obj.action != 'Finish inspection') {
            return;
        }

        if (!part.$rqDropVisible) {
            closeAllRqDropdowns();
            part.$rqDropVisible = true;
        } else {
            part.$rqDropVisible = false;
        }
    };


    /**
     * @name closeAllRqDropdowns
     * @desc Close all dropdowns before open one
     * @returns {Void} 
     * @memberOf Controllers.UnitController
     */
    function closeAllRqDropdowns() {
        $scope.obj.parts.forEach(function (part) {
            part.$rqDropVisible = false;
        });
    };

    /**
     * @name valueRatingRQ
     * @desc Compute the RQ value based on part
     * @param {Object} part repair part 
     * @returns {String} Selected rating 
     * @memberOf Controllers.UnitController
     */
    function valueRatingRQ(part) {
        if ((!part.$ratingRQ && part.$ratingRQ != 0) || part.$rqDropVisible) {
            return "RQ";
        } else {
            return part.$ratingRQ;
        }
    };

    /**
     * @name setRatingRQ
     * @desc Set the RQ value based on user selection
     * @param {String} value selected value by user
     * @param {Object} part repair part 
     * @returns {Void} 
     * @memberOf Controllers.UnitController
     */
    function setRatingRQ(value, part) {
        part.$ratingRQ = value;
        part.$rqDropVisible = false;
    };


    /**
     * @name setRatingRQ
     * @desc Get CSS Class based on selected rating from RQ dropwdown
     * @param {Object} part repair part 
     * @returns {String} css class naming 
     * @memberOf Controllers.UnitController
     */
    function getClassRQ(part) {
        var rating = part.$ratingRQ;
        if (part.$rqDropVisible) {
            return "active";
        }

        if ((!rating && rating != 0) || rating == undefined) {
            return;
        }

        if (rating == 'N/A') {
            return RATING_CLASSES_CONST.notAvailable;
        } else if (rating == 0) {
            return RATING_CLASSES_CONST.bad;
        } else if (rating == 1 || rating == 2) {
            return RATING_CLASSES_CONST.semiGood;
        } else if (rating > 2) {
            return RATING_CLASSES_CONST.good;
        }
    };

    /**
     * @name calculateRating
     * @desc Compute rating based on the repair Quality 
     * @returns {Void} 
     * @memberOf Controllers.UnitController
     */
    function calculateRating() {
        var ratingObj = {
            sum: 0,
            nrElem: 0
        }

        $scope.obj.parts.forEach(function (part) {
            if (part.$ratingRQ && part.$ratingRQ != 'N/A') {
                ratingObj.sum = ratingObj.sum + parseInt(part.$ratingRQ);
                ratingObj.nrElem++;
            }
        });

        if (ratingObj.nrElem != 0) {
            $scope.obj.ratingRQ = (ratingObj.sum / ratingObj.nrElem).toFixed(2);
        } else {
            $scope.obj.ratingRQ = 'N/A';
        }
    };

    /**
     * @name hasRating
     * @desc Returns the availability of rating or an additional part in a repair
     * @param {Object} element part in a WO
     * @returns {Boolean}
     * @memberOf Controllers.UnitController
     */
    function hasRating(element) {
        return element.$ratingRQ || element.$addedPart ? true : false;
    };

    //Returns the availability of Repair Quality in a part
    function hasOneRating(element) {
        return element.$ratingRQ ? true : false;
    };

    //Switch a component visiblity from True to False and vice-versa
    function changeComponentVisibility(index) {
        $scope.componentsVisibility[index] = !$scope.componentsVisibility[index];
    };

    //Save in JSONstore parts of a WO
    function saveToLocalStorage() {
        // If coming from repair code or part code, try to get new description from json store, then update parts, then save to local storage.
        $unitRefer.saveToLocalStorage($scope.obj.parts);
    };

    /**
     * @name repairCodeChanged
     * @desc Update repair description after Repair Code changed
     * @param {Object} repair a repair in Work Order
     * @returns {Void}
     * @memberOf Controllers.UnitController
     */
    function repairCodeChanged(repair) {
        WOCodesService.getRepairDescription(repair.repairCode, repair.mode).then(function (description) {
            repair.$postRepairDescription = description;
            $timeout(function () {});
            $unitRefer.saveToLocalStorage($scope.obj.parts);
        });
    };

    /**
     * @name partCodeChanged
     * @desc Update part description after Part Code changed
     * @param {Object} part a part in Work Order
     * @returns {Void}
     * @memberOf Controllers.UnitController
     */
    function partCodeChanged(part) {
        WOCodesService.getPartDescription(part.partCode).then(function (description) {
            part.$postPartDescription = description;
            $timeout(function () {});
            $unitRefer.saveToLocalStorage($scope.obj.parts);
        });
    }

    /**
     * @name editComponent
     * @desc Edit an updated component 
     * @param {Integer} index in array of components
     * @returns {Void}
     * @memberOf Controllers.UnitController
     */
    function editComponent(index) {
        $ionicLoading.show(loaderConfig);

        var changedComponent = $scope.obj.components[index]; // from ng-repeat
        var currentComponent = $scope.obj.jsonStoreComponents[index]; // jsonstore format

        $unitRefer.editComponent(currentComponent, changedComponent).then(function () {
            $ionicLoading.hide();
        });
    };

    /**
     * @name acceptLine
     * @desc Accept a repair in WO
     * @param {Object} part in WO
     * @returns {Void}
     * @memberOf Controllers.UnitController
     */
    function acceptLine(part) {
        if (part.$aggregateRejected && !part.$notRequired) { //this means orange button on
            showResubmitModal(part);
        } else {

            if (part.$accepted) {
                part.$accepted = undefined;
                $timeout(function () {});
                $scope.saveToLocalStorage();
                return;
            }

            if (partIsUnchanged(part)) {
                revertRepairAndChildrenPartsToOriginalValues(part, true);
                $timeout(function () {});
                $scope.saveToLocalStorage();
                // We can accept it as is!
            } else {
                // Need to notify user and let him confirm that he will revert to original values
                revertRepairAndChildrenPartsToOriginalValues(part, true);
                $timeout(function () {});
                $scope.saveToLocalStorage();
            }
        }
    };

    /**
     * @name acceptPostLine
     * @desc Accept a repair in WO in a Post Repair visit
     * @param {Object} part in WO
     * @returns {Void}
     * @memberOf Controllers.UnitController
     */
    function acceptPostLine(part) {
        if (part.$aggregateRejected && !part.$notRequired) { //this means orange button on
            showDeviationModal(part);
        } else {
            if (part.$accepted) {
                part.$accepted = undefined;
                $timeout(function () {});
                $scope.saveToLocalStorage();
                return;
            }
            if (partIsUnchanged(part)) {
                revertPostRepairAndChildrenPartsToOriginalValues(part, true);
                $timeout(function () {});
                $scope.saveToLocalStorage();
                // We can accept it as is!
            } else {
                // Need to notify user and let him confirm that he will revert to original values
                revertPostRepairAndChildrenPartsToOriginalValues(part, true);
                $timeout(function () {});
                $scope.saveToLocalStorage();
            }

        }
    }

    /**
     * @name handleResubmitAction
     * @desc Handle user tap action on Resubmit action button
     * @param {Object} part in WO
     * @returns {Void}
     * @memberOf Controllers.UnitController
     */
    function handleResubmitAction(part) {
        if ($scope.obj.action != 'Finish inspection')
            return;
        if (part.$accepted || part.$notRequired) {
            ToastService.showNotificationMsg('Please unlock the line by deselecting the current status. "Resubmit" is automatically selected when repairs or part(s) are modified.');
        } else if (part.$aggregateRejected && !part.$notRequired) {
            showResubmitModal(part);
        } else {
            ToastService.showNotificationMsg('"Resubmit" is automatically selected when repairs or part(s) are modified.');
        }
    }

    /**
     * @name deleteLine
     * @desc Handle user tap action on Not Required action button
     * @param {Object} part in WO
     * @returns {Void}
     * @memberOf Controllers.UnitController
     */
    function deleteLine(part) {
        // if repair is unchanged - just do it else ask user to confirm
        if (part.$aggregateRejected && !part.$notRequired) {
            showResubmitModal(part);
        } else {
            if (part.$accepted) {
                part.$accepted = undefined;
                part.quantity = "0";
                part.$notRequired = true;
                $scope.setLinesStatus(part);
                $timeout(function () {});
                $scope.saveToLocalStorage();
                return;
            }

            if (part.$notRequired) {
                revertRepairFromNotRequired(part);
                $timeout(function () {});
                $scope.saveToLocalStorage();
                return;
            }

            setRepairToNotRequired(part);
            $timeout(function () {});
            $scope.saveToLocalStorage();
        }
    }

    /**
     * @name deleteLine
     * @desc Handle user tap action on Not Required action button for a post repair 
     * @param {Object} part in WO
     * @returns {Void}
     * @memberOf Controllers.UnitController
     */
    function deletePostLine(part) {
        if (part.$aggregateRejected && !part.$notRequired) {
            showDeviationModal(part);
        } else {
            if (part.$accepted) {
                part.$accepted = undefined;
                part.quantity = "0";
                $scope.setRatingRQ("0", part)
                part.$notRequired = true;
                $scope.setLinesStatus(part);
                $timeout(function () {});
                $scope.saveToLocalStorage();
                return;
            }

            if (part.$notRequired) {
                revertRepairFromNotRequired(part);
                $timeout(function () {});
                $scope.saveToLocalStorage();
                return;
            }

            setRepairToNotRequired(part);
            $timeout(function () {});
            $scope.saveToLocalStorage();
        }
    };

    /**
     * @name handleDeviationAction
     * @desc Handle deviation action on a repair in Work Order
     * @param {Object} part in WO
     * @returns {Void}
     * @memberOf Controllers.UnitController
     */
    function handleDeviationAction(part) {
        if ($scope.obj.action != 'Finish inspection')
            return;
        if (part.$accepted || part.$notRequired) {
            ToastService.showNotificationMsg('Please unlock the line by deselecting the current status. "Deviation" is automatically selected when repairs or part(s) are modified.');
        } else if (part.$aggregateRejected && !part.$notRequired) {
            showDeviationModal(part);
        } else {
            ToastService.showNotificationMsg('"Deviation" is automatically selected when repairs or part(s) are modified.');
        }
    };

    /**
     * @name showDeviationModal
     * @desc Show deviation modal
     * @param {Object} part in WO
     * @returns {Void}
     * @memberOf Controllers.UnitController
     */
    function showDeviationModal(part) {
        $deviationModal = multiModal.setActions($deviationModal, function () {
            revertPostRepairAndChildrenPartsToOriginalValues(part, false);
            $timeout(function () {});
            $scope.saveToLocalStorage();
        });

        multiModal.openModal($deviationModal, $scope);
    };

    /**
     * @name showResubmitModal
     * @desc Show modal to revert changes to initial values 
     * @param {Object} part in WO
     * @returns {Void}
     * @memberOf Controllers.UnitController
     */
    function showResubmitModal(part) {
        $resubmitModal = multiModal.setActions($resubmitModal, function () {
            revertRepairAndChildrenPartsToOriginalValues(part, false);
            $timeout(function () {});
            $scope.saveToLocalStorage();
        });

        multiModal.openModal($resubmitModal, $scope);
    }

    /**
     * @name setPartToNotRequired
     * @desc Reset part to original values
     * @param {Object} part in WO
     * @returns {Void}
     * @memberOf Controllers.UnitController
     */
    function setPartToNotRequired(part, repair) {
        // if part not $rejected just change to not required and 0
        if (repair.$accepted || repair.$notRequired) {
            return;
        }

        if (part.$notRequired) {
            // revert to original
            revertPartFromNotRequired(part, repair);
            $timeout(function () {});
            $scope.saveToLocalStorage();
            return;
        }

        if (!part.$rejected) {
            part.quantity = "0";
            part.$notRequired = true;
            $scope.setLinesStatus(repair, part);
            $timeout(function () {});
            $scope.saveToLocalStorage();
            return;
        }

        if (part.$rejected) {
            // alert user
            setPartToNotRequiredImpl(part, repair);
            $timeout(function () {});
            $scope.saveToLocalStorage();
            return;
        }
    };

    //Implementation for setting a part to not required 
    function setPartToNotRequiredImpl(part, repair) {
        var repairIndex = $scope.obj.parts.indexOf(repair);
        var partIndex = $scope.obj.parts[repairIndex].$childParts.indexOf(part);

        part = angular.copy($scope.obj.preAuditParts[repairIndex].$childParts[partIndex]);
        part.quantity = "0";
        part.$notRequired = true;
        $scope.obj.parts[repairIndex].$childParts[partIndex] = part;

        $scope.setLinesStatus(repair, $scope.obj.parts[repairIndex].$childParts[partIndex]);
    }

    //Implementation for reverting post repair parts to initial values
    function revertPostRepairAndChildrenPartsToOriginalValues(repair, acceptedStatus) {

        var rating = {
            $rating: repair.$ratingRQ,
            $notRequired: repair.$notRequired
        }
        var repairIndex = $scope.obj.parts.indexOf(repair);
        repair = angular.copy($scope.obj.preAuditParts[repairIndex]);
        repair.$accepted = acceptedStatus;
        repair.$notRequired = false;
        if (rating.$rating && !rating.$notRequired) {
            repair.$ratingRQ = rating.$rating;
        }
        $scope.obj.parts[repairIndex] = repair;
    };

    //Revert a part to original values
    function revertPartFromNotRequired(part, repair) {
        var repairIndex = $scope.obj.parts.indexOf(repair);
        var partIndex = $scope.obj.parts[repairIndex].$childParts.indexOf(part);

        part = angular.copy($scope.obj.preAuditParts[repairIndex].$childParts[partIndex]);

        part.$notRequired = false;
        $scope.obj.parts[repairIndex].$childParts[partIndex] = part;

        $scope.setLinesStatus(repair, $scope.obj.parts[repairIndex].$childParts[partIndex]);
    }

    /**
     * @name partIsUnchanged
     * @desc Checks if editable values are same as original values for a repair 
     * @param {Object} repair in WO
     * @returns {Boolean} isUnchanged 
     * @memberOf Controllers.UnitController
     */
    function partIsUnchanged(repair) {
    
        var repairIndex = $scope.obj.parts.indexOf(repair);
        var preRepair = $scope.obj.preAuditParts[repairIndex];

        var isUnchanged = angular.equals(repair, preRepair);
        return isUnchanged;
    }

     /**
     * @name revertRepairAndChildrenPartsToOriginalValues
     * @desc Implementation for reverting repair parts to initial values
     * @param {Object} repair in WO
     * @param {Boolean} acceptedStatus repair is accepted
     * @returns {Void}  
     * @memberOf Controllers.UnitController
     */
    function revertRepairAndChildrenPartsToOriginalValues(repair, acceptedStatus) {
        var repairIndex = $scope.obj.parts.indexOf(repair);
        repair = angular.copy($scope.obj.preAuditParts[repairIndex]);
        repair.$accepted = acceptedStatus;
        repair.$notRequired = false;
        $scope.obj.parts[repairIndex] = repair;
    }

    /**
     * @name revertRepairFromNotRequired
     * @desc Revert a repair to original values
     * @param {Object} repair in WO
     * @returns {Void}  
     * @memberOf Controllers.UnitController
     */
    function revertRepairFromNotRequired(repair) {
        var repairIndex = $scope.obj.parts.indexOf(repair);
        repair = angular.copy($scope.obj.preAuditParts[repairIndex]);
        repair.$notRequired = false;
        $scope.obj.parts[repairIndex] = repair;
        $scope.setLinesStatus($scope.obj.parts[repairIndex]);
    }

     /**
     * @name setRepairToNotRequired
     * @desc Setting a repair to not required status
     * @param {Object} repair in WO
     * @returns {Void}  
     * @memberOf Controllers.UnitController
     */
    function setRepairToNotRequired(repair) {
        var repairIndex = $scope.obj.parts.indexOf(repair);
        repair = angular.copy($scope.obj.preAuditParts[repairIndex]);
        repair.quantity = "0";
        repair.$ratingRQ = "0";
        repair.$notRequired = true;
        $scope.obj.parts[repairIndex] = repair;
        $scope.setLinesStatus($scope.obj.parts[repairIndex]);
    }

    /**
     * @name unacceptLine
     * @desc Change the status of a line to undefined to revert initial state
     * @param {Object} repair in WO
     * @returns {Void}  
     * @memberOf Controllers.UnitController
     */
    function unacceptLine(part) {
        part.$accepted = undefined;
        part.$aggregateRejected = undefined;
    }

    /**
     * @name setLinesStatus
     * @desc Set the status of lines in Work Order
     * @param {Object} part in WO
     * @param {Object} subPart of the part
     * @returns {Void}  
     * @memberOf Controllers.UnitController
     */
    function setLinesStatus(part, subPart) {

        // Check if it's a repair or a part
        var isRepair = (subPart) ? false : true;
        var changedLine = (isRepair) ? part : subPart;
        var equalPreAudit;

        // Check if line is accepted or rejected
        if (!changedLine.$addedPart) { // only if the part is not added
            equalPreAudit = equalsPreAuditLine(part, subPart);
        } else {
            return;
        }

        changedLine.$rejected = !equalPreAudit;
        changedLine.$status = (equalPreAudit) ? 'Accepted' : 'Rejected'

        // set aggregate status
        if (changedLine.$rejected) {
            part.$aggregateRejected = true; // if any line is rejected the aggregate status is rejected
        } else {
            part.$aggregateRejected = getAggregateRejected(part);
        }

    }

     /**
     * @name equalsPreAuditLine
     * @desc Check if there are exact lines 
     * @param {Object} part in WO
     * @param {Object} subPart of the part
     * @returns {Boolean} isEqual 
     * @memberOf Controllers.UnitController
     */
    function equalsPreAuditLine(part, subPart) {
        var isEqual;
        var repairIndex = $scope.obj.parts.indexOf(part);

        if (typeof $scope.obj.preAuditParts[repairIndex].quantity == 'number') {
            $scope.obj.preAuditParts[repairIndex].quantity = $scope.obj.preAuditParts[repairIndex].quantity.toString();
        }

        if (typeof $scope.obj.parts[repairIndex].quantity == 'number') {
            $scope.obj.parts[repairIndex].quantity = $scope.obj.parts[repairIndex].quantity.toString();
        }

        if (subPart) {
            // Get part index to
            var partIndex = $scope.obj.parts[repairIndex].$childParts.indexOf(subPart);

            if (typeof $scope.obj.preAuditParts[repairIndex].$childParts[partIndex].quantity == 'number') {
                $scope.obj.preAuditParts[repairIndex].$childParts[partIndex].quantity = $scope.obj.preAuditParts[repairIndex].$childParts[partIndex].quantity.toString();
            }

            if (typeof $scope.obj.parts[repairIndex].$childParts[partIndex].quantity == 'number') {
                $scope.obj.parts[repairIndex].$childParts[partIndex].quantity = $scope.obj.parts[repairIndex].$childParts[partIndex].quantity.toString();
            }

            isEqual = angular.equals($scope.obj.parts[repairIndex].$childParts[partIndex], $scope.obj.preAuditParts[repairIndex].$childParts[partIndex]);
        } else {
            isEqual = angular.equals($scope.obj.parts[repairIndex], $scope.obj.preAuditParts[repairIndex]);
        }

        return isEqual;
    }

     /**
     * @name getAggregateRejected
     * @desc Return the status of a part calculated using child parts
     * @param {Object} part in WO
     * @returns {Boolean} - status of the part
     * @memberOf Controllers.UnitController
     */
    function getAggregateRejected(part) {

        for (var i = 0; i < part.$childParts.length; i++) {
            var childPart = part.$childParts[i];

            if (childPart.$rejected) return true;
        }

        return part.$rejected;
    }

    
    /**
     * @name getAggregateRejected
     * @desc Check all lines to be completed with either Accept or Reject
     * @param {Object} part in WO
     * @returns {Boolean} True - If all lines are set, False - otherwise
     * @memberOf Controllers.UnitController
     */
    function checkIfLinesSet() {
        for (var i = 0; i < $scope.obj.parts.length; i++) {

            var part = $scope.obj.parts[i];

            if (!part.$rejected && !part.$accepted && !part.$aggregateRejected) return false;
        }
        return true;
    };

    //@Deprecated - check to remove all dependencies
    function confirmAddLine(repairSelected, partSelected, addLineForm) {

        if (addLineForm.$invalid && addLineForm.$pristine) {
            $scope.addLineConf = true;
        }

        if (repairSelected) {
            // Add repair line - no validation required
            var repairLine = {
                TPI: '',
                repairDescription: '',
                damageCode: '',
                partDescription: '',
                partCode: '',
                quantity: 0,
                repairCode: '',
                repairLocCode: '',
                $status: 'Rejected',
                $aggregateRejected: true,
                $rejected: true,
                $addedPart: true,
                $childParts: [],
            }

            $scope.$evalAsync(function () {
                $scope.obj.parts.push(repairLine);
                $unitRefer.saveToLocalStorage($scope.obj.parts);
            });

            multiModal.closeCurrentModal();

            return;
        } else if (partSelected) {
            // Need to validate as well
            if (addLineForm.$invalid) {
                addLineForm.$dirty = true;
                return;
            }

            var inputRepairCode = addLineForm.repairCode.$modelValue;
            var inputRepairLocCode = addLineForm.repairLocCode.$modelValue;

            // Need to check that the repair has a corresponding repair line
            var parentIndex = getParentIndex(inputRepairCode, inputRepairLocCode);
            var indexRepairCode = getParentIndexRepairCode(inputRepairCode);
            var indexRepairLocCode = getParentIndexRepairLocCode(inputRepairLocCode);


            if (indexRepairCode === null) {
                // can't add it yet,
                addLineForm.repairCode.$invalid = true;
                addLineForm.repairCode.$dirty = true;
            }

            if (indexRepairLocCode === null) {
                // can't add it yet,
                addLineForm.repairLocCode.$invalid = true;
                addLineForm.repairLocCode.$dirty = true;
            }


            if (parentIndex === null) {
                // can't add it yet,
                addLineForm.$invalid = true;
                addLineForm.$dirty = true;
                return;
            }

            // Add part line
            var partLine = {
                quantity: 0,
                repairCode: inputRepairCode,
                partCode: "",
                $addedPart: true,
                $status: 'Rejected',
                $rejected: true,
                $accepted: false,
            }

            // add identifier as well

            $scope.obj.parts[parentIndex].$childParts.push(partLine);
            $scope.obj.parts[parentIndex].$aggregateRejected = true;

            $unitRefer.saveToLocalStorage($scope.obj.parts);

            multiModal.closeCurrentModal();
        } else {
            addLineForm.$invalid = true;
            addLineForm.$dirty = true;
        }

        function getParentIndexRepairCode(repairCode) {
            var index = null;

            for (var i = 0; i < $scope.obj.parts.length; i++) {
                var repairLine = $scope.obj.parts[i];

                if (repairLine.repairCode == repairCode) {
                    if (repairLine.$accepted) continue;
                    index = i;
                    break;
                }
            }

            return index;
        }

        function getParentIndexRepairLocCode(repairLocCode) {
            var index = null;

            for (var i = 0; i < $scope.obj.parts.length; i++) {
                var repairLine = $scope.obj.parts[i];

                if (repairLine.repairLocCode == repairLocCode) {
                    if (repairLine.$accepted) continue;
                    index = i;
                    break;
                }
            }

            return index;
        }

        function getParentIndex(repairCode, repairLocCode) {
            var index = null;

            for (var i = 0; i < $scope.obj.parts.length; i++) {
                var repairLine = $scope.obj.parts[i];

                if (repairLine.repairCode == repairCode && repairLine.repairLocCode == repairLocCode) {
                    if (repairLine.$accepted) continue;
                    index = i;
                    break;
                }
            }

            return index;
        }
    };

    /**
     * @name updateChildRepairCodes
     * @desc Update the child repair codes to correspond to performed changes
     * @param {Object} repair in WO
     * @returns {Void} 
     * @memberOf Controllers.UnitController
     */
    function updateChildRepairCodes(repair) {
        var repairIndex = $scope.obj.parts.indexOf(repair);

        if (repair.$childParts && repair.$childParts.length > 0) {
            angular.forEach($scope.obj.parts[repairIndex].$childParts, function (part, idxPart) {
                var partLine = $scope.obj.parts[repairIndex].$childParts[idxPart];

                partLine.repairCode = repair.repairCode;
                // set status
                if (!partLine.$addedPart) {
                    var isEqual = angular.equals($scope.obj.parts[repairIndex].$childParts[idxPart], $scope.obj.preAuditParts[repairIndex].$childParts[idxPart]);

                    partLine.$rejected = !isEqual;
                    partLine.$status = (isEqual) ? 'Accepted' : 'Rejected';
                }

            });
        }
    }

    /**
     * @name updateChildRepairCodes
     * @desc Check whether data is consistent (no duplicate keys etc),
     * There can't be duplicate repairs (same repair code and repair loc code)
     * There can't be duplicate child parts (same partCode)
     * @returns {Object} contains validation information about repairs and parts 
     * @memberOf Controllers.UnitController
     */
    function linesAreValid() {

        var validationInfo = {
            repairs: true,
            parts: true
        };

        var repairFields = ['repairCode', 'repairLocCode'];

        if (elementsAreUniqueForFields($scope.obj.parts, repairFields)) {
            // We need to check for child parts too
            var partFields = ['partCode'];

            for (var i = 0; i < $scope.obj.parts.length; i++) {
                var repairLine = $scope.obj.parts[i];

                if (repairLine.$childParts && repairLine.$childParts.length > 0) {
                    if (!elementsAreUniqueForFields(repairLine.$childParts, partFields)) {
                        // Duplicate child parts
                        validationInfo.parts = false;
                        break;
                    }
                }
            }
        } else {
            // Duplicate repairs
            validationInfo.repairs = false;
        }

        return validationInfo;
    }

    /**
     * @name elementsAreUniqueForFields
     * @desc Check in all elements inside an array to be unique against specified fields as parameter
     * @param {Array} array to search into for unieuq elements
     * @param {Array} fieldsArray of strings with properties to check against
     * @returns {Boolean} - true if elements are unique, false otherwise
     * @memberOf Controllers.UnitController
     */
    function elementsAreUniqueForFields(array, fieldsArray) {
        var filteredArray = [];

        for (var i = 0; i < array.length; i++) {
            for (var j = i + 1; j < array.length; j++) {
                for (var k = 0; k < fieldsArray.length; k++) {
                    if (array[i][fieldsArray[k]].toLowerCase() != array[j][fieldsArray[k]].toLowerCase()) {
                        break;
                    } else {
                        if (k == fieldsArray.length - 1) {
                            return false;
                        }
                    }
                }
            }
        }

        return true;
    }

    /**
     * @name addNewRepair
     * @desc Add a new repair line
     * @returns {Void}
     * @memberOf Controllers.UnitController
     */
    function addNewRepair() {
        $timeout(function () {
            $ionicScrollDelegate.scrollBottom();
        }, 100);


        if ($scope.formObject.componentsForm.$invalid) {
            ToastService.showNotificationMsg('Please fill in the details before proceeding');
            return;
        }

        var dataValidationInfo = linesAreValid();

        if (!dataValidationInfo.repairs) {
            multiModal.openModal($uniqueRepairsError, $scope);
            return;
        }

        if (!dataValidationInfo.parts) {
            multiModal.openModal($uniquePartsError, $scope);
            return;
        }

        var repairLine = {
            TPI: '',
            repairDescription: '',
            damageCode: '',
            partDescription: '',
            partCode: '',
            quantity: "1",
            repairCode: '',
            repairLocCode: '',
            $status: 'Rejected',
            $aggregateRejected: true,
            $rejected: true,
            $addedPart: true,
            $childParts: [],
        }

        $scope.$evalAsync(function () {
            $scope.obj.parts.push(repairLine);
            $unitRefer.saveToLocalStorage($scope.obj.parts);
        });
    };

    //@Deprecated - possible deprecated, to be checked
    function addNewLine() {
        $scope.addLineConf = false;
        if ($scope.formObject.componentsForm.$invalid) {
            multiModal.openModal($validationErrors, $scope);
            return;
        }

        var dataValidationInfo = linesAreValid();

        if (!dataValidationInfo.repairs) {
            multiModal.openModal($uniqueRepairsError, $scope);
            return;
        }

        if (!dataValidationInfo.parts) {
            multiModal.openModal($uniquePartsError, $scope);
            return;
        }

        var $addLineModal = multiModal.setBasicOptions('confirm', 'addLineModal', null, 'full');
        $addLineModal = multiModal.setTemplateFile($addLineModal, 'templates/components/addLineModal.html');

        multiModal.openModal($addLineModal, $scope, true);
    };

    /**
     * @name removeRepair
     * @desc Remove a repair line from WO with all its children
     * @param {Object} line - line to be removed
     * @returns {Void}
     * @memberOf Controllers.UnitController
     */
    function removeRepair(line) {
        // note that it will remove all children too
        var lineIndex = $scope.obj.parts.indexOf(line);

        removeLine(lineIndex);

        function removeLine(index) {
            $scope.obj.parts.splice(index, 1);
            $unitRefer.saveToLocalStorage($scope.obj.parts);
        }
    }

    /**
     * @name addPart
     * @desc Adds a new part to a specified repair 
     * @param {Object} repair - attach a part to repair parameter
     * @returns {Void}
     * @memberOf Controllers.UnitController
     */
    function addPart(repair) {
        if ($scope.formObject.componentsForm.$invalid) {
            ToastService.showNotificationMsg('Please fill in the details before proceeding');
            return;
        }

        var dataValidationInfo = linesAreValid();

        if (!dataValidationInfo.repairs) {
            multiModal.openModal($uniqueRepairsError, $scope);
            return;
        }

        if (!dataValidationInfo.parts) {
            multiModal.openModal($uniquePartsError, $scope);
            return;
        }

        if (repair.$accepted || repair.$notRequired) {
            ToastService.showNotificationMsg("Please unlock the line by deselecting the current status in order to add a new repair or part.");
            return;
        }
        // need validation first!

        var parentIndex = $scope.obj.parts.indexOf(repair);

        // Add part line

        var partLine = {
            quantity: "1",
            repairCode: repair.repairCode,
            partCode: "",
            $addedPart: true,
            $status: 'Rejected',
            $rejected: true,
            $accepted: false,
        }

        // add identifier as well

        $scope.obj.parts[parentIndex].$childParts.push(partLine);
        $scope.obj.parts[parentIndex].$aggregateRejected = true;

        $unitRefer.saveToLocalStorage($scope.obj.parts);
    }

    /**
     * @name removePart
     * @desc Remove a part from a specified repair 
     * @param {Object} part - part to be removed from repair object
     * @param {Object} repair - object with part data to be removed
     * @returns {Void}
     * @memberOf Controllers.UnitController
     */
    function removePart(part, repair) {
        var parentIndex = $scope.obj.parts.indexOf(repair);
        var partIndex = $scope.obj.parts[parentIndex].$childParts.indexOf(part);

        $scope.obj.parts[parentIndex].$childParts.splice(partIndex, 1);

        if (!repair.$rejected) {
            repair.$aggregateRejected = getAggregateRejected(repair);
        }

        $unitRefer.saveToLocalStorage($scope.obj.parts);
    }

    /**
     * @name isECRW
     * @desc Check if current inspection has "Estimate Correct But Reject WO" STATUS and is finished
     * @returns {Boolean}
     * @memberOf Controllers.UnitController
     */
    function isECRW() {
        if (!$scope.obj.referUnitStatus.finished || $scope.obj.auditResult != "Rejected") {
            return false;
        } else {
            var linesAreAccepted = areLinesAccepted();
            if (!linesAreAccepted) {
                return false;
            }

            $scope.obj.action = "Estimate Correct But Reject WO";
            return true;
        }
    }

    /**
     * @name hasManuallyAddedParts
     * @desc Checks if there are manually added parts in inspection
     * @param {Array} parts Array of parts 
     * @returns {Boolean}
     * @memberOf Controllers.UnitController
     */
    function hasManuallyAddedParts(parts) {

        return parts.some(hasAddedPart);

        function hasAddedPart(part) {
            return !!part.$addedPart;
        };
    };


    /**
     * @name areLinesAccepted
     * @desc Check if all lines are accepted
     * @returns {Boolean}
     * @memberOf Controllers.UnitController
     */
    function areLinesAccepted() {
        for (var i = 0; i < $scope.obj.parts.length; i++) {
            var part = $scope.obj.parts[i];

            if (part.$rejected || part.$aggregateRejected) {
                return false;
            }
        }
        return true;
    };


    /**
     * @name estimateCorrectRejectWO
     * @desc Finish inspection with Estimate Correct but Reject WO status
     * @returns {void}
     * @memberOf Controllers.UnitController
     */
    function estimateCorrectRejectWO() {
        if (hasManuallyAddedParts($scope.obj.parts)) { //Check if there are manually added parts
            ToastService.showNotificationMsg('To select this action, all repairs on the original estimate must be accepted with no modifications.');
            return;
        } else if (!areLinesAccepted()) { //Check all lines are accepted
            ToastService.showNotificationMsg('To select this action, all repairs must be accepted.');
            return;
        } else {
            //Proceed to finish inspection Rejecting the WO
            $scope.isEstimatedButRejected = true;

            //Call function for finishing the inspection
            actionStartStop();
        };
    };

    
    /**
     * @name actionStartStop
     * @desc Function that manages the change of state of an inspection based on its current status
     * @returns {void}
     * @memberOf Controllers.UnitController
     */
    function actionStartStop() {
        var auditStatus = $scope.obj.visitStatus;

        switch (auditStatus) {
            case Messages.missed:
                // you have to reschedule a missed visit
                var $notificationRescheduleMissed = multiModal.setBasicOptions('notification', 'notificationRescheduleMissed', 'You are not able to start inspecting this container because the visit is missed. You need to reschedule first.');
                multiModal.openModal($notificationRescheduleMissed, $scope);
                break;
            case Messages.planned:
                // a planned visit will be started in order to start inspection on a work order
                var $startVisitFirst = multiModal.setBasicOptions('notification', 'startVisitFirst', 'You need to start the visit before you can start inspection on a work order.');
                $startVisitFirst = multiModal.setActions($startVisitFirst, function () {
                    window.history.back();
                });
                multiModal.openModal($startVisitFirst, $scope);
                break;
            case Messages.started:
                switch ($scope.obj.action) {
                    case Messages.startInspection:
                        var $StartInspection = multiModal.setBasicOptions('confirm', 'startInspection', 'You are about to start the inspection on container : ' + $scope.obj.containerNo + ' .\nAre you sure you want to continue?');
                        $StartInspection = multiModal.setActions($StartInspection, function () {
                            $ionicLoading.show(loaderConfig);

                            $unitRefer.setInProgressStatus();
                            $scope.saveToLocalStorage();

                            startedAudit.startWorkOrder($scope.obj.workOrderNo, $scope.obj.visitId).then(function () {
                                $timeout(function () {
                                    $ionicLoading.hide();
                                })
                            });
                        });
                        multiModal.openModal($StartInspection, $scope);
                        break;
                    case Messages.finishInspection:
                        if ($scope.formObject.componentsForm.$invalid) {
                            //								multiModal.openModal($validationErrors, $scope);
                            ToastService.showNotificationMsg('Please fill in the details before proceeding');
                            return;
                        }

                        var dataValidationInfo = linesAreValid();

                        if (!dataValidationInfo.repairs) {
                            multiModal.openModal($uniqueRepairsError, $scope);
                            return;
                        }

                        if (!dataValidationInfo.parts) {
                            multiModal.openModal($uniquePartsError, $scope);
                            return;
                        }

                        // Check that all lines are either accepted or rejected

                        if (!checkIfLinesSet()) {
                            multiModal.openModal($allLinesNotAudited, $scope);
                            return;
                        }

                        $scope.requiredComment = checkIfRejected();
                        var $confirmFinishInpection

                        //Modal view for finishing inspection with Correct Estimate but Reject WO
                        if ($scope.isEstimatedButRejected) {
                            $confirmFinishInpection = multiModal.setBasicOptions('confirm', '$confirmFinishInpection', 'Are you sure you would like to validate these repairs as correct, but reject the WO in MERC+?');
                            $scope.requiredComment = true; //Cannot skip comment because inspection is rejected and we have default message for ECRW
                        } else if ($scope.obj.parts.length > 0) {
                            $confirmFinishInpection = multiModal.setBasicOptions('confirm', '$confirmFinishInpection', 'Are you sure you want to finish this inspection?');
                        } else {
                            $confirmFinishInpection = multiModal.setBasicOptions('confirm', '$confirmFinishInpection', 'Finishing the inspection now will approve the work order. Do you want to continue?');
                        }

                        var rejectedComment = "";

                        if ($scope.requiredComment) {
                            var postRepairs = $scope.obj.parts;
                            var preRepairs = $scope.obj.preAuditParts;

                            rejectedComment = $unitRefer.generateComment(postRepairs, preRepairs);
                        }

                        //Add comment if inspection is Correct Estimated But Rejected WO
                        if ($scope.isEstimatedButRejected) {
                            rejectedComment = "WO estimate correct, but work not to be carried out. \n" + rejectedComment;
                        }

                        $scope.closeCommentModal = multiModal.closeCurrentModal;

                        $confirmFinishInpection = multiModal.setActions($confirmFinishInpection, function () {
                            $commentModal = multiModal.setActions($commentModal,
                                function () {
                                    finishAll();
                                },
                                function () {
                                    $scope.data.finalComment = "";
                                    finishAll();
                                });
                            $scope.data.finalComment = rejectedComment;
                            multiModal.openModal($commentModal, $scope, true);

                        });

                        //handle POSTREPAIR
                        if (auditService.isPostRepair($scope.obj.statusCode)) {
                            var allHave = $scope.obj.parts.every(hasRating);
                            var oneHas = $scope.obj.parts.some(hasOneRating);

                            if (allHave) {
                                calculateRating();
                                multiModal.openModal($confirmFinishInpection, $scope);
                            } else if (!oneHas) {
                                $scope.obj.ratingRQ = undefined;
                                openFinishPostRepairModal('You have not selected a Repair Quality for the repairs in the WO. Would you like to make changes?', rejectedComment)
                            } else {
                                $scope.obj.ratingRQ = 'N/A'
                                openFinishPostRepairModal('You have not selected a Repair Quality for all repairs in the WO. Would you like to make changes?', rejectedComment)
                            }

                        } else {
                            multiModal.openModal($confirmFinishInpection, $scope);
                        }

                        break;
                }
            default: // finished visit is read only
                return;
        };
    };

    /**
     * @name openFinishPostRepairModal
     * @desc Open a modal to finish POST REPAIR inspection and adding a comment
     * @param {String} message of the modal to be displayed
     * @param {String} comment added by user
     * @returns {void}
     * @memberOf Controllers.UnitController
     */
    function openFinishPostRepairModal(message, comment) {
        $finishPostInspection = multiModal.setBasicOptions('confirm', 'finishPostInspection', message);
        var $finishPostInspection = multiModal.setTemplateFile($finishPostInspection, 'templates/common/noRepairQualityModal.html');
        $finishPostInspection = multiModal.setActions($finishPostInspection, function () {}, function () {
            $commentModal = multiModal.setActions($commentModal,
                function () {
                    finishAll();
                },
                function () {
                    $scope.data.finalComment = "";
                    finishAll();
                });
            $scope.data.finalComment = comment;
            multiModal.openModal($commentModal, $scope, true);
        });
        $scope.data.finalComment = comment;
        multiModal.openModal($finishPostInspection, $scope);
    };

    /**
     * @name repairFieldChanged
     * @desc Check if a repair field was modified by user
     * @param {Object} repair to check against its field for changes
     * @param {String} fieldName field that was modified
     * @returns {Boolean} - True if repair fields were changed
     * @memberOf Controllers.UnitController
     */
    function repairFieldChanged(repair, fieldName) {
        if (fieldName == 'quantity' && repair.quantity == "0") return false;

        if (repair.$addedPart) return false;

        var repairIndex = $scope.obj.parts.indexOf(repair);
        var preRepair = $scope.obj.preAuditParts[repairIndex];

        if (repair[fieldName] == preRepair[fieldName]) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * @name partFieldChanged
     * @desc Check if a part field was modified by user
     * @param {Object} repair containing the part to check for modified fields
     * @param {Object} part to check against its fields for changes
     * @param {String} fieldName field that was modified
     * @returns {Boolean} - True if part fields were changed
     * @memberOf Controllers.UnitController
     */
    function partFieldChanged(repair, part, fieldName) {
        if (fieldName == 'quantity' && part.quantity == "0") return false;

        if (part.$addedPart) return false;

        var repairIndex = $scope.obj.parts.indexOf(repair);
        var partIndex = $scope.obj.parts[repairIndex].$childParts.indexOf(part);

        var prePart = $scope.obj.preAuditParts[repairIndex].$childParts[partIndex];

        if (part[fieldName] == prePart[fieldName]) {
            return false;
        } else {
            return true;
        }
    }

     /**
     * @name partFieldChanged
     * @desc Check if the inspection is in rejected status
     * @returns {Boolean} - True if inspection is rejected
     * @memberOf Controllers.UnitController
     */
    function checkIfRejected() {
        var rejected = false;
        var partsArray = obj.parts;

        if (partsArray) {
            for (var i = 0; i < partsArray.length; i++) {
                if (partsArray[i].$aggregateRejected) {
                    rejected = true;
                    break;
                }
            }
        }

        return rejected;
    };

     /**
     * @name finishAll
     * @desc Finish the current inspection by setting the status, final comment, RQ
     * @returns {Void}
     * @memberOf Controllers.UnitController
     */
    function finishAll() {
        if ($scope.requiredComment && $scope.data.finalComment.length == 0)
            return;

        //Control finishInspection using inspectionType variable to reject WO dispite all accepted parts
        if ($scope.isEstimatedButRejected) {
            var inspectionType = "Estimated But Rejected WO";
        }

        $unitRefer.setFinalComment($scope.data.finalComment)
        $unitRefer.setWORatingQuality($scope.obj.ratingRQ);
        $ionicLoading.show(loaderConfig);
        $unitRefer.finishInspection(inspectionType).then(function () {
            $scope.$evalAsync(function () {
                $scope.obj = $unitRefer.getObj();
            })
            setTimeout(function () {
                window.history.back();
            }, 100);
            $ionicLoading.hide();
        }).fail(function () {
            $ionicLoading.hide();
        });
    }

     /**
     * @name openGalleryModal
     * @desc Open the modal with photos gallery for current inspection
     * @param {String} auditStatus - status of current inspection
     * @returns {Void}
     * @memberOf Controllers.UnitController
     */
    function openGalleryModal(auditStatus) {

        //If auditStatus is finished, isVIsitFinished is TRUE
        if (auditStatus.localeCompare('Finished') == 0) {
            $scope.isVisitFinished = true;
        } else {
            $scope.isVisitFinished = false;
        }

        $scope.currentImgId = undefined;

        $scope.selectedImages = [];

        // Open main gallery multiModal
        var $galleryModalOptions = multiModal.setBasicOptions('confirm', 'galleryModal', 'notification', ' popup', false);
        $galleryModalOptions = multiModal.setTemplateFile($galleryModalOptions, 'templates/gallery/photoGallery.html');
        multiModal.openModal($galleryModalOptions, $scope);

        // Open photo details
        var $photoItemModalOptions = multiModal.setBasicOptions('confirm', 'photoItemModal', 'notification', ' popup', false);
        $photoItemModalOptions = multiModal.setTemplateFile($photoItemModalOptions, 'templates/gallery/image-viewer-modal.html');

        /**
         * @name openFullImg
         * @desc Open the photo full screen
         * @param {Integer} key - photo index
         * @returns {Void}
         * @memberOf Controllers.UnitController
         */
        $scope.openFullImg = function (key) {
            var photoData = {
                photos: [],
                index: null
            };

            for (index in $scope.dataArray.array) {
                var object = {
                    comment: '',
                    date: $scope.dataArray.array[index].taggedImages.json.date,
                    path: PhotoStorageDirectory.getDocumentsFolder() + "/" + $scope.dataArray.array[index].taggedImages.json.path,
                    tag: $scope.dataArray.array[index].taggedImages.json.date
                }
                photoData.photos.push(object);
                photoData.index = key;
            }

            hybrid.displayPhotoViewer(photoData);
        };

        /**
         * @name closeFullImg
         * @desc Close the full screen photo perspective
         * @param {Object} evt
         * @returns {Boolean} - FALSE if it's not the targeted html element to be closed
         * @memberOf Controllers.UnitController
         */
        $scope.closeFullImg = function (evt) {
            if (!angular.element(evt.target).hasClass('full-image-view')) {
                return false;
            }
            $scope.fullViewVisible = false;
        };

        var $confirmRemoveTag = multiModal.setBasicOptions('confirm', 'confirmRemoveTag', 'Are you sure you want to remove this tag?');

         /**
         * @name sendToEmail
         * @desc Send selected images via email
         * @returns {Void}
         * @memberOf Controllers.UnitController
         */
        $scope.sendToEmail = function () {
            var att = [];
            if (checkIfSelected($scope.dataArray.array)) {
                $scope.dataArray.array.forEach(function (image) {
                    if (image.checked) {
                        var attrComponent = {};
                        attrComponent = {
                            fileName: null,
                            data: null
                        };
                        attrComponent.fileName = image.taggedImages.json.name; //+'.jpeg';
                        attrComponent.data = image.base64.substring(23, image.base64.length);
                        att.push(attrComponent);
                    }

                });

                var $vOptionsEmailInput = multiModal.setBasicOptions('input', 'addEmail', null, 'popup', true, 'Send Email');
                $vOptionsEmailInput = multiModal.setTemplateFile($vOptionsEmailInput, 'templates/components/addEmailModal.html');
                $vOptionsEmailInput = multiModal.setActions($vOptionsEmailInput, function (userInput, isValid) {
                    $ionicLoading.show(loaderConfig);
                    $backend.sendMail(userContext.getEmail(), userInput, 'ContainerAudit-Photos', 'Photos sent from Audit Application :', JSON.stringify(att)).then(function (message) {
                        for (index in $scope.dataArray.array)
                            $scope.dataArray.array[index].checked = false;
                        var $emailSent = multiModal.setBasicOptions('notification', 'email', "Email successfully sent!");
                        $ionicLoading.hide();
                        multiModal.openModal($emailSent, $scope);
                    }).fail(function (err) {
                        var $emailSent = multiModal.setBasicOptions('notification', 'email', "Could not send email. Please check your internet connection.");
                        multiModal.openModal($emailSent, $scope);
                        $ionicLoading.hide();
                    });

                    // Reset selected images array
                    $scope.selectedImages = [];

                });
                multiModal.openModal($vOptionsEmailInput, $rootScope, true);
            } else {
                var $photoSelected = multiModal.setBasicOptions('notification', 'photoSelected', "Please select at least one photo!");
                multiModal.openModal($photoSelected, $scope);
            }
        };
    };

    //Open information modal
    function openInfoStartedModal() {
        multiModal.openModal($openInfoStartedModal, $scope, true);
    };

    //Set the modal view
    $scope.setView = function (view) {
        $scope.viewModal = view;
    }

    //Return the selected modal view 
    $scope.screenSelected = function () {
        return $scope.viewModal;
    };

    /**
    * @name deletePicture
    * @desc Delete selected pictures from the gallery
    * @returns {Void}
    * @memberOf Controllers.UnitController
    */
    function deletePicture() {

        if (checkIfSelected($scope.dataArray.array)) {
            // display a modal for confirmation
            var $confirmRemoveImages = multiModal.setBasicOptions('confirm', 'confirmRemoveImages', 'Are you sure you want to remove the selected images?');
            $confirmRemoveImages = multiModal.setActions($confirmRemoveImages, function () {
                deletePhotoFromCollection();
                $ionicScrollDelegate.$getByHandle('horizontalScroll').resize(true);
            });
            multiModal.openModal($confirmRemoveImages, $scope);
        } else {
            var $photoSelected = multiModal.setBasicOptions('notification', 'photoSelected', "Please select at least one photo!");
            multiModal.openModal($photoSelected, $scope);
        }
    };

    /**
    * @name deletePhotoFromCollection
    * @desc Delete marked photos from a specific collection
    * @returns {Void}
    * @memberOf Controllers.UnitController
    */
    function deletePhotoFromCollection() {
        for (var indexCollection = 0; indexCollection < $scope.dataArray.array.length; indexCollection++) {
            if ($scope.dataArray.array[indexCollection].checked == true) {
                // delete file from JsonStore
                $localDB.erase('galleryPhoto', $scope.dataArray.array[indexCollection].taggedImages, {});
                $base64EncoderDecoder.removeFileFromPhoneGap(cordova.file.applicationStorageDirectory +
                    "Documents/" + $scope.dataArray.array[indexCollection].taggedImages.json.path, $scope.dataArray.array[indexCollection].taggedImages.json.path.split("/").pop());
                $scope.dataArray.array.splice(indexCollection, 1);
                indexCollection--;
            }
        }
    };

    /**
    * @name checkIfSelected
    * @desc Checks if there is at least one photo selected
    * @param {Array} data - array of photos object from gallery
    * @returns {Boolean} - True if there is at least one photo selected
    * @memberOf Controllers.UnitController
    */
    function checkIfSelected(data) {
        for (var index in data) {
            if (data[index].checked)
                return true;
        }
        return false;
    };


    /**
     * @name compressLines
     * @desc Collapse all the repairs and parts onDestroy
     * @returns {void}
     * @memberOf Controllers.UnitController
     */
    function compressLines() {
        $scope.obj.parts.forEach(function (part) {
            if (part.$visible === true)
                part.$visible = !part.$visible;

            if (!part.$childParts) {
                return;
            }

            if (part.$childParts.length > 0) {
                part.$childParts.forEach(function (child) {
                    if (child.$visible === true)
                        child.$visible = !child.$visible;
                });
            }
        });
    }

    //Remove all modals
    function hideModal() {
        multiModal.removeAllModals();
    };

    //Show inspection comment
    function showComment() {
        multiModal.openModal($commentModalNotEditable, $scope, true);
    };

    //Triggered when user start swipe 
    function onSwipeStart(dir, deltaX, deltaY, $event) {
        $scope.startSwipe = $event.x;
    };

    //Event triggered onMove swipe action
    function onSwipeMove(dir, deltaX, deltaY) {
        var x = parseInt($scope.startSwipe);
        var aux = parseInt(deltaX);
        if (x + aux > (window.innerWidth - 7 / 100 * window.innerWidth))
            try {
                hybrid.unblockScroll();
            } catch (ex) {

            }
    };

    //Helper function: Fast Click on Textarea
    function fastClickTextareaModal() {
        var fastClickTextarea = document.getElementById('modalTextarea');
        FastClick.attach(fastClickTextarea);
        return true;
    }

    //Helper function; Fast click on textarea read-only
    function fastClickTextareaNoEditModal() {
        var fastClickTextareaFinal = document.getElementById('modalTextareaFinal');
        FastClick.attach(fastClickTextareaFinal);
        return true;
    }

    //Event triggered when the scope is destroyed
    $scope.$on("$destroy", function () {
        closeAllRqDropdowns();
        $galleryModule.setData({
            array: []
        });
        compressLines();
    });

};