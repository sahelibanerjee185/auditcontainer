
audit
	.controller("PdfController", ['$scope','$document','$ionicScrollDelegate', function($scope,$document,$ionicScrollDelegate){
		
		var signaturePadAuditor = new SignaturePad(document.getElementById('canvas_1'));
	    var signaturePadShopManager = new SignaturePad(document.getElementById('canvas_2'));
	    
		$scope.touched = false;

	    $scope.touchStart = function() {
	        $scope.touched = true;
	    }

	    $scope.touchEnd = function() {
	        $scope.touched = false;
	    }
	    
	    $scope.agreement = {
	    	auditor: false,
	    	shopManager: false
	    };
	    
	    $scope.signature = {
	    	auditor: false,
	    	shopManager: false
	    };
	    
	    $scope.auditorSigned = function() {
	    	$scope.signature.auditor = true;
	    	$scope.getCanvasImage(signaturePadAuditor,'canvas_1');
	    };
	    
	    $scope.shopManagerSigned = function() {
	    	$scope.signature.shopManager = true;
	    	$scope.getCanvasImage(signaturePadShopManager,'canvas_2');
	    };
		
	    $scope.signatures = {};
	    
	    $scope.clearAuditorSignature = function(){
			$scope.clearCanvas(signaturePadAuditor);
			$scope.signature.auditor = false;
		};
		
		$scope.clearShopManagerSignature = function(){
			$scope.clearCanvas(signaturePadShopManager);
			$scope.signature.shopManager = false;
		};
		
		$scope.clearCanvas = function(canvasObj){
			canvasObj.clear();
		};
		
		$scope.getCanvasImage = function(canvasObj, id){
        	var dataURL = canvasObj.toDataURL("image/png");
        	$scope.signatures[id] = dataURL;
        	return dataURL;
        };
		
		 $scope.freezeScroll = function(handle) {
	    	$ionicScrollDelegate.$getByHandle(handle).getScrollView().options.scrollingY = false;
	     }
	     $scope.enableScroll = function(handle) {
	    	$ionicScrollDelegate.$getByHandle(handle).getScrollView().options.scrollingY = true;
	     }
		
	     $scope.pushNotificationChange = function() {
	    	 console.log('Push Notification Change', $scope.pushNotification.checked);
         };
           
         $scope.pushNotification = { checked: true };
           
}]).directive('myTouchstart', [function() {
    return function(scope, element, attr) {

        element.on('touchstart', function(event) {
            scope.$apply(function() { 
                scope.$eval(attr.myTouchstart); 
            });
        });
    };
}]).directive('myTouchend', [function() {
    return function(scope, element, attr) {

        element.on('touchend', function(event) {
            scope.$apply(function() { 
                scope.$eval(attr.myTouchend); 
            });
        });
    };
}]);