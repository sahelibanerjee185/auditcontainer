/**
 * CalendarCtrl Controller
 * @namespace Controllers
 */

(function(){
	'use strict';
	
	audit.controller('CalendarCtrl', CalendarCtrl);
	
	CalendarCtrl.$inject = ['$surveyModule', '$depot', '$backend', '$scope', '$location',
	    '$ionicPopover', '$ionicPosition', '$timeout', '$commonOperations', 'multiModal',
	    '$ionicLoading', '$filter', 'auditService', 'startedAudit', '$routeParams',
	    'reportGenerator', '$rootScope', 'AuditPollingService', 'PendingAuditsService',
	    '$localDB', 'pdfModule', 'finishVisitProgressService', 'pdfGenerationHandler',
	    'EmailNotificationService', 'ToastService'];
	
	/**
	 * @namespace CalendarCtrl
	 * @desc Controller used for inspections screen, provides overview of visit activity and working with a started visit
	 * @memberOf Controllers
	 */
	function CalendarCtrl($surveyModule, $depot, $backend, $scope, $location, $ionicPopover,
	    $ionicPosition, $timeout, $commonOperations, multiModal, $ionicLoading, $filter,
	    auditService, startedAudit, $routeParams, reportGenerator, $rootScope, AuditPollingService,
	    PendingAuditsService, $localDB, pdfModule, finishVisitProgressService, pdfGenerationHandler,
	    EmailNotificationService, ToastService) {
	
	    // FINISH VISIT MODAL MOCKUP
	    var $finishVisitModal = multiModal.setBasicOptions('notification', 'finishVisit', null, 'popup');
	    $finishVisitModal = multiModal.setTemplateFile($finishVisitModal, 'templates/components/finishVisitModal.html');
	    /**************************/
	
	    $scope.changeView = changeView;
		$scope.confirmCancelVisit = confirmCancelVisit;
	    $scope.currentStep = 1;
		$scope.deletePdf = deletePdf;
	    $scope.getStartedTabData = getStartedTabData;
		$scope.goToDepotDetails = goToDepotDetails;
		$scope.goToPlanVisit = goToPlanVisit;
	    $scope.isVisitStarted = startedAudit.isVisitStarted();
	    $scope.togleStatus = false;
		$scope.openPopover = openPopover;
		$scope.selectVisitDate = selectVisitDate;
	    $scope.setView = setView;
		$scope.screenSelected = screenSelected;
	    $scope.steps = {};

		if ($routeParams.tab == 'started' || $scope.isVisitStarted) {
	        $scope.$evalAsync(function () {
	            $scope.screen = 'started';
	        })
	    }
	
	    if ($routeParams.tab == 'upcoming') {
	        $scope.$evalAsync(function () {
	            $scope.screen = 'upcoming';
	        })
	    }
	
	    $scope.obj = $surveyModule.getObj();
	    $scope.disableUI = false;
	
	    // Get the native footer height to correctly set the keyboard height
	    hybrid.getFooterInfo();
	
	    // Datepicker init
	    $scope.datePicker = {};
	    $scope.datePicker.date = new Date();
	
	    //Set current tab based on existing visits found in Obj.
	    $scope.screen = $scope.obj.currentTab;

		// Prepare the tooltip
	    $ionicPopover.fromTemplateUrl('templates/survey/visitsTooltip.html', {
	        scope: $scope,
	    }).then(function (popover) {
	        $scope.popover = popover;
	    });

		/**
	     * @name changeView
	     * @desc Sets current view in the inspections tab
	     * @param {Bool} togleStatus, if true sets month view.
	     * @returns N/A - has side effects
	     * @memberOf Controllers.CalendarCtrl
	     */
	    function changeView(togleStatus) {
	        setTimeout(function () {
	            $ionicLoading.show(loaderConfig);
	        }, 5);
	
	        if ($scope.togleStatus == false) {
	            if ($routeParams.tab == 'started' || $scope.isVisitStarted) {
	                $scope.setView('started');
	                $rootScope.lastView = 'started';
	                $scope.screen = "started";
	                $scope.togleStatusForFull = $scope.togleStatus;
	            } else if (typeof $rootScope.lastView === 'undefined') {
                    console.log("changeView upcoming");
	                $scope.setView('upcoming');
	                $rootScope.lastView = 'upcoming';
	                $scope.screen = "upcoming";
	                $scope.togleStatusForFull = $scope.togleStatus;
	            } else {
	                $scope.setView($rootScope.lastView);
	                $scope.togleStatusForFull = $scope.togleStatus;
	            }
	        }
	        else {
	            $rootScope.lastView = $scope.screen;
	            if ($rootScope.lastView != 'month') {
	                $ionicLoading.show(loaderConfig);
	                setTimeout(function () {
                        console.log("changeView month");
	                    $scope.setView('month');
	                    $scope.togleStatusForFull = $scope.togleStatus;
	                }, 5);
	
	            }
	        }
	        setTimeout(function () {
	            $ionicLoading.hide();
	        }, 400);
	    }
	
	    /**
	     * @name getStartedTabData
	     * @desc Populates the '$depot' factory variable 'object' with the current started visit data, UI is based on this data
	     * @param N/A
	     * @returns {Promise}
	     * @memberOf Controllers.CalendarCtrl
	     */
	    function getStartedTabData() {
	        var dfd = $.Deferred();
	
	        startedAudit.getStartedVisit()
	            .then(function (visitId) {
	                return $depot.getVisitInfo(visitId, "Started");
	            })
	            .then(function () {
	                dfd.resolve();
	            }).fail(function (e) {
	                dfd.reject(e);
	            });
	
	        return dfd.promise();
	    }
	
	    /**
	     * @name setView
	     * @desc Updates data used in UI for each of the tabs in the inspections screen	
	     * @param {String} view, name of view to display
	     * @returns N/A - has side effects
	     * @memberOf Controllers.CalendarCtrl
	     */
	    function setView(view) {
	        if (view != 'month') {
	            $rootScope.lastView = view;
	        }
	        if ($scope.disableUI) return;
            console.log("$rootScope.lastView", $rootScope.lastView);
            console.log("view", view);
            hybrid.updateNativeUI(RoutingTitles[view], true, true, false, true);

	        if (view == 'started') {
	        	if (!$scope.isVisitStarted) return;
	            $ionicLoading.show(loaderConfig);
	            $scope.screen = view;
	
	            // Get started visit and display needed info
	            getStartedTabData().then(function() {
	                $ionicLoading.hide();
	                $timeout(function() { });
	            }).fail(function(e) {
	                $ionicLoading.hide();
	                var $vFailed = multiModal.setBasicOptions('notification', 'vFailed', 'Could not load visit data. Please try again.');
	                multiModal.openModal($vFailed, $scope);
	            });
	        } else if (view == 'month') {
	            $ionicLoading.show(loaderConfig);
	            $scope.screen = view;
	        } else {
                // hybrid.updateNativeUI(RoutingTitles["month"], true, true, false, true);
	            $scope.screen = view;
	        }
	    };
	
	    $scope.changeView();
	
	    function screenSelected() {
	        if ($scope.screen == 'started') {
	            $scope.obj = pdfGenerationHandler.generatePdfModel($scope.obj);
	        }
	
	        return $scope.screen;
	    };
	
	    function deletePdf() {
	        return pdfGenerationHandler.deletePdf();
	    };
	
		/**
	     * @name openPopover
	     * @desc displays the popover positioned accordingly
		 * depending on calendar date element positioning
	     * @param {Object} $event, tap event passed from html - not used anymore
		 * @param {Object} $el, element tapped passed from html
	     * @returns N/A - has side effects
	     * @memberOf Controllers.CalendarCtrl
	     */
		function openPopover($event, $el) {
	        $scope.popover.show($event);
	        $scope.elPosition = getOffset($el);
	
	        $scope.windowPosition = getOffset(angular.element(document.getElementsByTagName('body')));
	
	        if (($scope.windowPosition.width - $scope.elPosition.left) < 160) {
	            // Tooltip is on right side
	            document.getElementById("tooltipContent").style.left = '';
	            angular.element(document.getElementById("tooltipContent")).removeClass('leftTooltip').addClass('rightTooltip');
	        } else if ($scope.elPosition.left < 145) {
	            // Tooltip is on left side
	            document.getElementById("tooltipContent").style.left = '';
	            angular.element(document.getElementById("tooltipContent")).removeClass('rightTooltip').addClass('leftTooltip');
	        } else {
	            angular.element(document.getElementById("tooltipContent")).removeClass('rightTooltip').removeClass('leftTooltip');
	            document.getElementById("tooltipContent").style.left = ($scope.elPosition.left - 80) + "px";
	        }
	
	        if ($scope.elPosition.top > 400) {
	            // Tooltip is on bottom
	            document.getElementById("tooltipContent").style.top = 'auto';
	            document.getElementById("tooltipContent").style.bottom = (($scope.windowPosition.height - $scope.elPosition.top - 50)) + "px";
	            angular.element(document.getElementById("tooltipContent")).addClass('bottomTooltip');
	        } else {
	            document.getElementById("tooltipContent").style.bottom = 'auto';
	            document.getElementById("tooltipContent").style.top = ($scope.elPosition.top + 110) + "px";
	            angular.element(document.getElementById("tooltipContent")).removeClass('bottomTooltip');
	        }
	    };
	
	    function getOffset(element) {
	        return $ionicPosition.position(element);
	    };
	
	    function closePopover() {
	        $scope.popover.hide();
	    };
	
	    //Cleanup the popover when we're done with it!
	    $scope.$on('$destroy', function () {
	        $scope.popover.remove();
	    });
	
		/**
	     * @name selectVisitDate
	     * @desc applies logic to see if date is in the past, current day or future
		 * to display proper UI and get selected date associated visits if needed.
		 * Note that this function is called from "ui-bootstrap-tpls-0.13.0.js file"
		 * which includes the calendar functionality
	     * @param {String} $event, tap event passed from html - not used anymore
		 * @param {Object} clickedButton, element tapped passed from html
	     * @returns N/A - has side effects
	     * @memberOf Controllers.CalendarCtrl
	     */
	    function selectVisitDate(selectedDate, clickedButton) {
	        var today = new Date();
	        today.setHours(0, 0, 0, 0);
	        var dfd = $.Deferred();
	        var toolTipButton = angular.element(clickedButton);
	        var jsonStoreDate = $filter('date')(selectedDate, 'yyyy-MM-dd');
	        var showToolTip;
	        $scope.selectedDayVisits = [];
	
	        $scope.visitsMessage = 'Loading...';
	        if ($scope.obj.dateHasVisit[jsonStoreDate]) {
	            $ionicLoading.show(loaderConfig);
	            $surveyModule.getVisitsForDay(jsonStoreDate).then(function (visits) {
	                $scope.selectedDayVisits = visits;
	                dfd.resolve();
	                $scope.visitsMessage = false;
	            });
	        } else if (selectedDate.getTime() > today.getTime()) {
	            $scope.visitsMessage = 'You have no visits planned.';
	            dfd.resolve();
	        } else {
	            $scope.visitsMessage = false;
	        }
	
	        dfd.promise().then(function () {
	            $ionicLoading.hide();
	
	            $scope.$evalAsync(function () {
	                $scope.toolTipAddVisit = (selectedDate.getTime() > today.getTime()) ? true : false;
	            });
	
	            if ($scope.selectedDayVisits.length > 0) {
	                showToolTip = true;
	            } else if (selectedDate > today) {
	                showToolTip = true;
	                $scope.toolTipAddVisit = true;
	            } else {
	                showToolTip = false;
	                $scope.toolTipAddVisit = false;
	            }
	            if (showToolTip) {
	                $scope.selectedDate = selectedDate;
	                showToolTipForButton(toolTipButton);
	            }
	        });
	    };
	
	    function showToolTipForButton(toolTipButton) {
	        $scope.openPopover('', toolTipButton);
	    };
	
		/**
	     * @name goToDepotDetails
	     * @desc retrieves the corresponding visit data, populating the $depot service
		 * and directs the user to the visit details screen
	     * @param {String} visitId visit id passed from html
		 * @param {String} auditStatus, audit status passed from html
	     * @returns N/A - has side effects
	     * @memberOf Controllers.CalendarCtrl
	     */
	    function goToDepotDetails(visitId, auditStatus) {
	        $ionicLoading.show(loaderConfig);
	
	        $depot.getVisitInfo(visitId, auditStatus).then(function () {
	            goToDepotScreen();
	        }).fail(function (e) {
	            // show error
	            $ionicLoading.hide();
	            var $error = multiModal.setBasicOptions('notification', 'error', 'No cached information for this visit. Please retry with internet connection.');
	            multiModal.openModal($error, $scope);
	        });
	    };
	
	    function goToDepotScreen() {
	        $ionicLoading.hide();
	        $scope.$evalAsync(function () {
	            $location.path('/depotDetails/');
	        });
	    }
	
		/**
	     * @name goToPlanVisit
	     * @desc Directs user to Plan Visit tab and passes the selected date
		 * as a route parameter, the date passed is auto-selected 
		 * in the Plan Visit screen
	     * @param {String} date 
	     * @returns N/A - has side effects
	     * @memberOf Controllers.CalendarCtrl
	     */
	    function goToPlanVisit(date) {
	        auditService.testSignal().then(function () {
	            var jsonStoreDate = $filter('date')(date, 'dd-MM-yyyy');
	            $scope.$evalAsync(function () {
	                $location.switchTab('/planVisitTab/' + jsonStoreDate);
	            });
	        }).fail(function () {
	            var $confirmOfflineModeModal = multiModal.setBasicOptions('notification', 'offlineMode', 'This action can\'t be perfomed in offline mode', 'popup');
	            $confirmOfflineModeModal = multiModal.setActions($confirmOfflineModeModal);
	
	            multiModal.openModal($confirmOfflineModeModal, $scope);
	        });
	    };
	
		/**
	     * @name confirmCancelVisit
	     * @desc Cancels a visit and removes all related data
		 * Populates the data needed for the email notification modal
	     * @param {String} visitId 
	     * @returns N/A - has side effects
	     * @memberOf Controllers.CalendarCtrl
	     */
	    function confirmCancelVisit(visitId) {
	        auditService.testSignal().then(function () {
				var $confirmRemoveVisit = multiModal.setBasicOptions('confirm', 'confirmRemoveVisit', 'Are you sure you want to cancel this visit?');
	            $confirmRemoveVisit = multiModal.setActions($confirmRemoveVisit, function () {
	                $ionicLoading.show(loaderConfig);
	                $localDB.find('visit', { visitId: visitId }, {}).then(function(visitShop) {
	                    var shopCode = visitShop[0].json.shopCode;
	                    var containerList = getContainerList(visitShop);
	                    $scope.auditDate = visitShop[0].json.auditDate;
	                    auditService.cancelAudit(visitId).then(function () {
	                        $surveyModule.refreshVisitData().then(function () {
	                            $scope.$evalAsync(function () {
	                                $ionicLoading.hide();
	                                EmailNotificationService.setCallback(cancelVisitCallback);
	                                EmailNotificationService.showModal('cancelVisit', $scope, shopCode, containerList);
	                            });
	                        });
	                    });
	                });
	            });
	            multiModal.openModal($confirmRemoveVisit, $scope);
	        }).fail(function () {
	            var $confirmOfflineModeModal = multiModal.setBasicOptions('notification', 'offlineMode', 'This action can\'t be perfomed in offline mode', 'popup');
	            $confirmOfflineModeModal = multiModal.setActions($confirmOfflineModeModal);
	
	            multiModal.openModal($confirmOfflineModeModal, $scope);
	        });
	    };
	
	    function getContainerList(jsonStoreWOs) {
	        var containerList = "";
	        var arrayHelper = [];
	        for (var i = 0; i < jsonStoreWOs.length; i++) {
	            var workOrderInfo = jsonStoreWOs[i].json;
	            arrayHelper.push(workOrderInfo.containerNo);
	        }
	
	        containerList = arrayHelper.join(", ");
	        return containerList;
	    }
	
	    function cancelVisitCallback() {
	        multiModal.removeAllModals();
	        ToastService.showNotificationMsg('Your visit has been successfully canceled.')
	    }
	}
})();

