/**
 * Failed Inspections Controller
 * @namespace Controllers
 */
audit.controller('FailedInspectionsController',FailedInspectionsController);

FailedInspectionsController.$inject = ['$scope','$kpiModule','$backend','$location','$timeout',
	'multiModal','$commonOperations','$filter','KPIService','$ionicScrollDelegate']; 


/**
 * @namespace FailedInspectionsController
 * @desc Controller used populate failed inspections KPIs
 * @memberOf Controllers
 */
function FailedInspectionsController($scope,$kpiModule,$backend,$location,$timeout,multiModal,
	$commonOperations,$filter,KPIService,$ionicScrollDelegate) {

	$scope.showLoader = true;
	$scope.parts = {};
	$scope.graph={};
	$scope.graph.labels = [];
	$scope.graph.series = [{ name: "Spare Part", code: "#004e6b" }, { name: "Repair Code", code: "#fcb91c" }, { name: "Repair Loc. Code", code: "#69b8d6" }, { name: "Damage Code", code: "#ff7507" }, { name: "Pcs.", code: "#b50030" }, { name: "Tpi", code: "#52c1b8" }];
	
	$scope.failledData = $kpiModule.getFailedObj();
	$scope.graph.data = [];
	$scope.graph.data.push($scope.failledData.sparePartNumber);
	$scope.graph.data.push($scope.failledData.repairCodeNumber);
	$scope.graph.data.push($scope.failledData.repairLocCodeNumber);
	$scope.graph.data.push($scope.failledData.damageCodeNumber);
	$scope.graph.data.push($scope.failledData.pcsNumber);
	$scope.graph.data.push($scope.failledData.tpiNumber);
	
	$scope.graph.colors = ['#004e6b','#fcb91c','#69b8d6','#ff7507','#b50030','#52c1b8'];
	$scope.graph.popupData = ['Spare Part','Repair Code','Repair Loc. Code','Damage Code','Pcs.','Tpi'];;
		
	$scope.graph.someOptions = {
		 datasetFill : false,	
    };
	
	//Retrieve failed Work Orders for a specified shop
	KPIService.getAllFailledWOForShop($kpiModule.getShopCode()).then(function(dataWO){
		if(dataWO.length == 0){
			$scope.$evalAsync(function(){
				$scope.showLoader = false;
			})
		}else{
			var count =0;
			for(index in dataWO){
				KPIService.getAllPartsForWO(dataWO[index].WO_ID, dataWO[index].AUDIT_DATE.substring(0,10)+"%")
					.then(function(data){
						$scope.parts[data[0].woNo] = data;
					count++;
					if(count == dataWO.length){
						$scope.$evalAsync(function(){
							$scope.failedWONumbers = dataWO;
							$scope.showLoader = false;
						})
					}
				})
			}
		}
	});

	//Takes the last 12 months from the current date 
	var today = new Date();
	var aMonth = today.getMonth();
	for(i = aMonth , a = 0;  a<12 ; a++, i--){
		if(i == -1) i = 11;
		$scope.graph.labels.unshift(monthNames[i])
	}
	 
	 //Preparing the history modal
	 var $kpiModalHistory= multiModal.setBasicOptions('notification', 'KpiModal',' popup', false);
	 $kpiModalHistory = multiModal.setTemplateFile($kpiModalHistory, 'templates/kpi/kpiModal.html');
	 
	/**
	* @name viewHistory
	* @desc Show history of a work order 
	* @param {Integer} woNo unique identifier of a work order
	* @returns {Void}
    * @memberOf Controllers.FailedInspectionsController
    */
	 $scope.viewHistory=function(woNo){
		$scope.modalParts = $scope.parts[woNo];
		multiModal.openModal($kpiModalHistory, $scope);
	 };
	 
	 $scope.res = function(){
		 $ionicScrollDelegate.resize();
	 }
	
	 // function for the header sorting
	$scope.defaultOrderTag = 'depotName';
	$scope.switchOrder = true;   // ascending or descending

	/**
	* @name ShopsOrderFunction
	* @desc Change the filtering order
	* @param {String} defaultOrderTag tag to order by
	* @returns {Void}
    * @memberOf Controllers.FailedInspectionsController
    */
	$scope.ShopsOrderFunction = function(defaultOrderTag) {
		$scope.switchOrder = ($scope.defaultOrderTag === defaultOrderTag) ? !$scope.switchOrder : false;
		$scope.defaultOrderTag = defaultOrderTag;
	};
	 
}