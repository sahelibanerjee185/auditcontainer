/* JavaScript content from js/controllers/ManualsController.js in folder common */

/* JavaScript content from js/controllers/ManualsController.js in folder common */

audit
    .controller(
        "ManualsController", ['multiModal', '$locale', '$ionicLoading', '$scope', '$http', '$routeParams', '$ionicScrollDelegate', '$timeout', '$backend', '$localDB', '$manualsModule', '$filter', '$codesModule', 'ToastService', 'auditService', 'WOCodesService', '$planVisit',
            function (multiModal, $locale, $ionicLoading, $scope, $http, $routeParams, $ionicScrollDelegate, $timeout, $backend, $localDB, $manualsModule, $filter, $codesModule, ToastService, auditService, WOCodesService, $planVisit) {


                //SCOPE VARIABLES
                $scope.breadcrumbObject = {
                    title: "Manuals",
                    call: "changeView",
                    params: [true],
                    level: 0
                };
                $scope.breadcrumbsArray = [$scope.breadcrumbObject];
                $scope.filterHelp = {};
                $scope.manualBase64String = {};
                $scope.convertedDate = [];
                $scope.arrayOfFormatedDates = [];
                $scope.obj = $manualsModule.getObj();
                $scope.formatedDate = {};
                $scope.sysInfoModalView = 'templates/manuals/manuals.html';
                $scope.inverseOrder = true;
                $scope.searchFieldsFilter = ["title", "addedDate", "language"];
                $scope.manualsView = true;
                $scope.codeScreens = ["Repair STS Code", "Master Parts", "Associate Parts with Code/Mode", "Container Repair Location", "Damage Code", "Shop Profile", "Shop Limits", "Shop Contract", "Labor Rates"];
                $scope.selectedCodeScreen = -1;
                $scope.currentResults = [];
                $scope.detailedCode = {};
                $scope.shopSet = false;
                $scope.blurAllInputs = blurAllInputs;
                $scope.code = {
                    search: "",
                    search2: "",
                    repairCode: '',
                    shopCode: ''
                };
                $scope.config = {
                    pagesLimit: 3,
                    entriesPerPage: 9,
                    arrayLength: 0,
                    pageNo: 1
                }

                $scope.preFilterSettings = {};

                $scope.dropdownInfo = {
                	showDropManual : false,
                	showDropMode : false,
                	showDropPartsGroup : false,
                	showDropManufacturerCode : false
                }

                // Dropdowns

                $scope.manualCodesList = [];
                $scope.modeCodesList = [];
                $scope.partGroupsList = [];
                $scope.manufacturerCodesList = [];

                // LOCAL VARIABLES
                var manualsSearchTimer;
                var manualsWatch = $scope.$watch('code.search2', function () {
                    $timeout.cancel(manualsSearchTimer);

                    manualsSearchTimer = $timeout(function () {
                        $scope.scrollToTop();
                    });
                });
                var $noDoneScreenModal = multiModal.setBasicOptions('notification', 'noDone', 'Cannot access this screen','popup');
                var $failRetriveCodeData = multiModal.setBasicOptions('notification', 'failData', 'Could not retrieve data from server, please check your internet connection.','popup');

                //FUNCTION DECLARATIONS
                $scope.callBreadcrumbFunction = callBreadcrumbFunction;
                $scope.changePageResults = changePageResults;
                $scope.changeView = changeView;
                $scope.goBackToPreviousScreen = goBackToPreviousScreen;
                $scope.goBackToShopProfile = goBackToShopProfile;
                $scope.goToCodeScreen = goToCodeScreen;
                $scope.goToPageDetails = goToPageDetails;
                $scope.goToShopDetails = goToShopDetails;
                $scope.orderManuals = orderManuals;
                $scope.preserveStateAfterClose = preserveStateAfterClose;
                $scope.resetCodeSearch = resetCodeSearch;
                $scope.resetData = resetData;
                $scope.resetSearch2 = resetSearch2;
                $scope.scrollToTop = scrollToTop;
                $scope.searchByShopCode = searchByShopCode;
                $scope.sortEntries = sortEntries;
                $scope.startsWith = startsWith;
                $scope.updateBreadcrums = updateBreadcrums;
                $scope.updateResults = updateResults;
                $scope.viewPdf = viewPdf;
                $scope.keyboardEnter = keyboardEnter;
                $scope.resetDropDowns = resetDropDowns;
                $scope.showAdditionalMessage = showAdditionalMessage;
                $scope.validateAndSetShopCode = validateAndSetShopCode;
                $scope.searchForContracts = searchForContracts;
                $scope.resetShopSearch = resetShopSearch;
                $scope.resetRepairSearch = resetRepairSearch;
                $scope.selectManualCode = selectManualCode;
                $scope.selectModeCode = selectModeCode;
                $scope.selectPartsGroupCode = selectPartsGroupCode;
                $scope.selectManufacturerCode = selectManufacturerCode;
                $scope.selectDropdown = selectDropdown;

                //FUNCTION CALLS
                $codesModule.setEntriesPerPage($scope.config.entriesPerPage);

                //FUNCTION IMPLEMENTATION

                function keyboardEnter($event){
                  	 $event.target.blur();
                };

                function preserveStateAfterClose(){
                    hybrid.switchToolbarButton(hybrid.getCurrentFooterButtonIndex());
                };

                function updateBreadcrums(pageTitle, functionToBeCalled, paramsForFunction, level) {

                    $scope.breadcrumbsArray.splice(level, $scope.breadcrumbsArray.length);
                    $scope.breadcrumbObject = {
                        title: pageTitle,
                        call: functionToBeCalled,
                        params: paramsForFunction,
                        level: level
                    };
                    $scope.breadcrumbsArray.push($scope.breadcrumbObject);
                };

                function callBreadcrumbFunction(obj) {
                    if(obj == $scope.breadcrumbsArray[$scope.breadcrumbsArray.length -1]){
                        return;
                    }
                    if (angular.isFunction($scope[obj.call])) {
                        $scope[obj.call].apply(this, obj.params);
                    }
                };

                function viewPdf2($backend, $scope, manualPath,
                                 $index, $ionicLoading) {
                    // call getPdf function from backend and retrive the base64 FDP from server
                    $backend
                        .getManual(manualPath)
                        .then(
                            function (manualInfo) {
                                // i need relative path and title!
                                nativePdfPromise = $
                                    .Deferred();

                                WL.App
                                    .sendActionToNative(
                                        "previewPdf", {
                                            pdfPath: manualInfo.relativePath,
                                            pdfName: manualInfo.manualTitle
                                        });

                                nativePdfPromise
                                    .promise()
                                    .then(
                                        function () {
                                            $ionicLoading
                                                .hide();
                                        })
                                    .fail(
                                        function () {
                                            $ionicLoading
                                                .hide();
                                            // Fail message
                                        });
                            })
                        .fail(
                            function () {
                                $ionicLoading.hide();
                                var $pdfFailed = multiModal
                                    .setBasicOptions(
                                        'notification',
                                        'pdfFailed',
                                        'You need internet connection to download this manual.');
                                multiModal.openModal(
                                    $pdfFailed, $scope);
                            });
                }

                function resetSearch2() {
                    $scope.code.search2 = "";
                    document.getElementById("manualInput").blur(); 
                };

                function orderManuals(delaultManual) {
                    $scope.inverseOrder = ($scope.delaultManual === delaultManual) ? !$scope.inverseOrder :
                        false;
                    $scope.delaultManual = delaultManual;
                };

                function scrollToTop(noBlur) {
                    $ionicScrollDelegate.scrollTop();
                }

                function viewPdf(serverPath, $index) {
                    var manualPath = serverPath;

                    $ionicLoading.show(loaderConfig);
                    // call getMANUAL function from backend and retrive the base64 FDP from server
                    viewPdf2($backend, $scope, manualPath, $index,
                        $ionicLoading)

                }

                function startsWith(actual, expected) {
                    var lowerStr = (actual + "").toLowerCase();
                    return lowerStr.indexOf(expected.toLowerCase()) === 0;
                }

                //Implementation of the SystemInfo Modal view

                function changeView(view) {
                    if (view) {
                        $scope.updateBreadcrums("Manuals", "changeView", [true], 0);
                        $scope.sysInfoModalView = 'templates/manuals/manuals.html';
                    } else {
                        $scope.updateBreadcrums("MERC+ Codes", "changeView", [false], 0);
                        $scope.sysInfoModalView = 'templates/manuals/codes.html';
                    }
                }

                function goToCodeScreen(screen, index) {

                  $scope.selectedCodeScreen = index;
                  $timeout(function(){
                    $scope.selectedCodeScreen = -1;
                  }, 500, true);

                    var sysInfoModalView = 'templates/manuals/codes.html';
                    var functionArray = [];
                    //make this 2 one
                    resetCodeObj();
                    $codesModule.resetCodeData();
                    $codesModule.resetPrefilterSettings();
                    $scope.resetDropDowns();
                    $scope.preFilterSettings = {};

                    $ionicLoading.show(loaderConfig);
                    switch (screen) {
                        case "Repair STS Code":
                        	sysInfoModalView = "templates/manuals/Codes/repairSTS.html";
                        	functionArray.push($codesModule.getRepairSTSCodesData());
                        	$scope.manualCodesList = WOCodesService.getDropdownListsInfo().manualCodesList;
                        	$scope.modeCodesList = $planVisit.getObj().woModeList.resultSet;
                            break;
                        case "Master Parts":
                        	sysInfoModalView = "templates/manuals/Codes/masterParts.html";
                        	functionArray.push($codesModule.getMasterPartsData());
                        	$scope.partGroupsList = WOCodesService.getDropdownListsInfo().partGroupsList;
                        	$scope.manufacturerCodesList = WOCodesService.getDropdownListsInfo().manufacturerCodesList;
                            break;
                        case "Associate Parts with Code/Mode":
                        	sysInfoModalView = "templates/manuals/Codes/assPartsCodeMode.html";
                        	functionArray.push($codesModule.getAssociatePartsWithCodesData());
                        	$scope.manualCodesList = WOCodesService.getDropdownListsInfo().manualCodesList;
                        	$scope.modeCodesList = $planVisit.getObj().woModeList.resultSet;
                            break;
                        case "Container Repair Location":
                            sysInfoModalView = "templates/manuals/Codes/containerRepairLocation.html";
                            functionArray.push($codesModule.getRepairLocCodesData());
                            break;
                        case "Damage Code":
                            sysInfoModalView = "templates/manuals/Codes/damageCode.html";
                            functionArray.push($codesModule.getDamageCodeData());
                            break;
                        case "Shop Profile":
                            sysInfoModalView = "templates/manuals/Codes/shopProfile.html";
                            $codesModule.setSearchFieldsForShop($codesModule.getShopInfoData());
                            break;
                        case "Shop Limits":
                        	sysInfoModalView = "templates/manuals/Codes/shopLimits.html";
                        	functionArray.push($codesModule.getShopLimits());
                            break;
                        case "Shop Contract":
                        	sysInfoModalView = "templates/manuals/Codes/shopContact.html";
                        	$scope.code.shopSet = false;
                            break;
                        case "Labor Rates":
                        	sysInfoModalView = "templates/manuals/Codes/laborRates.html";
                        	functionArray.push($codesModule.getLaborRates());
                            break;
                    }

                    $.when.apply($, functionArray).then(function () {
                        $scope.config.arrayLength = $codesModule.getDataArrayLength();
                        $scope.currentResults = $codesModule.setCurrentResults($scope.config.pageNo, $scope.config.entriesPerPage, "");
                        $scope.$evalAsync(function () {
                            $scope.updateBreadcrums(screen, "goToCodeScreen", [screen], 1);
                            $scope.sysInfoModalView = sysInfoModalView;
                            $ionicLoading.hide();
                        })
                    }).fail(function (error) {
                    	multiModal.openModal($failRetriveCodeData,$scope);
                    	$ionicLoading.hide();
                    });
                }

                function changePageResults() {
                	var defer = $.Deferred();

                	$scope.currentResults = $codesModule.setCurrentResults($scope.config.pageNo, $scope.code.search);
                	defer.resolve();

                    return defer.promise();
                }

                function sortEntries(fieldName) {
                	$ionicLoading.show(loaderConfig);
                	setTimeout(function(){
                		$scope.currentResults = $codesModule.sortEntries(fieldName, $scope.config.pageNo, $scope.code.search);
                        $scope.config.pageNo = 1;
                        $scope.fieldName = fieldName;
                        $scope.inverseOrder = $codesModule.getSortingOrder();
                        $scope.$broadcast("paginationResultsNeedUpdating");
                        setTimeout(function(){
                        	$ionicLoading.hide();
                        });
                        // add arrow classes
//                        $scope.inverseOrder = ($scope.fieldName === fieldName) ? !$scope.inverseOrder : false;
//                        $scope.fieldName = fieldName;
//                        $scope.$broadcast("paginationResultsNeedUpdating");
                	}, 80)
                }

                function resetCodeSearch() {
                	resetCodeObj();
                    updateResults();
                }

                function resetCodeObj(){
                	$scope.code.shopDesc = "";
                	$scope.code.search = "";
                	$scope.config.pageNo = 1;
                    $scope.code.repairCode = '',
                    $scope.code.shopCode = ''
                }

                function resetData(){
                	$codesModule.resetCodeData();
                	resetCodeSearch();
                }

                function updateResults() {
                	var defer = $.Deferred();

                	console.log("updateResults");

                	$ionicLoading.show(loaderConfig);

                	setTimeout(function(){
                		$scope.config.pageNo = 1;
                        $scope.currentResults = $codesModule.setCurrentResults(1, $scope.code.search);
                    	$scope.config.arrayLength = $codesModule.getDataArrayLength();

                    	$scope.$broadcast("paginationResultsNeedUpdating");

                        defer.resolve();

                        console.log("updateResults done");
                	}, 80)

                    return defer.promise();
                }


                function goToPageDetails(code,pageName){

                	$scope.updateBreadcrums(pageName, "goBackToPreviousScreen", [pageName,$scope.sysInfoModalView], 1); //update previous breadcrumb
                	$scope.updateBreadcrums("Details", "goToPageDetails", [code,pageName], 2); //set current one

                	$scope.sysInfoModalView = 'templates/manuals/Codes/rowDetails.html';
                	$scope.detailedCode = code;
                }

                function goBackToPreviousScreen(pageName,template){
                	$scope.updateBreadcrums(pageName, "goToCodeScreen", [pageName], 1);
                	$scope.sysInfoModalView = template;
                	$scope.updateResults();
//                	$scope.$apply();
                }

                function goToShopDetails(code){

                	$scope.updateBreadcrums('Shop Profile', "goBackToShopProfile", [], 1); //update previous breadcrumb
                	$scope.updateBreadcrums(code.code + " - " + code.name, "goToShopDetails", [code], 2); //set current one

                	$scope.sysInfoModalView = 'templates/manuals/Codes/shopProfileDetails.html';
                	$scope.detailedCode = code;
                }

                function goBackToShopProfile(){
                	$scope.updateBreadcrums("Shop Profile", "goToCodeScreen", ["Shop Profile"], 1);
                	$scope.sysInfoModalView = 'templates/manuals/Codes/shopProfile.html';
                }

                function resetDropDowns(){
                	$scope.dropdownInfo = {
                    	showDropManual : false,
                    	showDropMode : false,
                    	showDropPartsGroup : false,
                    	showDropManufacturerCode : false
                    }
                }

                function selectDropdown(name) {
                	resetDropDowns();
                	 $scope.scrollToTop();
                	$scope.dropdownInfo[name] = true;
                }

                function searchByShopCode(){
                    blurAllInputs();
                	$ionicLoading.show(loaderConfig);
                	$codesModule.getShopDataByShopCode($scope.code.search).then(function(shopInfo){
                		updateResults().then(function(){
                			if(shopInfo.length > 0){
                    			$scope.code.shopDesc = $scope.currentResults[0].name;
                    		}else{
                    			$scope.code.shopDesc = "";
                    			var $noRecordsFound = multiModal.setBasicOptions('notification','noRecordsFound', 'No records found for the given Shop Code '+$scope.code.search);
                    			multiModal.openModal($noRecordsFound, $scope);
                    		}
                    		$ionicLoading.hide();
                		})
                	}).fail(function(e){
                		$ionicLoading.hide();
                		console.log("Error in searchByShopCode: " + e);
                	})
                }

                function showAdditionalMessage(screen){
                	var messageScreens = ["Repair STS Code", "Master Parts", "Associate Parts with Code/Mode", "Shop Limits", "Shop Contract", "Labor Rates"];
                	for(var i=0;i<messageScreens.length;i++){
                		if(screen == messageScreens[i]){
                			return true;
                		}
                	}

                	return false;
                }

                function selectManualCode(code) {
                	$scope.dropdownInfo.showDropManual = false;
                    if ($scope.preFilterSettings.manualCode == code) return;

                    $scope.preFilterSettings.manualCode = code;
                    $codesModule.setManualCode(code);
                    updateResults();
                    // search and update results
                }

                function selectModeCode(code) {
                	$scope.dropdownInfo.showDropMode = false;
                    if ($scope.preFilterSettings.mode == code) return;

                    $scope.preFilterSettings.mode = code;
                    $codesModule.setModeCode(code);
                    updateResults();
                }

                function selectPartsGroupCode(code) {
                	$scope.dropdownInfo.showDropPartsGroup = false;
                    if ($scope.preFilterSettings.partGroup == code) return;

                    $scope.preFilterSettings.partGroup = code;
                    $codesModule.setPartsGroupCode(code);
                    updateResults();
                }

                function selectManufacturerCode(code) {
                	$scope.dropdownInfo.showDropManufacturerCode = false;
                    if ($scope.preFilterSettings.manufacturer == code) return;

                    $scope.preFilterSettings.manufacturer = code;
                    $codesModule.setManufacturerCode(code);
                    updateResults();
                }

                function validateAndSetShopCode(shopCode) {
                    //Close the keyboard
                    document.activeElement.blur();
                	if (shopCode && shopCode.length == 3) {
                		$ionicLoading.show(loaderConfig);
                		$localDB.find("repairShop", {code : shopCode}, {exact : true}).then(function(data){
                    		if (data && data.length == 1) {
                    			var selectedShop = data[0].json;
                    			$ionicLoading.hide();
                    			$scope.code.shopSet = true;
                    			$scope.code.shopCode = $scope.code.shopCode + " " + selectedShop.name;
                    			ToastService.showNotificationMsg('Shop selected.');
                    		} else {
                    			$ionicLoading.hide();
                    			ToastService.showNotificationMsg('Shop not found.');
                    		}
                    	}).fail(function(error){
                    		$ionicLoading.hide();
                    	});
                	} else {
                		ToastService.showNotificationMsg('You need to type a 3 letter shop code to set the shop.');
                	}
                  
                }

                function searchForContracts(shopCode, repairCode, shopSet) {
                    //Close the keyboard
                    document.activeElement.blur();
                  if (shopCode.length === 0 || !shopSet) {
                    ToastService.showNotificationMsg('Please search for a shop code first');
                  } else if (repairCode && repairCode.length >=3 && repairCode.length <=4) {
                		$ionicLoading.show(loaderConfig);
                		// check for internet connection
                		auditService.testSignal().then(function(){
                			WOCodesService.getContractsSearch(shopCode.substring(0, 3), repairCode).then(function(results){
                				$ionicLoading.hide();
                				$codesModule.buildCodeObject(results);
                				updateResults();
                    		}).fail(function(){
                    			// could not retrieve data from server
                    			ToastService.showNotificationMsg('Could not retrieve data from server.');
                    			$ionicLoading.hide();
                    		});
                		}).fail(function(error){
                			$ionicLoading.hide();
                			ToastService.showNotificationMsg('You need internet connection for this functionality.');
                		});
                	} else {
                		ToastService.showNotificationMsg('You need to search for 3 to 4 letters.');
                	}
                }

                function resetShopSearch() {
                	$scope.code.shopCode = "";
                	$scope.code.shopSet = false;
                	$codesModule.resetCodeData();
                	document.getElementById("inputShop").blur(); 
                	updateResults();
                }       

                function resetRepairSearch() {
                	$scope.code.repairCode = "";
                	$codesModule.resetCodeData();
                	document.getElementById("inputRepair").blur(); 
                	updateResults();
                }

                function blurAllInputs() {
                    //Close the keyboard
                    document.activeElement.blur();
                }

            }])
