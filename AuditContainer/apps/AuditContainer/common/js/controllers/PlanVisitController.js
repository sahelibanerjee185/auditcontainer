/**
 * PlanVisitCtrl Controller
 * @namespace Controllers
 */

(function(){
	'use strict';
	
	audit.controller('PlanVisitCtrl', PlanVisitCtrl);
	
	PlanVisitCtrl.$inject = ['$scope', '$planVisit', 'multiModal', '$ionicLoading', '$location', '$depot', 
		'$routeParams', '$filter', '$surveyModule', '$localDB', 'repairShopService', 'containerHistoryService',
		'auditService', '$timeout', '$rootScope', '$ionicScrollDelegate', 'ocrService'];
	
	/**
	 * @namespace PlanVisitCtrl
	 * @desc Controller used for inspections screen, provides overview of visit activity and working with a started visit
	 * @memberOf Controllers
	 */
	function PlanVisitCtrl($scope, $planVisit, multiModal, $ionicLoading, $location, $depot,
	$routeParams, $filter, $surveyModule, $localDB, repairShopService, containerHistoryService,
	auditService, $timeout, $rootScope, $ionicScrollDelegate, ocrService) {
		
		$timeout(function(){
			$ionicLoading.hide();	
		}, 500);
		
		$scope.uniqueFilterActivated = false; 
	 	$scope.countryToggle = false;
		$scope.messages = [];
		$scope.goSearch = goSearch;
		$scope.countrySearch = '';
		$scope.currentCountry = {};
		$scope.selectedCountry = {};
		addMessages($scope.messages, ['searchDepots']);
		$scope.filterHelp = {};
		$scope.filteredLength = filteredLength;
		$scope.finishScroll = finishScroll;
		$scope.dropdownVisible = true;
		$scope.resetSearch2  = resetSearch2;
		$scope.reverse = false;
		$scope.scrollTop = scrollTop;
		$scope.selectCountry= selectCountry;
		$scope.selectRepairShop = selectRepairShop;
		$scope.ShopsOrderFunction = ShopsOrderFunction;
		$scope.show = {};
		$scope.show.itemSelected = false;
		$scope.show.itemSelected = false;
		$scope.startScroll = startScroll;
		$scope.sortRepairShops = sortRepairShops;
		$scope.rememberMe = rememberMe;
		$scope.toggleDropdown = toggleDropdown;
		$scope.obj = $surveyModule.getObj();
		$scope.planVisitObj = $planVisit.getObj();
		$scope.allRepairShops = $planVisit.getObj().repairShops;
		$scope.allCountries = $planVisit.getObj().countries;
		$scope.maxEnableDate = new Date();
		$scope.datePicker = {};	
		$scope.globalResults = [];
		$scope.countScrolling = 0;
			
		function startScroll(){
			$scope.countScrolling++;
        }

        function finishScroll(){
    		$scope.countScrolling=0;
        }

        function toggleDropdown(){
        	$scope.dropdownVisible = $scope.dropdownVisible === false ? true: false;
        	return $scope.dropdownVisible;
        };
        
		/**
	     * @name sortRepairShops
	     * @desc Sets the sorting options for plan visit screen
		 * and broadcasts the sorting event
		 * which is handled in the databaseSearch directive
	     * @param {String} column - column to sort by
	     * @returns N/A - has side effects
	     * @memberOf Controllers.PlanVisitCtrl
	     */
        function sortRepairShops(column){
        	$scope.countScrolling = 0;
        	$scope.showLoader = true;
        	if (!$scope.sortOptions) 
        		$scope.sortOptions = {};
        	if (column == $scope.sortOptions.column) {
        		$scope.sortOptions.ascending = !$scope.sortOptions.ascending;
        	} else {
        		$scope.sortOptions = {column : column, ascending: true}
        	}
        	$rootScope.$broadcast("sortResultsForPlanVisit", {});
    	}
        
		/**
	     * @name selectCountry
	     * @desc Selects the country to filter shops by
		 * firing the selectCountryForPlanVisit event
		 * which is handled by the databaseSearch directive
		 * remembers the selected country if remember me is enabled
		 * @param {String} country
	     * @returns N/A - has side effects
	     * @memberOf Controllers.PlanVisitCtrl
	     */
		 function selectCountry(country) {
			$scope.countScrolling = 0;
			$scope.showLoader = true;
			$scope.currentCountry = country;
			$scope.selectedCountry.name = $scope.currentCountry.name;
			$scope.selectedCountry.code = $scope.currentCountry.code;
			$rootScope.$broadcast("selectCountryForPlanVisit", {country: country});
			
			if ($scope.rememberCountry) {
				var countryObject = {
					rememberCountry : $scope.selectedCountry.name,
					code : $scope.selectedCountry.code
				}
					
				$planVisit.saveRememberCountry(countryObject).then(function(){
					$planVisit.getRememberCountry();
				});
			}
		}
		 
		function goSearch() {
			 $scope.countScrolling=0;
			 if (document.activeElement) document.activeElement.blur();
		 }
		
		/**
	     * @name checkForSavedCountry
	     * @desc Set selected country to remembered one
		 * or the default state when
		 * the controller is initialized
		 * @returns N/A - has side effects
	     * @memberOf Controllers.PlanVisitCtrl
	     */
		function checkForSavedCountry(){
			$scope.rememberCountry = $scope.planVisitObj.country.rememberme;
			
			if ($scope.rememberCountry) {
				$scope.selectedCountry = {
					name : $scope.planVisitObj.country.rememberCountry,
					code : $scope.planVisitObj.country.code
				}
				
				$scope.currentCountry = $scope.selectedCountry;
				
				if ($scope.selectedCountry.name != "All Countries") {
					$scope.showLoader = true;
					setTimeout(function(){
						$rootScope.$broadcast("selectCountryForPlanVisit", {country: $scope.selectedCountry});
					}, 100)
				}
			} else {
				$scope.selectedCountry.name = "All Countries";
				$scope.currentCountry = $scope.selectedCountry;
			}
		}
		
		checkForSavedCountry();
		
		var $noWorkOrders = multiModal.setBasicOptions('notification', 'noWorkOrders', 'There are no work orders available for this repair shop.');
		var $noCachedWorkOrders = multiModal.setBasicOptions('notification', 'noWorkOrders', 'You have no cached information for this repair shop.');
		
		/**
	     * @name selectRepairShop
	     * @desc Retrieves shop work orders from the server if online
		 * else looks in json store for any cached data
		 * directs to the add visit screen
		 * @param {Object} $event - click event passed from html
		 * @param {String} repairShopNo
	     * @memberOf Controllers.PlanVisitCtrl
	     */
		function selectRepairShop($event, repairShopNo){
			$event.stopPropagation();
			
			ocrService.clearVisitDetails();
			$scope.show.itemSelected=true;
			var selectedDate = $scope.datePicker.date.format("YYYY-MM-DD");
			$scope.selectedRepairShop = repairShopNo;
			if (window.Connection) {
	             if (navigator.connection.type == Connection.NONE) {
					$ionicLoading.show(loaderConfig);
					
					$planVisit.getCollectionForRepairShop(repairShopNo).then(function(collectionName){
						$planVisit.getCachedCollection(collectionName).then(function(response){
							$planVisit.setWorkOrdersForRepairShop(response).then(function(){
								$ionicLoading.hide();
								goToAddVisit(repairShopNo, selectedDate);
							});
						});
						
					}).fail(function(){
						$ionicLoading.hide();
						multiModal.openModal($noCachedWorkOrders, $scope);
					});
				} else {
					$ionicLoading.show(serverLoaderConfig);
					
					// test Internet signal
					auditService.testSignal()
					// if there is an internet connection with signal
					.then(function(success){
						repairShopService.getWorkOrdersByShopId(repairShopNo).then(function(response){
							$planVisit.setWorkOrdersForRepairShop(response.results).then(function(workOrders){
								$ionicLoading.hide();
								if ($location.path().indexOf("planVisitTab") != -1){
									if (workOrders.length == 0) {
										multiModal.openModal($noWorkOrders, $scope);
										return;
									}
									
									if (response.countExceeded) {
										var $largeDataNotice =  multiModal.setBasicOptions('notification', 'largeDataNotice', 'The selected repair shop has a high data volume. Work orders starting from ' + response.startingDate + ' will be listed.');
										$largeDataNotice = multiModal.setTemplateFile($largeDataNotice, 'templates/common/largeDataNoticeModal.html');
										$largeDataNotice = multiModal.setActions($largeDataNotice, function(){
											goToAddVisit(repairShopNo, selectedDate);
										});
										
										multiModal.openModal($largeDataNotice, $scope);
										return;
									}
									
									goToAddVisit(repairShopNo, selectedDate);
								}
							});
						})
					})
					// if there is an internet connection but no signal
					.fail(function(error){
						
						$planVisit.getCollectionForRepairShop(repairShopNo).then(function(collectionName){
							$planVisit.getCachedCollection(collectionName).then(function(response){
								$planVisit.setWorkOrdersForRepairShop(response).then(function(){
									$ionicLoading.hide();
									goToAddVisit(repairShopNo, selectedDate);
								});
							});
							
						}).fail(function(){
							$ionicLoading.hide();
							multiModal.openModal($noCachedWorkOrders, $scope);
						});
					});
					
				}
			}
			$timeout(function(){
				$scope.show.itemSelected=false;
			}, 300);
		}

		function goToAddVisit(repairShopNo, selectedDate) {
			$planVisit.setShopCode(repairShopNo);
			
			$scope.$evalAsync(function(){
				$location.path("/addVisitTab/" + selectedDate);
			});
		}
		
		if ($routeParams.date !== undefined) {
			$scope.datePicker.date = new Date($routeParams.date.replace(Messages.datePattern, '$3-$2-$1'));
		} else {
			$scope.datePicker.date = new Date();
		}
		
		function filteredLength(){
			if ($scope.filterHelp.filteredShops.length != 0){
				return false;
			} else {
				return true;
			}
		}

		$scope.isAll = null;
		
		/**
	     * @name rememberMe
	     * @desc Clears remember country data
		 * and disables functionality
		 * or saves it in and enables it
		 * info is in 'country' collection
		 * @param {N/A}
	     * @memberOf Controllers.PlanVisitCtrl
	     */
		function rememberMe(){
			if ($scope.rememberCountry) {
				$scope.$evalAsync(function(){
					$scope.rememberCountry = false;
				});
				$planVisit.clearRememberCountry().then(function(){
					$planVisit.getRememberCountry();
				});
			} else {
				$scope.$evalAsync(function(){
					$scope.rememberCountry = true;
				});
				var countryObject = {
					rememberCountry : $scope.selectedCountry.name,
					code : $scope.selectedCountry.code 
				}
				
				$planVisit.saveRememberCountry(countryObject).then(function(){
					$planVisit.getRememberCountry();
				});
			}
		}
		
	   	$scope.dropdownVisible = true;
		$scope.toggleDropDown = false;
		$scope.countriesArrayLength=null;
		$scope.getCountriesArrayLength=function(){
			$scope.countriesArrayLength=$scope.allCountries.length;
		};
		
        $scope.saveCountry = function( countryObject){
        	$localDB.replace('country',countryObject,{}).then(function(){console.log("replace succesfull")});
        }
        
        $scope.showDiv = false;
        $scope.seeOptions = function(){
        	showDiv = true;
        }
        
        $scope.selectedContainerNo = "ContainerNo"
          
		$scope.$on('$destroy', function() {
			$ionicLoading.hide();
			multiModal.removeAllModals();
		})
		// function for order
		$scope.defaultOrderTag = 'containerNo';
    	$scope.switchOrder = true;   // ascending or descending

    	function ShopsOrderFunction(defaultOrderTag) {
    	    $scope.switchOrder = ($scope.defaultOrderTag === defaultOrderTag) ? !$scope.switchOrder : false;
    	    $scope.defaultOrderTag = defaultOrderTag;
    	}
    	
    	var queryPart;
		var searchString = $scope.countrySearch;
		
		var options = {
			filter: ['code', 'name', 'location', 'country', 'cached']
		};
		
		//scroll on top when typing in search input
		$scope.$watch('shopSearch',function(){
			$ionicScrollDelegate.scrollTop();
		});
		
    	/**
	     * @name resetSearch2
	     * @desc Resets shop search and reloads list data
		 * @param {N/A}
		 * @param {String} searchString - will be passed as undefined from html
	     * @memberOf Controllers.PlanVisitCtrl
	     */
    	function resetSearch2(searchString){
    		$scope.countScrolling=0;
    		var countryCode = $scope.selectedCountry.code;
			$scope.shopSearch='';
			
			console.log(countryCode);
			var queryShopCode = WL.JSONStore.QueryPart()
			.leftLike('code', searchString);
		
			var queryShopName = WL.JSONStore.QueryPart()
			.like('name', searchString);
		
			var queryLocation = WL.JSONStore.QueryPart()
			.like('location', searchString);
			if (typeof countryCode === 'string') {
				queryShopCode.equal('country', countryCode);
				queryShopName.equal('country', countryCode);
				queryLocation.equal('country', countryCode);
			}
			
			if ($scope.sortOptions && $scope.sortOptions.column) {
				var sortingObject = {};
				var columnName = $scope.sortOptions.column;
				sortingObject[columnName] = $scope.sortOptions.ascending ? 'ASC' : 'DESC';
				options.sort = [sortingObject];
			}
			
			var queryPartsArray = [queryShopCode, queryShopName, queryLocation];
			WL.JSONStore.get('repairShop').advancedFind(queryPartsArray, options).then(function(results){
				$rootScope.$evalAsync(function(){
					$scope.allRepairShops = results;
					$scope.countScrolling=0;
					obj.scrollDownOffsetActiveSearch=results.length;
				})
			});	
		}
    		
    	function scrollTop(){
    		$scope.countScrolling = 0;
    		$ionicScrollDelegate.scrollTop();
    	}
	}
})();