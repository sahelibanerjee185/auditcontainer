/**
 * AddVisit Controller
 * @namespace Controllers
 */
audit.controller("AddVisitCtrl", AddVisitCtrl);

AddVisitCtrl.$inject = ['$scope', '$planVisit', 'multiModal', '$ionicLoading', '$location',
    '$depot', '$routeParams', '$filter', '$timeout', '$ionicScrollDelegate',
    'containerHistoryService', 'auditService', 'repairShopService', '$surveyModule', 'startedAudit',
    '$rootScope', 'PendingAuditsService', 'ocrService', 'EmailNotificationService', 'ToastService', '$route'
];


/**
 * @namespace AddVisitCtrl
 * @desc Controller used manage visits booking
 * @memberOf Controllers
 */
function AddVisitCtrl($scope, $planVisit, multiModal, $ionicLoading, $location, $depot, $routeParams, $filter,
    $timeout, $ionicScrollDelegate, containerHistoryService, auditService, repairShopService, $surveyModule,
    startedAudit, $rootScope, PendingAuditsService, ocrService, EmailNotificationService, ToastService, $route) {

    $scope.obj = $planVisit.getObj();
    $scope.ModalFilterHelp = {};
    $scope.containersForNewVisit = [];
    var selectedDate;
    var currentVisitId;
    $scope.ModalFilterHelp.filteredData = $scope.obj.containersForRepairShop;
    $scope.copyOfArray = $scope.obj.containersForRepairShop;
    $scope.filterParam = "showAll";
    $scope.visit = {};
    $scope.visit.description = null;
    $scope.obj.addVisitText = "Book Visit";
    $scope.backToStartedTab = false;

    //Add container in visit from OCR
    var visitDetailsOCR = ocrService.getVisitDetails();

    if ($routeParams.startedTab) {
        $scope.backToStartedTab = true;
    }

    if ($routeParams.date !== undefined) {
        selectedDate = $routeParams.date;
        $scope.selectedDate = selectedDate;
    } else if (visitDetailsOCR.date !== null) {
        //Add container in visit from OCR
        selectedDate = visitDetailsOCR.date;
        $scope.selectedDate = selectedDate;
    }


	/* If we have a visitId as route parameter, it means that we add containers 
	to an existing visit if not, it means we need to create a new visit */
    if ($routeParams.visitId !== undefined) {
        currentVisitId = $routeParams.visitId;
        $scope.obj.addVisitText = "Add";
    } else if (visitDetailsOCR.visitId !== null) {
        //Add container in visit from OCR
        currentVisitId = visitDetailsOCR.visitId;
        $scope.obj.addVisitText = "Add";
    } else {
        $scope.obj.addVisitText = "Book Visit";
    }


    //OCR Functionality
    $scope.ocrService = ocrService;
    $scope.openOCR = openOCR;
    $scope.capitalizeLetters = capitalizeLetters;
    $scope.validateOCR = validateOCR;
    $scope.workOrderPreview = workOrderPreview;

	/**
	 * @name capitalizeLetters
	 * @desc Capitalize a given letter
	 * @param {String} item a character
	 * @param {Integer} index index of the character in letters array
	 * @returns {Void} 
	 * @memberOf Controllers.AddVisitController
	 */
    function capitalizeLetters(item, index) {
        $scope.ocrData.letters[index] = item[item.length - 1].toUpperCase();
    };

	/**
	 * @name validateOCR
	 * @desc Check the validity of the ContainerID
	 * @param {Object} ocrData object containing details about OCR recognition
	 * @returns {Void} 
	 * @memberOf Controllers.AddVisitController
	 */
    function validateOCR(ocrData) {
        var containerID = ocrData.letters.join("") + ocrData.numbers.join("");
        $scope.errors = ocrService.validateOcrText(containerID);
    };


	/**
	 * @name openOCR
	 * @desc Open OCR camera to recognize a ContainerID
	 * @returns {Void} 
	 * @memberOf Controllers.AddVisitController
	 */
    function openOCR() {

        // We lose hybrid control, show loader and hide after we gain it again
        $ionicLoading.show(loaderConfig);

        var path = ocrService.buildPhotoPath();

        /* This will trigger native OCR using hybrid.openOCR function */
        hybrid.openOCR(path).then(function (data) {

            $ionicLoading.hide(); //Hide Loader

            var containerID = data.text;
            var path = data.fileName;

            //Parse OCR data
            ocrService.parseOCRData(containerID, path).then(function (ocrData) {

                $scope.ocrData = ocrData;

                $scope.partialOCRModalView = "templates/OCR/containerIdEditPartial.html";
                $scope.validateOCR($scope.ocrData); //Validate containerID received from Native OCR

                //Open ContainerID Editing Modal
                ocrService.openOCRMainModal($scope);
            });

        }, function (error) {
            //Display Toast that OCR failed "Please try again"
            $ionicLoading.hide();
            if (error.status == 102) {
                ToastService.showNotificationMsg('OCR Failed to recognize container ID. Please try again!');
            }
        });
    };

	/**
	 * @name workOrderPreview
	 * @desc Details preview for the WO identified by OCR
	 * @returns {Void} 
	 * @memberOf Controllers.AddVisitController
	 */
    function workOrderPreview() {
        auditService.testSignal().then(function () {
            ocrService.removeFileByName($scope.ocrData.imageName); //remove image from temporary folder
            //Build ContainerID
            $scope.ocrData.containerID = $scope.ocrData.letters.join("") + $scope.ocrData.numbers.join("");


            //GET repairs and parts for latest WO available for the provided ContainerID
            var shopCode = $planVisit.getShopCode();
            findContainerByID($scope.ocrData.containerID, shopCode).then(function (workOrderId) {
                $scope.ocrData.modalTitle = "Work Order Preview";
                $scope.selectContainerForVisitOCRByWorkOrderId(workOrderId, true);
                $ionicLoading.hide();
            }).fail(function (err) {
                $ionicLoading.hide();
            });
        }).fail(function () {
            ToastService.showNotificationMsg('No internet Connection!');
        });
    };

	/**
	 * @name findContainerByID
	 * @desc Get latest WO data based on ContainerID and ShopId
	 * @param {Integer} containerID unique identified of a container
	 * @param {Integer} shopID unique identifier of a shop
	 * @returns {Void} 
	 * @memberOf Controllers.AddVisitController
	 */
    function findContainerByID(containerID, shopID) {
        var defer = $.Deferred();
        $ionicLoading.show(loaderConfig);
        ocrService.getLatestWORepairsByID(containerID, shopID).then(function (results) {
            var workOrderId = results[0].workOrderId;
            defer.resolve(workOrderId);
        }, function (err) {
            ToastService.showNotificationMsg('No Work Order is currently available for Container no: ' + containerID);
            defer.reject();
        });
        return defer.promise();
    };


    //Force losing focus on active inputs to hide the keyboard
    $scope.searchGo = function () {
        document.activeElement.blur();
    };

	/**
	 * @name toggleSelectContainer
	 * @desc Select a container in array
	 * @param {Integer} $index position in array
	 * @returns {Void} 
	 * @memberOf Controllers.AddVisitController
	 */
    $scope.toggleSelectContainer = function ($index) {
        if ($scope.ModalFilterHelp.filteredData[$index].selected)
            $scope.ModalFilterHelp.filteredData[$index].auditDate = "N/A";
        else
            $scope.ModalFilterHelp.filteredData[$index].auditDate = selectedDate;

        $scope.ModalFilterHelp.filteredData[$index].selected = !$scope.ModalFilterHelp.filteredData[$index].selected;
    };

    //Gets back to previous screen - planVisit or depotDetails
    $scope.cancelVisitAdd = function () {
        window.history.back();
    };

    $scope.refreshResults = refreshResults;
    $scope.cacheResults = cacheResults;

	/**
	 * @name refreshResults
	 * @desc Update the Work Orders for the current shop
	 * @returns {Void} 
	 * @memberOf Controllers.AddVisitController
	 */
    function refreshResults() {
        $ionicLoading.show(serverLoaderConfig);

        var shopCode = $planVisit.getShopCode();

        repairShopService.getWorkOrdersByShopId(shopCode).then(function (response) {
            $planVisit.setWorkOrdersForRepairShop(response.results).then(function (workOrders) {
                $rootScope.$broadcast("updatedWorkOrdersFromServer", {
                    results: workOrders
                });
                $ionicLoading.hide();
                multiModal.removeAllModals();
            });
        });
    };

	/**
	 * @name cacheResults
	 * @desc Stores locally set of data for a specific shop
	 * @param {Object} dataSource data to be cached
	 * @returns {Void} 
	 * @memberOf Controllers.AddVisitController
	 */
    function cacheResults(dataSource) {
        var shopCode = $planVisit.getShopCode();
        $ionicLoading.show(loaderConfig);
        $planVisit.cacheRepairShop(shopCode, dataSource).then(function () {
            $planVisit.getRepairShops().then(function () {
                $ionicLoading.hide();
            });
        }).fail(function () {
            $ionicLoading.hide();
        });
    };

	/**
	 * @name selectContainerForVisitOCRByWorkOrderId
	 * @desc Select a container to be added in a visit from OCR perspective
	 * @param {Integer} workOrderId unique identifier of a work order
	 * @param {Boolean} noConfirm 
	 * @returns {Void} 
	 * @memberOf Controllers.AddVisitController
	 */
    $scope.selectContainerForVisitOCRByWorkOrderId = function (workOrderId, noConfirm) {
        for (var i = 0; i < $scope.obj.workOrdersForRepairShop.length; i++) {
            var workOrder = $scope.obj.workOrdersForRepairShop[i];
            if (workOrder.workOrderId == workOrderId) {
                if (workOrder.locked) {
                    console.error('WO ' + workOrderId + ' cannot be added IS LOCKED');
                    ToastService.showNotificationMsg('Associated work order is locked and cannot be selected.');
                } else {
                    workOrder.selected = true;
                    if (!noConfirm) {
                        $scope.confirmVisitAdd();
                    } else {
                        // close modal
                        multiModal.removeAllModals();
                        ToastService.showNotificationMsg('Associated work order selected.');
                    }
                }
                return;
            }
        }

        ToastService.showNotificationMsg('Could not find the associated work order in the shop.');
    };

	/**
	 * @name confirmVisitAdd
	 * @desc Shows a modal to confirm adding one or more containers in a visit
	 * @returns {Void} 
	 * @memberOf Controllers.AddVisitController
	 */
    $scope.confirmVisitAdd = function () {
        $scope.containersForNewVisit = [];
        angular.forEach($scope.obj.workOrdersForRepairShop, function (container) {
            if (container.selected)
                $scope.containersForNewVisit.push(container);
        });
        if ($scope.containersForNewVisit.length == 0) {
            multiModal.openModal($selectOneContainer, $scope);
            return;
        }
        $scope.visit.description = null;
        multiModal.openModal($confirmVisitsOptions, $scope);
        $scope.takeAllFirstLetters();
    };

	/**
	 * @name resetScroll
	 * @desc Resets the scroll for dropdown
	 * @returns {Void} 
	 * @memberOf Controllers.AddVisitController
	 */
    $scope.resetScroll = function () {
        $timeout(function () {
            $scope.delegate = $ionicScrollDelegate.$getByHandle('dropDownScroll');
            $ionicScrollDelegate.resize();
            setTimeout(function () {
                $ionicScrollDelegate.scrollTop();
            }, 50);

        }, 10);
    };

	/**
	 * @name scrollTop
	 * @desc Scroll the page to top
	 * @returns {Void} 
	 * @memberOf Controllers.AddVisitController
	 */
    $scope.scrollTop = function () {
        var offSetTop = $ionicScrollDelegate.$getByHandle('upComingVisitsScroll').getScrollPosition().top;
        if (offSetTop > 20) {
            $ionicScrollDelegate.scrollTop();
        }
    };

    //----------------------------------History and Remarks SECTION -----------------------------------

    // Pagination config object
    $scope.config = {
        pagesLimit: 3,
        entriesPerPage: 7,
        arrayLength: 0,
        pageNo: 1
    };

    //History object to keep track of data, containerId, woId
    $scope.historyObject = {
        containerId: null,
        woId: null,
        workOrderArray: [],
        repairWorkOrders: [],
        partialHistoryRemarksArray: [],
        historyRemarksArray: [],
        currentView: null,
        currentTemplate: null
    };

    /**
     * @desc Used to show/hide remarks for a row, and recalculate scroll
     * @param item - a remark item
     */
    $scope.showHideRemarks = function (item) {
        if (item.displayRemarks) {
            item.displayRemarks = false;
            $ionicScrollDelegate.$getByHandle('mainScroll').resize();
        } else {
            //Close all other remarks 
            $scope.historyObject.partialHistoryRemarksArray.forEach(function (remark) {
                remark.displayRemarks = false;
            });
            // Expand current remark item
            item.displayRemarks = true;
            //If item is one of last 3 in partialHistoryRemarksArray and we expand it, scroll to bottom and update scroll
            var indexOfCurrentItemInArray = $scope.historyObject.partialHistoryRemarksArray.indexOf(item);
            //If we pass the half of the screen, just scroll to bottom to render all text
            if (indexOfCurrentItemInArray >= 3) {
                $ionicScrollDelegate.$getByHandle('mainScroll').scrollBottom();
            }
        }
    };

	/**
	 * @name showHistory
	 * @desc Open a modal to show the history of a container
	 * @param {String} containerNo the id of a container
	 * @returns {Void} 
	 * @memberOf Controllers.AddVisitController
	 */
    $scope.showHistory = function (containerNo) {
        //Build the promise
        var defer = $.Deferred();

        // User can navigate though History and Remarks until the modal is closed
        if (!!$scope.historyObject.workOrderArray && !!$scope.historyObject.workOrderArray.length) {
            //Update the modal variables
            $scope.historyObject.currentView = 'history';
            $scope.historyObject.currentTemplate = 'templates/common/modalHistory.html';
            defer.resolve();
        } else {
            containerHistoryService.getContainerHistory(containerNo).then(function (history) {
                $scope.historyObject.workOrderArray = containerHistoryService.getContainerHistoryArray();

                if ($scope.historyObject.workOrderArray.length < 1) {
                    defer.reject({ 'errorCode': 101, 'error': 'No containers' });
                    return;
                }

                $scope.historyObject.repairWorkOrders = containerHistoryService.getContainerHistoryRepair();
                //Update the modal variables
                $scope.historyObject.currentView = 'history';
                $scope.historyObject.currentTemplate = 'templates/common/modalHistory.html';
                // We have history, proceed to display the modal
                defer.resolve();

            }, function (err) {
                defer.reject({ 'errorCode': 102, 'error': err });
                console.log("AddVisitController[showHistory] error " + err);
                return;
            });
        }

        return defer.promise();
    };

    /**
     * Retrieve history remarks 
     */
    $scope.showHistoryRemarks = function (woId) {
        var defer = $.Deferred();

        if (!!$scope.historyObject.historyRemarksArray && !!$scope.historyObject.historyRemarksArray.length) {
            //Update the modal variables
            $scope.historyObject.currentView = 'remarks';
            $scope.historyObject.currentTemplate = 'templates/common/modalRemarks.html';
            //Create a sub-array for pagination starting with page 1 using config and historyObject
            $scope.historyObject.partialHistoryRemarksArray = $scope.historyObject.historyRemarksArray.slice(0, $scope.config.entriesPerPage);
            defer.resolve();
        } else {
            containerHistoryService.getWorkOrderRemarksHistoryArray(woId)
                .then(function (results) {
                    console.log(results);
                    //If there are no results, we can disiplay nothing
                    if (results.length < 1) {
                        defer.reject({ 'errorCode': 101, 'error': 'No remarks' })
                        return;
                    }

                    //Limit the number of comments to 21
                    results = results.slice(0, 21);
                    //Attach history remarks data on scope
                    $scope.historyObject.historyRemarksArray = results;
                    //Configure pagination directive
                    $scope.config.arrayLength = results.length;
                    //Create a sub-array for pagination starting with page 1 using config and historyObject
                    $scope.historyObject.partialHistoryRemarksArray = results.slice(0, $scope.config.entriesPerPage);
                    //Update the modal variables
                    $scope.historyObject.currentView = 'remarks';
                    $scope.historyObject.currentTemplate = 'templates/common/modalRemarks.html';
                    //We have all data, resolve the promise
                    defer.resolve();
                }, function (err) {
                    defer.reject({ 'errorCode': 102, 'error': err });
                    console.log(err);
                });
        }

        return defer.promise();
    };

    /**
     * Display history modal with History or Remarks
     */
    $scope.showVisitDetails = function (containerNo, woId, view) {
        //Set the local variables to handle switch between views
        $scope.historyObject.containerId = containerNo;
        $scope.historyObject.woId = woId;

        if (view === 'history') {
            $ionicLoading.show(serverLoaderConfig);
            //Load data for container's history
            $scope.showHistory(containerNo).then(function () {
                $ionicLoading.hide();
                multiModal.openModal($historyModal, $scope);
            }, function (err) {
                $ionicLoading.hide();
                if (err.errorCode == 101)
                    multiModal.openModal($noVisits, $scope);
            });

        } else if (view === 'remarks') {
            $ionicLoading.show(serverLoaderConfig);
            //Call function to retrieve remarks history for the provided WO id
            $scope.showHistoryRemarks(woId).then(function () {
                $ionicLoading.hide();
                multiModal.openModal($historyModal, $scope);
            }, function (err) {
                $ionicLoading.hide();
                if (err.errorCode == 101)
                    multiModal.openModal($noHistoryRemarks, $scope);
            })

        }
    };

	/**
	 * @name switchVisitDetailsHistoryRemarks
	 * @desc Used to switch between visitDetailsHistory tabs (history or remarks)
	 * @param {String} view - used to call specific function for each type of view
	 * @returns {Void} 
	 * @memberOf Controllers.AddVisitController
	 */
    $scope.switchVisitDetailsHistoryRemarks = function (view) {
        $ionicLoading.show(serverLoaderConfig);
        if (view == 'history') {
            $scope.showHistory($scope.historyObject.containerId).then(function () {
                $ionicLoading.hide();
            }, function (err) {
                console.log(err);
                $ionicLoading.hide();
                if (err.errorCode == 101)
                    multiModal.openModal($noVisits, $scope);
            })
        } else if (view == 'remarks') {
            $scope.showHistoryRemarks($scope.historyObject.woId).then(function () {
                $ionicLoading.hide();
            }, function (err) {
                $ionicLoading.hide();
                if (err.errorCode == 101)
                    multiModal.openModal($noHistoryRemarks, $scope);
            })
        }
    };

    /**
     * Reset history object when the history modal is closed 
     */
    $scope.resetHistoryData = function () {
        //Reset the history object to avoid displaying same data for different WOs/Containers
        $scope.historyObject = {
            containerId: null,
            woId: null,
            workOrderArray: [],
            repairWorkOrders: [],
            historyRemarksArray: [],
            currentView: null,
            currentTemplate: null
        }
    };

    /**
     * Called by pagination directive to update data to be displayed in the current page
     */
    $scope.changePageResults = function () {
        var defer = $.Deferred();
        console.log($scope.config);

        var offset = ($scope.config.pageNo - 1) * $scope.config.entriesPerPage;

        $scope.historyObject.partialHistoryRemarksArray = $scope.historyObject.historyRemarksArray.slice(offset, offset + $scope.config.entriesPerPage);

        defer.resolve();

        return defer.promise();
    }



    //----------------------------------END OF History and Remarks SECTION -----------------------------------

    //Resize the scroll section
    $scope.res = function () {
        $ionicScrollDelegate.resize();
    };

    //Notification modals
    var failMessage = multiModal.setBasicOptions('notification', 'failMessage', 'Could not connect to the server. Please try again later.')
    var $selectOneContainer = multiModal.setBasicOptions('notification', 'selectOneContainer', 'Please select at least one container.');
    var $noVisits = multiModal.setBasicOptions('notification', 'containerHistory', 'There are no prior visits to this container.', 'popup');
    var $noHistoryRemarks = multiModal.setBasicOptions('notification', 'containerHistoryRemarks', 'There are no remarks for this container.', 'popup');
    var $visitAdded;

    //Accept a line setting the $accepted property to true
    $scope.acceptLine = function (description) {
        description.$accepted = true;
    }

    //If currentVisit id exists, we have an In Progress visit and we add containers into it
    if (currentVisitId) {
        $visitAdded = multiModal.setBasicOptions('notification', 'visitAdded', 'The selected containers have been added to your visit.');
        $visitAdded = multiModal.setActions($visitAdded, function () {
            $ionicLoading.show(loaderConfig);
            $depot.getVisitInfo(currentVisitId).then(function () {
                $ionicLoading.hide();
                if (visitDetailsOCR.visitId) {
                    multiModal.removeAllModals();
                    setTimeout(function () {
                        $route.reload();
                    }, 0);
                }
                ocrService.clearVisitDetails(); //Clear visit details to avoid logic gaps
                startedAudit.checkForStartedVisit().then(function () {
                    $scope.$evalAsync(function () {
                        if ($scope.backToStartedTab) {
                            hybrid.navigateHybridMenu('surveyTab');
                        } else {
                            $location.path('/depotDetails/');
                        }
                    });
                });
            });
        });
    } else { //else we create a new visit using selected containers
        $visitAdded = multiModal.setBasicOptions('notification', 'visitAdded', 'Your visit has been planned.');
        $visitAdded = multiModal.setActions($visitAdded, function () {
            var containerList = getContainerList();
            EmailNotificationService.showModal('planVisit', $scope, $scope.obj.shopCode, containerList).then(function () {
                EmailNotificationService.setCallback(finishSchedule);
            });
        });
    }

    var $visitAddedWithExceptions;

    //Set modal views if the visit is added with exceptions
    if (currentVisitId) {
        $visitAddedWithExceptions = multiModal.setBasicOptions('notification', 'visitAdded', 'Some of the work orders selected have already been booked. The available work orders have been added to the visit.');
        $visitAddedWithExceptions = multiModal.setActions($visitAddedWithExceptions, function () {
            $ionicLoading.show(loaderConfig);
            $depot.getVisitInfo(currentVisitId).then(function () {
                startedAudit.checkForStartedVisit().then(function () {
                    $scope.$evalAsync(function () {
                        $ionicLoading.hide();
                        if ($scope.backToStartedTab) {
                            hybrid.navigateHybridMenu('surveyTab');
                        } else {
                            $location.path('/depotDetails/');
                        }
                    });
                });
            });
        });
    } else {
        $visitAddedWithExceptions = multiModal.setBasicOptions('notification', 'visitAdded', 'Some of the work orders selected have already been booked. Your visit has been planned for the available work orders.');
        $visitAddedWithExceptions = multiModal.setActions($visitAddedWithExceptions, function () {
            $ionicLoading.show(loaderConfig);
            startedAudit.checkForStartedVisit().then(function () {
                $ionicLoading.hide();
                $location.switchTab('surveyTab');
            });
        });
    }

    var $allBooked = multiModal.setBasicOptions('notification', 'allBooked', 'The selected work orders have already been booked. The work order list will be refreshed.');
    $allBooked = multiModal.setActions($allBooked, function () {
        refreshResults();
    });

	/**
	 * @name setAllBookedRefreshAction
	 * @desc Refresh list if all WO are already booked
	 * @param {Object} modalConfig modal configuration object
	 * @returns {Void} 
	 * @memberOf Controllers.AddVisitController
	 */
    function setAllBookedRefreshAction(modalConfig) {
        return multiModal.setActions(modalConfig, function () {
            refreshResults();
        });
    }

	/**
	 * @name getContainerList
	 * @desc Get a list of containers for visit
	 * @returns {String} list of containers comma separated
	 * @memberOf Controllers.AddVisitController
	 */
    function getContainerList() {
        var containerList = "";
        var arrayHelper = [];
        for (var i = 0; i < $scope.containersForNewVisit.length; i++) {
            var workOrderInfo = $scope.containersForNewVisit[i];
            arrayHelper.push(workOrderInfo.containerNo);
        }
        containerList = arrayHelper.join(", ");
        return containerList;
    };

    //Set up options for review and confirmation modal for planned visit
    var $confirmVisitsOptions = multiModal.setBasicOptions('confirm', 'planVisitModal', 'notification', ' popup', false);

    if (currentVisitId) {
        //Set up modal without description
        $confirmVisitsOptions = multiModal.setTemplateFile($confirmVisitsOptions, 'templates/planVisit/addVisitModal2.html');
    } else {
        $confirmVisitsOptions = multiModal.setTemplateFile($confirmVisitsOptions, 'templates/planVisit/addVisitModal.html');
    }

    if (currentVisitId) {
        //here we add containers to an existing visit
        $confirmVisitsOptions = multiModal.setActions($confirmVisitsOptions, function () {
            // we need visit Id, description and date. get them from json store for now. use markWorkOrders function

            var options = {
                filter: ['auditDate', 'description', 'shopCode', 'visitId', 'auditStatus', 'statusCode'],
                limit: 1
            };

            PendingAuditsService.runVisitQueue().then(function () {
                $ionicLoading.show(loaderConfig);
                auditService.updateDeletedWorkOrders().then(function () {
                    WL.JSONStore.get('visit').find({
                        visitId: currentVisitId
                    }, options)
                        .then(function (visitInfo) {
                            var visitInfo = visitInfo[0];
                            var visitStatus = visitInfo.auditstatus;

                            auditService.getWorkOrderPartsForWOArrray($scope.containersForNewVisit).then(function (woLines) {
                                auditService.addWorkOrdersToVisit(visitInfo.visitid, visitInfo.auditdate, visitInfo.shopcode, visitInfo.description, visitStatus, $scope.containersForNewVisit).then(function (response) {
                                	var lockedWorkOrders = response.lockedWorkOrders;
                                    var plannedWorkOrders = getPlannedContainers($scope.containersForNewVisit, lockedWorkOrders);
                                    var jsonStoreVisits = response.plannedWorkOrders;

                                    auditService.saveWorkOrderLines(woLines, plannedWorkOrders, visitInfo.visitid);
                                    
                                    var startedDefer = $.Deferred();

                                    if (visitStatus === 'Started') {
                                        var newStartedWorkOrders = [];

                                        angular.forEach(plannedWorkOrders, function (workOrder) {
                                            console.log("WO:");
                                            console.log(workOrder.workOrderId);
                                            newStartedWorkOrders.push({
                                                visitId: visitInfo.visitid,
                                                workOrderId: workOrder.workOrderId,
                                                inspectionStatus: 'Planned',
                                                shopCode: workOrder.shopCode,
                                                visitDesc: visitInfo.description,
                                                containerNo: workOrder.containerNo,
                                                auditResult: "",
                                                mode: workOrder.repairMode,
                                                repairCost: workOrder.repairCost,
                                                repairDate: workOrder.repairDate,
                                                woStatus: workOrder.status,
                                                statusCode: workOrder.statusCode
                                            })
                                        });
                                        
                                        startedAudit.addStartedWorkOrders(newStartedWorkOrders).then(function () {
                                            $surveyModule.getStartedVisitInfo().then(function (notification) {
                                                startedDefer.resolve();
                                            }).fail(function(err) {
                                            	startedDefer.resolve();
                                            })
                                        });
                                    } else {
                                    	startedDefer.resolve();
                                    }
                                    
                                    startedDefer.promise()
                                    .then(function(){
                                    	$surveyModule.refreshVisitData().then(function () {
                                            $ionicLoading.hide();
                                            feedbackPlanVisit(lockedWorkOrders, $scope.containersForNewVisit, visitInfo.shopcode);
                                        }).fail(function () {
                                            // add to visit collection and refresh offline
                                            auditService.addWorkOrdersToVisitCollection(jsonStoreVisits).then(function () {
                                                $surveyModule.refreshVisitDataOffline().then(function () {
                                                    $ionicLoading.hide();
                                                    feedbackPlanVisit(lockedWorkOrders, $scope.containersForNewVisit, visitInfo.shopcode);
                                                }).fail(function () {
                                                    $ionicLoading.hide();
                                                    feedbackPlanVisit(lockedWorkOrders, $scope.containersForNewVisit, visitInfo.shopcode);
                                                });
                                            });
                                        });
                                    });
                                });
                            }).fail(function () {
                                multiModal.openModal(failMessage, $scope);
                                $ionicLoading.hide();
                            });
                        }).fail(function () {
                            multiModal.openModal(failMessage, $scope);
                            $ionicLoading.hide();
                        });
                }).fail(function (error) {
                    WL.Logger.error("[AddVisitController][AddContainersToVisit][updateDeletedWorkOrders] username: " + userContext.getUsername() + " " + JSON.stringify(error));
                    multiModal.openModal(failMessage, $scope);
                    $ionicLoading.hide();
                });
            }).fail(function () {
                multiModal.openModal(failMessage, $scope);
                $ionicLoading.hide();
            });
        }, function () {
            $scope.containersForNewVisit = [];
        });
    } else {
        // Here we create a new visit
        $confirmVisitsOptions = multiModal.setActions($confirmVisitsOptions, function () {

            var currentShop = repairShopService.getCurrentShop();
            var description = ($scope.visit.description === null) ? "" : $scope.visit.description;


            PendingAuditsService.runVisitQueue().then(function () {
                $ionicLoading.show(loaderConfig);
                auditService.getWorkOrderPartsForWOArrray($scope.containersForNewVisit).then(function (woLines) {
                    auditService.planVisit(selectedDate, currentShop, description, $scope.containersForNewVisit).then(function (result) {
                        WL.Analytics.log({
                            AC_action: "Schedule Visit",
                            AC_userName: userContext.getUsername(),
                            AC_shop: currentShop,
                            AC_nrOfWorkOrders: result.plannedWorkOrders.length,
                            AC_timestamp: getUTCDate()
                        }, 'Schedule Visit');
                        var lockedWorkOrders = result.lockedWorkOrders
                        var jsonStoreVisits = result.plannedWorkOrders;
                        var visitId = result.visitId;
                        var plannedWorkOrders = getPlannedContainers($scope.containersForNewVisit, lockedWorkOrders);
                        // we have woLines and visitId, just add the visitId to the lines and save them in json store!

                        auditService.saveWorkOrderLines(woLines, plannedWorkOrders, visitId);

                        // send mail to shop
                        repairShopService.getShopEmail(currentShop).then(function (shopEmail) {
                            repairShopService.sendEmailToShop(shopEmail, userContext.getEmail(), selectedDate, currentShop, plannedWorkOrders, {
                                planned: true
                            });
                        });

                        $surveyModule.refreshVisitData().then(function () {
                            $ionicLoading.hide();
                            feedbackPlanVisit(lockedWorkOrders, $scope.containersForNewVisit, currentShop);
                        }).fail(function () {
                            // add to visit collection and refresh offline
                            auditService.addWorkOrdersToVisitCollection(jsonStoreVisits).then(function () {
                                $surveyModule.refreshVisitDataOffline().then(function () {
                                    $ionicLoading.hide();
                                    feedbackPlanVisit(lockedWorkOrders, $scope.containersForNewVisit, currentShop);
                                }).fail(function () {
                                    $ionicLoading.hide();
                                    feedbackPlanVisit(lockedWorkOrders, $scope.containersForNewVisit, currentShop);
                                });
                            });
                        });
                    });
                }).fail(function () {
                    multiModal.openModal(failMessage, $scope);
                    $ionicLoading.hide();
                });
            }).fail(function () {
                multiModal.openModal(failMessage, $scope);
                $ionicLoading.hide();
            });
        }, function () {
            $scope.containersForNewVisit = [];
        });
    }

	/**
	 * @name getPlannedContainers
	 * @desc Retrieve only Work Orders that can be planned and selected by user
	 * @param {Array} selectedWorkOrders selected work orders by the user
	 * @param {Array} lockedWorkOrders locked work orders in current shop
	 * @returns {Object} list of planned work orders
	 * @memberOf Controllers.AddVisitController
	 */
    function getPlannedContainers(selectedWorkOrders, lockedWorkOrders) {
        var plannedWorkOrders = [];

        for (var i = 0; i < selectedWorkOrders.length; i++) {
            var containerInfo = selectedWorkOrders[i];
            var planned = true;
            for (var j = 0; j < lockedWorkOrders.length; j++) {
                if (lockedWorkOrders[j] == containerInfo.workOrderId) {
                    planned = false;
                    break;
                }
            }
            if (planned) {
                plannedWorkOrders.push(containerInfo);
            }
        }

        return plannedWorkOrders;
    }


    $failAddLockedWO = multiModal.setBasicOptions('notification', 'failAddLockedWO', 'The selected Work Order is LOCKED and can not be added to the visit.');
    $failAddLockedWO = multiModal.setActions($failAddLockedWO, function () {
        multiModal.removeAllModals();
    });

	/**
	 * @name feedbackPlanVisit
	 * @desc Give feedback to user based on selected work orders to add in visit
	 * @param {Array} lockedWorkOrders list of locked work orders in specified shop
	 * @param {Array} expectedWorkOrders list of selected work orders by user 
	 * @param {Integer} currentShop id of shop
	 * @returns {Void}
	 * @memberOf Controllers.AddVisitController
	 */
    function feedbackPlanVisit(lockedWorkOrders, expectedWorkOrders, currentShop) {
        if (visitDetailsOCR.date && lockedWorkOrders.length > 0) {
            //Added WO is locked and cannot be added to the visit
            console.error("CANNOT ADD A LOCKED WORKORDER IN VISIT ", lockedWorkOrders);
            multiModal.openModal($failAddLockedWO, $scope);
            return;
        } else if (lockedWorkOrders.length == 0) { // optimal case
            multiModal.openModal($visitAdded, $scope);
        } else if (lockedWorkOrders.length > 0 && lockedWorkOrders.length < expectedWorkOrders.length) { // some workorders were already booked
            multiModal.openModal($visitAddedWithExceptions, $scope);
        } else if (lockedWorkOrders.length >= expectedWorkOrders.length) { // all workorders were already booked
            $allBooked = setAllBookedRefreshAction($allBooked, currentShop);
            multiModal.openModal($allBooked, $scope);
        }
    }

    //History modal
    var $historyModal = multiModal.setBasicOptions('confirm', 'historyModal', null, 'full');
    $historyModal = multiModal.setTemplateFile($historyModal, 'templates/common/visitDetailsModal.html');


    $scope.letterObject = {};
    $scope.letterObject.index = 1;
    $scope.extractedLetters = [];

	/**
	 * @name takeFirst4letters
	 * @desc Extract and set only first 4 letters from each container number
	 * @returns {Void}
	 * @memberOf Controllers.AddVisitController
	 */
    $scope.takeFirst4letters = function () {
        for (var i = 0; i < $scope.copyOfArray.length; i++) {
            $scope.extractedLetters.pop();
        }
        $scope.extractedLetters = [];
        $scope.extractedLetters.push({
            letter: 'All',
        });

        for (var j = 0; j < $scope.copyOfArray.length; j++) {
            var letterObject = $scope.copyOfArray[j].containerNo.substring(0, 4);
            if ($scope.extractedLetters.indexOf(letterObject) == -1)
                $scope.extractedLetters.push({
                    letter: letterObject,
                });
        }
        $scope.arrayLengthLetters = $scope.extractedLetters.length;
    };
    $scope.takeFirst4letters();


    $scope.takeAllFirstLetters = function () {
        if ($scope.obj.containersForRepairShop != undefined)
            $scope.takeFirst4letters($scope.obj.containersForRepairShop);
    };

    $scope.takeFirst4letters();

    $scope.allWO = [];

	/**
	 * @name getAllWO
	 * @desc Get all Work Orders in current shop
	 * @returns {Void}
	 * @memberOf Controllers.AddVisitController
	 */
    $scope.getAllWO = function () {
        for (var i = 0; i < $scope.copyOfArray.length; i++) {
            $scope.allWO.pop();
        }
        $scope.allWO = [];
        $scope.allWO.push({
            WO: 'All',
        })

        for (var j = 0; j < $scope.copyOfArray.length; j++) {
            var WO = $scope.copyOfArray[j].workOrderId;
            if ($scope.allWO.indexOf(WO) == -1) {
                $scope.allWO.push({
                    WO: $scope.copyOfArray[j].workOrderId,
                })
            }

        }
        $scope.arrayLengthWOs = $scope.allWO.length;
    };

    $scope.getAllWO();

	/**
	 * @name getAllWO
	 * @desc Function that resets the filter's dropdown lists
	 * @returns {Void}
	 * @memberOf Controllers.AddVisitController
	 */
    $scope.resetFilter = function () {
        $scope.extractedLetters = [];
        $scope.repairModes = [];
        $scope.arrayOfStatus = [];
        $scope.arrayOfCosts = [];
        $scope.arrayOfDates = [];
    };

    $scope.repairModes = [];

	/**
	 * @name takeRepairModes
	 * @desc Get all Repair Modes for containers in shop
	 * @returns {Void}
	 * @memberOf Controllers.AddVisitController
	 */
    $scope.takeRepairModes = function () {
        for (var i = 0; i < $scope.copyOfArray.length + 1; i++) {
            $scope.repairModes.pop();
        }
        $scope.repairModes.push('All');
        for (var i = 0; i < $scope.copyOfArray.length; i++) {
            var modeRepair = $scope.copyOfArray[i].repairMode;
            if ($scope.repairModes.indexOf(modeRepair) == -1)
                $scope.repairModes.push(modeRepair);
        }
        $scope.arrayLengthModes = $scope.repairModes.length;
    };
    $scope.takeRepairModes();

	/**
	 * @name setParametersForFilter
	 * @desc Sets the filter options
	 * @param {String} filterParameter filter parameter
	 * @param {String} filterByParam filter option by parameter
	 * @returns {Void}
	 * @memberOf Controllers.AddVisitController
	 */
    $scope.setParametersForFilter = function (filterParameter, filterByParam) {
        if (filterByParam == 'All') {
            $scope.filterParam = 'showAll';
        } else {
            $scope.filterParam = filterParameter;
            $scope.filterFor = filterByParam;
        }
    }

    $scope.arrayOfStatus = [];

	/**
	 * @name takeStatus
	 * @desc Get an array of statuses
	 * @returns {Void}
	 * @memberOf Controllers.AddVisitController
	 */
    $scope.takeStatus = function () {
        for (var i = 0; i < $scope.copyOfArray.length + 1; i++) {
            $scope.arrayOfStatus.pop();
        }
        $scope.arrayOfStatus.push('All');
        for (var i = 0; i < $scope.copyOfArray.length; i++) {
            var statuss = $scope.copyOfArray[i].status;
            if ($scope.arrayOfStatus.indexOf(statuss) == -1)
                $scope.arrayOfStatus.push(statuss);
        }
        $scope.arrayLengthStatus = $scope.arrayOfStatus.length;
    };
    $scope.takeStatus();

    $scope.arrayOfCosts = [];

	/**
	 * @name takeCost
	 * @desc Get an array of all costs of containers
	 * @returns {Void}
	 * @memberOf Controllers.AddVisitController
	 */
    $scope.takeCost = function () {
        for (var i = 0; i < $scope.copyOfArray.length + 1; i++) {
            $scope.arrayOfCosts.pop();
        }
        $scope.arrayOfCosts.push('All');
        for (var i = 0; i < $scope.copyOfArray.length; i++) {
            var cost = $scope.copyOfArray[i].repairCost;
            if ($scope.arrayOfCosts.indexOf(cost) == -1)
                $scope.arrayOfCosts.push(cost);
        }
        $scope.arrayLengthCosts = $scope.arrayOfCosts.length;
    }
    $scope.takeCost();

    $scope.arrayOfDates = [];

	/**
	 * @name takeDate
	 * @desc Get an array of all dates of containers
	 * @returns {Void}
	 * @memberOf Controllers.AddVisitController
	 */
    $scope.takeDate = function () {
        for (var i = 0; i < $scope.copyOfArray.length + 1; i++) {
            $scope.arrayOfDates.pop();
        }
        $scope.arrayOfDates.push("All");
        for (var i = 0; i < $scope.copyOfArray.length; i++) {
            var date = $scope.copyOfArray[i].visitDate;
            if ($scope.arrayOfDates.indexOf(date) == -1)
                $scope.arrayOfDates.push(date);
        }
        $scope.arrayLengthDates = $scope.arrayOfDates.length;
    }
    $scope.takeDate();

    $scope.currentChoice = {};
    $scope.dropdownVisible = true;
    $scope.toggleDropDown = false;

	/**
	 * @name selectOption
	 * @desc Select country from array of countries
	 * @returns {Void}
	 * @memberOf Controllers.AddVisitController
	 */
    $scope.selectOption = function (container, $index) {
        $scope.showDiv = false;
        $scope.currentCountry = $scope.countries[$index];
        $scope.currentCountryName = $scope.currentCountry.name;
    };

    //Order function for table's header
    $scope.predicate = '';
    $scope.reverse = false;

    $scope.order = function (predicate) {
        $scope.reverse = ($scope.predicate === predicate) ? !$scope.reverse : false;
        $scope.predicate = predicate;
    };


    $scope.filterParam = null;
    $scope.filterFor = null;

    $scope.searchFields = ['workOrderId', 'containerNo', 'status', 'repairDate', 'repairMode', 'displayRepairCost'];
    $scope.count = 0;

	/**
	* @name filterTable
	* @desc Apply filters on table 
	* @param {String} filterField
	* @param {String} filterFor
	* @returns {Void}
	* @memberOf Controllers.AddVisitController
	*/
    $scope.filterTable = function (filterField, filterFor) {
        if ($scope.count != 0) {
            $scope.obj.containersForRepairShop = [];
            for (var i = 0; i < $scope.copyOfArray.length; i++) {
                $scope.obj.containersForRepairShop.push($scope.copyOfArray[i]);
            }

            if (filterFor == 'All') {
                $scope.filterParam = 'showAll';
                $scope.obj.containersForRepairShop = $filter('generalFilter')($scope.obj.containersForRepairShop, 'showAll', $scope.filterFor);
                $scope.ModalFilterHelp.filteredData = $scope.obj.containersForRepairShop;
                $scope.count = $scope.count + 1;
            } else {
                $scope.filterParam = filterField;
                $scope.filterFor = filterFor;
                $scope.obj.containersForRepairShop = $filter('generalFilter')($scope.obj.containersForRepairShop, $scope.filterParam, $scope.filterFor);
                $scope.ModalFilterHelp.filteredData = $scope.obj.containersForRepairShop;
                $scope.count = $scope.count + 1;
            }
        } else {
            if (filterFor == 'All') {
                $scope.filterParam = 'showAll';
                $scope.obj.containersForRepairShop = $filter('generalFilter')($scope.obj.containersForRepairShop, 'showAll', $scope.filterFor);
                $scope.ModalFilterHelp.filteredData = $scope.obj.containersForRepairShop;
                $scope.count = $scope.count + 1;
            } else {
                $scope.filterParam = filterField;
                $scope.filterFor = filterFor;
                $scope.obj.containersForRepairShop = $filter('generalFilter')($scope.obj.containersForRepairShop, $scope.filterParam, $scope.filterFor);
                $scope.ModalFilterHelp.filteredData = $scope.obj.containersForRepairShop;
                $scope.count = $scope.count + 1;
            }
        }
    }

    $scope.sortTable = function (value) {
        $scope.reverse = ($scope.predicate === value) ? false : !$scope.reverse;
        $scope.obj.workOrdersForRepairShop = $filter('orderBy')($scope.obj.workOrdersForRepairShop, value, $scope.reverse);
        $scope.ModalFilterHelp.filteredData = $scope.obj.workOrdersForRepairShop;
    }

    //clear search
    $scope.clearSearch = function () {
        $scope.search = '';
    };

	/**
	* @name finishSchedule
	* @desc Finish adding containers and redirect to upcoming
	* @returns {Void}
	* @memberOf Controllers.AddVisitController
	*/
    function finishSchedule() {
        //Close all modals
        multiModal.removeAllModals();
        $ionicLoading.show(loaderConfig);
        startedAudit.checkForStartedVisit().then(function () {
            $ionicLoading.hide();
            $scope.$evalAsync(function () {
                $location.switchTab('surveyTab/upcoming');
            });
        });
    }

};