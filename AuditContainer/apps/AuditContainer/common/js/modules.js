var audit = angular.module("Audit", ['ngRoute','angular-swipe-element', 'ionic', 'ui.bootstrap','localDBModule','multiModal','ngIOS9UIWebViewPatch', 'chart.js','angular-svg-round-progress','searchUtils','ngImageViewer','ngStorage', 'iosTouchend', 'autoSuggest', 'ngDraggable', 'databaseSearch','reporting', 'integer', 'pagination','pcsValidator']);
var multiModal = angular.module('multiModal', []);
var localDBModule = angular.module('localDBModule', ['loggerModule']);
var logModule = angular.module("loggerModule", []);