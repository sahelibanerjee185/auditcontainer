/**
 * Oct 28, 2015 WL common init v2 AlinPreda
 */

/**
 * Main AppInit
 * @namespace AppInit
 */

/**
 * @name wlCommonInit
 * @desc Configures logger,
 * initializes json store collections,
 * initializes userContext,
 * initializes ActionReceiver to communicate with native
 * @param N/A
 * @returns {Promise}
 * @memberOf AppInit.Main
 */
function wlCommonInit() {
    
    var LOG_CONFIG = {
        enabled: true,
        level: 'error',
        stringify: true,
        pretty: true,
        tag: {level: true, pkg: true},
        whitelist: [],
        blacklist: [],
        autoSendLogs: true,
        capture: true
    };
    
    WL.Logger.config(LOG_CONFIG);

    getServer(); //TODO: get server url and specify env by that
    
    //Calls WL.Client.Connect
    WLClientConnect();

    //Set a handler for when the app returns to Foreground to reconnect immediately
    WL.App.BackgroundHandler.setOnAppEnteringForeground(WLClientConnect);

    // hybrid init has to be initialized here
    hybrid.init();
    window.contextPromise = $.Deferred();
    angular.bootstrap(document, ['Audit']);

    // Initialize JSONStore and add dummy data
    var injector = angular.injector([ 'localDBModule', 'ng' ]);
    injector.invoke(['$localDB', '$q', '$settings',	function($localDB, $q, $settings) {

        // JSONStore options
        var options = {
            markDirty : false
        };

        function loadNonCriticalDummyData(){
            var defer = $q.defer();

            $localDB.add('country',JSONStoreDummyData.countryData,options)
            .then(function(){
                defer.resolve();
            })
            return defer.promise;
        }

        function printCollection(collection){
            WL.Logger.debug("Printing the",collection,"collection")
            $localDB.find(collection,{},{})
            .then(function(data) {
                WL.Logger.debug(data)
            })
        }

        // JSONStore initialization
        $localDB.init(collections)
        .then(function() {
            WL.Logger.debug("JSONStore successfully initialized! Checking if there is data already in it!");
            // checking if JSONStore already has data in it
            $localDB.countCollection('user')
            .then(function(numberOfDocuments) {
                if (numberOfDocuments < 1) {
                    WL.Logger.debug("JSONStore was empty! Adding default/dummy data!")
                    // adding dummy data
                    $localDB.add('settings',JSONStoreDummyData.settingsData,options)
                    .then(function() {
                        $localDB.add('user',JSONStoreDummyData.userData,options)
                        .then(function() {
                            // loading user context when enough of the JSONStore
                            // is initialized
                            WL.Logger.debug("Critical default/dummy data succesfully added to JSONStore! Loading User Context!")
                            userContext.load()
                            .then(function() {
                                window.contextPromise.resolve();
                            })
                            .then(function() {
                                WL.Logger.debug("All default/dummy data succesfully added to JSONStore!")
                            })
                        })
                    })
                }
                else {
                    WL.Logger.debug("JSONStore already had data in it! Loading User Context!")
                    userContext.load()
                    .then(function() {
                        window.contextPromise.resolve();
                    })
                }
            })
        })
        .fail(function(error) {
            WL.Logger.error("JSONStore corrupted!  Will be destroyed and rebuilt! --> ",error);
            $localDB.destroy()
            .then(function() {
                WL.Client.reloadApp();
            });
        })
    }]);

    function WLClientConnect(){
        console.log("Into WLClientConnect");
        //WL.Client.connect() to obtain token from server
        WL.Client.connect({
            onSuccess: onConnectSuccess,
            onFailure: onConnectFailure
        });
    }

    function onConnectSuccess(){
        WL.Logger.debug("WL.Client.connect success");
        WL.Analytics.log({AC_action : "App Session", AC_timestamp : getUTCDate() },'App Session');
        WLobtainAccessToken();
    }

    function onConnectFailure(failure){
        WL.Logger.info("WL.Client.connect failure: " + JSON.stringify(failure));
    }

    function getServer(){
        WL.App.getServerUrl(function(result){
            console.log("getServerUrl result: " + JSON.stringify(result));
        }, function(error){
            console.log("getServerUrl error: " + JSON.stringify(error));
        });
    }

    function WLobtainAccessToken() {
        WLAuthorizationManager.obtainAuthorizationHeader()
        .then(function(header) {
            //WL.Logger.debug("obtainAuthorizationHeader. Header: " +header);
        }, function(error) {
            WL.Logger.info("obtainAuthorizationHeader failure: " + JSON.stringify(error));
        });
    }
}

