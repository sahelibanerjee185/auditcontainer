// Uncomment the initialization options as required. For advanced initialization options please refer to IBM Worklight Information Center 
 
 var wlInitOptions = {
		 
	// # Should application automatically attempt to connect to Worklight Server on application start up
	// # The default value is true, we are overriding it to false here.
	showIOS7StatusBar : false,
	
	// # To disable automatic hiding of the splash screen uncomment this property and use WL.App.hideSplashScreen() API
	autoHideSplash: true,
	analytics : {enabled: true}
		 
	// # The callback function to invoke in case application fails to connect to MobileFirst Server
	//onConnectionFailure: function (){},
	
	// # MobileFirst Server connection timeout
	//timeout: 30000,
	
	// # How often heartbeat request will be sent to MobileFirst Server
	//heartBeatIntervalInSecs: 7 * 60,
	
	// # Enable FIPS 140-2 for data-in-motion (network) and data-at-rest (JSONStore) on iOS or Android.
	//   Requires the FIPS 140-2 optional feature to be enabled also.
	//enableFIPS : false,
	
	// # The options of busy indicator used during application start up
	//busyOptions: {text: "Loading..."}
};

 if (window.addEventListener) {
		window.addEventListener('load', function() { 
			WL.Client.init(wlInitOptions);
			document.addEventListener("deviceready", function(){
				ionic.Platform.isFullScreen = true;
				if (ionic.Platform.isAndroid()) {
					if (typeof(initKeboardEvents) === 'function') {
						initKeboardEvents();
					}
				}
			}, false);

		}, false);
	} else if (window.attachEvent) {
		window.attachEvent('onload',  function() { 
			WL.Client.init(wlInitOptions); 
			document.attachEvent("deviceready", function(){
				ionic.Platform.isFullScreen = true;
				if (ionic.Platform.isAndroid()) {
					if (typeof(initKeboardEvents) === 'function') {
						initKeboardEvents();
					}
				}
			}, false);
		});
	}; 
