var ScreenShot = {
	formats : ['png', 'jpg'],
save:function(callback,format, quality, top, left, height, width) {
	//console.log(arguments);
	if (window.cordova === undefined){
		callback && callback("Cordova not defined!");
		return;
	}
	format = (format || 'png').toLowerCase();
	//filename = filename || 'screenshot_'+Math.round((+(new Date()) + Math.random()));
	if(this.formats.indexOf(format) === -1){
		return callback && callback(new Error('invalid format '+format));
	}
	quality = typeof(quality) !== 'number'?100:quality;
	cordova.exec(function(res){
					callback && callback(null,res);
				}, function(error){
					callback && callback(error);
				}, "ScreenShot", "saveScreenShot", [format, quality, top, left, height, width]);
}
};