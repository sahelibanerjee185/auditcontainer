var Keyboard = function() {
};

Keyboard.hideKeyboardAccessoryBar = function(hide) {
    cordova.exec(null, null, "Keyboard", "hideKeyboardAccessoryBar", [hide]);
};

Keyboard.close = function() {	
	cordova.exec(null, null, "Keyboard", "close", []);
};

Keyboard.show = function() {
	cordova.exec(null, null, "Keyboard", "show", []);
};

Keyboard.disableScroll = function(disable) {
	cordova.exec(null, null, "Keyboard", "disableScroll", [disable]);
};

/*
Keyboard.styleDark = function(dark) {
 exec(null, null, "Keyboard", "styleDark", [dark]);
};
*/

Keyboard.isVisible = false;