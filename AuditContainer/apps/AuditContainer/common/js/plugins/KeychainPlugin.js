var KeychainPlugin = {
	saveToken: function (success, fail, serviceIdentifier, token) {
		if (window.cordova === undefined){
			if (fail !== undefined)
				fail.call(null, -1);
			return;
		}
		return cordova.exec(success, fail, "KeychainPlugin", "saveToken", [serviceIdentifier, token]);
	},
	loadToken: function (success, fail, serviceIdentifier) {
		if (window.cordova === undefined){
			if (fail !== undefined)
				fail.call(null, -1);
			return;
		}
		return cordova.exec(success, fail, "KeychainPlugin", "loadToken", [serviceIdentifier]);
	}
};