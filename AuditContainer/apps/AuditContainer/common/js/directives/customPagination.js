audit
    .directive(
        'customPagination',
        function ($state, pagService, $ionicLoading, $codesModule) {
            return {
                restrict: "EA",
                scope: {
                    updateResults: '&',
                    config : '='
                },
                templateUrl: "templates/customPagination/pagination.html",
                link: function (scope, elem, attrs) {
                	console.log("link start");
                	scope.currentPage = 1;
                	scope.paginationButtons = pagService.buildPagination(scope.currentPage, scope.config.arrayLength, scope.config.entriesPerPage, scope.config.pagesLimit);
                	hybrid.sendDisableNativeUI(true);
                	console.log("pagination done");

                	scope.setCurrentPage = function(pageNo) {
                		// Check for smaller datasources!
                		console.log("setCurrentPage");
                		$ionicLoading.show(loaderConfig);
                		setTimeout(function(){
                			scope.currentPage = pageNo;
                    		scope.paginationButtons = pagService.buildPagination(pageNo, scope.config.arrayLength, scope.config.entriesPerPage, scope.config.pagesLimit);
                    		scope.config.pageNo = pageNo;
                    		scope.updateResults().then(function(){
                    			setTimeout(function(){
                    				console.log("setCurrentPage done");
                                	$ionicLoading.hide();
                                });
                    		});
                		}, 80);
                	}

                	scope.$on('paginationResultsNeedUpdating', function(){
                		scope.setCurrentPage(scope.config.pageNo);
                	});
                }
            };
        });
