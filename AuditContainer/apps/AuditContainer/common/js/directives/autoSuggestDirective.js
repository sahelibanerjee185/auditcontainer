angular.module("autoSuggest", []).directive('autoSuggest',['$parse', '$rootScope', '$compile', function($parse, $rootScope, $compile) {
 
     return {
        restrict: "A",
        scope: {
        	suggestionsOptions : "&autoSuggest",
        	model : '=ngModel'
        },
        require: 'ngModel',
        link: function (scope, element, attrs, ngModelController) {
        	
        	// temporary disabled
        	
        	return;
        	
        	scope.suggestions = scope.suggestionsOptions().suggestions;
        	
        	if (scope.suggestionsOptions().minLength)
        		scope.minLength = scope.suggestionsOptions().minLength; 
        	else 
        		scope.minLength = 2;
        	
        	var adjustHeight;
        	if (scope.suggestionsOptions().adjustHeight)
        		adjustHeight = scope.suggestionsOptions().adjustHeight
        	else 
        		adjustHeight = 0;
        	
        	scope.selectSuggestion = selectSuggestion;
        	scope.isFocused = false;
        	scope.searchHelper = {filteredItems : [] };
        	scope.startsWith = startsWith;
        	
//        	may move template to a file to be more readable
        	
        	var template = 
        	'<div ng-class="{\'oneResult\' : searchHelper.filteredItems.length == 1, \'twoResults\' : searchHelper.filteredItems.length == 2, \'threeResults\' : searchHelper.filteredItems.length >= 3}" \
        	class="dropWrapAutoTip" ng-if="isFocused && model.length >= minLength"><ion-content has-bouncing="true"> \
        	<div ng-repeat="item in searchHelper.filteredItems = (suggestions | filter:model:startsWith)" ng-click="selectSuggestion(item)" class="suggestItem">{{item}}</div> \
        	</ion-content></div>';
        	
        	var listItems = angular.element(template);
        	
        	element.after(listItems);
    		$compile(listItems)(scope);
    		
        	element.on('blur', function(event) {
        		if (typeof keyboardInfo != 'undefined' && typeof keyboardInfo.adjustHeight == 'function')
        			keyboardInfo.adjustHeight(0);
        		
        		scope.$evalAsync(function(){
        			scope.isFocused = false;
        		});
        	});
        	
        	element.on('focus', function(event) {
        		if (typeof keyboardInfo != 'undefined' && typeof keyboardInfo.adjustHeight == 'function')
        			keyboardInfo.adjustHeight(adjustHeight);
        		
        		scope.$evalAsync(function(){
        			scope.isFocused = true;
        		});
        	});
        	
//        	utility functions
        	
        	function selectSuggestion(item) {
        		ngModelController.$setViewValue(item);
        		ngModelController.$render();
            }
        	
        	function startsWith(actual, expected) {
        	    var lowerStr = (actual + "").toLowerCase();
        	    return lowerStr.indexOf(expected.toLowerCase()) === 0;
        	}
        }
    };
 
}]);