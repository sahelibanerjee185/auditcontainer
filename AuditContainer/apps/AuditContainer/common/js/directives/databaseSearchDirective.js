/**
 * Apr, 6 2016
 * databaseSearchDirective.js
 * Stavarache Vlad
 */
(function(){
//	'use strict';
	
	angular.module("databaseSearch", []).directive('databaseSearch', databaseSearch);
		
	databaseSearch.$inject = ['$parse', '$rootScope', '$compile', '$localDB', 'repairShopService', '$planVisit','$timeout','$ionicScrollDelegate','$ionicLoading'];	
		
	function databaseSearch($parse, $rootScope, $compile, $localDB, repairShopService, $planVisit,$timeout,$ionicScrollDelegate,$ionicLoading) {
		return {
	        restrict: "A",
	        scope: {
	        	searchSettings : "=databaseSearch",
	        	model : '=ngModel'
	        },
	        require: 'ngModel',
	        link: function (scope, element, attrs, ngModelController) {
	    		ngModelController.$viewChangeListeners.push(
                    handleNgModelChange
                );
	        	 var obj={
		    				currentScrollDownOffset:0,
		    				currentPullUpOffset:0,
		    				scrollDownOffsetActiveSearch:0,
		    				withSearch:false,
		    				maxLengthRepairShops:300
		    		}
	        	 scope.currentRepairShops =[];
	        	 scope.newRepairShops=[];
	        	 scope.initQueryPartsArray=[];
	        	 
	        	 $rootScope.$on("selectCountryForPlanVisit", function(event, args){
	        		 handleNgModelChange();
	         	 });
	        	 
	        	 $rootScope.$on("sortResultsForPlanVisit", function(event, args){
	        		 handleNgModelChange();
	         	 });
	        	 
	    		function handleNgModelChange(){
	    			var searchString = ngModelController.$viewValue;
	    			var countryInfo = scope.$parent.selectedCountry;
	    			var countryCode;
	    			if (countryInfo.name != 'All Countries') {
	    				countryCode = scope.$parent.selectedCountry.code;
	    			}
	    			var sortOptions = scope.$parent.sortOptions;
	    			
	    			// $scope.sortOptions = {column : column, ascending: true}
	                
	                var options = {
						filter: ['code', 'name', 'location', 'country', 'cached']
					};
	                
	                if (typeof sortOptions === 'object') {
	                	var columnName = scope.$parent.sortOptions.column;
	                	var sortingAscending = scope.$parent.sortOptions.ascending;
	                	var sortingObject = {};
	                	sortingObject[columnName] = sortingAscending ? 'ASC' : 'DESC';
	                	options.sort = [sortingObject];
	                }
	                
	    			(function(searchString){
	    				if (searchString && searchString.length >= 0) {
		    				// call search
	    					// move call to json store in separate function TODO
	    					
	    					var queryShopCode = WL.JSONStore.QueryPart()
	    	        			.like('code', searchString);
	    					
	    					var queryShopName = WL.JSONStore.QueryPart()
	    						.like('name', searchString);
	    					
	    					var queryLocation = WL.JSONStore.QueryPart()
    	        				.like('location', searchString);
	    					
	    					if (typeof countryCode === 'string' ) {
	    						// first find for specific countries
	    						queryShopCode.equal('country', countryCode);
	    						queryShopName.equal('country', countryCode);
	    						queryLocation.equal('country', countryCode);
	    					}
	    					
	    					var queryPartsArray = [queryShopCode, queryShopName, queryLocation];
	    					
	    					WL.JSONStore.get('repairShop').advancedFind(queryPartsArray, options)
	    					.then(function(searchResults){
    							if (searchString == ngModelController.$viewValue) {
    								scope.$parent.showLoader = false;
    								$rootScope.$evalAsync(function(){
    									scope.$parent.allRepairShops = searchResults;
    								})
    							}
	    					}).fail(function(e){
	    						scope.$parent.showLoader = false;
	    						console.log('not working', e);
	    					});
		    			} else {
		    				var queryPart;
		    				
		    				if (typeof countryCode === 'string') {
		    					queryPart = WL.JSONStore.QueryPart().equal('country', countryCode);
		    				} else {
		    					queryPart = {};
		    				}
		    				
		    				WL.JSONStore.get('repairShop').advancedFind([queryPart], options).then(function(results){
		    					scope.$parent.showLoader = false;
		    					$rootScope.$evalAsync(function(){
									scope.$parent.allRepairShops = results;
								})
		    				}).fail(function(e){
		    					scope.$parent.showLoader = false;
		    					console.log('not working', e);
		    				});
		    			}
	    			})(searchString);
	    		}
	        }
	    };
	}
})();

