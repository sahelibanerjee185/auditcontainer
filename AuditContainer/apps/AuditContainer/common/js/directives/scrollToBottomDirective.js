'use strict';

/**
 * scrollToBottom directive
 *
 * scrolls to the bottom of the div
 */

audit.directive('scrollToBottom', function($timeout, $window) {
    return {
        scope: {
            scrollToBottom: '='
        },
        restrict: 'A',
        link: function(scope, element, attrs) {
            scope.$watchCollection('scrollToBottom', function(newVal) {
                if (newVal) {
                    $timeout(function() {
                        element[0].scrollTop = element[0].scrollHeight;
                    }, 0);
                }
            })
        }
    }
});
