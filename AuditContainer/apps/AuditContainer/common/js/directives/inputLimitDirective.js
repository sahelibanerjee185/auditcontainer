'use strict';

/**
 * This directive limits the user input to the desired length
 * usage: <input limit-to="15" ... />
 */
audit.directive('limitTo', limitTo);

function limitTo() {
  return {
    restrict: 'A',
    link: function(scope, element, attrs, ctrl) {

      var limit = parseInt(attrs.limitTo);

      angular.element(element).on('keypress', function(e) {
        if (this.value.length >= limit) {
          e.preventDefault();
        }
      });

    }
  }
};
