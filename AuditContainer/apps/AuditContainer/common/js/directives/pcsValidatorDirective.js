angular.module("pcsValidator", []).directive('pcsValidator',function(){
	return {
		restrict : 'A',
		require : 'ngModel',
		scope : {
			part : '='
		},
		link : function(scope,elem,attrs,ngModelCtrl){
			
			console.log("this is the part", scope.part);
			
			ngModelCtrl.$parsers.push(function(modelValue){
				if(modelValue == "0"){
					modelValue = "";
				}
				
				ngModelCtrl.$setViewValue(modelValue);
				ngModelCtrl.$render();
				return modelValue;
			});
			
			ngModelCtrl.$formatters.push(function(modelValue){
				if(scope.part.$notRequired){
					modelValue = "0";
				}
				return modelValue;
			});
		}
	}
});