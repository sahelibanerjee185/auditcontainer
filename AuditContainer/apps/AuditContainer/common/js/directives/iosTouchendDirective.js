angular.module("iosTouchend", []).directive('iosTouchend',['$parse', '$rootScope', function($parse, $rootScope) {
	return {
		restrict: 'A',
		scope: true,
		compile: function($element, attr){
			
			var eventName = ('ontouchstart' in window) ? 'touchstart' : 'click';
			var fn = $parse(attr['iosTouchend'], null, true);
			
			var disabledTime;
			var maxMovement;
			
			if (attr['modalMaxMovement']) {
				maxMovement = attr['modalMaxMovement'];
			} else {
				maxMovement = 15;
			}
			
			if (attr['modalDisableTime']) {
				disabledTime = attr['modalDisableTime'];
			} else {
				disabledTime = 1000;
			}
			
			return eventHandler;
			
			function eventHandler(scope, element) {
				
				element.on(eventName, function(eventStart){
					
					var isDisabled = $parse(attr['ngDisabled'])(scope);
                    
                    if (isDisabled) return;
					
					if (eventName === 'touchstart') {
						
//						this is the case for mobiles, tablets, touch devices
						
						if (eventStart.target != element[0]) 
							return;
						
//						remove previous event listener for touchend
						
						element.off('touchend');
						
						element.on('touchend', function(eventEnd){
							
//							if user clicks more than once within a short interval don't do anything
							
							if (scope.isDisabled)
								return;
							
							scope.isDisabled = true;
							
//							reenable touchend execution after a short interval
							
							setTimeout(function(){
								scope.isDisabled = false;
							}, disabledTime);
							
//							check if the touchend moved significantly from the touchstart event, if it did, ignore the action
							
							if (Math.abs(eventEnd.pageX - eventStart.pageX) > maxMovement || Math.abs(eventEnd.pageY - eventStart.pageY) > maxMovement) 
								return;
							
							executeCallback(eventEnd, scope);
							
						})
						
					} else if (eventName === 'click') {
						
//						this is the case for non touch

						if (event.target != element[0]) 
							return;
						
						executeCallback(eventStart, scope);
					} 
				});
			}
			
			function executeCallback(event, scope) {
				event.stopPropagation();
				event.preventDefault();
				
				var callback = function(){
					fn(scope, {$event : event});
				};
				
				if ($rootScope.$$phase) {
	                scope.$evalAsync(callback);
	            } else {
	                scope.$apply(callback);
	            }
			}
			
		}
	}
}]);

