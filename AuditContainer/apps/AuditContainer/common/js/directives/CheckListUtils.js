(function (audit) {

    audit
        .factory('CheckListUtils', CheckListUtils);

    CheckListUtils.$inject = [];

    function CheckListUtils() {

        var exports = {
            buildCheckListConfig: _buildCheckListConfig,
            initCheckListValues: _initCheckListValues,
            getCheckListValue: _getCheckListValue
        };

        return exports;



        function _buildCheckListConfig(index, label) {
            return {
                label: label,
                persistenceIndex: index,
                value: false
            };

        }

        function _initCheckListValues(checkListConfig, stringValue) {
            //TODO... error checking
            for (var i = 0; i < checkListConfig.length; i++) {
                var item = checkListConfig[i];
                item.value = stringValue.charAt(item.persistenceIndex) == '1' ? true : false;
            }
        }

        function _getCheckListValue(checkListConfig) {
            //TODO... error checking
            var valuesArray = new Array(checkListConfig.length + 1);

            for (var i = 0; i < checkListConfig.length; i++) {
                var item = checkListConfig[i];
                valuesArray[item.persistenceIndex] = item.value === true ? '1' : '0';
            }

            return valuesArray.join("");
        }

    }
}(audit));