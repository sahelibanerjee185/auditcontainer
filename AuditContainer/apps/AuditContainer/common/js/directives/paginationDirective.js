angular.module("pagination", []).directive('paginationCustom',['$parse', '$rootScope', '$compile', '$http', '$filter', '$ionicLoading', '$ionicScrollDelegate', '$planVisit', 'ToastService','ocrService','$timeout', function($parse, $rootScope, $compile, $http, $filter, $ionicLoading, $ionicScrollDelegate, $planVisit, ToastService,ocrService,$timeout) {

     return {
        restrict: "A",
        scope: {
        	paginationOptions : "&paginationCustom",
        	model : '=ngModel'
        },
        require: 'ngModel',
        link: function (scope, element, attrs, ngModelController) {

        	var dataSource = scope.paginationOptions().dataSource;
        	// var myWorkOrderStatuses = [{status: "Rejected to CPH"}, {status: "Suspended to CPH"}];
        	scope.statusValues = $planVisit.getObj().woStatusList.resultSet;
        	scope.modeValues = $planVisit.getObj().woModeList.resultSet;
        	scope.gateInValues = $planVisit.getObj().gateInValuesList;
        	scope.buttonLabel = $planVisit.getObj().addVisitText;

        	$rootScope.$on("updatedWorkOrdersFromServer", function(event, args){
        		dataSource = args.results;
        		scope.paginationSearch = "";
        		scope.currentPage = 1;
        		scope.sortingOptions = {fieldName : null, ascending : null};
        		setCurrentResults(1, dataSource, scope.paginationSearch);
        	});

        	var searchFields = scope.paginationOptions().searchFields;

        	if (scope.paginationOptions().entriesPerPage) {
        		scope.entriesPerPage = scope.paginationOptions().entriesPerPage;
        	} else {
        		scope.entriesPerPage = 20;
        	}

        	if (scope.paginationOptions().pagesLimit) {
        		scope.pagesLimit = scope.paginationOptions().pagesLimit;
        	} else {
        		scope.pagesLimit = 3;
        	}

        	initView();

        	var templatePath = 'templates/pagination/pagination.html'

       		$http.get(templatePath).then(function(response) {
       			var listItems = angular.element(response.data);

       			element.replaceWith(listItems);
        		$compile(listItems)(scope);

        		scope.$watch('paginationSearch', function() {
        			modelChangeHandler(scope.paginationSearch)
        	    });
       		});

        	function initView() {
                scope.preFilterSettings = {status: null, mode: null, gate: null};
        		scope.currentPage = 1;
        		scope.sortingOptions = {fieldName : null, ascending : null};
        		// setCurrentResults(1, dataSource, scope.paginationSearch);
        		scope.setCurrentPage = setCurrentPage;
        		scope.confirmSelections = confirmSelections;
        		scope.selectRow = selectRow;
        		scope.sortEntries = sortEntries;
        		scope.clearSearch = clearSearch;
        		scope.refreshResults = refreshResults;
        		scope.cacheResults = cacheResults;
        		scope.showHistory = showHistory;
        		scope.showRemarks = showRemarks;
                scope.changeStatus = changeStatus;
                scope.changeMode = changeMode;
                scope.changeGateInDate = changeGateInDate;
                scope.setMyWorkOrders = setMyWorkOrders;
                scope.setAllWorkOrders = setAllWorkOrders;
                scope.myWorkOrders = false;
                scope.changePageResults = changePageResults;

				scope.openOCR = openOCR;
				scope.ocrService = ocrService;
				scope.capitalizeLetters = capitalizeLetters;
				scope.validateOCR = validateOCR;
				scope.workOrderPreview = workOrderPreview;
        	}


			//OCR LINK FUNCTIONS
			function openOCR(){ scope.$parent.openOCR(); };
			function capitalizeLetters(item,index){
				scope.$parent.capitalizeLetters(item,index);
			};
			function validateOCR(ocrData){
				scope.$parent.validateOCR(ocrData);
			};
			function workOrderPreview(){
				scope.$parent.workOrderPreview();
			}
			//END OCR FUNCTIONS

        	function setMyWorkOrders(){
        		if (scope.myWorkOrders) return;
        		scope.myWorkOrders = true;
        		scope.statusValues = myWorkOrderStatuses;
        		setCurrentResults(1, dataSource, scope.paginationSearch);
        	}

        	function setAllWorkOrders(){
        		if (!scope.myWorkOrders) return;
        		scope.myWorkOrders = false;
        		scope.statusValues = $planVisit.getObj().woStatusList.resultSet;
        		setCurrentResults(1, dataSource, scope.paginationSearch);
        	}

            function changeStatus(status){
                scope.hideDropStatus=false;
                 if (scope.preFilterSettings.status == status) return;

                scope.preFilterSettings.status = status;

                setCurrentResults(1, dataSource, scope.paginationSearch);

            }

            function changeGateInDate(GateInInfo){
                scope.hideDropGate = false;

                if (scope.preFilterSettings.gate == GateInInfo) return;

                if (GateInInfo) {
                	scope.preFilterSettings.gate = GateInInfo;
                } else {
                	scope.preFilterSettings.gate = null;
                }

                setCurrentResults(1, dataSource, scope.paginationSearch);

            }

            function changeMode(mode){
                scope.hideDropMode=false;
                if (scope.preFilterSettings.mode == mode) return;

                scope.preFilterSettings.mode = mode;

                setCurrentResults(1, dataSource, scope.paginationSearch);

            }

        	function modelChangeHandler() {
        		setCurrentResults(1, dataSource, scope.paginationSearch);
        	}

        	function setCurrentResults(currentPage, dataSource, searchString) {
        		$ionicLoading.show(loaderConfig);
        		scope.currentPage = currentPage;
        		var filteredResults = dataSource; // filter dataSource before pagination
        		scope.currentResults = [];
        		// if filtered results doesn't change, don't filter again TODO

                // if status or mode is selected from dropdown filter datasource

                var filterObject = {};

                if (scope.myWorkOrders) {
                	// show only unlocked work orders
                	filterObject.locked = 0;
                	var filteredResults = $filter('filter')(dataSource, {statusCode : [140, 330]}, arrayComparator, "statusCode")
                }

                if (scope.preFilterSettings.status != null) {
                    filterObject.status = scope.preFilterSettings.status;
                }

                if (scope.preFilterSettings.mode != null) {
                    filterObject.repairMode = scope.preFilterSettings.mode;
                }

                if (!angular.equals({}, filterObject)) {
                    filteredResults = $filter('filter')(filteredResults, filterObject, true);
                }

                if (scope.preFilterSettings.gate != null) {
                	var today = new Date();
                	var fromDays = scope.preFilterSettings.gate.from
                	var toDays;
                	var fromDate = new Date();
                	var toDate = new Date();

                	if (scope.preFilterSettings.gate.from) {
                		fromDate.setDate(today.getDate() - fromDays);
                	}

                	if (scope.preFilterSettings.gate.to) {
                		toDays = scope.preFilterSettings.gate.to;
                		toDate.setDate(today.getDate() - toDays)
                	} else {
                		toDate = null; // don't compare for toDate
                	}

                	filteredResults = $filter('betweenDatesFilter')(filteredResults, "gateInDateTimeStamp", fromDate, toDate);
                }

        		if (searchString) {
        			filteredResults = $filter('multiSearch')(filteredResults, searchFields, searchString);
        		}

        		filteredResults = getSortedResults(filteredResults, scope.sortingOptions);

        		// sorting options TODO

        		scope.currentResults  = setPageEntries(filteredResults);
        		scope.paginationButtons = buildPagination(currentPage, 1, filteredResults, scope.entriesPerPage, scope.pagesLimit);

        		setTimeout(function(){
        			$ionicLoading.hide();
        		});
        	}

        	function setPageEntries(filteredResults) {
        		var offset = (scope.currentPage - 1) * scope.entriesPerPage;

        		return filteredResults.slice(offset, offset + scope.entriesPerPage);
        	}

        	function getLastPage(filteredResults, entriesPerPage) {
        		return Math.ceil(filteredResults.length/entriesPerPage);
        	}

        	function buildPagination(currentPage, firstPage, filteredResults, entriesPerPage, pagesLimit) {
        		var paginationArray = [];
        		var lastPage = getLastPage(filteredResults, entriesPerPage);

        		if (filteredResults.length > 0) {
        			paginationArray.push({text : '<<', disabled : (currentPage == firstPage), page : firstPage}); // go to first page
            		paginationArray.push({text : '<', disabled : (currentPage == firstPage), page : currentPage - 1}); // go to previous page
            		if (lastPage >= 1) {
            			paginationArray.push({text: '1', disabled : (currentPage == firstPage), page : firstPage}); // first page
            		}
            		// check to see if we need a '...' block
            		if ((currentPage - pagesLimit) > 2) {
            			paginationArray.push({text : '...', disabled : true}); // '...' block
            		}
            		// get pages before current page
            		if (currentPage > 1) {
            			for (var i = 0; i < pagesLimit; i++) {
            				var pageNo = currentPage - pagesLimit + i;
            				if (pageNo > firstPage) {
            					paginationArray.push({text : pageNo, disabled : false, page : pageNo});
            				}
            			}
            		}

            		if (currentPage != firstPage && currentPage != lastPage) {
            			paginationArray.push({text : currentPage, disabled : true, page : currentPage}); // current page
            		}

            		// get pages after current page
            		if (currentPage >= 1) {
            			// up to a max of pagesLimit
            			for (var i = 1; i <= pagesLimit; i++) {
            				var pageNo = currentPage + i;
            				if (pageNo < lastPage) {
            					paginationArray.push({text : pageNo, disabled : false, page : pageNo});
            				}
            			}
            		}

            		// check to see if we need a '...' block
            		if ((currentPage + pagesLimit) < (lastPage - 1)) {
            			paginationArray.push({text : '...', disabled : true}); // '...' block
            		}

            		if (lastPage > 1) {
            			paginationArray.push({text: lastPage, disabled : (currentPage == lastPage), page : lastPage}); // last page
            		}

            		paginationArray.push({text : '>', disabled : (currentPage == lastPage), page : currentPage + 1}); // go to first page
            		paginationArray.push({text : '>>', disabled : (currentPage == lastPage), page : lastPage}); // go to previous page
        		}

        		return paginationArray;
        	}

        	function setCurrentPage(pageNo) {
        		setCurrentResults(pageNo, dataSource, scope.paginationSearch);
        	}

        	function selectRow(entry) {
        		if (!entry.locked) {
        			entry.selected = !entry.selected;
        		} else if (entry.locked == 2) {
        			ToastService.showNotificationMsg('You do not have permissions to work on this WO.');
        		} else if (entry.locked == 1) {
        			ToastService.showNotificationMsg('This work order is already booked.');
        		}
        	}

        	function confirmSelections() {
        		
        		scope.isActionInProgress = true;
        		
        		scope.$parent.confirmVisitAdd();
        		
        		$timeout(function(){
        			scope.isActionInProgress = false;
        		},1000);
        	}

        	function cacheResults() {
        		scope.$parent.cacheResults(dataSource);
        	}

        	function sortEntries(fieldName) {
        		if (scope.sortingOptions.fieldName == fieldName) {
        			scope.sortingOptions.ascending = !scope.sortingOptions.ascending;
        		} else {
        			scope.sortingOptions.fieldName = fieldName;
        			scope.sortingOptions.ascending = true; // ascending first
        		}

        		if (dataSource.length > 3000) {
        			$ionicLoading.show(loaderConfig);
        			setTimeout(function(){
        				setCurrentResults(1, dataSource, scope.paginationSearch);
        			}, 100);
        		} else {
        			setCurrentResults(1, dataSource, scope.paginationSearch);
        		}
        	}

        	function resetSorting() {
        		scope.sortingOptions = {fieldName : null};
        	}

        	function arrayComparator(actual, expected) {
        		for (var i = 0; i < expected.length; i++) {
        			if (actual == expected[i]) return true;
        		}

        		return false;
        	}

        	function getSortedResults(results, sortingOptions) {
        		if (scope.sortingOptions.fieldName === null) {
        			return results;
        		}

        		var sortedResults = results;
        		var fieldName = sortingOptions.fieldName;
        		var sortOrder = sortingOptions.ascending;

    			sortedResults = $filter('orderBy')(sortedResults, fieldName, sortOrder);

        		return sortedResults;
        	}

        	function clearSearch() {
        		scope.paginationSearch = "";
        	}

        	function refreshResults() {
        		scope.$parent.refreshResults();
        	}

        	function showHistory(containerNo,woId) {
        		scope.$parent.showVisitDetails(containerNo, woId, 'history');
        	}
        	
        	function showRemarks(containerNo,woId) {
        		scope.$parent.showVisitDetails(containerNo, woId, 'remarks');
        	}
        	
            function changePageResults() {
                scope.$parent.changePageResults();
            }


        }
    };

}]);
