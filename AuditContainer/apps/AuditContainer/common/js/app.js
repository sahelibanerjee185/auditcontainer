/**
 * App Router
 * @namespace Router
 */
(function(){
	'use strict';

	audit
        .config(config)
        .config(IonicLoadingConfig)
        .run(run);

    /**
     * @namespace config
     * @desc Configures app routing
     * @memberOf Router
     */
    function config($routeProvider) {
        $routeProvider
            .when("/logOutTab", {
                templateUrl : 'templates/login/login.html',
                controller : 'LoginCtrl'
            })
            .when("/surveyTab/:tab?", {
                templateUrl:'templates/survey/survey.html',
                controller:'CalendarCtrl'
            })	
            .when("/depotDetails/:repairShopNo", {
                templateUrl : 'templates/survey/depotDetails.html',
                controller : 'DepotDetailsCtrl',
                resolve : {
                    kpiInit : function($kpiModule) {
                        return $.when.apply($, [$kpiModule.initKpi()]);
                    }
                }
            })	
            .when("/unitRefer/:backScreen?", {
                templateUrl: 'templates/unitRefer/unitRefer.html',
                controller : 'UnitCtrl'
            })
            .when("/unitReferPostRepair/:backScreen?", {
                templateUrl: 'templates/unitRefer/unitReferPostRepair.html',
                controller : 'UnitCtrl'
            })
            .when("/planVisitTab/:date?", {
                templateUrl : 'templates/planVisit/planVisit.html',
                controller : 'PlanVisitCtrl'
            })	
            .when("/addVisitTab/:date/:visitId?/:startedTab?", {
                templateUrl : 'templates/planVisit/addVisit.html',
                controller : 'AddVisitCtrl'
            })		
            .when("/galleryTab", {
                templateUrl: 'templates/gallery/photoGallery.html',
                controller : 'GalleryCtrl'
            })
            .when("/kpiTab", {
                templateUrl : 'templates/kpi/kpi.html',
                controller : 'KpiController',
                    resolve : {
                        kpiInit : function($kpiModule) {
                            return $.when.apply($, [$kpiModule.initKpi()]);
                        }
                    }
            })	
            .when("/failedInspections", {
                templateUrl : 'templates/kpi/failedInspections.html',
                controller : 'FailedInspectionsController'
            })
            .when("/previewPdf", {
                templateUrl : 'templates/components/pdf.html',
                controller : 'PreviewPdfCtrl'
            })
            .when("/", {
                redirectTo : '/logOutTab'
            }).otherwise({
                redirectTo : '/surveyTab'
            });
    }

    /**
     * @namespace run
     * @desc Used to update native UI, setting title, displaying header,
     * footer, back button, refresh button
     * @memberOf Router
     */
    function run($rootScope, $location, $ionicPlatform, $window, multiModal, $templateCache) {
        window.addEventListener('native.keyboardshow', keyboardShowHandler);
					
        function keyboardShowHandler(event) {
            keyboardInfo.keyboardHeight = event.keyboardHeight+10; ;
        }

        $rootScope.$on("$routeChangeSuccess", function(event, next, current) {
            multiModal.removeAllModals();
            hybrid.sendDisableNativeUI(false);
            
            if (typeof keyboardInfo != 'undefined' && typeof keyboardInfo.adjustHeight == 'function')
                keyboardInfo.adjustHeight(0);
            
            if (next.$$route !== undefined){
                switch(next.originalPath) {
                    case "/logOutTab":
                        hybrid.updateNativeUI(RoutingTitles['logOutTab'], false, false ,false, false);
                        break;
                    case "/surveyTab/:tab?":
                        hybrid.updateNativeUI(RoutingTitles['inspections'], true, true, false, true);
                        break;
                    case "/previewPdf":
                        hybrid.updateNativeUI(RoutingTitles['previewPdf'], true, true, true, true);
                        break;
                    case "/depotDetails/:repairShopNo":
                        hybrid.updateNativeUI(RoutingTitles['startedVisitWO'], true, true, true, true);
                        break;
                    case "/planVisitTab/:date?":
                        hybrid.updateNativeUI(RoutingTitles['planVisitTab'], true, true, false, true);
                        break;
                    case "/addVisitTab/:date/:visitId?/:startedTab?":
                        hybrid.updateNativeUI(RoutingTitles['planVisitTab'], true, true, true, true);
                        break;
                    case "/kpiTab":
                        hybrid.updateNativeUI(RoutingTitles['kpiTab'], true, true, false, true);
                        break;
                    case "/depotDetails":
                        hybrid.updateNativeUI(RoutingTitles['startedVisitWO'], true, true, false, true);
                        break;
                    case "/manualsTab":
                        hybrid.updateNativeUI(RoutingTitles['manualsTab'], true, true, false, true);
                        break;
                    case "/unitRefer/:backScreen?":
                        var title = (next.pathParams.backScreen) ? 'startedVisitWODetails' : 'WODetails';
                        hybrid.updateNativeUI(RoutingTitles[title], true, true, true, true);
                        break;
                    case "/unitReferPostRepair/:backScreen?":
                        var title = (next.pathParams.backScreen) ? 'startedVisitWODetails' : 'WODetails';
                        hybrid.updateNativeUI(RoutingTitles[title], true, true, true, true);
                        break;
                    case "/galleryTab":
                        hybrid.updateNativeUI(RoutingTitles["galleryTab"], true, true, false, false);
                        break;
                    case "/failedInspections":
                        hybrid.updateNativeUI(RoutingTitles["failedInspections"], true, true, true, true);
                        break;
                }
            }
        });
    }
									
	IonicLoadingConfig.$inject = ['$provide'];

    /**
     * @namespace IonicLoadingConfig
     * @desc Used to disable native UI on loader showing
     * and enabling native UI in loader hiding
     * @memberOf Router
     */
	function IonicLoadingConfig($provide) {
		$provide.decorator('$ionicLoading', ['$delegate', function ($delegate) {
			var originalShow = $delegate.show;
			var originalHide = $delegate.hide;
			
			$delegate.show = function () {
			var args = [].slice.call(arguments);
			
			hybrid.sendDisableNativeUI(true);
			
			originalShow.apply(null, args);
			}
			
			$delegate.hide = function () {
			var args = [].slice.call(arguments);
			
			hybrid.sendDisableNativeUI(false);
			
			originalHide.apply(null, args);
			}
			return $delegate;
		}]);
	}

})();

