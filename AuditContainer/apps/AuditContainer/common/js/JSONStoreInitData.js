/**
 * Aug 13, 2015 
 * JSONStoreInitData.js 
 * AlinPreda
 */

var collections = {
	// Object that defines the user collection which contains just one element
	user : {
		searchFields : {
			username : 'string',
			token : 'string',
			certificateId : 'string',
			lastLoginTime : 'string'
		}
	},
	visitPolling : {
		searchFields : {
			visitId : 'string'
		}
	},
	calendarVisits : {
		searchFields : {	
			date : 'string',
			description : 'string',
			shopCode : 'string',
			shopDescription : 'string',
			visitId : 'string',
			auditStatus : 'string',
			country : 'string',
			locationDescription : 'string'
		}	
		// add isCached and timestamp here?
	},
	visit : {
		searchFields : {	
			containerNo : 'string',
			workOrderId : 'string',
			woStatus : 'string',
			containerType : 'string',
			repairDate : 'string',
			auditDate : 'string',
			description : 'string',
			inspectionStatus : 'string', // remove TODO
			repairCost : 'string',
			shopCode : 'string',
			visitId : 'string',
			auditStatus : 'string',
			woType : 'string',
			shopDescription : 'string',
			country : 'string',
			location : 'string',
            mode:'string',
            auditResult:'string',
            statusCode: 'string'
           }
	},
	
	workOrderParts : {
		searchFields : {
			visitId : 'string',
			workOrderId : 'string',
			woRepair : 'boolean',
			repairCode : 'string',
			damageCode : 'string',
			repairLocCode : 'string',
			TPI : 'string',
			mode :  'string',
			repairDescription : 'string',
			partCode : 'string',
			partDescription : 'string',
			quantity : 'string',
			partCost : 'string',
			materialCost : 'string',
			manHrs : 'string',
			totalPerCode : 'string'
		}
	},
	
	finishedVisit : {
		searchFields : {
			containerNo : 'string',
			workOrderId : 'string',
			woStatus : 'string',
			containerType : 'string',
			repairDate : 'string',
			auditDate : 'string',
			description : 'string',
			inspectionStatus : 'string', // remove TODO
			repairCost : 'string',
			shopCode : 'string',
			visitId : 'string',
			auditStatus : 'string',
			auditResult : 'string',
			woType : 'string',
			shopDescription : 'string',
			country : 'string',
			location : 'string',
			finalComment : 'string',
            mode:'string',
            mercStatus:'string',
            mercComments:'string',
            repairQuality : 'string',
            statusCode : 'string'
		}
	},
	
	startedAuditWorkOrders : {
		searchFields : {
			visitId : 'string',
			workOrderId : 'string',
			inspectionStatus : 'string',
			shopCode : 'string',
			visitDesc : 'string',
			auditResult : 'boolean',
			containerNo : 'string',
			finalComment : 'string',
            mode:'string',
            repairCost:'string',
            repairDate:'string',
            woStatus : 'string',
            statusCode : 'string',
            repairQuality : 'string'
		}
	},
	
	pendingAuditWorkOrders : {
		searchFields : {
			visitId : 'string',
			workOrderId : 'string',
			inspectionStatus : 'string',
			shopCode : 'string',
			visitDesc : 'string',
			auditResult : 'boolean',
			containerNo : 'string',
			finalComment : 'string',
            mode:'string',
            repairCost:'string',
            repairDate:'string',
            woStatus : 'string',
            statusCode : 'string',
            repairQuality : 'string'
		}
	},
	
	pendingVisitsQueue : {
		searchFields : {
			visitId : 'string',
			timestamp : 'integer',
		}
	},
	
	finishedAuditParts : {
		searchFields : {
			visitId : 'string',
			accepted : 'boolean', 
			workOrderId : 'string',
			partIndex : 'integer',
			repairIndex : 'integer',
			prePartCode : 'string',
			postPartCode : 'string',
			preRepairCode : 'string',
			postRepairCode : 'string',
			preQuantity : 'string',
			postQuantity : 'string',
			preDamageCode : 'string',
			postDamageCode : 'string',
			preRepairLocCode : 'string',
			postRepairLocCode : 'string',
			preTPI : 'string',
			postTPI : 'string',
			indentifier : 'string',
			woRepair : 'boolean',
			partDescription : 'string',
			preRepairDecription : 'string',
			postRepairDescription : 'string',
			manHrs : 'string',
			partCost : 'string',
			materialCost : 'string',
			totalPerCode : 'string',
			repairQuality : 'string'
		}
	},
	
	startedAuditParts : {
		searchFields : {
			visitId : 'string',
			woRepair : 'boolean',
			accepted : 'boolean', 
			workOrderId : 'string',
			partIndex : 'integer',
			repairIndex : 'integer',
			addedPart : 'boolean',
			hasChildParts : 'boolean',
			prePartCode : 'string',
			postPartCode : 'string',
			preRepairCode : 'string',
			postRepairCode : 'string',
			preQuantity : 'string',
			postQuantity : 'string',
			preDamageCode : 'string',
			postDamageCode : 'string',
			preRepairLocCode : 'string',
			postRepairLocCode : 'string',
			rejectedPart : 'boolean',
			preTPI : 'string',
			postTPI : 'string',
			mode : 'string',
			repairDescription : 'string',
			partDescription : 'string',
			manHrs : 'string',
			partCost : 'string',
			materialCost : 'string',
			totalPerCode : 'string',
			repairQuality : 'string'
		}
	},
	
	pendingAuditWorkOrderParts : {
		searchFields : {
			visitId : 'string',
			woRepair : 'boolean',
			accepted : 'boolean', 
			workOrderId : 'string',
			partIndex : 'integer',
			repairIndex : 'integer',
			addedPart : 'boolean',
			hasChildParts : 'boolean',
			prePartCode : 'string',
			postPartCode : 'string',
			preRepairCode : 'string',
			postRepairCode : 'string',
			preQuantity : 'string',
			postQuantity : 'string',
			preDamageCode : 'string',
			postDamageCode : 'string',
			preRepairLocCode : 'string',
			postRepairLocCode : 'string',
			rejectedPart : 'boolean',
			preTPI : 'string',
			postTPI : 'string',
			mode : 'string',
			repairDescription : 'string',
			partDescription : 'string',
			manHrs : 'string',
			partCost : 'string',
			materialCost : 'string',
			totalPerCode : 'string',
			repairQuality : 'string'
		}
	},
	
	//collection for the country cluster
	country:{
		searchFields : {
			rememberCountry : 'string',
			code : 'string'
		}
	},
	
	staticDataRefresh : {
		timestamp : 'integer'
	},
	
	codeDescriptionsRefresh : {
		timestamp : 'integer'
	},
	
	repairCodes : {
		searchFields : {
			code : 'string',
			mode : 'string'
		}
	},
	
	partCodes : {
		searchFields : {
			code : 'string'
		}
	},
	
	repairLocCodes : {
		searchFields : {
			code : 'string',
			description : 'string'
		}
	},
	
	damageCodes : {
		searchFields : {
			code : 'string',
			description : 'string'
		}
	},
	
	manualCodesList : {
		
	},
	
	partsGroupList : {
		
	},
	
	manufacturerCodesList : {
		
	},
	
	// System Info collections
	
	repairSTSCodes : {
//		manualCode : 'string',
//		modeDesc : 'string',
//		mode : 'string',
//		indexId : 'integer',
//		indexDesc : 'string',
//		repairCode : 'string',
//		repairDesc : 'string'
		// Add Manual code description! TODO
	},
	
	repairSTTCodesLoadded : {
		searchFields : {
			loaded : 'boolean'
		}
	},
	
	shopLimits : {
		
	},
	
	shopLimitsLoadded : {
		searchFields : {
			loaded : 'boolean'
		}
	},
	
	laborRates : {
		shopCode : 'string',
		customer : 'string',
		eqType : 'string',
		effDate : 'string',
		expDate : 'string',
		regRate : 'string',
		ot1Rate : 'string',
		ot2Rate : 'string',
		ot3Rate : 'string'
	},
	
	laborRatesLoaded : {
		searchFields : {
			loaded : 'boolean'
		}
	},
	
	shopInfo : {
		searchFields : {
			code : 'string'
		}
		// can add fields for reference - TODO
	},
	
	associatedParts : {
		// can add fields for reference - TODO
	},
	
	associatedPartsLoaded : {
		searchFields : {
			loaded : 'boolean'
		}
	},
	
	masterParts : {
		// can add fields for reference - TODO
		searchFields : {
			code : 'string'
		}
	},
	
	masterPartsLoaded : {
		searchFields : {
			manufacturer : 'string',
			partGroup : 'string'
		}
	},
	
	galleryPhoto : {
		searchFields : {
			path 	: 'string',
			name    : 'string',
			date 	: 'string',
			visitId : 'string',
			wono    : 'string'
		}
	},
	// Object that defines a collection of attachments
	attachment : {
		searchFields : {
			url : 'string',
			listId : 'integer'
		}
	},
	//Object that defines the settings collection which contains just one element
	settings : {
		searchFields : {
			remember : 'boolean',
		}
	},
	// country dropdown
	countries : {
		name : 'string',
		code : 'string'
	},
	//Object that defines repair shop depots
	repairShop : {
		searchFields : {
			code : 'string',
			name : 'string',
			location : 'string',
			country : 'string',
			cached : 'boolean',
			timestamp : 'string',
			storedInCollection : 'string'
		}
	},
	
	cacheCollectionsStatus : {
		searchFields : {
			storeNext : 'string'
		}
	},
	
	workOrders : {
		searchFields : {
			repairCost : 'string',
			containerNo : 'string',
			shopCode : 'string',
			auditDate : 'string',
			status : 'string',
			workOrderId : 'string',
			repairDate : 'string',
			locked : 'string',
			repairMode : 'string'
		}
	},
	
	cachedWorkOrders1 : {
//		searchFields : {
			repairCost : 'string',
			containerNo : 'string',
			shopCode : 'string',
			auditDate : 'string',
			status : 'string',
			workOrderId : 'string',
			repairDate : 'string',
			locked : 'string',
			repairMode : 'string',
			reapirDateTimeStamp : 'integer'
//		}
	},
	
	cachedWorkOrders2 : {
//		searchFields : {
			repairCost : 'string',
			containerNo : 'string',
			shopCode : 'string',
			auditDate : 'string',
			status : 'string',
			workOrderId : 'string',
			repairDate : 'string',
			locked : 'string',
			repairMode : 'string',
			reapirDateTimeStamp : 'integer'
//		}
	},
	
	cachedWorkOrders3 : {
//		searchFields : {
			repairCost : 'string',
			containerNo : 'string',
			shopCode : 'string',
			auditDate : 'string',
			status : 'string',
			workOrderId : 'string',
			repairDate : 'string',
			locked : 'string',
			repairMode : 'string',
			reapirDateTimeStamp : 'integer'
//		}
	},
	
	cachedWorkOrders4 : {
//		searchFields : {
			repairCost : 'string',
			containerNo : 'string',
			shopCode : 'string',
			auditDate : 'string',
			status : 'string',
			workOrderId : 'string',
			repairDate : 'string',
			locked : 'string',
			repairMode : 'string',
			reapirDateTimeStamp : 'integer'
//		}
	},
	
	cachedWorkOrders5 : {
//		searchFields : {
			repairCost : 'string',
			containerNo : 'string',
			shopCode : 'string',
			auditDate : 'string',
			status : 'string',
			workOrderId : 'string',
			repairDate : 'string',
			locked : 'string',
			repairMode : 'string',
			reapirDateTimeStamp : 'integer'
//		}
	},
	
	//Object that defines a list of manuals
	manual : {
		searchFields : {
			title : 'string',
			language : 'string',
			extension : 'string',
			serverPath : 'string',
			localPath : 'string',
			addedDate : 'string',
			attachmentListId : 'integer'
		}
	},
	
	utils : {
		searchFields : {
			photoCount : 'string'
		}
	},
	
	kpi : {
		searchFields : {
			totalCount : 'number',
			passedCount : 'number',
			rejectedCount : 'number'
		}
	},
	
	kpiMonthCount : {
		searchFields : {
			result : 'integer',
			year : 'integer',
			month : 'integer',
			inspectionsCount : 'integer'
		}
	},
	
	kpiShops : {
		searchFields : {
			SHOP_CD : 'string',
			LOC_DESC : 'string',
			SHOP_DESC : 'string'
		}
	},
	
	auditQueue : {
		searchFields : {
			visitId : 'string',
			timestamp : 'string'
		}
	},
	
	visitRefreshTimer : {
		searchFields : {
			timestamp : 'number'
		}
	},
	
	staticDataRefreshTimer : {
		searchFields : {
			timestamp : 'number'
		}
	},
	
	woMode : {
		searchFields : {
			mode : 'string'
		}
	},
	
	woStatus : {
		searchFields : {
			status : 'string'
		}
	}
};

JSONStoreDummyData = {
	
	countryData:[{
		rememberCountry:'Please select a country',
		code:''
	}],
		
	userData : [ {
		username : 'maersk',
		token : 'efaedba4-48cf-9a23-2c31-d647a63555a5',
		certificateId: '',
		lastLoginTime : ''
	}],
	
	settingsData : [{
		remember : false
	}],
	
	attachmentData : [{
		url : '/nowhere/test_url',
		listId : 10
	}],
	
	manualData : [ {
		manualId : 1000,
		title : 'Auditing for dummies',
		addedDate : '09-11-2014',
	},
	{
		manualId : 1001,
		title : 'Auditing for dummies 2nd vol',
		addedDate : '09-11-2014',
	},
	{
		manualId : 1002,
		title : 'Maersk Line Equipment guide',
		addedDate : '08-12-2014',
	},
	{
		manualId : 1003,
		title : 'User Guide to New Maersk Line',
		addedDate : '01-04-2015',
	},
	{
		manualId : 1004,
		title : 'E-Auction – Supplier guide - Maersk',
		addedDate : '01-04-2015',
	}],
}
