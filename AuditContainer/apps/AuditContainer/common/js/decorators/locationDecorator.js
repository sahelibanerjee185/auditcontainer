/**
 * Dec 8, 2015
 * locationDecorator.js
 * StavaracheVlad
 */

audit.config([
    '$provide', function ($provide) {
    	 $provide.decorator('$location', ['$delegate', function($delegate){
    		
    		 $delegate.switchTab = function(path){
    			 
    			 console.log('hybrid.getTabObject()', hybrid.getTabObject());
    			 this.path(path);
    			 var pathArray = path.split('/');
    			 var simplePath = (pathArray[0] !== "") ? pathArray[0] : pathArray[1];
    			 var currentTabIndex = hybrid.getTabObject()[Messages.locationPathIndex[simplePath]];
    			 hybrid.switchToolbarButton(currentTabIndex);
    			 
    		 }
    		 return $delegate;
    	 }])
    }
])