/**
 * Apr, 7 2016
 * Filters.js
 * Stavarache Vlad
*/

/**
 * countryFilter Filter
 * @namespace Filters
 */
(function (audit, angular) {
	'use strict';

    /**
     * @namespace countryFilter
     * @desc Filter used to search through country dropdown in plan visit screen
     * @memberOf Filters
     */
	audit.filter('countryFilter', countryFilter);

    function countryFilter() {
		return function (countryArray, searchString) {
			var filteredCountries = [{name : 'All Countries', code : null}];
			  
			if (typeof searchString != 'string' || (searchString.trim().length == 0)) {
				return filteredCountries;
			}
			  
			angular.forEach(countryArray, function(country) {
				if (defaultSearch(country.name, searchString)) {
					filteredCountries.push(country);
				}
			});
			
			return filteredCountries;
		}
	};
	
	function defaultSearch(actual, expected) {
		var lowerStr = (actual + "").toLowerCase();
		return lowerStr.indexOf(expected.toLowerCase()) >= 0;
	}
	
})(audit, angular);