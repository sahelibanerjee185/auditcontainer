/**
 * @namespace PdfCreator
 * @desc Service used to handle PDF creation
 * @memberOf Factories
 */
var nativePdfPromise = $.when({});
(function(angular, WL, $) {
  //'use strict';

  angular.module("reporting").factory("PdfCreator", PdfCreator);

  PdfCreator.$inject = ["HtmlCompiler"];

  function PdfCreator(HtmlCompiler) {
    var exports = {
      
    /**
    * @name generatePdf
    * @desc Compile HTML to generate PDF for a specific platform 
    * @param {Object} options compile options
    * @param {Integer} visitId unique identifier for an existing visit
    * @param {Boolean} pdfPreview view pdf in preview mode
    * @returns {Promise} - generated pdf from native
    * @memberOf Factories.pdfGenerationHandler
    */
    generatePdf: function(options, visitId, pdfPreview) {
        var defer = $.Deferred();
        HtmlCompiler.compile(options).then(
          function(reportHtml) {
            if (WL.Client.getEnvironment() === WL.Environment.ANDROID) {
              return generatePdfAndroid(reportHtml);
            } else if (
              WL.Client.getEnvironment() === WL.Environment.IPAD ||
              WL.Client.getEnvironment() === WL.Environment.IPHONE
            ) {
              generatePdfIos(reportHtml, visitId, pdfPreview)
                .then(function(data) {
                  defer.resolve(data);
                })
                .fail(function(error) {
                  defer.reject(error);
                });
            } else {
              var uri = "data:text/html," + encodeURIComponent(reportHtml);
              var newWindow = window.open(uri);
            }
          },
          function(error) {
            console.log("Error generating report: " + error);
            defer.reject("Could not generate PDF " + JSON.stringify(error));
          }
        );

        return defer.promise();
      }
    };
    return exports;

    //NOT USED
    function generatePdfAndroid(reportHtml) {
      return cordova.exec(
        function() {},
        function(error) {
          console.log("Error printing" + error);
        },
        "Printer",
        "print",
        [reportHtml, {}]
      );
    }

    /**
    * @name generatePdfIos
    * @desc Native PDF generate for iOS - send html2pdf to native and wait for response 
    * @param {Object} reportHtml report to be converted
    * @param {Integer} visitId unique identifier for an existing visit
    * @param {Boolean} pdfPreview view pdf in preview mode
    * @returns {Promise} - generated pdf from native
    * @memberOf Factories.pdfGenerationHandler
    */
    function generatePdfIos(reportHtml, visitId, pdfPreview) {
      if (pdfPreview === true) {
        var pdfName = "preview-";
      } else {
        var pdfName = "";
      }
      nativePdfPromise = $.Deferred();
      WL.App.sendActionToNative("html2pdf", {
        html: reportHtml,
        pdfPath: "PdfReports/" + visitId,
        pdfName: pdfName + "audit-report-" + visitId + ".pdf"
        // pdfPath: documentsPath+"Documents/audit-report-test.pdf"
      });
      return nativePdfPromise.promise();
    }
  }
})(angular, WL, $);
