(function (angular, document, cordova) {
    'use strict';
    angular
        .module('reporting')
        .directive('printLauncher', PrintLauncher);

    PrintLauncher.$inject = [];

    function PrintLauncher() {
        return {
            restrict: 'A',
            scope: {
                printElementId: '@',
            },
            link: function (scope, element) {
                console.log('elementId = ' + scope.elementId);

                element.bind('click', function () {
                    //TODO: in progress
                    var head = document.getElementsByTagName("head")[0];
              
                    var content = document.getElementById(scope.printElementId);

                    var printContent = head.innerHTML + content.innerHTML;

                    console.log(printContent);
                    cordova.exec(function () {}, function (error) {
                            console.log('Error printing' + error);
                        },
                        'Printer', 'print', [printContent, {}]
                    );
                });
            }
        };
    }
})(angular, document, cordova);