/**  Created by andrewradulescu on 2/21/17. */
/**
 * @namespace PdfService
 * @desc Service used to handle PDF upload/download
 * @memberOf Factories
 */

(function () {
    'use strict';

    audit.factory('pdfService', pdfService);

    pdfService.$inject = [];

    function pdfService() {

        //Define factory methods
        var factory = {
            uploadPDF: uploadPDF,
            downloadPDF: downloadPDF
        };

        //Returning the factory as an object
        return factory;

        /**
		* @name downloadPDF
		* @desc Call the PdfArchiveAdapter with visitId to retrieve the file.
        Adapter will build the PATH to the file with params. (pay attention)
        Returns a promise with the file (as Base64) or error
        * @param {Integer} visitId unique identifier for an existing visit
		* @returns {Function} 
		* @memberOf Factories.pdfService
		*/
        function downloadPDF(visitId) { //FileName should contain either name and extension (visitID.pdf)

            var defer = $.Deferred();

            WL.Client.invokeProcedure({
                adapter: 'PdfArchiveAdapter',
                procedure: 'downloadPDF',
                parameters: [visitId]
            }, {
                onSuccess: function (response) {
                    successHandler(response, defer);
                },
                onFailure: function (error) {
                    errorHandler(error, defer);
                }
            });
            return defer.promise();
        };

        /**
		* @name uploadPDF
		* @desc Call PdfArchiveAdapter with visitId and fileData to upload the file on server
        * @param {Integer} visitId unique identifier for an existing visit
        * @param {String} fileData base64 encoded string representing the PDF file
		* @returns {Function} - standard success/fail function
		* @memberOf Factories.pdfService
		*/
        function uploadPDF(visitId, fileData) {

            var defer = $.Deferred();

            console.log('Uploading PDF with name '  + visitId + ".pdf");

            WL.Client.invokeProcedure({
                adapter: 'PdfArchiveAdapter',
                procedure: 'uploadPDF',
                parameters: [visitId, fileData]
            }, {
                onSuccess: function (response) {
                    successHandler(response, defer);
                },
                onFailure: function (error) {
                    errorHandler(error, defer);
                }
            });

            return defer.promise();

        };

        //Standard success handler
        function successHandler(response, defer) {
            console.log('response', response);
            if (!response.invocationResult.isSuccessful) {
                defer.reject(response.invocationResult);
            } else {
                var resultSet = response.invocationResult.invocationResult;
                defer.resolve(resultSet);
            }
        }

        //Standard fail handler
        function errorHandler(error, defer) {
            console.log('error', error);
            defer.reject(error);
        }

    }

})();