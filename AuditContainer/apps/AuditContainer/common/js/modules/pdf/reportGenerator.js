(function (angular) {
    'use strict';
    angular
        .module('reporting')
        .factory('reportGenerator', reportGenerator);

    reportGenerator.$inject = ['PdfCreator', 'CheckListUtils'];

    function reportGenerator(PdfCreator, CheckListUtils) {
        var exports = {
            generate: function (context, visitId, pdfPreview) {
                return _generate(context, visitId, pdfPreview);
            }
        };

        return exports;

        function _generate(context, visitId, pdfPreview) {
            var data = _buildTemplateData(context);
            
            var options = {
            	templates : getTemplatePaths(context),
            	data : data
            }
            return PdfCreator.generatePdf(options, visitId, pdfPreview);
        }

        function _buildTemplateData(context) {
            return context;
        }

        function getTemplatePaths(context) {
        	var templatePaths = ["templates/pdf-reports/pdf-report.html"];
        	return templatePaths;
        }
    }
})(angular);