(function (angular, Handlebars) {
    //'use strict';
    angular
        .module('reporting')
        .factory('HtmlCompiler', HtmlCompiler);

    HtmlCompiler.$inject = ['$templateRequest', '$filter', '$location', '$q', '$http', '$templateCache'];

    function HtmlCompiler($templateRequest, $filter, $location, $q, $http, $templateCache) {
        var exports = {
            compile: function (options) {
                return compile(options);
            }
        };

        config($filter);

        return exports;

        function config() {
            Handlebars.registerHelper("formatDate", function (input) {
                if (input === null|| input === undefined) return "";

                var _date = $filter('date')(new Date(input), 'dd MMM yyyy')

                return _date;
            });

            Handlebars.registerHelper("formatPrecision2", function (input) {
                if (input === null || input === undefined) return "0.00";

                return Number(input).toFixed(2);
            });

            Handlebars.registerHelper('if_eq', function (a, b, opts) {
                if (a == b) // Or === depending on your needs
                    return opts.fn(this);
                else
                    return opts.inverse(this);
            });

            Handlebars.registerHelper('treeEach', function (context, options) {
                if (!options) {
                    throw new _exception2['default']('Must pass iterator to #each');
                }

                var fn = options.fn,
                    inverse = options.inverse,
                    i = 0,
                    ret = '',
                    data = undefined,
                    contextPath = undefined;

                if (options.data && options.ids) {
                    contextPath = Handlebars.Utils.appendContextPath(options.data.contextPath, options.ids[0]) + '.';
                }

                if (Handlebars.Utils.isFunction(context)) {
                    context = context.call(this);
                }

                if (options.data) {
                    data = Handlebars.Utils.createFrame(options.data);
                }

                function execIteration(field, index, last) {
                    if (data) {
                        data.key = field;
                        data.index = index;
                        data.first = index === 0;
                        data.last = !!last;

                        if (contextPath) {
                            data.contextPath = contextPath + field;
                        }
                    }

                    ret = ret + fn(array[field], {
                        data: data,
                        blockParams: Handlebars.Utils.blockParams([array[field], field], [contextPath + field, null])
                    });
                }

                if (context && typeof context === 'object') {
                    var array = [];
                    if (!Handlebars.Utils.isArray(context)) {
                        for (var objectKey in context) {
                            if (context[objectKey].id) {
                                array.push(context[objectKey]);
                            }
                        }
                    } else {
                        array = context;
                    }


                    array.sort(function (a, b) {
                        if (a.order === undefined) {
                            return 1;
                        }
                        if (b.order === undefined) {
                            return -1;
                        }

                        a = parseInt(a.order);
                        b = parseInt(b.order);
                        return a - b;
                    });


                    for (i = 0; i < array.length; i++) {
                        execIteration(i, i, i === array.length - 1);
                    }
                }

                if (i === 0) {
                    ret = inverse(this);
                }

                return ret;
            });


            Handlebars.registerHelper('objectEach', function (context, options) {
                if (!options) {
                    throw new _exception2['default']('Must pass iterator to #each');
                }

                var fn = options.fn,
                    inverse = options.inverse,
                    i = 0,
                    ret = '',
                    data = undefined,
                    contextPath = undefined;

                if (options.data && options.ids) {
                    contextPath = Handlebars.Utils.appendContextPath(options.data.contextPath, options.ids[0]) + '.';
                }

                if (Handlebars.Utils.isFunction(context)) {
                    context = context.call(this);
                }

                if (options.data) {
                    data = Handlebars.Utils.createFrame(options.data);
                }

                function execIteration(field, index, last) {
                    if (data) {
                        data.key = field;
                        data.index = index;
                        data.first = index === 0;
                        data.last = !!last;

                        if (contextPath) {
                            data.contextPath = contextPath + field;
                        }
                    }

                    ret = ret + fn(array[field], {
                        data: data,
                        blockParams: Handlebars.Utils.blockParams([array[field], field], [contextPath + field, null])
                    });
                }

                if (context && typeof context === 'object') {
                    var array = [];
                    if (!Handlebars.Utils.isArray(context)) {
                        for (var objectKey in context) {
                            array.push(context[objectKey]);
                        }
                    } else {
                        array = context;
                    }


                    array.sort(function (a, b) {
                        if (a.order === undefined) {
                            return 1;
                        }
                        if (b.order === undefined) {
                            return -1;
                        }

                        a = parseInt(a.order);
                        b = parseInt(b.order);
                        return a - b;
                    });


                    for (i = 0; i < array.length; i++) {
                        execIteration(i, i, i === array.length - 1);
                    }
                }

                if (i === 0) {
                    ret = inverse(this);
                }

                return ret;
            });
            
            Handlebars.registerHelper('ifCond', function (v1, operator, v2, options) {

                switch (operator) {
                    case '==':
                        return (v1 == v2) ? options.fn(this) : options.inverse(this);
                    case '===':
                        return (v1 === v2) ? options.fn(this) : options.inverse(this);
                    case '!=':
                        return (v1 != v2) ? options.fn(this) : options.inverse(this);
                    case '<':
                        return (v1 < v2) ? options.fn(this) : options.inverse(this);
                    case '<=':
                        return (v1 <= v2) ? options.fn(this) : options.inverse(this);
                    case '>':
                        return (v1 > v2) ? options.fn(this) : options.inverse(this);
                    case '>=':
                        return (v1 >= v2) ? options.fn(this) : options.inverse(this);
                    case '&&':
                        return (v1 && v2) ? options.fn(this) : options.inverse(this);
                    case '||':
                        return (v1 || v2) ? options.fn(this) : options.inverse(this);
                    default:
                        return options.inverse(this);
                }
            });
        }

        function compile(options) {
            var deferred = $.Deferred();

            var appPath = "";
            if (typeof (cordova) !== "undefined") {
                appPath = cordova.file.applicationDirectory + 'www/default/';
            } else {
                appPath = $location.absUrl().split('index.html')[0];
            }
            console.log('App directory: ' + appPath);

            var data = options.data;
            data.appPath = appPath.replace('file://', '');

            var paths = options.templates;

            $q.all(paths.map(function(data) {
            	return $http.get(data, {cache: $templateCache})
        	}))
                .then(function (values) {
                        var allContent = "";
                        for (i in values) {
                            var template = values[i].data;
                            var content = Handlebars.compile(template)(data);
                            allContent += content;
                            // console.log('Compiled content is  ' + content);
                        }
                        deferred.resolve(allContent);
                    },
                    function (error) {
                        console.log('Erro fetching report template: ' + error);
                        deferred.reject(error);
                    });
            return deferred.promise();
        }
    }
})(angular, Handlebars);