/**
 * May, 16 2017
 * pdfGenerationHandler.js
 * Florian Birloi
 */
/**
 * @namespace PdfGenerationHandler
 * @desc Service used to handle PDF operations
 * @memberOf Factories
 */

(function () {
    'use strict';

    audit.factory('pdfGenerationHandler',pdfGenerationHandler);
    
    pdfGenerationHandler.$inject = ['reportGenerator','multiModal','$rootScope','$ionicLoading','pdfModule'];
	
	function pdfGenerationHandler(reportGenerator, multiModal, $rootScope, $ionicLoading, pdfModule) {

        var obj = {

        }

        /**
		* @name generatePdf
		* @desc Generate PDF file using user's signatures and visit's data
        * @param {Object} signatures user's signatures
        * @param {Boolean} pdfPreview view pdf in preview mode
        * @param {Integer} visitId unique identifier for an existing visit
		* @returns {Promise} - true - pdf generated
		* @memberOf Factories.pdfGenerationHandler
		*/
        function generatePdf(signatures ,pdfPreview, visitId) {
            var dfd = $.Deferred();

            obj = generatePdfModel(obj);
            obj.startedVisitInfo.signatures = signatures;
            
            if(visitId){
                obj.startedVisitInfo.visitId = visitId;
            }
            
            if (pdfPreview === true) {
                obj.pdfPreview = 'pdfPreview';
            } else {
                obj.pdfPreview = '';
            }

            setTimeout(function () {
                reportGenerator.generate(obj, obj.startedVisitInfo.visitId, false).then(function () {
                    dfd.resolve(true);
                }).fail(function (error) {
                    dfd.reject(error);
                    WL.Logger.error("[pdfGenerationHandler][generatePdf][reportGenerator.generate] username: " + userContext.getUsername() + " " + JSON.stringify(error));
                });
            }, 200);

            return dfd.promise();
        };

        /**
		* @name generatePdfModel
		* @desc Generate PDF model attaching properties to visit object
        * @param {Object} obj containing visit data
		* @returns {Object} 
		* @memberOf Factories.pdfGenerationHandler
		*/
        function generatePdfModel(obj) {
            var aWoTypes = ["plannedWorkOrders", "startedWorkOrders", "passedWorkOrders", "failedWorkOrders"];

            // Iterate through work order types
            angular.forEach(aWoTypes, function (woType, key) {
                // Check if woType exists in started info object
                if (obj.startedVisitInfo[woType] && obj.startedVisitInfo[woType].length > 0) {
                    // Iterate through visit info by work order type
                    angular.forEach(obj.startedVisitInfo[woType], function (wo, woKey) {
                        obj.startedVisitInfo[woType][woKey]['parts'] = new Array();
                        if (obj.partsObject[wo.workorderid]) {
                            obj.startedVisitInfo[woType][woKey]['parts'] = obj.partsObject[wo.workorderid];
                        }
                    });
                }
            });
            
            return obj;
        }
        
        /**
		* @name deletePdf
		* @desc Call deletePdf from native to remove the current pdf from device
		* @returns {Promise} - resolved after deletion is finished
		* @memberOf Factories.pdfGenerationHandler
		*/
        function deletePdf() {
            nativePdfPromise = $.Deferred();
            WL.App.sendActionToNative("deletePdf", {
                pdfPath: userContext.getPdfPath()
            });
            return nativePdfPromise.promise();
        };
        
         /**
		* @name openPreviewPdf
		* @desc Open PDF in preview mode from native 
        * @param {Integer} visitId unique identifier for a visit 
        * @param {String} pdfName to be opened
		* @returns {Promise} - resolved after preview is opened
		* @memberOf Factories.pdfGenerationHandler
		*/
        function openPreviewPdf(visitId, pdfName) {
            nativePdfPromise = $.Deferred();
            WL.App.sendActionToNative("previewPdf", {
                pdfPath: "PdfReports/" + visitId + "/" + pdfName + "audit-report-" + visitId + ".pdf"
            });
            return nativePdfPromise.promise();
        };

        function setPdfContext(context) {
            obj = context;
        }
        
        /**
		* @name previewPdf
		* @desc Open PDF in preview mode with dummy signatures if pdfPreview is true
        * @param {Integer} visitId unique identifier for a visit 
        * @param {Boolean} pdfPreview true for dummy signature
		* @returns {Void}
		* @memberOf Factories.pdfGenerationHandler
		*/
        function previewPdf(visitId, pdfPreview) {
            if (pdfPreview === true) {
                var pdfName = 'preview-';
                $ionicLoading.show(loaderConfig);
                generatePdf('previewDummySignature', true, visitId).then(function () {
                    $ionicLoading.hide();
                    openPreviewPdf(visitId, pdfName);
                });
            } else {
                var pdfName = '';
                openPreviewPdf(visitId, pdfName);
            }
        };
        
        /**
		* @name uploadPDF
		* @desc Generate PDF with all data and upload it on server
        * @param {Integer} visitId unique identifier for a visit 
		* @returns {Promise}
		* @memberOf Factories.pdfGenerationHandler
		*/
        function uploadPDF(visitId){
            
            var defer = $.Deferred();
            
            generatePdf(obj.startedVisitInfo.signatures, false).then(function () {
			    getFileContentAsBase64(userContext.getPdfPath(), function (base64String) {
			        pdfModule.uploadPDFOnServer(visitId, base64String).then(function (result) {
			            defer.resolve();
			        }).fail(function(error){
                        defer.reject(error);
                        WL.Logger.error("[pdfGenerationHandler][uploadPDF][pdfModule.uploadPDFOnServer] username: " + userContext.getUsername() + " " + JSON.stringify(error));
                    });
			    }, function (error) {
			        WL.Logger.error("[pdfGenerationHandler][uploadPDF][getFileContentAsBase64] username: " + userContext.getUsername() + " " + JSON.stringify(error));
			        defer.reject(error)
			    });
			}).fail(function () {
                WL.Logger.error("[pdfGenerationHandler][uploadPDF][generatePdf] username: " + userContext.getUsername() + " " + JSON.stringify(error));
			    defer.reject();
			});
            
            return defer.promise();

        }

        return {
            previewPdf : previewPdf,
            deletePdf : deletePdf,
            generatePdfModel : generatePdfModel,
            generatePdf : generatePdf,
            setPdfContext : setPdfContext,
            uploadPDF : uploadPDF
        }

    }

}());