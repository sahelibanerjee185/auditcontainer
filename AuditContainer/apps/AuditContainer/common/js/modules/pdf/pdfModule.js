/** Created by andrewradulescu on 2/21/17. */
/**
 * @namespace PdfModule
 * @desc Service used to handle PDF upload/download
 * @memberOf Factories
 */
(function () {
    'use strict';

    audit.factory('pdfModule', pdfModule);

    pdfModule.$inject = ['pdfService'];

    function pdfModule(pdfService) {

        var factory = {
            uploadPDFOnServer: uploadPDFOnServer,
            downloadPDFFromServer: downloadPDFFromServer,
            uploadPendingPDF: uploadPendingPDF
        };

        return factory;

        /**
		* @name downloadPDFFromServer
		* @desc Download the PDF file from server as base64 and save it locally based on an exiting visit
        * @param {Integer} visitId unique identifier for an existing visit
		* @returns {Promise} 
		* @memberOf Factories.pdfModule
		*/
        function downloadPDFFromServer(visitId) {

            var defer = $.Deferred();

            pdfService.downloadPDF(visitId)
                .then(function (pdfFile) {
                    //Folder path where to save file
                    var folderPath = cordova.file.applicationStorageDirectory + "Documents";
                    //Folder name
                    var folderName = visitId+"";
                    //File name
                    var fileName = "audit-report-" + visitId + ".pdf";
                    //Split base64 string to get contentType and content
                    var block = pdfFile.split(";");
                    //Get the contentType
                    var contentType = block[0].split(":")[1];
                    //Get clean base64 content
                    var base64Content = block[1].split(",")[1];
                    //Save the base64 as PDF in the specified folder
                    savebase64AsPDF(folderPath, folderName, fileName, base64Content, contentType)
                        .then(function(){
                            defer.resolve();
                        }).fail(function(err){
                            defer.reject(err);
                        });

                })
                .fail(function (error) {
                    console.log('Error downloading PDF from server');
                    defer.reject('Error downloading PDF from server');
                })

            return defer.promise();
        };

        /**
		* @name uploadPDFOnServer
		* @desc Upload PDF on server as base64 encoded string 
        * @param {Integer} visitId unique identifier for an existing visit
        * @param {String} fileData base64 encoded string
		* @returns {Promise} 
		* @memberOf Factories.pdfModule
		*/
        function uploadPDFOnServer(visitId, fileData) {
            var defer = $.Deferred();
            
            if (typeof fileData == 'undefined' || fileData == 'ERROR') {
            	defer.resolve();
            	return defer.promise();
            }
            
            pdfService.uploadPDF(visitId, fileData)
                .then(function (success) {
                    console.log('PDF Uploaded with success!');
                    defer.resolve(success);
                })
                .fail(function (error) {
                    console.log('Failed to upload PDF on server');
                    defer.reject('Failed to upload PDF on server');
                });

            return defer.promise();
        };

        
        /**
		* @name uploadPendingPDF
		* @desc Upload PDF when Sync offline visits 
        * Using the VisitID will retrieve the pdf from device (as base64) and upload it to server
        * @param {Integer} visitId unique identifier for an existing visit
		* @returns {Promise} 
		* @memberOf Factories.pdfModule
		*/
        function uploadPendingPDF(visitId) {

            var defer = $.Deferred();

            var absolutePath = cordova.file.applicationStorageDirectory + "Documents/PdfReports/"+visitId+"/audit-report-" + visitId + ".pdf";

            //Remove file:// from the beginning
            var path = absolutePath.substring(7,absolutePath.length);

            //Get pdf as base64
            getFileContentAsBase64(path, function (base64PdfString) {

                if (base64PdfString == undefined || base64PdfString.length == 0) {
                    console.log("PDF Couldn't be retrieved from device");
                    defer.resolve();
                }else if(base64PdfString == "ERROR"){
                    WL.Logger.error("[PdfModule][uploadPendingPDF] username: " + userContext.getUsername() + 'PROBLEM WHEN TRYING TO GET PDF AS BASE64');
                    defer.reject("PROBLEM WHEN TRYING TO GET PDF AS BASE64");
                }

                //Upload PDF on server
                uploadPDFOnServer(visitId, base64PdfString).then(function (result) {
                    console.log('PDF Successfully uploaded to server with payload ' + result);
                    defer.resolve()
                }).fail(function(error){
                    WL.Logger.error("[PdfModule][uploadPendingPDF] username: " + userContext.getUsername() + JSON.stringify(error));
                    defer.reject(error);
                })
            });

            return defer.promise();
        };

        /**
		* @name uploadPendingPDF
		* @desc Create a PDF file according to its data base64 content only.
        * @param folderpath {String} The folder where the file will be created
        * @param filename {String} The name of the file that will be created
        * @param content {Base64 String} Important : The content can't contain the following string 
        * (data:application/pdf;base64). Only the base64 string is expected.
        * @param contentType {String} the content type of the file i.e (application/pdf - text/plain)
		* @returns {Promise} 
		* @memberOf Factories.pdfModule
		*/
        function savebase64AsPDF(folderpath, foldername, filename,content,contentType){
            var defer = $.Deferred();

            // Convert the base64 string in a Blob
            var DataBlob = b64toBlob(content,contentType);

            console.log("Starting to write the file :3");

            window.resolveLocalFileSystemURL(folderpath, function(dir) {
                dir.getDirectory("PdfReports", {create: true}, function (dirDocuments) {
                    dirDocuments.getDirectory(foldername, {create: true}, function (dirEntry) {
                        console.log("Access to the directory granted succesfully");
                        dirEntry.getFile(filename, {create: true}, function (file) {
                            console.log("File created succesfully.");
                            file.createWriter(function (fileWriter) {
                                console.log("Writing content to file");
                                fileWriter.write(DataBlob);

                                defer.resolve();
                            }, function () {
                                alert('Unable to save file in path ' + folderpath);
                                defer.reject('Unable to save file in path ' + folderpath);
                            });
                        });
                    });
                });
            },function(err){
            	console.log(err);
            	defer.reject(err);
            });

            return defer.promise();
        };

        /**
         * Convert a base64 string in a Blob according to the data and contentType.
         *
         * @param b64Data {String} Pure base64 string without contentType
         * @param contentType {String} the content type of the file i.e (application/pdf - text/plain)
         * @param sliceSize {Int} SliceSize to process the byteCharacters
         * @see http://stackoverflow.com/questions/16245767/creating-a-blob-from-a-base64-string-in-javascript
         * @return Blob
         */
        function b64toBlob(b64Data, contentType, sliceSize) {
            contentType = contentType || '';
            sliceSize = sliceSize || 512;

            var byteCharacters = atob(b64Data);
            var byteArrays = [];

            for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
                var slice = byteCharacters.slice(offset, offset + sliceSize);

                var byteNumbers = new Array(slice.length);
                for (var i = 0; i < slice.length; i++) {
                    byteNumbers[i] = slice.charCodeAt(i);
                }

                var byteArray = new Uint8Array(byteNumbers);

                byteArrays.push(byteArray);
            }

            var blob = new Blob(byteArrays, {type: contentType});
            return blob;
        };
    };
})();