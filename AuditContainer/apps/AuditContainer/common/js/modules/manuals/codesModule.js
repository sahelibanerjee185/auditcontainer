
/* JavaScript content from js/modules/manuals/codesModule.js in folder common */
audit.factory('$codesModule', function($localDB, $filter, WOCodesService, $ionicLoading){

	var obj = {
		results : [],
		entriesPerPage : 0,
		searchFields : [],
		filteredLength : 0,
		sortingOptions : {
			fieldName : null,
			ascending : true,
			sortingNeeded : false
		},
		preFilterSettings : {}
	}

	function setEntriesPerPage(entriesPerPage){
		obj.entriesPerPage = entriesPerPage;
	}
	
	function setManualCode(newValue) {
		obj.preFilterSettings.manualCode = newValue;
	}
	
	function setPartsGroupCode(newValue) {
		obj.preFilterSettings.partGroup = newValue;
	}
	
	function setManufacturerCode(newValue) {
		obj.preFilterSettings.manufacturer = newValue;
	}
	
	function resetPrefilterSettings() {
		obj.preFilterSettings = {};
	}
	
	function setModeCode(newValue) {
		obj.preFilterSettings.mode = newValue;
	}
	
	function getSortingOrder() {
		return obj.sortingOptions.ascending;
	}
	
	function setSortingNeeded(newValue) {
		obj.sortingOptions.sortingNeeded = newValue;
	}
	
	function setCurrentResults(currentPage, searchString) {
//		$ionicLoading.show(loaderConfig);
		var filteredResults = obj.results; // filter dataSource before pagination
		var filterObject = {};
		
		if (obj.sortingOptions.sortingNeeded) {
			obj.results = getSortedResults(filteredResults, obj.sortingOptions);
			obj.sortingOptions.sortingNeeded = false;
		}
		
		if (obj.preFilterSettings.manualCode != null) {
            filterObject.manualCode = obj.preFilterSettings.manualCode;
        }
		
		if (obj.preFilterSettings.mode != null) {
            filterObject.mode = obj.preFilterSettings.mode;
        }
		
		if (obj.preFilterSettings.partGroup != null) {
			filterObject.partGroup = obj.preFilterSettings.partGroup;
		}
		
		if (obj.preFilterSettings.manufacturer != null) {
			filterObject.manufacturer = obj.preFilterSettings.manufacturer;
		}

        if (!angular.equals({}, filterObject)) {
            filteredResults = $filter('filter')(filteredResults, filterObject, true);
        }

		if (searchString) {
            // Check what searchFields are being passed - reduce to minimum and test how it performs for master parts!
			filteredResults = $filter('multiSearch')(filteredResults, obj.searchFields, searchString);
		}
		
		obj.filteredLength = filteredResults.length;

		var currentResults  = setPageEntries(currentPage,filteredResults,obj.entriesPerPage);
//		setTimeout(function(){
//			$ionicLoading.hide();
//		});
		return currentResults;
	}

	function setPageEntries(currentPage, filteredResults) {
		var offset = (currentPage - 1) * obj.entriesPerPage;

		return filteredResults.slice(offset, offset + obj.entriesPerPage);
	}

	function sortEntries(fieldName, currentPage, searchString) {
		if (obj.sortingOptions.fieldName == fieldName) {
			obj.sortingOptions.ascending = !obj.sortingOptions.ascending;
		} else {
			obj.sortingOptions.fieldName = fieldName;
			obj.sortingOptions.ascending = true; // ascending first
		}
		
		obj.sortingOptions.sortingNeeded = true;

		return setCurrentResults(1, searchString);
	}

	function resetSorting() {
		obj.sortingOptions = {
				fieldName : null,
				ascending : true
		};
	}

	function ascSorting(fieldName) {
		obj.sortingOptions = {
				fieldName : fieldName,
				ascending : false
		};
	}

	function getSortedResults(results, sortingOptions) {
		if (sortingOptions.fieldName === null) {
			return results;
		}

		var sortedResults = results;
		var fieldName = sortingOptions.fieldName;
		var sortOrder = sortingOptions.ascending;

		sortedResults = $filter('orderBy')(sortedResults, fieldName, sortOrder);

		return sortedResults;
	}

	function getDataArrayLength(){
		return obj.filteredLength;
	}

	function getSearchFields(){
		if(obj.results.length > 0){
			return Object.keys(obj.results[0]);
		}

		return null;
	}

	function setSearchFieldsForShop(){
		obj.searchFields = ['code'];
	}

	function resetCodeData(){
		obj.results = [];
		obj.searchFields = []
		obj.filteredLength = 0;
	}

	function getDamageCodeData(){

		var dfd = $.Deferred();

		WOCodesService.getDamageCodesInfo().then(function(data){
			buildCodeObject(data);
			removeSearchFieldByName('chts_ts');
			dfd.resolve();

		}).fail(function(e){
			dfd.reject(e);
		});

		return dfd.promise();
	}

	function getRepairLocCodesData(){

		var dfd = $.Deferred();
		WOCodesService.getRepairLocCodesInfo().then(function(data){
			buildCodeObject(data);
			dfd.resolve();

		}).fail(function(e){
			dfd.reject(e);
		});

		return dfd.promise();
	}

	function getShopInfoData(){

		var dfd = $.Deferred();

		WOCodesService.getShopsInfo().then(function(data){
			dfd.resolve();
		}).fail(function(e){
			dfd.reject(e);
		});

		return dfd.promise();
	}

	function getShopDataByShopCode(shopCode){
		var dfd = $.Deferred();

		$localDB.find('shopInfo', { code : shopCode }, {exact : true }).then(function(data){
			var parsedResults = data.map(function(entry){
				if (entry.json) return entry.json;
			});

			buildCodeObject(parsedResults);
			dfd.resolve(parsedResults);
		}).fail(function(e){
			dfd.reject(e);
		})

		return dfd.promise();
	}

	function getMasterPartsData(){
		var dfd = $.Deferred();

		WOCodesService.getMasterPartCodes().then(function(data){
			// console.log(data);
			buildCodeObject(data);

			// getDropdownData('masterParts', 'manufacturer').then(function(dropData){
				// obj.dropdownOptions.manufacturer = dropData;
				// getDropdownData('masterParts', 'partGroup').then(function(dropData){
					// obj.dropdownOptions.partGroup = dropData;
					dfd.resolve();
				// });
			// });

		}).fail(function(e){
			dfd.reject(e);
		});

		return dfd.promise();
	}
	
	function getRepairSTSCodesData() {
		var dfd = $.Deferred();
		
		WOCodesService.getRepairSTSCodes().then(function(data){
			buildCodeObject(data);
			dfd.resolve();
		}).fail(function(e){
			dfd.reject(e);
		});

		return dfd.promise();
	}
	
	function getAssociatePartsWithCodesData() {
		var dfd = $.Deferred();
		
		WOCodesService.getAssociatedPartsWithCodes().then(function(data){
			buildCodeObject(data);
			dfd.resolve();
		}).fail(function(e){
			dfd.reject(e);
		});

		return dfd.promise();
	}
	
	function getShopLimits() {
		var dfd = $.Deferred();
		
		WOCodesService.getShopLimits().then(function(data){
			buildCodeObject(data);
			dfd.resolve();
		}).fail(function(e){
			dfd.reject(e);
		});

		return dfd.promise();
	}
	
	function getLaborRates() {
		var dfd = $.Deferred();
		
		WOCodesService.getLaborRates().then(function(data){
			buildCodeObject(data);
			dfd.resolve();
		}).fail(function(e){
			dfd.reject(e);
		});

		return dfd.promise();
	}

	function buildCodeObject(data){
		resetCodeData();
		obj.results = data;
		obj.filteredLength = data.length;
		obj.searchFields = getSearchFields();
	}
	
	function removeSearchFieldByName(sName){
		for(var i = 0;i<obj.searchFields.length;i++){
			if(obj.searchFields[i] == sName){
				obj.searchFields.splice(i,1);
				break;
			}
		}
	}

	function getDropdownData(collectionName, propertyName){
		var dfd = $.Deferred();

		var options = {
				filter : [propertyName]
		};

		$localDB.find(collectionName, {}, options).then(function(data){
			var response = buildArrayUniqueValues(data,propertyName);
			dfd.resolve(response);
		});

		return dfd.promise();
	}

	function buildArrayUniqueValues(array, propName){

        var returnedArray = [];

        array.forEach(function(item){
           if(returnedArray.indexOf(item[propName])==-1){
               returnedArray.push(item[propName]);
           }
        });

        return returnedArray;

    };

	return {
		setCurrentResults : setCurrentResults,
		sortEntries : sortEntries,
		resetSorting : resetSorting,
		ascSorting : ascSorting,
		setEntriesPerPage : setEntriesPerPage,
		setSortingNeeded : setSortingNeeded,
		getDataArrayLength : getDataArrayLength,
		getDamageCodeData : getDamageCodeData,
		getShopInfoData : getShopInfoData,
		getRepairLocCodesData : getRepairLocCodesData,
		getMasterPartsData : getMasterPartsData,
		getRepairSTSCodesData : getRepairSTSCodesData,
		getAssociatePartsWithCodesData : getAssociatePartsWithCodesData,
		getShopLimits: getShopLimits,
		getLaborRates: getLaborRates,
		setSearchFieldsForShop : setSearchFieldsForShop,
		getShopDataByShopCode : getShopDataByShopCode,
		resetCodeData : resetCodeData,
		getSortingOrder : getSortingOrder,
		buildCodeObject : buildCodeObject,
		setManualCode : setManualCode,
		setModeCode : setModeCode,
		resetPrefilterSettings : resetPrefilterSettings,
		setPartsGroupCode : setPartsGroupCode,
		setManufacturerCode : setManufacturerCode
	}

});
