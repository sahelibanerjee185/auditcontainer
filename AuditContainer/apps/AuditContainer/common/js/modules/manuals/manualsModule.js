
/* JavaScript content from js/modules/manuals/manualsModule.js in folder common */
audit.factory('$manualsModule', function($localDB, $filter, $commonOperations,$backend,$timeout){
	
	var obj = {
		manuals : [],
	};
			
	return { 
		getManuals : function(callback) {
			var manuals = [];
			var dfd = $.Deferred();
			$backend.refreshManual()
			.then(function(){
				WL.JSONStore.get('manual').advancedFind([], {})
					.then(function (data) {
						for (i in data) {
							if(data[i].json !== undefined){
								var manual = {
									title 				: data[i].json.title,
									language 			: ManualsLanguages[data[i].json.language],
									extension 			: data[i].json.extension,
									serverPath			: data[i].json.serverPath,
									localPath 			: data[i].json.localPath,
									addedDate			: data[i].json.addedDate,
									attachmentListId 	: data[i].json.attachmentListId
								}
								manuals.push(manual);
							} 					 
						}
						if (typeof(callback) == "function") callback(manuals);
						obj.manuals = manuals;
						dfd.resolve(manuals);
					})
					.fail(function(err){
						dfd.reject("Error on getting all manuals");
					})
				});
			return dfd.promise();
		},
		
		//SPOT 6 - if offline get manuals from storage
		getManualsOffline : function(callback) {
			var manuals = [];
			var dfd = $.Deferred();
			WL.JSONStore.get('manual').advancedFind([], {})
					.then(function (data) {
						for (i in data) {
							if(data[i].json !== undefined){
								var manual = {
									title 				: data[i].json.title,
									language 			: ManualsLanguages[data[i].json.language],
									extension 			: data[i].json.extension,
									serverPath			: data[i].json.serverPath,
									localPath 			: data[i].json.localPath,
									addedDate			: data[i].json.addedDate,
									attachmentListId 	: data[i].json.attachmentListId
								}
								manuals.push(manual);
							} 					 
						}
						if (typeof(callback) == "function") callback(manuals);
						obj.manuals = manuals;
						dfd.resolve(manuals);
					}).fail(function(err){
						dfd.reject("Error on getting all manuals");
			});
			
			return dfd.promise();
		},//
		
		getObj : function() {
			return obj;
		}		
	}
			
});