/**
 * Apr, 13 2017
 * SystemInfoService.js
 * Constantin Radu
*/

(function(){
	'use strict';

	audit
    .factory('SystemInfoService', SystemInfoService);

	SystemInfoService.$inject = ['$ionicLoading', 'multiModal', '$rootScope', '$localDB', '$q', '$manualsModule'];

	function SystemInfoService($ionicLoading, multiModal, $rootScope, $localDB, $q, $manualsModule) {

		var service = {
			showModal : showModal
		};

		/**
		 * Function		: showModal
		 * Purpose		: load the modal with 'Manuals' and 'MERC+ codes' tabs
		 * */
		function showModal() {
			// Render the module
			var $systemInfoServiceModal = multiModal.setBasicOptions('confirm', 'commentModal', null, 'full'),
			$systemInfoServiceModal = multiModal.setTemplateFile($systemInfoServiceModal, 'templates/manuals/systemInfoModal.html');
			multiModal.openModal($systemInfoServiceModal, $rootScope);
		};

		return service;

	}
})();
