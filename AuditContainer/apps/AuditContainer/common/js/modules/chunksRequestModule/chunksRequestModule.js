/**
 * May, 12 2016
 * ChunksRequestModule.js
 * Stavarache Vlad
 */

/**
 * ChunksRequestModule Factory
 * @namespace Factories
 */
(function(){
	'use strict';
	
	audit
    .factory('ChunksRequestModule', ChunksRequestModule);
	
	ChunksRequestModule.$inject = ['$localDB', '$ionicLoading'];
	
	/**
	 * @namespace ChunksRequestModule
	 * @desc Helper service used to get large amounts of static data more efficiently
	 * with low memory consumption, making several small requests to the server
	 * and to json store
	 * @memberOf Factories
	 */
	function ChunksRequestModule($localDB, $ionicLoading) {
		
		// Need to declare in JSONStoreInitData main jsonstore collection to store the requests response in
		// Need to declare in JSONStoreInitData jsonstore collection to tell if the request has been successful or not
		// Need to specify a function that retrieves the query rows count
		// Need to specify a function that retrieves the data from the main query
		// Need to specify if you want to save the data in jsonstore or not
		// Need to specify variable in which to store the data from the request
		
		var service = {
			getData: getData,
			// getAndStoreData: getAndStoreData,
			clearLocalData: clearLocalData,
			setChunkSize: setChunkSize
		};
		
		var chunkSize = 5000;
		
		return service;
		
		/**
	     * @name getData
	     * @desc function used to populate a passed reference and a json store collection
		 * with relevant data depending on passed params
	     * @param {Array} results - reference to a variable in another service to use for storing the data from the request
         * @param {Function} getEntries - function that gets data for request : must return a promise, must have offset, limit parameters
         * @param {Function} getEntriesCount - function that gets data count for request : must return a promise
		 * @param {Object} collectionInfo requires 2 properties: mainCollection (collection to save data to)
		 * and mainCollectionLoaded(collection that serves as a model to tell if all the data has been saved or not) 
	     * @returns {Promise}
	     * @memberOf Factories.ChunksRequestModule
	     */
		function getData(results, getEntries, getEntriesCount, collectionInfo) {
			var defer = $.Deferred();
			
			if (results && results.length > 0) {
				defer.resolve(results);
				return defer.promise();
			}
			
			_isCollectionLoaded(collectionInfo)
			.then(function(isLoaded){
				console.log("isLoaded", isLoaded);
				// if it is loaded, we need to get the data from jsonstore
				// use get collection count to know how many requests we need!
				if (isLoaded) {
					return getDataFromJSONStore(results, collectionInfo.mainCollection);
				} else {
					return getDataFromServer(results, getEntries, getEntriesCount, collectionInfo);
				}
			})
			.then(function(){
				defer.resolve(results);
			})
			.fail(function(error){
				results = [];
				defer.reject(error)
			});
			
			return defer.promise();
		}
		
		function setChunkSize(size) {
			chunkSize = size;
		}
		
		/**
	     * @name clearLocalData
	     * @desc Removes all local data and empties passed reference
	     * @param {Array} results - reference to be cleared
		 * @param {Object} collectionInfo has 2 properties: mainCollection (collection to save data to)
		 * and mainCollectionLoaded(collection that serves as a model to tell if all the data has been saved or not) 
	     * @returns {Promise}
	     * @memberOf Factories.ChunksRequestModule
	     */
		function clearLocalData(results, collectionInfo) {
			// delete reference and clear both collections
			var defer = $.Deferred();
			
			results = [];
			
			if (typeof collectionInfo === "object" && collectionInfo.mainCollection && collectionInfo.mainCollectionLoaded) {
				$localDB.clearCollection(collectionInfo.mainCollectionLoaded)
				.then(function(){
					return $localDB.clearCollection(collectionInfo.mainCollection);
				})
				.then(function(){
					defer.resolve();
				})
				.fail(function(error){
					defer.reject(error);
				});
			} else {
				defer.resolve();
			}
			
			return defer.promise();
		}
		
		function getDataFromServer(results, getEntries, getEntriesCount, collectionInfo) {
			var defer = $.Deferred();
			
			var numberOfRequests;
			var requestArray = [];
			
			clearLocalData(results, collectionInfo)
			.then(function(){
				return getEntriesCount();
			})
			.then(function(count){
				if (count > 0) {
					numberOfRequests = Math.ceil(count/chunkSize);
					
					requestArray = _generateServerRequestArray(results, getEntries, numberOfRequests, collectionInfo.mainCollection);
					
					return runPromiseSequenceArray(requestArray);
				} else {
					defer.resolve([])
					return defer.promise();
				}
			})
			.then(function(){
				return _setCollectionLoaded(collectionInfo.mainCollectionLoaded);
			})
			.then(function(){
				defer.resolve(results);
			})
			.fail(function(error){
				results = [];
				defer.reject(error);
			});
			
			return defer.promise();
		}
		
		function getDataFromJSONStore(results, collection) {
			var defer = $.Deferred();
			var numberOfRequests;
			var requestArray = [];
			
			WL.JSONStore.get(collection).count()
			.then(function(count){
				if (count > 0) {
					numberOfRequests = Math.ceil(count/chunkSize);
					requestArray = _generateRequestArray(results, _getFromJSONStore, numberOfRequests, collection);
					return runPromiseSequenceArray(requestArray);
				} else {
					defer.resolve([])
					return defer.promise();
				}
			})
			.then(function(){
				defer.resolve(results);
			})
			.fail(function(error){
				defer.reject(error)
			});
			
			return defer.promise();
		}
		
		function _generateRequestArray(results, _callback, numberOfRequests, collection) {
			var requestArray = [];
			
			for (var i = 0; i < numberOfRequests; i++) {
				var offset = i * chunkSize;
				var callback = _callback.bind(null, results, offset, chunkSize, collection);
				requestArray.push(callback);
			}
			
			return requestArray;
		}
		
		function _generateServerRequestArray(results, _callback, numberOfRequests, collection) {
			var requestArray = [];
			
			for (var i = 0; i < numberOfRequests; i++) {
				var offset = i * chunkSize;
				var getFromServer = _getFromServer.bind(null, _callback, results, offset, chunkSize, collection)
				
				requestArray.push(getFromServer);
			}
			
			return requestArray;
		}
		
		function _getFromServer(serverRequest, results, offset, limit, collection) {
			var defer = $.Deferred();
			
			if (typeof serverRequest === "function") {
				serverRequest(offset, limit)
				.then(function(data){
					_addToArray(results, data);
					if (collection) {
						// Need to save in jsonstore as well
						return $localDB.add(collection, data, {});
					} else {
						defer.resolve();
						return defer.promise();
					}
				})
				.then(function(){
					defer.resolve();
				})
				.fail(function(error){
					results = [];
					defer.reject(error);
				});
			}
			
			return defer.promise();
		}
		
		function _getFromJSONStore(results, offset, limit, collection) {
			var defer = $.Deferred();
			
			WL.JSONStore.get(collection).advancedFind([], {limit: limit, offset: offset}).then(function(data){
				var parsedResults = _jsonStoreParse(data);
				_addToArray(results, parsedResults);
				defer.resolve();
			}).fail(function(error){
				defer.reject(error);
			})
			
			return defer.promise();
		}
		
		function runPromiseSequenceArray(requestArray) {
			var sequence = $.when();
			
			var queue = requestArray;
			
			sequence = queue.reduce(function(prev, cur) {
			    return prev.then(cur);
			}, $.Deferred().resolve());
			
			return sequence;
		}
		
		function _addToArray(mainArray, arrayToAdd) {
			Array.prototype.push.apply(mainArray, arrayToAdd);
		}
		
		function _isCollectionLoaded(collectionInfo) {
			var defer = $.Deferred();
			var collectionLoaded = false;
			
			if (typeof collectionInfo === "object" && collectionInfo.mainCollection && collectionInfo.mainCollectionLoaded) {
				$localDB.find(collectionInfo.mainCollectionLoaded, {}, {}).then(function(data){
					if (data && data.length > 0) {
						collectionLoaded = data[0].json.loaded;
						defer.resolve(collectionLoaded);
					} else {
						defer.resolve(collectionLoaded);
					}
				}).fail(function(error){
					defer.reject(error);
				})
			} else {
				defer.resolve(collectionLoaded);
			}
			
			return defer.promise();
		}
		
		function _setCollectionLoaded(collection) {
			var defer = $.Deferred();
			
			$localDB.clearCollection(collection).then(function(){
				$localDB.add(collection, [{loaded : true}], {}).then(function(){
					defer.resolve();
				}).fail(function(error){
					defer.reject(error);
				});
			}).fail(function(error){
				defer.reject(error);
			})
			
			return defer.promise();
		}
		
		function _convertDateToTimeStampParse(dataSet, dateFields) {
			for (var i = 0; i < dataSet.length; i++) {
				var entry = dataSet[i];
				for (var j = 0; j < dateFields.length; j++) {
					var dateField = dateFields[j];
					// Create new field with same name just add _ts to it
					entry[dateField + "_ts"] = new Date(entry[dateField]).getTime(); 
				}
			}
		}
		
		function _jsonStoreParse(dataSet) {
			var parsedResult = [];
			for (var i = 0; i < dataSet.length; i++) {
				var entry = dataSet[i];
				if (entry && entry.json) {
					parsedResult.push(entry.json);
				}
			}
			return parsedResult;
		}
	}
})();