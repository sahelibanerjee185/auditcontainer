/**
 * Sep 17, 2015
 * Backend.js
 * AlinPreda
 */

/**
 * $backend Factory
 * @namespace Factories
 */
(function(){
	'use strict';
	
	audit
    .factory('$backend', $backend);
	
	$backend.$inject = ['$localDB', '$location', '$rootScope', '$settings', 'responseHandler','$nestedDirectory','auditService'];
	
	/**
	 * @namespace $backend
	 * @desc Service used for some interaction with MFP
	 * @memberOf Factories
	 */
	function $backend($localDB, $location, $rootScope, $settings, responseHandler, $nestedDirectory, auditService) {
		// Public API in service
		var service = {
			resetApp : resetApp,
			sendMail : sendMail,
			refreshManual : refreshManual,
			getManual : getManual,
			login : login,
			loginWithCertificate : loginWithCertificate,
			verifyIdentity : verifyIdentity
		}
		
		var adaptorInvoker = {
			getManual : this.getManual,
			getManuals : this.getManuals,
			sendMail : this.sendMail,
			login : this.login,
			loginWithCertificate : this.loginWithCertificate,
			verifyIdentity : this.verifyIdentity
		}

		var localImpl = {
			getManual : this.getManual,
			refreshManual : this.refreshManual,
			sendMail : this.sendMail,
			login : this.login,
			verifyIdentity : this.verifyIdentity,
			loginWithCertificate : this.loginWithCertificate
		}

		/**
	     * @name resetApp
	     * @desc Removes all local data in jsonstore and reloads the app
	     * @param {N/A}
	     * @returns {Promise}
	     * @memberOf Factories.$backend
	     */
		function resetApp() {
			var defer = $.Deferred();
			$localDB.destroy()
			.then(function() {
				WL.Client.reloadApp();
				defer.resolve();
			});
			return defer.promise();
		}
		
		/**
	     * @name sendMail
	     * @desc Sends emails with attachments if 
	     * @param {String} from - from address
		 * @param {String} to - to address, comma delimited string of email addresses
		 * @param {String} subject
		 * @param {String} message
		 * @param {Object} attachments - need to pass it even if no attachments like this : JSON.stringify([{fileName:null,data:null}])
		 * if attachments are to be added - filename is the name of the file as a string
		 * and data is the file to add as base64 string
		 * @param {String} bcc - optional
	     * @returns {Promise}
	     * @memberof Factories.$backend
	     */
		function sendMail(from, to, subject, message, attachments, bcc){
			return localImpl.sendMail(from,to,subject,message,attachments,bcc);
		}
		
		/**
	     * @name refreshManual
	     * @desc Gets the list of manuals from the server 
		 * and updates the manual collection with the newest data
	     * @param {N/A}
	     * @returns {Promise}
	     * @memberof Factories.$backend
	     */
		function refreshManual(){
			var defer = $.Deferred();
			
				localImpl.refreshManual()
				.then(function(){
					WL.Logger.debug("$backend :: All manuals successfully refreshed!")
					defer.resolve();
				})
				.fail(function(error){
					defer.reject(error);
				})
			
			return defer.promise();
		}
		
		/**
	     * @name getManual
	     * @desc Returns the base64 pdf document from local storage if available or downloads it from server and updates local storage
	     * @param {String} serverPath - path of document on the server
	     * @returns {Promise}
	     * @memberof Factories.$backend
	     */
		function getManual(serverPath){
			var defer = $.Deferred();
			
			localImpl.getManual(serverPath)
			.then(function(pdf){
				WL.Logger.debug("$backend :: PDF document successfully retrieved!")
				defer.resolve(pdf);
			})
			.fail(function(error){
				defer.reject(error);
			})
		
			return defer.promise();
		}
		
		/**
	     * @name login
	     * @desc Deprecated
	     * @memberof Factories.$backend
	     */
		function login(username, password){
			var defer = $.Deferred();
			
				localImpl.login(username, password)
				.then(function(){
					WL.Logger.debug("$backend :: Login successful!")
					defer.resolve();
				})
				.fail(function(error){
					WL.Logger.debug("$backend :: Login failed!")
					defer.reject(error);
				})
			
			return defer.promise();
		}
		
		/**
	     * @name loginWithCertificate
	     * @desc Verifies validity of certificate agains DB
	     * @param {String} certificateId
	     * @returns {Promise}
	     * @memberof Factories.$backend
	     */
		function loginWithCertificate(certificateId){
			return localImpl.loginWithCertificate(certificateId);
		}
		
		/**
	     * @name verifyIdentity
	     * @desc Deprecated
	     * @memberof Factories.$backend
	     */
		function verifyIdentity(){
			var defer = $.Deferred();
			
			localImpl.verifyIdentity()
			.then(function(){
				WL.Logger.debug("$backend :: Identity successfully verified!")
				defer.resolve();
			})
			.fail(function(error){
				WL.Logger.debug("$backend :: Identity verification failed!")
				defer.reject(error);
			})
			
			return defer.promise();
		}

		adaptorInvoker.getManual = function(url) {
			var defer = $.Deferred();	  
			
			var resourceRequest = new WLResourceRequest(
				"/adapters/ManualJavaAdapter/getManual",
				WLResourceRequest.GET
			);
	
			var username = userContext.getUsername();
			var token = userContext.getToken();				
			var params = [username,token,url,""];
			
			resourceRequest.setQueryParameter("parameters", params);
			
			resourceRequest.send().then(
					function(response){
						var response = response.responseJSON;
						if (!response.isSuccessful){
								if(response.errors == undefined){
									defer.reject({errorCode : response.errorCode, errorMsg : response.errorMsg});
								} else {
									defer.reject(response.errors);
								}
							}
							else {
								WL.Logger.debug(response.invocationResult)
								defer.resolve(response.invocationResult);
							}
					},
					function(error){
						console.log(JSON.stringify(error));
						if (error != undefined && error.invocationResult != undefined && error.invocationResult.errorCode == 102){
								responseHandler.feedback(error);
							}
						defer.reject(error);
					}
				);
			
			return defer.promise();
		}

        /**
         * @name getManuals
         * @desc Returns the list of manuals from DB2
         * @param {N/A}
         * @returns {Promise}
         * @memberof Factories.$backend
         */
		adaptorInvoker.getManuals = function(){
			var defer = $.Deferred();
			WL.Client.invokeProcedure({
					adapter : 'DBAdapterContainer',
					procedure : 'getManualList',
					parameters : [userContext.getUsername(),userContext.getToken()]
				}, {
					onSuccess : function(response) {
						if (!response.invocationResult.isSuccessful){
							if(response.errors == undefined){
								defer.reject({errorCode : response.invocationResult.errorCode, errorMsg : response.invocationResult.errorMsg});
							} else {
								defer.reject(response.errors);
							}
						}
						else {
							WL.Logger.debug(response.invocationResult.invocationResult)
							defer.resolve(response.invocationResult.invocationResult);
						}
					},
					onFailure: function(error){
						if (error != undefined && error.invocationResult != undefined && error.invocationResult.errorCode == 102){
							responseHandler.feedback(error);
						}
						defer.reject(error);
					}
				});
			return defer.promise();
		}
			
		adaptorInvoker.sendMail = function(from, to, subject, message, attachments, bcc) {
			var defer = $.Deferred();
			
			if (!bcc) bcc = ""; // need to pass an empty string anyway
			
			var resourceRequest = new WLResourceRequest(
					"/adapters/EmailAdapter/sendMail/" + from + "/" + to,
					WLResourceRequest.POST
			);
			resourceRequest.sendFormParameters({message : message, attachments : attachments, subject : subject, bcc : bcc})
			.then(function(response){
				defer.resolve(response.responseJSON)
			})
			.fail(function(error){
				defer.reject(error)
			});
			return defer.promise();
		}
			
		adaptorInvoker.login = function(username, password){
			var defer = $.Deferred();
			
			WL.Client.invokeProcedure({
				adapter : 'LoginAdapter',
				procedure : 'login',
				parameters : [username, password]
			}, {
				onSuccess : function(response) {
					if (!response.invocationResult.isSuccessful) {
						defer.reject(response.invocationResult);
					} else {
						defer.resolve(response.invocationResult.invocationResult.token);
					}
				},
				onFailure: function(error){
					WL.Logger.debug(error);
					defer.reject(error);
				}
			});
			
			return defer.promise();
		}

		adaptorInvoker.loginWithCertificate = function(certificateId){
			var defer = $.Deferred();
			
			WL.Client.invokeProcedure({
				adapter : 'LoginAdapter',
				procedure : 'loginWithCertificate',
				parameters : [certificateId]
			}, {
				onSuccess : function(response) {
					if (!response.invocationResult.isSuccessful) {
						defer.reject(response.invocationResult);
					} else {
						defer.resolve(response.invocationResult.invocationResult);
					}
				},
				onFailure: function(error){
					WL.Logger.debug(error);
					defer.reject(error);
				}
			});
			
			return defer.promise();
		}
			
		adaptorInvoker.verifyIdentity = function(){
			var defer = $.Deferred();
			WL.Client.invokeProcedure({
					adapter : 'LoginAdapter',
					procedure : 'verifyIdentity',
					parameters : [userContext.getUsername(),userContext.getToken()]
				}, {
					onSuccess : function(response) {
						if (!response.invocationResult.isSuccessful){
							defer.reject(response.invocationResult);
						}
						else {
								defer.resolve();
						}
					},
					onFailure: function(error){
						defer.reject(error);
					}
				});
			return defer.promise();
		}
	
		function _base64ToArrayBuffer(base64) {
			var binary_string =  window.atob(base64);
			var len = binary_string.length;
			var bytes = new Uint8Array( len );
			for (var i = 0; i < len; i++)        {
				bytes[i] = binary_string.charCodeAt(i);
			}
			return bytes.buffer;
		}
	
		function checkIfFileExists(path) {
			var dfd = $.Deferred();
			var fileExists = false;
			
			window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function(fileSystem){
				fileSystem.root.getFile(path, { create: false }, function(){
					fileExists = true;
					dfd.resolve(fileExists);
				}, function(error){
					dfd.resolve(fileExists);
				});
			}, function(error){
				dfd.reject(error);
			});
			
			return dfd.promise();
		}
	
		function saveManual(content, fileName) {
			var dfd = $.Deferred();
			var directoryPath = window.PERSISTENT;
			var directoryName = "manuals";
			
			var arrayBuffer = _base64ToArrayBuffer(content);
			var fullManualsPath = cordova.file.applicationStorageDirectory + "/Documents/" + directoryName + "/";
			
			// Need to create the directory if it doesn't exist.
			$nestedDirectory.createDirectory(directoryName).then(function(response){
				// Need to save the file
				window.resolveLocalFileSystemURL(fullManualsPath, function(dirEntry) {
					dirEntry.getFile(fileName, {create: true, exclusive: false}, function(fileEntry) {
						fileEntry.createWriter(function (writer) {
							writer.onwriteend = function(evt) {
								console.log("evt", evt);
								dfd.resolve(fullManualsPath + fileName);
							};

							writer.write(arrayBuffer);
						},
						function(err){
							WL.Logger.debug(err)
							dfd.reject(err);
						});
					},
					function(err){
						WL.Logger.debug(err)
						dfd.reject(err);
					});
				}, function(error) {
					console.log("error", error);
					dfd.reject(error)
				});
			});
			
			return dfd.promise();
		}

		localImpl.getManual = function(serverPath){
			var defer = $.Deferred();
			
			// get file name!
			var fileName = serverPath.slice(serverPath.lastIndexOf("/"));
			var manualTitle;
			
			$localDB.find('manual',{serverPath : serverPath},{exact : true})
			.then(function(manual){
				if (manual.length == 0){
					defer.reject("Incorrect URL!");
				}
				else {
					manualTitle = manual[0].json.title;
					var relativePath = "manuals/" + fileName;
					// Check if manual already saved and open it if it is!
					checkIfFileExists(relativePath).then(function(fileExists){
						if (fileExists) {
							// We already have the pdf, just open it!
							defer.resolve({relativePath : relativePath, manualTitle : manualTitle})
						} else {
							// We need to get the pdf from the server
							auditService.testSignal().then(function(){
								adaptorInvoker.getManual(decodeURI(serverPath))
								.then(function(response){
									saveManual(response, fileName).then(function(){
										defer.resolve({relativePath : relativePath, manualTitle : manualTitle});
									}).fail(function(error){
										defer.reject(error);
									});
								})
								.fail(function(error){
									defer.reject(error);
								})
							}).fail(function(error){
								defer.reject(error);
							});
						}
					}).fail(function(error){
						defer.reject(error);
					});
				};
			});
			
			return defer.promise();
		}
			
		localImpl.refreshManual = function(){
			var defer = $.Deferred();
			adaptorInvoker.getManuals()
			.then(function(response){
				$localDB.find('manual',{},{})
				.then(function(data){
					for (var serverManual in response){
						for (var storeManual in data){
							if(response[serverManual].serverPath == data[storeManual].json.serverPath){
								response[serverManual].localPath = data[storeManual].json.localPath;
								response[serverManual].attachmentListId = data[storeManual].json.attachmentListId;
							}
						}
					}
					$localDB.erase('manual',data,{})
					.then(function(){
						$localDB.add('manual',response,{markDirty : false})
						.then(function(){
							defer.resolve();
						});
					})
				})
			})
			.fail(function(error){
				defer.reject(error);
			});
			
			return defer.promise();
		}
		
		localImpl.sendMail = function(from, to ,subject ,message, attachments, bcc){
			return adaptorInvoker.sendMail(from,to,subject,message,attachments,bcc);
		}
		
		localImpl.login = function(username, password){
			var defer = $.Deferred();
			
			adaptorInvoker.login(username, password)
			.then(function(token){
				userContext.setUsername(username)
				.then(function(){
					userContext.setToken(token)
					.then(function(){
						defer.resolve();
					})
				})
			})
			.fail(function(error){
				defer.reject(error);
			});
			
			return defer.promise();
		}
		
		localImpl.verifyIdentity = function() {
			var defer = $.Deferred();
			
			adaptorInvoker.verifyIdentity()
			.then(function(){
				defer.resolve();
			})
			.fail(function(error){
				defer.reject(error);
			});
			
			return defer.promise();
		};
		
		localImpl.loginWithCertificate = function(certificateId){
			return adaptorInvoker.loginWithCertificate(certificateId);
		};
		
		return service;
	}
})();