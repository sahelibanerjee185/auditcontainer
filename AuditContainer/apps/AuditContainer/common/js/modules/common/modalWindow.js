/**
 * multiModal Factory
 * @namespace Factories
 */
(function(){
	'use strict';
	
	audit
    .factory('multiModal', multiModal);
	
	multiModal.$inject = ['$http', '$compile', '$controller', '$timeout', '$q'];
	
	/**
     * @namespace multiModal
     * @desc Service for modal windows functionality 
	 * configuring them, opening, closing etc
     * @memberOf Factories
     */
	function multiModal($http, $compile, $controller, $timeout, $q) {
		var multiModalElementScopes = [];
		var modalsOpen = false;
		var _pauseModal = false;
		var _pauseModalClicking = false;
		var _pauseInterval = 500;
		
		var defaults = {
    		settings: {
    			templateFile: 'templates/components/modal.html',
				type: 'notification',
				mode: 'popup',
				wrapper: document.body,
				removeOnAction: true,
				manualShow: false
    		},
    		contents: {
    			modalConfirmButton: Messages.ok,
    			modalCancelButton: Messages.cancel,
				modalId: 'modalID'
    		},
			inputOptions: {
				placeHolder: '',
				inputType: 'text',
				validationRule: null
			},
			actions: {}
    	}

		var service = {
			options : {},
			context : {},
			defaults : defaults,
			resetPause : resetPause,
			processBackButton : processBackButton,
			modalIsOpen : modalIsOpen,
			getInstances : getInstances,
			getCurrentInstance : getCurrentInstance,
			closeCurrentModal : closeCurrentModal,
			removeAllModals : removeAllModals,
			setBasicOptions : setBasicOptions,
			setManualShow : setManualShow,
			setActions : setActions,
			setInputOptions : setInputOptions,
			openModal : openModal,
			setTemplateFile: setTemplateFile
		}

		return service;

		function resetPause() {
    		_pauseModal = false;
    	}

		/**
	     * @name processBackButton
	     * @desc Not used - could be used for android back button functionality
	     * @param {N/A}
	     * @returns {N/A} - has side effects
	     * @memberof Factories.multiModal
	     */
    	function processBackButton() {
    		if (multiModalElementScopes.length == 0) return
    		
    		if (sideMenuObject.open) return;
    		
    		var currentModalScope = multiModalElementScopes[multiModalElementScopes.length - 1];
    		
        	if (currentModalScope.settings.type == 'notification') {
        		currentModalScope.confirmMultiModal();
        	} else {
        		currentModalScope.cancelMultiModal();
        	}
    	}

		/**
	     * @name modalIsOpen
	     * @desc returns whether a modal is currently open
	     * @param {N/A}
	     * @returns {Bool}
	     * @memberof Factories.multiModal
	     */
    	function modalIsOpen() {
    		return modalsOpen;
    	}

		/**
	     * @name getInstances
	     * @desc returns an array of scopes of all the modals currently opened
	     * @param {N/A}
	     * @returns {Array} - array of modal scopes
	     * @memberof Factories.multiModal
	     */
    	function getInstances() {
    		return multiModalElementScopes;
    	}

		/**
	     * @name getCurrentInstance
	     * @desc returns the scope of the latest modal window opened
	     * @param {N/A}
	     * @returns {Object} - a scope
	     * @memberof Factories.multiModal
	     */
    	function getCurrentInstance() {
    		return multiModalElementScopes[multiModalElementScopes.length - 1];
    	}
    	
    	function closeCurrentModal() {
    		if (modalsOpen) {
    			multiModalElementScopes[multiModalElementScopes.length - 1].closeMultiModal();
    		} 		
    	}

    	function removeAllModals() {
    		if (multiModalElementScopes.length > 0) {
    			$.each(multiModalElementScopes, function( index, modalWindowScope ) {
    				modalWindowScope.modalTemplate.remove();
				});
    		}
    		multiModalElementScopes.length = 0;
    		modalsOpen = false;
    	}

		/**
	     * @name setBasicOptions
	     * @desc Used to generate a config object to be passed to 
		 * openModal function to set modal options
	     * @param {String} type - notification, input, confirm
		 * @param {String} id - modal id 
		 * @param {String} content - modal main message
		 * @param {String} mode - not relevant
		 * @param {Bool} removeOnAction - whether to auto close the modal on any action
		 * @param {String} confirmButton - confirm button text
		 * @param {String} cancelButton - cancel button text
		 * @param {Object} wrapper - the body is set as default
	     * @returns {Object} - a config object
	     * @memberof Factories.multiModal
	     */
		function setBasicOptions(type, id, content, mode, removeOnAction, confirmButton, cancelButton, wrapper) {
    		var newOptions = {
				settings: {
    				type: type,
    				mode: mode,
    				wrapper: wrapper,
    				removeOnAction: removeOnAction
        		},
        		contents: {
        			modalContent: content,
        			modalConfirmButton: confirmButton,
        			modalCancelButton: cancelButton,
    				modalId: id
    			},
    			actions: {},
    			inputOptions: {}
    		}
    		var newObject = deepMerge(newOptions, this.defaults);
    		
			if (!confirmButton) newObject.contents.modalConfirmButton = "OK";
    		if (!cancelButton) newObject.contents.modalCancelButton = "Cancel";
    		
			return newObject;
    	}

    	function setManualShow(optionsObject, manualShow) {
    		optionsObject.settings.manualShow = manualShow;
    		return optionsObject;
    	}

		/**
	     * @name setTemplateFile
	     * @desc Used to set a custom template for the modal
	     * @param {Object} optionsObject - a modal config object
		 * @param {String} template - template path
	     * @returns {Object} - a config object
	     * @memberof Factories.multiModal
	     */
		function setTemplateFile(optionsObject, template) {
    		optionsObject.settings.templateFile = template;
    		return optionsObject;
    	}

		/**
	     * @name setActions
	     * @desc Used to set callbacks for available actions on modal
	     * @param {Object} optionsObject - a modal config object
		 * @param {Function} confirm - confirm callback
		 * @param {Function} decline - decline callback
	     * @returns {Object} - a config object
	     * @memberof Factories.multiModal
	     */
    	function setActions(optionsObject, confirm, decline) {
    		if (confirm) optionsObject.actions.confirm = function(input) {
    			confirm(input);
    		};
    		if (decline) optionsObject.actions.decline = function() {
    			decline();
    		};
    		return optionsObject;
    	}

    	function setInputOptions(optionsObject, placeHolder, inputType, validationRule, validationMessage) {
    		optionsObject.inputOptions.placeHolder = placeHolder;
    		optionsObject.inputOptions.inputType = inputType;
    		optionsObject.inputOptions.validationRule = validationRule;
    		optionsObject.inputOptions.validationMessage = validationMessage;
    		
    		return optionsObject;
    	}
    	
		/**
	     * @name openModal
	     * @desc Opens a modal window based on the passed options object
	     * @param {Object} options - a modal config object
		 * @param {Function} scope - will be used as a parent scope for the modal
		 * @param {Bool} handleKeyboard - adds events to raise or lower modal if true
	     * @returns {Promise} - resolves once modal is loaded and ready
	     * @memberof Factories.multiModal
	     */
    	function openModal(options, scope, handleKeyboard) {
    		// Define configuration variables
    		var dfd = $.Deferred();
    		var multiModal = this;
    		
    		if (_pauseModal) {
        		return;
        	}
    		
    		_handlePause(_pauseModal);
    		
    		this.context = options;
    		
    		var vTpl,
    			container = angular.element(this.context.settings.wrapper),
    			customClass, overlayDiv; 
    		
    		// Create a child scope for the modal
    		var modalScope = scope.$new();
    		
    		// Check if a custom template is available
			vTpl = this.context.settings.templateFile;
			hybrid.sendDisableNativeUI(true);  
    		
    		// Load content from template
    		$http.get(vTpl).then(function(response) {
    			modalScope.modalTemplate = angular.element(response.data);
    			
    			// If the modal's heigh is small enough we can just fit in in what is left of the screen when the keyboard shows
    			
    			if (handleKeyboard) {
    				console.log("In handleKeyboard");
    				
    				modalScope.contentWrapper = modalScope.modalTemplate.children()[0];
        			
        			window.addEventListener('native.keyboardshow', keyboardShowHandler);
        			window.addEventListener('native.keyboardhide', keyboardHideHandler);
    			}
    			
    			$compile(modalScope.modalTemplate)(modalScope);
    			
                // Push the variable to the scope
                
                modalScope.contents = multiModal.context.contents;
                modalScope.settings = multiModal.context.settings;
                modalScope.inputOptions = multiModal.context.inputOptions;
                modalScope.actions = multiModal.context.actions;
                
                // Add default behavior for confirm and close
                
                modalScope.closeMultiModal = function(){
                	document.activeElement.blur();
                	removeElementFromModalStack(modalScope.modalTemplate);
                	modalScope.modalTemplate.remove();
                };
                
                modalScope.cancelMultiModal = function(){
            		if (_pauseModalClicking) {
                		return;
                	}
            		
            		_handlePause(_pauseModalClicking);
            		
            		document.activeElement.blur();
                	
                	modalScope.closeMultiModal();
                	// check if there's any callback function to be executed
                	if (modalScope.actions) {
                		if (modalScope.actions.decline){
                			modalScope.actions.decline();
                    	}
                	}
                };
                
                modalScope.confirmMultiModal = function(){
            		if (_pauseModalClicking) {
                		return;
                	}
            		
            		_handlePause(_pauseModalClicking);
            		
            		document.activeElement.blur();
            		
                	if (modalScope.settings.removeOnAction) modalScope.closeMultiModal();
                	// check if there's any callback function to be executed
                	if (modalScope.actions) {
                		if (modalScope.actions.confirm){
                			modalScope.actions.confirm();
                    	}
                	}
                };
                
                modalScope.cancelIfBackDrop = function($event){
                	if (angular.element($event.target).hasClass('modalContentWrapper')) {
                		if (modalScope.settings.type === 'notification')
                			modalScope.confirmMultiModal();
                		else 
                			modalScope.closeMultiModal();
                	}
                	document.activeElement.blur();
                }
                
                // set a separate confirm function for input modals that has a user validation rule
                
                if (modalScope.settings.type == 'input') {
                	
                	// add helper functions for input focus and blur
                    
                	modalScope.addFocusToParent = function($event){
            			angular.element($event.target).parent().addClass('entryFieldTap');
            		};
            		
            		modalScope.removeFocusFromParent = function($event){
            			angular.element($event.target).parent().removeClass('entryFieldTap');
            		};
                	
                	modalScope.addFocusClass = function($event) {
            			angular.element($event.target).addClass('entryFieldTap');
            		}
            		
                	modalScope.removeFocusClass = function($event) {
            			angular.element($event.target).removeClass('entryFieldTap');
            		}
                	
                	modalScope.inputObject = {
                		value: "",
                		isValid: true
                	}
                	
                	modalScope.confirmInputModal = function(userInput, persist) {
                		if (modalScope.inputOptions.validationRule) modalScope.inputObject.isValid = modalScope.inputOptions.validationRule(userInput);
                		if (modalScope.inputObject.isValid) {
                			if(!(!!persist)) modalScope.closeMultiModal();
                			if (modalScope.actions){
                        		if (modalScope.actions.confirm){
                        			modalScope.actions.confirm(userInput);
                            	}
                        	}
                		} else {
                			multiModal.getCurrentInstance().modalTemplate.addClass('hidden');
                			var $invalidScope = modalScope.$new();
                			$invalidNotificationOptions = multiModal.setBasicOptions('notification', 'invalidInput', modalScope.inputOptions.validationMessage);
                			$invalidNotificationOptions = multiModal.setActions($invalidNotificationOptions, function(){
                				multiModal.removeAllModals();
                			}); 
                			multiModal.openModal($invalidNotificationOptions, $invalidScope);
                		}
                	}
                }
                
                modalScope.modalTemplate.on('$destroy', function() {
                	modalScope.$destroy();
                	if (handleKeyboard) {
                		window.removeEventListener('native.keyboardshow', keyboardShowHandler);
            			window.removeEventListener('native.keyboardhide', keyboardHideHandler);
                	}
                });
                
                function keyboardShowHandler(event) {
					var keyboardHeight = keyboardInfo.keyboardHeight - keyboardInfo.nativeFooterHeight;
					modalScope.contentWrapper.style.bottom = keyboardHeight/2 + 'px';
			    }
				
				function keyboardHideHandler() {
					modalScope.contentWrapper.style.bottom = '0px';
				}
                
                addElementToModalStack(modalScope);
                
                modalScope.modalTemplate.addClass('hidden');
                
                container.append(modalScope.modalTemplate);
                
                if (modalScope.settings.manualShow) return;
                
                modalScope.modalTemplate.removeClass('hidden');
                
                dfd.resolve();
            });
			
			return dfd.promise();
    	}

		function addElementToModalStack(modalScope) {
			multiModalElementScopes.push(modalScope);
			modalsOpen = true;
		}

		function removeElementFromModalStack(modalElement) {
			var currentScope = multiModalElementScopes.pop();
			if (multiModalElementScopes.length == 0) {
				modalsOpen = false;
				hybrid.sendDisableNativeUI(false);  
			}
		}

		function _handlePause(pauseFlag) {
			pauseFlag = true;
			
			$timeout(function(){
				pauseFlag = false;
			}, _pauseInterval, false);
		}
	}
})();

	
	
