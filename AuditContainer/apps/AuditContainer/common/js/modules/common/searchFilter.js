angular.module('searchUtils', []).factory('searchUtils', function(){
  return{
    compareStr: function(stra, strb){
    	stra = ("" + stra).toLowerCase();
        strb = ("" + strb).toLowerCase();
        return stra.indexOf(strb) !== -1;
    }
  };
});

audit.filter('shopFilter', function(searchUtils){
  return function(input, shopSearch){
    if(!shopSearch) return input;
    var result = [];
    
    angular.forEach(input, function(shop){
      if(searchUtils.compareStr(shop.repairShopNo, shopSearch) || searchUtils.compareStr(shop.repairShopName, shopSearch) || searchUtils.compareStr(shop.depot, shopSearch)  ||searchUtils.compareStr(shop.city, shopSearch))
        result.push(shop);          
    });
    return result;
  };
});


