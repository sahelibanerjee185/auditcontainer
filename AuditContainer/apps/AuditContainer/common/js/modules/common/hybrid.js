
/* JavaScript content from js/modules/common/hybrid.js in folder common */
/**
 * hybrid
 * @namespace hybrid
 */

/**
 * @name hybrid
 * @desc Global object used to communicate with native
 * @param N/A
 * @memberOf hybrid
 */
var hybrid = (function () {

    var footerButtonIndex = {
        InspectionsIndex: 1,
        PlanVisitIndex: 2,
        WatchListIndex: 3,
        ManualsButtonIndex: 4,
        KpiButtonIndex: 5,
        LogOutButtonIndex: 7
    }
    var currentFooterButtonIndex = 1;

    var deferredOCR; //global variable to resolve OCR response

    return {
        init: init,
        updateNativeUI: updateNativeUI,
        switchToolbarButton: switchToolbarButton,
        footerButtonIndex: footerButtonIndex,
        logoutWarning: logoutWarning,
        getTabObject: getTabObject,
        getFooterInfo: getFooterInfo,
        sendDisableNativeUI: sendDisableNativeUI,
        displayPhotoViewer: displayPhotoViewer,
        getMAMInfo: getMAMInfo,
        getDeviceUDID: getDeviceUDID,
        unblockScroll: unblockScroll,
        startSync: startSync,
        stopSync: stopSync,
        setNoUnsyncedCommands: setNoUnsyncedCommands,
        getCurrentFooterButtonIndex : getCurrentFooterButtonIndex,
        setCurrentFooterButtonIndex : setCurrentFooterButtonIndex,
        openOCR: openOCR,
        navigateHybridMenu: navigateHybridMenu
    };

    /**
     * @name receiveAction
     * @desc callback attached with WL.App.addActionReceiver
     * handles the event passed from native such as tapping
     * the back button on the native header
     * @param {Object} received
     * @returns {N/A} 
     * @memberof hybrid
     */
    function receiveAction(received) {
        WL.Logger.debug("ReceivedAction Native to Hybrid :: ", received);
        console.log('received', received);
        switch (received.action) {
            case "successOCR":
                successOCR(received.data);
                break;
            case "failOCR":
                failOCR();
                break;
            case "cancelOCR":
                cancelOCR();
                break;
            case "backButtonClicked" :
                onBackButtonClicked();
                break;
            case "syncButtonClicked" :
                syncButtonClicked();
                break;
            case "setFooterInfo" :
                setFooterInfo(received);
                break;
            case "panGestureOutsideBoundries" :
                unblockScroll(received);
                break;
            case "planVisitTab" :
            case "watchListTab" :
            case "manualsTab" :
            case "kpiTab" :
            case "surveyTab":
            case "galleryTab":
            case "takePhotoTab":
                navigateHybridMenu(received.action);
                break;
            case "logOutTab":
                logoutWarning(received.action);
                break;
            case "pdfRenderingDone":
                onPdfRenderingDone(received);
                break;
            case "MAMUser":
                onReceiveMAMInfo(received);
                break;
            case "deviceUDID":
                onReceiveDeviceUDID(received);
                break;
        }
    }

    /**
     * @name openOCR
     * @desc opens the native OCR functionality to take photo
     * and recognize characters
     * @param {String} path - path on device where to store the photo
     * taken with the OCR
     * @returns {Promise} 
     * @memberof hybrid
     */
    function openOCR(path){
        angular.element(document).injector().invoke(function ($q) {
            deferredOCR = $q.defer();  //resolved in successOCR, cancelOCR or failOCR

            WL.App.sendActionToNative("openOCR", {
            	path: path
            }); 

        });
        return deferredOCR.promise;
    }

    function successOCR(data){
        deferredOCR.resolve(data);
    };

    // Reject with status 101 to handle cancel
    function cancelOCR(){
        deferredOCR.reject({status: 101});
    };

    // Reject with status 102 to handle fail
    function failOCR(){
        deferredOCR.reject({status:102});
    };

    function getCurrentFooterButtonIndex(){
        return currentFooterButtonIndex;
    }

    function setCurrentFooterButtonIndex(index){
        currentFooterButtonIndex = index;
    }

    /**
     * @name init
     * @desc Attaches a callback for receiving events/messages from native code
     * @param {N/A}
     * @returns {N/A} 
     * @memberof hybrid
     */
    function init() {
        WL.App.addActionReceiver("MyActionReceiver", receiveAction);
    }

    function onPdfRenderingDone(received) {
        var data = received.data;

        if (data.success === true) {
            if (data.path && data.path.length > 0) {
                userContext.setPdfPath(data.path);
            }
            nativePdfPromise.resolve('Pdf rendering finished ok in native.');
        } else {
            nativePdfPromise.reject('Pdf rendering failed in native!');
        }
    }

    /**
     * @name setNoUnsyncedCommands
     * @desc Sets the number displayed in the sync button 
     * in the native header
     * @param {Int} total 
     * @returns {N/A} 
     * @memberof hybrid
     */
    function setNoUnsyncedCommands(total) {
        WL.App.sendActionToNative("updateSyncCount", {
            count: total
        });
    }

    /**
     * @name logoutWarning
     * @desc Not used
     * @memberOf hybrid
     */
    function logoutWarning(action) {
        angular.element(document)
            .injector()
            .invoke(function ($rootScope, multiModal) {
                var $logoutConfirmation = multiModal.setBasicOptions('confirm', 'logoutConfirmation', 'notification', 'popup', false);
                $logoutConfirmation = multiModal.setTemplateFile($logoutConfirmation, 'templates/components/logoutModal.html');
                $logoutConfirmation = multiModal.setActions($logoutConfirmation, function () {
                    switchToolbarButton(1);
                    navigateHybridMenu(action);
                    multiModal.removeAllModals();
                });
                multiModal.openModal($logoutConfirmation, $rootScope);
            })
    }


    function getFooterInfo() {
        WL.App.sendActionToNative("getFooterInfo", {});
    }

    function getMAMInfo() {
        WL.App.sendActionToNative("getMAMInfo", {});
    }

    function getDeviceUDID() {
        WL.App.sendActionToNative("getDeviceUDID");
    }

    function displayPhotoViewer(obj) {
        WL.App.sendActionToNative("displayPhotoViewer", obj);
    }

    function setFooterInfo(footerInfo) {
        keyboardInfo.nativeFooterHeight = footerInfo.data.height;
    }

    function unblockScroll(scrollInfo) {
        var event = new CustomEvent("touchend", {"detail": "Reset scroll"});
        document.dispatchEvent(event);
    }

    /**
     * @name sendDisableNativeUI
     * @desc Disables all user interaction with native UI if true,
     * if true enables it
     * @param {Bool} state 
     * @returns {N/A} 
     * @memberof hybrid
     */
    function sendDisableNativeUI(state) {
        WL.Logger.debug('Sending setNativeState:disabled:' + state + ' to native.');
        WL.App.sendActionToNative("setNativeState", {
            disabled: state
        });
    }

    /**
     * @name navigateHybridMenu
     * @desc Handles navigating through the app tabs 
     * from the native footer
     * @param {String} page 
     * @returns {N/A} 
     * @memberof hybrid
     */
    function navigateHybridMenu(page) {
    	
    	if (page !== 'manualsTab'){
    		window.location.hash = '#' + page;	
    	}
        
        WL.Logger.debug("Navigate Menu Actions ");

        switch (page) {
            case 'kpiTab' :
                setCurrentFooterButtonIndex(footerButtonIndex.KpiButtonIndex);
                WL.Analytics.log({
                    AC_action: "Entered Page",
                    AC_timestamp: getUTCDate(),
                    AC_userName: userContext.getUsername(),
                    AC_pageName: "KPI"
                });
                break;
            case 'manualsTab' :
                angular.element(document)
                    .injector()
                    .invoke(function (SystemInfoService) {  //Open modal for Modes&Codes
                        SystemInfoService.showModal();
                    });

                WL.Analytics.log({
                    AC_action: "Entered Page",
                    AC_timestamp: getUTCDate(),
                    AC_userName: userContext.getUsername(),
                    AC_pageName: "Manuals"
                });
                break;
            case 'planVisitTab':
                setCurrentFooterButtonIndex(footerButtonIndex.PlanVisitIndex);
                WL.Analytics.log({
                    AC_action: "Entered Page",
                    AC_timestamp: getUTCDate(),
                    AC_userName: userContext.getUsername(),
                    AC_pageName: "Plan Visit"
                });
                break;
            case 'surveyTab':
                setCurrentFooterButtonIndex(footerButtonIndex.InspectionsIndex);
                WL.Analytics.log({
                    AC_action: "Entered Page",
                    AC_timestamp: getUTCDate(),
                    AC_userName: userContext.getUsername(),
                    AC_pageName: "Inspections"
                });
                break;
        }
    }

    /**
     * @name onBackButtonClicked
     * @desc Function that is called when the native back button in the header is tapped,
     * normally window.history.back is called here
     * @param N/A
     * @returns N/A
     * @memberOf hybrid
     */
    function onBackButtonClicked() {
        document.activeElement.blur();

        angular.element(document)
        .injector()
        .invoke(function ($location, $timeout) {
            var locationPath = $location.path();
            $timeout(function() {
                if (locationPath.localeCompare("/depotDetails/") == 0) {
                    $location.path("surveyTab");
                } else {
                    window.history.back();
                }
            }, 300);
        });
    }

    function getTabObject() {
        return footerButtonIndex;
    }

    /**
     * @name onReceiveMDMInfo
     * @desc callback function for mdm authentication 
     * will receive user and email info,
     * will be null if auth was not successful
     * @param {Object} info - response object
     * @returns N/A
     * @memberOf hybrid
     */
    function onReceiveMAMInfo(info) {
        // console.log('mdmInfo', info.data);
        angular.element(document)
            .injector()
            .invoke(function ($rootScope) {
                $rootScope.$broadcast("MAMInfoReceived", {response: info.data});
            });
    }

    /**
     * @name onReceiveDeviceUDID
     * @desc Not used
     * @memberOf hybrid
     */
    function onReceiveDeviceUDID(received) {
        console.log('received', received);
        // broadcast
        angular.element(document)
            .injector()
            .invoke(function ($rootScope) {
                $rootScope.$broadcast("DeviceUDIDReceived", {response: received.data.udid});
            });
    }

    /**
     * @name updateNativeUI
     * @desc Sets the native ui display depending on app screen
     * @param {String} newTitle - sets header title
     * @param {Bool} showHeader - if header should be displayed
     * @param {Bool} showFooter - if footer should be displayed
     * @param {Bool} showBackButton - if back button should be displayed
     * @param {Bool} showRefreshButton - if refresh button should be displayed
     * @returns N/A - has side effects
     * @memberOf hybrid
     */
    function updateNativeUI(newTitle, showHeader, showFooter, showBackButton, showRefreshButton) {
        //console.log("Title: "+newTitle);
        //	WL.Logger.debug("Title: "+newTitle);
        closeAllModals();
        document.activeElement.blur();
        WL.App.sendActionToNative("updateNativeUI", {
            title: newTitle,
            showHeader: showHeader,
            showFooter: showFooter,
            showBackButton: showBackButton,
            showRefreshButton: showRefreshButton
        });
    }

    function switchToolbarButton(buttonIndex) {
        WL.App.sendActionToNative("switchToolbarButton", {
            toolbarButtonIndex: buttonIndex
        });
    };

    function syncButtonClicked() {
        document.activeElement.blur();
        angular.element(document)
            .injector()
            .invoke(function (syncService, $ionicLoading) {
                syncService.syncAll().then(function () {
                    stopSync();
                    $ionicLoading.hide();
                }).fail(function () {
                    stopSync();
                    $ionicLoading.hide();
                });
            })
    }

    function stopSync() {
        WL.App.sendActionToNative("syncCompleted", {});
    }

    /**
     * @name startSync
     * @desc Causes the spinner in the native header to start spinning
     * @param {N/A} 
     * @returns N/A - has side effects
     * @memberOf hybrid
     */
    function startSync() {
        WL.App.sendActionToNative("syncStart", {});
    }

    /*
     * ActionBar Methods
     */
    function onMenuItemClicked(itemId) {
        if (itemId >= 1 && itemId <= 3) {
            changePage(itemId);
        } else {
            WL.Logger.debug("Other Action");
        }
    }

    function closeAllModals() {
        document.activeElement.blur();
        angular.element(document)
            .injector()
            .invoke(function (multiModal) {
                multiModal.removeAllModals();
            })
    }

}());

