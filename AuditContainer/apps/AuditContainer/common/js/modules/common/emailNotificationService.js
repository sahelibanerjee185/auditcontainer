/* JavaScript content from js/modules/common/emailNotificationService.js in folder common */
(function() {

	'use strict';

	/**
	 * EmailNotificationService Factory
	 * @namespace Factories
	 */

	audit.factory('EmailNotificationService', EmailNotificationService);

	EmailNotificationService.$inject = [ '$rootScope', '$q', 'multiModal',
			'$backend', 'repairShopService', '$ionicLoading', 'ToastService',
			'$filter' ];

	/**
	 * @namespace EmailNotificationService
	 * @desc service used to work with notifications mails sent to repair shop
	 * @memberOf Factories
	 */

	function EmailNotificationService($rootScope, $q, multiModal, $backend,
			repairShopService, $ionicLoading, ToastService, $filter) {

		var output = {
			showModal : showModal,
			setCallback : setCallback
		};

		var callback;
		var mainEmailNotificationModal = multiModal.setBasicOptions('notification', mainEmailNotificationModal, null, 'full'),
		mainEmailNotificationModal = multiModal.setTemplateFile(mainEmailNotificationModal, 'templates/components/emailMainModal.html');

		var validSeparatorKeyCodes = [ 32, 13, 59, 44 ]; // 'space', 'enter', ';', ','

		function showModal(type, modalScope, shopId, containerList) {
			var deffered = $q.defer();

			modalScope.recipientAdr = '';
			modalScope.toggleRcpnts = true;
			modalScope.shopMail = [];

			modalScope.email = {
				from : userContext.getEmail(),
				recipients : [],
				bccPlanned : "auditcontainerplannedvisits@maersk.com",
				bccCancel : "auditcontainercancelledvisits@maersk.com",
				bccFinish : "auditcontainervisitresults@maersk.com"
			};

			modalScope.upModal = false;

			switch (type) {
			case 'planVisit':
				modalScope.title = "Plan";
				modalScope.email.subject = 'New visit planned for ' + modalScope.selectedDate;
				modalScope.email.notes = "The following containers are to be inspected during the upcoming visit: " + containerList + "." + "\n";
				modalScope.emailStateView = 'templates/components/emailPlanVisit.html';
				getEmailForShop(modalScope, shopId, deffered);
				break;
			case 'cancelVisit':
				modalScope.title = "Cancel";
				var auditDate = modalScope.auditDate || modalScope.obj.visitDate;
				modalScope.email.subject = 'The visit scheduled for ' + auditDate + ' has been canceled';
				modalScope.emailStateView = 'templates/components/emailCancelVisit.html';
				if (containerList && containerList.length > 0) {
					modalScope.email.notes = "The planned visit has been cancelled. List of containers that were planned to be inspected during the visit: " + containerList + "." + "\n";
				}
				getEmailForShop(modalScope, shopId, deffered);
				break;
			case 'finishVisit':
				modalScope.title = "Finish";
				var auditDate = $filter('date')(new Date(), 'yyyy-MM-dd')
				modalScope.email.subject = "Inspection Outcome - PDF Report for the visit completed on "
						+ auditDate;
				modalScope.email.notes = "Please find attached the PDF Report for the completed visit.";
				modalScope.emailStateView = 'templates/components/emailFinishVisit.html';
				getEmailForShop(modalScope, shopId, deffered);
				break;
			default:

			}

			modalScope.raiseModal = function() {
				var activeElementName = document.activeElement.tagName;

				if (activeElementName === 'INPUT' || activeElementName === 'TEXTAREA') {
					modalScope.upModal = true;
				}
			}

			modalScope.lowerModal = function() {

				setTimeout(function() {
					var activeElementName = document.activeElement.tagName;

					if (activeElementName !== 'INPUT' || activeElementName !== 'TEXTAREA') {
						modalScope.upModal = false;
					}
				}, 100);
			}

			modalScope.clearRecipients = function() {
				modalScope.email.recipients = "";
			}

			modalScope.sendEmail = function(from, subject, to, message, pdf, bcc) {

				// Blur input when tap on send
				document.activeElement.blur();

				var arr = [];
				var recipients = '';

				to.forEach(function(recipent) {
					arr.push(recipent.adr);
				})

				recipients = arr.join(',');
				console.log('>>>recipients', recipients);

				$ionicLoading.show(loaderConfig);

				if (subject === undefined) {
					subject = '';
				}
				if (message === undefined) {
					message = '';
				}

				getPdf(pdf).then(function(data) {
					var attrs = data;
					$backend.sendMail(from, recipients,subject, message,JSON.stringify(attrs), bcc).then(
						function() {
							$ionicLoading.hide();
							ToastService.showNotificationMsg('Your email has been sent.');
							modalScope.skip();
							arr = [];
							recipients = '';
						}).fail(
							function(error) {
								$ionicLoading.hide();
								ToastService.showNotificationMsg('Your email could not be sent. Please check your internet connection.');
								arr = [];
								recipients = '';
							});
						});
			}

			modalScope.skip = function() {
				callback();
			}

			modalScope.removeRecipientAdr = function(index) {
				modalScope.email.recipients.splice(index, 1);
				console.log("remaining addresses", modalScope.email.recipients);
			}


			/**
			 * @name addRecipientAdr
			 * @desc Adds an email to the recipients field if it's a valid address, and
			 *       when the user presses one of the characters from validSeparatorKeyCodes
			 * @param  {Object} event [$event]
			 * @param  {String} model
			 * @return N/A updates the array of recepients
			 * @memberOf EmailNotificationService.modalScope
			 */

			modalScope.addRecipientAdr = function(event, model) {
				if (validSeparatorKeyCodes.indexOf(event.keyCode) != -1) {
					event.preventDefault();
					if (validateEmail(model)) {
						modalScope.email.recipients.push({ adr : model });
						modalScope.email.recipientAdr = '';
						if (modalScope.email.recipients.length >= 5) {
							modalScope.lowerModal();
						}
					} else {
						ToastService.showNotificationMsg('Please enter a valid email address.');
					}
				}
			}


			/**
			 * @name depoAddress
			 * @desc Adds or removes shop emails from the recipients field inside emailFinishVisit.html modal
			 *       based on the toggle. By default the shop emails are added automatically.
			 *       There is a limit of 5 email addresses that can be added.
			 * @param  {Boolean} toggle
			 * @return N/A
			 * @memberOf EmailNotificationService.modalScope
			 */

			modalScope.depoAddress = function(toggle) {
				modalScope.toggleRcpnts = !modalScope.toggleRcpnts;
					if (!toggle) {
						modalScope.shopMail.forEach(function(email){
							if (modalScope.email.recipients.length < 5) {
								modalScope.email.recipients.unshift({ adr : email });
							}
						});
						modalScope.$evalAsync(function() {
							modalScope.email.recipients = modalScope.email.recipients;
						});
					} else {
						var remaingEmails = 0;
						for (var i = 0; i < modalScope.email.recipients.length; i++) {
							for (var j = 0; j < modalScope.shopMail.length; j++) {
								if (modalScope.email.recipients[i].adr === modalScope.shopMail[j]) {
									remaingEmails++
								}
							}
						}
						modalScope.$evalAsync(function() {
							modalScope.email.recipients.splice(0, remaingEmails);
							console.log("recipients", modalScope.email.recipients);
						});
					}
			};


			/**
			 * @name addRecipientOnBlur
			 * @desc Adds the address to the email recipients field if the user typed in a valid address,
			 *       but forgot to add it by pressing one of the characters from validSeparatorKeyCodes.
			 *       There is a limit of 5 email addresses that can be added.
			 * @param  {String} model
			 * @return N/A
			 * @memberOf EmailNotificationService.modalScope
			 */

			modalScope.addRecipientOnBlur = function(model) {
				if (modalScope.email.recipients && modalScope.email.recipients.length < 5 && model && model.length !== 0) {
					modalScope.email.recipients.push({ adr : model });
					modalScope.email.recipientAdr = '';
					console.log('--->>>', modalScope.email.recipients);
				}
			};

			return deffered.promise;
		}

		function setCallback(fCallback) {
			callback = fCallback;
		}


		/**
		 * @name getEmailForShop
		 * @desc Gets all the emails for a specific shop and adds them into recipients field in all email visit modals (plan/cancel/finish).
		 * @param  {Object} modalScope
		 * @param  {Number} shopId
		 * @param  {Object} deffered
		 * @return N/A
		 * @memberOf EmailNotificationService
		 */

		function getEmailForShop(modalScope, shopId, deffered) {
			repairShopService.getShopEmail(shopId).then(function(shopMail) {
				openEmailNotificationModal(shopMail, modalScope);
				deffered.resolve();
			}).fail(function(error) {
				openEmailNotificationModal(null, modalScope);
				deffered.resolve();
			});
		}
		
		function openEmailNotificationModal(shopMail, modalScope) {
			modalScope.shopMail = shopMail || [];
			console.log('<<<shopMail>>>', shopMail);

			if (shopMail) {
				shopMail.forEach(function(email){
					if (modalScope.email.recipients.length < 5) {
						modalScope.email.recipients.unshift({ adr : email });
					}
				});
			}
			
			multiModal.openModal(mainEmailNotificationModal, modalScope);
		}

		function getPdf(pdf) {
			var defer = $.Deferred();

			getFileContentAsBase64(userContext.getPdfPath(), function(
					base64String) {
				var att;
				if (base64String === "ERROR" || base64String === undefined || !pdf) {
					att = [ {
						fileName : null,
						data : null
					} ];
					defer.resolve(att);
				} else {
					var path = userContext.getPdfPath();
					var visitId = path.substring(path.length - 17,
							path.length - 4);
					var data = base64String.substring(28, base64String.length);
					att = [ {
						fileName : 'audit-report' + visitId + '.pdf',
						data : data
					} ];
					defer.resolve(att);
				}
			});

			return defer.promise();
		}

		return output;
	}

})();
