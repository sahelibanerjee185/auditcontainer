/**
 * Local DB module
 */

localDBModule.factory("$localDB", ['$logger', function($logger) {
	var localDBImpl = {
		init: function(collections, options) {
			return WL.JSONStore.init(collections, options); // returns promise;
		},
		find: function(query, options) {
			return WL.JSONStore.find(query, options); // returns promise; 
		},
		get: function(collectionName) {
			return WL.JSONStore.get(collectionName); // returns promise;
		},
		destroy: function() {
			return WL.JSONStore.destroy(); // returns promise;
		},
		closeAll: function() {
			return WL.JSONStore.closeAll(); // returns promise;
		}		
	};
	var localCollection = {};

	return {
//Initializes JSONStore
		init: function(collections, initOptions) {
			//$logger.log('[localDB] - Initializing collections!');
			return localDBImpl.init(collections, initOptions);
		},
//Finds documents in a collection using search fields
		find: function(collectionName, jsonObj, options) {
			var dfd = $.Deferred();
			var collection = localDBImpl.get(collectionName);
			if (collection == undefined) {
				//$logger.logWarning("[localDB] - Collection '" + collectionName + "' does not exist or init(...) was not called!");
				dfd.reject();
			} else {
				collection.find(jsonObj, options)
					.then(function(arrayResults) { dfd.resolve(arrayResults); })
					.fail(function(errorObject) { dfd.reject(errorObject); });
			}
			return dfd.promise();
		},
//Finds a document in a collection using it's document id
		findById: function(collectionName, id, options) {
			var dfd = $.Deferred();
			var collection = localDBImpl.get(collectionName);
			if (collection == undefined) {
				//$logger.logWarning("[localDB] - Collection '" + collectionName + "' does not exist or init(...) was not called!");
				dfd.reject();
			} else {
				collection.findById(id, options)
					.then(function(arrayResults) { dfd.resolve(arrayResults); })
					.fail(function(errorObject) { dfd.reject(errorObject); });
			}
			return dfd.promise();
		},
//Adds new documents to a collection
		add: function(collectionName, data, options) {
			var dfd = $.Deferred();

			var collection = localDBImpl.get(collectionName);

			if (collection == undefined) {
				//$logger.logWarning("[localDB] - Collection '" + collectionName + "' does not exist");
				dfd.reject();
			} else {
				collection.add(data, options)
					.then(function() {
						//$logger.log("[localDB] - Data successfully added to '" + collectionName + "'...");
						dfd.resolve();
					})
					.fail(function() {
						//$logger.logError("[localDB] - Adding data failed for '" + collectionName + "'...");
						dfd.reject();
					});
			}

			return dfd.promise();
		},
//Replaces documents in a collection. Must provide documents with id included in their format.
		replace: function(collectionName, data, options) {
			var dfd = $.Deferred();
			var collection = localDBImpl.get(collectionName);
			if (collection == undefined) {
				//$logger.logWarning("[localDB] - Collection '" + collectionName + "' does not exist");
				dfd.reject();
			} else {
				collection.replace(data, options)
					.then(function() {
						//$logger.log("[localDB] - Data successfully replaced to '" + collectionName + "'...");
						dfd.resolve();
					})
					.fail(function(error) {
						//$logger.logError("[localDB] - Replacing data failed for '" + collectionName + "'..." + error);
						dfd.reject();
					});
			}
			return dfd.promise();
		},
//Destroys data and definitions in JSONStore
		destroy: function() {
			//$logger.log("[localDB] - Destroy all collections.");
			return localDBImpl.destroy();
		},
		closeAll: function() {
			//$logger.log("[localDB] - Close all collections.");
			return localDBImpl.closeAll();
		},
		removeCollection: function(collectionName) {
			var dfd = $.Deferred();
			//$logger.log("[localDB] - removeCollection: " + collectionName);
			var collection = localDBImpl.get(collectionName);
			if (collection == undefined) {
				//$logger.logWarning("[localDB] - Collection '" + collectionName + "' does not exist");
				dfd.reject();
			} else {
				collection.removeCollection()
					.then(function() { dfd.resolve(); })
					.fail(function() { dfd.reject(); });
			}
			return dfd.promise();
		},
//This will count the number of documents in a collection
		countCollection: function(collectionName) {
			var dfd = $.Deferred();
			//$logger.log("[localDB] - countCollection: " + collectionName);
			var collection = localDBImpl.get(collectionName);
			if (collection == undefined) {
				//$logger.logWarning("[localDB] - Collection '" + collectionName + "' does not exist");
				dfd.reject();
			} else {
				collection.count()
					.then(function(count) { dfd.resolve(count); })
					.fail(function() { dfd.reject(); });
			}
			return dfd.promise();
		},
//This will completely erase the documents in the collection from the device
		erase: function(collectionName, data, options){
			var dfd = $.Deferred();
			var collection = localDBImpl.get(collectionName);
			if (collection == undefined) {
				//$logger.logWarning("[localDB] - Collection '" + collectionName + "' does not exist");
				dfd.reject();
			} else {
				options.push = false;
				collection.erase(data, options)
				.then(function() {
					//$logger.log("[localDB] - Data successfully erased for '" + collectionName + "'...");
					dfd.resolve();
				})
				.fail(function(error) {
					//$logger.logError("[localDB] - Erasing data failed for '" + collectionName + "'..." + error);
					dfd.reject();
				});
			}
			return dfd.promise();
		},
//This will delete all documents in a collection, but not destroy the collection
		clearCollection: function(collectionName){
			//$logger.log("[localDB] - Erasing all data in '" + collectionName + "'...");
			return localDBImpl.get(collectionName).clear();
		},
//This will generate an id (integer) that is unique for the field in a collection		
		generatUniqueId: function(collectionName, fieldName){
			var dfd = $.Deferred();
			var collection = localDBImpl.get(collectionName);
			if (collection == undefined) {
				//$logger.logWarning("[localDB] - Collection '" + collectionName + "' does not exist");
				dfd.reject();
			} else {
				collection.find({}, {})
				.then(function(arrayResults) { 
					var isUnique = false;
					var newId;
					while (!isUnique){
						isUnique = true;
						newId = Math.floor(Math.random() * 20000);
						for (var document in arrayResults){
							if (document[fieldName] == newId){
								isUnique = false;
								break;
							}
						}
					}
					dfd.resolve(newId); 
				})
				.fail(function(errorObject) {
					dfd.reject(errorObject); 
				});
			}
			return dfd.promise();
		}
	};
}]); 