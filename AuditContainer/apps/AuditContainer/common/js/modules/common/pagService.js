audit
    .factory('pagService', function(){
    	
    	var buildPagination = function buildPagination(currentPage, arrayLength, entriesPerPage, pagesLimit) {
            var paginationArray = [];
            var firstPage = 1;
            var lastPage = getLastPage(arrayLength, entriesPerPage);

            if (arrayLength > 0) {
                paginationArray.push({
                    text: '<<',
                    disabled: (currentPage == firstPage),
                    page: firstPage
                }); // go to first page
                paginationArray.push({
                    text: '<',
                    disabled: (currentPage == firstPage),
                    page: currentPage - 1
                }); // go to previous page
                if (lastPage >= 1) {
                    paginationArray.push({
                        text: '1',
                        disabled: (currentPage == firstPage),
                        page: firstPage
                    }); // first page
                }
                // check to see if we need a '...' block
                if ((currentPage - pagesLimit) > 2) {
                    paginationArray.push({
                        text: '...',
                        disabled: true
                    }); // '...' block
                }
                // get pages before current page
                if (currentPage > 1) {
                    for (var i = 0; i < pagesLimit; i++) {
                        var pageNo = currentPage - pagesLimit + i;
                        if (pageNo > firstPage) {
                            paginationArray.push({
                                text: pageNo,
                                disabled: false,
                                page: pageNo
                            });
                        }
                    }
                }

                if (currentPage != firstPage && currentPage != lastPage) {
                    paginationArray.push({
                        text: currentPage,
                        disabled: true,
                        page: currentPage
                    }); // current page
                }

                // get pages after current page
                if (currentPage >= 1) {
                    // up to a max of pagesLimit
                    for (var i = 1; i <= pagesLimit; i++) {
                        var pageNo = currentPage + i;
                        if (pageNo < lastPage) {
                            paginationArray.push({
                                text: pageNo,
                                disabled: false,
                                page: pageNo
                            });
                        }
                    }
                }

                // check to see if we need a '...' block
                if ((currentPage + pagesLimit) < (lastPage - 1)) {
                    paginationArray.push({
                        text: '...',
                        disabled: true
                    }); // '...' block
                }

                if (lastPage > 1) {
                    paginationArray.push({
                        text: lastPage,
                        disabled: (currentPage == lastPage),
                        page: lastPage
                    }); // last page
                }

                paginationArray.push({
                    text: '>',
                    disabled: (currentPage == lastPage),
                    page: currentPage + 1
                }); // go to first page
                paginationArray.push({
                    text: '>>',
                    disabled: (currentPage == lastPage),
                    page: lastPage
                }); // go to previous page
            }

            return paginationArray;
        }
        
        function getLastPage(arrayLength, entriesPerPage){
            return Math.ceil(arrayLength/entriesPerPage);
        }

        return {
            buildPagination: buildPagination
        }
    	
    });