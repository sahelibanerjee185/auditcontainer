/**
 * factory for updating the Angular $locale used by the date-picker in order to change its language during runtime, rather than having to stop 
 * the app in order to load the localization file and have Angular reset in order to change the language
 */

localeUpdate.factory('localeUpdate',['$locale', function($locale){
	
	return{
	//!!MAKE SURE THAT DAYS ARE ALIGNED TO HOW BOOTSTRAP UI DATEPICKER READS THEM: SUNDAY IS THE FIRST DAY CURRENTLY
		changeLocale : function(){
			    var weekdays = [];
			    addMessages(weekdays, ['sun', 'mon', 'tue', 'wed', 'thu', 'fri', 'sat']);
			    var pos = 0;
			    for(day in weekdays){
			        $locale.DATETIME_FORMATS.SHORTDAY[pos] = weekdays[day];
			        pos++;
			    }
			    var months = [];
			    addMessages(months, ['jan', 'feb', 'mar', 'apr', 'may', 'jun', 'jul', 'aug', 'sept', 'oct', 'nov', 'dec']);
			    pos = 0;
			    for(month in months){
			        $locale.DATETIME_FORMATS.MONTH[pos] = months[month];
			        pos++;
			    }
		}
	}
}])