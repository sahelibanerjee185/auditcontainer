/**
 * Dec 7, 2016
 * toastService.js
 * StavaracheVlad
*/

(function(){
	'use strict';

	audit
    .factory('ToastService', ToastService);

	ToastService.$inject = [];

	function ToastService() {

		var appendedToast = false;
		var messageDuration = 5000; // 5 sec
		var timer;
		var toastContainer;
		var toastIcon;
		var toastText
		var toastMessage = "Your visit has been processed."
		var _acceptedClass = "acceptedIcon";
		var _rejectedClass = "rejectedIcon";
		var _hidden = "noVisibility";
		var _visible = "addVisibility";
		var _padLeftSmall="padLeftSmall";
		var _padLeftBig="padLeftBig";
		var customClass = '';
		var iconPad = 'iconPadLeft';

		var service = {
			showSuccessMessage : showSuccessMessage,
			showWarningMessage : showWarningMessage,
			showNotificationMsg	: showNotificationMsg,
			showSuccessMessagePlural : showSuccessMessagePlural,
			showWarningMessagePlural : showWarningMessagePlural

		};

		// self init

		init();

		return service;

		function init() {
			toastContainer = document.createElement("DIV");
			toastContainer.id = "toastContainer";
			toastContainer.classList.add(_hidden);

			toastIcon = document.createElement("DIV");
			toastIcon.classList.add("toastIcon");

			toastText = document.createElement("SPAN");
			toastText.classList.add("toastText");
			setToastText(toastMessage);

			toastContainer.appendChild(toastIcon);
			toastContainer.appendChild(toastText);

			// Hide on tap
			toastContainer.addEventListener('touchstart', hide);

			body.appendChild(toastContainer);
		}

		function showSuccessMessage(message, callback) {
			setSuccessToastIcon();
			setToastText(toastMessage);
			showMessage(message, callback);
		}

		function showSuccessMessagePlural(message, callback) {
			setSuccessToastIcon();
			showMessage( "Your visits have been processed.", callback);
		}

		function showWarningMessage(message, callback) {
			setWarningToastIcon();
			setToastText(toastMessage);
			showMessage(message, callback );
		}
		function showWarningMessagePlural(message, callback) {
			setWarningToastIcon();
			showMessage( "Your visits have been processed.", callback);
		}

		function showNotificationMsg(message, callback) {
			restartToast()
			removeIcons();
			showMessage(message, callback);

		}

		function removeCustomClass(){
			toastText.classList.remove(iconPad);
		}

		function showMessage(message, callback) {
			// Attach callback
			var _callback = function() {
				if (typeof callback === 'function') callback();
			}

			toastContainer.addEventListener('touchstart', function cb() {
				clearTimer();
				_callback();
				toastContainer.removeEventListener('touchstart', cb);
			});

			if (message) setToastText(message);

			// Only display toast for a limited time
			startTimer();

			show();
		}

		function startTimer() {
			timer = setTimeout(function(){
				removeAllEventListeners();
				removeCustomClass();
			}, messageDuration);
		}

		function clearTimer() {
			clearTimeout(timer);
		}

		function restartToast(){
			clearTimer();
			removeAllEventListeners();
			removeCustomClass();
		}

		function removeAllEventListeners() {
			toastContainer.remove();
			init();
		}

		function show() {
			toastContainer.classList.add(_visible);
		}

		function hide() {
			toastContainer.classList.remove(_visible);
		}

		function setToastText(text) {
			toastText.textContent = text;
		}

		function removeIcons() {
			toastIcon.classList.remove(_acceptedClass, _rejectedClass);
		}

		function setSuccessToastIcon() {
			toastIcon.classList.remove(_acceptedClass, _rejectedClass);
			toastText.classList.remove(iconPad);
			toastIcon.classList.add(_acceptedClass);
			toastText.classList.add(iconPad);
		}

		function setWarningToastIcon() {
			toastIcon.classList.remove(_acceptedClass, _rejectedClass);
			toastText.classList.remove(iconPad);
			toastIcon.classList.add(_rejectedClass);
			toastText.classList.add(iconPad);
		}

		function setToastPositioning(className){
			toastContainer.classList.remove( className);
			toastContainer.classList.add(className);
		}
	}
})();
