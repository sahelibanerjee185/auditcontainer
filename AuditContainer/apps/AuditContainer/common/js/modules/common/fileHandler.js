
/**
 * Dec 22, 2015
 * fileHandler.js
 * AlinPreda
 */


localDBModule.factory("$fileHandler", ['$logger', function($logger) {
	var localImpl = {
		//returns the cordova file writer for the file on Android
		fileWriterAndroid : function(fileName, promise, writerFunction){
			var fail = function(error){
				promise.reject(error)
			}
			var gotFS = function(fileSystem){
				fileSystem.root.getFile(fileName, {create: true, exclusive: false}, gotFileEntry, fail);
			}
			
			var gotFileEntry = function(fileEntry){
				fileEntry.createWriter(writerFunction, fail);
			}
			window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, gotFS, fail);
			
		},
		//returns the cordova file writer for the file on iOS
		fileWriterIOS : function(fileName, promise, writerFunction){
			alert("Not implemented yet for iOS!")
		},
		//returns the cordova file for the file on Android
		fileAndroid : function(fileName, promise, readerFunction){
			var fail = function(error){
				promise.reject(error)
			}
			var gotFS = function(fileSystem){
				fileSystem.root.getFile(fileName, {create: true, exclusive: false}, gotFileEntry, fail);
			}
			
			var gotFileEntry = function(fileEntry){
				fileEntry.file(readerFunction, fail);
			}
			window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, gotFS, fail);
			
		},
		//returns the cordova file for the file on iOS
		fileIOS : function(fileName, promise, readerFunction){
			alert("Not implemented yet for iOS!")
		}
	}
	return {
		//writes the base64 string to the file from the url
		Base64ToUrl : function(base64,url){
			var defer = $.Deferred();
			
			var writerFunction = function(writer){
				writer.onwriteend = function(evt) {
					defer.resolve();
				}
				writer.write(base64);
			};
			
			if (device.platform == "Android")
				localImpl.fileWriterAndroid(url, defer, writerFunction);
			else
				localImpl.fileWriterIOS(url, defer, writerFunction);
			
			return defer.promise();
		},
		//reads the base64 string from the file specified by the url
		UrlToBase64 : function(url){
			var defer = $.Deferred();
			
			var readerFunction = function(file){
				var reader = new FileReader();
		        reader.onloadend = function(evt) {
		            defer.resolve(evt.target.result);
		        };
		        reader.readAsText(file);
			};
			
			if (device.platform == "Android")
				localImpl.fileAndroid(url, defer, readerFunction);
			else
				localImpl.fileIOS(url, defer, readerFunction);
			
			return defer.promise();
		},
	}
}]);