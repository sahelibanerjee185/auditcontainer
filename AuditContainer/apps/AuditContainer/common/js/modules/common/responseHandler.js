/**
 * Mar, 31 2016
 * responseHandler.js
 * Stavarache Vlad
 */

(function(){
	'use strict';
	
	audit
    .factory('responseHandler', responseHandler);
	
	responseHandler.$inject = ['multiModal', '$rootScope', '$location', '$ionicLoading'];
	
	function responseHandler(multiModal, $rootScope, $location, $ionicLoading) {
	
		var service = {
			_defaultResponse : _defaultResponse,
			feedback: feedback
		};
		
		return service;
		
		function _defaultResponse(errorCode) {
			var response;
			
			switch (errorCode) {
				case 102:
				case 105:
					response = "Your session has expired. Please login again.";
					break;
				case 110: 
					// update statement that doesn't affect any rows
					response = "Could not perform required changes.";
					break;
				default:
					response = "Could not connect to the server. Please try again later.";
			}
			
			return response;
		}
		
		function feedback(error) {
			var errorCode;
			
			if (error && error.invocationResult && error.invocationResult.errorCode) {
				errorCode = error.invocationResult.errorCode;
			}
			
			var $errorFeedback = multiModal.setBasicOptions('notification', 'connectionError', _defaultResponse(errorCode));
			
			if (errorCode == 102 || errorCode == 105) {
				$errorFeedback = multiModal.setActions($errorFeedback, function(){
					$ionicLoading.hide();
					$rootScope.$evalAsync(function(){
						$location.path('/');
					});
				});
			}
			
			multiModal.openModal($errorFeedback, $rootScope);
		}
	}
})();