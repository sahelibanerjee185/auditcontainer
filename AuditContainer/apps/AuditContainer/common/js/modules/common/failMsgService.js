(function(){
	'use strict';
	
	audit
    .factory('FailMsgService', FailMsgService);
	
	FailMsgService.$inject = [];
	
	function FailMsgService() {
		
		function getFaildComment(serverComment){
			var newComm;
			var code = getFailCode(serverComment);
			
			switch(code){
				case "101": newComm = "Certificate ID not provided."
					break;
				case "102": newComm = "Certificate ID is not valid."
					break;
				case "103": newComm = "Certificate does not have access to the work order's shop."
					break;
				case "104": newComm = "Certificate role does not have permission to change work order status."
					break;
				case "105": newComm = "Certificate does not have permission to reject work order - Pending MSL Approval."
					break;
				case "106": newComm = "Certificate does not have permission to accept work order - Suspended to MSL."
					break;
				case "107": newComm = "Unable to process your request. Please contact help desk."
					break;
				case "108": newComm = "Your request could not change work order's current status."
					break;
			}
			
			return newComm;
		}
		
		function getFailCode(comment){
			return comment.slice(0,3);
		}
		
		return {
				getFaildComment : getFaildComment
		};
		
	}
})();