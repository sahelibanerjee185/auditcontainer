audit.filter('countryFilter', function(searchUtils) {
	 var compareStr = function(stra, strb) {
         stra = ("" + stra).toLowerCase();
         strb = ("" + strb).toLowerCase();
         return stra.indexOf(strb) !== -1;
     }
	
	 return function(repairshops, currentCountryName){
		 if (!angular.isUndefined(repairshops) && !angular.isUndefined( currentCountryName) &&  currentCountryName != 'Please select a country') {
	        var arrayToReturn = [];
	        if(compareStr(currentCountryName, 'All Countries')){
	        	 angular.forEach(repairshops, function (repairshop){
	        		 arrayToReturn.push(repairshop);
	        	 })
	        	 return arrayToReturn;
        	} 
	        	else
	        angular.forEach(repairshops, function (repairshop){
	        	if( compareStr(repairshop.country, currentCountryName)){
	        		arrayToReturn.push(repairshop);
	        	}
	        });
	        return arrayToReturn;
		 } 
		 else{
			 return repairshops;
		 }
	 }
	});

