
audit.filter('generalFilter', function(searchUtils) {
	 var compareStr = function(stra, strb) {
         stra = ("" + stra).toLowerCase();
         strb = ("" + strb).toLowerCase();
         return stra.indexOf(strb) !== -1;
     }
	
	 return function(filteredData, filterParam, filterFor){
		 if (!angular.isUndefined(filteredData) && !angular.isUndefined( filterParam)) {
			 var arrayToReturn = [];
			 if(compareStr (filterParam, 'status')){
				   angular.forEach(filteredData, function (container){
					   if(container[filterParam] === filterFor){
						   arrayToReturn.push(container);
					   }
				   })
		        	 return arrayToReturn;
			   }
			 
			   if(compareStr (filterParam, 'containerNo')){
				   angular.forEach(filteredData, function (container){
					   if(container[filterParam].substring(0,4) === filterFor){
						   arrayToReturn.push(container);
					   }
		        	 })
		        	 return arrayToReturn;
			   }
			   
			   if(compareStr (filterParam, 'visitDate')){
				   angular.forEach(filteredData, function (container){
					   if(container[filterParam] === filterFor){
						   arrayToReturn.push(container);
					   }
		        	 })
		        	 return arrayToReturn;
				   
			   }
			   
			   if(compareStr (filterParam, 'workOrderNo')){
				   angular.forEach(filteredData, function (container){
					   if(container.workOrderId == filterFor){
						   arrayToReturn.push(container);
					   }
		        	 })
		        	 return arrayToReturn;
			   }
		
	        if(compareStr (filterParam, 'showAll')){
	        	 angular.forEach(filteredData, function (container){
	        		 arrayToReturn.push(container);
	        	 })
	        	 return arrayToReturn;
	        } else
	        angular.forEach(filteredData, function (container){
	        	if( compareStr(container[filterParam], filterFor)){
	        		arrayToReturn.push(container);
	        	}
	        });
	        
	        return arrayToReturn;
		 } 
		 else{
			 return filteredData;
			
		 }
	 }
	});

audit.filter('countryFilter',function(){
	 var compareStr = function(stra, strb) {
         stra = ("" + stra).toLowerCase();
         strb = ("" + strb).toLowerCase();
         return stra.indexOf(strb) !== -1;
     }
			
	 return function(allCountries,countrySearch){
		 var originalSearch = countrySearch;
//		 clearTimeout(timeoutSearchCountry);
		 var arrayToReturn = [];
		 if(countrySearch ==''){
			 return arrayToReturn;
		 } else {
			 
//			 var timeoutSearchCountry=setTimeout(function(){
				 if(originalSearch=countrySearch){
					 if (!angular.isUndefined(allCountries) && !angular.isUndefined( countrySearch)){
						 angular.forEach(allCountries,function(country){
							 var lowerStr = (country.name + "").toLowerCase(); 
							 if(lowerStr.indexOf(countrySearch.toLowerCase()) === 0){
								 arrayToReturn.push(country);
							 }
						 })
						 
					}
					return arrayToReturn;
				 }
//			 },300)
			
			
		 }
		 
	 }
})

audit.filter('betweenDatesFilter', function(){
	return function(dataArray, dateField, fromDate, toDate) {
		// fromDate is newer date - eg: 14 Mar
		// toDate is older date - eg: 7 Mar		
		var filteredDataArray = [];
		
		var fromTimeStamp = fromDate.getTime();
		var toTimeStamp = (toDate ? toDate.getTime() : null);
		
		for (var i = 0; i < dataArray.length; i++) {
			var itemInfo = dataArray[i];
			if (toTimeStamp) {
				if (itemInfo[dateField] <= fromTimeStamp && itemInfo[dateField] >= toTimeStamp) {
					filteredDataArray.push(itemInfo);
				}
			} else {
				if (itemInfo[dateField] <= fromTimeStamp) {
					filteredDataArray.push(itemInfo);
				}
			}
		}
		
		return filteredDataArray;
	}
})

audit.filter('multiSearch',function(){
	return function(dataArray,searchFields,searchString,searchFunction){
		if(typeof searchString != 'string' || (searchString.trim().length == 0)){
			return dataArray;
		}
		if(typeof searchFunction == 'function'){
			var searchFunction=searchFunction;
		}else{
			var searchFunction =defaultSearch;
		}
		var searchStringArray=searchString.trim().split(" ");
		
		var currentResults = dataArray;
		
		for(var i=0;i<searchStringArray.length;i++){
			if(currentResults.length == 0)
				break;
			
			var searchString = searchStringArray[i];
			var filteredDataArray=[];
			for(var j=0;j<currentResults.length;j++){
				var currentEntry =currentResults[j];
			
					for(var k=0;k<searchFields.length;k++){
						if(searchFunction(currentEntry[searchFields[k]],searchString)){
							filteredDataArray.push(currentEntry);
							break;
						}
					}
			
			}
			currentResults = filteredDataArray;
		}
		return currentResults;
	}
	function defaultSearch(actual,expected){
		var lowerStr=(actual+"").toLowerCase();
		return lowerStr.indexOf(expected.toLowerCase())>=0;
	}
	function scrollUp(){
		 $ionicScrollDelegate.$getByHandle('upComingVisitsScroll').scrollTo(0,0);
	}
});

