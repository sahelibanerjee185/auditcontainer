audit.filter('statusFilter', function(searchUtils) {
	 var compareStr = function(stra, strb) {
         stra = ("" + stra).toLowerCase();
         strb = ("" + strb).toLowerCase();
         return stra.indexOf(strb) !== -1;
     }
	
	 return function(kpiDepots, currentStatus){
		 if (!angular.isUndefined(kpiDepots) && !angular.isUndefined(currentStatus)) {
	        var arrayToReturn = [];
	        if(compareStr(currentStatus, 'all')){
	        	 angular.forEach(kpiDepots, function (depot){
	        		 arrayToReturn.push(depot);
	        	 })
	        	 return arrayToReturn;
        	} 
	        	else if( compareStr(currentStatus,'passed')){
	        		angular.forEach(kpiDepots, function (depot){
	        				if( compareStr(depot.status, 'Passed')){
	        					arrayToReturn.push(depot);
	        				}
	        		})
	        				return arrayToReturn;
	        		}
	        		else if( compareStr(currentStatus,'rejected')){
	        			angular.forEach(kpiDepots, function (depot){
	        				if( compareStr(depot.status, 'Failed')){
	        					arrayToReturn.push(depot);
	        				}
	        		})
	        		return arrayToReturn;
	        		
	        			
	        }
	        return arrayToReturn;
	 } 
		 else{
			 return kpiDepots;
		 }
	   }
	
});

