/**
 * userContext
 * @namespace userContext
 */

/**
 * @name userContext
 * @desc Global object used for 
 * handling user session information
 * @param N/A
 * @memberOf userContext
 */
var userContext = (function() {
	var contextData = {
		user : {				
			token : null,
			username : null,
			email: null,
			certificateId: null,
			lastLoginTime : null
		},
		certificateChanged : false,
		remember : false,
		isLoaded : false,
		planVisitDataLoaded : false,
		codeDescriptionsDataLoaded : false,
		kpiDataLoaded : false,
		staticDataNeedsRefresh : false,
		codeDescriptionsNeedRefresh : false
	}

	var service = {
		load : load,
		unload : unload,
		isLoaded : isLoaded,
		setUsername :setUsername,
		getUsername:getUsername,
		setRemember :setRemember,
		getRemember :getRemember,
		setCertificateHasChanged : setCertificateHasChanged,
		changedCertificate : changedCertificate,
		setToken:setToken,
		getToken:getToken,
		planVisitLoaded:planVisitLoaded,
		planVisitIsLoaded:planVisitIsLoaded,
		planVisitNotLoaded:planVisitNotLoaded,
		codeDescriptionsLoaded: codeDescriptionsLoaded,
		codeDescriptionsNotLoaded: codeDescriptionsNotLoaded,
		codeDescriptionsAreLoaded: codeDescriptionsAreLoaded,
		kpiLoaded:kpiLoaded,
		kpiNotLoaded:kpiNotLoaded,
		isKpiLoaded:isKpiLoaded,
		setStaticDataNeedsRefresh:setStaticDataNeedsRefresh,
		staticDataNeedsRefresh:staticDataNeedsRefresh,
		setCodeDescriptionsDataNeedsRefresh: setCodeDescriptionsDataNeedsRefresh,
		codeDescriptionsDataNeedsRefresh: codeDescriptionsDataNeedsRefresh,
		setPdfPath:setPdfPath,
		getPdfPath:getPdfPath,
		setEmail:setEmail,
		getEmail:getEmail,
		setCertificateId:setCertificateId,
		getCertificateId:getCertificateId,
		setLoginTime : setLoginTime,
		getLoginInterval : getLoginInterval,
		getLoginTime : getLoginTime
	}

	return service;
	
	function unload() {
		contextData.isLoaded = false;
		WL.Logger.debug("User Context has been unloaded!");
	}

	function isLoaded() {
		return contextData.isLoaded;
	}
	
	/**
	 * @name setCertificateHasChanged
	 * @desc Sets the contextData.certificateChanged property
	 * used to tell if the user changes certificate,
	 * case when local data is erased and retrieved from the server
	 * based on new certificate
	 * @param {Bool} value
	 * @returns {N/A}
	 * @memberOf userContext
	 */
	function setCertificateHasChanged(value) {
		contextData.certificateChanged = value;
	}
	
	function changedCertificate() {
		return contextData.certificateChanged;
	}
	

	function planVisitLoaded() {
		contextData.planVisitDataLoaded = true;
	}
	
	function planVisitNotLoaded() {
		contextData.planVisitDataLoaded = false;
	}
	
	/**
	 * @name planVisitIsLoaded
	 * @desc Used to tell if we have local data already
	 * for countries and shops
	 * @param {N/A}
	 * @returns {Bool}
	 * @memberOf userContext
	 */
	function planVisitIsLoaded() {
		return contextData.planVisitDataLoaded;
	}
	
	function codeDescriptionsLoaded() {
		contextData.codeDescriptionsDataLoaded = true;
	}
	
	function codeDescriptionsNotLoaded() {
		contextData.codeDescriptionsDataLoaded = false;
	}
	
	/**
	 * @name codeDescriptionsAreLoaded
	 * @desc Used to tell if code descriptions are loaded
	 * @param {N/A}
	 * @returns {Bool}
	 * @memberOf userContext
	 */
	function codeDescriptionsAreLoaded() {
		return contextData.codeDescriptionsDataLoaded;
	}
	
	function kpiLoaded() {
		contextData.kpiDataLoaded = true;
	}
	
	function kpiNotLoaded() {
		contextData.kpiDataLoaded = false;
	}
	
	/**
	 * @name isKpiLoaded
	 * @desc Used to tell if KPI data loaded
	 * @param {N/A}
	 * @returns {Bool}
	 * @memberOf userContext
	 */
	function isKpiLoaded() {
		return contextData.kpiDataLoaded;
	}
	
	function setStaticDataNeedsRefresh() {
		contextData.staticDataNeedsRefresh = true;
	}
	
	/**
	 * @name staticDataNeedsRefresh
	 * @desc Used to tell if we need to retrieve
	 * country and repair shop data again from the server
	 * and update local collections 
	 * @param {N/A}
	 * @returns {Bool}
	 * @memberof userContext
	 */
	function staticDataNeedsRefresh() {
		return contextData.staticDataNeedsRefresh;
	}
	
	function setCodeDescriptionsDataNeedsRefresh() {
		contextData.codeDescriptionsNeedRefresh = true;
	}
	
	function codeDescriptionsDataNeedsRefresh() {
		return contextData.codeDescriptionsNeedRefresh;
	}
	
	function getCertificateId() {
		return contextData.user.certificateId;
	}
	
	/**
	 * @name setCertificateId
	 * @desc sets the certificate id in the context and local db
	 * @param {String} newId
	 * @returns {Promise}
	 * @memberof userContext
	 */
	function setCertificateId(newID){
		var defer = $.Deferred();
		
		var injector = angular.injector([ 'localDBModule' ]);
		injector.invoke([ '$settings', function($settings) {
			$settings.set('user','certificateId',newID)
			.then(function() {
				contextData.user.certificateId = newID;
				defer.resolve();
			}).fail(function(data){
			})
		}]);
		
		return defer.promise();
	}
	
	/**
	 * @name setLoginTime
	 * @desc saves the successful online login time 
	 * in json store and information is used to handle
	 * logging in online
	 * @param {String} newValue
	 * @returns {Promise}
	 * @memberof userContext
	 */
	function setLoginTime(newValue){
		var defer = $.Deferred();
		if(!newValue) newValue = '';
		
		var injector = angular.injector([ 'localDBModule' ]);
		injector.invoke([ '$settings', function($settings) {
			$settings.set('user','lastLoginTime',newValue)
			.then(function() {
				contextData.user.lastLoginTime = newValue;
				defer.resolve();
			}).fail(function(data){
				console.log(data);
			})
		}]);
		
		return defer.promise();
	}
	
	function getLoginInterval(){
		return {
			begin : !!contextData.user.lastLoginTime ?  contextData.user.lastLoginTime : null,
			end : !!contextData.user.lastLoginTime ?  contextData.user.lastLoginTime+604800000 : null
		} 
	}
	
	function getLoginTime(){
		return !!contextData.user.lastLoginTime ? contextData.user.lastLoginTime : null;
	}
	
	/**
	 * @name load
	 * @desc called in wlCommonInit at app start time
	 * Populates contextData from json store values
	 * @param {N/A}
	 * @returns {Promise}
	 * @memberof userContext
	 */
	function load() {
		var defer = $.Deferred();
		
		var injector = angular.injector([ 'localDBModule' ]);
		injector.invoke([ '$localDB', function($localDB) {
			$localDB.find('user', {}, {})
			.then(function(userData) {
				console.log('User data at loading!',userData);
				
				contextData.user.token = userData[0].json.token;
				contextData.user.username = userData[0].json.username;
				contextData.user.certificateId = userData[0].json.certificateId;
				contextData.user.lastLoginTime = userData[0].json.lastLoginTime;
				
				return $localDB.find('settings', {}, {})
			}).then(function(settings) {
				contextData.remember = settings[0].json.remember;
				contextData.isLoaded = true;
				WL.Logger.debug("User Context succesfully loaded!");
				defer.resolve();
			});
		} ]);
		
		return defer.promise();
	}
	
	function setUsername(newUsername) {
		var defer = $.Deferred();
		
		var injector = angular.injector([ 'localDBModule' ]);
		injector.invoke([ '$settings', function($settings) {
			$settings.set('user','username',newUsername)
			.then(function() {
				contextData.user.username = newUsername;
				defer.resolve();
			})
		}]);
		
		return defer.promise();
	}
	
	function setEmail(email) {
		contextData.user.email = email;
	}
	
	function getEmail() {
		return contextData.user.email;
	}
	
	function getUsername(){
		if (isLoaded())
			return contextData.user.username;
		else
			return "User Context is not loaded";
	}
	
	/**
	 * @name setToken
	 * @desc Deprecated
	 * @memberof userContext
	 */
	function setToken(newToken){
		var defer = $.Deferred();
		
		var injector = angular.injector([ 'localDBModule' ]);
		injector.invoke([ '$settings', function($settings) {
			$settings.set('user','token',newToken)
			.then(function() {
				contextData.user.token = newToken;
				defer.resolve();
			})
		}]);
		
		return defer.promise();
	}
	
	function getToken() {
		if (isLoaded())
			return contextData.user.token;
		else
			return "User Context is not loaded";
	}
	
	/**
	 * @name setRemember
	 * @desc Deprecated
	 * @memberof userContext
	 */
	function setRemember(rememeber){
		var defer = $.Deferred();
		
		var injector = angular.injector([ 'localDBModule' ]);
		injector.invoke([ '$settings', function($settings) {
			$settings.set('settings','remember',rememeber)
			.then(function() {
				contextData.remember = rememeber;
				defer.resolve();
			})
		}]);
		
		return defer.promise();
	}
	
	function getRemember(){
		if (isLoaded())
			return contextData.remember;
		else
			return "User Context is not loaded";
	}
	
	function setPdfPath(path){
		contextData.pdfPath = path;
	}
	
	function getPdfPath(){
		return contextData.pdfPath;
	}
	
}());
