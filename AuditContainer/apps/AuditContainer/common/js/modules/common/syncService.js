/**
 * Jun 8, 2016
 * syncService.js
 * StavaracheVlad
*/

/**
 * syncService Factory
 * @namespace Factories
 */
(function(){
	'use strict';

	audit
    .factory('syncService', syncService);

	syncService.$inject = ['postLoginInit', '$localDB', '$surveyModule', '$ionicLoading', '$location', '$depot', '$rootScope', 'multiModal','ToastService','$timeout'];

	/**
     * @namespace syncService
     * @desc Service that manages sync operations with server
     * @memberOf Factories
     */
	function syncService(postLoginInit, $localDB, $surveyModule, $ionicLoading, $location, $depot, $rootScope, multiModal,ToastService,$timeout) {

		var refreshInterval;
		var refreshIntervalTime = 3600000; // 1 h
		var visitsIntervalToRefresh = 10800000; // 3 h
		var staticDataIntervalToRefresh = 86400000; // 24 h

		var service = {
			syncAll : syncAll,
			getRefreshInterval : getRefreshInterval,
			clearRefreshInterval : clearRefreshInterval,
			updateRefreshTimer : updateRefreshTimer,
			updateVisitRefreshTimer : updateVisitRefreshTimer,
			updateStaticDataRefreshTimer : updateStaticDataRefreshTimer,
			setVisitRefreshInterval : setVisitRefreshInterval
		};

		return service;

		/**
	     * @name syncAll
	     * @desc called when user taps on sync button from native header
		 * calls sync method in postLoginInit and updates
		 * the static data refresh time as well
	     * @param {N/A}
	     * @returns {Promise}
	     * @memberof Factories.syncService
	     */
		function syncAll() {
			return postLoginInit.sync().then(function(){
				updateStaticDataRefreshTimer().then(function(){
					updateUIIfNeeded();
				});
			}).fail(function(error, someVisitsSynced){
				WL.Logger.error("[SyncService][syncAll] username: " + userContext.getUsername() + JSON.stringify(error));
				
				if (someVisitsSynced) {
					ToastService.showNotificationMsg('Could not refresh visit data, please use the sync button again to retrieve visit information from the server.');
				} else {
					ToastService.showNotificationMsg('Could not connect to the server. Please check the internet connection.');
				}
			});
		}

		/**
	     * @name updateUIIfNeeded
	     * @desc called after sync is completed, for some screens
		 * repopulates screen data
	     * @param {N/A}
	     * @returns {Promise}
	     * @memberof Factories.syncService
	     */
		function updateUIIfNeeded() {
			var dfd = $.Deferred();

			if ($location.$$path == "/depotDetails/" || $location.$$path == "/unitRefer") {
				// get visit id
				var visitId = $depot.getobject().visitId;
				var visitStatus;
				$localDB.find('calendarVisits', {visitId : visitId}, {exact: true}).then(function(visits){
					if (visits && visits.length > 0) {
						visitStatus = visits[0].json.auditStatus;
						$depot.getVisitInfo(visitId, visitStatus).then(function(){
							$rootScope.$apply();
							dfd.resolve();
						})
					} else {
						dfd.resolve();
					}
				});
			} else if ($location.$$path == "/surveyTab") {
				$rootScope.$apply();
				dfd.resolve();
			} else {
				dfd.resolve();
			}

			return dfd.promise();
		}

		function getRefreshInterval() {
			return refreshInterval;
		}

		function clearRefreshInterval() {
			clearInterval(refreshInterval);
		}

		function updateVisitRefreshTimer() {
			return updateRefreshTimer('visitRefreshTimer');
		}

		function updateStaticDataRefreshTimer() {
			return updateRefreshTimer('staticDataRefreshTimer');
		}

		function setVisitRefreshInterval() {
			clearRefreshInterval();

			refreshInterval = setInterval(function(){
				_refreshHandler();
			}, refreshIntervalTime);
		}

		/**
	     * @name _refreshHandler
	     * @desc Refreshes visit data periodically
		 * based on refreshIntervalTime
	     * @param {N/A}
	     * @returns {Promise}
	     * @memberof Factories.syncService
	     */
		function _refreshHandler() {
			console.log('refresh');
			var that = this;

			var currentTime = (new Date()).getTime();

			var options = {
				filter: ['timestamp'],
				limit: 1
			};

			var dfd = $.Deferred();
			var visitsDfd = $.Deferred();

			WL.JSONStore.get('visitRefreshTimer').find({}, options).then(function(timerData){

				if (timerData.length > 0) {
					var lastRefreshTime = timerData[0].timestamp;

					if ((currentTime - lastRefreshTime) > visitsIntervalToRefresh) {
						$surveyModule.refreshVisitData().then(function(){
							updateUIIfNeeded().then(function(){
								visitsDfd.resolve();
							});
						}).fail(function(){
							visitsDfd.resolve();
						});
					} else {
						visitsDfd.resolve();
					}
				}
			});

			visitsDfd.then(function(){
				WL.JSONStore.get('staticDataRefreshTimer').find({}, options).then(function(timerData){
					if (timerData.length > 0) {
						var lastRefreshTime = timerData[0].timestamp;

						if ((currentTime - lastRefreshTime) > staticDataIntervalToRefresh) {
							$.when.apply(postLoginInit.setRepairShops(), postLoginInit.setCountries()).then(function(){
								updateStaticDataRefreshTimer();
							});
						}
					}
				});
			})
		}

		function updateRefreshTimer(collection) {
			var dfd = $.Deferred();
			var timerData;

			$localDB.clearCollection(collection).then(function(){
				var currentTime = (new Date()).getTime();
				timerData = [{timestamp : currentTime}];
				$localDB.add(collection, timerData, {}).then(function(){
					dfd.resolve();
				});
			});

			return dfd.promise();
		}
	}
})();
