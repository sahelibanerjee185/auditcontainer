/**
 * Nov 18, 2015
 * commonOperationsModule.js
 * StavaracheVlad
 */
audit.factory('$commonOperations', function ($localDB, multiModal, $filter){
	return {
		
		updateLayout : function(scope){   //force refresh view if another digest is not in progress
			if (!scope.$$phase){
				scope.$apply();
			}
		},
	}
});