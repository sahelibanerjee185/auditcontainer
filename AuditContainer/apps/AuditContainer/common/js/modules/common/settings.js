/**
 * Aug 7, 2015
 * settings.js
 * AlinPreda
 */


// $settings is a wrapper that allows easy data manipulation for JSONStore collections with single elements
// such as local application settings

localDBModule.factory("$settings", ['$localDB',  function($localDB) {
	
	var localImpl = {
			getSettings: function(collectionName) {
				return $localDB.find(collectionName,{},{});
			}
	}
	
	return {
		get: function(collectionName,attributeName) {
			var dfd = $.Deferred();
			localImpl.getSettings(collectionName).then(function(settings){
				var attribute = settings[0].json[attributeName];
				if (attribute == undefined){
					dfd.reject("The setting " + attributeName + " does not exist");
				}
				else{
					dfd.resolve(attribute);
				}
			});
			
			
			return dfd.promise();
		},
		
		set: function(collectionName,attributeName, attributeValue) {
			var dfd = $.Deferred();
			localImpl.getSettings(collectionName).then(function(settings){
				var attribute = settings[0].json[attributeName];
				if (attribute == undefined){
					dfd.reject("The setting " + attributeName + " does not exist");
				}
				else{
					settings[0].json[attributeName] = attributeValue;
					$localDB.replace(collectionName ,settings,{})
						.then(function(){
							dfd.resolve();
							});
				}
			});
			
			return dfd.promise();
		}
	}
	
}]);