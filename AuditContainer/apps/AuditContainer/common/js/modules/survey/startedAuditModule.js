/**
 * May, 12 2016
 * startedAuditModule.js
 * Stavarache Vlad
 */

/**
 * startedAudit Factory
 * @namespace Factories
 */
(function(){
	'use strict';
	
	audit
    .factory('startedAudit', startedAudit);
	
	startedAudit.$inject = ['$localDB', '$ionicLoading'];
	
	/**
	 * @namespace startedAudit
	 * @desc Service used to work with a started visit
	 * @memberOf Factories
	 */
	function startedAudit($localDB, $ionicLoading) {
		
		var eliminatedVisit = null;
        var visitIsStarted = false;
		
		var service = {
			initStartedWorkOrders: initStartedWorkOrders,
            isVisitStarted: isVisitStarted,
			checkForStartedVisit: checkForStartedVisit,
			saveAuditParts: saveAuditParts,
			addStartedWorkOrders: addStartedWorkOrders,
			getAuditPartsForWorkOrder: getAuditPartsForWorkOrder,
			getAuditParts: getAuditParts,
			getStartedWorkOrders: getStartedWorkOrders,
			getWorkOrdersWithAuditResults: getWorkOrdersWithAuditResults,
			removeStartedWorkOrder: removeStartedWorkOrder,
			updateStartedWorkOrder: updateStartedWorkOrder,
			startWorkOrder: startWorkOrder,
			getWorkOrderStatusAndComment: getWorkOrderStatusAndComment,
			finishWorkOrder: finishWorkOrder,
			getStartedVisit:getStartedVisit,
			incrementAuditQueue : incrementAuditQueue,
			eraseStartedVisitData : eraseStartedVisitData,
			getUnfilteredStartedWorkOrders : getUnfilteredStartedWorkOrders,
			getUnfilteredAuditParts : getUnfilteredAuditParts,
			moveStartedVisitsToFinished : moveStartedVisitsToFinished,
			checkStartedVisitOffline : checkStartedVisitOffline,
            updateIsVisitStarted : updateIsVisitStarted
		};
		
		return service;
		
		function initStartedWorkOrders(visitId) {
			var dfd = $.Deferred();
			
			WL.JSONStore.get('startedAuditParts').clear().then(function(){
				WL.JSONStore.get('startedAuditWorkOrders').clear().then(function(){
					$localDB.find('visit', {visitId : visitId, auditStatus : 'Started'}, {exact: true}).then(function(workOrders){
						var startedAuditWorkOrders = [];
						
						angular.forEach(workOrders, function(workOrder){
							startedAuditWorkOrders.push({
								visitId : workOrder.json.visitId,
								workOrderId : workOrder.json.workOrderId,
								inspectionStatus : 'Planned',
								shopCode : workOrder.json.shopCode,
								visitDesc : workOrder.json.description,
								containerNo : workOrder.json.containerNo,
                                mode:workOrder.json.mode,
                                repairCost:workOrder.json.repairCost,
                                repairDate:workOrder.json.repairDate,
//                                auditresults:workOrder.json.auditResults,
                                auditResults:workOrder.json.auditresults,
                                woStatus:workOrder.json.woStatus,
                                statusCode: workOrder.json.statusCode
							});
						});
						
						$localDB.add('startedAuditWorkOrders', startedAuditWorkOrders, {}).then(function(){
							dfd.resolve(startedAuditWorkOrders);
						});
					});
				});
			});
			
			return dfd.promise();
		}

        /**
         * @name isVisitStarted
         * @desc tells if a visit is currently started
         * @param N/A
         * @returns {Bool} - true if a visit is currently started
         * @memberOf Factories.startedAudit
         */
        function isVisitStarted() {
            return visitIsStarted;
        }

        /**
         * @name updateIsVisitStarted
         * @desc updates the visitIsStarted service variable 
         * @param {Bool} started
         * @returns N/A
         * @memberOf Factories.startedAudit
         */
        function updateIsVisitStarted(started) {
            visitIsStarted = started;
        }
		
		function getAuditPartsForWorkOrder(workOrderNo, visitId){
			var dfd = $.Deferred();
			var repairLines = [];
			var partLines = [];
			
			$localDB.find('startedAuditParts', {workOrderId: workOrderNo, visitId: visitId, woRepair: true}, {exact: true}).then(function(repairs){
				repairLines = repairs.map(function(jsonStoreEntry){
					return jsonStoreEntry.json;
				});
				$localDB.find('startedAuditParts', {workOrderId: workOrderNo, visitId: visitId, woRepair: false}, {exact: true}).then(function(parts){
					partLines = parts.map(function(jsonStoreEntry){
						return jsonStoreEntry.json;
					});
					
					// we have both repairs and parts information now
					
					dfd.resolve({repairLines: repairLines, partLines: partLines});
				});
			})
			
			return dfd.promise();
		}
		
		function eraseStartedVisitData() {
			var dfd = $.Deferred();
			
			WL.JSONStore.get('startedAuditWorkOrders').clear()
			.then(function(){
				return WL.JSONStore.get('startedAuditParts').clear();
			}).then(function(){
				dfd.resolve();
			}).fail(function (error){
				dfd.reject(error);
			});
			
			return dfd.promise();
		}
		
		function moveStartedVisitsToFinished() {
			var defer = $.Deferred();
			
			// Change started visits that are finished to Finished status
			// Delete other visits with Started Status in visit collection, that were not finished!
			
			getStartedWorkOrders().then(function(workOrders){
				var finishedWorkOrders = workOrders.finishedWorkOrders;
				var allWorkOrders = workOrders.allWorkOrders;
				
				var visitsToRemove = [];
				var visitsToSetToFinished = [];
				
				angular.forEach(finishedWorkOrders, function(workOrder){
					visitsToSetToFinished.push(changeStartedWorkOrderToFinished(workOrder.workorderid));
				});
				
				angular.forEach(allWorkOrders, function(workOrder){
					if (workOrder.inspectionstatus != "Finished") {
						visitsToRemove.push(removeStartedWorkOrderById(workOrder.workorderid));
					}
				})
				
				$.when.apply($, visitsToSetToFinished).then(function(){
					$.when.apply($, visitsToRemove).then(function(){
						eraseStartedVisitData().then(function(){
							defer.resolve();
						}).fail(function(error){
							console.log(error);
							defer.reject(error);
						});
					}).fail(function(error){
						console.log(error);
						defer.reject(error);
					});
				}).fail(function(error){
					console.log(error);
					defer.reject(error);
				});
				
				// Remove the ones that are started but not finished
			}).fail(function(error){
				console.log(error);
				defer.reject(error);
			});
				
			return defer.promise();
		}
		
		function removeStartedWorkOrderById(workOrderId) {
			var defer = $.Deferred();
			
			WL.JSONStore.get("visit").remove({workOrderId: workOrderId, auditStatus: "Started"}, {exact : true}).then(function(){
				defer.resolve();
			}).fail(function(error){
				console.log(error)
				defer.reject(error);
			});
			
			return defer.promise();
		}
		
		function changeStartedWorkOrderToFinished(workOrderId) {
			var defer = $.Deferred();
			
			$localDB.find('visit', {auditStatus : 'Started', workOrderId : workOrderId}, {exact : true}).then(function(workOrder){
				if (workOrder.length > 0) {
					workOrder[0].json.auditStatus = "Finished";
					$localDB.replace('visit', workOrder, {}).then(function(){
						defer.resolve();
					}).fail(function(error){
						console.log(error)
						defer.reject(error);
					});
	 			} else {
	 				defer.resolve();
	 			}
			}).fail(function(error){
				console.log(error)
				defer.reject(error);
			});
			
			return defer.promise();
		}
		
		function getAuditParts(){
			var dfd = $.Deferred();
			
			var options = {
				filter: ['visitId', 'accepted', 'workOrderId', 'woRepair',
				         'partIndex', 'repairIndex', 'repairDescription',
				         'preTPI', 'postTPI', 'prePartCode', 'postPartCode', 
				         'preRepairCode', 'postRepairCode', 'preQuantity', 'postQuantity',
				         'preDamageCode', 'postDamageCode', 'preRepairLocCode', 'postRepairLocCode',
				         'manHrs', 'partCost', 'materialCost', 'totalPerCode', 'partDescription', 'rejectedPart', 'repairQuality']
			};
			
			WL.JSONStore.get('startedAuditParts').find({}, options).then(function(data){
				dfd.resolve(data);
			}).fail(function(err){
				dfd.reject(err);
			})
			
			return dfd.promise();
		}
		
		function getUnfilteredAuditParts() {
			var dfd = $.Deferred();
			
			$localDB.find('startedAuditParts', {}, {}).then(function(data){
				dfd.resolve(data);
			}).fail(function(err){
				dfd.reject();
			})
			
			return dfd.promise();
		}
		
		function saveAuditParts(visitId, workOrderNo, preAuditParts, postAuditParts){
			var dfd = $.Deferred();
			var woLinesArray = [];
			
			angular.forEach(postAuditParts, function(repair, idxr){
				// check if added part - TODO
				
				// Set main properties and postaudit values
				
				var repairLine = {
					visitId : visitId,
					workOrderId : workOrderNo,
					woRepair : true,
					accepted : !repair.$rejected,
					rejectedPart : !!repair.$singleRejected,
					$postRepairDescription : repair.$postRepairDescription,
					repairIndex : idxr, // helps with displaying in UI
					postTPI : repair.TPI,
					postRepairCode : repair.repairCode,
					postQuantity : repair.quantity,
					postDamageCode : repair.damageCode,
					postRepairLocCode : repair.repairLocCode,
					repairDescription : repair.repairDescription,
					manHrs : repair.manHrs,
					totalPerCode : repair.totalPerCode,
					materialCost : repair.materialCost,
					repairQuality : repair.$ratingRQ,
					addedPart : repair.$addedPart
				};
				
				// Set preaudit values
				
				if (!repair.$addedPart) { // we only have preAudit info for repairs originally in the work order
					var preAuditLine = preAuditParts[idxr];
					repairLine.preTPI = preAuditLine.TPI;
					repairLine.preRepairCode = preAuditLine.repairCode;
					repairLine.preQuantity = preAuditLine.quantity;
					repairLine.preDamageCode = preAuditLine.damageCode;
					repairLine.preRepairLocCode = preAuditLine.repairLocCode
				}
					
				// Check if repair has child parts and save them as well
				
				if (repair.$childParts.length > 0) {
					repairLine.hasChildParts = true;
					
					// Values for repair line are set, we can push the repair into the array
					
					woLinesArray.push(repairLine);
					
					// Loop through and add child parts
					
					angular.forEach(repair.$childParts, function(part, idxp){
						
						var partLine = {
							visitId : visitId,
							workOrderId : workOrderNo,
							partIndex : idxp, // helps with displaying in UI
							repairIndex : repairLine.repairIndex,
							woRepair : false,
							$postPartDescription : part.$postPartDescription,
							accepted : !part.$rejected, 
							rejectedPart : !!part.$singleRejected,
							partDescription : part.partDescription,
							postPartCode : part.partCode,
							postRepairCode : part.repairCode,
							postQuantity : part.quantity,
							totalPerCode : part.totalPerCode,
							partCost : part.partCost,
							materialCost : part.materialCost,
							addedPart	: part.$addedPart
						};
						
						// Set preAudit values
						
						if (!part.$addedPart) { // // we only have preAudit info for parts originally in the work order
							var preAuditPart = preAuditLine.$childParts[idxp];
							
							partLine.prePartCode = preAuditPart.partCode;
							partLine.preRepairCode = preAuditPart.repairCode;
							partLine.preQuantity = preAuditPart.quantity;
						}
						
						// Values for child part are set, we can push the part into the array
						
						woLinesArray.push(partLine);
					});
				} else {
					woLinesArray.push(repairLine);
				}
				
			});
			
			$localDB.add("startedAuditParts", woLinesArray, {}).then(function(){
				dfd.resolve(woLinesArray);
			});
			
			return dfd.promise();
		}
		
		function checkForStartedVisit(){
			var dfd = $.Deferred();

			$localDB.find('visit', {auditStatus : 'Started'}, {exact : true}).then(function(result){
				var started = (result.length > 0) ? true : false;
                updateIsVisitStarted(started);
				dfd.resolve(started);
			}).fail(function(error){
                dfd.reject(error);
            });
			
			return dfd.promise();
		}
		
		/**
         * @name getStartedVisit
         * @desc Returns the visitId of the current started visit, fails if no visit is currently started
         * @param N/A
         * @returns {String}
         * @memberOf Factories.startedAudit
         */
		function getStartedVisit() {
			var dfd = $.Deferred();
			
			$localDB.find('visit', {auditStatus : 'Started'}, {exact : true}).then(function(result){
				if (result && result.length > 0) {
					dfd.resolve(result[0].json.visitId);
				} else {
					dfd.reject();
				}
			}).fail(function(err){
				dfd.reject(err);
			});
			
			return dfd.promise();
		}
		
		function getWorkOrderStatusAndComment(workOrderId, visitId) {
			var dfd = $.Deferred();
			
			$localDB.find('startedAuditWorkOrders', {workOrderId : workOrderId, visitId : visitId}, {}).then(function(workOrder){
				if (workOrder.length > 0) {
					var data = {
							workOrderStatus : workOrder[0].json.inspectionStatus,
							finalComment    : workOrder[0].json.finalComment,
							ratingRQ		: workOrder[0].json.repairQuality
					}
					
					dfd.resolve(data);
				} else {
					dfd.reject();
				}
			});
			
			return dfd.promise();
		}
		
		function getUnfilteredStartedWorkOrders(visitId) {
			var dfd = $.Deferred();
			
			$localDB.find('startedAuditWorkOrders', {visitId : visitId}, {}).then(function(data){
				dfd.resolve(data);
			}).fail(function(error){
				dfd.reject(error);
			});
			
			return dfd.promise();
		}
		
		function getStartedWorkOrders(visitId) {
			var dfd = $.Deferred();
			
			var options = {
				filter: ['visitId', 'inspectionStatus', 'workOrderId', 'containerNo', 'shopCode', 'visitDesc', 'auditResult','finalComment', 'repairQuality', 'statusCode'],
				exact: true
			};
				
			WL.JSONStore.get('startedAuditWorkOrders').find({visitId: visitId}, options).then(function(allWorkOrders){
				WL.JSONStore.get('startedAuditWorkOrders').find({visitId: visitId, inspectionStatus: 'Finished'}, options).then(function(finishedWorkOrders){
					dfd.resolve({allWorkOrders: allWorkOrders, finishedWorkOrders: finishedWorkOrders});
				});
			})
			
			return dfd.promise();
		}
		
		function getWorkOrdersWithAuditResults(visitId) {
			var dfd = $.Deferred();
			
			var options = {
				filter: ['visitId', 'inspectionStatus', 'workOrderId', 'containerNo', 'shopCode', 'visitDesc', 'auditResult', 'finalComment', 'repairQuality', 'statusCode', 'woStatus'],
				exact: true
			};
				
			WL.JSONStore.get('startedAuditWorkOrders').find({visitId: visitId}, options).then(function(allWorkOrders){
				WL.JSONStore.get('startedAuditWorkOrders').find({visitId: visitId, inspectionStatus: 'Finished'}, options).then(function(finishedWorkOrders){
					var finishedWorkOrdersInfo = getFinishedWorkOrdersWithAuditResults(finishedWorkOrders);
					WL.JSONStore.get('startedAuditWorkOrders').find({visitId: visitId, inspectionStatus: 'Started'}, options).then(function(startedWorkOrders){
						WL.JSONStore.get('startedAuditWorkOrders').find({visitId: visitId, inspectionStatus: 'Planned'}, options).then(function(plannedWorkOrders){
							dfd.resolve({allWorkOrders: allWorkOrders, startedWorkOrders: startedWorkOrders, finishedWorkOrders: finishedWorkOrders,
								passedWorkOrders: finishedWorkOrdersInfo.passedWorkOrders, failedWorkOrders: finishedWorkOrdersInfo.failedWorkOrders,
								plannedWorkOrders: plannedWorkOrders});
						});
					});
				});
			})
			
			return dfd.promise();
		}
		
		function getFinishedWorkOrdersWithAuditResults(finishedWorkOrders) {
			var passedWorkOrders = [];
			var failedWorkOrders = [];
			
			angular.forEach(finishedWorkOrders, function(workOrder){
				if (workOrder.auditresult == 1) {
					passedWorkOrders.push(workOrder);
				} else {
					failedWorkOrders.push(workOrder);
				}
			});
			
			return {passedWorkOrders: passedWorkOrders, failedWorkOrders: failedWorkOrders};
		}
		
		function addStartedWorkOrders(workOrderArray) {
			var dfd = $.Deferred();
			
			$localDB.add('startedAuditWorkOrders', workOrderArray, {}).then(function(){
				dfd.resolve();
			});
			
			return dfd.promise();
		}
		
		function removeStartedWorkOrder(workOrderId, visitId) {
			var dfd = $.Deferred();
			
			$localDB.find('startedAuditWorkOrders', {workOrderId : workOrderId, visitId : visitId}, {}).then(function(workOrder){
				$localDB.erase('startedAuditWorkOrders', workOrder, {}).then(function(){
					dfd.resolve();
				});
			});
			
			return dfd.promise();
		}
		
		/*
		 * 2457 -- save the ratingRq avarage
		 */
		function updateStartedWorkOrder(workOrderId, visitId, status, auditResult, finalComment, avarageRating) {
			var dfd = $.Deferred();
			
			if(finalComment==null)
				finalComment = finalComment;
			
			$localDB.find('startedAuditWorkOrders', {workOrderId : workOrderId, visitId : visitId}, {}).then(function(workOrder){
				var workOrder = angular.copy(workOrder);
				workOrder[0].json.finalComment = finalComment;
				workOrder[0].json.inspectionStatus = status;
				if(avarageRating){
					workOrder[0].json.repairQuality = avarageRating;
				}
				if (typeof auditResult !== 'undefined') {
					workOrder[0].json.auditResult = auditResult;
				}
				$localDB.replace('startedAuditWorkOrders', workOrder, {}).then(function(){
//					$localDB.find('startedAuditWorkOrders', {workOrderId : workOrderId, visitId : visitId}, {}).then(function(data){
//						console.log("_________________")
//						console.log(data);
//					});
					dfd.resolve();
				});
			});
			
			return dfd.promise();
		}
		
		function startWorkOrder(workOrderId, visitId) {
			return updateStartedWorkOrder(workOrderId, visitId, 'Started',"");
		}
		
		function finishWorkOrder(workOrderId, visitId, auditResult,finalComment, avarageRating) {
			return updateStartedWorkOrder(workOrderId, visitId, 'Finished', auditResult,finalComment, avarageRating);
		}
		
		/*
		 *  Function for caching data about visits in JsonStore
		 */
		function incrementAuditQueue(visitID){
			var dfd = $.Deferred();
			$localDB.find('auditQueue',{visitId:visitID},{}).then(function(isVisit){
				console.log(isVisit)
				// If visit already exists in queue change it's timestamp
				if(isVisit.length != 0){
					isVisit[0].json.timestamp = new Date();
					$localDB.replace('auditQueue',isVisit[0],{}).then(function(){
						$localDB.find('auditQueue',{},{}).then(function(data){
							dfd.resolve();
						})
					})
				}
				// If visit does not exist in queue, insert it
				else{
					$localDB.find('auditQueue',{},{}).then(function(data){
						// if there are less than 20 items cached in queue
						if(data.length < 20){
							var visitObj = {
								visitId : visitID,
								timestamp : new Date()
							}
							$localDB.add("auditQueue",visitObj,{}).then(function(){
								$localDB.find('auditQueue',{},{}).then(function(data){
									dfd.resolve();
								})
							})	
						}
						// if there are more than 20 items in queue, we eliminate the oldest one and add the newest one
						else{
							var min = data[0].json.timestamp;
							var minIndex = 0;
							
							for(index in data)
								if(new Date(min) > new Date(data[index].json.timestamp)){ 
									min = data[index].json.timestamp;
									minIndex = index;
								}
							
							eliminatedVisit = data[minIndex].json.visitId;
							
							// delete photos directory and //Remove PDF from disk
							window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, onFileSystemSuccess, fail);
							

							
							data[minIndex].json.timestamp = new Date();
							data[minIndex].json.visitId = visitID;
							
							var visitCleanupDfd = $.Deferred();
							 
							removeFinishedWorkOrders(eliminatedVisit).then(function(){
								removeFinishedWorkOrderParts(eliminatedVisit).then(function(){
									visitCleanupDfd.resolve();
								});
							});
							
							visitCleanupDfd.then(function(){
								$localDB.replace('auditQueue',data[minIndex],{}).then(function(){
									$localDB.find('auditQueue',{},{}).then(function(data){
										dfd.resolve();
									})
								})
							})
						}
					});
				}
			})
			return dfd.promise();
		}
		
		/*
		 * REPLICATED from auditService to solve a circular dependency
		 * TODO Improve this solution
		 */
		function removeFinishedWorkOrders(visitId) {
			var defer = $.Deferred();
			
			$localDB.find('finishedVisit', {visitId: visitId}, {exact: true}).then(function(auditToErase){
				if (auditToErase.length > 0) {
					$localDB.erase('finishedVisit', auditToErase, {}).then(function(){
						defer.resolve();
					});
				} else {
					defer.resolve();
				}
			});
			
			return defer.promise();
		}
		
		function removeFinishedWorkOrderParts(visitId) {
			var defer = $.Deferred();
			
			$localDB.find('finishedAuditParts', {visitId: visitId}, {exact: true}).then(function(partsToErase){
				if (partsToErase.length > 0) {
					$localDB.erase('finishedAuditParts', partsToErase, {}).then(function(){
						defer.resolve();
					});
				} else {
					defer.resolve();
				}
			});
			
			return defer.promise();
		}
		/* **/
		
		function fail(evt) {
	        console.log("FILE SYSTEM FAILURE" + evt.target.error.code);
	    }
		 
        function onFileSystemSuccess(fileSystem) {
            fileSystem.root.getDirectory(
                 ""+eliminatedVisit,
                {create : true, exclusive : false},
                function(entry) {
                entry.removeRecursively(function() {
                    console.log("Remove Recursively Succeeded");
                    $localDB.find("galleryPhoto", {visitId : eliminatedVisit }, {exact : true}).then(function(data){
                    	$localDB.erase("galleryPhoto",data,{}).then(function(){
                    		console.log("photos successfully deleted from local storage");
							removePdfFile(fileSystem);
                    	})
                    })
                }, fail);
            }, fail);
		};

		function removePdfFile(fileSystem){
			fileSystem.root.getDirectory("PdfReports/" + eliminatedVisit,{create : true, exclusive : false},function(entry){
				entry.removeRecursively(function(){
					console.log('PDF folder and file were deleted successfully!');
				},fail);
			},fail);
		};
		
		function replacePlannedWithStarted(visitId){
			var defer = $.Deferred();
			
			$localDB.find('visit', {visitId: visitId, auditStatus : 'Planned'}, {exact: true}).then(function(partsToReplace){
				if (partsToReplace.length > 0) {
					
					var changedData = partsToReplace.map(function(item){
						item.json.auditStatus = 'Started';
						return item;
					});
					
					$localDB.replace('visit', changedData, {}).then(function(){
						defer.resolve();
					});
				} else {
					defer.resolve();
				}
			}).fail(function(error){
				defer.reject();
			});
			
			return defer.promise();
		};
		
		function checkStartedVisitOffline(visitId){
			var defer = $.Deferred();
			 
			 $localDB.find('visit', {visitId: visitId, auditStatus : 'Started'}, {exact : true}).then(function(result){
				 if(result.length > 0){
					 replacePlannedWithStarted(visitId).then(function(){
						 defer.resolve();
					 }).fail(function(){
						 defer.reject();
					 })
				 }else{
					 defer.resolve();
				 }
			 }).fail(function(error){
				 defer.reject();
			 });
			 
			 return defer.promise();
		}
		
	}
})();