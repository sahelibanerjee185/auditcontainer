/**
 * $depot Factory
 * @namespace Factories
 */
(function(){
	'use strict';
	
	audit
    .factory('$depot', $depot);
	
	$depot.$inject = ['$localDB', '$localStorage', 'startedAudit', 'auditService',
	'archiveModule', '$surveyModule', 'pdfModule', 'rejectCorrectEstimate'];
	
	/**
     * @namespace $depot
     * @desc Service used to populate inspections scren and started tab
     * and work with work orders from a visit
     * @memberOf Factories
     */
	function $depot($localDB, $localStorage, startedAudit, auditService,
	archiveModule, $surveyModule,pdfModule, rejectCorrectEstimate) {
		
		var object = {
	        workOrdersForAudit: [],
	        visitsForRepairShop: [],
	        repairShopDetails: {},
	        historyForContainer: [],
	        inspectionStatus: null,
	        action: null,
	        visitDate: null,
	        visitId: null
	    };
		    
	    var options = {
	        filter: ['containerNo', 'repairCost', 'repairDate', 'shopCode', 'visitId', 'woStatus', 'workOrderId', 'mode', 'auditResult', 'statusCode']
	    };
		
		var service = {
			init : init,
			setVisitDate : setVisitDate,
			setVisitId : setVisitId,
			getAuditResult : getAuditResult,
			populateDepotDetailsForFinishedAudit : populateDepotDetailsForFinishedAudit,
			getVisitInfo : getVisitInfo,
			setActionButton : setActionButton,
			getInspectedContainers : getInspectedContainers,
			getobject : getobject,
			getVisitsForRepairShop : getVisitsForRepairShop,
			startInspection : startInspection,
			getInspectionStatus : getInspectionStatus,
			setAuditResults : setAuditResults
		};
		
		return service;

        function init() {
            object.workOrdersForAudit = [];
            object.visitsForRepairShop = [],
            object.repairShopDetails = {},
            object.historyForContainer = [],
            object.inspectionStatus = null,
            action = null
        }

        function setVisitDate(date) {
            object.visitDate = date;
        }

        function setVisitId(visitId) {
            object.visitId = visitId;
        }

        function getAuditResult(workOrders) {
            for (var i = 0; i < workOrders.length; i++) {
                var workOrder = workOrders[i];
                if (workOrder.auditresult == 'Rejected') {
                    return 'Rejected';
                }
            }

            return 'Accepted';
        }

        /**
	     * @name populateDepotDetailsForFinishedAudit
	     * @desc Populates the object variable in the service with appropriate
         * info for the inspections screen
	     * @param {Array} finishedWorkOrders
         * @param {String} visitId
         * @param {Function} callback
	     * @returns {N/A}
	     * @memberOf Factories.$depot
	     */
        function populateDepotDetailsForFinishedAudit(finishedWorkOrders, visitId, callback) {
            object.workOrdersForAudit = finishedWorkOrders;
            var workOrderFromVisit = object.workOrdersForAudit[0];

            // set visit status!
            object.inspectionStatus = 'Finished';
            object.visitId = visitId;

            object.repairShopDetails.description = workOrderFromVisit.shopdescription;
            object.repairShopDetails.shopCode = workOrderFromVisit.shopcode;
            object.visitDate = workOrderFromVisit.auditdate;

            setActionButton(getAuditResult(finishedWorkOrders));
            
            finishedWorkOrders.forEach(function(workOrder, index) {
    	        var workWorderNo = workOrder.workorderid;
    	        var auditresult = workOrder.auditresult;

    	        rejectCorrectEstimate.validateWO(workWorderNo, visitId, "Finished", auditresult).then(function(result) {
	                workOrder.isECRW = result;
	                if (index >= (finishedWorkOrders.length - 1)) {
	                	if (typeof callback === 'function') callback(finishedWorkOrders);
	                }
    	        }).fail(function (err) {
    	            workOrder.isECRW = false;
    	        });
    	    });        
        }

        /**
	     * @name getVisitInfo
	     * @desc Used to populate associated services
         * with requested visit data 
         * 1. For finished visits,
         * there is a caching queue logic,
         * the model for this is the 'auditQueue' collection,
         * there is a limit of 20 visits to be cached on the device
         * 2. For missed visits,
         * we just get the visit info from the server
         * or locally if no internet connection
         * 3. for pending visits,
         * we get the info from json store
         * 4. for started visits
         * we get the info from either server or jsonstore
	     * @param {String} visitId
         * @param {String} auditStatus
         * @param {Bool} postFinishAudit
	     * @returns {Promise}
	     * @memberOf Factories.$depot
	     */
        function getVisitInfo(visitId, auditStatus, postFinishAudit) {
            object.workOrdersForAudit = [];
            object.repairShopDepotDetails = {};

            var repairShopNo;
            var dfd = $.Deferred();

            var options = {
                filter: ['auditDate', 'auditStatus', 'containerNo', 'containerType', 'description',
				         'repairCost', 'repairDate', 'shopCode', 'visitId', 'woStatus', 'woType', 'workOrderId',
				         'shopDescription', 'statusCode', 'mode', 'auditResult']
            };

            /*
             *  QUEUE logic
             */

            // auditStatus === 'Missed' case!

            if (auditStatus === 'Finished') { 
                // search visit in audit queue
                $localDB.find("auditQueue", {
                    visitId: visitId
                }, {}).then(function (dataQueue) {
                    // if the visit is not in queue, insert it, maintaining the limit of maximum 20 audits in queue
                	//check also if it has internet connection
                    if (dataQueue.length == 0) {
                    	auditService.testSignal().then(function(){
                    		var workOrdersAndPartsDfd = $.Deferred();

                            auditService.storeFinishedVisitParts(visitId).then(function (finishedParts) {
                                
                                auditService.storeFinishedVisitWorkOrders(visitId).then(function (finishedWorkOrders) {
                                    populateDepotDetailsForFinishedAudit(finishedWorkOrders, visitId, function () {
                                        workOrdersAndPartsDfd.resolve();
                                    });
                                }).fail(function () {
                                    WL.Logger.error("[DepotDetails][getVisitInfo][storeFinishedVisitWorkOrders] username: " + userContext.getUsername() +" Failed to storeFinishedVisitWorkOrders");
                                    workOrdersAndPartsDfd.reject();
                                });
                            }).fail(function (error) {
                                WL.Logger.error("[DepotDetails][getVisitInfo][storeFinishedVisitParts] username: " + userContext.getUsername() +" " + JSON.stringify(error));
                                workOrdersAndPartsDfd.reject();
                            });

                            workOrdersAndPartsDfd.then(function () {
                                startedAudit.incrementAuditQueue(visitId).then(function () {
                                    if (postFinishAudit) {
                                        dfd.resolve();
                                    } else {
                                        pdfModule.downloadPDFFromServer(visitId).then(function(){
                                            auditService.getArchiveName(visitId).then(function (archiveName) {
                                                auditService.getPhotos(visitId).then(function (photos) {
                                                    $localDB.add("galleryPhoto", photos, {}).then(function () {
                                                        archiveModule.saveArchiveFromServer(archiveName.resultSet[0].ARCHIVE_ID).then(function () {
                                                            dfd.resolve();
                                                        }).fail(function(err){
                                                        	dfd.reject(err); //Couldn't retrieve Photo archive from server
                                                        })
                                                    }).fail(function (error) {
                                                        dfd.reject();
                                                    });
                                                }).fail(function (error) {
                                                    dfd.reject();
                                                });
                                            }).fail(function (error) {
                                                dfd.reject();
                                            });
                                        }).fail(function(err){ //pdf failed to be downloaded and saved
                                            dfd.reject(err);
                                        });
                                    }
                                });
                            }).fail(function (error) {
                                dfd.reject(error);
                            });
                    	}).fail(function(){
                    		dfd.reject();
                    		return dfd.promise();
                    	});
                    }
                    // if the visit is already added to the queue, read the data from locale storage and change 
                    // timestamp for that specific audit in queue
                    else {
                        //change timestampt for audit
                        dataQueue[0].json.timestamp = new Date();
                        // replace audit in queue with the new timestamp
                        $localDB.replace('auditQueue', dataQueue[0], {}).then(function () {
                            auditService.getFinishedVisitWorkOrdersFromJsonStore(visitId).then(function (finishedWorkOrders) {
                            	// Check if all statuses are processed
                            	// If not, make the request again.
                            	var allWorkOrdersProcessed = true;
                            	for (var i = 0; i < finishedWorkOrders.length; i++) {
                            		if (finishedWorkOrders.mercstatus != "FAILED" && finishedWorkOrders.mercstatus != "SUCCESS") {
                            			allWorkOrdersProcessed = false;
                            			break;
                            		}
                            	}
                            	
                            	if (allWorkOrdersProcessed) {
                            		populateDepotDetailsForFinishedAudit(finishedWorkOrders, visitId, function () {
                                        dfd.resolve();
                                    });
                            	} else {
                            		// try to make the request again - check for internet first - TODO
                            		auditService.testSignal().then(function(){
                            			auditService.storeFinishedVisitWorkOrders(visitId).then(function (finishedWorkOrdersFromServer) {
                                			populateDepotDetailsForFinishedAudit(finishedWorkOrdersFromServer, visitId, function () {
                                                dfd.resolve();
                                            });
                                		}).fail(function(){
                                			dfd.resolve();
                                		});
                            		}).fail(function(){
                            			populateDepotDetailsForFinishedAudit(finishedWorkOrders, visitId, function () {
                                            dfd.resolve();
                                        });
                            		});
                            	}
                            });
                        });
                    }
                })

            } else if (auditStatus === 'Missed') {  //SPOT 7 -  for missed is an adapter call on getMissedVisitWorkOrders
            	auditService.testSignal().then(function(){
            		auditService.getMissedVisitWorkOrders(visitId).then(function (missedWorkOrders) {
                        if (missedWorkOrders.length > 0) {
                            object.workOrdersForAudit = missedWorkOrders;
                            var workOrderFromVisit = object.workOrdersForAudit[0];

                            object.inspectionStatus = workOrderFromVisit.auditstatus;
                            object.visitId = visitId;

                            console.log('object.visitId', object.visitId);

                            object.repairShopDetails.description = workOrderFromVisit.shopdescription;
                            object.repairShopDetails.shopCode = workOrderFromVisit.shopcode;
                            object.visitDate = workOrderFromVisit.auditdate;

                            setActionButton(object.inspectionStatus);

                            dfd.resolve(missedWorkOrders);
                        } else {
                            dfd.reject();
                        }
                    }).fail(function (e) {
                        dfd.reject(e);
                    });
            	}).fail(function(){
            		auditService.getLocalMissedVisitWorkOrders(visitId).then(function (missedWorkOrders) {
                        if (missedWorkOrders.length > 0) {
                            object.workOrdersForAudit = missedWorkOrders;
                            var workOrderFromVisit = object.workOrdersForAudit[0];

                            object.inspectionStatus = workOrderFromVisit.auditstatus;
                            object.visitId = visitId;

                            console.log('object.visitId', object.visitId);

                            object.repairShopDetails.description = workOrderFromVisit.shopdescription;
                            object.repairShopDetails.shopCode = workOrderFromVisit.shopcode;
                            object.visitDate = workOrderFromVisit.auditdate;

                            setActionButton(object.inspectionStatus);

                            dfd.resolve(missedWorkOrders);
                        } else {
                            dfd.reject();
                        }
                    }).fail(function (e) {
                        dfd.reject(e);
                    });
            	});
            } else if(auditStatus === 'Pending'){
            	$localDB.find('visit', {visitId: visitId}, options).then(function(result){
            		if (result.length == 0) {
                        dfd.reject({
                            notFound: true
                        });
                        // probably this visit is missed or doesn't exist
                    }else{
                    	 auditService.getPendingVisitAuditResults(result).then(function(data){ 
                    		// data can be an empty array!
                             
                            if(data.length == 0){
                                dfd.reject({
                                    notFound: true
                                });
                            }else{
                                object.workOrdersForAudit = data;
                                var workOrderInfo = result[0];

                                object.inspectionStatus = "Pending";
                                object.visitId = visitId;

                                console.log('object.visitId', object.visitId);

                                object.repairShopDetails.description = workOrderInfo.shopdescription;
                                object.repairShopDetails.shopCode = workOrderInfo.shopcode;
                                object.visitDate = workOrderInfo.auditdate;

                                setActionButton("Pending");
                                dfd.resolve(data);
                            }
                    	 });
                    }
            	});
        	}else { // Planned
        		startedAudit.checkStartedVisitOffline(visitId).then(function(){
                    WL.JSONStore.get('visit').find({
                        visitId: visitId
                    }, options).then(function (data) {
                        if (data.length == 0) {
                            dfd.reject({
                                notFound: true
                            });
                            // probably this visit is missed or doesn't exist
                        } else {
                        	//check for deleted status
                        	var validData = []
                        	data.forEach(function(visit){
                        		if(visit.auditstatus !== 'Deleted')
                        			validData.push(visit);
    						});
                        	
                            object.workOrdersForAudit = validData;
                            var workOrderFromVisit = object.workOrdersForAudit[0];

                            object.inspectionStatus = workOrderFromVisit.auditstatus;
                            object.visitId = visitId;

                            console.log("last else Planned/Started");
                            console.log('object.visitId', object.visitId);

                            object.repairShopDetails.description = workOrderFromVisit.shopdescription;
                            object.repairShopDetails.shopCode = workOrderFromVisit.shopcode;
                            object.visitDate = workOrderFromVisit.auditdate;

                            setActionButton(object.inspectionStatus);

                            if (object.inspectionStatus === 'Started') {
                                startedAudit.getStartedWorkOrders(visitId).then(function (workOrdersInfo) {
                                    if (workOrdersInfo.allWorkOrders.length != validData.length) {
                                        startedAudit.initStartedWorkOrders(visitId).then(function () {
                                            dfd.resolve(validData);
                                        })
                                    } else {
                                    	setAuditResults(true).then(function(){
                                    	    var visitId = object.visitId;
                                    	    var visitStatus = object.inspectionStatus;

                                    	    object.workOrdersForAudit.forEach(function(workOrder, index) {
                                    	        var workWorderNo = workOrder.workorderid;
                                    	        var auditresult = workOrder.auditresult;

                                    	        rejectCorrectEstimate.validateWO(workWorderNo, visitId, visitStatus, auditresult).then(function(result) {
                                	                workOrder.isECRW = result;
                                	                if (index >= (object.workOrdersForAudit.length - 1)) {
                                	                	console.log("object", object);
                                	                	dfd.resolve(validData);
                                	                }
                                    	        }).fail(function (err) {
                                    	            workOrder.isECRW = false;
                                    	        });
                                    	    });
                                    		
                                    	}).fail(function(error){
                                    		dfd.reject(error);
                                    	});
                                    }
                                })
                            } else {
                            	// Planned
                                dfd.resolve(validData);
                            }
                        }
                    });
        		}).fail(function(error){
        			dfd.reject(error);
        		})
            }

            return dfd.promise();
        }
        
        function setActionButton(inspectionStatus) {
            switch (inspectionStatus) {
            case Messages.planned:
                object.action = Messages.startVisit;
                break;
            case Messages.started:
                object.action = Messages.finishVisit;
                break;
            case Messages.missed:
                object.action = Messages.missedVisit;
                break;
            case Messages.finished:
                object.action = Messages.finishedVisit;
                break;
            default:
                object.action = inspectionStatus;
            }
        }

        function getInspectedContainers(callback) {
            var inspectedContainers = [];

            angular.forEach(object.visitsForRepairShop, function (visit) {
                if (visit.json.inspectionStatus === 'Finished') {
                    inspectedContainers.push(visit.json);
                }
            });

            if (typeof callback == 'function') callback(inspectedContainers);
        }

        function getobject() {
            return object;
        }

        function getVisitsForRepairShop() {
            return object.visitsForRepairShop;
        }

        function startInspection(callback) {
            var dfd = $.Deferred();
            object.inspectionStatus = 'Started';
            object.action = Messages.finishVisit;

            dfd.resolve();

            if (typeof callback === 'function') callback();

            return dfd.promise();
        }

        function getInspectionStatus() {
            return object.inspectionStatus;
        }
        
        /**
	     * @name setAuditResults
	     * @desc Sets the appropriate audit results in the object.workOrdersForAudit
         * from startedAuditWorkOrders collecion
         * in the service
	     * @param {Bool} isStarted
	     * @returns {Promise}
	     * @memberOf Factories.$depot
	     */
        function setAuditResults(isStarted) {
            var defer = $.Deferred();
            
            if (isStarted) {
                $localDB.find('startedAuditWorkOrders', { visitId: object.visitId }, options).then(function (workOrders) {
                	object.workOrdersForAudit = [];
                    workOrders.forEach(function (item) {
                        if (item.auditresult == "1" || item.auditresult === true) {
                            item.auditresult = "Accepted";
                        } else if (item.auditresult == "0" || item.auditresult === false) {
                            item.auditresult = "Rejected";
                        } else {
                            item.auditresult = "Pending";
                        }

                        object.workOrdersForAudit.push(item);
                    });
                    defer.resolve(object.workOrdersForAudit);
                }).fail(function (err) {
                    console.log("error:  " + JSON.stringify(err));
                    defer.reject(err);
                })
            } else {
                defer.resolve();
            }

            return defer.promise();
        }
    }
	
})();  
    


