/**
 * Jun 14, 2017
 * Florian Birloi
 *
 * rejectCorrectEstimate Factory
 * @namespace Factories
 */

audit.factory('rejectCorrectEstimate' , function(auditService, startedAudit){
	
  /**
   * @namespace rejectCorrectEstimate
   * @desc Factory which handles the Rjected work orders with all the parts accepted in the Work Order screen/Depodetails screen for a Started or Past Visit
   * @memberOf Factories
   */
	
	var service = {
		validateWO : validateWO
	}
	
	return service;
	
	
	/**
     * @name validateWO
     * @desc Validates if the work order is rejected with all the repairs accepted
     * @param {Integer} workWorderNo Work Order to validate
     * @param {Integer} visitId the visit associated with the Work Order
     * @param {String} visitStatus the status of the visit
     * @param {String} auditresult result of the inspection
     * @returns {Object}
     * @memberOf Factories.rejectCorrectEstimate
     */
	function validateWO(workWorderNo, visitId, visitStatus, auditresult){
		var defer = $.Deferred();
		
		if(auditresult != 'Rejected'){
			defer.resolve(false);
		}else{
			if(visitStatus == 'Started'){
				console.log(workWorderNo + " " + visitId);
				validateStartedWO(workWorderNo, visitId).then(function(result){
					defer.resolve(result);
				});
			}else if (visitStatus == 'Finished'){
				validateFinishedWO(workWorderNo, visitId).then(function(result){
					defer.resolve(result);
				});
			}else{
				defer.resolve(false);
			}
		}
		
		return defer.promise();
	}
	
	/**
     * @name validateStartedWO
     * @desc Validates if a work order of a Started Visit is rejected with all the repairs accepted
     * @param {Integer} workWorderNo Work Order to validate
     * @param {Integer} visitId the visit associated with the Work Order
     * @returns {Object}
     * @memberOf Factories.rejectCorrectEstimate
     */
	function validateStartedWO(workWorderNo, visitId){
		var defer = $.Deferred();
		
		startedAudit.getAuditPartsForWorkOrder(workWorderNo, visitId).then(function(result){
			console.log('validateStartedWO')
			console.log(result);
			defer.resolve(validateWorkWorderParts(result.repairLines));
		}).fail(function(error){
			defer.resolve(false);
		});
		
		return defer.promise();
	}
	
	/**
     * @name validateFinishedWO
     * @desc Validates if a work order of a Finished Visit is rejected with all the repairs accepted
     * @param {Integer} workWorderNo Work Order to validate
     * @param {Integer} visitId the visit associated with the Work Order
     * @returns {Object}
     * @memberOf Factories.rejectCorrectEstimate
     */
	function validateFinishedWO(workWorderNo, visitId){
		var defer = $.Deferred();
		
		auditService.getFinishedWorkOrderParts(workWorderNo, visitId).then(function(result){
			defer.resolve(validateWorkWorderParts(result))
		}).fail(function(error){
			defer.resolve(false);
		});
		
		return defer.promise();
	}
	
	/**
     * @name validateWorkWorderParts
     * @desc Validates every work order part
     * @param {Array} woParts array containing the parts objects of the work order
     * @returns {Boolean}
     * @memberOf Factories.Logger
     */
	function validateWorkWorderParts(woParts){
		return woParts.every(isAccepted);
	}
	
	/**
     * @name isAccepted
     * @desc Checks if a part is accepted
     * @param {Object} element part object
     * @returns {Boolean}
     * @memberOf Factories.Logger
     */
    function isAccepted(element){
    	return element.accepted === true || element.accepted === 1 ? true : false;
    };
	
});
		