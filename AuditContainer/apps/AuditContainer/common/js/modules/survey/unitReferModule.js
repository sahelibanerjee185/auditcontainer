/**
 * Nov 16, 2015
 * uniReferModule.js
 * BuzilaEugen
 */
audit.factory('$unitRefer', function ($localDB, $depot, $localStorage, startedAudit){
	
	obj ={
		containerNo : null,
		workOrderNo : null,
		woStatus : null,
		ratingRQ : null,
		statusCode : null,
		visitId  : null,
		jsonStoreComponents : [],
		parts : [],
		preAuditParts : [],
		referUnitStatus  : {
			started 	: false,
			finished 	: false,
			pending 	: false
		},
		localStorage : {},
		action : null,
		withoutComponents : true,
		finalComment : "",
		storageHelpers : [], // key : work order no / value : timestamp
		auditResult: null // Accepted or Rejected
	}
	
	return {
		
		setAuditResult: function(status){
			if(status!= null){
				obj.auditResult = status;
			}
		},
		
		setFinalComment : function(finalCom){
			if(finalCom == null) finalCom = "";
			obj.finalComment = finalCom;
		},
		
		deleteStorageHelpers : function() {
			var localStorageKeysArray = Object.keys($localStorage);
			
			for (var i = 0; i < localStorageKeysArray.length; i++) {
				var localKey = localStorageKeysArray[i];
				
				if (localKey[0] == "h") delete $localStorage[localKey];
			}
		},
		
		getFinalComment : function(){
			return obj.finalComment;
		},
		/*
		 * Initialize to null all the data from factory
		 */
		init : function(){
			obj.visitId = null;
			obj.containerNo = null;
			obj.finalComment = "";
			obj.components = [];
			obj.rawJsonStoreComponents = [];
		},
		setParts : function(workOrderLines) {
			// need to recheck all logic
			var self = this;
			
			angular.forEach(workOrderLines, function(repair, idxRepair){
				var repairWO = workOrderLines[idxRepair];
				
				repairWO.$preRepairCode = repair.repairCode;
				repairWO.$preRepairCode = repair.repairCode;
				repairWO.$idx = idxRepair;
				
				// loop through child parts as well
				
				angular.forEach(repairWO.$childParts, function(part, idxPart){
					var partWO = repairWO.$childParts[idxPart];
					
					partWO.$prePartCode = part.partCode;
					partWO.$idx = idxPart;
				});
			});
			
			// maybe we'll add indexes in adapter instead of client side
			
			obj.preAuditParts = workOrderLines;
			obj.parts = angular.copy(obj.preAuditParts);
			
		},
		
		setFinishedParts : function(workOrderLines) {
			// loop and set obj.parts and obj.preAuditParts
			// add $addedPart logic later!
			
			obj.parts = [];
			obj.preAuditParts = [];
			var self = this;
			
			angular.forEach(workOrderLines, function(lineInfo){
				var preLine = {};
				var postLine = {};
				
				// Loop through repair lines first
				
				if (lineInfo.woRepair == 1) {
					// Set pre audit values
					
					if (lineInfo.preRepairCode) {
						preLine.manHrs = lineInfo.manHrs;
						preLine.materialCost = lineInfo.materialCost;
						preLine.$idx = lineInfo.repairIndex;
						preLine.repairCode = lineInfo.preRepairCode;
						preLine.damageCode = lineInfo.preDamageCode;
						preLine.repairLocCode = lineInfo.preRepairLocCode;
						preLine.TPI = lineInfo.preTPI;
						preLine.quantity = lineInfo.preParts;
						preLine.repairDescription = lineInfo.preRepairDescription;
						preLine.totalPerCode = lineInfo.totalPerCode;
						preLine.materialCost = preLine.materialCost;
						preLine.$childParts = [];
						
						obj.preAuditParts[lineInfo.repairIndex] = preLine;
						
					} else {
						postLine.$addedPart = true;
					}
					
					// Set post audit values
					
					postLine.$rejected = (lineInfo.accepted == 0) ? true : false;
					postLine.$accepted = !postLine.$rejected;
					postLine.$postRepairDescription = lineInfo.postRepairDescription;
					postLine.quantity = lineInfo.postParts;
					postLine.repairCode = lineInfo.postRepairCode;
					postLine.damageCode = lineInfo.postDamageCode;
					postLine.repairLocCode = lineInfo.postRepairLocCode;
					postLine.totalPerCode = lineInfo.totalPerCode;
					postLine.materialCost = lineInfo.materialCost;
					postLine.manHrs = lineInfo.manHrs;
					postLine.TPI = lineInfo.postTPI;
					postLine.repairDescription = lineInfo.preRepairDescription;
					postLine.$childParts = [];
					postLine.$idx = lineInfo.repairIndex;
					postLine.$ratingRQ = lineInfo.repairQuality;
					
					if (postLine.$rejected) {
						postLine.$aggregateRejected = true;
					} else {
						postLine.$aggregateRejected = false;
					}
					
					// Values are set, we can add the lines to the arrays
					
					obj.parts[lineInfo.repairIndex] = postLine;
				}
			});
			
			angular.forEach(workOrderLines, function(lineInfo){
				var preLine = {};
				var postLine = {};
				
				// Loop through part lines 
				
				if (lineInfo.woRepair == 0) {
					var postPart = {
						totalPerCode: lineInfo.totalPerCode,
						partCost: lineInfo.partCost,
						partDescription: lineInfo.partDescription,
						materialCost: lineInfo.materialCost,
						$postPartDescription: lineInfo.postPartDescription,
						$rejected: (lineInfo.accepted == 1) ? false : true,
						quantity: lineInfo.postParts,
						TPI: lineInfo.preTPI,
						repairCode: lineInfo.postRepairCode,
						partCode: lineInfo.postPartCode
					}
					
					if (postPart.$rejected) {
						obj.parts[lineInfo.repairIndex].$aggregateRejected = true;
					}
					
					postPart.$accepted = !postPart.$rejected;
					
					if (lineInfo.preRepairCode) {
						var prePart = {
							totalPerCode: lineInfo.totalPerCode,
							partCost: lineInfo.partCost,
							partDescription: lineInfo.partDescription,
							materialCost: lineInfo.materialCost,
							$rejected: (lineInfo.accepted == 1) ? false : true,
							quantity: lineInfo.preParts,
							TPI: lineInfo.preTPI,
							partCode: lineInfo.prePartCode,
							repairCode: lineInfo.preRepairCode
						};
						
						obj.preAuditParts[lineInfo.repairIndex].$childParts[lineInfo.partIndex] = prePart;
					} else {
						postPart.$addedPart = true;
					}
					
					// Values are set, we can add the lines to the arrays
					
					obj.parts[lineInfo.repairIndex].$childParts[lineInfo.partIndex] = postPart;
				}
			});
		},
		setPendingParts : function(workOrderLines){
			repairs = [];
			parts = [];
			
			angular.forEach(workOrderLines, function(lineInfo, idxr){
				
				if(lineInfo && lineInfo.woRepair){
					repairLine = {};
					repairLine.$accepted = lineInfo.accepted;
					repairLine.$aggregateRejected = !lineInfo.accepted;
					repairLine.$childParts = [];
					repairLine.$idx = lineInfo.repairIndex
					repairLine.$rejected = !lineInfo.accepted
					repairLine.damageCode = lineInfo.postDamageCode;
					repairLine.repairLocCode = lineInfo.postRepairLocCode;
					repairLine.TPI = lineInfo.postTPI;
					repairLine.manHrs = lineInfo.manHrs;
					repairLine.materialCost = lineInfo.materialCost;
					repairLine.quantity = lineInfo.postQuantity;
					repairLine.repairCode = lineInfo.postRepairCode;
					repairLine.repairDescription = lineInfo.repairDescription;
					repairLine.totalPerCode = lineInfo.totalPerCode;
					repairLine.$ratingRQ = lineInfo.repairQuality;
					
					if(lineInfo.addedPart){
						repairLine.$addedPart = lineInfo.addedPart;
					}
					
					repairs.push(repairLine);
				}else{
					var partLine = {
							$accepted : lineInfo.accepted,
							partIndex: lineInfo.partIndex,
							quantity: lineInfo.postQuantity,
							repairCode: lineInfo.postRepairCode,
							partCode : lineInfo.postPartCode,
							$rejected : !lineInfo.accepted,
							$aggregateRejected : !lineInfo.accepted,
							repairIndex: lineInfo.repairIndex
						}
					
					if(lineInfo.addedPart){
						partLine.$addedPart = lineInfo.addedPart;
					}
						
					if(repairs[lineInfo.repairIndex]) {
						repairs[lineInfo.repairIndex].$childParts.push(partLine);
					}else{
						parts.push(partLine);
					}
					
				}
				
				if(parts.length > 0){
					angular.forEach(parts, function(part, idxr){
						repairs[part.repairIndex].$childParts.push(part);;
					});
				}
				
				obj.preAuditParts = repairs;
				obj.parts = angular.copy(obj.preAuditParts);
				
			});
			
		},
		setStoredWorkOrderParts : function(workOrderNo, visitId) {
			var dfd = $.Deferred();
			obj.parts = [];
			obj.preAuditParts = [];
			
			// loop through and set obj.parts and obj.preAuditParts
			
			startedAudit.getAuditPartsForWorkOrder(workOrderNo, visitId).then(function(woLinesObject){
				// Loop through repairs and parts and generate parts and preAudit parts arrays
				
				var repairLines = woLinesObject.repairLines;
				var partLines = woLinesObject.partLines;
				
				angular.forEach(repairLines, function(repair, idxr){
					
					var repairLine = {
						workOrderId: repair.workOrderId,
						partCode: repair.postPartCode,
						$postRepairDescription: repair.$postRepairDescription,
						repairDescription: repair.repairDescription,
						partDescription: repair.partDescription,
						quantity: repair.postQuantity,
						damageCode: repair.postDamageCode,
						repairCode: repair.postRepairCode,
						repairLocCode: repair.postRepairLocCode,
						manHrs: repair.manHrs,
						partCost: repair.partCost,
						materialCost: repair.materialCost,
						TPI: repair.postTPI,
						totalPerCode: repair.totalPerCode,
						$idx: repair.repairIndex,
						$rejected: (repair.accepted == 1) ? false : true
					}
					
					if(repair.repairQuality){
						repairLine.$ratingRQ = repair.repairQuality;
					}
						
					repairLine.$accepted = !repairLine.$rejected;
					
					if (repairLine.$rejected) {
						repairLine.$aggregateRejected = true;
					} else {
						repairLine.$aggregateRejected = false;
					}
					
					if (repair.hasChildParts) {
						repairLine.$childParts = [];
					}
					
					// Save repair line in post audit array
					
					if (repair.preRepairCode) {
						var preRepairLine = {
								partDescription: repair.partDescription,
								quantity: repair.preQuantity,
								damageCode: repair.preDamageCode,
								repairCode: repair.preRepairCode,
								repairLocCode: repair.preRepairLocCode,
								manHrs: repair.manHrs,
								partCost: repair.partCost,
								TPI: repair.preTPI,
								$childParts: []
							}
							
						obj.preAuditParts[repairLine.$idx] = preRepairLine;
					} else {
						repairLine.$addedPart = true;
					}
					
					obj.parts[repairLine.$idx] = repairLine; 
				});
				
				// Now that repair lines are set we loop through part lines as well
				
				angular.forEach(partLines, function(part, idxp){
					
					var partLine = {
						$idx: part.partIndex,
						workOrderId: part.workOrderId,
						materialCost: part.materialCost,
						partCode: part.postPartCode,
						partCost: part.partCost,
						$postPartDescription: part.$postPartDescription,
						partDescription: part.partDescription,
						quantity: part.postQuantity,
						repairCode: part.postRepairCode,
						totalPerCode: part.totalPerCode,
						$parentIndex: part.repairIndex,
						$rejected: (part.accepted == 1) ? false : true	
					}
					
					partLine.$accepted = !partLine.$rejected;
					
					if (part.prePartCode) {
						var prePartLine = {
							$idx: part.index,
							workOrderId: part.workOrderId,
							materialCost: part.materialCost,
							partCode: part.prePartCode,
							partCost: part.partCost,
							partDescription: part.partDescription,
							quantity: part.preQuantity,
							repairCode: part.preRepairCode,
							totalPerCode: part.totalPerCode,
							$parentIndex: part.parentIndex,
						}
							
						// Save part in the pre audit array
						
						obj.preAuditParts[part.repairIndex].$childParts[part.partIndex] = prePartLine; 
					} else {
						partLine.$addedPart = true;
					}
					
					// Save part in the post audit array
					
					 obj.parts[part.repairIndex].$childParts[part.partIndex] = partLine; 
					 
					 if (partLine.$rejected) {
						 obj.parts[part.repairIndex].$aggregateRejected = true;
					 }
					
				})
				
				dfd.resolve();
			});
			
			return dfd.promise();
		},
		
		getObj : function(){
			return obj;
		},
		setContainerNo : function(No){
			obj.containerNo = No;
		},
		setWorkOrderStatus : function(woStatus){
			obj.woStatus = woStatus;
		},
		setStatusCode : function(statusCode){
			obj.statusCode = statusCode;
		},
		setWORatingQuality : function(ratingRQ){
			obj.ratingRQ = ratingRQ;
		},
		getContainerNo : function(){
			return obj.containerNo;
		},
		setWorkOrderNo : function(No){
			obj.workOrderNo = No;
		},
		setVisitId : function(visitId){
			obj.visitId = visitId;
		},
		cloneContainerCollection : function(){
			var dfd = $.Deferred();
			
			obj.originalComponents = angular.copy(obj.components);
			$localStorage[obj.containerId] = angular.copy(obj.components);
			
			dfd.resolve();
			
			return dfd.promise();
		},
		
		saveToLocalStorage : function(collection){
			delete $localStorage[obj.storageHelpers[obj.workOrderNo]];
			
			var tempKey = "h-" + new Date().getTime();
			$localStorage[tempKey] = collection;
			obj.storageHelpers[obj.workOrderNo] = tempKey;
			
			delete $localStorage[obj.workOrderNo];
			$localStorage[obj.workOrderNo] = collection;
		},
		
		getLocalStorage : function(){
			console.log('storage!', $localStorage[obj.workOrderNo]);
		},
		
		resetLocalStorage : function(){
			$localStorage.$reset();
		},
		
		generateComment : function(postRepairs, preRepairs) {
			var rejectedComment = "";
			
			angular.forEach(postRepairs, function(postRepair, repairIndex){
				var preRepair = preRepairs[repairIndex];
				
				if ((postRepair.$rejected || postRepair.$aggregateRejected) && !postRepair.$addedPart) {
					// Handle changed repairs
					var identifiedByRepairCode = hasUniqueRepairCode(preRepair, preRepairs);
					
					if (postRepair.$rejected) rejectedComment += getRejectedRepairDescription(postRepair, preRepair, identifiedByRepairCode) + "\n";
					
					if (postRepair.$aggregateRejected) {
						angular.forEach(postRepair.$childParts, function(postPart, partIndex) {
							if (postPart.$rejected && !postPart.$addedPart) {
								// Handle changed parts
								var prePart = preRepairs[repairIndex].$childParts[partIndex];
								rejectedComment += getRejectedPartDescription(postPart, prePart, identifiedByRepairCode, preRepair) + "\n";
							} else if (postPart.$addedPart) {
								// Handle added parts
								rejectedComment += getRejectedPartDescription(postPart, null, identifiedByRepairCode, postRepair) + "\n";
							}
						});
					}
				} else if (postRepair.$addedPart) {
					// Handle added repairs
					rejectedComment += getRejectedRepairDescription(postRepair, null, identifiedByRepairCode) + "\n";
					// Look into added child parts as well.
					angular.forEach(postRepair.$childParts, function(postPart, partIndex) {
						rejectedComment += getRejectedPartDescription(postPart, null, identifiedByRepairCode, postRepair) + "\n";
					})
				}
			});
			
			return rejectedComment;
		},
		
		setInspectionStatus : function(inspectionStatus, workOrderStatus) {
			var dfd = $.Deferred();
			var that = this;
			
			switch(inspectionStatus) {
				case 'Planned':
					this.setStartedStatus();
					dfd.resolve();
					break;
				// started	
				case 'Started':
					switch(workOrderStatus) {
						case 'Started':
							this.setInProgressStatus();
							if (obj.workOrderNo && $localStorage[obj.workOrderNo]) {
								obj.parts = $localStorage[obj.workOrderNo];
							}
							dfd.resolve();
							break;
						case 'Planned':
							that.setStartedStatus();
							dfd.resolve();
							break;
						case 'Finished':
							that.setFinishedStatus();
							dfd.resolve();
							break;
					}
					break;
				case 'Finished':
					dfd.resolve();
					this.setFinishedStatus();
					break;
				case 'Pending':
					dfd.resolve();
					this.setFinishedStatus();
					break;
				default: 
					this.setStartedStatus();
					dfd.resolve();
			}
			
			return dfd.promise();
		},
		
		setInspectionStatus2 : function(inspectionStatus){
			var dfd = $.Deferred();

			switch(inspectionStatus) {
				case 'Not started':
					this.setStartedStatus();
					dfd.resolve();
					break;
				case 'In progress':
					this.setInProgressStatus();
					if (obj.containerNo && $localStorage[obj.containerId]) {
						obj.parts = $localStorage[obj.containerId];
					}
					dfd.resolve();
					break;
				case 'Rejected':
				case 'Approved':
				case 'Finished': // don't know what cases we need yet
					this.setFinishedStatus();
					dfd.resolve();
					break;
				default:
					this.setStartedStatus();
					dfd.resolve();
			}
			
			
			
			return dfd.promise();
		},
		
		removeComponent : function(index){
			var dfd = $.Deferred();
			var that = this;

			$localDB.find('containerClone', {containerNo : obj.containerNo}, {exact : true}).then(function(components){
				$localDB.erase('containerClone', components[index], {exact : true}).then(function(){
					obj.components.splice(index, 1);
					dfd.resolve(obj.components);
				}).fail(function(){
					dfd.reject("Failed to remove containers.");
				});
			}).fail(function(){
				dfd.reject("Failed to find containers.");
			});
			
			return dfd.promise();
		},
		
		addComponent : function(newComponent){
//			probabil ca am toate informatiile intr-un obiect si trebuie doar sa dau add
//			hardcodez ceva momentan
		},
		//Check if visit has rejected work orders
//		checkIfRejected : function(){
//			var visitId = obj.visitId;
//            var containerNo = obj.containerNo;
//            var workOrderNo = obj.workOrderNo;
//            var finalComment = obj.finalComment;
//            var workOrderAuditResult = this.getWorkOrderAuditResult(obj.parts);
//            for (var idx = 0; idx < $depot.getobject().workOrdersForAudit.length; idx++) {
//                if ($depot.getobject().workOrdersForAudit[idx].workorderid === workOrderNo && workOrderAuditResult === false) {
//                    $depot.getobject().workOrdersForAudit[idx].auditresult = "Rejected";
//                    return true;
//                }
//            }
//            
//            return false;
//		},
		//
		
		finishInspection : function(inspectionType){
			 var dfd = $.Deferred();
            var that = this;

            var visitId = obj.visitId;
            var containerNo = obj.containerNo;
            var workOrderNo = obj.workOrderNo;
            var finalComment = obj.finalComment;
            var avarageRating = obj.ratingRQ;

            // loop through all parts and set individual status to aggregate status
            this.setAggregateStatuses();
            var workOrderAuditResult = this.getWorkOrderAuditResult(obj.parts); 
            
            if(inspectionType && inspectionType.localeCompare('Estimated But Rejected WO')==0){
            	
            	workOrderAuditResult = false; //Reject WO if type of inspection is Correct Estimated but Reject WO
            	
            	WL.Analytics.log({
            		AC_action : "ECRW",
            		AC_userName : userContext.getUsername(),
            		AC_depotCode : $depot.getobject().repairShopDetails.shopCode,
            		AC_visitId: visitId,
            		AC_containerId: containerNo,
            		AC_workorderId: workOrderNo,
            		AC_timestamp : getUTCDate() },'Finish Inspection - Estimated Correct but Rejected WO');
            }else{
            	//Finish Inspection - classic mode
            	WL.Analytics.log({
            		AC_action : "FI",
            		AC_userName : userContext.getUsername(),
            		AC_depotCode : $depot.getobject().repairShopDetails.shopCode,
            		AC_visitId: visitId,
            		AC_containerId: containerNo,
            		AC_workorderId: workOrderNo,
            		AC_timestamp : getUTCDate() },'Finish Inspection - Estimated Correct but Rejected WO');
            }
            
            if (obj.parts.length > 0) {
                startedAudit.saveAuditParts(visitId, workOrderNo, obj.preAuditParts, obj.parts).then(function () {
                    startedAudit.finishWorkOrder(workOrderNo, visitId, workOrderAuditResult, finalComment,avarageRating).then(function () {
                        for (var idx = 0; idx < $depot.getobject().workOrdersForAudit.length; idx++) {
                            if ($depot.getobject().workOrdersForAudit[idx].workorderid === workOrderNo && workOrderAuditResult === true)
                                $depot.getobject().workOrdersForAudit[idx].auditresult = "Accepted";
                            else if ($depot.getobject().workOrdersForAudit[idx].workorderid === workOrderNo && workOrderAuditResult === false) {
                                $depot.getobject().workOrdersForAudit[idx].auditresult = "Rejected";
                            }
                        }

                        if ($localStorage[obj.workOrderNo]) delete $localStorage[obj.workOrderNo];
                        that.setFinishedStatus();
                        dfd.resolve();
                    });
                }).fail(function () {
                    dfd.reject();
                });
            } else {
                dfd.reject() // this should never happen
            }

            return dfd.promise();
		},
		
		setAggregateStatuses : function() {
			for (var i = 0; i < obj.parts.length; i++) {
				var repair = obj.parts[i];
				repair.$singleRejected = repair.$rejected;
				repair.$rejected = repair.$aggregateRejected;
				
				for (var j =0; j < repair.$childParts.length; j++) {
					var part = repair.$childParts[j];
					part.$singleRejected = part.$rejected;
					part.$rejected = repair.$aggregateRejected;
				}
			}
		},
		
		getWorkOrderAuditResult : function(parts){
			var workOrderResult = true; // accepted
			
			for (var i = 0; i < parts.length; i++) {
				var part = parts[i];
				
				if (part.$rejected) {
					workOrderResult = false;
					break;
				}
			}
			
			return workOrderResult;
		},
		
		setStartedStatus : function(){
			this.setReferUnitStatus(false, false, true);
		},
		
		setFinishedStatus : function(){
			this.setReferUnitStatus(false, true, false);
		},
		
		setInProgressStatus : function(){
			this.setReferUnitStatus(true, false, false);
		},
		
		setReferUnitStatus : function(started, finished, pending){
			obj.referUnitStatus.started = started;
			obj.referUnitStatus.finished=finished;
			obj.referUnitStatus.pending=pending;
			obj.action = (started)? Messages.finishInspection : (finished) ? Messages.finishedInspection : Messages.startInspection; 
		},
		getReferUnitStatus : function(){
			return setReferUnitStatus;
		},
		getAction : function(){
			return obj.action;
		},
		getIfContainerHasComponents : function(){
			return obj.withoutComponents ;
		}
	}
	
	function hasUniqueRepairCode(preRepair, preParts) {
		var unique = true;
		var count = 0;
		
		for (var i = 0; i < preParts.length; i++) {
			var repair = preParts[i];
			
			if (preRepair.repairCode == repair.repairCode) {
				count++;
				if (count >= 2) {
					unique = false;
					break;
				}
			}
		}
		
		return unique;
	}
	
	function getRejectedRepairDescription(postRepair, preRepair, hasUniqueRepairCode) {
		// Can use this for added repairs as well, check if preRepair is null
		var description = "";
		var keysArray = ["quantity", "damageCode", "TPI", "repairCode", "repairLocCode"]; // In order, maps with messages values
		
		if (preRepair == null) { // This is an added part
			description += Messages.addRepair + ": ";
			description += postRepair.quantity + " - " + postRepair.repairCode + "/" + postRepair.repairLocCode;
			return description; // Enough info for added repair
		}
		
		if (hasUniqueRepairCode) {
			// Need to specify only repair code to identify repair
			description = preRepair.repairCode + " : ";
		} else {
			// Need to specify repair loc code also to identify repair
			description = preRepair.repairCode + "/" + preRepair.repairLocCode + " : ";
		}
		
		// Pcs is 0 - Repair needs to be removed, no need to check other changes
		if (postRepair.quantity == 0) {
			description += Messages.remove;
			return description;
		}
		
		// Regular changes
		angular.forEach(keysArray, function(keyValue) {
			if (postRepair[keyValue] != preRepair[keyValue]) {
				if(!postRepair[keyValue]) {
					description += Messages[keyValue] + " " + Messages.changedTo + " " +" N/A " + postRepair[keyValue] + ", ";
				}else{
					description += Messages[keyValue] + " " + Messages.changedTo + " " + postRepair[keyValue] + ", ";
				}
			}
		});
		
		// Remove extra comma
		description = description.slice(0, -2);
		
		return description;
	}
	
	function getRejectedPartDescription(postPart, prePart, hasUniqueRepairCode, parent) {
		// Can use this for added parts as well, check if prePart is null
		var description = "";
		var keysArray = ["quantity", "partCode"]; // In order, maps with messages values
		
		if (prePart == null) { // This is an added part
			description += Messages.addPart + ": ";
			description += postPart.quantity + " - " + postPart.repairCode + "/" + parent.repairLocCode + "/" + postPart.partCode;
			return description; // Enough info for added repair
		}
		
		if (hasUniqueRepairCode) {
			// Need to specify only repair code to identify repair
			description += prePart.repairCode + "/" + prePart.partCode + " : ";
		} else {
			// Need to specify repair loc code also to identify repair
			description += prePart.repairCode + "/" + parent.repairLocCode + "/" + prePart.partCode + " : ";
		}
		
		// Pcs is 0 - Repair needs to be removed, no need to check other changes
		if (postPart.quantity == 0) {
			description += Messages.remove;
			return description;
		}
		
		// Regular changes
		angular.forEach(keysArray, function(keyValue) {
			if (postPart[keyValue] != prePart[keyValue]) {
				if(!postPart[keyValue]) {
					description += Messages[keyValue] + " " + Messages.changedTo + " N/A " + postPart[keyValue] + ", ";
				} else {
				description += Messages[keyValue] + " " + Messages.changedTo + " " + postPart[keyValue] + ", ";
				}
			}
		});
		
		// Remove extra comma
		description = description.slice(0, -2);
		
		return description;
	}
	
});
	