/**
 * June, 1 2016
 * WOCodesService.js
 * Stavarache Vlad
 */

/**
 * WOCodesService Factory
 * @namespace Factories
 */
(function(){
	'use strict';
	
	audit
    .factory('WOCodesService', WOCodesService);
	
	WOCodesService.$inject = ['$localDB', 'multiModal', '$rootScope', '$location', 'responseHandler', '$ionicLoading', 'ChunksRequestModule'];
	
	/**
	 * @namespace WOCodesService
	 * @desc Service that retrieves data for system info modal
	 * Serves as a model for the UI
	 * Data consists mainly of various codes and their descriptions
	 * which are useful for auditors to perform their job
	 * @memberOf Factories
	 */
	function WOCodesService($localDB, multiModal, $rootScope, $location, responseHandler, $ionicLoading, ChunksRequestModule) {
		
		var codesInfo = {
			repairLocCodes : [], //  Container Repair Location data
			damageCodes : [], // Damage Codes data
			shopInfo : [], // Shop Profile data
			masterParts : []
		}
		
		var repairSTSCodes = [];
		var associatedParts = [];
		var shopLimits = [];
		var laborRates = [];
		
		var selectInfo = {
			manualCodesList : [],
			partGroupsList : [],
			manufacturerCodesList : []
		}
		
		var service = {
			initCodes: initCodes,
			getRepairCodes: getRepairCodes,
			getDamageCodes: getDamageCodes,
			getCodesInfo: getCodesInfo,
			getDropdownListsInfo: getDropdownListsInfo, 
			getPartCodes: getPartCodes,
			getPartDescription: getPartDescription,
			getRepairDescription: getRepairDescription,
			//  Container Repair Location - View Methods:
			getRepairLocCodesInfo: getRepairLocCodesInfo,
			refreshRepairLocCodesInfo: refreshRepairLocCodesInfo,
			// - Shop Profile - View Methods
			getShopsInfo: getShopsInfo,
			refreshShopsInfo: refreshShopsInfo,
			// - Damage Codes - View Methods
			getDamageCodesInfo: getDamageCodesInfo,
			refreshDamageCodesInfo: refreshDamageCodesInfo,
			// - Master Parts - View Methods
			getMasterPartCodes: getMasterPartCodes,
			// - RepairSTS Codes - View Methods
			getRepairSTSCodes: getRepairSTSCodes,
			// - Associate Parts with Code/Mode - View Methods
			getAssociatedPartsWithCodes: getAssociatedPartsWithCodes,
			// - Shop Limits - View Methods
			getShopLimits: getShopLimits,
			// - Labor Rates - View Methods
			getLaborRates: getLaborRates,
			// - Shop Contract - View Methods
			getContractsSearch: getContractsSearch,
			// - Get manual modes dropdown 
			getManualCodes: getManualCodes,
			getManualCodesOffline: getManualCodesOffline,
			getPartsGroupCodes: getPartsGroupCodes,
			getPartsGroupCodesOffline: getPartsGroupCodesOffline,
			getManufacturerCodes: getManufacturerCodes,
			getManufacturerCodesOffline: getManufacturerCodesOffline
		};
		
		return service;
		
		function getCodesInfo() {
			return codesInfo;
		}
		
		function getDropdownListsInfo() {
			return selectInfo;
		}
		
		/**
	     * @name initCodes
	     * @desc function called on login
		 * gets repair codes and their description
		 * used for inspection functionality to populate 
		 * repair description field when user changes the repair code
	     * @param {N/A}
	     * @returns {Promise}
	     * @memberOf Factories.WOCodesService
	     */
		function initCodes() {
			var defer = $.Deferred();
			
			$.when.apply($, [getRepairCodes()]).then(function(){
				defer.resolve();
			}).fail(function(error){
				errorHandler(error, defer);
			});
			
			return defer.promise();
		}
		
		function getRepairDescription(repairCode, mode) {
			return getCodeDescription("repairCodes", repairCode, mode);
		}
		
		function getPartDescription(partCode) {
			return getCodeDescription("masterParts", partCode);
		}
		
		/**
	     * @name getCodeDescription
	     * @desc Gets the description of a code based on passed params
		 * used for either repairCodes or masterParts collections
	     * @param {String} collectionName - json store collection to search in
		 * @param {String} searchCode
		 * @param {String} searchMode
	     * @returns {Promise}
		 * @resolve {String} - code description
	     * @memberOf Factories.WOCodesService
	     */
		function getCodeDescription(collectionName, searchCode, searchMode) {
			var defer = $.Deferred();
			
			var searchObject = {code: searchCode};
			
			if (typeof searchMode != "undefined") {
				searchObject.mode = searchMode;
			}
			
			$localDB.find(collectionName, searchObject, {exact: true}).then(function(codeData) {
				if (codeData && codeData.length > 0) {
					var codeInfo = codeData[0].json;
					
					defer.resolve(codeInfo.description);
				} else {
					defer.resolve(null); // Code doesn't match any description
				}
			}).fail(function(error){
				defer.reject(error);
			})
			
			return defer.promise();
		}
		
		function getRepairCodes() {
			var defer = $.Deferred();
			
			WL.Client.invokeProcedure({
				adapter : 'WOCodesAdapter',
				procedure : 'getRepairCodes',
				parameters : [userContext.getUsername(), userContext.getToken()]
			}, {
				onSuccess : function(response){ 
					if (!response.invocationResult.isSuccessful) {
						defer.reject(response.invocationResult);
					} else {
						var results = response.invocationResult.invocationResult;
						
						$localDB.clearCollection('repairCodes').then(function(){
							$localDB.add('repairCodes', results, {markDirty: false}).then(function(){
								defer.resolve();
							});
						});
					}
				},
				onFailure: function(error){
					errorHandler(error, defer);
				}
			});		  
			return defer.promise();
		}
		
		function getPartCodes() {
			var defer = $.Deferred();
			
			WL.Client.invokeProcedure({
				adapter : 'WOCodesAdapter',
				procedure : 'getPartCodes',
				parameters : [userContext.getUsername(), userContext.getToken()]
			}, {
				onSuccess : function(response){ 
					if (!response.invocationResult.isSuccessful) {
						defer.reject(response.invocationResult);
					} else {
						var results = response.invocationResult.invocationResult;
						$localDB.clearCollection('partCodes').then(function(){
							$localDB.add('partCodes', results, {markDirty: false}).then(function(){
								defer.resolve();
							});
						});
					}
				},
				onFailure: function(error){
					errorHandler(error, defer);
				}
			});		  
			return defer.promise();
		}
		
		function getDamageCodes() {
			var defer = $.Deferred();
			WL.Client.invokeProcedure({
				adapter : 'WOCodesAdapter',
				procedure : 'getDamageCodes',
				parameters : [userContext.getUsername(), userContext.getToken()]
			}, {
				onSuccess : function(response){ 
					console.log('response getDamageCodes');
					if (!response.invocationResult.isSuccessful) {
						defer.reject(response.invocationResult);
					} else {
						var results = response.invocationResult.invocationResult;
						codesInfo.damageCodes = results;
						
						$localDB.clearCollection('damageCodes').then(function(){
							$localDB.add('damageCodes', results, {markDirty: false}).then(function(){
								defer.resolve();
							}).fail(function(error){
								defer.reject(error);
							});
						}).fail(function(error){
							defer.reject(error);
						});
					}
				},
				onFailure: function(error){
					errorHandler(error, defer);
				}
			});		  
			return defer.promise();
		}
		
		/**
	     * @name getManualCodes
	     * @desc Gets manual codes values used to populate a dropdown
		 * in system info modal
	     * @param {N/A}
	     * @returns {Promise}
	     * @memberOf Factories.WOCodesService
	     */
		function getManualCodes() {
			var defer = $.Deferred();
			WL.Client.invokeProcedure({
				adapter : 'WOCodesAdapter',
				procedure : 'getManualCodes',
				parameters : [userContext.getUsername(), userContext.getToken()]
			}, {
				onSuccess : function(response){ 
					if (!response.invocationResult.isSuccessful) {
						defer.reject(response.invocationResult);
					} else {
						var results = response.invocationResult.invocationResult;
						selectInfo.manualCodesList = results;
						
						$localDB.clearCollection('manualCodesList').then(function(){
							$localDB.add('manualCodesList', results, {markDirty: false}).then(function(){
								defer.resolve();
							}).fail(function(error){
								defer.reject(error);
							});
						}).fail(function(error){
							defer.reject(error);
						});
					}
				},
				onFailure: function(error){
					errorHandler(error, defer);
				}
			});		  
			return defer.promise();
		}
		
		function getManualCodesOffline() {
			var defer = $.Deferred();
			var results = [];
			
			$localDB.find("manualCodesList", {}, {}).then(function(data){
				results = _jsonStoreParse(data);
				selectInfo.manualCodesList = results;
				defer.resolve(results);
			}).fail(function(error){
				defer.reject(error);
			})
			
			return defer.promise();
		}
		
		/**
	     * @name getPartsGroupCodes
	     * @desc Gets part group codes values used to populate a dropdown
		 * in system info modal
	     * @param {N/A}
	     * @returns {Promise}
	     * @memberOf Factories.WOCodesService
	     */
		function getPartsGroupCodes() {
			var defer = $.Deferred();
			WL.Client.invokeProcedure({
				adapter : 'WOCodesAdapter',
				procedure : 'getPartsGroupCodes',
				parameters : [userContext.getUsername(), userContext.getToken()]
			}, {
				onSuccess : function(response){ 
					console.log('response getPartsGroupCodes');
					if (!response.invocationResult.isSuccessful) {
						defer.reject(response.invocationResult);
					} else {
						var results = response.invocationResult.invocationResult;
						selectInfo.partGroupsList = results;
						
						$localDB.clearCollection('partsGroupList').then(function(){
							$localDB.add('partsGroupList', results, {markDirty: false}).then(function(){
								defer.resolve();
							}).fail(function(error){
								defer.reject(error);
							});
						}).fail(function(error){
							defer.reject(error);
						});
					}
				},
				onFailure: function(error){
					errorHandler(error, defer);
				}
			});		  
			return defer.promise();
		}
		
		function getPartsGroupCodesOffline() {
			var defer = $.Deferred();
			var results = [];
			
			$localDB.find("partsGroupList", {}, {}).then(function(data){
				results = _jsonStoreParse(data);
				selectInfo.partGroupsList = results;
				defer.resolve(results);
			}).fail(function(error){
				defer.reject(error);
			})
			
			return defer.promise();
		}
		
		/**
	     * @name getManufacturerCodes
	     * @desc Gets manufacturer codes values used to populate a dropdown
		 * in system info modal
	     * @param {N/A}
	     * @returns {Promise}
	     * @memberOf Factories.WOCodesService
	     */
		function getManufacturerCodes() {
			var defer = $.Deferred();
			WL.Client.invokeProcedure({
				adapter : 'WOCodesAdapter',
				procedure : 'getManufacturerCodes',
				parameters : [userContext.getUsername(), userContext.getToken()]
			}, {
				onSuccess : function(response){ 
					if (!response.invocationResult.isSuccessful) {
						defer.reject(response.invocationResult);
					} else {
						var results = response.invocationResult.invocationResult;
						selectInfo.manufacturerCodesList = results;
						
						$localDB.clearCollection('manufacturerCodesList').then(function(){
							$localDB.add('manufacturerCodesList', results, {markDirty: false}).then(function(){
								defer.resolve();
							}).fail(function(error){
								defer.reject(error);
							});
						}).fail(function(error){
							defer.reject(error);
						});
					}
				},
				onFailure: function(error){
					errorHandler(error, defer);
				}
			});		  
			return defer.promise();
		}
		
		function getManufacturerCodesOffline() {
			var defer = $.Deferred();
			var results = [];
			
			$localDB.find("manufacturerCodesList", {}, {}).then(function(data){
				results = _jsonStoreParse(data);
				selectInfo.manufacturerCodesList = results;
				defer.resolve(results);
			}).fail(function(error){
				defer.reject(error);
			})
			
			return defer.promise();
		}
		
		function getRepairLocCodesInfo() {
			return _getCollectionInfo('repairLocCodes', 'getRepairLocCodes', []);
		}
		
		function refreshRepairLocCodesInfo() {
			return _refreshCollectionInfo('repairLocCodes', 'getRepairLocCodes', []);
		}
		
		function getShopsInfo() {
			return _getCollectionInfo('shopInfo', 'getAllShopsInfo', ["chts"]);
		}
		
		function refreshShopsInfo() {
			return _refreshCollectionInfo('shopInfo', 'getAllShopsInfo', ["chts"]);
		}
		
		function getDamageCodesInfo() {
			return _getCollectionInfo('damageCodes', 'getDamageCodes', ["chts"]);
		}
		
		function refreshDamageCodesInfo() {
			return _refreshCollectionInfo('damageCodes', 'getDamageCodes', ["chts"]);
		}
		
		function _getDataFromDB(collectionName, procedure, dateFields) {
			var defer = $.Deferred();
			
			WL.Client.invokeProcedure({
				adapter : 'WOCodesAdapter',
				procedure : procedure,
				parameters : [userContext.getUsername(), userContext.getToken()]
			}, {
				onSuccess : function(response){ 
					console.log('_getDataFromDB');
					if (!response.invocationResult.isSuccessful) {
						defer.reject(response.invocationResult);
					} else {
						var results = response.invocationResult.invocationResult;
						
						$localDB.clearCollection(collectionName).then(function(){
							$localDB.add(collectionName, results, {markDirty: false}).then(function(){
								defer.resolve(results);
							}).fail(function(error){
								defer.reject(error);
							});
						}).fail(function(error){
							defer.reject(error);
						});
					}
				},
				onFailure: function(error){
					errorHandler(error, defer);
				}
			});	
			
			return defer.promise();
		}
		
		function getMasterPartCodes() {
			var collectionInfo = {mainCollection : "masterParts", mainCollectionLoaded : "masterPartsLoaded"};
			
			return ChunksRequestModule.getData(codesInfo.masterParts, _getPartCodesForModes, _getCountForPartCodesForModes, collectionInfo);
		}
		
		function getRepairSTSCodes() {
			var collectionInfo = {mainCollection : "repairSTSCodes", mainCollectionLoaded : "repairSTTCodesLoadded"};
			
			return ChunksRequestModule.getData(repairSTSCodes, _getPartialRepairSTSCodes, _getCountForRepairSTSCodes, collectionInfo);
		}		
		
		function getAssociatedPartsWithCodes() {
			var collectionInfo = {mainCollection : "associatedParts", mainCollectionLoaded : "associatedPartsLoaded"};
			
			return ChunksRequestModule.getData(associatedParts, _getPartialAssociatedPartsWithCodes, _getCountForAssociatedPartsWithCodes, collectionInfo);
		}
		
		function getShopLimits() {
			var collectionInfo = {mainCollection : "shopLimits", mainCollectionLoaded : "shopLimitsLoadded"};
			
			return ChunksRequestModule.getData(shopLimits, _getPartialShopsLimits, _getCountForShopsLimits, collectionInfo);
		}
		
		function getLaborRates() {
			var collectionInfo = {mainCollection : "laborRates", mainCollectionLoaded : "laborRatesLoaded"};
			
			return ChunksRequestModule.getData(laborRates, _getPartialLaborRates, _getCountForLaborRates, collectionInfo);
		}
		
		function _addToArray(mainArray, arrayToAdd) {
			Array.prototype.push.apply(mainArray, arrayToAdd);
		}
		
		function _areMasterPartsLoaded() {
			var defer = $.Deferred();
			var partsLoaded = false;
			
			$localDB.find("masterPartsLoaded", {}, {}).then(function(data){
				if (data && data.length > 0) {
					partsLoaded = data[0].json.loaded;
					defer.resolve(partsLoaded);
				} else {
					defer.resolve(partsLoaded);
				}
			}).fail(function(error){
				defer.reject(error);
			})
			
			return defer.promise();
		}
		
		function _getPartCodesForModes(offset, limit, otherParameters) {
			return _getPartialData('getPartCodesForModes', offset, limit)
		}
		
		function _getCountForPartCodesForModes() {
			return _getQueryCount('getTotalCountForCodesForModes');
		}
		
		function _getPartialRepairSTSCodes(offset, limit) {
			return _getPartialData('getPartialRepairSTSCodes', offset, limit)
		}
		
		function _getCountForRepairSTSCodes() {
			return _getQueryCount('getTotalCountForRepairSTSCodes');
		}
		
		function _getPartialAssociatedPartsWithCodes(offset, limit) {
			return _getPartialData('getPartialAssociatePartsWithCodes', offset, limit)
		}
		
		function _getCountForAssociatedPartsWithCodes() {
			return _getQueryCount('getTotalCountForAssociatePartsWithCodes');
		}
		
		function _getPartialShopsLimits(offset, limit) {
			return _getPartialData('getPartialShopLimits', offset, limit)
		}
		
		function _getCountForShopsLimits() {
			return _getQueryCount('getTotalCountForAllShopLimits');
		}
		
		function _getPartialLaborRates(offset, limit) {
			var defer = $.Deferred();
			
			WL.Client.invokeProcedure({
				adapter : 'WOCodesAdapter',
				procedure : 'getPartialLaborRates',
				parameters : [userContext.getUsername(), userContext.getToken(), offset, limit]
			}, {
				onSuccess : function(response){ 
					if (!response.invocationResult.isSuccessful) {
						defer.reject(response.invocationResult);
					} else {
						var results = response.invocationResult.invocationResult;
						
						for (var i = 0; i < results.length; i++) {
							var itemInfo = results[i];
							itemInfo.expDate_ts = (new Date(itemInfo.expDate)).getTime();
							itemInfo.effDate_ts = (new Date(itemInfo.effDate)).getTime();
						}
						
						defer.resolve(results);
					}
				},
				onFailure: function(error){
					defer.reject(error);
				}
			});	
			
			return defer.promise();
		}
		
		/**
	     * @name getContractsSearch
	     * @desc function called in the contracts view
		 * to search contracts based on params, results are not stored locally
	     * @param {String} shopCode 
		 * @param {String} repairCode
	     * @returns {Promise}
	     * @memberOf Factories.WOCodesService
	     */
		function getContractsSearch(shopCode, repairCode) {
			var defer = $.Deferred();
			
			WL.Client.invokeProcedure({
				adapter : 'WOCodesAdapter',
				procedure : 'getContractsSearch',
				parameters : [userContext.getUsername(), userContext.getToken(), shopCode, repairCode]
			}, {
				onSuccess : function(response){ 
					if (!response.invocationResult.isSuccessful) {
						defer.reject(response.invocationResult);
					} else {
						var results = response.invocationResult.invocationResult;
						
						for (var i = 0; i < results.length; i++) {
							var itemInfo = results[i];
							itemInfo.expDate_ts = (new Date(itemInfo.expDate)).getTime();
							itemInfo.effDate_ts = (new Date(itemInfo.effDate)).getTime();
						}
						
						defer.resolve(results);
					}
				},
				onFailure: function(error){
					defer.reject(error);
				}
			});	
			
			return defer.promise();
		}
		
		function _getCountForLaborRates() {
			return _getQueryCount('getTotalCountForLaborRates');
		}
		
		/**
	     * @name _getPartialData
	     * @desc Necessary to be passed to ChunksRequestModule.getData function
		 * to be called sequentially to get all necessary data
	     * @param {String} procedure - adapter procedure to be called 
		 * @param {Int} offset
		 * @param {Int} limit
	     * @returns {Promise}
	     * @memberOf Factories.WOCodesService
	     */
		function _getPartialData(procedure, offset, limit) {
			var defer = $.Deferred();
			
			WL.Client.invokeProcedure({
				adapter : 'WOCodesAdapter',
				procedure : procedure,
				parameters : [userContext.getUsername(), userContext.getToken(), offset, limit]
			}, {
				onSuccess : function(response){ 
					if (!response.invocationResult.isSuccessful) {
						defer.reject(response.invocationResult);
					} else {
						var results = response.invocationResult.invocationResult;
						defer.resolve(results);
					}
				},
				onFailure: function(error){
					defer.reject(error);
				}
			});	
			
			return defer.promise();
		}
		
		/**
	     * @name _getQueryCount
	     * @desc Necessary to be passed to ChunksRequestModule.getData function
		 * to determine number of requests to be made to get all data sequentially
	     * @param {String} procedure - adapter procedure to be called 
	     * @returns {Promise}
		 * @resolve {Int} count - number of entries a query returns
	     * @memberOf Factories.WOCodesService
	     */
		function _getQueryCount(procedure) {
			var defer = $.Deferred();
			
			WL.Client.invokeProcedure({
				adapter : 'WOCodesAdapter',
				procedure : procedure,
				parameters : [userContext.getUsername(), userContext.getToken()]
			}, {
				onSuccess : function(response){ 
					if (!response.invocationResult.isSuccessful) {
						defer.reject(response.invocationResult);
					} else {
						var count = response.invocationResult.invocationResult;
						defer.resolve(count);
					}
				},
				onFailure: function(error){ 
					defer.reject(error);
				}
			});	
			
			return defer.promise();
		}
		
		/**
	     * @name _getCollectionInfo
	     * @desc Used to get data necessary to populate
		 * different views in the system info modal
		 * the logic is that if the associated data is already in the service
		 * it is just returned
		 * else we try to get the data from json store
		 * else we get it from the server and save it to json store
		 * Note that this is used for the views with less data
	     * @param {String} collectionName
	     * @param {String} procedure - partial data adapter procedure to be called 
	     * @param {Array} dateFields - array of properties with date values to be used to create
		 * new properties by adding "_ts" to property name and
		 * converting their associated values to timestamps
	     * @returns {Promise}
	     * @memberOf Factories.WOCodesService
	     */
		function _getCollectionInfo(collectionName, procedure, dateFields) {
			var defer = $.Deferred();
			
			// Check for data in service scope
			if (codesInfo[collectionName] && codesInfo[collectionName].length > 0) {
				defer.resolve(codesInfo[collectionName]);
			} else {
				// Check for data in JSON Store
				WL.JSONStore.get(collectionName).advancedFind([], {}).then(function(results){
					if (results && results.length > 0) {
						// Data locally
						var parsedResult = _jsonStoreParse(results);
						codesInfo[collectionName] = parsedResult;
						defer.resolve(parsedResult);
					} else {
						// We need to make the request to the server
						_getDataFromDB(collectionName, procedure, dateFields).then(function(results){
							if (dateFields && dateFields.length > 0) {
								_convertDateToTimeStampParse(results, dateFields);
							}
							
							codesInfo[collectionName] = results;
							defer.resolve(codesInfo[collectionName]);
						}).fail(function(error){
							defer.reject(error);
						});
					}
				}).fail(function(error){
					defer.reject(error);
				});
			}
			
			return defer.promise();
		}
		
		/**
	     * @name _refreshCollectionInfo
	     * @desc Used to clear and get data for system info view again from the server
		 * Note that it is not used at the moment in the app
	     * @param {String} collectionName
	     * @param {String} procedure - partial data adapter procedure to be called 
	     * @param {Array} dateFields - array of properties with date values to be used to create
		 * new properties by adding "_ts" to property name and
		 * converting their associated values to timestamps
	     * @returns {Promise}
	     * @memberOf Factories.WOCodesService
	     */
		function _refreshCollectionInfo(collectionName, procedure, dateFields) {
			var defer = $.Deferred();
			
			$localDB.clearCollection(collectionName).then(function(){
				codesInfo[collectionName] = null;
				_getCollectionInfo(collectionName, procedure, dateFields).then(function(results){
					defer.resolve(results);
				}).fail(function(error){
					defer.reject(error);
				});
			}).fail(function(error){
				defer.reject(error);
			});
			
			return defer.promise();
		}
		
		function _convertDateToTimeStampParse(dataSet, dateFields) {
			for (var i = 0; i < dataSet.length; i++) {
				var entry = dataSet[i];
				for (var j = 0; j < dateFields.length; j++) {
					var dateField = dateFields[j];
					// Create new field with same name just add _ts to it
					entry[dateField + "_ts"] = new Date(entry[dateField]).getTime(); 
				}
			}
		}
		
		function _jsonStoreParse(dataSet) {
			var parsedResult = [];
			for (var i = 0; i < dataSet.length; i++) {
				var entry = dataSet[i];
				if (entry && entry.json) {
					parsedResult.push(entry.json);
				}
			}
			return parsedResult;
		}
		
		function successHandler(response, defer,type) {
			console.log('successHandler');
			if (!response.invocationResult.isSuccessful) {
				defer.reject(response.invocationResult);
			} else {
				if(type)
					response.invocationResult.invocationResult.dataResult = response.responseJSON.invocationResult;
				defer.resolve(response.invocationResult.invocationResult );
			}
		}
		
		function errorHandler(error, defer) {
			console.log('error', JSON.stringify(error));
			defer.reject(error);
		} 
		
		function errorHandlerWithFeedback(error, defer) {
			console.log('error', JSON.stringify(error));
			$ionicLoading.hide();
			defer.reject(error);
			responseHandler.feedback(error);
		} 
	}
})();