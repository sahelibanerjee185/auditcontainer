/**
 * June, 22 2017
 * $surveyModule.js
 * Stavarache Vlad
 */

/**
 * $surveyModule Factory
 * @namespace Factories
 */
(function(){
	'use strict';
	
	audit
    .factory('$surveyModule', $surveyModule);
	
	$surveyModule.$inject = ['$localDB', '$ionicLoading', 'auditService', 'startedAudit', '$localStorage', 'PendingAuditsService'];
	
	/**
	 * @namespace $surveyModule
	 * @desc Service used to retrieve visits/work orders data associated with the user
	 * Used as model to populate inspections tab and preview pdf view
	 * @memberOf Factories
	 */
	function $surveyModule($localDB, $ionicLoading, auditService, startedAudit, $localStorage, PendingAuditsService) {
		
		var obj = {
			dateHasVisit : {},
			upcomingVisitDates : [],
			upcomingVisits : {},
			missedVisitDates : [],
			missedVisits : {},
			pastVisitDates : [],
			pastVisits : {},
			startedVisits : {},
			startedVisitInfo : {},
			startedAudit : [],
			currentTab: 'month'
		};
		
		var service = {
			setDatesWithVisit : setDatesWithVisit,
			setDisplayVisits : setDisplayVisits,
			refreshVisitData : refreshVisitData,
			refreshVisitDataOffline : refreshVisitDataOffline,
			updateVisitRefreshTimer : updateVisitRefreshTimer,
			setUpcomingVisits : setUpcomingVisits,
			setPastVisits : setPastVisits,
			setMissedVisits : setMissedVisits,
			setPendingVisits : setPendingVisits,
			setTabVisits : setTabVisits,
			getVisitsForDay : getVisitsForDay,
			getStartedVisitCount : getStartedVisitCount,
			getStartedVisitInfo : getStartedVisitInfo,
			buildPrePostRepairObject : buildPrePostRepairObject,
			addListNumberToConatiners : addListNumberToConatiners,
			populateStartedVisitTab : populateStartedVisitTab,
			getStartedVisitParts : getStartedVisitParts,
			getObj : getObj,
			approvedButRejected : approvedButRejected
		};
		
		return service;

		/**
	     * @name setDatesWithVisit
	     * @desc Populates obj.dateHasVisit variable in the service
		 * with key: auditDate and value true or false
		 * it's used as a model in the calendar UI
	     * @param {Array} auditArray
	     * @returns {Promise} resolve value is an object
	     * @memberOf Factories.$surveyModule
	     */
		function setDatesWithVisit(auditArray) {
			var dfd = $.Deferred();
			
			var datesObject = {};
			
			for (var i = 0; i < auditArray.length; i++) {
				var auditDate = auditArray[i].date;
				
				if (!datesObject[auditDate]) datesObject[auditDate] = true;
			}
			
			obj.dateHasVisit = datesObject;
			dfd.resolve(datesObject);
			
			return dfd.promise();
		}
		
		/**
	     * @name setDisplayVisits
	     * @desc Based on data in calendarVisits collection,
		 * we populate corresponding views in the inspections tab
		 * depending on status passed
		 * eg: upcoming visits tab
	     * @param {String} status
	     * @returns {Promise} 
	     * @memberOf Factories.$surveyModule
	     */
		function setDisplayVisits(status) {
			// get planned visits from json store - sort them and get ready for display
			var dfd = $.Deferred();
			var dates = [];
			var visitsObject = {};
			// object with visit date key and array of visits value
			
			var options = {
				filter: ['date', 'visitId', 'auditStatus', 'shopDescription', 'country', 'locationDescription', 'description']
			};
					
			var query = WL.JSONStore.QueryPart()
    			.equal('auditStatus', status);
			
			WL.JSONStore.get('calendarVisits').advancedFind([query], options).then(function(visits){
				if(status == 'Pending'){
					dates = obj.pastVisitDates;
					visitsObject = obj.pastVisits;
				}
				
				visits.sort(function(a, b) {
					return (b.visitid - a.visitid);
				})
				
				angular.forEach(visits, function(audit){
					if (!visitsObject[audit.date]) {
						visitsObject[audit.date] = [];
						dates.push(audit.date);
						
						visitsObject[audit.date].push({
							visitId : audit.visitid,
							shopDescription : audit.shopdescription,
							description : audit.description,
							location : audit.country + ", " + audit.locationdescription,
							status : audit.auditstatus
						});
					} else {
						var hasVisit = (visitsObject[audit.date].map(function(e){
							return e.visitId;
						}).indexOf(audit.visitid) === - 1) ? false : true;
						
						if (!hasVisit) {
							visitsObject[audit.date].push({
								visitId : audit.visitid,
								shopDescription : audit.shopdescription,
								description : audit.description,
								location : audit.country + ", " + audit.locationdescription,
								status : audit.auditstatus
							});
						}
					}
				});
				
				
				
				
				switch(status) {
					// Missed case
					case 'Planned':
						obj.upcomingVisitDates = dates.sort(function(a, b) {
							return (new Date(b)).getTime() - (new Date(a)).getTime();
						});
						obj.upcomingVisits = visitsObject;
						break;
					case 'Missed':
						obj.missedVisitDates = dates.sort(function(a, b) {
							return (new Date(b)).getTime() - (new Date(a)).getTime();
						});
						obj.missedVisits = visitsObject;
						break;
					case 'Finished':
						obj.pastVisitDates = dates.sort(function(a, b) {
							return (new Date(b)).getTime() - (new Date(a)).getTime();
						});
						obj.pastVisits = visitsObject;
						break;
					case 'Pending':
						obj.pastVisitDates = dates.sort(function(a, b) {
							return (new Date(b)).getTime() - (new Date(a)).getTime();
						});
						obj.pastVisits = visitsObject;
						break;
					default:
						obj.upcomingVisitDates = dates.sort(function(a, b) {
							return (new Date(b)).getTime() - (new Date(a)).getTime();
						});
						obj.upcomingVisits = visitsObject;
				}
				
				dfd.resolve();
			}).fail(function(error){
				dfd.reject(error);
			});
			
			return dfd.promise();
		}
		
		/**
	     * @name refreshVisitData
	     * @desc Runs functions to
		 * 1. finish pending visits if any
		 * 2. make work orders marked as deleted available in the db
		 * 3. set status of the appropriate work orders (plan date has passed)
		 * to "Missed" in the db
		 * 4. get visits associated with user again from server
		 * 5. get calendar data to populate calendar UI from server
		 * 6. populate inspections tab tab views
	     * @param {N/A}
	     * @returns {Promise} 
	     * @memberOf Factories.$surveyModule
	     */
		function refreshVisitData() {
			var defer = $.Deferred();
			
			PendingAuditsService.runVisitQueue().then(function(){
				auditService.updateDeletedWorkOrders().then(function(){
					auditService.updateMissedWorkOrders().then(function(){ 	//may need to update missed visits also locally
						auditService.getVisits().then(function(){
							// get calendar visits and set views.
							auditService.getCalendarData().then(function(visitsArray){
								//this should be called if offline
								setDatesWithVisit(visitsArray).then(function(){
									$.when.apply($, [setUpcomingVisits(), setMissedVisits(), setPastVisits(), setPendingVisits()]).then(function(){
										updateVisitRefreshTimer().then(function(){
											defer.resolve();
										});
									});
								});
								//
							}).fail(function(error){
								WL.Logger.error("[SurveyModule][refreshVisitData][getVisits] username: " + userContext.getUsername() +" Failed to getCalendarData "+ JSON.stringify(error));
								defer.reject(error);
							});
						}).fail(function(error){
							WL.Logger.error("[SurveyModule][refreshVisitData][getVisits] username: " + userContext.getUsername() +" Failed to getVisits "+ JSON.stringify(error));
							defer.reject(error);
						});
					}).fail(function(error){
						WL.Logger.error("[SurveyModule][refreshVisitData][updateMissedWorkOrders] username: " + userContext.getUsername() +" "+ JSON.stringify(error));
						defer.reject(error);
					});
				}).fail(function(error){
					WL.Logger.error("[SurveyModule][refreshVisitData][updateDeletedWorkOrders] username: " + userContext.getUsername() +" "+ JSON.stringify(error));
					defer.reject(error);
				})
			}).fail(function(error){
				WL.Logger.error("[SurveyModule][refreshVisitData][runVisitQueue] username: " + userContext.getUsername() + " " + JSON.stringify(error));
				defer.reject(error);
			});
			
			return defer.promise();
		}
		
		/**
	     * @name refreshVisitData
	     * @desc Runs functions to
		 * 1. set status of the appropriate work orders (plan date has passed)
		 * to "Missed" locally
		 * 2. get calendar data to populate calendar UI from json store
		 * 3. populate inspections tab tab views
	     * @param {N/A}
	     * @returns {Promise} 
	     * @memberOf Factories.$surveyModule
	     */
		function refreshVisitDataOffline() {
			var defer = $.Deferred();
			
			auditService.updateMissedWorkOrdersLocally().then(function(){
				auditService.getOfflineCalendarData().then(function(visitsArray){
					setDatesWithVisit(visitsArray).then(function(){
						$.when.apply($, [setUpcomingVisits(), setMissedVisits(), setPastVisits(), setPendingVisits()]).then(function(){
							defer.resolve();
						});
					});
				}).fail(function(){
					WL.Logger.error("[SurveyModule][refreshVisitDataOffline][getOfflineCalendarData] username: " + userContext.getUsername() + " Failed to get offline calendar data");
					defer.reject();
				});
			}).fail(function(){
				WL.Logger.error("[SurveyModule][refreshVisitDataOffline][updateMissedWorkOrdersLocally] username: " + userContext.getUsername() + " Failed to get updateMissedWorkOrdersLocally");
				defer.reject();
			});
			
			return defer.promise();
		}
		
		function updateVisitRefreshTimer() {
			var dfd = $.Deferred();
			var timerData;
			
			$localDB.clearCollection('visitRefreshTimer').then(function(){
				var currentTime = (new Date()).getTime();
				timerData = [{timestamp : currentTime}];
				$localDB.add('visitRefreshTimer', timerData, {}).then(function(){
					dfd.resolve();
				});
			});
			
			return dfd.promise();
		}
		
		function setUpcomingVisits() {
			return setDisplayVisits('Planned');
		}
		
		function setPastVisits() {
			return setDisplayVisits('Finished');
		}
		
		function setMissedVisits() {
			return setDisplayVisits('Missed');
		}
		
		function setPendingVisits() {
			return setDisplayVisits('Pending');
		}
		
		/**
	     * @name setTabVisits
	     * @desc Deprecated
	     * @memberOf Factories.$surveyModule
	     */
		function setTabVisits() {
			var dfd = $.Deferred();
			
			$.when.apply($, [setUpcomingVisits(), setMissedVisits(), setPastVisits()]).then(function(){
				dfd.resolve();
			}).fail(function(){
				dfd.reject();
			});
			
			return dfd.promise();
		}
		
		/**
	     * @name getVisitsForDay
	     * @desc queries calendarVisits collection to retrieve
		 * visit data to populate calendar tooltip
		 * (for when you tap on a date in the calendar)
	     * @param {String} date
	     * @returns {Promise} 
		 * @resolve {Array} - array of visit info objects
	     * @memberOf Factories.$surveyModule
	     */
		function getVisitsForDay(date) {
			var dfd = $.Deferred();
			var visitArray = [];
			
			$ionicLoading.hide();
			
			var options = {
				filter: ['date', 'visitId', 'auditStatus', 'shopDescription', 'country', 'locationDescription', 'description']
			};
						
			var query = WL.JSONStore.QueryPart()
    			.equal('date', date);
			
			WL.JSONStore.get('calendarVisits').advancedFind([query], options).then(function(visits){
				angular.forEach(visits, function(audit){
					visitArray.push({
						shopDescription : audit.shopdescription,
						locationInfo : audit.country + ", " + audit.locationdescription,
						visitId : audit.visitid,
						auditStatus : audit.auditstatus
					})
				});
				
				dfd.resolve(visitArray);
			});
			
			return dfd.promise();
		}
		
		function getStartedVisitCount() {
			var dfd = $.Deferred();

			// $ionicLoading.show();

			WL.JSONStore.get('visit').find({auditStatus : 'Started'}, {})
				.then(function(startedAuditWOs){
					var noOfAudits = startedAuditWOs.length;
					// $ionicLoading.hide();
					dfd.resolve(noOfAudits);
				});

			return dfd.promise();
		}

		/**
	     * @name getStartedVisitInfo
	     * @desc Gets the data associated with current started visit
		 * used as a model to populate the preview pdf screen
	     * @param {N/A}
	     * @returns {Promise} 
	     * @memberOf Factories.$surveyModule
	     */
		function getStartedVisitInfo() {
			var dfd = $.Deferred();
			
			var options = {
				filter: ['containerNo', 'workOrderId', 'woStatus', 'containerType', 'auditStatus',
				         'repairDate', 'auditDate', 'description', 'inspectionStatus', 'repairCost',
				         'shopCode', 'visitId', 'woType', 'shopDescription', 'location', 'statusCode', 'shopDescription', 'country']
			};

			WL.JSONStore.get('visit').find({auditStatus : 'Started'}, options).then(function(startedAuditWOs){
				obj.startedAudit = startedAuditWOs;
				
				if (obj.startedAudit.length > 0) {
					var auditWorkOrder = obj.startedAudit[0];
					
					obj.startedVisitInfo.visitId = auditWorkOrder.visitid;
					obj.startedVisitInfo.repairShopNo = auditWorkOrder.shopcode;
					obj.startedVisitInfo.repairShopName = auditWorkOrder.shopdescription;
					obj.startedVisitInfo.country = auditWorkOrder.country;
					
					startedAudit.getWorkOrdersWithAuditResults(auditWorkOrder.visitid).then(function(workOrdersObject){
						if (workOrdersObject.allWorkOrders.length === 0) {
							startedAudit.initStartedWorkOrders(auditWorkOrder.visitid).then(function(startedAuditWorkOrders){
								startedAudit.getWorkOrdersWithAuditResults(auditWorkOrder.visitid).then(function(startedWorkOrdersObject){
									var allStartedWorkOrders = startedWorkOrdersObject.allWorkOrders;
									getStartedVisitParts(allStartedWorkOrders).then(function(workOrderParts){
										populateStartedVisitTab(startedWorkOrdersObject, workOrderParts);
										dfd.resolve();
									});
								});
							});
						} else {
							startedAudit.getWorkOrdersWithAuditResults(obj.startedVisitInfo.visitId).then(function(startedWorkOrdersObject){
								var allStartedWorkOrders = startedWorkOrdersObject.allWorkOrders;
								getStartedVisitParts(allStartedWorkOrders).then(function(workOrderParts){
									populateStartedVisitTab(startedWorkOrdersObject, workOrderParts);
									dfd.resolve();
								});
							});
						}
					});
				} else {
					obj.startedAudit = [];
					obj.startedVisitInfo = {};
					obj.partsObject = {};
					dfd.resolve();
				}
			});
			
			return dfd.promise();
		}
		
		/**
	     * @name buildPrePostRepairObject
	     * @desc parses params and filters data
		 * in order to divide work orders in more specific statuses
		 * and populate preview pdf UI
	     * @param {Array} workOrders
		 * @param {Array} workOrderParts
	     * @returns {Object} includes info related to 
	     * @memberOf Factories.$surveyModule
	     */
		function buildPrePostRepairObject(workOrders, workOrderParts){
			var obj = {
				preRepairWO : [],
				postRepairWO : [],
				acceptedPre : [],
				acceptedPost : [],
				failedPre : [],
				failedPost : [],
				approvedButRejected : []
			}
			
			for(var i = 0; i < workOrders.passedWorkOrders.length; i++){
				if (workOrders.passedWorkOrders[i].statuscode <= 390){
					obj.acceptedPre.push(workOrders.passedWorkOrders[i]);
				} else {
					obj.acceptedPost.push(workOrders.passedWorkOrders[i]);
				}
			}
			
            for(var i = 0; i < workOrders.failedWorkOrders.length; i++){
            	var workOrderInfo = workOrders.failedWorkOrders[i];
				if (workOrderInfo.statuscode <= 390) {
					if (approvedButRejected(workOrderInfo, workOrderParts)) {
						obj.approvedButRejected.push(workOrderInfo);
					} else {
						obj.failedPre.push(workOrderInfo);
					}
				} else {
					obj.failedPost.push(workOrders.failedWorkOrders[i]);
				}
			}
			
			for(var i = 0; i < workOrders.allWorkOrders.length; i++){
				if(workOrders.allWorkOrders[i].statuscode <= 390){
					obj.preRepairWO.push(workOrders.allWorkOrders[i]);
				}else{
					obj.postRepairWO.push(workOrders.allWorkOrders[i]);
				}
			}

			return obj;
		}
		
		function addListNumberToConatiners(startedVisitInfoObj){
			var index = 1;
			index = addIndexToArray(index,'preApprovedWo');
			index = addIndexToArray(index,'preRejectedWo');
			index = addIndexToArray(index,'postApprovedWo');
			index = addIndexToArray(index,'postRejectedWo');
			index = addIndexToArray(index,'plannedWorkOrders');
		}

		function addIndexToArray(index, propertyName){
			angular.forEach(obj.startedVisitInfo[propertyName],function(item){
				item.idx = index;
				index++;
			});
			
			return index;
		}
		
		/**
	     * @name populateStartedVisitTab
	     * @desc parses params and filters data
		 * in order to divide work orders in more specific statuses
		 * and populate preview pdf UI
	     * @param {Object} workOrdersObject
		 * @param {Array} workOrderParts
	     * @returns {N/A}
	     * @memberOf Factories.$surveyModule
	     */
		function populateStartedVisitTab(workOrdersObject, workOrderParts) {
			obj.startedVisitInfo.totalNo = workOrdersObject.allWorkOrders.length;
			obj.startedVisitInfo.inProgressNo = workOrdersObject.startedWorkOrders.length;
			obj.startedVisitInfo.finishedNo = workOrdersObject.finishedWorkOrders.length;
			obj.startedVisitInfo.notStartedNo = workOrdersObject.plannedWorkOrders.length;
			obj.startedVisitInfo.notPassedNo = workOrdersObject.failedWorkOrders.length;
			obj.startedVisitInfo.passedNo = workOrdersObject.passedWorkOrders.length;
			
			obj.startedVisitInfo.plannedWorkOrders = workOrdersObject.plannedWorkOrders;
			obj.startedVisitInfo.startedWorkOrders = workOrdersObject.startedWorkOrders;
			obj.startedVisitInfo.passedWorkOrders = workOrdersObject.passedWorkOrders;
			obj.startedVisitInfo.failedWorkOrders = workOrdersObject.failedWorkOrders;
			
			var prePostObj = buildPrePostRepairObject(workOrdersObject, workOrderParts);
			obj.startedVisitInfo.preRepairWorkOrders = prePostObj.preRepairWO;
        	obj.startedVisitInfo.preApprovedWo = prePostObj.acceptedPre;
        	obj.startedVisitInfo.preRejectedWo = prePostObj.failedPre;
        	obj.startedVisitInfo.postRepairWorkOrders = prePostObj.postRepairWO;
        	obj.startedVisitInfo.postApprovedWo = prePostObj.acceptedPost;
        	obj.startedVisitInfo.postRejectedWo = prePostObj.failedPost;
        	obj.startedVisitInfo.approvedButRejected = prePostObj.approvedButRejected;
			obj.startedVisitInfo.preRepairWorkOrdersNo = obj.startedVisitInfo.preApprovedWo.length + obj.startedVisitInfo.preRejectedWo.length;;
			obj.startedVisitInfo.postRepairWorkOrdersNo = obj.startedVisitInfo.postApprovedWo.length + obj.startedVisitInfo.postRejectedWo.length;
			obj.startedVisitInfo.preApprovedWoNo = obj.startedVisitInfo.preApprovedWo.length;
			obj.startedVisitInfo.postApprovedWoNo = obj.startedVisitInfo.postApprovedWo.length;
			obj.startedVisitInfo.preFailedWoNo = obj.startedVisitInfo.preRejectedWo.length;
			obj.startedVisitInfo.postFailedWoNo = obj.startedVisitInfo.postRejectedWo.length;
			
			addListNumberToConatiners(obj.startedVisitInfo);
			
			obj.partsObject = {};
			
			
			angular.forEach(workOrderParts, function(part){
				if (!obj.partsObject[part.workOrderNo]) {
					obj.partsObject[part.workOrderNo] = [];
					obj.partsObject[part.workOrderNo].push(part);
				} else {
					obj.partsObject[part.workOrderNo].push(part);
				}
			});
		}
		
		/**
	     * @name getStartedVisitParts
	     * @desc gets all repairs and parts associated with
		 * the work orders passed as parameter
	     * @param {Array} workOrderArray
	     * @returns {Promise} 
		 * @resolve {Array} - array of repairs/parts
	     * @memberOf Factories.$surveyModule
	     */
		function getStartedVisitParts(workOrderArray) {
			var dfd = $.Deferred();
			var startedVisitParts = [];
			var promiseArray = [];
			
			angular.forEach(workOrderArray, function(workOrder){
				(function(workOrder){
					switch(workOrder.inspectionstatus) {
						case 'Planned':
							promiseArray.push(getWorkOrderPartsAndTestSignal(workOrder.workorderid).then(function(parts){
								if (parts === null) {
									return;
								}
								angular.forEach(parts, function(part){
									part.workOrderNo = workOrder.workorderid;
									startedVisitParts.push(part);
								})
							}));
						break;
						case 'Started':
							if ($localStorage[workOrder.workorderid]) {
								angular.forEach($localStorage[workOrder.workorderid], function(part){
									var copiedPart = angular.copy(part);
									copiedPart.workOrderNo = workOrder.workorderid;
									startedVisitParts.push(copiedPart);
								})
							} else {
								promiseArray.push(getWorkOrderPartsAndTestSignal(workOrder.workorderid).then(function(parts){
									if (parts === null) {
										return;
									}
									angular.forEach(parts, function(part){
										part.workOrderNo = workOrder.workorderid;
										startedVisitParts.push(part);
									})
								}));
							}
						break;
						case 'Finished':
							// get from json store
							promiseArray.push(startedAudit.getAuditPartsForWorkOrder(workOrder.workorderid, workOrder.visitid).then(function(parts){
								 angular.forEach(parts.repairLines, function(repair){
									 repair.workOrderNo = workOrder.workorderid;
									 startedVisitParts.push({
										 quantity: repair.postQuantity,
										 repairDescription: repair.repairDescription,
										 damageCode: repair.postDamageCode,
										 partCode: repair.postPartCode,
										 repairCode: repair.postRepairCode,
										 repairLocCode: repair.postRepairLocCode,
										 workOrderNo: repair.workOrderId,
										 repairIndex: repair.repairIndex,
										 // new helper values for pdf generation
										 preQuantity: repair.preQuantity,
										 preDamageCode: repair.preDamageCode,
										 preRepairCode: repair.preRepairCode,
										 preRepairLocCode: repair.preRepairLocCode,
										 preTPI: repair.preTPI,
										 TPI: repair.postTPI,
										 rejectedRepair: repair.rejectedPart,
										 borderClass: (repair.rejectedPart) ? "" : "pdfBottomBorder", 
										 notRequired: (repair.postQuantity == 0) ? "pdfBottomBorder" : "",
										 $childParts: [],
										 $ratingRQ  : repair.repairQuality,
										 $addedPart	: repair.addedPart
									 });
								 })
								 
								 angular.forEach(parts.partLines, function(part){
									 for (var i = 0; i < startedVisitParts.length; i++) {
										 var repairLine = startedVisitParts[i];
										 
										 if (repairLine.workOrderNo == part.workOrderId) {
											 if (repairLine.repairIndex == part.repairIndex) {
												 var parentIndex = startedVisitParts.indexOf(repairLine);
												 part.borderClass = (part.rejectedPart) ? "" : "pdfBottomBorder";
												 part.notRequired = (part.postQuantity == 0) ? "pdfBottomBorder" : "";
												 startedVisitParts[parentIndex].$childParts.push(part);
											 }
										 }
									 }
								 });
							 }));
						break;
					}
				})(workOrder);
			});
			
			$.when.apply($, promiseArray).then(function(){
				setTimeout(function(){
					dfd.resolve(startedVisitParts); // timeout to get the work order parts for the last work order
				});
			}).fail(function(error){
				dfd.reject(error);
			});
			
			return dfd.promise();
		}
		
		function getWorkOrderPartsAndTestSignal(workOrderId) {
			var dfd = $.Deferred();
			
			auditService.testSignal()
			.then(function() {
				auditService.getWorkOrderParts(workOrderId)
				.then(function(parts) {
					dfd.resolve(parts);
				})
				.fail(function(err) {
					dfd.resolve(null);
				});
			}).fail(function(err) {
				dfd.resolve(null);
			});
			
			return dfd.promise();
		}
		
		
		function getObj() {
			return obj;
		}
		
		/**
	     * @name approvedButRejected
	     * @desc checks that the status of a work order is 
		 * approved but rejected depending on
		 * associated repairs audit results
	     * @param {Object} workOrderInfo
		 * @param {Array} workOrderParts
	     * @returns {Bool} 
	     * @memberOf Factories.$surveyModule
	     */
		function approvedButRejected(workOrderInfo, workOrderParts) {
			var isApprovedButRejected;
			
			var repairsForWorkOrder = [];
			
			for (var i = 0; i < workOrderParts.length; i++) {
				if (workOrderParts[i].workOrderNo == workOrderInfo.workorderid) {
					repairsForWorkOrder.push(workOrderParts[i]);
				}
	 		}
			
			for (var i = 0; i < repairsForWorkOrder.length; i++) {
				if (isAggregateRejected(repairsForWorkOrder[i])) {
					isApprovedButRejected = false;
					break;
				}
				
				if (i == (repairsForWorkOrder.length - 1)) {
					isApprovedButRejected = true;
				}
			}
			
			return isApprovedButRejected;
		}
		
		function isAggregateRejected(repair) {
			if (repair.rejectedRepair) return true;
			for (var i = 0; i < repair.$childParts.length; i++) {
				if (repair.$childParts[i].rejectedPart) {
					return true;
				}
			}
			return false;
		}
	}
})();	
	
	
