/**
 * Created by andrewradulescu on 5/8/17.
 * OCR hybrid integration for recognizing Container IDs and add WO to a visit
 */
(function () {
    audit.factory('ocrService', ocrService);

    ocrService.$inject = ['$rootScope', 'multiModal', '$q'];

    function ocrService($rootScope, multiModal, $q) {


        var public = { //public exposed interface of the OCR service
            openOCRMainModal: openOCRMainModal,
            parseOCRData: parseOCRData,
            nextInput: nextInput,
            onTextClick: onTextClick,
            validateOcrText: validateOcrText,
            getLatestWORepairsByID: getLatestWORepairsByID,
            buildPhotoPath: buildPhotoPath,
            removeFileByName: removeFileByName,
            getVisitDetails: getVisitDetails,
            setVisitDetails: setVisitDetails,
            clearVisitDetails: clearVisitDetails

        };

        //Used for AddVisitCtrl to pass Date and VisitID before instantiate the controller
        var visitDetails = {
            date: null,
            visitId: null
        };

        //Init container validator to check OCR result and user input using Maersk algorithm
        var validator = null;

        //Expose the public interface returning the public object
        return public;


        /**
         * @name setVisitDetails
         * @desc Setter for visitDetails 
         * @param {String} date Date of visit
         * @param {Integer} visitId The ID of the visit
         * @returns {void}
         * @memberOf Factories.ocrService
        */
        function setVisitDetails(date, visitId) {
            if (date && visitId) {
                visitDetails.date = date;
                visitDetails.visitId = visitId;
            }
        };

        /**
         * @name getVisitDetails
         * @desc Getter for visitDetails 
         * @returns {Object}
         * @memberOf Factories.ocrService
        */
        function getVisitDetails() {
            return visitDetails;
        };

        /**
         * @name clearVisitDetails
         * @desc Clear visitDetails object by setting its properties with null
         * @returns {void}
         * @memberOf Factories.ocrService
        */
        function clearVisitDetails() {
            visitDetails.date = null;
            visitDetails.visitId = null;
        };

         /**
         * @name removeFileByName
         * @desc Remove a file from device TEMPORARY storage (OCR unused images)
         * @param {String} fileName Name of file to be removed
         * @returns {void}
         * @memberOf Factories.ocrService
        */
        function removeFileByName(fileName) {

            if (!fileName) {
                console.error("No fileName specified. File could not be deleted.");
                return false;
            }

            window.requestFileSystem(LocalFileSystem.TEMPORARY, 0, function (fileSystem) { // this returns the tmp folder

                // File found
                fileSystem.root.getFile(fileName, { create: false }, function (fileEntry) {
                    fileEntry.remove(function (success) {
                        console.log("Image " + fileName + " with success!");
                    }, function (error) {
                        console.error("deletion failed: " + error);
                    });
                }, function (err) {
                    console.log("Failed to  fileSystem.root.getFile");
                });
            }, function (err) {
                console.log("Failed to requestFileSystem");
            });

        };

        /**
         * @name initializeValidator
         * @desc Initialize Container ID Validator one time when use OCR
         * @returns {void}
         * @memberOf Factories.ocrService
        */
        function initializeValidator() {
            validator = new ContainerValidator();
        };

         /**
         * @name initializeValidator
         * @desc Generate a unique path to store photos in Temporary storage
         * @returns {String}
         * @memberOf Factories.ocrService
        */
        function buildPhotoPath() {
            var dir = cordova.file.tempDirectory;
            var path = dir + guid() + ".jpg";
            return path;
        }

        /**
         * @name openOCRMainModal
         * @desc Open main modal of OCR used as a container of different partials to navigate though OCR steps
         * @param {Object} scope Context of modal where to be displayed
         * @returns {void}
         * @memberOf Factories.ocrService
        */
        function openOCRMainModal(scope) {
            var $containerIdEdit = multiModal.setBasicOptions('confirm', 'commentModal', null, 'full'),
                $containerIdEdit = multiModal.setTemplateFile($containerIdEdit, 'templates/OCR/mainModalOCR.html');
            multiModal.openModal($containerIdEdit, scope);
        };

        
        /**
         * @name parseOCRData
         * @desc Prepare OCR data to be displayed
         * @param {String} containerID String received from OCR engine as an identified Container Id
         * @param {String} path Path received from OCR engine to the photo taken with OCR camera
         * @returns {Object}
         * @memberOf Factories.ocrService
        */
        function parseOCRData(containerID, path) {
            var defer = $q.defer();
            var textArray = containerID.split("");
            
            initializeValidator(); //initialize validator to be used in containerID edit

            var data = {
                letters: textArray.splice(0, 4),
                numbers: textArray,
                modalTitle: "Verify OCR Container No.",
                imageName: path
            };

            //Convert OCR image to Base64
            convertFileToBase64(path).then(function (imageBase64) {
                //Attach imageBase64 to OCR Data to be displayed in html
                data.imageBase64 = imageBase64;
                defer.resolve(data);
            }, function (err) {
                //Resolve promise even if photo couldn't be converted
                defer.resolve(data);
            });

            return defer.promise;
        };

        /**
         * @name nextInput
         * @desc Moves the focus from one input to another
         * @param {Object} e Event received from user interaction
         * @param {Boolean} isValid Validity of the form 
         * @returns {void}
         * @memberOf Factories.ocrService
        */
        function nextInput(e, isValid) {

            var nextEl = null;

            if (e.srcElement.id.includes('letter') && !e.srcElement.parentElement.parentElement.nextElementSibling) {
                /* Last letter in line */
                nextEl = document.getElementById('0number');

            } else if (e.srcElement.id.includes('number') && !e.srcElement.parentElement.parentElement.nextElementSibling) {
                /* Last number in line */
                if (isValid) {
                    e.srcElement.blur();
                }
                return;

            } else {
                nextEl = e.srcElement.parentElement.parentElement.nextElementSibling.getElementsByClassName('inputWrapper')[0].getElementsByTagName('input')[0];
            }

            if (e.srcElement.value.length != 0) {
                nextEl.focus();
                selectAll(nextEl.id);
            }
        };

         /**
         * @name onTextClick
         * @desc HELPER FUNCTION: Selects all text from an input
         * @param {Object} $event Event received from user interaction
         * @param {String} id Unique identified of the selected input
         * @returns {void}
         * @memberOf Factories.ocrService
        */
        function onTextClick($event, id) {
            $event.target.select();
            selectAll(id);
        };

        /**
         * @name selectAll
         * @desc HELPER FUNCTION: Selects all text from an input
         * @param {String} id Unique identified of the selected input
         * @returns {void}
         * @memberOf Factories.ocrService
        */
        function selectAll(id) {
            document.getElementById(id).selectionStart = 0
            document.getElementById(id).selectionEnd = 10
        }

         /**
         * @name validateOcrText
         * @desc Validate the ContainerId using Maersk Validator algorithm
         * @param {String} containerID A string representing a container id
         * @returns {Object} 
         * @memberOf Factories.ocrService
        */
        function validateOcrText(containerID) {
            var ocrDataString = containerID;

            validator.validate(ocrDataString);
            var err = validator.getErrorMessages();

            var result = {
                errorMessage: "",
                checkDigitError: false,
                ocrTextInvalid: false
            };

            if (err[0]) {
                result.errorMessage = err[0];
                if (err[0].localeCompare('The check digit does not match') == 0) {
                    result.checkDigitError = true;
                } else if (err[0].localeCompare('The container number is invalid') == 0) {
                    result.ocrTextInvalid = true;
                }
            } else {
                console.log('Valid Container Id');
                result.errorMessage = "";
                result.checkDigitError = false;
                result.ocrTextInvalid = false;
            }

            return result;

        }

        /**
         * @name getLatestWORepairsByID
         * @desc Get the latest WO repairs and parts for a specified ContainerID in a Shop
         * @param {String} containerID A string representing the container id
         * @param {String} shopID A string representing the shop id
         * @returns {Promise} 
         * @memberOf Factories.ocrService
        */
        function getLatestWORepairsByID(containerID, shopID) {
            var defer = $.Deferred();
            WL.Client.invokeProcedure({
                adapter: 'ContainerHistoryAdapter',
                procedure: 'getContainerRepairsPartsByContainerId',
                parameters: [userContext.getUsername(), userContext.getToken(), containerID, shopID]
            }, {
                    onSuccess: function (response) {
                        successHandler(response, defer)
                    },
                    onFailure: function (error) {
                        errorHandler(error, defer);
                    }
                });
            return defer.promise();
        }

        //HELPER FUNCTION: Handle promise success response 
        function successHandler(response, defer) {
            if (!response.invocationResult.isSuccessful) {
                defer.reject(response.invocationResult);
            } else {
                var resultSet = response.invocationResult.invocationResult;
                if (resultSet.length > 0) {
                    defer.resolve(resultSet);
                } else {
                    defer.reject();
                }

            }
        };

        //HELPER FUNCTION: Handle promise error response 
        function errorHandler(error, defer) {
            $ionicLoading.hide();
            defer.reject(error);
        }

        //HELPER FUNCTION: Generate an unique ID 
        function guid() {
            function s4() {
                return Math.floor((1 + Math.random()) * 0x10000)
                    .toString(16)
                    .substring(1);
            }

            return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
                s4() + '-' + s4() + s4() + s4();
        };

        //HELPER FUNCTION: Convert an image from TEMP files to base64 using its name
        function convertFileToBase64(imageName) {
            var defer = $q.defer();
            var dir = cordova.file.tempDirectory;

            var path = dir + imageName;

            window.resolveLocalFileSystemURL(path, function (entry) {
                entry.file(function (file) {
                    var FR = new FileReader();
                    FR.onloadend = function (image) {
                        defer.resolve(image.target.result);
                    }
                    FR.readAsDataURL(file);
                });
            }, function (err) {
                defer.reject(err);
            });

            return defer.promise;
        };


    };

})();