/**
 * Nov 12, 2015
 * planVisitModule.js
 * BuzilaEugen
 */

/**
 * $planVisit Factory
 * @namespace Factories
 */
(function(){
	'use strict';
	
	audit
    .factory('$planVisit', $planVisit);
	
	$planVisit.$inject = ['$localDB', '$commonOperations', '$ionicLoading', '$filter', 'auditService'];
	
	/**
     * @namespace $planVisit
     * @desc Service that checks database job successful completion for a visit and provides user feedback
     * @memberOf Factories
     */
	function $planVisit($localDB, $commonOperations, $ionicLoading, $filter, auditService) {

		var obj = {
			repairShops : [],
			countries : [],
			containersForRepairShop : [],
			country : {},
			woStatusList : {},
			woModeList : {},
			gateInValuesList : [{label : "0-1 week", from : 0, to : 7}, {label : "1-2 weeks", from : 7, to: 14}, {label : "2-3 weeks", from : 14, to: 21}, {label : "3-4 weeks", from : 21, to: 28}, {label : "4+ weeks", from : 29 }],
			shopCode : null
		};

		var service = {
			getWOStatusList : getWOStatusList,
			getWOModeList : getWOModeList,
			getLocalWOStatusList : getLocalWOStatusList,
			getLocalWOModeList : getLocalWOModeList,
			getCountries : getCountries,
			getShopCode : getShopCode,
			setShopCode : setShopCode,
			getRepairShops : getRepairShops,
			getRepairShopsForCountry : getRepairShopsForCountry,
			getRememberCountry : getRememberCountry,
			clearRememberCountry : clearRememberCountry,
			saveRememberCountry : saveRememberCountry,
			setWorkOrdersForRepairShop : setWorkOrdersForRepairShop,
			getObj : getObj,
			getCacheCollectionsStatus : getCacheCollectionsStatus,
			updateCacheCollectionsStatus : updateCacheCollectionsStatus,
			getCollectionForRepairShop : getCollectionForRepairShop,
			getCachedCollection : getCachedCollection,
			getWorkOrdersFromCache : getWorkOrdersFromCache,
			findOldestCachedShop : findOldestCachedShop,
			cacheRepairShop : cacheRepairShop
		};
		
		return service;

		/**
	     * @name getWOStatusList
	     * @desc Populates the obj.woStatusList property in the service
		 * with the appropriate work order statuses
	     * @param {N/A}
	     * @returns {Promise} - value resolved is an array of work order statuses
	     * @memberOf Factories.$planVisit
	     */
		function getWOStatusList() {
			var dfd = $.Deferred();
			
			auditService.getWOStatusList().then(function(result){
				obj.woStatusList = result;
				dfd.resolve(result);
			}).fail(function(e){
				dfd.reject(e);
			})
			return dfd.promise();
		}
		
		function getLocalWOModeList(){
			var dfd = $.Deferred();
			
			$localDB.find('woMode', {}, {}).then(function(result){
				
				
				var parsedResults = result.map(function(mode){
					if (mode.json) return mode.json;
				});
				
				obj.woModeList = {
						isSuccessful : true,
						resultSet : parsedResults
				}
				
				dfd.resolve(parsedResults);
			});
			
			dfd.promise();
		}

		/**
	     * @name getWOModeList
	     * @desc Populates the obj.woModeList property in the service
		 * with the appropriate work order modes
	     * @param {N/A}
	     * @returns {Promise} - value resolved is an array of work order modes
	     * @memberOf Factories.$planVisit
	     */
		function getWOModeList() {
			var dfd = $.Deferred();
			
			auditService.getWOModeList().then(function(result){
				obj.woModeList = result;
				dfd.resolve(result);
			}).fail(function(e){
				dfd.reject(e);
			})
			return dfd.promise();
		}

		/**
	     * @name getLocalWOStatusList
	     * @desc Populates the obj.woStatusList property in the service
		 * with data from json store
	     * @param {N/A}
	     * @returns {Promise} - value resolved is an array of work order statuses
	     * @memberOf Factories.$planVisit
	     */
		function getLocalWOStatusList() {
			var dfd = $.Deferred();
			
			$localDB.find('woStatus', {}, {}).then(function(result){
				var parsedResults = result.map(function(status){
					if (status.json) return status.json;
				});
				
				obj.woStatusList = {
					isSuccessful : true,
					resultSet : parsedResults
				}
				
				dfd.resolve(parsedResults);
			});
			
			dfd.promise();
		}

		/**
	     * @name getCountries
	     * @desc Populates the obj.countries property in the service
		 * with data from json store
	     * @param {N/A}
	     * @returns {Promise} - value resolved is an array of countries
	     * @memberOf Factories.$planVisit
	     */
		function getCountries(){
			var dfd = $.Deferred();
			var countryArray = [];
			
			WL.JSONStore.get('countries').advancedFind([], {}).then(function(data){
				for (var index in data) { 
					var country = data[index].json;
					if (country != undefined) { 
						countryArray.push({
							code : country.code,
							name : country.name,
						});
					}
				} 	
				obj.countries = countryArray;
				dfd.resolve(countryArray);
			}).fail(function(err){
				 dfd.reject("Error on getting all countries");
			})
			return dfd.promise();
		}

		function setShopCode(shopCode){
			obj.shopCode = shopCode;
		}

		function getShopCode(){
			return obj.shopCode;
		}

		/**
	     * @name getRepairShops
	     * @desc Populates the obj.allRepairShops and obj.repairShops property in the service
		 * with repair shop data from json store
	     * @param {N/A}
	     * @returns {Promise} - value resolved is an array of repair shops
	     * @memberOf Factories.$planVisit
	     */
		function getRepairShops(){
			var dfd = $.Deferred();
			
			var repairShops = [];
			var dfd = $.Deferred();
			
			WL.JSONStore.get('repairShop').advancedFind([], {}).then(function(data){
				for (var index in data) { 
					var repairShop = data[index].json;
					if (repairShop != undefined) { 
						repairShops.push({
							code : repairShop.code,
							name : repairShop.name,
							location : repairShop.location,
							country	: (repairShop.country) ? repairShop.country.toLowerCase() : null,
							cached : (repairShop.cached) ? 	repairShop.cached : false
						});
					}
				} 		
				obj.allRepairShops = repairShops;
				obj.repairShops = repairShops;
				dfd.resolve(repairShops);
			}).fail(function(err){
				 dfd.reject("Error on getting all repair shops");
			})
			return dfd.promise();
		}

		function getRepairShopsForCountry(countryCode){
			var dfd = $.Deferred();
			
			var repairShops = [];
			var dfd = $.Deferred();
			$localDB.find('repairShop', {country : countryCode}, {exact : true}).then(function(data) {
				for (var index in data) { 
					var repairShop = data[index].json;
					if (repairShop != undefined) { 
						repairShops.push({
							code : repairShop.code,
							name : repairShop.name,
							location : repairShop.location,
							country	: (repairShop.country) ? repairShop.country.toLowerCase() : null,
							cached : repairShop.cached
						});
					}
				} 					 
				obj.repairShops = repairShops;
				dfd.resolve(repairShops);
			}).fail(function(err){
				 dfd.reject("Error on getting all repair shops");
			})
			return dfd.promise();
		}

		/**
	     * @name getRememberCountry
	     * @desc Populates the obj.country property in the service
		 * which acts as a model for 
		 * remember country functionality
	     * @param {N/A}
	     * @returns {Promise}
	     * @memberOf Factories.$planVisit
	     */
		function getRememberCountry(){
			obj.country = {};
			var dfd = $.Deferred();
			$localDB.find('country', {}, {}).then(function(data){
				if (data.length > 0) {
					if (data[0]) {
						obj.country = {
							rememberCountry : data[0].json.rememberCountry,
							code : data[0].json.code,
							rememberme : true
						}
					}
				} else {
					obj.country = {
						rememberCountry	: 'All Countries',
						code : null,
						rememberme : false
					}
				}
				dfd.resolve();
			}).fail(function(err){
				console.log('err', err);
				dfd.reject("error getting selected country");
			})
			return dfd.promise();
		}

		function clearRememberCountry() {
			var dfd = $.Deferred();
			
			$localDB.clearCollection('country').then(function(){
				obj.country.rememberme = false;
				dfd.resolve();
			});
			
			return dfd.promise();
		}

		function saveRememberCountry(countryObject) {
			var dfd = $.Deferred();
			
			clearRememberCountry().then(function(){
				$localDB.add('country', countryObject, {}).then(function(){
					dfd.resolve();
				}).fail(function(){
					dfd.reject();
				});
			});
			
			return dfd.promise();
		}

		function setWorkOrdersForRepairShop(repairWorkOrders){
			var dfd = $.Deferred();
			
			obj.workOrdersForRepairShop = repairWorkOrders;
			
			dfd.resolve(obj.workOrdersForRepairShop);
			
			return dfd.promise();
		}

		function getObj(){
			return obj;
		}

		/**
	     * @name getCacheCollectionsStatus
	     * @desc gets 
	     * @param {N/A}
	     * @returns {Promise} - value resolved is the first collection name 
		 * used for caching such as cachedWorkOrders1
	     * @memberOf Factories.$planVisit
	     */
		function getCacheCollectionsStatus() {
			var dfd = $.Deferred();
			
			$localDB.find('cacheCollectionsStatus', {}, {}).then(function(cacheInfo){
				if (cacheInfo.length > 0) {
					var storeNext = cacheInfo[0].json.storeNext;
					dfd.resolve(storeNext);
				} else { 
					// init collection
					$localDB.add('cacheCollectionsStatus', {storeNext : 'cachedWorkOrders1'}, {}).then(function(){
						dfd.resolve('cachedWorkOrders1');
					});
				}
			});
			return dfd.promise();
		}

		/**
	     * @name updateCacheCollectionsStatus
	     * @desc Updates the cacheCollectionsStatus collection
		 * to be able to tell which caching collection
		 * to be used next for storing work orders associated with the repair shop
	     * @param {String} previousStatus 
	     * @returns {Promise} - value resolved is the updated collection number
	     * @memberOf Factories.$planVisit
	     */
		function updateCacheCollectionsStatus(previousStatus) {
			var dfd = $.Deferred();
			var maxCachedShops = 5; // need to add extra collections, for performance reasons to increase this limit!
			var newStatus;
			
			if (previousStatus != null) {
				var oldIndex = previousStatus.slice(-1);
				var newIndex = ++oldIndex;
				newStatus = (newIndex > maxCachedShops) ? null : 'cachedWorkOrders' + newIndex;
				
				$localDB.clearCollection('cacheCollectionsStatus').then(function(){
					$localDB.add('cacheCollectionsStatus', {storeNext : newStatus}, {}).then(function(){
						dfd.resolve(newStatus);
					});
				});
			} else {
				dfd.resolve(previousStatus); // null
			}
			return dfd.promise();
		}

		/**
	     * @name getCollectionForRepairShop
	     * @desc Updates the cacheCollectionsStatus collection
		 * to be able to tell which caching collection
		 * to be used next for storing work orders associated with the repair shop
	     * @param {String} shopCode 
	     * @returns {Promise} - value resolved is the name
		 * of the json store collection
		 * where the work orders associated with the shop are saved
	     * @memberOf Factories.$planVisit
	     */
		function getCollectionForRepairShop(shopCode) {
			var dfd = $.Deferred();
			
			$localDB.find('repairShop', {cached : true, code : shopCode}, {exact : true}).then(function(shopsData){
				if (shopsData.length > 0 && shopsData[0].json.storedInCollection) {
					var collectionName = shopsData[0].json.storedInCollection;
					dfd.resolve(collectionName);
				} else {
					dfd.reject();
				}
			});
			
			return dfd.promise();
		}

		function getCachedCollection(collectionName) {
			var dfd = $.Deferred();
			
			WL.JSONStore.get(collectionName).advancedFind([], {}).then(function(results){
				results = results.map(function(jsonWorkOrder){
					return jsonWorkOrder.json;
				})
				
    			dfd.resolve(results);
			});
			
			return dfd.promise();
		}

		/**
	     * @name getWorkOrdersFromCache
	     * @desc Gets cached work orders for shop code
	     * @param {String} shopCode 
	     * @returns {Promise} - value resolved is an array of cached work orders
		 * associated with the shop 
	     * @memberOf Factories.$planVisit
	     */
		function getWorkOrdersFromCache(shopCode) {
			var dfd = $.Deferred();
			
			$localDB.find('repairShop', {cached : true, code : shopCode}, {exact : true}).then(function(shopsData){
				if (shopsData.length > 0 && shopsData[0].json.storedInCollection) {
					var collectionName = shopsData[0].json.storedInCollection;
					
					var options = {
					    limit: 8000 
					}; 

					WL.JSONStore.get(collectionName).findAll(options).then(function (workOrders) {
						if (workOrders.length > 0) {
							var parsedWorkOrders = workOrders.map(function(jsonWorkOrder){
								return jsonWorkOrder.json;
							})
							dfd.resolve(parsedWorkOrders);
						} else {
							dfd.reject();
						}
					})
				} else {
					dfd.reject(); // nothing cached
				}
			});
			return dfd.promise();
		}

		/**
	     * @name findOldestCachedShop
	     * @desc Gets the collection name corresponding with
		 * the repairshop with the oldest caching timestamp
	     * @param {N/A} 
	     * @returns {Promise} 
	     * @memberOf Factories.$planVisit
	     */
		function findOldestCachedShop() {
			var dfd = $.Deferred();
			
			$localDB.find('repairShop', {cached : true}, {exact : true}).then(function(shopsData){
				if (shopsData.length > 0) {
					// sort array ascending
					
					var sortedShops = shopsData.sort(function(a, b){
						return a.json.timestamp - b.json.timestamp;
					})
					
					var repairShopInfo = sortedShops[0];
					var shopCode = sortedShops[0].json.code;
					var timestamp = sortedShops[0].json.timestamp;
					var collectionName = sortedShops[0].json.storedInCollection;
					
					// need to update removed shop info
					delete repairShopInfo.json.cached;
					delete repairShopInfo.json.timestamp;
					delete repairShopInfo.json.storedInCollection;
					
					$localDB.replace('repairShop', repairShopInfo, {}).then(function(){
						dfd.resolve(collectionName);
					});
				} else {
					dfd.reject();
				}
			});
			
			return dfd.promise();
		}

		/**
	     * @name cacheRepairShop
	     * @desc Called when a user taps the Cache results button 
		 * in the add visit screen
		 * the logic is based on the fact that there are 5 possible collections 
		 * to store work orders in cachedWorkOrders1 (2,3,4,5)
		 * the collection name to use for where to store is in the cacheCollectionsStatus collection
		 * once all the collections are populated, the logic is
		 * to erase the one with the oldest timestamp and reuse it
		 * entries in repairShop collection have the cached, timestamp and storedInCollection properties
		 * to tell if and where they are cached
	     * @param {String} shopCode 
		 * @param {Array} workOrders - Array of work orders to be cached
	     * @returns {Promise} 
	     * @memberOf Factories.$planVisit
	     */
		function cacheRepairShop(shopCode, workOrders){
			var dfd = $.Deferred();
			
			$localDB.find('repairShop',{code : shopCode},{exact : true}).then(function(shopInfo){
				// shop work orders are not cached
				if (!shopInfo[0].json.cached && workOrders.length != 0) {
					// see in what collection we need to cache work orders
					getCacheCollectionsStatus().then(function(collectionName){
						// update cache timestamp, collection name and status
						
						shopInfo[0].json.cached = true;
						shopInfo[0].json.timestamp = new Date().getTime();
						shopInfo[0].json.storedInCollection = collectionName;
						
						if (collectionName == null) { // no collections are empty
							findOldestCachedShop().then(function(collectionToReplace){
								shopInfo[0].json.storedInCollection = collectionToReplace;
								$localDB.replace('repairShop', shopInfo[0], {}).then(function(){
									$localDB.clearCollection(collectionToReplace).then(function(){
										$localDB.add(collectionToReplace, workOrders,{}).then(function(){
											dfd.resolve();
										})
									});
								});
							});
						} else {
							$localDB.replace('repairShop', shopInfo[0], {}).then(function(){
								// add work orders to cache collection
								$localDB.add(collectionName, workOrders,{}).then(function(){
									// update cacheCollectionsStatus - increment
									updateCacheCollectionsStatus(collectionName).then(function(){
										dfd.resolve();
									});
								})
							});
						}
					});
				} else if (shopInfo[0].json.cached && workOrders.length != 0) { // shop work orders are already cached
					shopInfo[0].json.timestamp = new Date().getTime();
					var collectionName = shopInfo[0].json.storedInCollection; 
					
					$localDB.replace('repairShop', shopInfo[0], {}).then(function(){
						// clear collection
						$localDB.clearCollection(collectionName).then(function(){
							$localDB.add(collectionName, workOrders,{}).then(function(){
								dfd.resolve();
							})
						});
					});
				} else {
					// something is not right
					dfd.reject();
				}
			});
			
			return dfd.promise();
		}
	}
})();