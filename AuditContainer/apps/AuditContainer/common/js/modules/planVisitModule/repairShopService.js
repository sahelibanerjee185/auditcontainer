
/* JavaScript content from js/modules/planVisitModule/repairShopService.js in folder common */
/**
 * Mar, 31 2016
 * repairShopService.js
 * Stavarache Vlad
 */

/**
 * repairShopService Factory
 * @namespace Factories
 */
(function(){
	'use strict';

	audit
    .factory('repairShopService', repairShopService);

	repairShopService.$inject = ['$localDB', 'multiModal', '$rootScope', '$location', 'responseHandler', '$ionicLoading', '$backend'];

	/**
     * @namespace repairShopService
     * @desc Service that retrieves country, repair shop and work order information from the DB
     * @memberOf Factories
     */
	function repairShopService($localDB, multiModal, $rootScope, $location, responseHandler, $ionicLoading, $backend) {

		var currentShop = null;

		var service = {
			getAvailableCountries: getAvailableCountries,
			getWorkOrdersByShopId: getWorkOrdersByShopId,
			getAllRepairShops: getAllRepairShops,
			getCurrentShop: getCurrentShop,
			getShopEmail: getShopEmail,
			sendEmailToShop: sendEmailToShop,
			getAvailableRepairShopsForCertificate : getAvailableRepairShopsForCertificate,
			getAvailableCountriesForCertificate : getAvailableCountriesForCertificate
		};

		return service;

		/**
	     * @name getAllRepairShops
	     * @desc Gets associated repair shops from server and saves them
		 * in the repairShop collection
	     * @param {Bool} forCertificate - either get shops associated with certificate if true
		 * or get all shops from db is false
	     * @returns {Promise}
	     * @memberOf Factories.repairShopService
	     */
		function getAllRepairShops(forCertificate) {
			var defer = $.Deferred();

			var getShopsProcedure = forCertificate ? "getShopsForCertificate" : "getAllRepairShops";

			WL.Client.invokeProcedure({
				adapter : 'RepairShopAdapter',
				procedure : getShopsProcedure,
				parameters : [userContext.getUsername(), userContext.getToken(), userContext.getCertificateId()]
			}, {
				onSuccess : function(response){
					if (!response.invocationResult.isSuccessful) {
						defer.reject(response.invocationResult);
					} else {
						var results = response.invocationResult.invocationResult;
						var cachedShopsObject = {};
						var checkForCachedShops = $.Deferred();
						var parsedResults = [];

						WL.JSONStore.get('repairShop').find({cached : 1}, {}).then(function(cachedShops){
							angular.forEach(cachedShops, function(jsonShop){
								var shopCode = jsonShop.json.code;
								cachedShopsObject[shopCode] = jsonShop.json;
							})
							checkForCachedShops.resolve(cachedShopsObject);
						});

						checkForCachedShops.promise().then(function(cachedShopsObject){
							if (angular.equals({}, cachedShopsObject)) {
								parsedResults = results;
							} else {
								parsedResults = results.map(function(shop){
									if (typeof cachedShopsObject[shop.code] === 'object') {
										shop.cached = true;
										shop.timestamp = cachedShopsObject[shop.code].timestamp;
										shop.storedInCollection = cachedShopsObject[shop.code].storedInCollection;
									}
									return shop;
								});
							}

							$localDB.clearCollection('repairShop').then(function(){
								$localDB.add('repairShop', parsedResults, {}).then(function(){
									defer.resolve();
								});
							});
						});
					}
				},
				onFailure: function(error){
					errorHandler(error, defer);
				}
			});
			return defer.promise();
		}

		function getAvailableRepairShopsForCertificate() {
			return getAllRepairShops(true);
		}

		/**
	     * @name getShopEmail
	     * @desc Gets the contact email addresses associated with a shop
	     * @param {String} shopCode
	     * @returns {Promise} - value resolved is an array of emails
	     * @memberOf Factories.repairShopService
	     */
		function getShopEmail(shopCode) {
			var defer = $.Deferred();

			WL.Client.invokeProcedure({
				adapter : 'RepairShopAdapter',
				procedure : 'getShopEmail',
				parameters : [userContext.getUsername(), userContext.getToken(), shopCode]
			}, {
				onSuccess : function(response){
					if (response && response.invocationResult  && response.invocationResult.isSuccessful) {
						var emailList = response.invocationResult.invocationResult.email;
						var shopEmail = [];

						// Emails may be separated by ";" "," "//"

						var helperEmailArray;

						if (emailList.indexOf(";") != -1) {
							helperEmailArray = emailList.split(";");
							shopEmail = helperEmailArray;
						} else if (emailList.indexOf(",") != -1) {
							helperEmailArray = emailList.split(",");
							shopEmail = helperEmailArray;
						} else if (emailList.indexOf("//") != -1) {
							helperEmailArray = emailList.split("//");
							shopEmail = helperEmailArray;
						} else {
							shopEmail.push(emailList);
						}

						defer.resolve(shopEmail);
					} else {
						defer.reject(response.invocationResult);
					}
				},
				onFailure: function(error){
					errorHandler(error, defer);
				}
			});

			return defer.promise();
		}

		/**
	     * @name sendEmailToShop
	     * @desc DEPRECATED
	     * @memberOf Factories.repairShopService
	     */
		function sendEmailToShop(email, from, date, shopCode, containers, type) {

			return;

			var defer = $.Deferred();
			var subject = "";

			if (type.planned) {
				subject = "Visit Planned";
			} else if (type.rescheduled) {
				subject = "Visit Rescheduled";
			} else if (type.added) {
				subject = "Containers added to Visit"
			} else {
				subject = "Visit Planned";
			}

			// type - planned, rescheduled, added

			var message = ""

			if (type.planned) {
				message += "Visit scheduled for " + date + ".";
			} else if (type.added) {
				message += "Visit scheduled for " + date + " information updated.";
			} else if (type.rescheduled) {
				message += "Visit rescheduled for " + date + ".";
			}

			message += "\n";

			if (type.planned || type.rescheduled) {
				message += "\nContainers to inspect:";
			} else if (type.added) {
				message += "\nAdded containers to inspect:";
			}

			message += "\n";

			for (var i = 0; i < containers.length; i++) {
				message += "\n" + containers[i].containerNo;
			}

			$backend.sendMail(userContext.getEmail(), email, subject, message, JSON.stringify([{fileName:null,data:null}])).then(function(message){
				defer.resolve();
			}).fail(function(e){
				errorHandler(error, defer);
			})

 			return defer.promise();
		}

		/**
	     * @name getAvailableCountries
	     * @desc Gets associated countries from server and saves them
		 * in the countries collection
	     * @param {Bool} forCertificate - either get countries associated with certificate if true
		 * or get all countries from db is false
	     * @returns {Promise}
	     * @memberOf Factories.repairShopService
	     */
		function getAvailableCountries(forCertificate) {
			var defer = $.Deferred();

			var getCountriesProcedure = forCertificate ? "getAvailableCountriesForCertificate" : "getAvailableCountries";

			WL.Client.invokeProcedure({
				adapter : 'RepairShopAdapter',
				procedure : getCountriesProcedure,
				parameters : [userContext.getUsername(), userContext.getToken(), userContext.getCertificateId()]
			}, {
				onSuccess : function(response){
					if (!response.invocationResult.isSuccessful) {
						defer.reject(response.invocationResult);
					} else {
						var results = response.invocationResult.invocationResult;

						// might move localDB operations to a separate function- refactor TODO
						$localDB.clearCollection('countries').then(function(){
							$localDB.add('countries', results, {}).then(function(){
								defer.resolve();
							});
						});
					}
				},
				onFailure: function(error){
					errorHandler(error, defer);
				}
			});
			return defer.promise();
		}

		function getAvailableCountriesForCertificate() {
			return getAvailableCountries(true);
		}

		/**
	     * @name getWorkOrdersByShopId
	     * @desc Gets work orders associated with a shop within the past year
		 * with a 40000 limit count
	     * @param {String} shopCode
	     * @returns {Promise} - value resolved is of type object: 
		 * {results : parsedResponse, countExceeded : countExceeded, startingDate : startingDate}
	     * @memberOf Factories.repairShopService
	     */
		function getWorkOrdersByShopId(shopCode) {
			var dateHelper = new Date();
			var fromDate = dateHelper.setFullYear(dateHelper.getFullYear() - 1);

			var defer = $.Deferred();
			WL.Client.invokeProcedure({
				adapter : 'RepairShopAdapter',
				procedure : 'getWorkOrdersByShopIdImproved',
				parameters : [userContext.getUsername(), userContext.getToken(), shopCode, fromDate]
			}, {
				onSuccess : function(response){
					if (!response.invocationResult.isSuccessful) {
						defer.reject(response.invocationResult);
					} else {
						currentShop = shopCode;
						var workOrders = response.invocationResult.invocationResult.results;
						var countExceeded = response.invocationResult.invocationResult.countExceeded;
						var startingDate = response.invocationResult.invocationResult.startingDate;

						var todayTimestamp = new Date().getTime();
						var dayDuration = 24*60*60*1000;

						var parsedResponse = workOrders.map(function(workOrder){
							workOrder.displayRepairCost = parseFloat(workOrder.repairCost).toFixed(2);
							workOrder.reapirDateTimeStamp = (new Date(workOrder.repairDate)).getTime();
							workOrder.gateInDateTimeStamp = (new Date(workOrder.gateInDate)).getTime();
							workOrder.daysSinceGateIn = Math.round(Math.abs((todayTimestamp - workOrder.gateInDateTimeStamp)/(dayDuration)))
							return workOrder;
						});

						defer.resolve({results : parsedResponse, countExceeded : countExceeded, startingDate : startingDate});
					}
				},
				onFailure: function(error){
					WL.Logger.error("[RepairShopService][getWorkOrdersByShopId] username: " + userContext.getUsername() + JSON.stringify(error));
					errorHandlerWithFeedback(error, defer);
				},
				timeout: 30000
			});
			return defer.promise();
		}

		function getCurrentShop() {
			return currentShop;
		}

		function successHandler(response, defer,type) {
			if (!response.invocationResult.isSuccessful) {
				defer.reject(response.invocationResult);
			} else {
				if(type) // check with eugen if mistake or intentional
					response.invocationResult.invocationResult.dataResult = response.responseJSON.invocationResult;
				defer.resolve(response.invocationResult.invocationResult);
			}
		}

		function errorHandler(error, defer) {
			console.log('error', error);
			defer.reject(error);
		}

		function errorHandlerWithFeedback(error, defer) {
			console.log('error', error);
			$ionicLoading.hide();
			defer.reject(error);
			responseHandler.feedback(error);
		}
	}
})();
