/**
 * Apr 20, 2016
 * postLoginInit.js
 * StavaracheVlad
*/

/**
 * postLoginInit Factory
 * @namespace Factories
 */
(function(){
	'use strict';
	
	audit
    .factory('postLoginInit', postLoginInit);
	
	postLoginInit.$inject = ['multiModal', '$localDB', '$rootScope', '$location', '$ionicLoading', 'repairShopService', '$planVisit', '$surveyModule', 'auditService', '$kpiModule', 'WOCodesService', '$manualsModule', 'PendingAuditsService'];
	
	/**
     * @namespace postLoginInit
     * @desc called after certificate is successfully validated 
	 * it retrieves the associated user data from the server 
	 * and saves it locally
     * @memberOf Factories
     */
	function postLoginInit(multiModal, $localDB, $rootScope, $location, $ionicLoading, repairShopService, $planVisit, $surveyModule, auditService, $kpiModule, WOCodesService, $manualsModule, PendingAuditsService) {
		
		var currentShop = null;
		var staticDataGetInterval = 2629746000; // 1 month
		// staticDataGetInterval = 360000; // 6 min
	
		var service = {
			init : init,
			initOffline : initOffline,
			sync : sync,
			failedFeedback : failedFeedback,
			setRepairShops : setRepairShops,
			setCountries : setCountries,
			loadPlanVisitData : loadPlanVisitData
		};
		
		return service;
		
		/**
	     * @name init
	     * @desc Gets countries and shops data from the server and saves it locally
		 * if data is already saved refreshes it after staticDataGetInterval
		 * Retrieves user associated visit data
		 * Retrieves user KPIs
		 * Retrieves manuals from server
		 * Retrieves some dropdown list values from db
	     * @param {N/A}
	     * @returns {Promise}
	     * @memberOf Factories.postLoginInit
	     */
		function init() {
			var defer = $.Deferred();
			var startTime = new Date().getTime();
			
			// if certificate has changed, clear country and shops collections!
			
			var clearCountriesAndShops = $.Deferred();
			
			if (userContext.changedCertificate()) {
				removeCountriesAndShops().then(function(){
					clearCountriesAndShops.resolve();
				}).fail(function(error){
					defer.reject(error);
				});
			} else {
				clearCountriesAndShops.resolve();
			}
			
			clearCountriesAndShops.promise().then(function(){
				// Check if there is something in repair shop, country and codes collections
				checkForLocalData().then(function(){
					PendingAuditsService.runVisitQueue().then(function(){
						var requestLoader = getVisitsLoaderConfig;
						var staticDataWasRefreshed = false;
						
						if (userContext.codeDescriptionsAreLoaded()) {
							if (userContext.codeDescriptionsDataNeedsRefresh()) {
								WOCodesService.initCodes().then(function(){
									updateLastStaticDataRefreshTime("codeDescriptionsRefresh");
								});
							}
						} else {
							WOCodesService.initCodes().then(function(){
								updateLastStaticDataRefreshTime("codeDescriptionsRefresh");
							});
						}
						
						var requestArray = [$surveyModule.refreshVisitData(), $planVisit.getRememberCountry(), $planVisit.getWOStatusList()];
						
						// Get dropdown values
						
						 requestArray.push($planVisit.getWOModeList(), WOCodesService.getManualCodes(), WOCodesService.getPartsGroupCodes(), WOCodesService.getManufacturerCodes());
						
						if (userContext.planVisitIsLoaded()) { // we have data stored locally!
							if (userContext.staticDataNeedsRefresh()) { // static data is pretty old!
								requestArray.push(setRepairShops(), setCountries());
								staticDataWasRefreshed = true;
								requestLoader = initAppLoaderConfig;
							} else {
								requestArray.push($planVisit.getRepairShops(), $planVisit.getCountries());
							}
						} else {
							// We don't have any data, need to get it from the server
							requestArray.push(setRepairShops(), setCountries());
							staticDataWasRefreshed = true;
							requestLoader = initAppLoaderConfig;
						}
					
						requestArray.push($kpiModule.getKpiFromServer());
						requestArray.push($manualsModule.getManuals());
						
						$ionicLoading.hide(); 
						$ionicLoading.show(requestLoader);
						$.when.apply($, requestArray).then(function(){
							var endTime = new Date().getTime();
							var duration = endTime - startTime;
							console.log("init duration", duration);
							if (staticDataWasRefreshed) {
								updateLastStaticDataRefreshTime("staticDataRefresh");
							}
							defer.resolve();
						}).fail(function(error){
							defer.reject(error);
						});
					}).fail(function(error){
						defer.reject(error);
					})
				});
			}).fail(function(error){
				defer.reject(error);
			});
			
			return defer.promise();
		}
		
		/**
	     * @name init
	     * @desc Can only be called if the user has successfully logged in online at least once
		 * Retrieves the data from json store and populates corresponding services
	     * @param {N/A}
	     * @returns {Promise}
	     * @memberOf Factories.postLoginInit
	     */
		function initOffline() {
			var defer = $.Deferred();
			
			checkForLocalData().then(function(){
				var requestLoader = getVisitsLoaderConfig;
				
				var requestArray = [$surveyModule.refreshVisitDataOffline(), $planVisit.getRememberCountry(), $planVisit.getLocalWOStatusList(), $planVisit.getLocalWOModeList()];
				
				if (userContext.planVisitIsLoaded()) { // we have data stored locally!
					
					requestArray.push($planVisit.getRepairShops(), $planVisit.getCountries(), $manualsModule.getManualsOffline());
					
					// Get dropdown values
					
					requestArray.push(WOCodesService.getManualCodesOffline(), WOCodesService.getPartsGroupCodesOffline(), WOCodesService.getManufacturerCodesOffline())
					
				} else {
					//open modal or could do a reject and on fail rise modal with no data sored locally
					var $noDataStoredModal = multiModal.setBasicOptions('notification', 'noLocalData', 'No data stored locally, please connect to a network','popup');
					$noDataStoredModal = multiModal.setActions($noDataStoredModal, function(){});
					multiModal.openModal($noDataStoredModal, $rootScope);
				}
				
				$ionicLoading.hide();
				$ionicLoading.show(requestLoader);
				$.when.apply($, requestArray).then(function(){
					$ionicLoading.hide();
					defer.resolve();
				}).fail(function(error){
					defer.reject();
				});
			});
			
			return defer.promise();
		}
		
		function checkLastStaticDataRefreshTime(collectionName) {
			// staticDataRefresh
			var defer = $.Deferred();
			
			$localDB.find(collectionName, {}, {}).then(function(data) {
				var timestamp;
				if (data.length > 0) {
					timestamp = data[0].json.timestamp;
				} else {
					timestamp = null;
				}
				
				var now = new Date().getTime();
				
				if (timestamp) {
					if ((now - timestamp) > staticDataGetInterval) {
						switch(collectionName) {
						case "staticDataRefresh":
							userContext.setStaticDataNeedsRefresh();
							break;
						case "codeDescriptionsRefresh":
							userContext.setCodeDescriptionsDataNeedsRefresh();
							break;
						default:
							break;
						}
					}
				}
				
				defer.resolve();
			});
			
			return defer.promise();
		}
		
		function updateLastStaticDataRefreshTime(collectionName) {
			var defer = $.Deferred();
			
			$localDB.clearCollection(collectionName).then(function(){
				var newTimeStamp = new Date().getTime();
				var timestampObject = {timestamp : newTimeStamp};
				
				$localDB.add(collectionName, timestampObject, {}).then(function(){
					defer.resolve();
				});
			});
			
			return defer.promise();
		}
		
		/**
	     * @name sync
	     * @desc Function that is called when user taps the sync button in the native header
		 * Uses PendingAuditsService to finish pending visits if any
		 * Retrieves visita data, countries and shops data, KPIs and manuals from the server 
		 * and updates the local collections
	     * @param {N/A}
	     * @returns {Promise}
	     * @memberOf Factories.postLoginInit
	     */
		function sync() {
			var defer = $.Deferred();
			
			PendingAuditsService.runVisitQueue().then(function(someVisitsSynced){
				var visitsSynced = someVisitsSynced;
                $ionicLoading.show(loaderConfig);
				$.when.apply($, [$surveyModule.refreshVisitData(), $planVisit.getRememberCountry(), setRepairShops(), setCountries(), $kpiModule.getKpiFromServer(), $manualsModule.getManuals()]).then(function(){
					$ionicLoading.hide();
					defer.resolve();
				}).fail(function(error){
					WL.Logger.error("[PostLoginInit][sync] username: " + userContext.getUsername() + " " + JSON.stringify(error));
					defer.reject(error, visitsSynced);
				});
			}).fail(function(error, someVisitsSynced){
				WL.Logger.error("[PostLoginInit][sync] username: " + userContext.getUsername() + " " + JSON.stringify(error));
				defer.reject(error, someVisitsSynced);
			});
			
			return defer.promise();
		}
		
		function checkForLocalData() {
			var defer = $.Deferred();
			
			$.when.apply($, [checkForPlanVisitData(), checkForLocalCodes(), checkForKpiData(), checkLastStaticDataRefreshTime("staticDataRefresh"), checkLastStaticDataRefreshTime("codeDescriptionsRefresh")]).then(function(){
				defer.resolve();
			})
			
			return defer.promise();
		}
		
		function checkForLocalCodes() {
			var defer = $.Deferred();
			
			WL.JSONStore.get('repairCodes').count().then(function(repairCodesCount) {
			    if (repairCodesCount == 0) {
			    	userContext.codeDescriptionsNotLoaded();
			    	defer.resolve();
			    } else {
					userContext.codeDescriptionsLoaded();
					defer.resolve();
			    }
			});
			
			return defer.promise();
		}
		
		
		function checkForKpiData() {
			var defer = $.Deferred();
			
			WL.JSONStore.get('kpi').count().then(function(kpiCount) {
				if (kpiCount == 0) {
					userContext.kpiNotLoaded();
					defer.resolve();
				} else {
					userContext.kpiLoaded();
					defer.resolve();
				}
			});
			
			return defer.promise();
		}
		
		function checkForPlanVisitData() {
			var defer = $.Deferred();
			
			WL.JSONStore.get('countries').count().then(function(countriesCount) {
			    if (countriesCount == 0) {
			    	userContext.planVisitNotLoaded();
			    	defer.resolve();
			    } else {
			    	WL.JSONStore.get('repairShop').count().then(function(shopCount) {
			    		if (shopCount == 0) {
			    			userContext.planVisitNotLoaded();
			    			defer.resolve();
			    		} else {
			    			userContext.planVisitLoaded();
			    			defer.resolve();
			    		}
			    	});
			    }
			});
			
			return defer.promise();
		}
		
		function loadPlanVisitData() {
			var defer = $.Deferred();
			if (userContext.planVisitIsLoaded()) {
				defer.resolve();
			} else {
				$ionicLoading.show(planVisitLoaderConfig);
				$.when.apply($, [setRepairShops(), setCountries()]).then(function(){
					userContext.planVisitLoaded();
					$ionicLoading.hide();
					defer.resolve();
				}).fail(function(){
					userContext.planVisitNotLoaded();
					$ionicLoading.hide();
					defer.resolve();
				});
			}
			
			return defer.promise();
		}
		
		function setRepairShops() { 
			var defer = $.Deferred();

			repairShopService.getAvailableRepairShopsForCertificate().then(function(response){
				$planVisit.getRepairShops().then(function() { 
					defer.resolve();
				}).fail(function(){
					defer.reject();
				});
			}).fail(function(){
				defer.reject();
			});
			
			return defer.promise();
		}
		
		function setCountries() {
			var defer = $.Deferred();
			
			repairShopService.getAvailableCountriesForCertificate().then(function(){
				$planVisit.getCountries().then(function(){
					defer.resolve();
				}).fail(function(){
					defer.reject();
				});
			}).fail(function(){
				defer.reject();
			});
			
			return defer.promise();
		}
		
		function removeCountriesAndShops() {
			var defer = $.Deferred();
			
			$localDB.clearCollection("countries").then(function(){
				$localDB.clearCollection("repairShop").then(function(){
					defer.resolve();
				}).fail(function(error){
					WL.Logger.error("[PostLoginInit][removeCountriesAndShops] username: " + userContext.getUsername() + " " + JSON.stringify(error));
					defer.reject(error);
				});
			}).fail(function(error){
				WL.Logger.error("[PostLoginInit][removeCountriesAndShops] username: " + userContext.getUsername() + " " + JSON.stringify(error));
				defer.reject(error);
			});
			
			return defer.promise();
		}
		
		function failedFeedback() {
			$ionicLoading.hide();
			var $loginFailed = multiModal.setBasicOptions('notification', 'loginFailed', 'Something went wrong. Please try again later.');
			$loginFailed = multiModal.setActions($loginFailed, function(){
				// navigator.app.exitApp();
			})
			multiModal.openModal($loginFailed, $rootScope);
		}
		
	}
})();
