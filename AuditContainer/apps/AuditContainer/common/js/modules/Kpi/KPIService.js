/**
 * May, 19 2016
 * KPIService.js
 * Eugen Buzila
 */

(function(){
	'use strict';
	
	audit
    .factory('KPIService', KPIService);
	
	KPIService.$inject = ['multiModal','$rootScope','$localDB', 'responseHandler', '$ionicLoading','$filter'];
	
	function KPIService(multiModal,$rootScope,$localDB, responseHandler, $ionicLoading,$filter) {
		
		var containerHistoryRepair = {};
		var containerHistoryArray = [];
	
		var service = {
			getPassedInspections	: getPassedInspections,
			getRejectedInspectionsForMonth : getRejectedInspectionsForMonth,
			getAllInspectionsForMonth:getAllInspectionsForMonth,
			getPassedInspectionsForMonth:getPassedInspectionsForMonth,
			getAllShops:getAllShops,
			getFailedData:getFailedData,
			getAllFailledWOForShop:getAllFailledWOForShop,
			getAllPartsForWO:getAllPartsForWO,
			getInspectionResultsForPastYear : getInspectionResultsForPastYear
		};
		
		return service;
		
		function getInspectionResultsForPastYear() {
			var defer = $.Deferred();
			
			// Set today date and format
			var today = new Date();
			today.setDate(today.getDate() + 1);
			today = $filter('date')(today, 'yyyy-MM-dd');
			
			WL.Client.invokeProcedure({
				adapter : 'KPIAdapter',
				procedure : 'getInspectionResultsForPastYear',
				parameters : [userContext.getUsername(), userContext.getToken(), today]
			}, {
				onSuccess : function(response){ 
					successHandler(response, defer)
				},
				onFailure: function(error){
					errorHandler(error, defer);
				}
			});		 
			
			return defer.promise();
		}
		
		function getPassedInspections() {
			
			var defer = $.Deferred();
			//current date
			var data = new Date();
			data.setDate(data.getDate() + 1);
			var today = $filter('date')(data,'yyyy-MM-dd');
			
			
			// get one year ago date
			var lastYear = new Date();
			lastYear.setMonth(lastYear.getMonth()-11);
			lastYear.setDate(1);
			var oneYearAgo = $filter('date')(lastYear,'yyyy-MM-dd');
			
			
			WL.Client.invokeProcedure({
				adapter : 'KPIAdapter',
				procedure : 'getPassedInspections',
				parameters : [userContext.getUsername(), userContext.getToken(),today,oneYearAgo]
			}, {
				onSuccess : function(response){ 
					successHandler(response, defer)
				},
				onFailure: function(error){
					errorHandler(error, defer);
				}
			});		  
			return defer.promise();
		}
		
		function getAllShops() {
			
			var defer = $.Deferred();
			//current date
			var data = new Date();
			data.setDate(data.getDate()+1);
			var today = $filter('date')(data,'yyyy-MM-dd');
			
			// get one year ago date
			var lastYear = new Date();
			lastYear.setMonth(lastYear.getMonth()-11);
			lastYear.setDate(1);
			var oneYearAgo = $filter('date')(lastYear,'yyyy-MM-dd');
			
			
			WL.Client.invokeProcedure({
				adapter : 'KPIAdapter',
				procedure : 'getAllShops',
				parameters : [userContext.getUsername(), userContext.getToken(),today,oneYearAgo]
			}, {
				onSuccess : function(response){ 
					successHandler(response, defer)
				},
				onFailure: function(error){
					errorHandler(error, defer);
				}
			});		  
			return defer.promise();
		}
		
		function getRejectedInspectionsForMonth() {
			
			var defer = $.Deferred();
			//current date
			var data = new Date();
			data.setDate(data.getDate());
			var today = $filter('date')(data,'yyyy-MM-dd');
			
			var actualDate = new Date(today);
			var month = actualDate.getMonth()+1;
			var actualYear = actualDate.getFullYear();
			
			WL.Client.invokeProcedure({
				adapter : 'KPIAdapter',
				procedure : 'getRejectedInspectionsForMonth',
				parameters : [userContext.getUsername(), userContext.getToken(),month,actualYear]
			}, {
				onSuccess : function(response){ 
					successHandler(response, defer)
				},
				onFailure: function(error){
					errorHandler(error, defer);
				}
			});		  
			return defer.promise();
		}
		
		
		
		function getPassedInspectionsForMonth() {
			
			var defer = $.Deferred();
			//current date
			var data = new Date();
			data.setDate(data.getDate());
			var today = $filter('date')(data,'yyyy-MM-dd');
			
			var actualDate = new Date(today);
			var month = actualDate.getMonth()+1;
			var actualYear = actualDate.getFullYear();
			
			WL.Client.invokeProcedure({
				adapter : 'KPIAdapter',
				procedure : 'getPassedInspectionsForMonth',
				parameters : [userContext.getUsername(), userContext.getToken(),month,actualYear]
			}, {
				onSuccess : function(response){ 
					successHandler(response, defer)
				},
				onFailure: function(error){
					errorHandler(error, defer);
				}
			});		  
			return defer.promise();
		}
		
		function getAllInspectionsForMonth() {
			
			var defer = $.Deferred();
			//current date
			var data = new Date();
			data.setDate(data.getDate());
			var today = $filter('date')(data,'yyyy-MM-dd');
			
			var actualDate = new Date(today);
			var month = actualDate.getMonth()+1;
			var actualYear = actualDate.getFullYear();
			
			WL.Client.invokeProcedure({
				adapter : 'KPIAdapter',
				procedure : 'getAllInspectionsForMonth',
				parameters : [userContext.getUsername(), userContext.getToken(),month,actualYear]
			}, {
				onSuccess : function(response){
					successHandler(response, defer)
				},
				onFailure: function(error){
					errorHandler(error, defer);
				}
			});		  
			return defer.promise();
		}
		
		
		/*
		 * Get KPI data for failed WO
		 */
		function getFailedData(shopCode) {
			
			var defer = $.Deferred();
			//current date
			var data = new Date();
			data.setDate(data.getDate());
			var today = $filter('date')(data,'yyyy-MM-dd');
			data.setDate(data.getDate()+1);
			var tomorow = $filter('date')(data,'yyyy-MM-dd');
			var actualDate = new Date(today);
			var month = actualDate.getMonth()+1;
			var actualYear = actualDate.getFullYear();
			
			// get one year ago date
			var lastYear = new Date();
			lastYear.setMonth(lastYear.getMonth()-11);
			lastYear.setDate(1);
			var oneYearAgo = $filter('date')(lastYear,'yyyy-MM-dd');
			
			WL.Client.invokeProcedure({
				adapter : 'KPIAdapter',
				procedure : 'getFailedData',
				parameters : [userContext.getUsername(), userContext.getToken(),month,actualYear,today,tomorow,oneYearAgo,shopCode]
			}, {
				onSuccess : function(response){ 
					successHandler(response, defer)
				},
				onFailure: function(error){
					errorHandler(error, defer);
				}
			});		  
			return defer.promise();
		}
		
		
		/*
		 *  Get all WO that are rejected for a specific shop
		 */
		function getAllFailledWOForShop(shop) {
			
			var defer = $.Deferred();
			//current date
			var data = new Date();
			data.setDate(data.getDate()+1);
			var today = $filter('date')(data,'yyyy-MM-dd');
			
			// get one year ago date
			var lastYear = new Date();
			lastYear.setMonth(lastYear.getMonth()-11);
			lastYear.setDate(1);
			var oneYearAgo = $filter('date')(lastYear,'yyyy-MM-dd');
			
			
			WL.Client.invokeProcedure({
				adapter : 'KPIAdapter',
				procedure : 'getAllFailledWOForShop',
				parameters : [userContext.getUsername(), userContext.getToken(),today,oneYearAgo,shop]
			}, {
				onSuccess : function(response){ 
					successHandler(response, defer)
				},
				onFailure: function(error){
					errorHandler(error, defer);
					
				}
			});		  
			return defer.promise();
		}
		
		/*
		 * Get all parts for specific WO
		 */
		function getAllPartsForWO(woNo,date) {
			var defer = $.Deferred();
			
			WL.Client.invokeProcedure({
				adapter : 'KPIAdapter',
				procedure : 'getAllPartsForWO',
				parameters : [ userContext.getUsername(), userContext.getToken(),woNo,date]
			}, {
				onSuccess : function(response){ 
					successHandler(response, defer)
				},
				onFailure: function(error){
					errorHandler(error, defer);
				}
			});		  
			return defer.promise();
		}
		
		
		

		function successHandler(response, defer) {
			if (!response.invocationResult.isSuccessful) {
				defer.reject(response.invocationResult);
			} else {
				var resultSet = response.invocationResult.invocationResult;
				defer.resolve(resultSet);
			}
		}
		
		function errorHandler(error, defer) {
			console.log('error', error);
			// $ionicLoading.hide();
			defer.reject(error);
		} 
		
		function errorHandlerWithFeedback(error, defer) {
			console.log('error', error);
			$ionicLoading.hide();
			defer.reject(error);
			responseHandler.feedback(error);
		} 
	}
})();