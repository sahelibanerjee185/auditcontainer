audit.factory('$kpiModule', function($localDB, $filter, $commonOperations,KPIService){
	
	var obj = {
		kpiDepots	:	[],
		kpiMonthCount : [],
		passedInspections : [],
		rejectedInspections : [],
		allInspections : [],
		totalCount : 0,
		passedCount : 0,
		rejectedCount : 0,
		count : {},
		shops : [],
		shopCode : null,
		rejectedWO : []
	};
	
	var failedObj= {};
	
	  function initKpi(){
		  var dfd = $.Deferred();
		  
		  WL.JSONStore.get('kpi').advancedFind([], {}).then(function(data){
				if (data.length > 0) {
					obj.totalCount = data[0].json.totalCount;
					obj.passedCount = data[0].json.passedCount;
					obj.rejectedCount = data[0].json.rejectedCount;
					WL.JSONStore.get('kpiShops').advancedFind([], {}).then(function(data){
						obj.shops = data;
						WL.JSONStore.get('kpiMonthCount').advancedFind([], {}).then(function(kpiData){
							var parsedKpiCount = kpiData.map(function(kpiItem){
								if (kpiItem.json) return kpiItem.json;
							});
							obj.kpiMonthCount = parsedKpiCount;
							dfd.resolve();
						});
					});
				} 
			})
		  return dfd.promise();
	  }
	  
	  function populateMainGraph() {
		  var countForMonths = obj.kpiMonthCount;
		  
		  var currentDate = new Date();
		  var currentMonth = currentDate.getMonth() + 1;
		  var currentYear = currentDate.getFullYear();
		  
		  // var oneYearAgoDate = new Date(new Date().setMonth(currentDate.getMonth() - 11));
		  var oneYearAgoYear;
		  var oneYearAgoMonth;
		  if (currentMonth == 12) {
			  oneYearAgoMonth = 1;
			  oneYearAgoYear = currentYear;
		  } else {
			  oneYearAgoMonth = currentMonth + 1;
			  oneYearAgoYear = currentYear - 1;
		  }
		  
		  // Need to find better way of solving this
		  
		  obj.passedInspections = _getPassedYearResults(oneYearAgoMonth, oneYearAgoYear);
		  obj.rejectedInspections = _getRejectedYearResults(oneYearAgoMonth, oneYearAgoYear);
		  obj.allInspections = _getAllYearResults(oneYearAgoMonth, oneYearAgoYear);
	  }
	  
	  function _getCountForMonth(month, year, result) {
		  var countResult = 0;
		  
		  for (var i = 0; i < obj.kpiMonthCount.length; i++) {
			  var monthInfo = obj.kpiMonthCount[i];
			  
			  if (result === null) {
				  // We need to count regardless of result
				  if (monthInfo.month == month && monthInfo.year == year) {
					  // We'll get total count this way
					  countResult += monthInfo.inspectionCount;
				  }
			  } else {
				  if (monthInfo.month == month && monthInfo.year == year && monthInfo.result == result) {
					  // We have a match
					  return monthInfo.inspectionCount;
				  }
			  }
		  }
		  return countResult;
	  }
	  
	  function _getYearResultsForResult(result, oneYearAgoMonth, oneYearAgoYear) {
		  var yearResults = [];
		  var monthsInYear = 12;
		  
		  for (var i = 0; i < monthsInYear; i++) { // from 0 to 11
			  var month = oneYearAgoMonth + i;
			  var year = oneYearAgoYear;
			  var matchFound = false;
			  
			  if (month > monthsInYear) {
				  month = month - monthsInYear;
				  year = oneYearAgoYear + 1;
			  } else {
				  month = oneYearAgoMonth + i;
			  }
			  
			  var count = _getCountForMonth(month, year, result);
			  
			  yearResults.push(count);
		  }
		  
		  return yearResults;
	  }
	  
	  function _getRejectedYearResults(oneYearAgoMonth, oneYearAgoYear) {
		  return _getYearResultsForResult(0, oneYearAgoMonth, oneYearAgoYear);
	  }
	  
	  function _getPassedYearResults(oneYearAgoMonth, oneYearAgoYear) {
		  return _getYearResultsForResult(1, oneYearAgoMonth, oneYearAgoYear);
	  }
	  
	  function _getAllYearResults(oneYearAgoMonth, oneYearAgoYear) {
		  return _getYearResultsForResult(null, oneYearAgoMonth, oneYearAgoYear);
	  }
	  
	  function resetCount(){
			obj.totalCount = 0;
			obj.passedCount = 0;
			obj.rejectedCount = 0;
	  }
	  
	return { 
		
		initKpi : initKpi,
		
		populateMainGraph : populateMainGraph,
		
		getKpiFromServer : function(){
			
			var kpiObj = {
				totalCount : 0,
				passedCount : 0,
				rejectedCount : 0
			};
			
			var dfd = $.Deferred();
			
			KPIService.getInspectionResultsForPastYear().then(function(inspectionsInfo){
				KPIService.getPassedInspections().then(function(result){
					kpiObj.totalCount = result.totalCount;
					kpiObj.passedCount = result.passedCount;
					kpiObj.rejectedCount = result.rejectedCount;
								 
					KPIService.getAllShops().then(function(response){
						$localDB.clearCollection('kpiShops').then(function(){
							$localDB.add('kpiShops',response.resultSet,{}).then(function(){
								$localDB.clearCollection('kpi').then(function(){
									 $localDB.add('kpi',kpiObj,{}).then(function(){
										 $localDB.clearCollection('kpiMonthCount').then(function(){
											 $localDB.add('kpiMonthCount',inspectionsInfo, {}).then(function(){
												 initKpi().then(function(){
													 dfd.resolve();
												 });
											 });
										 });
									 });
							 	});
							});
						})
					}).fail(function(){
						dfd.reject();
					});
				}).fail(function(){
					dfd.reject();
				});
			}).fail(function(){
				dfd.reject();
			});
			 
			return dfd.promise();
		},
		
		getObj : function() {
			return obj;
		},	
		
		setFailledObj : function(){
			var dfd = $.Deferred();
			
			 KPIService.getFailedData(obj.shopCode).then(function(result){
					failedObj = result;
					dfd.resolve("Success");
				 }).fail(function(){
					 dfd.reject();
				 });
			 return dfd.promise();
		},
		
		getFailedObj : function(){
			return failedObj;
		},
		
		getShopCode : function(){
			return obj.shopCode;
		}
	
	}
			
});