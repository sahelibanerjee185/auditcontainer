/**
 * Apr, 5 2016
 * auditService.js
 * Stavarache Vlad
 */

(function(){
	'use strict';
	
	audit
    .factory('auditService', auditService);
	
	auditService.$inject = ['$localDB', 'responseHandler', '$ionicLoading', 'startedAudit', 'multiModal', '$rootScope', '$kpiModule', 'GalleryExportService'];
	
	function auditService($localDB, responseHandler, $ionicLoading, startedAudit, multiModal, $rootScope, $kpiModule, GalleryExportService) {
		
		var lockedWorkOrders = []; // keeps track of work orders that weren't available for booking when planning a visit
		var plannedWorkOrders = []; // keeps track of work orders that were booked
		
		var service = {
			testSignal : testSignal,
			getCalendarData : getCalendarData,
			addWorkOrdersToVisitCollection : addWorkOrdersToVisitCollection,
			getOfflineCalendarData : getOfflineCalendarData,
			updateMissedWorkOrders : updateMissedWorkOrders,
			planVisit : planVisit,
			addWorkOrdersToVisit : addWorkOrdersToVisit,
			startAudit : startAudit,
			cancelAudit : cancelAudit,
			rescheduleAudit : rescheduleAudit,
			finishAudit : finishAudit,
			finishVisit: finishVisit,
			getVisits : getVisits,
			removeFinishedWorkOrders : removeFinishedWorkOrders,
			removeFinishedWorkOrderParts : removeFinishedWorkOrderParts,
			getWorkOrderParts : getWorkOrderParts,
			getFinishedWorkOrderParts : getFinishedWorkOrderParts,
			getVisitWorkOrders : getVisitWorkOrders,
			getWorkOrderPartsForWOArrray : getWorkOrderPartsForWOArrray,
			getFinishedVisitWorkOrders : getFinishedVisitWorkOrders, 
			storeFinishedVisitWorkOrders : storeFinishedVisitWorkOrders,
			getFinishedVisitWorkOrdersFromJsonStore : getFinishedVisitWorkOrdersFromJsonStore,
			getMissedVisitWorkOrders : getMissedVisitWorkOrders,
			getFinishedVisitParts : getFinishedVisitParts,
			storeFinishedVisitParts : storeFinishedVisitParts,
			setWorkOrderToAvailable : setWorkOrderToAvailable,
			saveFinishedWorkOrder: saveFinishedWorkOrder,
			saveFinishedSparePart: saveFinishedSparePart,
			saveWorkOrderLines: saveWorkOrderLines,
			getArchiveName:getArchiveName,
			getPhotos:getPhotos,
			getWOStatusList:getWOStatusList,
			// getWOModeList:getWOModeList, //duplicate found
			checkIfVisitFinished:checkIfVisitFinished,
			removeWOPartsByVisitId: removeWOPartsByVisitId,
			getWOModeList:getWOModeList,
			updateWorkOrderParts : updateWorkOrderParts,
			getLocalMissedVisitWorkOrders : getLocalMissedVisitWorkOrders,
			startAuditOffline : startAuditOffline,
			updateMissedWorkOrdersLocally : updateMissedWorkOrdersLocally,
			getLocalWorkOrderParts: getLocalWorkOrderParts,
			checkForLocalDataOnStartVisit : checkForLocalDataOnStartVisit,
			getPendingWorkOrderParts : getPendingWorkOrderParts,
			getModalOptions : getModalOptions,
			getPendingVisitAuditResults : getPendingVisitAuditResults,
			cancelAuditOffline : cancelAuditOffline,
			removeWOPartsByWorkOrderId : removeWOPartsByWorkOrderId,
			updateDeletedWorkOrders : updateDeletedWorkOrders,
			isPostRepair : isPostRepair,
			getWorkOrderInfo : getWorkOrderInfo
			
		};
		
		return service;
		
		function getCalendarData(){
			var defer = $.Deferred();
			
			WL.Client.invokeProcedure({
				adapter : 'AuditAdapter',
				procedure : 'getCalendarData',
				parameters : [userContext.getUsername(), userContext.getToken()]
			}, {
				onSuccess : function(response){
					if (!response.invocationResult.isSuccessful) {
						defer.reject(response.invocationResult);
					} else {
						var results = response.invocationResult.invocationResult;
						
						//CHANGES FOR OFFLINE MODE
						
						checkForStartedVisit().then(function(startedVisits){
							if(startedVisits && startedVisits.length > 0){
								
								console.log("A visit is started!!!");
				
								startedVisits.forEach(function(item){
									var startedVisit = item.json;
									var temp = [];
									results.forEach(function(result){
										if(result.visitId != startedVisit.visitId){
											temp.push(result);
										}
									});
									
									results = temp;
								});
								
								console.log("Final CalendarVisit Array on REFERSH");
							}
							
//							check for started visits
							$localDB.clearCollection('calendarVisits').then(function(){
								$localDB.add('calendarVisits', results, {}).then(function(){
									defer.resolve(results);
								});
							});
						});
					}
				},
				onFailure: function(error){
					errorHandler(error, defer);
				}
			});		  
			return defer.promise(); 
		}
		//
		
		function getOfflineCalendarData(){
			var defer = $.Deferred();
			
			var options = {
					filter: ['auditStatus','country','auditDate', 'description', 'location' , 'shopCode' , 'shopDescription' ,'visitId']
			};
			
			$localDB.find('calendarVisits', {auditStatus : 'Finished'}, {}).then(function(finishedVisits){
				
				var updatedCalendarVisits = finishedVisits.map(function(visit){
					if (visit.json) return visit.json;
				});
				
				console.log("THESE ARE THE FINISHED VISITS",updatedCalendarVisits);
				
				$localDB.find('visit', {}, options).then(function(visits){
					
					//2572 - make validation for 'Deleted' status
					visits.forEach(function(visitItem){
						if(visitItem.auditstatus !== 'Started' && visitItem.auditstatus !== 'Deleted'){
							updatedCalendarVisits.push({
								auditStatus : visitItem.auditstatus,
								country: visitItem.country,
								date : visitItem.auditdate,
								description: visitItem.description,
								locationDescription: visitItem.location,
								shopCode: visitItem.shopcode,
								shopDescription: visitItem.shopdescription,
								visitId: visitItem.visitid
							});
						}
					});
					
					console.log("THESE ARE FINAL UPDATEDVISITS",updatedCalendarVisits);
					
					$localDB.clearCollection('calendarVisits').then(function(){
						$localDB.add('calendarVisits', updatedCalendarVisits, {}).then(function(){
							defer.resolve(updatedCalendarVisits);
						});
					});
					
				});
			});
			
			return defer.promise();
		}
		
		function checkIfVisitFinished(visitId){
			var defer = $.Deferred();
			
			WL.Client.invokeProcedure({
				adapter : 'AuditAdapter',
				procedure : 'checkIfVisitFinished',
				parameters : [userContext.getUsername(), userContext.getToken(), visitId]
			}, {
				onSuccess : function(response){
					if (response.invocationResult && response.invocationResult.isSuccessful) {
						var isFinished = response.invocationResult.invocationResult.isFinished;
						defer.resolve(isFinished);
					} else {
						defer.reject("[AuditAdapter] Could not check if visit is finished");
					}
				},
				onFailure: function(error){
					defer.reject(error);
				}
			});
			
			return defer.promise();
		}
		
		/*
		 * 2572 get deleted workOrders from JsonStore
		 * */
		function updateDeletedWorkOrders(){
			var defer = $.Deferred();
			
			var requestArray = [];
			var deletedWO = [];
			$localDB.find('visit',{auditStatus : 'Deleted'},{}).then(function(workOrderArray){
				deletedWO = workOrderArray;
				
				if(workOrderArray.length > 0){
					workOrderArray.forEach(function(workOrder){
						requestArray.push(setWorkOrderToAvailable(workOrder.json.workOrderId));
					});
				}
				
				$.when.apply($,requestArray).then(function(){
					$localDB.erase('visit',deletedWO,{}).then(function(){
						defer.resolve();
					});
				}).fail(function(error){
					WL.Logger.error("[AuditService][updateDeletedWorkOrders] username: " + userContext.getUsername() + " " + JSON.stringify(error));
					defer.reject("Could not UpdateDeletedWorkOrders " + JSON.stringify(error));
				});
			});
			
			return defer.promise();
		}
		
		function updateMissedWorkOrders(){
			var defer = $.Deferred();
			var today = new Date();
			today.setHours(0, 0, 0);
			today = dateToSQLDateTime(today);
	
			console.log('today', today);
			
			WL.Client.invokeProcedure({
				adapter : 'AuditAdapter',
				procedure : 'updateMissedWorkOrders',
				parameters : [userContext.getUsername(), userContext.getToken(), today]
			}, {
				onSuccess : function(response){
					if (response && response.invocationResult && response.invocationResult.isSuccessful) {
						console.log('response', response);
						defer.resolve(response.invocationResult.invocationResult.updateStatementResult);
					} else {
						defer.reject();
					}
				},
				onFailure: function(error){
					errorHandler(error, defer);
				}
			});		  
			return defer.promise(); 
		}
		
		function updateMissedWorkOrdersLocally(){
			var defer = $.Deferred();
			var today = new Date();
			today.setHours(0, 0, 0);
//			today = dateToSQLDateTime(today);
			
			console.log('today', today);
			
			$localDB.find('visit', {}, {}).then(function(visits){
				var updatedVisits = [];
				visits.forEach(function(visit){
					var line = visit.json;
					if(Date.parse(line.auditDate) < Date.parse(today) && line.auditStatus == 'Planned'){
						line.auditStatus = 'Missed';
					}
					updatedVisits.push(line);
				});
				
				$localDB.clearCollection('visit').then(function(){
					$localDB.add('visit', updatedVisits, {}).then(function(){
						defer.resolve();
					});
				});
			});
			
			return defer.promise(); 
		}
		
		function savePhotos(path, name, date, visitId, woId, auditor, extra){
			var defer = $.Deferred();
			
			WL.Client.invokeProcedure({
				adapter : 'AuditAdapter',
				procedure : 'savePhotos',
				parameters : [userContext.getUsername(), userContext.getToken(),path, name, date, visitId, woId,auditor, extra]
			}, {
				onSuccess : function(response){
					if (!response.invocationResult.isSuccessful) {
						defer.reject(response.invocationResult);
					} else {
						defer.resolve(response.invocationResult.invocationResult.visitId);
					}
				},
				onFailure: function(error){
					errorHandler(error, defer);
				}
			});		  
			return defer.promise(); 
		}
		
		function getWorkOrderInfo(auditor, workOrderId) {
			var defer = $.Deferred();
			
			WL.Client.invokeProcedure({
				adapter : 'AuditAdapter',
				procedure : 'getWorkOrderInfo',
				parameters : [auditor, workOrderId, null]
			}, {
				onSuccess : function(response){
					console.log('response', response);
					if (response && response.invocationResult && !!response.invocationResult.invocationResult.length && response.invocationResult.isSuccessful) {
						var result = response.invocationResult.invocationResult[0];
						defer.resolve(result);
					} else {
						defer.reject();
					}
				},
				onFailure: function(error){
					defer.reject(error);
				}
			});		
			
			return defer.promise(); 
		}
		
		function planVisit(visitDate, currentShop, description, workOrderArray, context) {
			var defer = $.Deferred();
			
			WL.Client.invokeProcedure({
				adapter : 'AuditAdapter',
				procedure : 'planVisitImproved',
				parameters : [userContext.getUsername(), userContext.getToken(), visitDate, currentShop, description, workOrderArray, context]
			}, {
				onSuccess : function(response){
					console.log('response', response);
					if (response && response.invocationResult &&  response.invocationResult.isSuccessful) {
						var result = response.invocationResult.invocationContext;
							
						defer.resolve({lockedWorkOrders: result.lockedWorkOrders, plannedWorkOrders: result.plannedWorkOrders, visitId: result.visitId});
					} else {
						defer.reject();
					}
				},
				onFailure: function(error){
					console.log('error', error);
					$ionicLoading.hide();
					if(error.status==200)
					{var $pFailed = multiModal.setBasicOptions('notification', 'pFailed', 'Could not connect to the server. Please try again later.');
				     multiModal.openModal($pFailed, $rootScope);}
					else{
						errorHandlerWithFeedback(error, defer);
						
					}
				}
			});		
			
			return defer.promise(); 
		}
		
		function addWorkOrdersToVisit(visitId, visitDate, currentShop, description, auditStatus, workOrderArray, context) {
			var defer = $.Deferred();
			var auditStatusCode;
			
			if (auditStatus == 'Started') {
				auditStatusCode = 1;
			} else {
				auditStatusCode = 0;
			}
			
			WL.Client.invokeProcedure({
				adapter : 'AuditAdapter',
				procedure : 'addWorkOrdersToVisit',
				parameters : [userContext.getUsername(), userContext.getToken(), visitId, visitDate, currentShop, description, auditStatusCode, workOrderArray, context]
			}, {
				onSuccess : function(response){
					console.log('response', response);
					if (response && response.invocationResult &&  response.invocationResult.isSuccessful) {
						var result = response.invocationResult.invocationContext;
						defer.resolve({lockedWorkOrders : result.lockedWorkOrders, plannedWorkOrders : result.plannedWorkOrders}); // locked workorders
					} else {
						defer.reject();
					}
				},
				onFailure: function(error){
					console.log('error', error);
					errorHandlerWithFeedback(error, defer)
				}
			});		
			
			return defer.promise(); 
		}

		function rescheduleAudit(visitId, visitDate) {
			var defer = $.Deferred();
			
			visitDate.setHours(0, 0, 0);
			
			console.log('visitDate', visitDate);
			
			var sqlDate = dateToSQLDateTime(visitDate);
			
			WL.Client.invokeProcedure({
				adapter : 'AuditAdapter',
				procedure : 'rescheduleAudit',
				parameters : [userContext.getUsername(), userContext.getToken(), visitId, sqlDate]
			}, {
				onSuccess : function(response){
					if (response && response.invocationResult && response.invocationResult.isSuccessful) {
						defer.resolve(response.invocationResult.invocationResult.updateStatementResult);
					} else {
						defer.reject();
					}
				},
				onFailure: function(error){
					errorHandlerWithFeedback(error, defer);
				}
			});		  
			return defer.promise(); 
		}
		
		function startAudit(visitId) {
			var defer = $.Deferred();
			var currentDate = new Date();
			currentDate.setHours(0, 0, 0);
			
			currentDate = dateToSQLDateTime(currentDate)
			
			WL.Client.invokeProcedure({
				adapter : 'AuditAdapter',
				procedure : 'startAudit',
				parameters : [userContext.getUsername(), userContext.getToken(), visitId, currentDate]
			}, {
				onSuccess : function(response){
					if (response && response.invocationResult && response.invocationResult.isSuccessful) {
						defer.resolve(response.invocationResult.invocationResult.updateStatementResult.updateCount);
					} else {
						defer.reject();
					}
				},
				onFailure: function(error){
					errorHandlerWithFeedback(error, defer);
				}
			});		  
			return defer.promise(); 
		}

		function cancelAudit(visitId) {
			var defer = $.Deferred();
			
			WL.Client.invokeProcedure({
				adapter : 'AuditAdapter',
				procedure : 'cancelAudit',
				parameters : [userContext.getUsername(), userContext.getToken(), visitId]
			}, {
				onSuccess : function(response){
					if (response && response.invocationResult && response.invocationResult.isSuccessful) {
						// Need to delete local data as well
						removeWOPartsByVisitId(visitId).then(function(){
							//remove all phots for the canceled visit
							GalleryExportService.removePhotosForVisit(visitId);
							defer.resolve();
						});
					} else {
						WL.Logger.error("[AuditService][cancelAudit] username: " + userContext.getUsername() + 'Could not cancel audit');
						defer.reject();
					}
				},
				onFailure: function(error){
					WL.Logger.error("[AuditService][cancelAudit] username: " + userContext.getUsername() + " " + JSON.stringify(error));
					errorHandlerWithFeedback(error, defer);
				}
			});		  
			return defer.promise(); 
		}
		
		/*
		 * 2572 - Remove WO offline
		 */
		function cancelAuditOffline(visitId){
			var defer = $.Deferred();
			
			removeWOPartsByVisitId(visitId).then(function(){
				$localDB.find('calendarVisits', {visitId : visitId}, {}).then(function(calendarVisit){
					$localDB.erase('calendarVisits',calendarVisit,{}).then(function(){
						//remove all phots for the canceled visit
						GalleryExportService.removePhotosForVisit(visitId);
						defer.resolve();
					});
				});
			});
			
			return defer.promise();
		}
		
		function getFinishedVisitWorkOrders(visitId) {
			var defer = $.Deferred();
			
			WL.Client.invokeProcedure({
				adapter : 'AuditAdapter',
				procedure : 'getFinishedVisitWorkOrdersPostRepair',
				parameters : [userContext.getUsername(), userContext.getToken(), visitId]
			}, {
				onSuccess : function(response){
					console.log('omg response work orders', response);
					if (!response.invocationResult.isSuccessful) {
						defer.reject(response.invocationResult);
					} else {
						var results = filterPostRepairParts(response.invocationResult.invocationResult);
						
					    var parsedResults = results.map(function(workOrder){
							workOrder.repairCost = parseFloat(workOrder.repairCost).toFixed(2);
							return workOrder;
						})
						
						defer.resolve(parsedResults);
					}
				},
				onFailure: function(error){
					errorHandler(error, defer);
				}
			});	
			
			return defer.promise();
		}
		
		function removeFinishedWorkOrders(visitId) {
			var defer = $.Deferred();
			
			$localDB.find('finishedVisit', {visitId: visitId}, {exact: true}).then(function(auditToErase){
				if (auditToErase.length > 0) {
					$localDB.erase('finishedVisit', auditToErase, {}).then(function(){
						defer.resolve();
					});
				} else {
					defer.resolve();
				}
			});
			
			return defer.promise();
		}
		
		function removeFinishedWorkOrderParts(visitId) {
			var defer = $.Deferred();
			
			$localDB.find('finishedAuditParts', {visitId: visitId}, {exact: true}).then(function(partsToErase){
				if (partsToErase.length > 0) {
					$localDB.erase('finishedAuditParts', partsToErase, {}).then(function(){
						defer.resolve();
					});
				} else {
					defer.resolve();
				}
			});
			
			return defer.promise();
		}
		
		function storeFinishedVisitWorkOrders(visitId) {
			var defer = $.Deferred();
			var options = {
				filter: ['auditDate', 'auditStatus', 'containerNo', 'containerType', 'description', 
				         'repairCost', 'repairDate', 'shopCode', 'visitId', 'woStatus', 'woType', 'workOrderId',
				         'shopDescription', 'statusCode', 'auditResult','finalComment','mode', 'mercStatus', 'mercComments','repairQuality']
			};
			
			getFinishedVisitWorkOrders(visitId).then(function(finishedWorkOrders){
				console.log('finishedWorkOrders', finishedWorkOrders); // clear visitId info
				$localDB.find('finishedVisit', {visitId: visitId}, {exact: true}).then(function(auditToErase){
					var helperDefer = $.Deferred();
					if (auditToErase.length > 0) {
						$localDB.erase('finishedVisit', auditToErase, {}).then(function(){
							helperDefer.resolve();
						});
					} else {
						helperDefer.resolve();
					}
					
					helperDefer.then(function(){
						$localDB.add('finishedVisit', finishedWorkOrders, {}).then(function(){
							WL.JSONStore.get('finishedVisit').find({visitId : visitId}, options).then(function(filteredData){
								console.log('i added something to finishedVisit', filteredData);
								defer.resolve(filteredData);
							});
						});
					});
				});
				
			}).fail(function(error){
				defer.reject(error);
			});
			
			return defer.promise();
		}
		
		function getFinishedVisitWorkOrdersFromJsonStore(visitId) {
			var defer = $.Deferred();
			var options = {
				filter: ['auditDate', 'auditStatus', 'containerNo', 'containerType', 'description', 
				         'repairCost', 'repairDate', 'shopCode', 'visitId', 'woStatus', 'woType', 'workOrderId',
				         'shopDescription', 'statusCode', 'auditResult', 'finalComment', 'mode', 'mercStatus', 'mercComments']
			};
			
			WL.JSONStore.get('finishedVisit').find({visitId : visitId}, options).then(function(filteredData){
				defer.resolve(filteredData);
			});
			
			return defer.promise();
		}
		
		function getMissedVisitWorkOrders(visitId) {
			var defer = $.Deferred();
			
			WL.Client.invokeProcedure({
				adapter : 'AuditAdapter',
				procedure : 'getMissedVisitWorkOrders',
				parameters : [userContext.getUsername(), userContext.getToken(), visitId]
			}, {
				onSuccess : function(response){
					console.log('omg response work orders', response);
					if (!response.invocationResult.isSuccessful) {
						defer.reject(response.invocationResult);
					} else {
						var results = response.invocationResult.invocationResult;
						
						var parsedResults = results.map(function(workOrder){
							workOrder.repaircost = parseFloat(workOrder.repaircost).toFixed(2);
							return workOrder;
						});
						defer.resolve(results);
					}
				},
				onFailure: function(error){
					errorHandler(error, defer);
				}
			});	
			
			return defer.promise();
		}
		
		//get missed WO from localStorage
		function getLocalMissedVisitWorkOrders(visitId) {
			var defer = $.Deferred();
    		
    		WL.JSONStore.get('visit').find({ visitId: visitId }, {}).then(function (data) {
    			if(data && data.length > 0){
    				var missedWO = [];
    				
    				data.forEach(function(workOrder){
    					if(workOrder.json.auditStatus !== 'Deleted'){
    						var wo = {};
        					wo.auditdate = workOrder.json.auditDate;
        					wo.auditstatus = workOrder.json.auditStatus;
        					wo.containerno = workOrder.json.containerNo;
        					wo.containertype = workOrder.json.containerType;
        					wo.country = workOrder.json.country;
        					wo.description = workOrder.json.description;
        					wo.location = workOrder.json.location;
        					wo.mode = workOrder.json.mode;
        					wo.repaircost = workOrder.json.repairCost;
        					wo.repairdate = workOrder.json.repairDate;
        					wo.shopcode = workOrder.json.shopCode;
        					wo.shopdescription = workOrder.json.shopDescription;
        					wo.visitId = workOrder.json.visitId;
        					wo.workorderid = workOrder.json.workOrderId;
        					wo.wostatus = workOrder.json.woStatus;
        					wo.wotype = workOrder.json.woType;
        					
        					missedWO.push(wo);
    					}
    				});
    				
    				defer.resolve(missedWO);
    			}else{
    				defer.reject();
    			}
            }).fail(function(){
            	defer.reject();
            })
			
			return defer.promise();
		}
		//
		
		function getVisitWorkOrders(visitId) {
			var defer = $.Deferred();
			
			$localDB.find('visit', {visitId : visitId}, {exact : true}).then(function(workOrders){
				var parsedWorkOrders = [];
				
				angular.forEach(workOrders, function(workOrder){
					parsedWorkOrders.push(workOrder.json);
				});
				
				defer.resolve(parsedWorkOrders);
			}).fail(function(e){
				defer.reject(e);
			});
			
			return defer.promise(); 
		}
		
		function getWorkOrderParts(workOrderId) {
			var defer = $.Deferred();

			WL.Client.invokeProcedure({
				adapter : 'AuditAdapter',
				procedure : 'getWorkOrderPartsImproved',
				parameters : [userContext.getUsername(), userContext.getToken(), workOrderId]
			}, {
				onSuccess : function(response){
					console.log('response', response);
					if (!response.invocationResult.isSuccessful) {
						defer.reject(response.invocationResult);
					} else {
						var results = response.invocationResult.invocationResult;
						
						console.log('results', results);
						
						defer.resolve(results);
					}
				},
				onFailure: function(error){
					defer.reject(error);
				}
			});	
			
			return defer.promise();
		}
		
		function getWorkOrderPartsForWOArrray(workOrders) {
			var defer = $.Deferred();
			
			WL.Client.invokeProcedure({
				adapter : 'AuditAdapter',
				procedure : 'getWorkOrderPartsForWOArrray',
				parameters : [userContext.getUsername(), userContext.getToken(), workOrders]
			}, {
				onSuccess : function(response){
					console.log('response', response);
					if (!response.invocationResult.isSuccessful) {
						defer.reject(response.invocationResult);
					} else {
						var results = response.invocationResult.invocationResult;
						
						console.log('results', results);
						
						defer.resolve(results);
					}
				},
				onFailure: function(error){
					errorHandler(error, defer);
				}
			});	
			
			return defer.promise();
		}
		
		//SPOT 7 get offline data for depodetails
		function getLocalWorkOrderParts(workOrderId){
			var defer = $.Deferred();
			
			$localDB.find('workOrderParts', {workOrderId : workOrderId}, {exact: true}).then(function(result){
				if(result && result.length > 0){
					
					var response = result.map(function(part){
						if (part.json) return part.json;
					});
					
					var parentList = []; // Repair lines and isolated parts
					var childList = [];
					
					for (var i = 0; i < response.length; i++) {
						var line = response[i];
						
						// Check if line is repair 
						
						if (line.woRepair) { 
							line.$childParts = []; // init children array
							parentList.push(line);
						} else { 
							// This line is a part
							childList.push(line);
						}
					}
					
					childList.forEach(function(line){
						addChildToParent(line,parentList)
					});
					
					
					defer.resolve(parentList);
					
				}else{
					//no data saved locally for that work order
					defer.reject();
				}
			}).fail(function(){
				defer.reject();
			});
			
			return defer.promise();
			
		}
		
		function addChildToParent(line,parentList){
			
			var parentRepairLine = getObjectForRepairCodeHelper(line.repairCode, parentList);
			
			// Check to see if it's a child or an orphan
			
			line.totalPerCode = parentRepairLine.materialCost + line.partCost;
			
			if (parentRepairLine) {
				parentRepairLine.$childParts.push(line);
			} else {
				// not possible with how the data is inserted in MERC+
			}
		}
		
		function getObjectForRepairCodeHelper(repairCode, array){
			
			for (var i = 0; i < array.length; i++) {
				var object = array[i];
				
				if (object.repairCode == repairCode && object.$childParts instanceof Array) return object;
			}
			
			return false;
		}
		
		//start audit offline
		function startAuditOffline(visitId){
			var defer = $.Deferred();
//			var currentDate = new Date();
//			currentDate.setHours(0, 0, 0);
//			
//			currentDate = dateToSQLDateTime(currentDate)
			
			$localDB.find('visit', {visitId : visitId}, {exact : true}).then(function(startedWorkOrders){
				var count = 0;
				var pendingWorkOrders = startedWorkOrders.map(function(workOrder){
					if(workOrder.json.auditStatus !== 'Deleted'){
						workOrder.json.auditStatus = "Started";
						count++;
						return workOrder;
					}
				});
				
				$localDB.replace('visit', startedWorkOrders, {}).then(function(){
					defer.resolve(count);
				}).fail(function(){
					defer.reject(error);
				});
				
			}).fail(function(){
				defer.reject(error);
			});
			
			return defer.promise(); 
		}
		
		/*
		 * Validation for starting a visit Offline
		 * Check of your WorkOrders have all the repair and parts stored locallt if one of them is incomplete stop the operation
		*/
		function checkForLocalDataOnStartVisit(visitId){
			var defer = $.Deferred();
			
			$localDB.find('visit', {visitId : visitId}, {}).then(function(visitData){
				var requestArray = [];
				for(var i=0; i<visitData.length; i++){
					if(visitData[i].json.auditStatus !== 'Deleted')
						requestArray.push(checkForRepairPartsInWorkWorder(visitData[i].json.workOrderId));
				}
				
				$.when.apply($, requestArray).then(function(){
					defer.resolve();
				}).fail(function(error){
					defer.reject();
				});
				
			});
			
			return defer.promise();
		}
		
		function checkForRepairPartsInWorkWorder(workOrderId){
			var defer = $.Deferred();
			
			$localDB.find('workOrderParts', {workOrderId : workOrderId}, {exact: true}).then(function(result){
				console.log("WO RESULT",result);
				if(result && result.length>0){
					defer.resolve();
				}else{
					defer.reject();
				}
			}).fail(function(){
				defer.reject();
			});
			
			return defer.promise();
		}
		//
		
		function getFinishedVisitParts(visitId) {
			var defer = $.Deferred();
			
			WL.Client.invokeProcedure({
				adapter : 'AuditAdapter',
				procedure : 'getFinishedVisitPartsPostRepair',
				parameters : [userContext.getUsername(), userContext.getToken(), visitId]
			}, {
				onSuccess : function(response){
					if (!response.invocationResult.isSuccessful) {
						defer.reject(response.invocationResult);
					} else {
						var results = filterPostRepairParts(response.invocationResult.invocationResult);
						defer.resolve(results);
					}
				},
				onFailure: function(error){
					errorHandler(error, defer);
				}
			});	
			
			return defer.promise();
		}
		
		function filterPostRepairParts(finishedParts){
			
			var filteredParts = [];
			finishedParts.forEach(function(part){
				if(part.repairQuality == -1){
					part.repairQuality = "N/A";
				}
				filteredParts.push(part);
			});
			
			return filteredParts;
		}
		
		function saveWorkOrderLines(woLines, plannedWorkOrders, visitId) {
			var defer = $.Deferred();
			
			// Add visitId to the lines
			
			var parsedWoLines = [];
			
			angular.forEach(woLines, function(woLine){
				for (var i = 0; i < plannedWorkOrders.length; i++) {
					var workOrderId = plannedWorkOrders[i].workOrderId;
					
					if (woLine.workOrderId == workOrderId) {
						woLine.visitId = visitId;
						parsedWoLines.push(woLine);
						break;
					}
				}
			});
			
			// Find and remove any work order parts that might already be stored
			
			var cleanUpLines = []; // promise array
			
			angular.forEach(plannedWorkOrders, function(workOrder){
				var workOrderId = workOrder.workOrderId;
				cleanUpLines.push(removeWOPartsByWorkOrderId(workOrderId))
			});
			
			$.when.apply($, cleanUpLines).then(function(){
				$localDB.add('workOrderParts', parsedWoLines, {}).then(function(){
					defer.resolve(parsedWoLines);
				});
			});
			
			return defer.promise();
		}
		
		//update workorders
		function updateWorkOrderParts(workOrders, visitId){
			
			var defer = $.Deferred();
			
			var newWorkOrders = angular.copy(workOrders);
			var newLines = splitRepairsFormParts(newWorkOrders, visitId);
			
			var cleanUpLines = []; // promise array
			
			angular.forEach(newLines, function(workOrder){
				var workOrderId = workOrder.workOrderId;
				cleanUpLines.push(removeWOPartsByWorkOrderId(workOrderId))
			});
			
			$.when.apply($, cleanUpLines).then(function(){
				$localDB.add('workOrderParts', newLines, {}).then(function(){
					defer.resolve();
				});
			});
			
			return defer.promise();
			
		}
		
		function splitRepairsFormParts(newWorkOrders, visitId, array){
			
			// TODO CHECK THIS
			var updatedWorkOrders = array || [];
			
			for(var i=0;i<newWorkOrders.length;i++){
				if(newWorkOrders[i].$childParts && newWorkOrders[i].$childParts.length > 0){
					splitRepairsFormParts(newWorkOrders[i].$childParts, visitId,updatedWorkOrders);
				}
				
				var line = newWorkOrders[i];
				delete line.$childParts;
				line.woRepair = false;
				line.visitId = visitId;
				
				if(line.repairDescription && line.repairDescription.length > 0) line.woRepair = true;
				
				updatedWorkOrders.push(line);
				
			}
			
			return updatedWorkOrders;
		}
		//
		
		function removeWOPartsByWorkOrderId(workOrderId) {
			var defer = $.Deferred();
			
			$localDB.find('workOrderParts', {workOrderId : workOrderId}, {exact: true}).then(function(woLinesToRemove){
				if (woLinesToRemove.length > 0) {
					$localDB.erase('workOrderParts', woLinesToRemove, {}).then(function(){
						defer.resolve();
					});
				} else {
					defer.resolve();
				}
			})
			
			return defer.promise();
		}
		
		function removeWOPartsByVisitId(visitId) {
			var defer = $.Deferred();
			
			$localDB.find('workOrderParts', {visitId : visitId}, {exact: true}).then(function(woLinesToRemove){
				if (woLinesToRemove.length > 0) {
					$localDB.erase('workOrderParts', woLinesToRemove, {}).then(function(){
						defer.resolve();
					});
				} else {
					defer.resolve();
				}
			})
			
			return defer.promise();
		}
		
		function storeFinishedVisitParts(visitId) {
			var defer = $.Deferred();
			
			getFinishedVisitParts(visitId).then(function(finishedParts){
				$localDB.find('finishedAuditParts', {visitId: visitId}, {exact: true}).then(function(partsToErase){
					var helperDefer = $.Deferred();
					if (partsToErase.length > 0) {
						$localDB.erase('finishedAuditParts', partsToErase, {}).then(function(){
							helperDefer.resolve();
						});
					} else {
						helperDefer.resolve();
					}
					
					helperDefer.then(function(){
						$localDB.add('finishedAuditParts', finishedParts, {}).then(function(){
							defer.resolve(finishedParts);
						});
					});
				});
				
			}).fail(function(error){
				WL.Logger.error("[AuditService][storeFinishedVisitParts][getFinishedVisitParts] username: " + userContext.getUsername() + " " +JSON.stringify(error));
				defer.reject(error);
			});
			
			return defer.promise();
		}
		
		function getFinishedWorkOrderParts(workOrderId, visitId) {
			var defer = $.Deferred();
			
			$localDB.find('finishedAuditParts', {workOrderId : workOrderId, visitId : visitId}, {exact : true}).then(function(data){
				var filteredParts = data.map(function(partData){
					if (partData.json) return partData.json;
				})
				
				defer.resolve(filteredParts);
			});
			
			return defer.promise();
		}
		
		function getArchiveName(visitId) {
			var defer = $.Deferred();
			
			WL.Client.invokeProcedure({
				adapter : 'AuditAdapter',
				procedure : 'getArchivePath',
				parameters : [userContext.getUsername(), userContext.getToken(), visitId]
			}, {
				onSuccess : function(response){
					if (!response.invocationResult.isSuccessful) {
						defer.reject(response.invocationResult);
					} else {
						var results = response.invocationResult.invocationResult;
						defer.resolve(results);
					}
				},
				onFailure: function(error){
					errorHandlerWithFeedback(error, defer);
				}
			});	
			
			return defer.promise();
		}
		
		/*
		 * get photos for specific visit
		 */
		function getPhotos(visitId) {
			var defer = $.Deferred();
			
			WL.Client.invokeProcedure({
				adapter : 'AuditAdapter',
				procedure : 'getPhotos',
				parameters : [userContext.getUsername(), userContext.getToken(), visitId]
			}, {
				onSuccess : function(response){
					if (!response.invocationResult.isSuccessful) {
						defer.reject(response.invocationResult);
					} else {
						var results = response.invocationResult.invocationResult;
						defer.resolve(results);
					}
				},
				onFailure: function(error){
					errorHandlerWithFeedback(error, defer);
				}
			});	
			return defer.promise();
		}
		
		function setWorkOrderToAvailable(workOrderId) {
			var defer = $.Deferred();
			
			WL.Client.invokeProcedure({
				adapter : 'AuditAdapter',
				procedure : 'setWorkOrderToAvailable',
				parameters : [userContext.getUsername(), userContext.getToken(), workOrderId]
			}, {
				onSuccess : function(response){
					if (response && response.invocationResult && response.invocationResult.isSuccessful) {
						removeWOPartsByWorkOrderId(workOrderId).then(function(){
							defer.resolve();
						})
					} else {
						WL.Logger.error("[AuditService][setWorkOrderToAvailable][onSuccess] username: " + userContext.getUsername() + " Could not set WO to Available");
						defer.reject();
					}
				},
				onFailure: function(error){
					WL.Logger.error("[AuditService][setWorkOrderToAvailable] username: " + userContext.getUsername() + " Could not set WO to Available");
					defer.reject(error);
//					errorHandlerWithFeedback(error, defer);
				}
			});		  
			return defer.promise(); 
		}
		
		function getVisits() {
			var defer = $.Deferred();
			
			WL.Client.invokeProcedure({
				adapter : 'AuditAdapter',
				procedure : 'getVisits',
				parameters : [userContext.getUsername(), userContext.getToken()]
			}, {
				onSuccess : function(response){
					if (!response.invocationResult.isSuccessful) {
						defer.reject(response.invocationResult);
					} else {
						var results = response.invocationResult.invocationResult;
						// might move localDB operations to a separate function- refactor TODO
						var parsedResults = response.invocationResult.invocationResult.map(function(workOrder){
							workOrder.repairCost = parseFloat(workOrder.repairCost).toFixed(2);
							return workOrder;
						});
						// map to get toFixed
						
						//CHANGES FOR OFFLINE MODE
						console.log("Initial Visit Array on REFERSH", parsedResults);
						
						//TODO IMPROVED BASED ON MAP insteand of FOR and visitID insetead of started
						checkForStartedVisit().then(function(localVisits){
							if(localVisits && localVisits.length > 0){
								console.log("A visit is started!!!");
								localVisits.forEach(function(item){
									
									var startedVisit = item.json;
									for(var i=0;i< parsedResults.length;i++){
										if(parsedResults[i].workOrderId == startedVisit.workOrderId 
												&& parsedResults[i].auditStatus != startedVisit.auditStatus){
											console.log("found one", parsedResults[i]);
											parsedResults[i] = startedVisit;
										}
									}
									
								});
								
								console.log("Final Visit Array on REFERSH", parsedResults);
							}
							
							$localDB.clearCollection('visit').then(function(){
								$localDB.add('visit', parsedResults, {}).then(function(){
									defer.resolve();
								});
							});
							
						});
						//						
						
					}
				},
				onFailure: function(error){
					errorHandler(error, defer);
				}
			});	
			
			return defer.promise(); 
		}
		
		function addWorkOrdersToVisitCollection(workOrders) {
			var defer = $.Deferred();
			
			$localDB.add('visit', workOrders, {}).then(function(){
				defer.resolve();
			}).fail(function(error){
				defer.reject(error);
			})
			
			return defer.promise();
		}
		
		//HANDLE STARTED VISIT IN OFFLINE
		function checkForStartedVisit(){
			
			var defer = $.Deferred();
			
			$localDB.find('visit', {auditStatus : 'Started'}, {}).then(function(results){
				console.log('startedVisits11',results);
				defer.resolve(results);
			});
			
			return defer.promise();
		}
		//
		
		function saveFinishedWorkOrder(workOrderId, visitDate, visitId, visitDesc, shopCode, auditResult,archiveName) {
			var defer = $.Deferred();
			visitDesc = visitDesc || "";
			
			WL.Client.invokeProcedure({
				adapter : 'AuditAdapter',
				procedure : 'saveFinishedWorkOrderBeta',
				parameters : [userContext.getUsername(), userContext.getToken(), workOrderId, visitDate, visitId, visitDesc, shopCode, auditResult, archiveName]
			}, {
				onSuccess : function(response){
					successHandler(response, defer);
				}, 
				onFailure: function(error){
					errorHandler(error, defer);
				}
			});		  
			return defer.promise();
		}

		function finishVisit(finishedWorkOrders, sqlDate, zipPath, auditParts, shopCode, photos, visitId){
			var defer = $.Deferred();
			var certificateId = userContext.getCertificateId();

			
			WL.Client.invokeProcedure({
				adapter : 'FinishAdapter',
				procedure : 'finishVisitPostRepair',
				parameters : [userContext.getUsername(), userContext.getToken(), finishedWorkOrders, sqlDate, zipPath, auditParts, shopCode, photos, visitId, certificateId]
			}, {
				onSuccess : function(response){
					successHandler(response, defer);
				}, 
				onFailure: function(error){
					WL.Logger.error("[AuditService][finishVisit] username: " + userContext.getUsername() + " " + JSON.stringify(error));
					defer.reject(error);
				}
			});		  
			return defer.promise();
		}
		
		function saveFinishedSparePart(workOrderId, visitDate, visitId, shopCode, auditResult, preEqpNo, postEqpNo, 
	    preQtyParts, postQtyParts, preRepairCode, postRepairCode, preRepairLocCode, postRepairLocCode, preDamageCode, postDamageCode) {
			
			var defer = $.Deferred();
			
			WL.Client.invokeProcedure({
				adapter : 'AuditAdapter',
				procedure : 'saveFinishedWorkOrderPart',
				parameters : [userContext.getUsername(), userContext.getToken(), workOrderId, visitDate, visitId, shopCode, auditResult, preEqpNo, postEqpNo, 
				              preQtyParts, postQtyParts, preRepairCode, postRepairCode, preRepairLocCode, postRepairLocCode, preDamageCode, postDamageCode]
			}, {
				onSuccess : function(response){
					successHandler(response, defer);
				},
				onFailure: function(error){
					errorHandler(error, defer);
				}
			});		  
			return defer.promise();
		}
		
		function getWOStatusList(){
			var defer = $.Deferred();
			
			WL.Client.invokeProcedure({
				adapter : 'AuditAdapter',
				procedure : 'getWOStatusList',
				parameters : [userContext.getUsername(), userContext.getToken()]
			}, {
				onSuccess : function(response){
					console.log(response);
					if (!response.invocationResult.isSuccessful) {
						defer.reject(response.invocationResult);
					} else {
						var results = response.invocationResult.invocationResult;
						// Add Post - Repair status manually
						var statuses = results.resultSet;
						statuses.push({status: 'Post - Repair'})
						//save them locally
						$localDB.clearCollection('woStatus').then(function(){
							$localDB.add('woStatus', statuses, {}).then(function(){
								defer.resolve(results);
							});
						});
					}
				},
				onFailure: function(error){
					console.log('statusList e', error);
					errorHandler(error, defer);
				}
			});		  
			return defer.promise(); 
		}
		
		function getWOModeList(){
			var defer = $.Deferred();
			
			WL.Client.invokeProcedure({
				adapter : 'AuditAdapter',
				procedure : 'getWOModeList',
				parameters : [userContext.getUsername(), userContext.getToken()]
			}, {
				onSuccess : function(response){
					console.log(response);
					if (!response.invocationResult.isSuccessful) {
						defer.reject(response.invocationResult);
					} else {
						var results = response.invocationResult.invocationResult;
						
						var modes = results.resultSet;
						$localDB.clearCollection('woMode').then(function(){
							$localDB.add('woMode', modes, {}).then(function(){
								defer.resolve(results);
							});
						});
					}
				},
				onFailure: function(error){
					console.log('getWOModeList e', error);
					errorHandler(error, defer);
				}
			});		  
			return defer.promise(); 
		}
		
		function finishAudit(visitId, finishedWorkOrders, zipPath , finishCallback) {
			var defer = $.Deferred();
			var visitDate = new Date();
			
			var sqlDate = dateToSQLDateTime(visitDate);
			
			var wasCanceled; // tells us if at least one work order was audited or not
			
			// $ionicLoading.show(loaderConfig);
			
			// show confirm modal depending on audit status
			
			wasCanceled = false;
			if (finishedWorkOrders.length != 0) var shopCode = finishedWorkOrders[0].shopcode;
			
			startedAudit.getAuditParts().then(function(auditParts){ // get audited parts
				startedAudit.getStartedVisit().then(function(dataVisitId){
					 $localDB.find("galleryPhoto",{visitId : dataVisitId},{}).then(function(photos){
						 finishVisit(finishedWorkOrders, sqlDate, zipPath,auditParts,shopCode, photos,visitId)
						 	.then(function(data){
						 		// Reset photos counter
						 		$localDB.find("utils",{},{}).then(function(data){
						 			if (data.length > 0) {
										data[0].json.photoCount = 0;
										$localDB.replace("utils",data[0],{});
						 			}
								});
						 		
						 		// Remove local data
						 		removeWOPartsByVisitId(dataVisitId);
						 		// GalleryExportService.removePhotosForVisit(dataVisitId);
						 		
					 			if (finishedWorkOrders.length == 0) wasCanceled = true;
								
								// Update local data to reflect changes
								
								startedAudit.moveStartedVisitsToFinished().then(function(){
									defer.resolve(wasCanceled);
									 // not very useful resolve
									if (typeof finishCallback === 'function') finishCallback(wasCanceled);
								});
						 	}).fail(function(error){
							 WL.Logger.error("[AuditService][finishAudit][finishVisitAdapter] username: " + userContext.getUsername() + " Could not finish visit " + JSON.stringify(error));
						 		$ionicLoading.hide();
						 		defer.reject(error);
						 	});
					 });
				 }).fail(function(){
						WL.Logger.error("[AuditService][finishAudit][getStartedVisit] username: " + userContext.getUsername() + " Could not get started visit " + JSON.stringify(error));
						$ionicLoading.hide();
						defer.reject(error);
					 })
			}).fail(function(){
				WL.Logger.error("[AuditService][finishAudit][getAuditParts] username: " + userContext.getUsername() + " Could not get audit parts " + JSON.stringify(error));
				$ionicLoading.hide();
				defer.reject(error);
			 })
			
			
			return defer.promise();
		}
		
		function getModalOptions(allWorkOrders, finishedWorkOrders) {
			if (finishedWorkOrders.length == 0) {
				return multiModal.setBasicOptions('confirm', 'cancelVisit', 'Finishing the visit without inspecting any containers will cancel the visit. Are you sure you want to continue?');
			}
			
			if (finishedWorkOrders.length > 0 && finishedWorkOrders.length < allWorkOrders.length) {
				return multiModal.setBasicOptions('confirm', 'notAllInspectedContainers', 'The uninspected containers in this visit will be dismissed. Are you sure you want to finish this visit?');
			}
			
			if (finishedWorkOrders.length == allWorkOrders.length) {
				return multiModal.setBasicOptions('confirm', 'finishVisit', 'Are you sure you want to finish this visit?');
			}
		}
		
		function dateToSQLDateTime(date) {
			if (isNaN(date.getFullYear()))
				return;
			
			return date.getFullYear() + '-'
					+ ('00' + (date.getMonth() + 1)).slice(-2) + '-'
					+ ('00' + date.getDate()).slice(-2) + ' '
					+ ('00' + date.getHours()).slice(-2) + ':'
					+ ('00' + date.getMinutes()).slice(-2) + ':'
					+ ('00' + date.getSeconds()).slice(-2);
		}
		
		function successHandler(response, defer) {
			if (!response.invocationResult.isSuccessful) {
				defer.reject(response.invocationResult);
			} else {
				defer.resolve(response);
			}
		}
		
		function errorHandler(error, defer) {
			console.log('error', error);
			defer.reject(error);
		} 
		
		function errorHandlerWithFeedback(error, defer) {
			console.log('error', error);
			$ionicLoading.hide();
			defer.reject(error);
			responseHandler.feedback(error);
		} 
		
		function testSignal() {
			var defer = $.Deferred();
			
			if (window.Connection && (navigator.connection.type == Connection.NONE)) {
				defer.reject("error");
			}else{
				WL.Client.invokeProcedure({
					adapter : 'AuditAdapter',
					procedure : 'getVisitId',
					parameters : [userContext.getUsername(), userContext.getToken()]
				}, {
					onSuccess : function(response){
						defer.resolve("success");
					},
					onFailure: function(error){
						defer.reject("error");
					}
				});	
			}
			
			return defer.promise(); 
		}
		
		function getPendingWorkOrderParts(workOrderId){
			var defer = $.Deferred();
			
			$localDB.find('pendingAuditWorkOrderParts',{workOrderId : workOrderId},{}).then(function(result){
				
				if(result && result.length > 0){
					var parsedResults = result.map(function(part){
						if(part.json) return part.json;
					});
					
					defer.resolve(parsedResults);
				}else{
					defer.reject();
				}
			});
			
			return defer.promise();
		}
		
		function getPendingVisitAuditResults(visitData){
			var defer = $.Deferred();
			var parsedVisits = [];
			
			var requestArray = [];
			for(var i=0; i<visitData.length; i++){
				requestArray.push(getAuditResultOfWorkorder(visitData[i], parsedVisits));
			}
			
			$.when.apply($, requestArray).then(function(){
				defer.resolve(parsedVisits);
			}).fail(function(error){
				defer.reject();
			});
			
			return defer.promise();
		}
		
		function getAuditResultOfWorkorder(workOrder, parsedVisits){
			var defer = $.Deferred();
			
			$localDB.find('pendingAuditWorkOrders',{workOrderId : workOrder.workorderid},{}).then(function(result){
				if(result && result.length > 0){
					if (result[0].json.inspectionStatus == 'Finished') {
						var auditResult = result[0].json.auditResult ? "Accepted" : "Rejected";
						workOrder.auditresult = auditResult;
						workOrder.finalcomment = result[0].json.finalComment;
						parsedVisits.push(workOrder);
					}
				}
				defer.resolve()
			});
			
			return defer.promise();
		}

		function isPostRepair(statusCode){
			return statusCode > 390 ? true : false;
		}
	}
})();