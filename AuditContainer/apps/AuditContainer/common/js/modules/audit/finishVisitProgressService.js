
/* JavaScript content from js/modules/audit/finishVisitProgressService.js in folder common */
/**
 * May, 8 2017
 * finishVisitProgressService.js
 * Florian Birloi
 */

(function(){
	'use strict';
	
	audit
    .factory('finishVisitProgressService', finishVisitProgressService);
	
	finishVisitProgressService.$inject = ['$localDB','multiModal', 'pdfModule', 'archiveModule', 'startedAudit', 'auditService',
    '$rootScope', 'pdfGenerationHandler','ToastService', '$ionicLoading'];
	
	function finishVisitProgressService($localDB, multiModal, pdfModule, archiveModule, startedAudit, auditService,
    $rootScope, pdfGenerationHandler, ToastService, $ionicLoading) {
		
		/*
		 * ------------Finish Visit Progress Process-------------
		 * 
		 * If one of the processes fails you should be able to make a retry request and redo the process
		 * On Cancel you should clear all the data build during the operations from the process described by the user story
		 * All the functions and operations which can change the Context or change data from localStorage from the controller will be left after all the 3 processes will successfully finish
		 * In case one of the PDF or Archive are generated but fail to upload when the process is repeated they should be just uploaded and not generated again
		 */
		
		/*
		 * This obj will be populated with data from the surveyModule
		 * 
		 */
		var obj = { }
		var finishProcess = [
                { success : false , processing : false, processed : false, failed : false, process : uploadPhotoArhive, path : '' },
				{ success : false , processing : false, processed : false, failed : false, process : uploadPDF, generated : false }, 
				{ success : false , processing : false, processed : false, failed : false, process : updateDatabase }
		]
        var callback;
        var currentStep = 1;
        
        return {
			startFinishVisit : startFinishVisit,
            startFinishVisitOffline : startFinishVisitOffline,
			resetFinishProcessData : resetFinishProcessData,
            retry : retry,
            getFinishProcessObject : getFinishProcessObject,
            setCallbackFunction : setCallbackFunction,
            getCurrentStep : getCurrentStep
		}
        
		/*
		 * Open Modal with starting first task
		 */
		function startFinishVisit(signatures, finishedWorkOrders, calendarObject){
            if (finishedWorkOrders.length > 0) setOnlineProcesses();
            obj = calendarObject;
            obj.startedVisitInfo.signatures = signatures;
            obj.finishedWorkOrders = finishedWorkOrders;
            obj.dateTime = buildFinishDate();
            obj.visitId = obj.startedVisitInfo.visitId;
            pdfGenerationHandler.setPdfContext(obj);
            // Check if visit needs to be canceled or not
            if (finishedWorkOrders.length > 0) {
			    retry();
            } else {
                // Check if pdf on device is deleted etc...
                // Check if photos on device are deleted! - TODO
                $ionicLoading.show(cancelVisitConfig)
                updateDatabase().then(function(){
                    ToastService.showNotificationMsg('Your visit has been canceled.');
                    $ionicLoading.hide();
                }).fail(function(error){
                    // show error message
                    $ionicLoading.hide();
                });
            }
		}
        
        function startFinishVisitOffline(workOrders, sqlDate, workOrderParts, shopCode, photosToSave, visitId){
            if (workOrders.length > 0) setOfflineProcesses();
            obj.workOrders = workOrders; 
            obj.sqlDate = sqlDate; 
            obj.workOrderParts = workOrderParts; 
            obj.shopCode = shopCode; 
            obj.photosToSave = photosToSave; 
            obj.visitId = visitId;
            
            if (workOrders.length > 0) {
                retry();
            } else {
                // Check if pdf on device is deleted etc...
                // Check if photos on device are deleted! - TODO
                $ionicLoading.show(cancelVisitConfig)
                databaseOffline().then(function(){
                    ToastService.showNotificationMsg('Your visit has been canceled.');
                    $ionicLoading.hide();
                }).fail(function(error){
                    // show error message
                    $ionicLoading.hide();
                });
            }
		}
        
        function startProcess(idx, deff){
        	
        	var defer = deff || $.Deferred();
        	var index = idx || 0;
			
			if(index > finishProcess.length-1){
				defer.resolve();
				return;
			}
            
			currentStep = index + 1;
			if(finishProcess[index].success){
				startProcess(index+1, defer);
			}else{
                setProcessStatus(index,false,true,false, false);
				finishProcess[index].process().then(function(){
                    setProcessStatus(index,true,false,true, false);
					startProcess(index+1, defer);
				}).fail(function(error){
                    setProcessStatus(index,false,false,true, true);
                    WL.Logger.error("[finishVisitProgressService][startProcess] username: " + userContext.getUsername() + " " + JSON.stringify(error));
					defer.reject(error);
				});
			}
			
			return defer.promise();
		}
    
        function uploadPDF(){
        	var defer = $.Deferred();
			
			pdfGenerationHandler.setPdfContext(obj);
            pdfGenerationHandler.uploadPDF(obj.visitId).then(function(){
                finishProcess[1].generated = true;
                defer.resolve();
            }).fail(function(error){
                defer.reject(error);
                WL.Logger.error("[finishVisitProgressService][uploadPDF][pdfGenerationHandler.uploadPDF] username: " + userContext.getUsername() + " " + JSON.stringify(error));
            });
			
			return defer.promise();
        }
        
        function uploadPDFOffline(){
        	
            
            var defer = $.Deferred();
            
            pdfModule.uploadPendingPDF(obj.visitId).then(function(){
                defer.resolve();
            }).fail(function(error){
                defer.reject(error);
                WL.Logger.error("[finishVisitProgressService][uploadPDFOffline][pdfModule.uploadPendingPDF] username: " + userContext.getUsername() + " " + JSON.stringify(error));
            });
            
            return defer.promise();
        }
		
        /*
		 *   ARHIVE UPLOAD
		 */
		
		function uploadPhotoArhive(){
			var defer = $.Deferred();
			
			archiveModule.finishVisit(obj.visitId).then(function(zipPath){
                finishProcess[0].path = zipPath;
				defer.resolve();
			}).fail(function(error){
				defer.reject(error);
                WL.Logger.error("[finishVisitProgressService][uploadPhotoArhive][archiveModule.finishVisit] username: " + userContext.getUsername() + " " + JSON.stringify(error));
			});
			
			return defer.promise();
		}
        
        function uploadPhotoArhiveOffline(){
            var defer = $.Deferred();
            
            archiveModule.finishPendingVisit(obj.visitId).then(function(zipPath){
                finishProcess[0].path = zipPath;
                defer.resolve();
            }).fail(function(error){
                defer.reject(error);
                WL.Logger.error("[finishVisitProgressService][uploadPhotoArhiveOffline][archiveModule.finishPendingVisit] username: " + userContext.getUsername() + " " + JSON.stringify(error));
            })
            
            return defer.promise();
        }
		
        /*
        * MERC+ CHNAGES
        */
		function updateDatabase(){
            var defer = $.Deferred();
            
            if (obj.finishedWorkOrders.length > 0) {
            	auditService.finishAudit(obj.visitId, obj.finishedWorkOrders, finishProcess[0].path).then(function(wasCanceled){
                    if (typeof callback === 'function') {
                           callback(wasCanceled, obj.visitId);
                    }
                   defer.resolve();
               }).fail(function(error){
                   defer.reject(error);
                   WL.Logger.error("[finishVisitProgressService][updateDatabase][auditService.finishAudit] username: " + userContext.getUsername() + " " + JSON.stringify(error));
               });
            } else {
            	auditService.cancelAudit(obj.visitId).then(function(){
                     if (typeof callback === 'function') {
                            callback(true, obj.visitId)
                        } 
                        defer.resolve();
                }).fail(function(error){
                    defer.reject(error);
                    WL.Logger.error("[finishVisitProgressService][databaseOffline][auditService.cancelAudit] username: " + userContext.getUsername() + " " + JSON.stringify(error));
                });
            }

            return defer.promise();
		}
        
        function databaseOffline() {
            var defer = $.Deferred();
            
            if (obj.workOrders.length > 0) {
                auditService.finishVisit(obj.workOrders, obj.sqlDate, finishProcess[0].path, obj.workOrderParts, obj.shopCode, obj.photosToSave, obj.visitId).then(function(){
                  if (typeof callback === 'function') {
                        callback(obj.visitId).then(function (){
                            defer.resolve();
                        })
                } else{
                    defer.resolve();
                }  
                }).fail(function(error){
                    defer.reject(error);
                    WL.Logger.error("[finishVisitProgressService][databaseOffline][auditService.finishVisit] username: " + userContext.getUsername() + " " + JSON.stringify(error));
                });
            } else {
                // Cancel visit
                auditService.cancelAudit(obj.visitId).then(function(){
                     if (typeof callback === 'function') {
                            callback(obj.visitId)
                        } 
                        defer.resolve();
                }).fail(function(error){
                    defer.reject(error);
                    WL.Logger.error("[finishVisitProgressService][databaseOffline][auditService.cancelAudit] username: " + userContext.getUsername() + " " + JSON.stringify(error));
                });
            }
            
            return defer.promise();

        }
        
        function retry(){
            
            startProcess().then(function(){
                console.log("All Good");
            }).fail(function(error){
                WL.Logger.error("[finishVisitProgressService][retry][startProcess] username: " + userContext.getUsername() + " " + JSON.stringify(error));
            });
        }
        
        function getFinishProcessObject(){
            return finishProcess;
        }
        
        function setProcessStatus(index, success, processing, processed, failed){
            finishProcess[index].success = success;
            finishProcess[index].processing = processing;
            finishProcess[index].processed = processed;
            finishProcess[index].failed = failed;
            
            $rootScope.$broadcast('valueChanged');
        }
        
        function resetFinishProcessData(){
			obj = {};
            currentStep = 1
			finishProcess = [
				{ success : false , processing : false, processed : false, failed : false, process : uploadPDF, generated : false }, 
				{ success : false , processing : false, processed : false, failed : false, process : uploadPhotoArhive, path : '' },
				{ success : false , processing : false, processed : false, failed : false, process : updateDatabase }
		]
		}
        
        function buildFinishDate() {
            var d = new Date();
            var dateString = ("00" + d.getDate()).slice(-2) + "/" +
                ("00" + (d.getMonth() + 1)).slice(-2) + "/" +
                d.getFullYear() + " " +
                ("00" + d.getHours()).slice(-2) + ":" +
                ("00" + d.getMinutes()).slice(-2)
            return dateString
        }
        
        function setCallbackFunction(callbackFunction){
            callback = callbackFunction;
        }
        
        function setOnlineProcesses(){
            resetFinishProcessData();
            finishProcess[1].process = uploadPDF;
            finishProcess[0].process = uploadPhotoArhive;
            finishProcess[2].process = updateDatabase;
        }
        
        function setOfflineProcesses(){
            resetFinishProcessData();
            finishProcess[1].process = uploadPDFOffline;
            finishProcess[0].process = uploadPhotoArhiveOffline;
            finishProcess[2].process = databaseOffline;
        }
        
        function getCurrentStep(){
            return currentStep;
        }
        
        
	}
})();