/**
 * Jan, 11 2017
 * pendingAuditsService.js
 * Stavarache Vlad
*/

/**
 * PendingAuditsService Factory
 * @namespace Factories
 */
(function(){
	'use strict';

	audit
    .factory('PendingAuditsService', PendingAuditsService);

	PendingAuditsService.$inject = ['$ionicLoading', 'multiModal', '$rootScope', 'auditService', '$localDB', 'startedAudit', 'GalleryExportService', 'archiveModule', 'ToastService', 'AuditPollingService', '$location','finishVisitProgressService'];

	/**
     * @namespace PendingAuditsService
     * @desc Service used to work with pending visits (visits that have been finished offline)
	 * Based on a queue logic where pendingVisitsQueue collection is used as the model 
     * @memberOf Factories
     */
	function PendingAuditsService($ionicLoading, multiModal, $rootScope, auditService, $localDB, startedAudit, GalleryExportService, archiveModule, ToastService, AuditPollingService, location ,finishVisitProgressService) {
        
		var service = {
			savePendingVisit : savePendingVisit,
			runVisitQueue : runVisitQueue,
			getPendingVisitsCount : getPendingVisitsCount
		};
		
		return service;
        
		/**
	     * @name runVisitQueue
	     * @desc Goes through all the visits in the pendingVisitsQueue collection
		 * and uses the info stored in the pendingAuditWorkOrders and pendingAuditWorkOrderParts collections,
		 * locally stored pdfs and photos in order to finish the visit on the server
	     * @param {Bool} someVisitsSynced
		 * @param {Promise} dfd - promise that is passed along in order to call 
		 * the visit recursively
	     * @returns {Promise}
	     * @memberOf Factories.PendingAuditsService
	     */
        function runVisitQueue(someVisitsSynced, dfd) {
			var defer = dfd || $.Deferred();
			var visitsSynced = someVisitsSynced;
			var visitCheck = null;

			_getNextVisitInLineFromQueue().then(function(visitId){
				if (visitId === null) {
					visitCheck = visitId;
					return completeFinishVisit(true, someVisitsSynced, defer);
				} else {
					return _finishPendingVisit(visitId, defer);
				}
			}).fail(function(error){
				getPendingVisitsCount().then(function(num){
					hybrid.setNoUnsyncedCommands(num);
				});
				if(window.location.hash.localeCompare("#/logOutTab")==0	) {
					defer.reject(error, visitsSynced);
					WL.Logger.error("[PendingAuditService][runVisitQueue][logOutTab] username: " + userContext.getUsername() + " " +JSON.stringify(error));
					setTimeout(function(){
						ToastService.showNotificationMsg('Synchronisation Failed');
					}, 5000);

					}
				else {
					WL.Logger.error("[PendingAuditService][runVisitQueue][Sync Failed] username: " + userContext.getUsername() + " " +JSON.stringify(error));
					ToastService.showNotificationMsg('Synchronisation Failed');
					defer.reject(error, visitsSynced);
				}

			});

			return defer.promise();
		}
        
        function completeFinishVisit(queueFinished, someVisitsSynced, dfd) {
            var defer = dfd || $.Deferred();
            
            console.log("Complete Finish done");
            
            if (!queueFinished) {
                runVisitQueue(someVisitsSynced, dfd);
            } else {
                // update native header count set to 0
                hybrid.setNoUnsyncedCommands(0);
                // notificationToast
                if (window.location.hash.localeCompare("#/logOutTab") == 0) {

                    AuditPollingService.startPollIfNeeded();
                    defer.resolve(someVisitsSynced);
                    setTimeout(function () {
                        if(someVisitsSynced) { ToastService.showNotificationMsg('Synchronisation Complete'); }
                    }, 5000);
                } else {
                	if(someVisitsSynced) { ToastService.showNotificationMsg('Synchronisation Complete'); }
                    AuditPollingService.startPollIfNeeded();
                    defer.resolve(someVisitsSynced);
                }
            }
            
            return defer.promise();
        }
        
		/**
	     * @name _finishPendingVisit
	     * @desc Can only be called within the runVisitQueue function
		 * Finishes a pending visit
		 * Interacts with the finishVisitProgressService
	     * @param {String} visitId
		 * @param {Promise} dfd - promise that is passed along in order to call 
		 * the visit recursively
	     * @returns {Promise}
	     * @memberOf Factories.PendingAuditsService
	     */
        function _finishPendingVisit(visitId, dfd) {
			var defer = $.Deferred();
			var workOrders;
			var workOrderParts;
			var shopCode;
			var photosToSave;
			var photosZipPath;
			var sqlDate = dateToSQLDateTime(new Date());
            
            var $finishVisitModal = multiModal.setBasicOptions('notification', 'finishVisit', null, 'popup');
            $finishVisitModal = multiModal.setTemplateFile($finishVisitModal, 'templates/components/finishVisitModal.html');

			// Check if visit has already been finished
			auditService.checkIfVisitFinished(visitId).then(function(isFinished){
				if (isFinished) {
					// This means that finishAudit has been successful but updating the local data in the model was not
					// We need to clear data in pending collections regarding this visit
					GalleryExportService.removePersistentFile(visitId + ".zip").then(function(){
						_clearPendingVisitData(visitId).then(function(){
							defer.resolve(false, true);
						}).fail(function(error){
							WL.Logger.error("[PendingAuditService][_finishPendingVisit][_clearPendingVisitData] username: " + userContext.getUsername() + " " + JSON.stringify(error));
							defer.reject(error)
						});
					}).fail(function(error){
						WL.Logger.error("[PendingAuditService][_finishPendingVisit][GalleryExportService.removePersistentFile] username: " + userContext.getUsername() + " " +JSON.stringify(error));
						defer.reject(error)
					});
				} else {
					// We need to finish the visit
					_getPendingWorkOrdersForFinishVisit(visitId).then(function(WOs){
						workOrders = WOs;
						if (WOs && WOs.length > 0) {
							shopCode = WOs[0].shopcode;
							return _getPendingWorkOrderPartsForFinishVisit(visitId);
						}
					}).then(function(WOParts){
						workOrderParts = WOParts;
						return $localDB.find("galleryPhoto",{visitId : visitId},{});
					}).then(function(photos){
						photosToSave = photos;
                        console.log("workOrders", workOrders);
                        if (workOrders.length > 0) {
                            multiModal.openModal($finishVisitModal, $rootScope).then(function () {
                                finishVisitProgressService.setCallbackFunction(pendingCallback);
                                multiModal.getCurrentInstance().steps = finishVisitProgressService.getFinishProcessObject();

                                multiModal.getCurrentInstance().finish = function () {
                                    multiModal.closeCurrentModal();
                                    completeFinishVisit(false, true, dfd);
                                }

                                multiModal.getCurrentInstance().retry = function () {
                                    finishVisitProgressService.retry();
                                }

                                multiModal.getCurrentInstance().cancel = function () {
                                    hybrid.stopSync();
                                    finishVisitProgressService.resetFinishProcessData();
                                    multiModal.closeCurrentModal();
                                }

                                multiModal.getCurrentInstance().$on('valueChanged', function () {
                                    multiModal.getCurrentInstance().$evalAsync(function () {
                                        multiModal.getCurrentInstance().steps = finishVisitProgressService.getFinishProcessObject();
                                        multiModal.getCurrentInstance().currentStep = finishVisitProgressService.getCurrentStep();
                                    });
                                });

                                finishVisitProgressService.startFinishVisitOffline(workOrders, sqlDate, workOrderParts, shopCode, photosToSave, visitId);
                            }).fail(function (error) {
                                defer.reject(error);
                                WL.Logger.error("[PendingAuditsService][_finishPendingVisit][openModal] username: " + userContext.getUsername() + " " + JSON.stringify(error));
                            });
                        } else {
                            var callback = pendingCallback.bind(null, visitId, dfd);
                            finishVisitProgressService.setCallbackFunction(callback);
                            finishVisitProgressService.startFinishVisitOffline(workOrders, sqlDate, workOrderParts, shopCode, photosToSave, visitId);
                        }
					}).fail(function (error) {
                        WL.Logger.error("[PendingAuditsService][_finishPendingVisit] username: " + userContext.getUsername() + " " + JSON.stringify(error));
                        defer.reject(error);
                    })
				}
			}).fail(function(error){
				WL.Logger.error("[PendingAuditsService][_finishPendingVisit] username: " + userContext.getUsername() + '[AuditAdapter]' + " " + JSON.stringify(error));
				defer.reject(error);
			});


			return defer.promise();
		}
        
        function pendingCallback(visitId, cancelDefer) {
            
            var defer = $.Deferred();
            GalleryExportService.removePersistentFile(visitId + ".zip")
                .then(function () {
                    return _clearPendingVisitData(visitId);
                }).then(function () {
                    defer.resolve(false, true);
                    if (cancelDefer) {
                        completeFinishVisit(false, true, cancelDefer);
                    }
                }).fail(function (error) {
                    WL.Logger.error("[PendingAuditsService][pendingCallback][GalleryExportService.removePersistentFile] username: " + userContext.getUsername() + " " + JSON.stringify(error));
                    defer.reject(error);
            });
            
            return defer.promise();
            
        }

		/**
	     * @name savePendingVisit
	     * @desc Called when finishing a visit offline
		 * Copies the visit data in the corresponding pending collections
		 * Erases all the associated started visit collections data
	     * @param {N/A}
	     * @returns {Promise}
	     * @memberOf Factories.PendingAuditsService
	     */
		function savePendingVisit() {
			var defer = $.Deferred();

			// Start a transaction!

			WL.JSONStore.startTransaction()
			.then(function(){
				return _getStartedVisitData();
			}).then(function(visitInfo){
				console.log("visitInfo", visitInfo);
				return _savePendingVisitData(visitInfo)
			}).then(function(){
				return _resetPhotosCounter()
			}).then(function(){
				return _changeStartedVisitsToPending();
			}).then(function(){
				// this will get executed anyway.
				return startedAudit.eraseStartedVisitData();
			}).then(function(){
				return WL.JSONStore.commitTransaction();
			}).then(function(){
				getPendingVisitsCount().then(function(num){
					hybrid.setNoUnsyncedCommands(num);
				});
				defer.resolve();
			}).fail(function(error){
				WL.Logger.error("[PendingAuditService][savePendingVisit] username: " + userContext.getUsername() +" "+ JSON.stringify(error));
				WL.JSONStore.rollbackTransaction()
				.then(function(){
					WL.Logger.error("[PendingAuditService][savePendingVisit][Rollback] username: " + userContext.getUsername() + ' Rollback with success');
					defer.reject();
				})
				.fail(function (){
					// How to handle rollback failure, just retry?
					WL.Logger.error("[PendingAuditService][savePendingVisit][Rollback] username: " + userContext.getUsername() + ' Rollback failed!');
					defer.reject();
				})
			});

			return defer.promise();
		}

		function _changeStartedVisitsToPending() {
			var defer = $.Deferred();

			$localDB.find('visit', {auditStatus : 'Started'}, {exact : true}).then(function(startedWorkOrders){
				var pendingWorkOrders = startedWorkOrders.map(function(workOrder){
					workOrder.json.auditStatus = "Pending";
					return workOrder;
				});
				$localDB.replace('visit', pendingWorkOrders, {}).then(function(){
					defer.resolve();
				}).fail(function(){
					defer.reject(error);
				});
			}).fail(function(){
				defer.reject(error);
			});

			return defer.promise();
		}

		function _getStartedVisitData() {
			var defer = $.Deferred();

			var response = {}; // visitId, workOrders, workOrderParts

			startedAudit.getStartedVisit()
			.then(function(visitId){
				response.visitId = visitId;
				return startedAudit.getUnfilteredStartedWorkOrders(visitId)
			})
			.then(function(workOrders){
				response.workOrders = workOrders;
				return startedAudit.getUnfilteredAuditParts()
			})
			.then(function(woParts){
				response.workOrderParts = woParts;
				console.log("response", response);
				defer.resolve(response);
			}).fail(function(error){
				console.log("error", error);
				defer.reject(error);
			});

			return defer.promise();
		}

		function _savePendingVisitData(visitData) {
			var defer = $.Deferred();
			var visitId = visitData.visitId;
			var workOrders = visitData.workOrders;
			var workOrderParts = visitData.workOrderParts;

			_copyStartedWorkOrdersToPendingWorkOrders(visitId, workOrders).then(function(){
				return _copyStartedWorkOrderPartsToPendingWorkOrderParts(visitId, workOrderParts);
			}).then(function(){
				return _addPendingVisitToQueue(visitId);
			}).then(function(){
				// Save visit photos locally in a zip file
				return GalleryExportService.exportPersistentZipToDisk(visitId + ".zip");
			}).then(function(){
				defer.resolve();
			}).fail(function(error){
				defer.reject(error);
			});

			return defer.promise();
		}

		function _clearPendingVisitData(visitId) {
			var dfd = $.Deferred();
			var options = {exact: true};

			WL.JSONStore.startTransaction()
			.then(function(){
				return WL.JSONStore.get("pendingAuditWorkOrders").remove({visitId: visitId}, options);
			}).then(function(){
				return WL.JSONStore.get("pendingAuditWorkOrderParts").remove({visitId: visitId}, options);
			}).then(function(){
				return WL.JSONStore.get("pendingVisitsQueue").remove({visitId: visitId}, options);
			}).then(function(){
				return WL.JSONStore.get("galleryPhoto").remove({visitId: visitId}, options);
			}).then(function(){
				return WL.JSONStore.commitTransaction();
			}).then(function(){
				return GalleryExportService.removePhotosForVisit(visitId);
			}).then(function(){
				dfd.resolve();
			}).fail(function (errorObject){
				WL.JSONStore.rollbackTransaction()
				.then(function(){
					dfd.reject(errorObject);
				})
				.fail(function (){
					dfd.reject(errorObject);
				})
			});

			return dfd.promise();

		}

		function _getPendingWorkOrdersForFinishVisit(visitId) {
			var defer = $.Deferred();

			var options = {
				filter: ['visitId', 'inspectionStatus', 'workOrderId', 'containerNo', 'shopCode', 'visitDesc', 'auditResult','finalComment', 'repairQuality', 'statusCode'],
				exact: true
			};

			WL.JSONStore.get('pendingAuditWorkOrders').find({visitId: visitId, inspectionStatus: 'Finished'}, options).then(function(finishedWorkOrders){
				defer.resolve(finishedWorkOrders);
			}).fail(function(error){
				WL.Logger.error("[PendingAuditService][_getPendingWorkOrdersForFinishVisit] username: " + userContext.getUsername() + " " + JSON.stringify(error));
				defer.reject(error);
			});

			return defer.promise();
		}

		function _getPendingWorkOrderPartsForFinishVisit(){
			var defer = $.Deferred();

			var options = {
				filter: ['visitId', 'accepted', 'workOrderId', 'woRepair',
				         'partIndex', 'repairIndex', 'repairDescription',
				         'preTPI', 'postTPI', 'prePartCode', 'postPartCode',
				         'preRepairCode', 'postRepairCode', 'preQuantity', 'postQuantity',
				         'preDamageCode', 'postDamageCode', 'preRepairLocCode', 'postRepairLocCode',
				         'manHrs', 'partCost', 'materialCost', 'totalPerCode', 'partDescription', 'rejectedPart', 'repairQuality']
			};

			WL.JSONStore.get('pendingAuditWorkOrderParts').find({}, options).then(function(data){
				defer.resolve(data);
			}).fail(function(error){
				WL.Logger.error("[PendingAuditService][_getPendingWorkOrderPartsForFinishVisit] username: " + userContext.getUsername() + " " + JSON.stringify(error));
				defer.reject(error);
			})

			return defer.promise();
		}

		function _copyStartedWorkOrdersToPendingWorkOrders(visitId, workOrders) {
			var defer = $.Deferred();
			var workOrdersToAdd = [];

			_removePendingWorkOrdersByVisitId(visitId)
			.then(function(){
				workOrdersToAdd = workOrders.map(function(jsonWorkOrder){
					return jsonWorkOrder.json;
				});
				return _addPendingWorkOrders(workOrdersToAdd);
			}).then(function(){
				defer.resolve();
			}).fail(function(error){
				defer.reject(error);
			});

			return defer.promise();
		}

		function _removePendingWorkOrdersByVisitId(visitId) {
			var defer = $.Deferred();

			_getPendingWorkOrdersByVisitId(visitId).then(function(workOrders){
				if (workOrders.length > 0) {
					return $localDB.erase("pendingAuditWorkOrders", workOrders, {}).then(function(){
						defer.resolve();
					});
				} else {
					// Nothing to erase
					defer.resolve();
				}
			}).fail(function(error){
				defer.reject(error);
			});

			return defer.promise();
		}

		function _addPendingWorkOrders(workOrders) {
			var defer = $.Deferred();

			$localDB.add('pendingAuditWorkOrders', workOrders, {}).then(function(){
				defer.resolve(workOrders);
			}).fail(function(error){
				defer.reject(error);
			});

			return defer.promise();
		}

		function _resetPhotosCounter() {
			var defer = $.Deferred();

			$localDB.find("utils",{},{}).then(function(data){
				if (data.length > 0) {
					data[0].json.photoCount = 0;
					return $localDB.replace("utils",data[0],{});
	 			} else {
	 				defer.resolve();
	 			}
			}).then(function(){
				defer.resolve();
			}).fail(function(error){
				defer.reject(error);
			});

			return defer.promise();
		}

		function _getPendingWorkOrdersByVisitId(visitId) {
			var defer = $.Deferred();

			$localDB.find("pendingAuditWorkOrders", {visitId : visitId}, {exact : true}).then(function(workOrders){
				defer.resolve(workOrders);
			}).fail(function(error){
				defer.reject(error);
			});

			return defer.promise();
		}

		function _copyStartedWorkOrderPartsToPendingWorkOrderParts(visitId, workOrderParts) {
			var defer = $.Deferred();
			var workOrderPartsToAdd = [];

			_removePendingWorkOrderPartsByVisitId(visitId)
			.then(function(){
				workOrderPartsToAdd = workOrderParts.map(function(jsonWorkOrdePart){
					return jsonWorkOrdePart.json;
				});
				return _addPendingWorkOrderParts(workOrderPartsToAdd);
			}).then(function(){
				defer.resolve();
			}).fail(function(error){
				defer.reject(error);
			});

			return defer.promise();
		}

		function _getPendingWorkOrderPartsByVisitId(visitId) {
			var defer = $.Deferred();

			$localDB.find("pendingAuditWorkOrderParts", {visitId : visitId}, {exact : true}).then(function(workOrderParts){
				defer.resolve(workOrderParts);
			}).fail(function(error){
				defer.reject(error);
			});

			return defer.promise();
		}

		function _removePendingWorkOrderPartsByVisitId(visitId) {
			var defer = $.Deferred();

			_getPendingWorkOrderPartsByVisitId(visitId).then(function(workOrderParts){
				if (workOrderParts.length > 0) {
					return $localDB.erase("pendingAuditWorkOrderParts", workOrderParts, {}).then(function(){
						defer.resolve();
					});
				} else {
					// Nothing to erase
					defer.resolve();
				}
			}).fail(function(error){
				defer.reject(error);
			});

			return defer.promise();
		}

		function _addPendingWorkOrderParts(workOrderParts) {
			var defer = $.Deferred();

			$localDB.add('pendingAuditWorkOrderParts', workOrderParts, {}).then(function(){
				defer.resolve(workOrderParts);
			}).fail(function(error){
				defer.reject(error);
			});

			return defer.promise();
		}

		function _getPendingVisitFromQueue(visitId) {
			var defer = $.Deferred();

			$localDB.find("pendingVisitsQueue", {visitId : visitId}, {exact : true}).then(function(visitData){
				defer.resolve(visitData);
			}).fail(function(error){
				defer.reject(error);
			});

			return defer.promise();
		}

		function _removePendingVisitFromQueue(visitId) {
			var defer = $.Deferred();

			_getPendingVisitFromQueue(visitId).then(function(visitData){
				if (visitData.length > 0) {
					return $localDB.erase("pendingVisitsQueue", visitData, {}).then(function(){
						defer.resolve();
					});
				} else {
					// Nothing to erase
					defer.resolve();
				}
			}).fail(function(error){
				defer.reject(error);
			});

			return defer.promise();
		}

		function _addPendingVisitToQueue(visitId) {
			var defer = $.Deferred();

			var pendingVisitInfo = {visitId: visitId, timestamp: new Date().getTime()};

			_removePendingVisitFromQueue(visitId).then(function(){
				return $localDB.add('pendingVisitsQueue', [pendingVisitInfo], {})
			}).then(function(){
				defer.resolve();
			}).fail(function(error){
				defer.reject(error);
			});

			return defer.promise();
		}

		function _getNextVisitInLineFromQueue() {
			var defer = $.Deferred();
			// returns a visitId or null if nothing is found

			$localDB.find("pendingVisitsQueue", {}, {}).then(function(visitArray){
				if (visitArray.length > 0) {
					var sortedVisits = visitArray.sort(function(a, b){
						return a.json.timestamp - b.json.timestamp
					});

					var visitId = visitArray[0].json.visitId;

					console.log("next visit in line", visitId);
					defer.resolve(visitId);
				} else {
					defer.resolve(null);
				}
			}).fail(function(error){
				WL.Logger.error("[PendingAuditService][_getNextVisitInLineFromQueue] username: " + userContext.getUsername() + " " + JSON.stringify(error));
				defer.reject(error);
			});

			return defer.promise();
		}

		function dateToSQLDateTime(date) {
			if (isNaN(date.getFullYear()))
				return;

			return date.getFullYear() + '-'
					+ ('00' + (date.getMonth() + 1)).slice(-2) + '-'
					+ ('00' + date.getDate()).slice(-2) + ' '
					+ ('00' + date.getHours()).slice(-2) + ':'
					+ ('00' + date.getMinutes()).slice(-2) + ':'
					+ ('00' + date.getSeconds()).slice(-2);
		}

		function getPendingVisitsCount() {
			var defer = $.Deferred();

			WL.JSONStore.get("pendingVisitsQueue").count({}, {}).then(function(count){
				defer.resolve(count);
			}).fail(function(error){
				defer.reject(error);
			});

			return defer.promise();
		}
	}
})();