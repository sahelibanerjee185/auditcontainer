/**
 * Dec, 2 2016
 * AuditPollingService.js
 * Stavarache Vlad
 */

/**
 * AuditPollingService Factory
 * @namespace Factories
 */
(function(){
	'use strict';
	
	audit
    .factory('AuditPollingService', AuditPollingService);
	
	AuditPollingService.$inject = ['$ionicLoading', '$rootScope', '$location', 'ToastService', '$kpiModule', '$localDB'];
	
	/**
     * @namespace AuditPollingService
     * @desc Service that checks database job successful completion for a visit and provides user feedback
     * @memberOf Factories
     */
	function AuditPollingService($ionicLoading, $rootScope, $location, ToastService, $kpiModule, $localDB) {
		
		var timer;
		var intervalDuration = 15000; // 15 sec
		var pollingInProgress = false;
		
		var service = {
			add : add,
			inProgress : inProgress,
			checkVisitProcessingStatus : checkVisitProcessingStatus,
			startPollIfNeeded : startPollIfNeeded
		};
		
		return service;
		
		function inProgress() {
			return pollingInProgress;
		}
		
		/**
	     * @name setActivePollLocally
	     * @desc Adds the visit id of the visit to be polled in json store
		 * Serves as the model for the service
	     * @param {String} visitId
	     * @returns N/A - has side effects
	     * @memberOf Factories.AuditPollingService
	     */
		function setActivePollLocally(visitId) {
			var defer = $.Deferred();
			
			$localDB.add("visitPolling", [{visitId : visitId}], {}).then(function(){
				defer.resolve();
			}).fail(function(error){
				defer.reject(error);
			});
			
			return defer.promise();
		}
		
		function getLocalVisitIdsForPoll() {
			var defer = $.Deferred();
			var visitIds = []; // If nothing is stored locally return null
			
			$localDB.find('visitPolling', {}, {})
			.then(function(pollData) {
				angular.forEach(pollData, function(visitInfo){
					visitIds.push(visitInfo.json.visitId); 
				});
				
				defer.resolve(visitIds);
			})
			
			return defer.promise();
		}
		
		/**
	     * @name startPollIfNeeded
	     * @desc Checks 'visitPolling' collection model in json store 
		 * and starts polling if it finds any entries 
	     * @param {N/A}
	     * @returns {Promise}
	     * @memberOf Factories.AuditPollingService
	     */
		function startPollIfNeeded() {
			var defer = $.Deferred();
			
			if (pollingInProgress) {
				defer.resolve();
				return defer.promise();
			}
			
			getLocalVisitIdsForPoll().then(function(visitIds){
				if (visitIds && visitIds.length > 0) {
					hybrid.startSync();
					pollingInProgress = true;
					checkVisitProcessingStatus(visitIds);
				} else {
					defer.resolve();
				}
			});
			
			return defer.promise();
		}
		
		function clearLocalPollingData() {
			var defer = $.Deferred();
			
			$localDB.clearCollection("visitPolling").then(function(){
				defer.resolve();
			});
			
			return defer.promise();
		}
		
		function add(visitId) {
			return setActivePollLocally(visitId);
		}
		
		/**
	     * @name startTimer
	     * @desc Called when function called for polling is not yet completed
		 * waits for intervalDuration and starts polling again 
	     * @param {Array} visitIds - 
	     * @returns N/A - has side effects
	     * @memberOf Factories.AuditPollingService
	     */
		function startTimer(visitIds) {
			clearTimeout(timer);
			
			timer = setTimeout(function(){
				hybrid.startSync();
				checkVisitProcessingStatus(visitIds);
			}, intervalDuration)
		}
		
		function stop() {
			clearTimeout(timer);
			pollingInProgress = false;
			hybrid.stopSync();
		}
		
		/**
	     * @name checkVisitProcessingStatus
	     * @desc Calls the statusesForFinishedVisitsSync adapter method
		 * and checks if the work order of all the visits passed
		 * have been processed by the database job
		 * is called again if at least one of the work orders is not processed
		 * else polling is considered completed
	     * @param {Array} visitIds - array of visit id strings
	     * @returns {Promise} - value resolved is an object such as:
		 * {
		 * 	failedCount: 1,
		 * 	processedCount: 2,
		 * 	successfulCount: 1,
		 * 	totalCount: 2
		 * }
	     * @memberOf Factories.AuditPollingService
	     */
		function checkVisitProcessingStatus(visitIds){
			var defer = $.Deferred();
			
			WL.Client.invokeProcedure({
				adapter : 'AuditAdapter',
				procedure : 'statusesForFinishedVisitsSync',
				parameters : [userContext.getUsername(), userContext.getToken(), visitIds, userContext]
			}, {
				onSuccess : function(response){
					successHandler(defer, response, visitIds);
				},
				onFailure: function(error){
					console.log("failed status", error);
					failHandler(defer, error, visitIds)
				}
			});		  
			return defer.promise(); 
		}
		
		function successHandler(defer, response, visitIds) {
			if (response && response.invocationResult && response.invocationResult.isSuccessful) {
				var result = response.invocationResult.invocationResult;
				// Check if all work orders are processed
				
				if (result.processedCount != result.totalCount) {
					failHandler(defer, response, visitIds);
				} else {
					// Show notification and send to finish visit screen
					stop();
					$kpiModule.getKpiFromServer().then(function(){
						showToastMessage(result);
					}).fail(function(){
						showToastMessage(result);
					});
					
					defer.resolve();
				}
			} else {
				failHandler(defer, response, visitIds);
			}
		}
		
		function showToastMessage(result) {
			clearLocalPollingData().then(function(){
				if(window.location.hash.localeCompare("#/logOutTab")==0){
					setTimeout(function(){ 
					if (result.failedCount > 0) {
						if(result.visitCounter>1 )
							ToastService.showWarningMessagePlural();
						else
						ToastService.showWarningMessage();
					} else {
						if(result.visitCounter>1 )
							ToastService.showSuccessMessagePlural();
						else
							ToastService.showSuccessMessage();
					}
					}, 5000);
					
				}
				else {
					if (result.failedCount > 0) {
						if(result.visitCounter>1 )
							ToastService.showWarningMessagePlural();
						else
						ToastService.showWarningMessage();
					} else {
						if(result.visitCounter>1 )
							ToastService.showSuccessMessagePlural();
						else
							ToastService.showSuccessMessage();
					}
				}
					
				
			})
		}
		
		function failHandler(defer, error, visitIds) {
			hybrid.stopSync();
			startTimer(visitIds);
			defer.reject(error);
		}

	}
})();