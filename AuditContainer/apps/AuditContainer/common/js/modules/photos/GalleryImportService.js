/**
 * @namespace GalleryImportService
 * @desc Service used to manage the import of photos in visits from archive
 * @memberOf Factories
 */
(function (audit, userContext, $) {
    //'use strict';

    audit.factory('GalleryImportService', GalleryImportService);

    GalleryImportService.$inject = ['$localDB'];

    function GalleryImportService($localDB) {
        var exports = {
            unzip: unzip,
        };

        return exports;
        
    }

    /**
	* @name unzip
	* @desc Unzip a specified file to an output directory 
    * @param {String} fileName name of the file to be unzipped
    * @param {String} outputDirectory path to directory to unzip the content
    * @param {Function} callback to be called when the process is finished
    * @param {Function} progressCallback to be called to check progress 
    * @returns {Void} 
    * @memberOf Factories.GalleryExportService
    */
    function unzip(fileName, outputDirectory, callback, progressCallback) {
    	
        var win = function(result) {
            if (result && typeof result.loaded != "undefined") {
                if (progressCallback) {
                    return progressCallback(newProgressEvent(result));
                }
            } else if (callback) {
                callback(0);
            }
        };
        var fail = function(result) {
        	console.log("fail")
            if (callback) {
                callback(-1);
            }
        };
        cordova.exec(win, fail, 'ZipPlugin', 'unzip', [fileName, outputDirectory]);
        
        
        
        function newProgressEvent(result) {
            var event = {
                    loaded: result.loaded,
                    total: result.total
            };
            return event;
        }
    }
})(audit, userContext, $);