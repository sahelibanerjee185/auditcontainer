
    //'use strict';

    audit.factory('PhotoStorageDirectory', PhotoStorageDirectory);

    PhotoStorageDirectory.$inject = [];

    function PhotoStorageDirectory() {
	    var data = {
				visitId : "1",
				woNo 	: "1212"
		}
		
		
	    var exports = {
	        getVisitId: getVisitId,
	        setVisitId : setVisitId,
	        setWoNo : setWoNo,
	        getWoNo: getWoNo,
	        getFullPath : getFullPath,
	        getRelativePath : getRelativePath,
	        getFolderName : getFolderName,
	        getDocumentsFolder:getDocumentsFolder
	    };
	
	    return exports;
	    
	    function getFullPath() {
	    	return cordova.file.applicationStorageDirectory + 
				"/Documents/" + 
				data.visitId + '/' +
				data.woNo +
				'/';
	    }
	    
	    function getDocumentsFolder() {
	    	return cordova.file.applicationStorageDirectory + 
				"/Documents/" ;
	    }
	    
	    function getRelativePath() {
	    	return "/Documents/" + 
		        	data.visitId + '/' +
					data.woNo +
				    '/';
	    }
	    
	    function getFolderName() {
	    	return data.visitId + '/' +
				   data.woNo +'/';
	    }
	    
	    function getVisitId (){
	    	return data.visitId;
	    }
	    
	    function setVisitId(id){
	    	data.visitId = id;
	    }
	    
	    function getWoNo(){
	    	return data.woNo;
	    }
	    
	    function setWoNo(woNo){
	    	data.woNo = woNo;
	    }  

	}
