(function (audit, $) {

    'use strict';

    audit.factory('$galleryModule', Service);

    Service.$inject = ['$q', 'PhotoStorageDirectory'];

    function Service($q, PhotoStorageDirectory) {

    	var data = {
    			array : []
    	}
        
        var service = {
                getBase64Image			: getBase64Image,
                getData					: getData,
                addPhoto				: addPhoto,
                setData					: setData
        };
        
    	function getData(){
    		return data;
    	}

    	function setData (datatToBeSet){
    		data = datatToBeSet;
    	}

    	function addPhoto(data){
    		data.array.push(data);
    	}
        
        /*
		 * Get all the photos from locale storage
		 */
        function getBase64Image(dataObject, url){   // returns a promise
			var defer = $q.defer();
			console.log(url)
			var localePath = PhotoStorageDirectory.getFullPath();
			var counter = dataObject.length;
				window.resolveLocalFileSystemURL(url, 
					function(entry){
						entry.file(
							function(file){
								var reader = new FileReader();
								reader.onloadend = function(image) {
									for(index in dataObject){ 
										
										var componentsUrl = dataObject[index].taggedImages.json.path.split("/");
										if((localePath+""+componentsUrl[componentsUrl.length - 1]).localeCompare(url)==0){
											dataObject[index].base64 = image.target.result;
										}	
										
										defer.resolve(dataObject);
									}
								}
								reader.onerror = function(event) {
								    console.error("File could not be read! Code " + event.target.error.code);
								};
								reader.readAsDataURL(file);
							},
							function(){
								defer.reject('error1');
							});
					},
					function(){
						defer.reject('error2');
					});
			return defer.promise;
		}
        
        
        return service;

    }

})(audit, $);