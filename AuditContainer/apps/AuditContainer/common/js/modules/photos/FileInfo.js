(function (audit, userContext) {
    //'use strict';

    audit.factory('fileInfo', fileInfo);

    fileInfo.$inject = [];

    function fileInfo() {
    	
    	var data = {
    			visitId : null,
    			woNo 	: null
    	}
    	
    	
        var exports = {
            getVisitId: getVisitId,
            setVisitId : setVisitId,
            setWoNo : setWoNo,
            getWoNo: getWoNo,
            getFullPath : getFullPath,
            getRelativePath : getRelativePath,
            getFolderName : getFolderName
        };

        return exports;
        
        function getFullPath() {
        	return cordova.file.applicationStorageDirectory + 
				"/Documents/" + 
				data.visitId + '/' +
				data.woNo +
				'/';
        }
        
        function getRelativePath() {
        	return "/Documents/" + 
		        	data.visitId + '/' +
					data.woNo +
				    '/';
        }
        
        function getFolderName() {
        	return data.woNo+'/';
        }
        
        function getVisitId (){
        	return data.visitId;
        }
        
        function setVisitId(id){
        	data.visitId = id;
        }
        
        function getWoNo(){
        	return data.woNo;
        }
        
        function setWoNo(woNo){
        	data.woNo = woNo;
        }
        

	}
})(audit, userContext);