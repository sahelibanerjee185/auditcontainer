/**
 * Sept 07, 2015
 * Base64EncoderDecoder.js
 * BuzilaEugen
 */

audit.factory("$base64EncoderDecoder", function($localDB,$q,$nestedDirectory, PhotoStorageDirectory) {

var Base64Binary = {
	_keyStr : "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",
			
		getFilePath : function(fileName) {
			 return PhotoStorageDirectory.getFullPath() + fileName;
		},
		 
		getTarget : function() {
			 return "_system";
		},
		
		/* will return a  Uint8Array type */
		  decodeArrayBuffer: function(input){
			var bytes = (input.length/4) * 3;
			var ab = new ArrayBuffer(bytes);
			this.decode(input, ab);
			
			return ab;
		},

		  removePaddingChars : function(input){
			var lkey = this._keyStr.indexOf(input.charAt(input.length - 1));
			if(lkey == 64){
				return input.substring(0,input.length - 1);
			}
			return input;
		},

		decode : function(input, arrayBuffer) {
			//get last chars to see if are valid
			input = this.removePaddingChars(input);
			input = this.removePaddingChars(input);
			var bytes = parseInt((input.length / 4) * 3, 10);
			var uarray;
			var chr1, chr2, chr3;
			var enc1, enc2, enc3, enc4;
			var i = 0;
			var j = 0;
			
			if (arrayBuffer)
				uarray = new Uint8Array(arrayBuffer);
			else
				uarray = new Uint8Array(bytes);
			
			input = input.replace(/[^A-Za-z0-9\+\/\=]/g, "");
			
			for (i=0; i<bytes; i+=3) {	
				//get the 3 octects in 4 ascii chars
				enc1 = this._keyStr.indexOf(input.charAt(j++));
				enc2 = this._keyStr.indexOf(input.charAt(j++));
				enc3 = this._keyStr.indexOf(input.charAt(j++));
				enc4 = this._keyStr.indexOf(input.charAt(j++));
		
				chr1 = (enc1 << 2) | (enc2 >> 4);
				chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
				chr3 = ((enc3 & 3) << 6) | enc4;
		
				uarray[i] = chr1;			
				if (enc3 != 64) uarray[i+1] = chr2;
				if (enc4 != 64) uarray[i+2] = chr3;
			}
			return uarray;	
		},
}



var Base64={_keyStr:"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",encode:function(e){var t="";var n,r,i,s,o,u,a;var f=0;e=Base64._utf8_encode(e);while(f<e.length){n=e.charCodeAt(f++);r=e.charCodeAt(f++);i=e.charCodeAt(f++);s=n>>2;o=(n&3)<<4|r>>4;u=(r&15)<<2|i>>6;a=i&63;if(isNaN(r)){u=a=64}else if(isNaN(i)){a=64}t=t+this._keyStr.charAt(s)+this._keyStr.charAt(o)+this._keyStr.charAt(u)+this._keyStr.charAt(a)}return t},decode:function(e){var t="";var n,r,i;var s,o,u,a;var f=0;e=e.replace(/[^A-Za-z0-9\+\/\=]/g,"");while(f<e.length){s=this._keyStr.indexOf(e.charAt(f++));o=this._keyStr.indexOf(e.charAt(f++));u=this._keyStr.indexOf(e.charAt(f++));a=this._keyStr.indexOf(e.charAt(f++));n=s<<2|o>>4;r=(o&15)<<4|u>>2;i=(u&3)<<6|a;t=t+String.fromCharCode(n);if(u!=64){t=t+String.fromCharCode(r)}if(a!=64){t=t+String.fromCharCode(i)}}t=Base64._utf8_decode(t);return t},_utf8_encode:function(e){e=e.replace(/\r\n/g,"\n");var t="";for(var n=0;n<e.length;n++){var r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r)}else if(r>127&&r<2048){t+=String.fromCharCode(r>>6|192);t+=String.fromCharCode(r&63|128)}else{t+=String.fromCharCode(r>>12|224);t+=String.fromCharCode(r>>6&63|128);t+=String.fromCharCode(r&63|128)}}return t},_utf8_decode:function(e){var t="";var n=0;var r=c1=c2=0;while(n<e.length){r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r);n++}else if(r>191&&r<224){c2=e.charCodeAt(n+1);t+=String.fromCharCode((r&31)<<6|c2&63);n+=2}else{c2=e.charCodeAt(n+1);c3=e.charCodeAt(n+2);t+=String.fromCharCode((r&15)<<12|(c2&63)<<6|c3&63);n+=3}}return t}}


		return {
			writePhotoToLocalStorage : function(base64File,receivedDate, fileName){
				var defer = $q.defer();
						
				var content = Base64Binary.decodeArrayBuffer(base64File.replace("data:image/jpeg;base64,",""));
				
					$nestedDirectory.createDirectory(PhotoStorageDirectory.getFolderName()).then(function(){
						
						
						var localPath = PhotoStorageDirectory.getFullPath() + fileName;
						console.log(localPath)
						var photoObj = {
								path 	: PhotoStorageDirectory.getFolderName()+fileName,
								date 	: receivedDate,
								name	: fileName,
								visitId : PhotoStorageDirectory.getVisitId(),
								wono 	: PhotoStorageDirectory.getWoNo()
						}
						
						console.log(photoObj);

						$localDB.add('galleryPhoto',photoObj,{}).then(function(){
							console.log("am adaugat path")
						    window.resolveLocalFileSystemURL(
					                localPath.substring(0, localPath.lastIndexOf('/')), // retrieve directory
					                function(dirEntry) {
					                    dirEntry.getFile( // open new file in write mode
					                    		fileName,
					                            {create: true, exclusive: false},
					                            function(fileEntry) {
					                                fileEntry.createWriter(
					                                        function (writer) {
					                                            writer.onwriteend = function(evt) {
					                                            };

					                                            writer.write(content);
					                                            WL.Logger.debug('success')
					                                            
					                                            defer.resolve(PhotoStorageDirectory.getFolderName()+fileName);

					                                        },
					                                        function(err){
					                                        	WL.Logger.debug(err)
					                                        	defer.reject(err);
					                                        });
					                            },
					                            function(err){
					                            	WL.Logger.debug(err)
		                                        	defer.reject(err);
					                            });
					                },
					                function(err){
					                	WL.Logger.debug(err)
		                            	defer.reject(err);
				                    });
						    
						});
						
						
					});
				//});
				
				return defer.promise;
			},

			removeFileFromPhoneGap : function(localPath,fileName){
				console.log("@@@@@@@@@@@@@@@@@@");
				console.log(localPath);
				window.resolveLocalFileSystemURL(
		                localPath.substring(0, localPath.lastIndexOf('/')), // retrieve directory
		                function(dirEntry) {
		                    dirEntry.getFile( // open new file in write mode
		                    		fileName,
		                            {create: true, exclusive: false},
		                            function(fileEntry) {
		                                fileEntry.remove(function(){
		                                	console.log('success removing')
		                                }, function(){
		                                	console.log('error removing file')
		                                });
		                            },
		                            function(){
		                            	WL.Logger.debug('error12')
		                            	console.log("error12")
		                            });
		                },
		                function(){
		                	WL.Logger.debug('error3')
		                	console.log("error3")
	                    });
			}
		}
});