/**
 * @namespace containerHistoryService
 * @desc Service used to manage directories
 * @memberOf Factories
 */
audit.factory("$nestedDirectory", function($q) {
  return {
	//Exposed function to create a directory to a specified path in PERSISTENT local file system
    createDirectory: function(path) {
      var defer = $q.defer();
      window.requestFileSystem =
        window.requestFileSystem || window.webkitRequestFileSystem;
      window.requestFileSystem(
        LocalFileSystem.PERSISTENT,
        0,
        function gotFS(fileSystem) {
          window.FS = fileSystem;

          var printDirPath = function(entry) {
          };

          createDirectory(path, printDirPath, defer);
        },
        fail
      );
      return defer.promise;
    }
  };

  function fail() {
    console.log("failed to get filesystem");
  }

  /**
	* @name createDirectory
	* @desc Create a directory to a specified path 
	* @param {Function} success callback for success handling
	* @param {Object} defer for resolving the promise
	* @returns {Promise}
	* @memberOf Services.nestedDirectory
  */
  function createDirectory(path, success, defer) {
    var dirs = path.split("/").reverse();
    var root = window.FS.root;

    var createDir = function(dir) {
      root.getDirectory(
        dir,
        {
          create: true,
          exclusive: false
        },
        successCB,
        failCB
      );
    };

    var successCB = function(entry) {
      root = entry;
      if (dirs.length > 0) {
        createDir(dirs.pop());
      } else {
        console.log("all dir created");
        defer.resolve("success");
        success(entry);
      }
    };

    var failCB = function() {
      defer.reject("error in creating the folders");
      console.log("failed to create dir " + dir);
    };

    createDir(dirs.pop());
  }
});
