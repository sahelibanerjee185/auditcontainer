/**
 * May, 30 2016
 * ArchiveService.js
 * Eugen Buzila
 */

/**
 * @namespace ArchiveService
 * @desc Service used to upload and download photos archives from server
 * @memberOf Factories
 */
(function() {
  "use strict";

  audit.factory("ArchiveService", ArchiveService);

  ArchiveService.$inject = ["$localDB", "responseHandler"];

  function ArchiveService($localDB, responseHandler) {
    var service = {
      addArchiveToServer: addArchiveToServer,
      getArchiveFromServer: getArchiveFromServer
    };

    return service;

    /**
	* @name addArchiveToServer
	* @desc Call PhotosArchiveAdapter to upload a base64 photos archive on server 
	* @param {Integer} visitId unique identifier for an existing visit
	* @param {String} archive base64 encoded string representing an archive of photos
	* @returns {Function} - Standard success/fail function 
	* @memberOf Factories.ArchiveService
	*/
    function addArchiveToServer(visitId, archive) {
      var defer = $.Deferred();
      var username = userContext.getUsername();

      WL.Client.invokeProcedure(
        {
          adapter: "PhotosArchiveAdapter",
          procedure: "addArchive",
          parameters: [userContext.getUsername(), visitId, archive]
        },
        {
          onSuccess: function(response) {
            successHandler(response, defer);
          },
          onFailure: function(error) {
            errorHandler(error, defer);
          }
        }
      );

      return defer.promise();
    };

	/**
	* @name getArchiveFromServer
	* @desc Call PhotosArchiveAdapter to download a base64 photos archive from server 
	* @param {String} archiveName to be downloaded from server
	* @returns {Function} - Standard success/fail function 
	* @memberOf Factories.ArchiveService
	*/
    function getArchiveFromServer(archiveName) {
      var defer = $.Deferred();

      WL.Client.invokeProcedure(
        {
          adapter: "PhotosArchiveAdapter",
          procedure: "getPictureArchive",
          parameters: [
            userContext.getUsername(),
            userContext.getToken(),
            archiveName
          ]
        },
        {
          onSuccess: function(response) {
            successHandler(response, defer);
          },
          onFailure: function(error) {
            errorHandler(error, defer);
          }
        }
      );
      return defer.promise();
    }

	//Standard success handler
    function successHandler(response, defer) {
      if (!response.invocationResult.isSuccessful) {
        defer.reject(response.invocationResult);
      } else {
        var resultSet = response.invocationResult.invocationResult;
        defer.resolve(resultSet);
      }
    }

	//Standard fail handler
    function errorHandler(error, defer) {
      console.log("[ArchiveService] error", error);
      defer.reject(error);
    }

	//Error handler with feedback
    function errorHandlerWithFeedback(error, defer) {
      console.log("[ArchiveService] error", error);
      defer.reject(error);
      responseHandler.feedback(error);
    }
  }
})();
