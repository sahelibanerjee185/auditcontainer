/**
 * May, 30 2016
 * archiveModule.js
 * Eugen Buzila
 */

/**
 * @namespace ArchiveModule
 * @desc Service used to upload and download photos archives from server, zip and unzip archives
 * @memberOf Factories
 */
(function () {
	'use strict';

	audit.factory('archiveModule', archiveModule);

	archiveModule.$inject = ['$localDB', 'GalleryExportService', 'Base64Service',
		'ArchiveService', 'GalleryImportService', 'PhotoStorageDirectory'
	];

	function archiveModule($localDB, GalleryExportService, Base64Service,
		ArchiveService, GalleryImportService, PhotoStorageDirectory) {

		//Public exposed operations
		var service = {
			zip: zip,
			unzip: unzip,
			addArchiveToServer: addArchiveToServer,
			getArchiveFromServer: getArchiveFromServer,
			finishVisit: finishVisit,
			saveArchiveFromServer: saveArchiveFromServer,
			finishPendingVisit: finishPendingVisit
		};

		return service;

		/**
		* @name zip
		* @desc Create an archive using all photos from the current "In Progress" visit
		* Calls GalleryExportService.exportZipAsByteArray to prepare the archive 
		* @returns {Promise} 
		* @resolve ByteArray
		* @memberOf Factories.archiveModule
		*/
		function zip() {
			var defer = $.Deferred();

			GalleryExportService.exportZipAsByteArray().then(function (response) {
					console.log('zip exported ok');
					defer.resolve(Base64Service.fromByteArray(response));
					GalleryExportService.saveZip('example2.zip', Base64Service.toByteArray(Base64Service.fromByteArray(response))).then(function () {
						console.log('zip saved to disk');
					});
				})
				.fail(function (error) {
					WL.Logger.error("[ArchiveModule][zip][exportZipAsByteArray] username: " + userContext.getUsername() + JSON.stringify(error));
					defer.reject("Error zipping the file");
				});

			return defer.promise();
		}

		/**
		* @name unzip
		* @desc Unzip the photos archive retrieved from the specified zipName
		* @param {String} zipName - name of the archive to be unzipped
		* @returns {Promise} 
		* @memberOf Factories.archiveModule
		*/
		function unzip(zipName) {

			var defer = $.Deferred();
			GalleryImportService.unzip(cordova.file.tempDirectory + zipName, PhotoStorageDirectory.getDocumentsFolder(), function () {
				console.log('Zip extracted');
				defer.resolve("Successfully unzipping file!");
			})

			return defer.promise();
		}

		/**
		* @name addArchiveToServer
		* @desc Upload a base64 photos archive on server 
		* @param {String} base64Zip base64 encoded string representing an archive of photos
		* @returns {Promise} 
		* @memberOf Factories.archiveModule
		*/
		function addArchiveToServer(base64Zip) {
			var defer = $.Deferred();
			ArchiveService.addArchiveToServer(PhotoStorageDirectory.getVisitId(), base64Zip).then(function (data) {
				defer.resolve(data);
			}).fail(function (error) {
				WL.Logger.error("[ArchiveModule][addArchiveToServer] username: " + userContext.getUsername() + " Error adding archive to server with error: " + JSON.stringify(error));
				defer.reject("Error adding archive to server!");
			})
			return defer.promise();
		};

		/**
		* @name addPendingArchiveToServer
		* @desc Upload a base64 photos archive on server for finished offline visits (Pending Visits)
		* @param {String} base64Zip base64 encoded string representing an archive of photos
		* @param {Integer} visitId unique identifier for an existing visit
		* @returns {Promise} 
		* @memberOf Factories.archiveModule
		*/
		function addPendingArchiveToServer(base64Zip, visitId) {
			var defer = $.Deferred();
			ArchiveService.addArchiveToServer(visitId, base64Zip).then(function (data) {
				defer.resolve(data);
			}).fail(function (error) {
				WL.Logger.error("[ArchiveModule][addPendingArchiveToServer] username: " + userContext.getUsername() + " Error adding pending archive to server with error: " + JSON.stringify(error));
				defer.reject("Error adding archive to server!");
			})
			return defer.promise();
		};

		/**
		* @name getArchiveFromServer
		* @desc Download the photos archive from server 
		* @param {String} archiveName to retrieve 
		* @returns {Promise} 
		* @memberOf Factories.archiveModule
		*/
		function getArchiveFromServer(archiveName) {
			var defer = $.Deferred();
			ArchiveService.getArchiveFromServer(archiveName).then(function (data) {
				console.log("Archive successfully downloaded from server")
				defer.resolve(data);
			}).fail(function (err) {
				defer.reject("Error getting archive from server!");
			})
			return defer.promise();
		};

		/**
		* @name finishVisit
		* @desc Prepare the zip archive and upload it on server
		* @returns {Promise} 
		* @resolve {zipName}
		* @memberOf Factories.archiveModule
		*/
		function finishVisit() {
			var defer = $.Deferred();
			zip().then(function (base64Zip) {
				addArchiveToServer(base64Zip).then(function (zipName) {
					defer.resolve(zipName);
				}).fail(function (err) {
					WL.Logger.error("[ArchiveModule][finishVisit][addArchiveToServer] username: " + userContext.getUsername() + JSON.stringify(err));
					defer.reject(err);
				})
			}).fail(function (err) {
				WL.Logger.error("[ArchiveModule][finishVisit][zip] username: " + userContext.getUsername() + JSON.stringify(err));
				defer.reject(err);
			})
			return defer.promise();
		};

		/**
		* @name finishPendingVisit
		* @desc Retrieve the zip archive of photos from local and upload it on server
		* @returns {Promise} 
		* @resolve {zipName}
		* @memberOf Factories.archiveModule
		*/
		function finishPendingVisit(visitId) {
			var defer = $.Deferred();

			GalleryExportService.getLocalFileAsBase64(visitId + ".zip").then(function (base64Zip) {
				if (base64Zip === null) {
					// No archive to add to server
					defer.resolve(null);
				} else {
					return addPendingArchiveToServer(base64Zip, visitId);
				}
			}).then(function (zipName) {
				defer.resolve(zipName);
			}).fail(function (error) {
				WL.Logger.error("[ArchiveModule][finishPendingVisit] username: " + userContext.getUsername() + " " + JSON.stringify(error));
				defer.reject(error);
			});

			return defer.promise();
		}

		/**
		* @name saveArchiveFromServer
		* @desc Download the archive from server and save it locally
		* @returns {Promise} 
		* @resolve {"Success"}
		* @memberOf Factories.archiveModule
		*/
		function saveArchiveFromServer(zipName) {
			var defer = $.Deferred();

			getArchiveFromServer(zipName).then(function (base64Zip) {
				GalleryExportService.saveZip('test.zip', Base64Service.toByteArray(base64Zip)).then(function () {
					console.log('zip saved to disk');
					unzip('test.zip').then(function () {
						defer.resolve("Success");
					})
				});
			}).fail(function (err) {
				WL.Logger.error("[ArchiveModule][saveArchiveFromServer] username: " + userContext.getUsername() + " " + JSON.stringify(err));
				defer.reject(err);
			})
			return defer.promise();
		};

	}
})();