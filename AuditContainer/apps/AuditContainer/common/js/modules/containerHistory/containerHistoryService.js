/**
 * Apr, 4 2016
 * containerHistoryService.js
 * Stavarache Vlad
 */
/**
 * @namespace containerHistoryService
 * @desc Service used retrieve container history
 * @memberOf Factories
 */
(function () {
	'use strict';

	audit
		.factory('containerHistoryService', containerHistoryService);

	containerHistoryService.$inject = ['$localDB', 'responseHandler', '$ionicLoading'];

	function containerHistoryService($localDB, responseHandler, $ionicLoading) {

		var containerHistoryRepair = {};
		var containerHistoryArray = [];

		var service = {
			getContainerHistory: getContainerHistory,
			_buildHistoryStructure: _buildHistoryStructure,
			getContainerHistoryRepair: getContainerHistoryRepair,
			getContainerHistoryArray: getContainerHistoryArray,
			getWorkOrderRemarksHistoryArray: getWorkOrderRemarksHistoryArray
		};

		return service;

		
		/**
		* @name _buildHistoryStructure
		* @desc Helper function to build a structure of data to be displayed in UI
		* @param {Array} resultSet array of objects representing history of a container (repairs,parts)
		* @returns {Promise}
		* @memberOf Services.containerHistoryService
		*/
		function _buildHistoryStructure(resultSet) {
			containerHistoryArray = [];
			containerHistoryRepair = {};

			for (var index in resultSet) {
				var repairWorkOrder = resultSet[index];

				if (!containerHistoryRepair[repairWorkOrder.workOrderId]) {
					var workOrderInfo = {
						shopCode: repairWorkOrder.shopCode,
						shopName: repairWorkOrder.shopName,
						repairDate: repairWorkOrder.repairDate,
						locationName: repairWorkOrder.locationName,
						locationCode: repairWorkOrder.locationCode,
						workOrderId: repairWorkOrder.workOrderId,
						totalRepairCost: repairWorkOrder.totalRepairCost,
						repairMode: repairWorkOrder.repairMode
					};

					containerHistoryRepair[repairWorkOrder.workOrderId] = [];
					containerHistoryArray.push(workOrderInfo);
				}

				validatePartRepair(containerHistoryRepair[repairWorkOrder.workOrderId], {
					parts: repairWorkOrder.parts,
					damageCode: repairWorkOrder.damageCode,
					repairCode: repairWorkOrder.repairCode,
					repairLocCode: repairWorkOrder.repairLocCode,
					repairDescription: repairWorkOrder.repairDescription,
					repairMode: repairWorkOrder.repairMode,
					partDescription: repairWorkOrder.partDescription,
					partNumber: repairWorkOrder.partNumber
				});
			}
		}

		//Check existence of an item in an array and push it if doesn't exist to avoid duplicates
		function validatePartRepair(repairPartArray, item) {
			if (item.partNumber && item.partNumber.length > 0) {
				for (var index in repairPartArray) {
					var repairPart = repairPartArray[index];
					if (repairPart.repairCode == item.repairCode && repairPart.repairLocCode == item.repairLocCode) {
						repairPartArray.splice(index + 1, 0, item);
						break;
					}
				}
			} else {
				repairPartArray.push(item);
			}
		}

		function getContainerHistoryRepair() {
			return containerHistoryRepair;
		}

		function getContainerHistoryArray() {
			return containerHistoryArray;
		}

		/**
		* @name getWorkOrderRemarksHistoryArray
		* @desc Get the remarks history for a specified work order
		* @param {Integer} workOrderId The ID of the work order
		* @returns {Promise}
		* @memberOf Services.containerHistoryService
		*/
		function getWorkOrderRemarksHistoryArray(workOrderId) {
			var defer = $.Deferred();
			WL.Client.invokeProcedure({
				adapter: 'ContainerHistoryAdapter',
				procedure: 'getWORemarksHistory',
				parameters: [userContext.getUsername(), userContext.getToken(), workOrderId]
			}, {
					onSuccess: function (response) {
						if (!response.invocationResult.isSuccessful) {
							defer.reject(response.invocationResult);
						} else {
							var resultSet = response.invocationResult.invocationResult;
							var parsedResult = resultSet.map(function (entry) {
								entry.source = parseRemarkSourceType(entry.source)
								return entry;
							})

							defer.resolve(resultSet);
						}
					},
					onFailure: function (error) {
						errorHandlerWithFeedback(error, defer);
					}
				});
			return defer.promise();
		};

		//Parse the remark source received from DB for UI display
		function parseRemarkSourceType(source) {
			switch (source) {
				case 'S':
					return 'System';
				case 'I':
					return "Internal";
				case 'E':
					return "External";
				case 'V':
					return "Shop Remarks";
				default:
					return "Unknown";
			}
		};

		/**
		* @name getContainerHistory
		* @desc Get the container history for a specified container
		* @param {Integer} containerNo The ID of the container
		* @returns {Function} - standard function to handle success and fail
		* @memberOf Services.containerHistoryService
		*/
		function getContainerHistory(containerNo) {
			var defer = $.Deferred();
			WL.Client.invokeProcedure({
				adapter: 'ContainerHistoryAdapter',
				procedure: 'getContainerHistory',
				parameters: [userContext.getUsername(), userContext.getToken(), containerNo]
			}, {
					onSuccess: function (response) {
						successHandler(response, defer)
					},
					onFailure: function (error) {
						errorHandlerWithFeedback(error, defer);
					}
				});
			return defer.promise();
		}

		//Standard success handler
		function successHandler(response, defer) {
			if (!response.invocationResult.isSuccessful) {
				defer.reject(response.invocationResult);
			} else {
				var resultSet = response.invocationResult.invocationResult;
				var parsedResult = resultSet.map(function (entry) {
					entry.totalRepairCost = parseFloat(entry.totalRepairCost).toFixed(2);
					return entry;
				})

				service._buildHistoryStructure(resultSet)
				defer.resolve(resultSet);
			}
		}

		//Standard error handler
		function errorHandler(error, defer) {
			console.log('error', error);
			$ionicLoading.hide();
			defer.reject(error);
		}

		//Error handler with feedback
		function errorHandlerWithFeedback(error, defer) {
			console.log('error', error);
			$ionicLoading.hide();
			defer.reject(error);
			responseHandler.feedback(error);
		}
	}
})();