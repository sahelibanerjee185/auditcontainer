
/* JavaScript content from js/messages.js in folder common */

/**
 * Globalization messages
 */
var RoutingTitles = {
	login : "Login",
	inspections:"Inspections",
	planVisitTab:"Plan Visit",
	manualsTab: "Manuals",
	kpiTab:"Inspection History - KPI",
	unitRefer:"Inspections",
	logOutTab:"Log out",
	gallery:"Gallery",
	addPhoto:"Add Photo",
	photo: 'Take Photo',
	failedInspections:'Failed Inspections',
	startedVisitWO : 'Work Orders',
	WODetails : 'Work Order Details',
	startedVisitWODetails : 'Started Visit | Work Order Details',
	previewPdf : 'Started Visit PDF Preview',
    started : "Started Visit | Work Orders",
    upcoming : "Inspections | Upcoming Visits",
    past : "Inspections | Past Visits",
    missed : "Inspections | Missed Visits",
    month : "Inspections | Month View"
};


var ManualsLanguages = {
		EN : "English",
		DE :  "German",
		DK :  "Danish"
}

/**
 * English
 */
Messages = {
	/* Utility */
	locationPathIndex : {
		'surveyTab'		: 'InspectionsIndex',
		'planVisitTab'		: 'PlanVisitIndex',
		'manualsTab'		: 'ManualsButtonIndex',
		'gallery'		: 'GalleryButtonIndex',
		'addPhoto'		: 'AddPhotoButtonIndex',
		'kpiTab'			: 'KpiButtonIndex',
		'logOutTab'		: 'LogOutButtonIndex'
	},
	datePattern : /(\d{2})\-(\d{2})\-(\d{4})/,
	countriesArray : [ 
	    { name: 'All Countries', imagePath: ''},
        { name: 'Denmark', imagePath: 'images/flags/denmark.PNG'},
  		{ name: 'France',  imagePath: 'images/flags/france.PNG'},
  		{ name: 'Greece',  imagePath: 'images/flags/greece.PNG'},
  		{ name: 'Norway',  imagePath: 'images/flags/norway.PNG'},
  		{ name: 'Spain',   imagePath: 'images/flags/spain.PNG'},
  		{ name: 'Italy',   imagePath: 'images/flags/italy.PNG'},
  		{ name: 'Russia',  imagePath: 'images/flags/russia.PNG'},
  		{ name: 'Liechtenstein', imagePath: 'images/flags/lichtenstein.PNG'},
  		{ name: 'Turkey',  imagePath: 'images/flags/turkey.PNG'},
    ],
	
	/* Login */
	username : "Username",
	password : "Password",
	remember: "Remember user name",
	signIn:"Sign In",
	forgotPass: "Forgot password?",
	back:"Back",

	/* User Settings */
	submit:"SUBMIT",
	location:"Location",
	email:"Email",
	cluster:"Cluster",
	repairShop:"Repair Shop",
	currency:"Currency",
	
	/* Inspections */
	jan: "January",
	feb: "February",
	mar: "March",
	apr: "April",
	may: "May",
	jun: "June",
	jul: "July",
	aug:"August",
	sept:"September",
	oct: "October",
	nov: "November",
	dec: "December",
	monday:"Monday",
	tuesday:"Tuesday",
	wednesday:"Wednesday",
	thursday:"Thursday",
	friday:"Friday",
	saturday:"Saturday",
	sunday:"Sunday",
	mon:"Mon",
	tue:"Tue",
	wed:"Wed",
	thu:"Thu",
	fri:"Fri",
	sat:"Sat",
	sun:"Sun",
	week:"Week",
	month:"Month",
	
	/* Depot Details */
	depotName:"Depot Name",
	startVisit:"Start visit",
	finishVisit:"Complete visit",
	finishedVisit:"Finished visit",
	missedVisit:"Missed visit",
	startInspection:"Start inspection",
	finishInspection:"Finish inspection",
	finishedInspection : 'Finished inspection',
	requestContainer:"Request Container",
	previousReports:"Previous Reports",
	addedContainers:"Added Containers",
	succesfulPreInspect:"Succesful Pre-Inspections",
	failedPreInspect:"Failed Pre-Inspections",
	containerType:"Container Type",
	containerNo:"Container No.",
	status:"Status",
	repairCost:"Repair Cost",
	date:"Date",
	remove:"Remove",
	history:"History",
	pending: 'Pending',
	planned :'Planned',
	missed : 'Missed',
	inProgress:'In progress',
	finished :'Finished',
	started:'Started',
	
		
	/* Reefer unit */
	reeferUnit:"Reefer Unit",
	pcs:"Pcs.",
	damageCode:"Damage Code",
	repairCode:"Repair Code",
	repairLoc:"Repair Loc Code",
	description:"Description",
	sparePartNr:"Spare Part Number",
	manHrs:"Man Hrs/Piece:",
	tpiIndicator:"TPI Indicator:",
	coreTag:"Core s/n or Tag No.:",
	materialCost: "Material Cost/Piece:",
	partNumber:"Part Number:",
	partCost:"Part Cost:",
	totalPerCode:"Total per Code:",
	
	/* Plan Visit */
	
	depot:"Depot",
	preRepair:"Pre Repair",
	postRepair:"Post Repair",
	all:"ALL",	
	searchDepots:"Search for depots",
	
	/* Modals */
	modalRemove:"Are you sure you want to remove this container?",
	cancel:"Cancel",
	confirm:"Confirm",
	ok:"Ok",
	repairDate:"Repair Date",
	repairShopNo:"Repair shop No. ",
	woType:"WO Type",
	photoGallery:"Photo Gallery",
	photos:"Photos",
	repairShopName:"Repair Shop Name",
	modalSubmit:"Are you sure you want to submit changes?",
	repairMode:"Repair Mode",
	addContainer:"Add Container",
	add:"Add",
	
	//modal 2 parts - Container sucessfully submitted
	container:"Container",
	succesfullySubmitted:"has been successfully submitted for inspection.",

	//modal in 2 parts
	modalCalendar:"Are you sure you want to add Container ",
	modalCalendar2:"to Calendar?",
	
	modalSubmitChanges:"Are you sure you want to submit changes?",
	infoChanges:"There are several changes that you made highlighted with orange.",
	searchForRepairShop:"Please search below the Repair Shop details",
	repairShopId:"Repair Shop Name/ID",
	
	/* Modal Change Status */
	changeStatus:"Change status for ",
	approved:"Approved",
	notApproved:"Not Approved",
	pending:"Pending",
	statusName:"Status Name",
	otherStatus:"Other status",
	anotherStatus:"Another status",
	modalSmallerFiles:"Please use smaller files!",
	modalForSameFiles: "Please do not attach the same file multiple times!",
	modalFilesSmaller: "Please use files smaller than 4MB!",
	modalMaxOfFiles: "You can attach a maximum of 4 files!",
	
	/*Modal Logout*/
	modalLogout: "You are about to logout. Are you sure you want to continue?",
	
	/* Auto-generated Comment Keywords */
		
	remove : "delete",
	changedTo : "to",
	quantity : "Pcs",
	damageCode : "Dmg Code",
	TPI : "TPI",
	repairCode : "Rep Code",
	repairLocCode : "Rep Loc Code",
	partCode : "Eq No",
	addRepair : "Add Repair",
	addPart : "Add Part"
};
	
function addMessages (scope, messages){
		for(index in messages){
			scope[messages[index]] = Messages[messages[index]];
		}
			
}
