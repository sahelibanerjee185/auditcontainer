/**
 * Oct 3, 2017
 * build.js
 * StavaracheVlad
*/

/**
 * buildVersionService Factory
 * @namespace Factories
 */
(function(){
	'use strict';
	
	audit
    .factory('buildVersionService', buildVersionService);
	
	buildVersionService.$inject = [];
	
	/**
     * @namespace buildVersionService
     * @desc Returns the App Version as configured in the application-descriptor.xml, reflected in the worklight.plist (iOS) or worklight.properties (Android)
     * @memberOf Factories
     */
	function buildVersionService() {
		var service = {
			getAppBuild : getAppBuild,
			getPrettyAppBuild : getPrettyAppBuild,
		};
		
		return service;
		
		/**
		 * @name getPrettyAppBuild
	     * @desc returns pretty app build version and environment
	     * @param {N/A}
	     * @returns {Promise}
	     * @memberOf buildVersionService
		 */
		function getPrettyAppBuild() {
			var defer = $.Deferred();
			
			getAppBuild().
			then(function(build){
				var build = "v" + build;
				/** HARDCODE -- START **/
				// build = "v2.6.0 PREPROD NO MDM"
				/** HARDCODE -- END **/
				defer.resolve(build);
			});
			
			return defer.promise();
		}
		
		/**
	     * @name getAppBuild
	     * @desc returns app build version and environment
	     * @param {N/A}
	     * @returns {Promise}
	     * @memberOf buildVersionService
	     */
		function getAppBuild() {
			var defer = $.Deferred();
			
			var version = WL.Client.getAppProperty(WL.AppProperty.APP_VERSION);
			WL.App.getServerUrl(function(url){
				var environment = getEnvironmentFromServerUrl(url);
				version = version + " " + environment;
				defer.resolve(version);
			}, function(err){
				defer.resolve(version);
			});
			
			return defer.promise();
		}	
		
		function getEnvironmentFromServerUrl(url) {
			var environment = "";
			
			if (url.indexOf("mob-dev") != -1) {
				environment = "Dev";
			} else if (url.indexOf("mob-preprod") != -1) {
				environment = "UAT";
			}
			
			return environment;
		}
	}
})();
